@setlocal

@cd ..
@cd ..

@set classpath=%classpath%;./bin

@rem for HttpBehavior (if with authentication)
@set classpath=%classpath%;./lib/commons/commons-codec-1.10.jar

@rem for ServerBehavior
@set classpath=%classpath%;./lib/jetty/jetty-server-9.4.11.v20180605.jar;./lib/jetty/jetty-servlet-9.4.11.v20180605.jar;./lib/jetty/jetty-io-9.4.11.v20180605.jar;./lib/jetty/jetty-http-9.4.11.v20180605.jar;./lib/jetty/jetty-continuation-9.4.11.v20180605.jar;./lib/jetty/jetty-util-9.4.11.v20180605.jar;./lib/jetty/jetty-security-9.4.11.v20180605.jar;./lib/jetty/servlet-api-3.1.jar

@rem for EmailSenderBehavior
@set classpath=%classpath%;./lib/mail/javax.mail.jar

@rem for JsonReaderBehavior and JsonOntonut
@set classpath=%classpath%;./lib/json/json.jar

@rem for ExcelReaderBehavior
@set classpath=%classpath%;./lib/poi/poi-3.17.jar;./lib/poi/poi-ooxml-3.17.jar;./lib/poi/poi-ooxml-schemas-3.17.jar;./lib/poi/xmlbeans-2.6.0.jar;./lib/poi/commons-collections4-4.1.jar

@rem for RdfReaderBehavior
@set classpath=%classpath%;./lib/rdf4j/eclipse-rdf4j-2.3.2-onejar.jar;./lib/commons/commons-io-2.6.jar;./lib/slf4j/slf4j-api-1.7.25.jar;./lib/slf4j/slf4j-jdk14-1.7.25.jar 

@rem PostgreSQL driver for SqlReaderBehavior
@rem @set classpath=%classpath%;./lib/postgresql/postgresql-42.2.2.jar

@rem for SpeechBehavior
@rem @set classpath=%classpath%;./lib/marytts/marytts-client-5.0-jar-with-dependencies.jar

@rem for XmppReceiverBehavior, XmppSenderBehavior, InstantSurveyBehavior
@rem @set classpath=%classpath%;./lib/smack/smack.jar;./lib/smack/smackx.jar;./lib/smack/smackx-debug.jar;./lib/smack/smackx-jingle.jar

@rem for InstantSurveyBehavior
@rem @set classpath=%classpath%;./lib/simple-xml/simple-xml-2.7.jar

@rem for WebSocketOntonutListenerBehavior
@rem @set classpath=%classpath%;./lib/tyrus/tyrus-standalone-client-1.10.jar


@rem java sapl.SaplTest


java sapl.Sapl -name saplTest apps/test/test.sapl


@endlocal