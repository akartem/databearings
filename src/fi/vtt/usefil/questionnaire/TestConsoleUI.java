package fi.vtt.usefil.questionnaire;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** 
 * Test console-based UserCommunicationInterface for Instant Survey.
 *  Implementation of timeouts is incomplete: 
 *  works as such but if the last question timed out, System.in thread is left hanging.  
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class TestConsoleUI implements UserCommunicationInterface {

	final static String SORRY_MESSAGE = "Sorry, did not understand your answer. Try again";

	String input;
	Thread th;
	
	Boolean inputNotifier = new Boolean(true);

	@Override
	public boolean startup() {
		return true;
	}

	@Override
	public void sendMessage(Message message) {
		System.out.println(message.getText().trim());
	}

	@Override
	public Object askQuestion(Question question) {
		System.out.println(question.getText());
		for(Answer answer : question.getAnswers()){
			String possibleInput = "'"+answer.getValue()+"'";
			if(answer.getShortcut()!=null) possibleInput+=" or '"+answer.getShortcut()+"'";
			System.out.println("    "+answer.getText()+" (answer "+possibleInput+")");
		}

		int timeout = -1;
		Timeout t = question.getTimeout();
		if(t!=null){
			timeout = t.getValue().intValue();
		}
		
		while(true){
			input = "";
			if(th==null){
				th = new Thread(new Runnable() {
					public void run() {
						try {
							BufferedReader br = new BufferedReader(new InputStreamReader(System.in));						
							input = br.readLine();
						} catch (IOException e) {
							e.printStackTrace();
							input = null;
						}
						th = null;
						synchronized (inputNotifier) {
							inputNotifier.notify();
						}						
					}
				});
				th.start();
			}

			long now = System.currentTimeMillis();
			synchronized (inputNotifier) {
				try {
					if(timeout!=-1)
						inputNotifier.wait(timeout);
					else inputNotifier.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
			}

			if(input==null) return null;
			else 
				if(input.equals("") && timeout!=-1 && System.currentTimeMillis()>=now+timeout-10){
					return t;
				}
			
			for(Answer answer : question.getAnswers()){
				if(input.equals(answer.getValue()) || input.equals(answer.getShortcut()))
					return answer;
			}

			System.out.println(SORRY_MESSAGE);
		}
	}

	@Override
	public void shutdown() {
	}

	@Override
	public void endQuestionnaire() {
	}

}
