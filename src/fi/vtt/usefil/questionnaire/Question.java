package fi.vtt.usefil.questionnaire;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Namespace;

/** 
 * Representation of a question in an Instant Survey   
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

@Root
@NamespaceList({
	@Namespace(reference="http://usefil.eu/dialog#"),
	@Namespace(reference="http://usefil.eu/ontology#", prefix="usefil")
})
public class Question extends Message{

	@Attribute (required=false)
	private String id;
	
	@Attribute(required=false)
	private String wordsets;

	@Element(required=false)
	@Namespace(reference="http://usefil.eu/ontology#")
	private String observedProperty;

	@ElementList(entry="Answer",inline=true,required=false)
	private List<Answer> answers;

	@Element (name="Timeout", required=false)
	@Namespace(reference="http://usefil.eu/dialog#")
	private Timeout timeout;	
	
	public Question (){
		super();
	}
	
	public Question (Question toCopy){
		super(toCopy);
		this.id = toCopy.id;
		this.text = toCopy.text;
		this.wordsets = toCopy.wordsets;
		this.observedProperty = toCopy.observedProperty;
		this.timeout = toCopy.timeout;
	}
	
	public List<Answer> getAnswers(){
		return answers;
	}
	public void add(Answer answer){
		if(answers==null) answers = new ArrayList<Answer>();
		answers.add(answer);
	}
	
	public String getId(){
		return id;
	}
	
	public Timeout getTimeout(){
		return timeout;
	}

	public String getObservedProperty(){
		return observedProperty;
	}
	public String getWordsets(){
		return wordsets;
	}
}




