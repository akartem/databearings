package fi.vtt.usefil.questionnaire;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/** 
 * The Instant Survey Engine.
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.3 (19.03.2015)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2015, VTT
 * 
 * */ 

public class DialogEngine {

	public static final String START_STATE = "start";
	public static final String END_STATE = "end";

	protected Questionnaire questionnaire = null;

	protected HashMap<String,Message> statesMap = new HashMap<String,Message>(); 

	protected String currentState = START_STATE;

	UserCommunicationInterface userCommunicationInterface = null;

	DatabaseCommunicationInterface databaseCommunicationInterface = null;

	public void setUserCommunicationInterface(UserCommunicationInterface commInterface){
		this.userCommunicationInterface = commInterface;
	}

	public void setDatabaseCommunicationInterface(DatabaseCommunicationInterface commInterface){
		this.databaseCommunicationInterface = commInterface;
	}

	public void initializeFromFile(String filename) {
		this.questionnaire = getQuestionnaireFromFile(filename);
		preprocessQuestionnaire();
	}

	protected Questionnaire getQuestionnaireFromFile(String filename) {
		Serializer serializer = new Persister();

		FileInputStream in;
		try {
			in = new FileInputStream(filename);
			Reader source = new InputStreamReader(in, "utf-8");  
			Questionnaire q = serializer.read(Questionnaire.class, source);
			return q;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}	

	protected void preprocessQuestionnaire(){

		List<Message> messages = questionnaire.getMessages();
		if(messages!=null){
			for(Message message : messages){
				preprocessMessage(message);
			}
		}

		List<Question> questions = questionnaire.getQuestions();
		if(questions!=null){
			for(Question question : questions){
				preprocessMessage(question);
			}		
		}
	}

	protected void preprocessMessage(Message message){
		String state = message.getState();

		if(state==null){
			System.err.println("Warning: Top level messages/questions must have 'state' defined: Message/question with text '"+message.getText()+"' will be ignored");
			return;
		}
		if(statesMap.containsKey(state)){
			System.err.println("Warning: Duplicated state '"+state+"': Message/question with text '"+message.getText()+"' will be ignored");
			return;
		}

		statesMap.put(state, message);

		message.setOriginalText(message.getText());

	} 

	public void run(){
		while(!currentState.equals(END_STATE)){

			Message message = statesMap.get(currentState);

			List<Query> queries = message.getQueries();

			if(queries!=null && databaseCommunicationInterface!=null){
				String query = null;
				for(Query q : queries){
					if(q.getType().toLowerCase().equals(databaseCommunicationInterface.getQueryLanguageName().toLowerCase()))
						query = q.getText();
				}

				if(query!=null){
					query = query.trim().replaceAll("\\s+", " ");
					try {
						List<Map<String, String>> result = databaseCommunicationInterface.executeQuery(query);
						if(result.size()==0){
							System.err.println("Warning: Query defined for message/question with text '"+message.getText().trim()+"' did not yield any results. Ignoring");
						}
						else{
							if(result.size()>1) System.err.println("Warning: Query defined for message/question with text '"+message.getText().trim()+"' yielded more than 1 ("+result.size()+") result. Using the first");
							String newText = message.getOriginalText();
							for(String var : result.get(0).keySet()){
								String replace = "%"+var+"%";
								newText = newText.replace(replace, result.get(0).get(var));
							}
							message.setText(newText);
						} 
					} catch (Exception e) {
						e.printStackTrace();
						//but continue
					}
				}
				else System.err.println("Query(ies) defined for message/question with text '"+message.getText().trim()+"' did not inlclude '"+databaseCommunicationInterface.getQueryLanguageName()+"' . Ignoring");
			}

			Object result = null;
			if(message instanceof Question){
				Question question = (Question)message;

				if(databaseCommunicationInterface!=null)
					question = this.extendAnswers(question);

				result = userCommunicationInterface.askQuestion(question);
				if(result==null)
					throw new Error("Could not get a result from question. Cannot proceed");

				if(result instanceof UserRequest){
					UserRequest req = (UserRequest)result;
					if(req.getValue() == UserRequest.STOP_REQUEST)
						break;
				}

				if(result instanceof Answer){
					Answer answer = (Answer)result;

					if(answer.getMessage()!=null){
						userCommunicationInterface.sendMessage(answer.getMessage());
						try {
							Thread.sleep(500);  
						} catch (InterruptedException e) {
							e.printStackTrace();
						}						
					}

					if(question.getObservedProperty()!=null && answer.getObservationResult()!=null && databaseCommunicationInterface!=null){
						try {
							databaseCommunicationInterface.addMeasurement(question.getObservedProperty(), answer.getObservationResult());
						} catch (Exception e) {
							e.printStackTrace();
							//but continue
						}
					}
				}
			}
			else{
				userCommunicationInterface.sendMessage(message);
			}

			if(result==null || !(result instanceof Timeout)){
				Integer pause = message.getPause();
				if(pause!=null){
					try {
						Thread.sleep(pause);  
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			makeTransition(message, result);			
		}

		userCommunicationInterface.endQuestionnaire();
	}

	private Question extendAnswers(Question question) {
		Question extendedQuestion = null;
		ArrayList<Answer> directAnswers = new ArrayList<Answer>();
		for(Answer answer : question.getAnswers()){
			List<Query> queries = answer.getQueries();
			if(queries==null){
				directAnswers.add(answer);
			}
			else {
				String query = null;
				for(Query q : queries){
					if(q.getType().toLowerCase().equals(databaseCommunicationInterface.getQueryLanguageName().toLowerCase()))
						query = q.getText();
				}

				if(query!=null){
					query = query.trim().replaceAll("\\s+", " ");
					try {
						List<Map<String, String>> result = databaseCommunicationInterface.executeQuery(query);
						if(result.size()==0){
							System.err.println("Warning: Query defined for message/question with text '"+answer.getText().trim()+"' did not yield any results. Ignoring");
						}
						else{
							if(extendedQuestion==null){
								extendedQuestion = new Question(question);
							}
							for(Map<String, String> vars : result){
								Answer newAnswer = new Answer(answer);
								for(String var : vars.keySet()){
									String replace = "%"+var+"%";
									if(newAnswer.getText()!=null)
										newAnswer.setText(newAnswer.getText().replace(replace,vars.get(var)));
									if(newAnswer.getShortcut()!=null)
										newAnswer.setShortcut(newAnswer.getShortcut().replace(replace,vars.get(var)));
									if(newAnswer.getValue()!=null)
										newAnswer.setValue(newAnswer.getValue().replace(replace,vars.get(var)));
									if(newAnswer.getObservationResult()!=null)
										newAnswer.setObservationResult(newAnswer.getObservationResult().replace(replace,vars.get(var)));
									if(newAnswer.getTransition()!=null)
										newAnswer.setTransition(newAnswer.getTransition().replace(replace,vars.get(var)));
								}
								extendedQuestion.add(newAnswer);
							}
						} 
					} catch (Exception e) {
						e.printStackTrace();
						//but continue
					}
				}
				else System.err.println("Query(ies) defined for answer with text '"+answer.getText().trim()+"' did not inlclude '"+databaseCommunicationInterface.getQueryLanguageName()+"' . Ignoring");
			}			

		}

		if(extendedQuestion!=null){
			for(Answer a : directAnswers){
				extendedQuestion.add(a);
			}
			return extendedQuestion;
		}
		else return question;

	}

	protected void makeTransition(Message message, Object result){
		String newState = null;

		if(result==null) newState = message.getTransition();
		else if(result instanceof Timeout){
			newState = ((Timeout)result).getTransition();
		}
		else if(result instanceof Answer){
			Answer answer = (Answer)result;
			if(answer.getTransition()!=null)
				newState = answer.getTransition();
			else 
				newState = message.getTransition();
		}

		if(newState==null)
			throw new Error("No transition specified. Cannot proceed");

		currentState = newState;

		if(!statesMap.containsKey(currentState) && !currentState.equals(END_STATE))
			throw new Error("Transition to unknown state '"+currentState+"' specified. Cannot proceed");
	}

}
