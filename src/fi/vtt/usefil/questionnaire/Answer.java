package fi.vtt.usefil.questionnaire;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Namespace;

/** 
 * Representation of an answer option in an Instant Survey   
 * 
 * Developed in and given a home after FP7 USEFIL project
 *    
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

@Root
@NamespaceList({
	@Namespace(reference="http://usefil.eu/dialog#"),
	@Namespace(reference="http://usefil.eu/ontology#", prefix="usefil")
})
public class Answer {

	@Attribute(required=false)
	private String transition;

	@Element (name="Shortcut", required=false)
	private String shortcut;

	@Element (name = "Text", required=true)
	private String text;

	@Element (name = "Value", required=true)
	private String val;

	@Element (name = "Message", required=false)
	private Message message;

	@Element (required=false)
	@Namespace(reference="http://usefil.eu/ontology#")
	private String observationResult;
	
	@ElementList(entry = "Query",inline=true,required=false)
	@Namespace(reference="http://usefil.eu/database#")
	private List<Query> queries;

	public Answer (){
	}

	public Answer (Answer toCopy){
		this.transition = toCopy.transition;
		this.shortcut = toCopy.shortcut;
		this.text = toCopy.text;
		this.val = toCopy.val;
		this.message = toCopy.message;
		this.observationResult = toCopy.observationResult;
	}
	
	public String getShortcut(){
		return shortcut;
	}
	public void setShortcut(String shortcut){
		this.shortcut = shortcut;
	}

	public String getText(){
		return text;
	}
	public void setText(String text){
		this.text = text;
	}
	
	public String getValue(){
		return val;
	}
	public void setValue(String value){
		this.val = value;
	}	

	public Message getMessage(){
		return message;
	}	
	
	public String getTransition(){
		return transition;
	}
	public void setTransition(String transition){
		this.transition = transition;
	}
	
	public String getObservationResult(){
		return observationResult;
	}
	public void setObservationResult(String observationResult){
		this.observationResult = observationResult;
	}
	
	
	public List<Query> getQueries(){
		return queries;
	}
	public void add(Query query){
		queries.add(query);
	}

}

