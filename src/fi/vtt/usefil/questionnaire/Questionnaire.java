package fi.vtt.usefil.questionnaire;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/** 
 * Representation of a questionnaire in an Instant Survey.
 *  Thus this is the root object produced when parsing a questionnaire definition file.    
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

@Root
@NamespaceList({
	@Namespace(reference="http://usefil.eu/dialog#"),
	@Namespace(reference="http://usefil.eu/ontology#", prefix="usefil")
})
public class Questionnaire {

	@Attribute (required=false)
	private String id;

	@ElementList (entry = "Message",inline=true,required=false)
	private List<Message> messages;

	@ElementList(entry = "Question",inline=true,required=false)
	private List<Question> questions;

	public List<Message> getMessages(){
		return messages;
	}

	public List<Question> getQuestions(){
		return questions;
	}
	public void add(Question question){
		questions.add(question);
	}
	public void add(Message message){
		messages.add(message);
	}
}
