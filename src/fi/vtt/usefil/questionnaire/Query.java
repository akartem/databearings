package fi.vtt.usefil.questionnaire;

import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Text;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Attribute;

/** 
 * Representation of a database query in an Instant Survey  
 * 
 * Developed in and given a home after FP7 USEFIL project
 *  
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

@Element(data=true)
@Namespace(reference="http://usefil.eu/database#")
public class Query {
	
	@Attribute(required=true)
    private String type;

	@Text(data=true)
	private String text; 
	
	public String getType(){
		return type;
	}
	
	public String getText(){
		return text;
	}
}
