package fi.vtt.usefil.questionnaire;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.FormField.Option;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.DefaultParticipantStatusListener;

import java.util.Collection;

/** 
 * XMPP-based UserCommunicationInterface for Instant Survey
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class MultiUserChatSession implements UserCommunicationInterface {

	public static boolean DEBUG_PRINT = false;

	final static String DEFAULT_NICKNAME = "USEFIL surveys";

	final static String SORRY_MESSAGE = "Sorry, did not understand your answer. Try again";
	final static String REPEAT_REQUEST = "repeat!";
	//final static String SKIP_REQUEST = "skip!";
	final static String STOP_REQUEST = "stop!";
	final static String END_INFORM = "end!";

	// FORMS_IN_MESSAGE: if true, also Messages need to be answered explicitly in XMPP chat clients
	//final static boolean FORMS_IN_MESSAGES = false;

	private String user;
	private String password;
	private String nickname;
	private String room;
	private String roomFullName;
	private String domain;
	private String participantName;

	private Connection connection;
	private MultiUserChat muc;

	/**Message received, set by the listener*/
	private String lastIncomingMessage = null;

	/**Object for notifications between main thread and listeners*/
	private Object incomingMessageNotifier = new Object();

	static {
		try {
			Connection.addConnectionCreationListener(new ConnectionCreationListener() {
				public void connectionCreated(Connection connection) {
					if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Connection created");
				}
			});

		} catch (Exception ex) {
			System.out.println(MultiUserChat.class.getName()+": Problem loading reconnection manager: "+ex);
		}
	}

	/**
	 * Constructor
	 * @param user						questioner name, typically the user name, the user must be existing in the XMPP server
	 * 									specified by the domain. Example: TimoT
	 * @param password					questioner password. Example  timot1
	 * @param roomFullName						The chatroom name dedicated for synchronizing single user's different UI devices, e.g. TimoT_questionnaires 
	 * 									The roomFullName name must match an existing roomFullName name defined by the XMPP server in the domain. 
	 * 									Example: TimoT_questionnaires   	  
	 * @param domain					the server name of the XMPP server, e.g. example.com
	 */
	public MultiUserChatSession(String user, String password, String room, String domain){
		this.nickname = DEFAULT_NICKNAME;
		this.password = password;
		this.room = room;
		this.domain = domain;
		this.user = user;
	}

	@Override
	public boolean startup() {
		ConnectionConfiguration config = new ConnectionConfiguration(domain,5222);
		config.setSASLAuthenticationEnabled(false);
		config.setReconnectionAllowed(true);
		//config.setCompressionEnabled(false);
		//config.setDebuggerEnabled(true);
		connection = new XMPPConnection(config);

		try {
			connection.connect();

			connection.addConnectionListener(new ConnectionListener(){
				@Override
				public void connectionClosed() {
					if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Connection closed");
				}
				@Override
				public void connectionClosedOnError(Exception arg0) {
					if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Connection closed on error: "+arg0.getClass().getName());
				}
				@Override
				public void reconnectingIn(int arg0) {
					if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Reconnection in " +arg0+ " seconds");
				}
				@Override
				public void reconnectionFailed(Exception arg0) {
					if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Reconnection failed: "+ arg0);
				}
				@Override
				public void reconnectionSuccessful() {
					if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Reconnection successful");
					try {
						muc = new MultiUserChat(connection, roomFullName);
						DiscussionHistory history = new DiscussionHistory();
						history.setMaxStanzas(0);	  
						muc.join(nickname, "", history, 1000l);
						if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Re-joined the chat with nickname: "+nickname);
					}
					catch (Exception e){
						e.printStackTrace();
						System.out.println(MultiUserChat.class.getName()+": Error in reconnecting: "+e.getMessage());
					}
				}
			});

			connection.login(user, password, nickname);
			if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Logged in to "+domain);
		}	
		catch (XMPPException e) {
			e.printStackTrace();
			return false;
		}	

		//Find the MultiChatRoom based its provided local name 
		Collection<String> services;
		try {
			services = MultiUserChat.getServiceNames(connection);
			for(String service : services){
				Collection<HostedRoom> rooms = MultiUserChat.getHostedRooms(connection, service);
				for(HostedRoom room :rooms){
					String jid = room.getJid();
					if(jid.startsWith(this.room+"@")){
						this.roomFullName = jid;
						this.participantName = this.roomFullName+"/"+nickname;
						break;
					}
				}
			}
		} catch (XMPPException e1) {
			e1.printStackTrace();
		}

		if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Room name: "+this.roomFullName);
		muc = new MultiUserChat(connection, roomFullName);
		muc.addParticipantStatusListener(new SessionParticipantStatusListener());

		try {	
			DiscussionHistory history = new DiscussionHistory();
			history.setMaxStanzas(0);	  

			muc.addMessageListener(new SessionListener());
			muc.join(nickname, "", history, 1000l);
		}
		catch (XMPPException e) {
			e.printStackTrace();
			return false;
		}
		if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Joined with nickname: "+nickname);

		return true;
	}

	@Override
	public void shutdown(){
		muc.leave();
		connection.disconnect();
	}

	@Override
	public void sendMessage(Message m) {
		String body = m.getText().replaceAll("\\s+"," ");
		org.jivesoftware.smack.packet.Message msg = new org.jivesoftware.smack.packet.Message(roomFullName);
		msg.setType(org.jivesoftware.smack.packet.Message.Type.groupchat);

		//		if (FORMS_IN_MESSAGES){
		//			body += " answer 'ok'";
		//			Form form = buildMessageForm(m);
		//			msg.addExtension(form.getDataFormToSend());
		//		}
		msg.setBody(body);

		if(m instanceof Question){
			Form form = buildQuestionForm((Question)m);
			msg.addExtension(form.getDataFormToSend());
		}

		//if (DEBUG_PRINT) System.out.println(body);
		if (DEBUG_PRINT) System.out.println("Sending:: " + msg.toXML());

		try {
			muc.sendMessage(msg);
		}	
		catch (XMPPException e) {
			System.out.println("MultiUserChat could not send the message: "+e.getMessage());
		} 

		//		if (FORMS_IN_MESSAGES) {
		//			Object mon = new Object();
		//			while(true){
		//				synchronized(incomingMessageNotifier){
		//					try {
		//						incomingMessageNotifier.wait();
		//					} catch (InterruptedException e) {
		//						e.printStackTrace();
		//						return;
		//					}
		//				}  
		//				if (DEBUG_PRINT)  System.out.println("Answer: " + this.lastIncomingMessage);
		//				return;
		//			}
		//		}	

	}	

	@Override
	public Object askQuestion(Question question) {

		String body = question.getText().replaceAll("\\s+"," ")+"\n";
		for(Answer answer : question.getAnswers()){
			String possibleInput = "'"+answer.getValue()+"'";
			if(answer.getShortcut()!=null) possibleInput+=" or '"+answer.getShortcut()+"'";
			body += "    "+answer.getText()+" (answer "+possibleInput+")"+"\n";
		}

		//if(DEBUG_PRINT) System.out.println(body);

		org.jivesoftware.smack.packet.Message msg = new org.jivesoftware.smack.packet.Message(roomFullName);
		msg.setBody(body);
		msg.setType(org.jivesoftware.smack.packet.Message.Type.groupchat);
		Form form = buildQuestionForm(question);
		msg.addExtension(form.getDataFormToSend());

		if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": Sending:: " + msg.toXML());

		try {
			muc.sendMessage(msg);

			int timeout = -1;
			Timeout t = question.getTimeout();
			if(t!=null){
				timeout = t.getValue().intValue();
			}

			while(true){
				long now = System.currentTimeMillis();
				this.lastIncomingMessage = "";

				synchronized(incomingMessageNotifier){
					try {
						if(timeout!=-1)
							incomingMessageNotifier.wait(timeout);
						else incomingMessageNotifier.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
						return null;
					}
				}

				String input = this.lastIncomingMessage;

				if(input.equals("") && timeout!=-1 && System.currentTimeMillis() >= now+timeout-10){
					return t;
				}

				for(Answer answer : question.getAnswers()){
					if(input.equals(answer.getText())||input.equals(answer.getValue()) || input.equals(answer.getShortcut())) {
						return answer;
					}	
				}

				if(input.equals(STOP_REQUEST)) 
					return new UserRequest(UserRequest.STOP_REQUEST); 

				org.jivesoftware.smack.packet.Message msg2;
				if(input.equals(REPEAT_REQUEST)) msg2 = msg; 
				else{
					String m = SORRY_MESSAGE;
					//if(DEBUG_PRINT) System.out.println(m);
					msg2 = new org.jivesoftware.smack.packet.Message(roomFullName);
					msg2.setBody(m);
					msg2.setType(org.jivesoftware.smack.packet.Message.Type.groupchat);
					//msg2.addExtension(form.getDataFormToSend());
				}
				muc.sendMessage(msg2);
				if(DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": "+msg2.toXML());			

			}
		}	
		catch(XMPPException e){
			System.out.println(MultiUserChat.class.getName()+": Couldn't send the question: "+e.getStackTrace());
		}
		return null;
	}

	/**
	 * used only when FORMS_IN_MESSAGES = true;
	 * @param message
	 * @return
	 */
	//	private Form buildMessageForm(Message message){
	//		Form formToSend = new Form(Form.TYPE_FORM);
	//		formToSend.setTitle("Questionaire");
	//		// formToSend.setInstructions(null);
	//		FormField field = new FormField("message");
	//		String txt = message.getText();
	//		txt.replaceAll("\n", "");
	//		txt.replaceAll("\r", "");
	//		field.setLabel(txt);
	//		field.setType(FormField.TYPE_FIXED);
	//		formToSend.addField(field);
	//		/**Option option = new Option("ok","ok");
	//    	// Add a text-single variable to the form
	//    		field.addOption(option);*/  
	//		return formToSend;
	//	}

	private Form buildQuestionForm(Question question) {

		Form formToSend = new Form(Form.TYPE_FORM);
		formToSend.setTitle("USEFIL");
		// formToSend.setInstructions(null);
		FormField field = new FormField();
		// Jarkko editoi ->
		//		field.setLabel("wordsets");
		//		field.setType(FormField.TYPE_HIDDEN);
		//		field.addValue(question.getWordsets());
		//		formToSend.addField(field);
		// <--
		//if (DEBUG_PRINT) System.out.println(field.toXML());
		field = new FormField("question");

		//field.setType(FormField.TYPE_TEXT_SINGLE);
		field.setType(FormField.TYPE_LIST_SINGLE);
		field.setLabel(question.getText());

		Collection<Answer> answers = question.getAnswers();
		for (Answer a: answers){ 
			Option option = new Option(a.getText(),a.getValue());
			field.addOption(option);
			// jarkko edotoi ->
			//			field.addValue("["+a.getValue()+"]["+a.getShortcut()+"]");
		}
		// if (DEBUG_PRINT) System.out.println(field.toXML());
		formToSend.addField(field);
		return formToSend;

	}

	/**XMPP status listener*/
	public class SessionParticipantStatusListener extends DefaultParticipantStatusListener {
		@Override
		public void kicked(String participant, String actor,String reason){
			if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": kicked  out: "+ participant+" by "+actor+" for reason: "+ reason);
		}
		@Override
		public void left(String participant){
			if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": participant : "+ participant+" left");
		}
		@Override
		public void joined(String participant){
			if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": participant : "+ participant+" joined");
		}
	}

	/**XMPP packet listener*/
	public class SessionListener implements PacketListener{

		@Override
		public void processPacket(org.jivesoftware.smack.packet.Packet message) {
			if (!message.getFrom().equalsIgnoreCase(participantName)){
				lastIncomingMessage = ((org.jivesoftware.smack.packet.Message)message).getBody();
				if (DEBUG_PRINT) System.out.println(MultiUserChat.class.getName()+": "+message.getFrom()+": "+lastIncomingMessage);
				synchronized (incomingMessageNotifier){
					incomingMessageNotifier.notify();
				}	
			}   
		}
	}

	/**Methods added for S-APL platform*/

	public void addMessageListener(PacketListener listener) {
		muc.addMessageListener(listener);
	}
	public String getParticipantName(){
		return this.participantName;
	}
	public String setNickname(String nick){
		return this.nickname = nick;
	}
	public boolean isConnected(){
		return connection.isConnected();
	}
	public void endQuestionnaire(){
		Message m = new Message();
		m.setText(END_INFORM);
		this.sendMessage(m);			
	}

}