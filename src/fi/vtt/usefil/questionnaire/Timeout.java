package fi.vtt.usefil.questionnaire;

import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Attribute;

/** 
 * Representation of a question timeout in an Instant Survey
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

@Root
@NamespaceList({
	@Namespace(reference="http://usefil.eu/dialog#"),
	@Namespace(reference="http://usefil.eu/database#", prefix="db"),
	@Namespace(reference="http://usefil.eu/ontology#", prefix="usefil")
})
public class Timeout {
	
	@Attribute(required=true)
    private Integer value;

	@Attribute(required=true)
    private String transition;

	public Integer getValue(){
		return value;
	}
	
	public String getTransition(){
		return transition;
	}
}
