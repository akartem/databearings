package fi.vtt.usefil.questionnaire;

import java.util.List;
import java.util.Map;

/** 
 * Interface towards a database in an Instant Survey.
 * Database is used to tailor the messages/questions and to store the answers.
 * 
 * Developed in and given a home after FP7 USEFIL project
 *    
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.2 (25.10.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public interface DatabaseCommunicationInterface {
	public List<Map<String, String>> executeQuery(String query) throws Exception;
	
	public void addMeasurement(String property, String value) throws Exception;
	
	public String getQueryLanguageName();
}
