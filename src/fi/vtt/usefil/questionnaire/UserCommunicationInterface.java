package fi.vtt.usefil.questionnaire;

/** 
 * Interface towards a user in an Instant Survey.
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public interface UserCommunicationInterface {
	
	public boolean startup();
	
	public void endQuestionnaire();
	
	public void shutdown();
	
	public void sendMessage(Message message);
	
	public Object askQuestion(Question question);
	
}
