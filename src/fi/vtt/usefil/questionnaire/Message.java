package fi.vtt.usefil.questionnaire;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Namespace;

/** 
 * Representation of a message in an Instant Survey.
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * @author Timo Tuomisto (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

@Root
@NamespaceList({
	@Namespace(reference="http://usefil.eu/dialog#"),
	@Namespace(reference="http://usefil.eu/database#", prefix="db"),
	@Namespace(reference="http://usefil.eu/ontology#", prefix="usefil")
})
public class Message {

	@Attribute(required=false)
	protected String state;

	@Attribute(required=false)
	protected String transition;

	@Attribute(required=false)
	protected Integer pause;

	@ElementList(entry = "Query",inline=true,required=false)
	@Namespace(reference="http://usefil.eu/database#")
	protected List<Query> queries;

	@Element (name= "Text", required=false)
	@Namespace(reference="http://usefil.eu/dialog#")
	protected String text;
	
	protected String originalText;

	public Message (){
	}
	
	public Message (Message toCopy){
		this.transition = toCopy.transition;
		this.state = toCopy.state;
		this.pause = toCopy.pause;
		this.text = toCopy.text;
		this.originalText = toCopy.originalText;
	}
	
	public List<Query> getQueries(){
		return queries;
	}
	public void add(Query query){
		queries.add(query);
	}
	
	
	public String getText(){
		return text;
	}
	public void setText(String t){
		text = t;
	}
	
	public Integer getPause(){
		return pause;
	}
	
	public String getState(){
		return state;
	}
	
	public String getTransition(){
		return transition;
	}
	
	public void setOriginalText(String text){
		originalText = text;
	}
	public String getOriginalText(){
		return originalText;
	}
}
