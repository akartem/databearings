package fi.vtt.usefil.questionnaire;

/** 
 * Class to convey (from a UserCommunicationInterface to the Instant Survey engine)
 *  special user requests such as to stop a questionnaire
 * 
 * Developed in and given a home after FP7 USEFIL project
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.2 (11.03.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class UserRequest {
	public static final int STOP_REQUEST = 1;
	public static final int SKIP_REQUEST = 2;
	
	private int value;

	public UserRequest (int value){
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
}
