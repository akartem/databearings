package sapl.shared.eii;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.rab.RABUtils;
import sapl.core.sapln3.SaplN3Parser;

/** 
 * Ontonut representing a Microsoft Excel document (supports both .xls and .xslx).
 * 
 * @author Artem Katasonov (VTT + Elisa from July 2018) 
 * 
 * @version 5.2 (08.04.2020)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019-2020, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */ 

public class ExcelOntonut extends HttpOntonut {

	protected List<String> lastLabels = null;
	
	protected Set<String> tableDelimiter = null;
	
	protected boolean transpose = false;
	
	final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.US);
	
	protected Set<Integer> columnReferences = new HashSet<Integer>(); 
	protected Set<Integer> cellsReferences = new HashSet<Integer>();
	protected boolean hasFillReferences = false;
	
	protected Map<String,String> columnsWithRepeat = new HashMap<String,String>();
	
	protected int rowCount;
	protected int columnCount;
	
	protected static Set<String> openFiles = new HashSet<String>();
	
	
	@Override
	public boolean loadDefinition() {
		if(!super.loadDefinition())
			return false;

		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "{<"+this.id+"> o:tableDelimiter ?tableD} s:is s:Optional. {<"+this.id+"> o:transpose ?transpose} s:is s:Optional. "; 
		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);

		if(resultSet!=null && resultSet.getNumberOfSolutions()>0){
		
			String trans = resultSet.getSolution(0).get("transpose");
			if(trans != null && trans.equals("true"))
				this.transpose = true;
			
			for (int i=0; i<resultSet.getNumberOfSolutions(); i++) {
				Map<String, String> def = resultSet.getSolution(i);
				String tableD = def.get("tableD");
				if(tableD!=null) {
					if(this.tableDelimiter==null)
						this.tableDelimiter = new HashSet<String>();
					this.tableDelimiter.add(tableD.toLowerCase());
				}
			}
		}

		return true;		
	}

	
	@Override
	public void findDataItems(String contextID, Map<String, String> result) {
		findDataItemsRecursion(contextID, result);
		super.findDataItems(contextID, result);
	}


	public void findDataItemsRecursion(String contextID, Map<String, String> result) {
		Map<String, String> labels = new HashMap<String, String>();
		Map<String, Integer> cellOrder = new HashMap<String, Integer>();
		List<SemanticStatement> statements = this.parent.getStatements(contextID);
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"cell")){
				Integer order = Integer.valueOf(st.object);
				cellOrder.put(st.subject, order);
				cellsReferences.add(order);
			}
		}
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"column")){
				if(st.isObjectVar()){
					Integer order = cellOrder.get(st.subject);
					if(order!=null){
						result.put(st.object, "CELL#"+String.valueOf(order)+"/@"); //column label
					}
					else if(st.subject.startsWith(SaplConstants.DATA_NS)){
						int index = Integer.valueOf(st.subject.substring(SaplConstants.DATA_NS.length()));
						result.put(st.object, "COLUMN#"+String.valueOf(index)+"/@"); //column label
						columnReferences.add(index);
					}
					else if(st.isSubjectAny()) {
						result.put(st.object, "COLUMN#"+SaplConstants.ANYTHING+"/@");
						columnReferences.add(-1);
						
						if(st.isSubjectVar())
							result.put(st.subject, "COLUMN#"+SaplConstants.ANYTHING+"/#");
					}
						
				}
				else {
					labels.put(st.subject, st.object);
					if(st.isSubjectVar())
						result.put(st.subject, st.object+"/#");
				}
			}
			else if(st.isObjectContext()){
				this.findDataItemsRecursion(st.object, result);
			}
		}
		for (SemanticStatement st: statements){
			if((st.predicate.equals(SaplConstants.DATA_NS+"value") || st.predicate.equals(SaplConstants.DATA_NS+"fill")) && st.isObjectVar()){
				String prefix = "";
				if(st.predicate.equals(SaplConstants.DATA_NS+"fill")){
					prefix = "FILL#"; 
					hasFillReferences = true;
				}
				String id = labels.get(st.subject);
				if(id != null){
					result.put(st.object,prefix+id);
				}
				else{ 
					Integer order = cellOrder.get(st.subject);
					if(order!=null){
						result.put(st.object, prefix+"CELL#"+String.valueOf(order)+"/!"); //nth cell value
					}
					else if(st.subject.startsWith(SaplConstants.DATA_NS)){
						int index = Integer.valueOf(st.subject.substring(SaplConstants.DATA_NS.length()));
						result.put(st.object, prefix+"COLUMN#"+String.valueOf(index)+"/!"); //column value
						columnReferences.add(index);
					}
					else if(st.isSubjectAny()) {
						result.put(st.object, prefix+"COLUMN#"+SaplConstants.ANYTHING+"/!");
						columnReferences.add(-1);
					}					
				}
			}
			else if(st.predicate.equals(SaplConstants.DATA_NS+"repeat")){
				String id = labels.get(st.subject);
				if(id!=null){
					columnsWithRepeat.put(id.toUpperCase(),"");
				}
				else{ 
					if(st.subject.startsWith(SaplConstants.DATA_NS)){
						int index = Integer.valueOf(st.subject.substring(SaplConstants.DATA_NS.length()));
						columnsWithRepeat.put("COLUMN#"+String.valueOf(index),"");
					}
				}				
			}			
		}

	}
	
	private Integer getLastColumn(Sheet sheet) {
	    int lastColumn = 0;
	    for (Row row : sheet) {
	        if (lastColumn < row.getLastCellNum()) {
	            lastColumn = row.getLastCellNum();
	        }
	    }
	    return lastColumn;
	}

	private Cell getCell(Sheet sheet, int rowIndex, int columnIndex){
		if(!this.transpose){
			Row row = sheet.getRow(rowIndex);
			if(row==null) return null;
			else return row.getCell(columnIndex);
		}
		else {
			Row row = sheet.getRow(columnIndex);
			if(row==null) return null;
			else return row.getCell(rowIndex);
		}
	}
	
	@Override
	protected List<Map<String, String>> runSingleQuery(String request, String body, String reqUsername, String reqPassword, int subindex, Map<String, String> localSol) throws Exception {
		
		long s = System.currentTimeMillis();

		String sheetName = null;
		String fileName = null;
		if(request.contains(".xlsx#") || request.contains(".xls#")) {
			int index = request.lastIndexOf("#");
			sheetName = request.substring(index+1);
			fileName = request.substring(0, index);
		}
		else fileName = request;
		
		synchronized (openFiles) {
			while(openFiles.contains(fileName)) {
				openFiles.wait();
			}
			openFiles.add(fileName);
		}
		
		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			this.print("Accessing "+request, subindex);
		
		List<Map<String,String>> sols  = new ArrayList<Map<String,String>>();
		
		try {
			URL url = null;
			try {
				url = new URL(fileName);
			} catch (MalformedURLException e) {}
			

			Workbook workbook;
			if(url!=null){
				workbook = WorkbookFactory.create(url.openStream());
			}
			else{
				fileName = URLDecoder.decode(fileName, "UTF-8");
				workbook = WorkbookFactory.create(new File(fileName));
			}
	
			Sheet sheet;
			if(sheetName != null){
				sheet = workbook.getSheet(sheetName);
				if(sheet==null) {
					throw new Exception("Sheet "+sheetName+" does not exist in "+this.uri);
				}
			}
			else sheet = workbook.getSheetAt(0);
	
			if(!this.transpose) {
				rowCount = sheet.getLastRowNum()+1;
				columnCount = getLastColumn(sheet)+1;
			}
			else {
				columnCount = sheet.getLastRowNum()+1;
				rowCount = getLastColumn(sheet)+1;
			}
			
			String labels[] = null;
			String labelsUpper[] = null;
			
			Map<String,String> generalValues = new LinkedHashMap<String,String>();
	
			for (int iRow = 0; iRow <= rowCount; iRow++) {
					if(this.tableDelimiter==null && iRow==0 
							|| this.tableDelimiter!=null && startsNewTable(sheet, iRow)){ // a labels row
						
						labels = new String[columnCount];
						labelsUpper = new String[columnCount];
						
						this.lastLabels = new ArrayList<String>();
						
						for (int iCol = 0; iCol < columnCount; iCol++) {
							Cell cell = getCell(sheet, iRow, iCol);
							if(cell!=null){
								String title = this.getValue(cell);
								labels[iCol] = title;
								labelsUpper[iCol] = RABUtils.escapeString(title.toUpperCase().replace("\\n", " ").replaceAll("\\s+", " "));
								lastLabels.add(title);
							}
						}
						
					}
					else { //other rows
						Map<String,String> keyValue = getKeyValuePair(sheet, iRow);
						
						if(keyValue!=null){ // the row has just 2 cells set
							String key = keyValue.keySet().iterator().next();
							String keyUpper = key.toUpperCase();
							
							String var = this.dataItemsToSelect.get(keyUpper);
							if(var!=null){
								var = var.substring(SaplConstants.VARIABLE_PREFIX.length());
							
								if(labels==null || 
										//if already have labels, only if redefining something met before labels
										generalValues.containsKey(var)){
									generalValues.put(var, keyValue.get(key));
									continue;
								}
							}
						}
						
						if (labels!=null) { // normal row after a labels row
							Map<String,String> sol = new HashMap<String,String>();
							
							String lastLabel = "";
							
							int cellsFound = 0;
							
							List<Map<String,String>> solsViaAny = null;
							if(columnReferences.contains(-1))
								solsViaAny = new ArrayList<Map<String,String>>();
							
							for (int iCol = 0; iCol < columnCount; iCol++) {
								
								Cell cell = this.getCell(sheet, iRow, iCol);
								
								String value = this.getValue(cell);
								if(value != null && value.isEmpty()) value = null;
								
								String label = null;
								String labelUpper = null;
								if (iCol < labels.length && labels[iCol] != null) {
									label = labels[iCol];
									labelUpper = labelsUpper[iCol];
								}
								else {
									label = lastLabel+"_next";
									labelUpper = lastLabel.toUpperCase()+"_next";
								}
								
								lastLabel = label;
								
								if(!columnsWithRepeat.isEmpty()) {
									
									String colNum = "COLUMN#"+String.valueOf(iCol+1);
									
									if (value == null) {
										String prev = columnsWithRepeat.get(labelUpper);
										if(prev!=null) value = prev;
										else {
											prev = columnsWithRepeat.get(colNum);
											if(prev!=null) value = prev;
										}
									}
									else {
										if(columnsWithRepeat.containsKey(label))
											columnsWithRepeat.put(label, value);
										if(columnsWithRepeat.containsKey(colNum))
											columnsWithRepeat.put(colNum, value);								
									}
								}
	
								
								String fill = hasFillReferences? getFill(cell) : null;
								
								String tvar = this.dataItemsToSelect.get(labelUpper+"/#");
								if(tvar != null) {
									sol.put(tvar.substring(SaplConstants.VARIABLE_PREFIX.length()), String.valueOf(iCol+1));
								}

								if (value != null || fill != null){
									
									if(value != null) cellsFound++;
									
									String var;
	
									boolean added = false;
									
									if(columnReferences.contains(iCol+1)){
										var = this.dataItemsToSelect.get("COLUMN#"+(iCol+1)+"/@");
										if(var!=null){
											sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), label);
											added = true;
										}
									}
									
									if(cellsReferences.contains(cellsFound)){
										var = this.dataItemsToSelect.get("CELL#"+cellsFound+"/@");
										if(var!=null){
											sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), label);
											added = true;
										}
									}
									
									if(fill != null) {
										var = this.dataItemsToSelect.get("FILL#"+labelUpper);  
										if(var!=null)
											sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), fill);
									}
									
									var = this.dataItemsToSelect.get(labelUpper);
									if(var != null && value != null) {
										String key = var.substring(SaplConstants.VARIABLE_PREFIX.length());
										if(!sol.containsKey(key)){
											sol.put(key, value);
											added = true;
										}
									}
									else{
										if(columnReferences.contains(iCol+1)){
											if(value != null) {
												var = this.dataItemsToSelect.get("COLUMN#"+(iCol+1)+"/!");
												if(var != null){
													sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
													added = true;
												}
											}
											if(fill != null) {
												var = this.dataItemsToSelect.get("FILL#COLUMN#"+(iCol+1)+"/!");  
												if(var != null)
													sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), fill);
											}
										}
										if(cellsReferences.contains(cellsFound)){
											if(value != null) {
												var = this.dataItemsToSelect.get("CELL#"+cellsFound+"/!");
												if(var!=null){
													sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
													added = true;
												}
											}
											if(fill != null) {
												var = this.dataItemsToSelect.get("FILL#CELL#"+cellsFound+"/!");  
												if(var!=null)
													sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), fill);
											}
										}
									}
									
									if(!added) {
										if(columnReferences.contains(-1)) {
											Map<String,String> newsol = new HashMap<String,String>();
											
											var = this.dataItemsToSelect.get("COLUMN#"+SaplConstants.ANYTHING+"/@");
											if(var!=null){
												newsol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), label);
												
												String var2 = this.dataItemsToSelect.get("COLUMN#"+SaplConstants.ANYTHING+"/#");
												if(var2!=null){
													newsol.put(var2.substring(SaplConstants.VARIABLE_PREFIX.length()), String.valueOf(iCol+1));
												}
											}	
											if(value != null) {
												var = this.dataItemsToSelect.get("COLUMN#"+SaplConstants.ANYTHING+"/!");
												if(var!=null)
													newsol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
											}			
											if(fill != null) {
												var = this.dataItemsToSelect.get("FILL#COLUMN#"+SaplConstants.ANYTHING+"/!");
												if(var!=null)
													newsol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), fill);
											}
											if(!newsol.isEmpty())
												solsViaAny.add(newsol);
										}
									}
									
								}
							}
							
							if(solsViaAny!=null && !solsViaAny.isEmpty()) {
								for(Map<String,String> solViaAny : solsViaAny) {
									solViaAny.putAll(sol);
									if(!generalValues.isEmpty())
										sol.putAll(generalValues);
									sols.add(solViaAny);								
								}
							}
							else {
								if(sol.size()>0){
									if(!generalValues.isEmpty())
										sol.putAll(generalValues);
									sols.add(sol);
								}
							}
						}
					}
					
			}
			
			workbook.close();
		}
		finally {
			synchronized (openFiles) {
				openFiles.remove(fileName);
				openFiles.notifyAll();
			}
		}

		dataItemsToSelect.remove("");
		if(this.dataItemsToSelect.isEmpty()) return null;

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			this.print("Processed "+sols.size()+" results ["+(System.currentTimeMillis()-s)+"]", subindex);

		if(sols.size()>0) return sols;
		else return null;

	}	

	protected String getValue(Cell cell) {
		if (cell == null) {
			return null;
		}
		CellType type = cell.getCellTypeEnum();

		String value;
		if ((type == CellType.NUMERIC) || (type == CellType.FORMULA)) {

			try {
				if (DateUtil.isCellDateFormatted(cell)) {
						Date d = cell.getDateCellValue();
						value = sdf.format(d);
				}
				else {
						Double v = cell.getNumericCellValue();
						double d = Math.floor(v);
						if (d == v) {
							value = String.valueOf((long) d);
						} else {
							value = v.toString();
						}
				}
			}
			catch(Exception e){
				try{
					value = cell.getStringCellValue();
				}
				catch(Exception e2){
					return null;
				}
			}
				
		} else {
			value = cell.getStringCellValue();
		}
		
		return value.trim();
	}
	
	protected String getFill(Cell cell) {
		if (cell == null) {
			return null;
		}
		XSSFCellStyle cellStyle = (XSSFCellStyle)cell.getCellStyle();          
        XSSFColor color = (XSSFColor)cellStyle.getFillForegroundColorColor();
		if(color != null)	
			return color.getARGBHex();
		else return null;
	}
	
	protected boolean startsNewTable (Sheet sheet, int rowIndex){
		if(this.tableDelimiter==null) return false;
		else {
			for (int iCol = 0; iCol < this.columnCount; iCol++) {
				Cell cell = getCell(sheet, rowIndex, iCol);
				if(cell!=null){
					CellType type = cell.getCellTypeEnum();
					if ( type != CellType.NUMERIC && type != CellType.FORMULA &&
							this.tableDelimiter.contains(cell.getStringCellValue().toLowerCase()))
						return true;
				}
			}
			return false;
		}
	}
	
	protected Map<String,String> getKeyValuePair (Sheet sheet, int rowIndex){
		
		String key = null, value = null;
		for (int iCol = 0; iCol < this.columnCount; iCol++) {
			Cell cell = getCell(sheet, rowIndex, iCol);
			if(cell!=null){
				String v = this.getValue(cell);
				if(v!=null && !v.isEmpty()) {
					if(key==null) key = this.getValue(cell);
					else if(value==null) value = this.getValue(cell);
					else return null;
				}
			}
		}
		if(key==null || value==null) return null;
		else{
			Map<String,String> result = new HashMap<String,String>();
			key = key.trim();
			if(key.endsWith(":")) key = key.substring(0, key.indexOf(":")).trim();
			result.put(key, value);
			return result;
		}
	}
	

	@Override
	public List<Map<String, String>> processResponse(Object content) throws Exception {
		return null; //not used as the files are binary
	}

	//Support of IDE 

	@Override
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables) throws Throwable{
		getDoc();
		
		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		ns += "#";
		
		String result = "";
		result += "* d:table {\n";
		result += "\t* d:row {\n";
		for(String title : lastLabels){
			String varName = title.toLowerCase();
			result += "\t\t[d:column \""+title+"\"] d:value "+SaplConstants.VARIABLE_PREFIX+varName+" .\n";
			
			if(fillVariables!=null) fillVariables.put(ns+title, varName);			
		}
		result += "\t} .\n";
		result += "} .";
		return result;
	}	

	@Override
	public String generateOntologyDefinition() throws Throwable{
		getDoc();

		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		String className = ns.substring(ns.lastIndexOf("/")+1);
		ns += "#";

		String result = "ds:"+className+" a rdfs:Class .\n";

		for(String title : lastLabels){
			result += "ds:"+title+" a rdf:Property; rdfs:domain ds:"+className+" .\n";
		}
		
		result = SaplN3Parser.PREFIX_KEYWORD+" rdf: <"+SaplConstants.RDF_NS+"> . "+
				SaplN3Parser.PREFIX_KEYWORD+" rdfs: <"+SaplConstants.RDFS_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" owl: <"+SaplConstants.OWL_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" ds: <"+ns+"> . \n"+
				"ds:ontology a owl:Ontology . \n" +
				result;
		return result;

	}	

	@Override
	protected void getDoc() throws Throwable{
		if(this.lastLabels==null){
			this.addVarToItemsToSelect("", "");
			executeQuery(new SaplQueryResultSet(),null);
		}
	}


}
