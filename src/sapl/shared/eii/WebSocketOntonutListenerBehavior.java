package sapl.shared.eii;

import java.io.IOException;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.HttpTools;
import sapl.util.IDGenerator;
import sapl.util.URITools;

import javax.websocket.ClientEndpoint;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.client.ClientProperties;

/** 
 * Experimental:
 * Opens a WebSockets connection, receives messages, processes them via an Ontonut,
 * and publishes the results (e.g. events formatted according to a defined schema) as S-APL local data. 
 * 
 * @author Artem Katasonov (VTT + Elisa since July 2018) 
 * 
 * @version 4.7 (30.04.2019)
 * 
 * @since 4.5
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2017-2018, VTT
 * 
 * */ 

@ClientEndpoint
public class WebSocketOntonutListenerBehavior extends OntonutListenerBehavior{

	@Parameter (name="proxy") private String proxy;
	
	private String url;
	
	private Session session;
	
	IDGenerator eventIDGenerator;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		super.initializeBehavior(parameters);
		
		this.proxy = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "proxy"));

		this.finished = false;
		eventIDGenerator = new IDGenerator("WebSocket"); 
	}

	@Override
	protected void doAction() throws Exception {
		
		super.doAction();
		
		if(this.ontonut instanceof HttpOntonut)
			this.url = ((HttpOntonut)this.ontonut).requestContent;
		else{
			this.setFailed();
			throw new Error ("Non-HTTP ontonut type provided");
		}

		if(this.url==null){
			this.setFailed();
			throw new Error ("No service URI provided");
		}
		
		try {
			final URI uri = new URI(this.url);
			
			ClientManager client = ClientManager.createClient();
			if(this.proxy != null) {
				print("Using proxy "+ this.proxy);
				client.getProperties().put(ClientProperties.PROXY_URI, this.proxy);
			}

			//Credentials credentials = new Credentials("", "");
			//client.getProperties().put(ClientProperties.CREDENTIALS, credentials);

			ClientEndpointConfig cec = ClientEndpointConfig.Builder.create()
					.configurator(new ClientEndpointConfig.Configurator() {
						@Override
						public void beforeRequest(Map<String, List<String>> headers) {
							super.beforeRequest(headers);
							List<String> cookieList = headers.get("Cookie");
							if (null == cookieList) {
								cookieList = new ArrayList<String>();
							}
							try {
								List<HttpCookie> cookies = HttpTools.getCookies(new URI("http://"+uri.getHost()+":"+uri.getPort()));
								for(HttpCookie cookie : cookies){
									cookieList.add(cookie.toString());
								}
							} catch (URISyntaxException e) {
								e.printStackTrace();
							}
							headers.put("Cookie", cookieList);
						}
					}).build();

			session = client.connectToServer(new WebSocketEndpoint(), cec, uri);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	@Override
	protected void stop(){
		try {
			session.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setFinished();
	}
	
	protected void handleMessage(String message){
		//if(DEBUG_PRINT)
			print("Web Socket message: "+message);
		
		try{
			List<Map<String,String>> solutions = processMessage(message);
			
			if(solutions!=null && !solutions.isEmpty()){
				applyFilters(solutions);
				applyPrecondition(solutions);
				addConclusions(solutions);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	protected List<Map<String,String>> processMessage(String message) throws Exception{
		 return ((HttpOntonut)ontonut).processResponse(message);
	}
	
	protected void applyFilters(List<Map<String,String>> solutions){
		if(!solutions.isEmpty() && !ontonut.filters.isEmpty()){
			Set<String> definedVars = new HashSet<String>(solutions.get(0).keySet());
			SaplQueryResultSet rs = new SaplQueryResultSet(solutions, definedVars);
			boolean r = hasBeliefs(new ArrayList<String>(ontonut.filters.keySet()), SaplConstants.G, rs);
			if(r){
				solutions.clear();
				for(int i=0; i<rs.getNumberOfSolutions(); i++)
					solutions.add(rs.getSolution(i));
			}
			else solutions.clear();
		}
	}
	
	protected void applyPrecondition(List<Map<String,String>> solutions){
		if(!solutions.isEmpty() && ontonut.preconditionQuery!=null){
			Set<String> definedVars = new HashSet<String>(solutions.get(0).keySet());
			SaplQueryResultSet rs = new SaplQueryResultSet(solutions, definedVars);
			boolean r = this.hasBeliefs(ontonut.preconditionQuery, SaplConstants.G, rs);
			if(r){
				ArrayList<String> all = new ArrayList<String>();
				all.add(SaplConstants.ANYTHING);
				solutions.clear();
				solutions.addAll(rs.getSolutions(all));
			}
			else{
				printError("Could not finalize: "+solutions);
				solutions.clear();
			}
		}
	}
	
	protected void addConclusions(List<Map<String,String>> solutions){
		if(!solutions.isEmpty()){
			for(Map<String,String> vars : solutions){
				Resource eventID = eventIDGenerator.getNewResource();
				vars.put("this",eventID.toString());
				addBeliefs(ontonutSemantics, vars);
			}
			wakeReasoner();
		}
	}

	private class WebSocketEndpoint extends Endpoint{

		@Override
		public void onOpen(Session s, EndpointConfig config) {
			if(INTERACTION_PRINT)
				print("[WebSocketOntonutListenerBehavior] Web Socket to "+url+" is open");
			
			session = s;
			session.addMessageHandler(new MessageHandler.Whole<String>() {
				@Override
				public void onMessage(String message) {
					handleMessage(message);
				}
			});
			
			//session.getAsyncRemote().sendText("Hello World!");
		}
	}
	
}
