package sapl.shared.eii;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.ccil.cowan.tagsoup.jaxp.SAXParserImpl;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import sapl.core.SaplConstants;
import sapl.core.SemanticStatement;

/**
 * Experimental: 
 * Ontonut representing a semi-structured XHTML Web page.
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (20.01.2016)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2016, VTT
 * 
 * */

public class XhtmlOntonut extends HttpOntonut{

	protected Map<String,String[]> dataItemsToSelectParts = new HashMap<String,String[]>();
	
	Stack<List<Map<String,String>>> solutionsStack = new Stack<List<Map<String,String>>>();
	
	@Override
	public void findDataItems(String contextID, Map<String, String> result) {
		this.findDataItemsRecursion(contextID, result, "");
		super.findDataItems(contextID, result);
	}

	public void findDataItemsRecursion(String contextID, Map<String, String> result, String currPath) {

		List<SemanticStatement> statements = this.parent.getStatements(contextID);

		Map<String, String> ids = new HashMap<String, String>();
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"element"))
				ids.put(st.subject, st.object);
			else if(st.predicate.equals(SaplConstants.DATA_NS+"class"))
				ids.put(st.subject, "!"+st.object);
		}

		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
				String newPath = currPath;
				if(ids.containsKey(st.subject)){
					newPath = currPath+"/"+ids.get(st.subject);
				}
				result.put(st.object, newPath);

			}

			if(st.isObjectContext()){
				String newPath = currPath;
				if(st.predicate.equals(SaplConstants.DATA_NS+"branch")){
					newPath += (newPath.isEmpty()?"":"/") + ids.get(st.subject) ;
				}

				this.findDataItemsRecursion(st.object, result, newPath);
			}
		}
	}

	@Override
	public List<Map<String,String>> processResponse(Object content) throws Exception{
		
		for(String item : dataItemsToSelect.keySet()){
			String[] parts = item.split("/");
			dataItemsToSelectParts.put(item, parts);
		}
		
		solutionsStack.push(null);
		
		InputStream is = new ByteArrayInputStream(((String)content).getBytes("UTF-8"));

		SAXParserImpl.newInstance(null).parse( is, new Handler() );

		//System.out.println(solutionsStack);
		
		return solutionsStack.pop();
	}

	private class Handler extends DefaultHandler {
		
		Stack<String> stack =  new Stack<String>();
	
		StringBuilder literal = new StringBuilder();
		
		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
			String currPath;
			if(stack.isEmpty()) currPath = ""; 
			else currPath = stack.peek();
			
			boolean hasClass = false;
			for (int i = 0; i < atts.getLength(); i++) {
				if(atts.getLocalName(i).equals("class")){
					currPath += "/!"+ atts.getValue(i).toUpperCase();
					hasClass = true;
					break;
				}
			}			
			if(!hasClass) currPath += "/"+localName.toUpperCase();
			stack.push(currPath);
			
			if(hasClass) literal.setLength(0);
			
			solutionsStack.push(null);
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			String currPath = stack.pop();

			boolean match = false;

			for(String item : dataItemsToSelect.keySet()){
				String parts[] = dataItemsToSelectParts.get(item);
				if(currPath.endsWith("/"+parts[parts.length-1])){

					match = true;

					if(parts.length>1){
						int ind = 0;
						for(int i=0; i<parts.length-1; i++){
							ind = currPath.indexOf("/"+parts[i], ind);
							if(ind==-1){
								match = false;
								break;
							}
							else ind++;
						}
					}

					if(match){
						List<Map<String,String>> solutions = solutionsStack.peek();
						if(solutions==null){
							solutions = new ArrayList<Map<String,String>>();
							solutionsStack.pop();
							solutionsStack.push(solutions);
						}

						String var = dataItemsToSelect.get(item);
						Map<String,String> sol = new HashMap<String,String>();
						sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), literal.toString().trim());

						solutions.add(sol);
					}
				}
			}
			
			if(match || !localName.equals("span"))
				literal.setLength(0);
			
			List<Map<String,String>> solutions = solutionsStack.pop();
			if(solutions!=null){

				List<Map<String,String>> upperSolutions = solutionsStack.peek();
				if(upperSolutions==null) {
					solutionsStack.pop();
					solutionsStack.push(solutions);
				}
				else{
					boolean join = true;
					Set<String> allVars = new HashSet<String>(); 
					for(Map<String,String> upperSolution : upperSolutions){
						allVars.addAll(upperSolution.keySet());
					}
					for(Map<String,String> sol : solutions){
						for(String var : sol.keySet()){
							if(allVars.contains(var)){
								join = false;
								break;
							}
						}
						if(!join) break;
					}
					
					if(join){
						List<Map<String,String>> newUpperSolutions = new ArrayList<Map<String,String>>();
						
						for(Map<String,String> upperSolution : upperSolutions){
							for(Map<String,String> sol : solutions){
								Map<String,String> toAdd = new HashMap<String,String> (upperSolution);
								toAdd.putAll(sol);
								newUpperSolutions.add(toAdd);
							}
						}
						solutionsStack.pop();
						solutionsStack.push(newUpperSolutions);
					}
					else{
						upperSolutions.addAll(solutions);
					}
				}
			}
			
		}
		
		@Override
		public void characters(char[] ch, int start, int length) {
			literal.append(ch, start, length);
		}
	}
	
}
