package sapl.shared.eii;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import sapl.core.SaplConstants;

/**
 * Common constants for DataBearings EII framework
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.3 (06.10.2015)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015, VTT
 * 
 * */

public class EiiConstants {

	public static final String Ontonut = SaplConstants.ONTONUT_NS + "Ontonut";

	public static final String disabled = SaplConstants.ONTONUT_NS + "disabled";

	public static final String type = SaplConstants.ONTONUT_NS + "type";
	public static final String service = SaplConstants.ONTONUT_NS + "service";
	public static final String uri = SaplConstants.ONTONUT_NS + "uri";
	public static final String body = SaplConstants.ONTONUT_NS + "body";

	public static final String data = SaplConstants.ONTONUT_NS + "data";

	public static final String username = SaplConstants.ONTONUT_NS + "username";
	public static final String password = SaplConstants.ONTONUT_NS + "password";
	public static final String authMethod = SaplConstants.ONTONUT_NS + "authMethod";
	public static final String authUri = SaplConstants.ONTONUT_NS + "authUri";
	
	public static final String semantics = SaplConstants.ONTONUT_NS + "semantics";
	public static final String input = SaplConstants.ONTONUT_NS + "input";
	public static final String precondition = SaplConstants.ONTONUT_NS + "precondition";
	public static final String postcondition = SaplConstants.ONTONUT_NS + "postcondition";

	public static final String getPattern = SaplConstants.ONTONUT_NS + "getPattern";
	public static final String setPattern = SaplConstants.ONTONUT_NS + "setPattern";
	public static final String provider = SaplConstants.ONTONUT_NS + "provider";
	
	public static final String log = SaplConstants.ONTONUT_NS + "log";
	
	public static final String timeout = SaplConstants.ONTONUT_NS + "timeout";
	public static final String encoding = SaplConstants.ONTONUT_NS + "encoding";
	public static final String method = SaplConstants.ONTONUT_NS + "method";

	public static final String passwordDigest = SaplConstants.ONTONUT_NS + "passwordDigest"; //only SOAP
	public static final String version = SaplConstants.ONTONUT_NS + "version"; //only SOAP
	public static final String action = SaplConstants.ONTONUT_NS + "action"; //only SOAP

	public static final String rowDelimiter = SaplConstants.ONTONUT_NS + "rowDelimiter"; //only CSV
	public static final String columnDelimiter = SaplConstants.ONTONUT_NS + "columnDelimiter"; //only CSV
	public static final String hasHeader = SaplConstants.ONTONUT_NS + "hasHeader"; //only CSV

	public static final String driver = SaplConstants.ONTONUT_NS + "driver"; //only SQL
	public static final String table = SaplConstants.ONTONUT_NS + "table"; //only SQL
	
	public final static Set<String> concepts = EiiConstants.create_concepts();
	private static Set<String> create_concepts() {
		Set<String> words = new HashSet<String>();
		words.add(EiiConstants.Ontonut);
		
		words.add(EiiConstants.disabled);
		words.add(EiiConstants.type);
		words.add(EiiConstants.service);
		words.add(EiiConstants.uri);
		words.add(EiiConstants.body);
		words.add(EiiConstants.username);
		words.add(EiiConstants.password);
		words.add(EiiConstants.authMethod);
		words.add(EiiConstants.authUri);

		words.add(EiiConstants.data);

		words.add(EiiConstants.semantics);
		words.add(EiiConstants.input);
		words.add(EiiConstants.precondition);
		words.add(EiiConstants.postcondition);
		words.add(EiiConstants.getPattern);
		words.add(EiiConstants.setPattern);
		words.add(EiiConstants.provider);
		words.add(EiiConstants.log);

		words.add(EiiConstants.timeout);
		words.add(EiiConstants.encoding);
		words.add(EiiConstants.method);

		words.add(EiiConstants.passwordDigest);
		words.add(EiiConstants.version);
		words.add(EiiConstants.action);
		words.add(EiiConstants.rowDelimiter);
		words.add(EiiConstants.columnDelimiter);
		words.add(EiiConstants.hasHeader);
		words.add(EiiConstants.driver);
		words.add(EiiConstants.table);
		
		return Collections.unmodifiableSet(words);
	}
	
}
