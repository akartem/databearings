package sapl.shared.eii;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.SaplN3Producer;
import sapl.util.WhiteSpaceString;

/** 
 * Ontonut representing a REST Web Service with XML content
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (05.07.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */
 
public class XmlOntonut extends HttpOntonut {

	protected Set<String> paths = null;

	Document doc = null;

	Map<String, Boolean> canBeMultiple = new HashMap<String, Boolean>();

	@Override
	public boolean loadDefinition() {
		boolean initResult = super.loadDefinition();
		if(this.contentType==null) this.contentType = "application/xml";
		return initResult;
	}

	@Override
	/**Result will map variables used in the semantics definition to explicit Xpath of XML elements*/
	public void findDataItems(String contextID, Map<String, String> result) {
		this.findDataItemsRecursion(contextID, result, "");
		super.findDataItems(contextID, result);
	}

	public void findDataItemsRecursion(String contextID, Map<String, String> result, String currPath) {

		List<SemanticStatement> statements = this.parent.getStatements(contextID);

		Map<String, String> ids = new HashMap<String, String>();
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"element"))
				ids.put(st.subject, st.object);
			else if(st.predicate.equals(SaplConstants.DATA_NS+"attribute"))
				ids.put(st.subject, "@"+st.object);
		}

		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"value") /*&& st.isObjectVar()*/){
				String newPath = currPath;
				if(ids.containsKey(st.subject)){
					newPath = currPath+"/"+ids.get(st.subject);
				}
				result.put(st.object, newPath);

			}

			if(st.isObjectContext()){
				String newPath = currPath;
				if(st.predicate.equals(SaplConstants.DATA_NS+"branch")/* && st.isSubjectContext()*/){
					newPath += (newPath.isEmpty()?"":"/") + ids.get(st.subject) ;
				}

				canBeMultiple.put(newPath, st.isSubjectAny() || st.isSubjectBlankNode());

				this.findDataItemsRecursion(st.object, result, newPath);
			}
		}
	}

	protected void findActivePaths(){
		this.paths = new HashSet<String>();
		for(String item : this.dataItemsToSelect.keySet()){
			this.paths.add(item);
			int ind = -1;
			while((ind = item.indexOf("/", ind+1)) != -1){
				this.paths.add(item.substring(0,ind));
			}
		}		
	}

	@Override
	public List<Map<String,String>> processResponse(Object content) throws Exception{
		Node doc = null;
		if(content instanceof Node){ 
			doc = (Node)content;
		}
		else{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(new InputSource(new StringReader(content.toString())));
		}

		//SaplAgent.printMemory("After XML parse", false);

		List<Map<String,String>> result = this.processResponseNode(doc,"");

		//SaplAgent.printMemory("After XML processing", false);

		return result;
	}

	public List<Map<String,String>> processResponseNode(Node node, String currPath){

		if(node==null || node.getNodeType() == Node.TEXT_NODE || node.getNodeType() == Node.ATTRIBUTE_NODE){
			String var = this.dataItemsToSelect.get(currPath);
			
			if(var!=null){
				Map<String,String> sol = new HashMap<String,String>(1);
				List<Map<String,String>> sols = new ArrayList<Map<String,String>>(1);
				sols.add(sol);
				sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), node!=null ? node.getTextContent().trim() : "");
				return sols;
			}
			else 
				return null;
		}

		if(node.getNodeType()!=Node.ELEMENT_NODE && node.getNodeType()!=Node.DOCUMENT_NODE) return null;

		if(this.paths == null) findActivePaths();

		String newPath = currPath;
		if(node.getNodeType() != Node.DOCUMENT_NODE){
			newPath += (currPath.isEmpty()?"":"/") + node.getLocalName().toUpperCase();		
			if(!paths.contains(newPath)) return null;
		}

		List<Map<String,String>> solutions = null;

		NamedNodeMap attrs = node.getAttributes();
		if(attrs!=null){
			for(int i=0; i<attrs.getLength(); i++){
				Node item = attrs.item(i);
				String attrPath = newPath +"/@"+item.getLocalName().toUpperCase();
				
				List<Map<String,String>> sols = this.processResponseNode(item, attrPath);
				if(sols!=null){
					if(solutions==null) solutions = sols;
					else {
						for(Map<String,String> solution : solutions)
							solution.putAll(sols.get(0));
					}
				}
			}
		}

		NodeList children = node.getChildNodes();
		int numOfChildren = children.getLength();

		Map<String, List<Node>> groupedNodes = new HashMap<String, List<Node>>(numOfChildren);
		for(int i=0; i<numOfChildren+1; i++){
			Node item;
			if(i >= numOfChildren){
				if(numOfChildren==0 && node.getNodeType()==Node.ELEMENT_NODE) item = null;
				else continue;
			}
			else item = children.item(i);

			String tag = item==null? null : item.getLocalName();
			List<Node> group = groupedNodes.get(tag);
			if(group==null){
				group = new ArrayList<Node>(1);
				groupedNodes.put(tag, group);
			}
			group.add(item);
		}

		for(String tag : groupedNodes.keySet()){

			List<Map<String,String>> newSolutions = null; 

			List<Node> group = groupedNodes.get(tag);
			for(Node item : group){
				List<Map<String,String>> sols = this.processResponseNode(item, newPath);
				if(sols!=null){
					if(newSolutions==null) newSolutions = new ArrayList<Map<String,String>>();
					if(solutions==null) newSolutions.addAll(sols);
					else {
						for(Map<String,String> solution : solutions){
							for(Map<String,String> sol : sols){
								Map<String,String> toAdd = new HashMap<String,String> (solution);
								toAdd.putAll(sol);
								newSolutions.add(toAdd);
							}
						}
					}
				}
			}
			if(newSolutions!=null && !newSolutions.isEmpty())
				solutions = newSolutions;
		}

		return solutions;
	}

	protected String getUpdateContent(List<Map<String, String>> updates) throws Exception{

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document result = db.newDocument();
		Element topElement = null;

		for(Map<String,String> update : updates){
			Map<Element, Element> thisUpdateMembers = new HashMap<Element, Element>();

			for(String var : this.dataItems.keySet()){
				String item = this.dataItems.get(var);

				if(item!=null && !item.startsWith("%%")){

					String value;
					if(var.startsWith(SaplConstants.VARIABLE_PREFIX)){
						String varName = var.substring(SaplConstants.VARIABLE_PREFIX.length()); 
						value = update.get(varName);
						if(value==null) continue;
					}
					else value = var;

					String parts[] = item.split("/");
					Element curr = null;
					String currPath = "";

					for(int i=0; i<parts.length; i++){
						String part = parts[i];
						currPath += (currPath.isEmpty()?"":"/")+part;

						if(i==0){
							if(topElement==null){
								Element child = result.createElement(part);
								result.appendChild(child);
								topElement = child;
							}
							curr = topElement;
						}
						else if(i<parts.length-1){
							Element created = thisUpdateMembers.get(curr);
							if(created!=null){
								curr = created;
							}
							else{
								if(canBeMultiple.get(currPath)==false){
									NodeList existing = curr.getElementsByTagName(part);
									if(existing!=null && existing.getLength()>0){
										Element child = (Element)existing.item(0);
										thisUpdateMembers.put(curr, child);
										curr = child;
										continue;
									}
								}

								Element child = result.createElement(part);
								curr.appendChild(child);
								thisUpdateMembers.put(curr, child);
								curr = child;
							}
						}
						else{
							if(part.startsWith("@")){
								String attrName = part.substring(1);
								String set = curr.getAttribute(attrName);
								if(set.isEmpty()) curr.setAttribute(attrName, value);
								else if(!set.equals(value)) curr.setAttribute(attrName, set+" "+value);
							}
							else {
								if(canBeMultiple.get(currPath)==false){
									NodeList existing = curr.getElementsByTagName(part);
									if(existing!=null && existing.getLength()>0){
										Element child = (Element)existing.item(0);
										child.setTextContent(child.getTextContent()+" "+value);
										continue;
									}
								}

								Element child = result.createElement(part);
								curr.appendChild(child);
								child.setTextContent(value);
							}
						}
					}
				}
			}
		}

		DOMImplementationLS domImplementation = (DOMImplementationLS) result.getImplementation();
		LSSerializer lsSerializer = domImplementation.createLSSerializer();
		String resultStr = lsSerializer.writeToString(result); 
		if(resultStr.startsWith("<?xml")) resultStr = resultStr.substring(resultStr.indexOf('>')+1).trim();
		return resultStr;
	}
	
	
	@Override
	protected Object runUpdate(List<Map<String, String>> updates) throws Exception{
		if(updates.size()>0)
			this.bindRequestVars(updates.get(0));

		String resultStr = getUpdateContent(updates);
		return this.sendUpdate(this.requestContent, resultStr);
	}

	
	//Support of IDE 

	@Override
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables) throws Throwable{
		getDoc();
		String result = "";
		result += "* d:tree {\n";
		if(this.doc!=null){
			Map<String,Integer> vars = new HashMap<String,Integer>();
			String ns = this.uri;
			if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
			ns += "#";
			result += processSyntaxExampleNode(this.doc,1,vars,false,null,includeEntities,fillVariables,ns);
		}
		result += "} .";
		return result;
	}		

	@Override
	public String generateOntologyDefinition() throws Throwable{
		getDoc();
		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		ns += "#";
		String result = SaplN3Parser.PREFIX_KEYWORD+" rdf: <"+SaplConstants.RDF_NS+"> . "+
				SaplN3Parser.PREFIX_KEYWORD+" rdfs: <"+SaplConstants.RDFS_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" owl: <"+SaplConstants.OWL_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" ds: <"+ns+"> . \n"+
				"ds:ontology a owl:Ontology . \n";
		
		if(this.doc!=null){
			result += processSyntaxExampleNode(this.doc,0,null,true,null,null,null,ns);
		}

		return result;
	}	


	@Override
	protected void getDoc() throws Throwable{
		if(this.doc == null){
			if(this.lastContent==null){
				this.addVarToItemsToSelect("", "");
				executeQuery(new SaplQueryResultSet(),null);
			}
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db;
			db = dbf.newDocumentBuilder();
			this.doc = db.parse(new InputSource(new StringReader((String)this.lastContent)));
		}
	}

	protected String processSyntaxExampleNode(Node node, int indentLevel, Map<String, Integer> vars, boolean doOntology, String currentClass, Set<String> includeEntities, Map<String,String> fillVariables, String namespace){
		String result = "";
		String indent = WhiteSpaceString.getWhiteSpaceString(SaplN3Producer.indent_size*indentLevel);

		String name = node.getLocalName();

		NodeList children = node.getChildNodes();		
		int numOfChildren = children.getLength();

		NamedNodeMap attrs = node.getAttributes();
		if(attrs!=null){
			for(int i=0; i<attrs.getLength(); i++){
				Node item = attrs.item(i);
				String ns = item.getPrefix();
				String attr = item.getLocalName();
				if(attr.equals("xmlns") || ns!=null && ns.equals("xmlns")) continue;
				if(doOntology){
					//nothing yet
				} else {
					if(includeEntities==null || includeEntities.contains("#"+attr)){
						String varName = attr;
						Integer n = vars.get(varName);
						if(n==null) n = 1;
						else{ n++; varName += n; }
						vars.put(attr,n);
						result += indent + "[d:attribute \""+attr+"\"] d:value "+SaplConstants.VARIABLE_PREFIX+varName+" .\n";
					}
				}
			}
		}

		Set<String> childTags = new HashSet<String>();
		for(int i=0; i<numOfChildren; i++){
			Node item = children.item(i);
			String tag = item.getLocalName();

			if(childTags.contains(tag)) continue;
			childTags.add(tag);

			if(item.getNodeType()==Node.ELEMENT_NODE){

				if(doOntology){
					String propName = tag;
					String className = tag.substring(0,1).toUpperCase()+tag.substring(1);
					String elementResult = processSyntaxExampleNode(item, indentLevel, vars, doOntology, className, includeEntities, fillVariables, namespace);	
					if(elementResult.isEmpty()){
						if(currentClass!=null){
							result += indent + "ds:"+propName+" a rdf:Property;";
							result += " rdfs:domain ds:"+currentClass+";";
							result += " rdfs:range rdfs:Literal .\n";
						}
					}
					else{
						if(currentClass!=null){
							result += indent + "ds:"+propName+" a rdf:Property;";
							result += " rdfs:domain ds:"+currentClass+";";
							result += " rdfs:range ds:"+className+" .\n";
						}
						result += indent + "ds:"+className+" a rdfs:Class .\n";
						result += elementResult;
					}
				} else {
					String elementResult = processSyntaxExampleNode(item, indentLevel+1, vars, doOntology, null, includeEntities, fillVariables, namespace);					
					if(elementResult.contains("{") || elementResult.contains("[")){
						result += indent + "[d:element \""+tag+"\"] d:branch {";
						result += "\n"+elementResult+indent;
						result += "} . \n";
					}
					else{
						if(includeEntities==null || includeEntities.contains("#"+tag)) {
							result += indent + "[d:element \""+tag+"\"] d:branch {";
							elementResult = elementResult.trim();
							if(elementResult.endsWith(".")) elementResult=elementResult.substring(0,elementResult.length()-1);
							result += " "+elementResult+" ";
							result += "} . \n";
						}
					}
				}
			}
			else if(item.getNodeType()==Node.TEXT_NODE){
				if(numOfChildren==1 || !item.getTextContent().trim().isEmpty()){
					if(!doOntology){
						String varName = name.toLowerCase();
						Integer n = vars.get(name);
						if(n==null) n = 1;
						else{ n++; varName += n; }
						vars.put(name,n);
						result += indent+"* d:value "+SaplConstants.VARIABLE_PREFIX+varName+". \n";

						if(fillVariables!=null) fillVariables.put(namespace+name, varName);
					}
				}
			}
		}

		return result;
	}	

}
