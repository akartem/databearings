package sapl.shared.eii;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.URITools;

/**
 * Behavior that performs data de-abstraction, i.e. substitutes a class name
 * with its full definition (e.g. ?x a Mother -> ?x a Human; hasGender Female; hasChild ?y).
 *  
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (29.05.2015)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015, VTT
 * 
 * */

public class ClassifierBehavior extends ReusableAtomicBehavior {

	public static boolean PERFORMANCE_PRINT = true;
	public static boolean DEBUG_PRINT = false;

	protected SaplN3ProducerOptions saplN3ProducerOptions = new SaplN3ProducerOptions(false, false); 

	protected Collection<String> resultTo;
	
	protected String statementsContextID;
	
	protected List<SemanticStatement> statements;
	protected List<String> originalStIDs = new ArrayList<String>();

	protected List<SemanticStatement> statementsInG;

	protected Map<String, String> abstractProperties = new HashMap<String, String>(); 

	protected Map<String, String> abstractClasses = new HashMap<String, String>(); 

	protected int generatedVarCount = 0;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.statementsContextID = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "statement"));
		statements = getStatements(this.statementsContextID, originalStIDs);
		if(statements.size()==0)
			throw new IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "statement"), statements.toString(), State.empty);

		this.resultTo = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "context")); 
	
		List<String> fixedResultTo = new ArrayList<String>();

		Iterator<String> it = resultTo.iterator();
		while(it.hasNext()){
			String contextID = it.next();
			if(contextID.startsWith(SaplConstants.CONTEXT_PREFIX))
				fixedResultTo.add(contextID);
		}
		this.resultTo = fixedResultTo;
	}

	@Override
	protected void doAction() throws Exception {

		long start = System.currentTimeMillis();
		long s = start;

		//create list of known abstract classes
		SaplQueryResultSet rs = hasBeliefsN3("?class a <"+SaplConstants.META_NS+"Class>");
		if(rs!=null){
			for(int i=0; i<rs.getNumberOfSolutions(); i++){
				String cl = rs.getSolution(i).get("class");
				abstractClasses.put(cl, "");
			}
		}
		//create list of known abstract properties
		SaplQueryResultSet rs2 = hasBeliefsN3("?property a <"+SaplConstants.META_NS+"Property>");
		if(rs2!=null){
			for(int i=0; i<rs2.getNumberOfSolutions(); i++){
				String property = rs2.getSolution(i).get("property");
				abstractProperties.put(property, "");
			}
		}
		
		statementsInG = this.getStatements(SaplConstants.G);

		for(int i=0; i<this.statements.size(); i++){
			
			generatedVarCount = 0;
			
			SemanticStatement st = this.statements.get(i);
			String originalStId = this.originalStIDs.get(i);
		
			String def;
			if(st.predicate.equals(SaplConstants.rdfType)){
				//resolving abstract class
				String variable = st.subject;
				String classID = st.object;		
				def = getSAPLDefinition(classID);
	
				def = def.replace("?this", variable);
				if(DEBUG_PRINT)
					print("Definition of class "+classID+" : \n"+ def);
	
				if(PERFORMANCE_PRINT)
					print("Definition of class "+classID+" processed: ["+(System.currentTimeMillis()-s)+"]");
			}
			else{
				//resolving abstract property
				String thing = st.subject;
				String variable = st.object;
				String property = st.predicate;
				def = getPropertySAPLDefinition(property);
				
				if(this.statements.size()>1){
					def = def.replace(SaplConstants.VARIABLE_PREFIX, SaplConstants.VARIABLE_PREFIX+"c"+(i+1)+"_");
					def = def.replace("?c"+(i+1)+"_var0", variable);
					def += ". ?c"+(i+1)+"_this <"+SaplConstants.expression+"> <"+thing+">";
				}
				else{
					def = def.replace("?var0", variable);
					def += ". ?this <"+SaplConstants.expression+"> <"+thing+">";
				}

				def = def.replace(variable+" <"+SaplConstants.eq, variable+" <"+SaplConstants.expression);
				
				if(DEBUG_PRINT)
					print("Definition of property "+property+" : \n"+ def);
	
				if(PERFORMANCE_PRINT)
					print("Definition of property "+property+" processed ["+(System.currentTimeMillis()-s)+"]");
			}
	
	
			s = System.currentTimeMillis();
	
			Iterator<String> it = resultTo.iterator();
			String firstContext = it.next();
	
			boolean result = this.addBeliefsN3(def, firstContext, true);
			if(!result){
				this.setFailed();
			}		
	
			if(it.hasNext()){
				List<String> ids = new ArrayList<String>();
				this.getStatements(firstContext, ids);
				while(it.hasNext()){
					String contextID = it.next();
					this.putStatements(ids, contextID);
				}
			}
	
			this.eraseBeliefs(originalStId);
	
			if(PERFORMANCE_PRINT)
				print("Definition loaded ["+(System.currentTimeMillis()-s)+"]");
		}

	}

	public String constructSAPLDefinition(String classID){
		String baseClass = null;
		String restriction = null;
		String context = null;

		for(SemanticStatement statement : statementsInG){
			if(statement.subject.equals(classID)){
				String predicate = statement.predicate;
				String object = statement.object;

				if(predicate.equals(SaplConstants.OWL_NS+"equivalentClass"))
					return getSAPLDefinition(object);
				else if(predicate.equals(SaplConstants.OWL_NS+"intersectionOf"))
					return getSAPLIntersectionOf(object);
				else if(predicate.equals(SaplConstants.OWL_NS+"unionOf"))
					return getSAPLUnionOf(object);
				else if(predicate.equals(SaplConstants.OWL_NS+"complementOf"))
					return getSAPLComplementOf(object);
				else if(predicate.equals(SaplConstants.META_NS+"pattern"))
					return getSAPLPattern(object);
				else if(predicate.equals(SaplConstants.META_NS+"base"))
					baseClass = getSAPLDefinition(object);
				else if(predicate.equals(SaplConstants.META_NS+"restriction")){
					if(restriction!=null){
						if(restriction.length()>0 && !restriction.trim().endsWith(".")) restriction += ". ";
						else if(restriction.length()>0 && !restriction.endsWith(" ")) restriction += " ";
						restriction += getSAPLPattern(object);
					}
					else restriction = getSAPLPattern(object);
				}
				else if(predicate.equals(SaplConstants.META_NS+"context")){
					String bnode = null;

					List<SemanticStatement> localSts = this.getStatements(object);
					for(SemanticStatement st: localSts){
						if(st.predicate.equals(SaplConstants.rdfType) && st.object.equals(SaplConstants.Context))
							bnode = st.subject;
					}
					context = this.produceN3(object, saplN3ProducerOptions)+" . ";
					context = context.replace(bnode+" "+"<"+SaplConstants.rdfType+">"+" <"+SaplConstants.Context+"> .", "").replace(bnode, "{ %%base%% }").trim();
				}

			}
		}

		if(baseClass==null && restriction==null){
			if(context!=null ) print("Warning: context without base class ignored for "+classID);
			return "?this a <"+classID+">";
		}
		else{
			if(baseClass==null) print("Warning: restriction given but not the base class for "+classID);
			else if(restriction==null && context==null){
				print("Warning: base class given but neither restriction nor context for "+classID);
			}

			String result = (baseClass!=null)?baseClass:"";
			if(restriction!=null){
				if(result.length()>0 && !result.trim().endsWith(".")) result += " . ";
				else if(result.length()>0 && !result.endsWith(" ")) result += " ";
				result += restriction;
			}
			if(context!=null){
				result = context.replace("%%base%%", result);
			}

			return result;
		}
	}


	public String getSAPLDefinition(String classID){
		String pattern = abstractClasses.get(classID);
		boolean anonym = false; 
		if(pattern != null || (anonym=classID.startsWith(SaplConstants.BLANK_NODE_PREFIX))){ // known to be abstract or an anonymous
			if(anonym || pattern.equals("")){ // and not yet processed
				pattern = constructSAPLDefinition(classID);
				abstractClasses.put(classID, pattern);
				return pattern;
			}
			else return pattern;
		}
		else return "?this a <"+classID+">";
	}
	

	protected String getSAPLIntersectionOf(String classID) {
		List<String> listValues = this.getRDFListValues(classID);
		String result = "";
		for(String value : listValues){
			if(result.length()==0) result = this.getSAPLDefinition(value);
			else result = result+" . "+this.getSAPLDefinition(value);
		}
		return result;
	}

	protected String getSAPLUnionOf(String classID) {
		List<String> listValues = this.getRDFListValues(classID);
		String result = "";
		for(String value : listValues){
			if(result.length()==0) result = this.getSAPLDefinition(value);
			else result = "{ "+result+" } <"+ SaplConstants.or +"> { "+this.getSAPLDefinition(value)+" }";
		}

		return result;
	}

	protected String getSAPLComplementOf(String object) {
		String result = this.getSAPLDefinition(object);
		result = "<"+SaplConstants.I+">" +" <"+SaplConstants.doNotBelieve + "> { "+result+" }";
		return result;
	}

	protected List<String> getRDFListValues(String head){
		List<String> result = new ArrayList<String>();
		List<String> tail = null;

		for(SemanticStatement statement : statementsInG){
			if(statement.subject.equals(head)){
				String predicate = statement.predicate;
				String object = statement.object;

				if(predicate.equals(SaplConstants.rdfFirst))
					result.add(object);
				else if(predicate.equals(SaplConstants.rdfRest)){
					if(!object.equals(SaplConstants.rdfNil))
						tail = getRDFListValues(object);		
				}
			}
		}
		if(tail!=null) result.addAll(tail);

		return result;
	}	

	protected String getSAPLPattern(String object) {
		String result = this.produceN3(object, saplN3ProducerOptions);

		findAbstractClasses(object);

		findAbstractProperties(object);

		for(String abstractClass : abstractClasses.keySet()){
			String pattern = abstractClasses.get(abstractClass);
			if(!pattern.equals("")){
				int ind;
				if((ind = result.indexOf(abstractClass))!=-1){
					int start = result.lastIndexOf(". ", ind);
					int end = result.indexOf(". ", ind);
					
					String[] parts = result.substring ( (start==-1 ? 0 : start+2) , (end==-1? result.length() : end)).split("\\s+");
					if(parts[1].equals("<"+SaplConstants.rdfType+">")){
						pattern = pattern.replace("?this", parts[0]);
						result = (start==-1?"":result.substring(0,start+2)) + pattern + (end==-1?"":result.substring(end));
					}
				}
			}
		}

		for(String abstractProperty : abstractProperties.keySet()){
			String pattern = abstractProperties.get(abstractProperty);
			if(!pattern.equals("")){
				int ind;
				if((ind = result.indexOf(abstractProperty))!=-1){
					int start = result.lastIndexOf(". ", ind);
					int end = result.indexOf(". ", ind);
					result = (start==-1?"":result.substring(0,start+2)) + pattern + (end==-1?"":result.substring(end));
				}
			}
		}

		return result;
	}

	protected void findAbstractClasses(String object) {
		List<SemanticStatement> sts = this.getStatements(object);
		for(SemanticStatement st : sts){
			if(st.predicate.equals(SaplConstants.rdfType))
				getSAPLDefinition(st.object);

			if(st.isSubjectContext())
				findAbstractProperties(st.subject);
			if(st.isObjectContext())
				findAbstractProperties(st.object);
		}
	}

	protected void findAbstractProperties(String object) {
		List<SemanticStatement> sts = this.getStatements(object);
		for(SemanticStatement st : sts){
			getPropertySAPLDefinition(st.predicate);

			if(st.isSubjectContext())
				findAbstractProperties(st.subject);
			if(st.isObjectContext())
				findAbstractProperties(st.object);
		}
	}

	protected String getPropertySAPLDefinition(String property){
		String pattern = abstractProperties.get(property);
		if(pattern != null){ // known to be abstract
			if(pattern.equals("")){ // and not yet processed
				SaplQueryResultSet rs = hasBeliefsN3("<"+property+"> <"+SaplConstants.META_NS+"equivalentProperty> ?definition. {<"+property+">" + " <"+SaplConstants.RDFS_NS+"range> ?range} <"+SaplConstants.SAPL_NS+"is> <"+SaplConstants.SAPL_NS+"Optional> ");
				if(rs!=null && rs.getNumberOfSolutions()>0){
					Map<String, String> sol = rs.getSolution(0);
					
					String generated = "?var"+generatedVarCount;
					generatedVarCount++;
					
					String definition = getSAPLDefinition(sol.get("definition")).replace("?subject", "?this").replace("?object", generated);

					String range = sol.get("range");
					if(range!=null){
						String rangeClass = getSAPLDefinition(range).replace("?this", generated);
						definition = rangeClass+". "+definition;
					}

					abstractProperties.put(property, definition);

					return definition;
				}
				else return property;
			}
			else return pattern;
		}
		else return property;
	}
}