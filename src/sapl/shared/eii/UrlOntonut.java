package sapl.shared.eii;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** 
 * Ontonut representing a REST Web Service where data is encoded into URL itself.
 * It only implements update operation, not query. 
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (30.08.2017)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2017, VTT
 * 
 * */
 
public class UrlOntonut extends HttpOntonut {

	String result = null;
	
	@Override
	public List<Map<String, String>> processResponse(Object content) throws Exception {
		return null;
	}

	@Override
	protected String runUpdate(List<Map<String, String>> updates) throws Exception{
		String updateURL = this.requestContent;

		final List<Thread> threads;
		if(updates.size()>1) threads = new ArrayList<Thread>(updates.size());
		else threads = null;
		
		for(Map<String, String> update : updates){
			String singleUpdateURL = updateURL;
			for(String var: update.keySet()){
				String value = update.get(var);
				String key = "%%"+var+"%%";
				if(singleUpdateURL.contains(key)){
					singleUpdateURL = singleUpdateURL.replace(key, value);
				}
			}
			
			if(updates.size()==1){
				return sendUpdate(singleUpdateURL, "");
			}
			else{
				final String f_UpdateURL = singleUpdateURL;
				Thread th = new Thread(new Runnable() {
					public void run(){
						try{
							result = sendUpdate(f_UpdateURL, "");
						}
						catch (Exception e){
							parent.printException(e, true);
						}
					}
				});
				th.start();
				threads.add(th);
			}
		}
		
		if(threads!=null){
			for (Thread th : threads) {
				th.join();
			}
		}
			
		return result;
	}

}
