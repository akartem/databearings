package sapl.shared.eii;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;

/** 
 * A part of Universal Adapter Behavior responsible for analyzing an S-APL query and
 * figuring out how to execute it on a set of ontonuts + local data
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (26.06.2018)
 * 
 * @since 4.2
 * 
 * Copyright (C) 2014-2018, VTT
 * 
 * */

public class QueryDecomposer {

	public static boolean DEBUG_PRINT = false;

	protected static final int PASS_SELECT = 0;
	protected static final int PASS_FILTER = 1;

	protected static final int PROCESS_HAS_SINGLE_ONTONUT_VARS = 1;
	protected static final int PROCESS_HAS_NO_VARS = 2;
	protected static final int PROCESS_HAS_LOCAL_VARS_ONLY = 3;
	protected static final int PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS = 4;
	protected static final int PROCESS_HAS_NOT_HANDLED_VARS = 5;	
	protected static final int PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS = 6;
	protected static final int PROCESS_NOT_READY = 1000;

	protected ReusableAtomicBehavior parent;

	protected int ontonutsN;
	protected String[] ontonutSemantics;
	protected List<Map<String, String>> dataItems;

	protected Map<String,Integer> alreadyHandledStatementIDs; // with result
	protected Map<String,QueryDecompositionResult> contextQDRs;
	protected Set<String> notCoveredStatementIDs;
	protected Map<String,SemanticStatement> notYetHandledSpecialStatements;

	QueryDecompositionResult topLevelQDR;

	Map<String,Boolean> areOrStatementsFilters = null;

	QueryDecomposer(ReusableAtomicBehavior parent, int ontonutsN, String[] ontonutSemantics, List<Map<String, String>> dataItems) {
		this.parent = parent;
		this.ontonutsN = ontonutsN;
		this.ontonutSemantics = ontonutSemantics;
		this.dataItems = dataItems;
	}

	boolean decompose(String queryContext, QueryDecompositionResult result){
		topLevelQDR = result;

		alreadyHandledStatementIDs = new HashMap<String,Integer>();
		contextQDRs = new HashMap<String,QueryDecompositionResult>();
		notCoveredStatementIDs = new HashSet<String>();
		notYetHandledSpecialStatements = new HashMap<String,SemanticStatement>();

		this.processQueryContext(queryContext, result, PASS_SELECT, null, false, false);

		if(result.hasMandatory && result.allMandatoryCoveredByMoreThanOneOntonut)
			result.ontonutsSolutionsCombinationMode = QueryDecompositionResult.COMBINATION_UNION;

		this.analyseObjectsInQuery(result);

		if(notYetHandledSpecialStatements.size()>0){
			while(notYetHandledSpecialStatements.size()>0){
				if(DEBUG_PRINT)
					System.out.println("PASS");
				int before = notYetHandledSpecialStatements.size();
				this.processQueryContext(queryContext,result,PASS_FILTER, null, false, false);
				if(before == notYetHandledSpecialStatements.size()){
					parent.printToErr("Cannot decompose the query, reason: "+notYetHandledSpecialStatements.values());
					return false;
				}

				this.analyseObjectsInQuery(result);
			}
		}
		else this.processQueryContext(queryContext,result,PASS_FILTER, null, false, false);

		this.analyseOntonutCoveredStatements(result);

		if(result.notHandledOntonutVars!=null){
			for(int ontonut : result.notHandledOntonutVars.keySet()){
				result.allOntonutVars.get(ontonut).removeAll(result.notHandledOntonutVars.get(ontonut));
			}
		}
		return true;
	}

	int processQueryContext(String queryContext, QueryDecompositionResult qdr, int mode, String topLevelID, boolean isOptional, boolean isNegative){

		QueryDecompositionResult previous_pass = contextQDRs.get(queryContext);
		if(previous_pass!=null){
			qdr.addAllFrom(previous_pass, true);
		}

		//get query statements
		ArrayList<String> ids = new ArrayList<String>();
		List<SemanticStatement> statements = this.parent.getStatements(queryContext, ids);

		int contextResult = 0;
		for(int i = 0; i < statements.size(); i++){
			SemanticStatement st = statements.get(i);
			String stID = ids.get(i);

			int statementResult = 0;

			if(alreadyHandledStatementIDs.containsKey(stID)){
				statementResult = alreadyHandledStatementIDs.get(stID);
			}
			else {
				QueryDecompositionResult result = new QueryDecompositionResult();
				if(st.predicate.equals(SaplConstants.and) && st.isSubjectContext() && st.isObjectContext()){

					int left = this.processQueryContext(st.subject, result, mode, topLevelID!=null?topLevelID:stID, true, false);
					QueryDecompositionResult save = this.topLevelQDR;
					QueryDecompositionResult temp = new QueryDecompositionResult();
					temp.addAllFrom(save, true);
					temp.addAllFrom(result, true);
					
					this.topLevelQDR = temp;
					int right = this.processQueryContext(st.object, result, mode, topLevelID!=null?topLevelID:stID, true, false);
					this.topLevelQDR = save;

					statementResult = Math.max(left, right);
		            
					if(!isOptional && (statementResult==PROCESS_HAS_SINGLE_ONTONUT_VARS 
							|| statementResult==PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS
							|| statementResult==PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS)){
						// make to be a local starter, will get joined with ontonut's results by variable values
						statementResult = PROCESS_HAS_LOCAL_VARS_ONLY;
					}		            
				}
				else if(st.predicate.equals(SaplConstants.or) && st.isSubjectContext() && st.isObjectContext()){

					if(topLevelID==null){
						if(areOrStatementsFilters==null) areOrStatementsFilters = new HashMap<String, Boolean>(2);
						if(areOrStatementsFilters.get(stID)==null) 
							areOrStatementsFilters.put(stID, isOrStatementFilter(st));
					}

					if(topLevelID==null && areOrStatementsFilters.get(stID)==true){
						if(mode==PASS_SELECT){
							statementResult = PROCESS_NOT_READY;
							notYetHandledSpecialStatements.put(stID, st);
						}
						else if(mode==PASS_FILTER){
							QueryDecompositionResult upToNow = new QueryDecompositionResult();
							upToNow.addAllFrom(this.topLevelQDR, true);
							if(previous_pass!=null) upToNow.addAllFrom(previous_pass, true);

							statementResult = processOrStatement(true, stID, st, upToNow, result, isOptional);

							if(statementResult != PROCESS_NOT_READY){
								notYetHandledSpecialStatements.remove(stID);
							}
						}
					}
					else {
						int left = this.processQueryContext(st.subject, result, mode, topLevelID!=null?topLevelID:stID, true, false);
						int right = this.processQueryContext(st.object, result, mode, topLevelID!=null?topLevelID:stID, true, false);
						statementResult = Math.max(left, right);
					}
				}
				else if(st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.Optional) && st.isSubjectContext()){
					statementResult = processQueryContext(st.subject, result, mode, topLevelID!=null?topLevelID:stID, true, false);
				}
				else if(st.predicate.equals(SaplConstants.doNotBelieve) && st.subject.equals(SaplConstants.I) && st.isObjectContext()){
					statementResult = this.processQueryContext(st.object, result, mode, topLevelID!=null?topLevelID:stID, true, true);
				}
				else if(SaplConstants.embedded_predicates.contains(st.predicate)){
					if(mode==PASS_SELECT){
						statementResult = PROCESS_NOT_READY;
						notYetHandledSpecialStatements.put(stID, st);
					}
					else if(mode==PASS_FILTER){
						QueryDecompositionResult upToNow = new QueryDecompositionResult();
						upToNow.addAllFrom(this.topLevelQDR, true);
						if(previous_pass!=null) upToNow.addAllFrom(previous_pass, true);

						statementResult = processSpecialStatement(stID, st, upToNow, result, isOptional);
						if(statementResult != PROCESS_NOT_READY){
							notYetHandledSpecialStatements.remove(stID);
						}
					}
				}
				else if(st.predicate.equals(SaplConstants.ID)){
					if(mode==PASS_SELECT){ 
						statementResult = PROCESS_HAS_LOCAL_VARS_ONLY;
						addAllVarsAsNotCovered(st, result);
					}
				}
				else {
					//whether this statement matches some of ontonuts
					boolean notCovered;
					HashSet<Integer> coveringOntonuts = null;
					if(notCoveredStatementIDs.contains(stID)) notCovered = true;
					else {
						coveringOntonuts = this.checkStatementVsOntonutsSemantics(stID, st, result);
						notCovered = coveringOntonuts.isEmpty();
					}
					
					if(notCovered){
						if(mode==PASS_SELECT){
							notCoveredStatementIDs.add(stID);
							statementResult = PROCESS_NOT_READY;
						}
						else {
							Set<String> required = new HashSet<String>();
							this.parent.findAllVarsInStatement(st, required, null);
							
							statementResult = this.analyseVariablesSet(this.topLevelQDR, required, -1, null);
							
							if(!isOptional && (statementResult==PROCESS_HAS_SINGLE_ONTONUT_VARS 
									|| statementResult==PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS
									|| statementResult==PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS)){

								// make to be a local starter, will get joined with ontonut's results by variable values
								statementResult = PROCESS_HAS_LOCAL_VARS_ONLY; 
							}
						}
					}
					else {
						statementResult = PROCESS_HAS_SINGLE_ONTONUT_VARS;

						for(Integer ontonut : coveringOntonuts){
							result.addCoveredSelectionStatement(ontonut, stID, st);
							if(topLevelID!=null)
								result.addTopLevelCoveredStatementID(ontonut, topLevelID);
							else result.addTopLevelCoveredStatementID(ontonut, stID);
							if(isOptional && !isNegative) 
								result.addOptionalCoveredStatementID(ontonut, stID);
						}

						if(!isOptional){
							result.hasMandatory = true;
							if(coveringOntonuts.size()==1)
								result.allMandatoryCoveredByMoreThanOneOntonut = false;
						}

					}	

					//CHANGE 19.01.2017: Moved here from 'else' block above  
					if(st.predicate.equals(SaplConstants.rdfType)){
						if(st.isSubjectVar()) result.addRdfTypeVar(st.getSubjectVariableName());
						else if(st.isSubjectBlankNode()) result.addRdfTypeVar(st.subject);
						result.addQueryObject(st.subject, coveringOntonuts);
						if(isNegative) result.addNegativeObjectID(st.subject);
					}
					//CHANGE 19.01.2017: Moved here from 'analyseObjectsInQuery'
					else {
						if(qdr.objectsInQuery!=null){
							for(String objURI : qdr.objectsInQuery.keySet()){
								Set<String> related = qdr.objectsInQuery.get(objURI);
								if(related.contains(st.subject))
									related.add(st.object);
							}
						}
					}

				}

				if(DEBUG_PRINT)
					System.out.println(st+" "+statementResult);

				if(statementResult != PROCESS_NOT_READY){
					alreadyHandledStatementIDs.put(stID,statementResult);
				}

				if(statementResult==PROCESS_HAS_SINGLE_ONTONUT_VARS)
					qdr.addAllFrom(result, true);
				else if (statementResult==PROCESS_HAS_NOT_HANDLED_VARS) 
					qdr.addAllFrom(result, false);
				else qdr.addBasicsFrom(result);

			}// statement types

			contextResult = Math.max(contextResult, statementResult);

			if(topLevelID==null){ //at the top level now
				if(statementResult==PROCESS_HAS_SINGLE_ONTONUT_VARS){
				}
				else if(statementResult==PROCESS_HAS_LOCAL_VARS_ONLY || statementResult==PROCESS_HAS_NO_VARS){
					qdr.addLocalStarter(stID, st);
					
					if(statementResult==PROCESS_HAS_LOCAL_VARS_ONLY){
						Set<String> required = new HashSet<String>();
						this.parent.findAllVarsInStatement(st, required, null);

						for(String var : required)
							this.topLevelQDR.addLocalStarterVar(var);
					}
					
				}
				else if(statementResult==PROCESS_HAS_NOT_HANDLED_VARS){
					qdr.addLocalFinalizer(stID, st);
				}
				else if(statementResult==PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS){
					//TODO Also { = } s:and { > } may be handled as joiner, although not very sensible to use in query
					if(SaplConstants.log_predicates.contains(st.predicate))
						qdr.addLocalJoiner(stID, st);
					else qdr.addLocalFinalizer(stID, st);
				}
				else if(statementResult==PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS){
					if(SaplConstants.log_predicates.contains(st.predicate))
						qdr.addJoiner(stID, st);
					else qdr.addLocalFinalizer(stID, st);
				}
			}
		} // for statement

		if(contextResult==PROCESS_NOT_READY && topLevelID!=null) 
			contextQDRs.put(queryContext, qdr);

		return contextResult;

	}

	protected HashSet<Integer> checkStatementVsOntonutsSemantics(String statementID, SemanticStatement st, QueryDecompositionResult qdr){
		HashSet<Integer> result = new HashSet<Integer>();
		boolean covered = false;
		for(int i=0; i < ontonutsN; i++){
			SaplQueryResultSet match = this.parent.hasBeliefs(statementID, ontonutSemantics[i], true);
			if(match!=null){
				covered = true;
				result.add(i);
				for(String var: match.getDefinedVars()){
					qdr.addCoveredVar(i, var);
				}
			}
		}

		if(!covered){
			this.addAllVarsAsNotCovered(st, qdr);
		}

		return result;
	}	

	protected void addAllVarsAsNotCovered(SemanticStatement st, QueryDecompositionResult qdr){
		if(!SaplConstants.embedded_predicates.contains(st.predicate)){
			String sVar = st.getSubjectVariableName();
			if(sVar!=null) qdr.addNotCoveredVar(sVar);
			String pVar = st.getPredicateVariableName();
			if(pVar!=null) qdr.addNotCoveredVar(pVar);
			String oVar = st.getObjectVariableName();
			if(oVar!=null) qdr.addNotCoveredVar(oVar);

			if(st.isSubjectBlankNode()) qdr.addNotCoveredVar(st.subject);
			if(st.isObjectBlankNode()) qdr.addNotCoveredVar(st.object);

			if(st.isSubjectContext()) this.addAllVarsAsNotCovered(st.subject, qdr);
			if(st.isObjectContext()) this.addAllVarsAsNotCovered(st.object, qdr);
		}
	}

	protected void addAllVarsAsNotCovered(String queryContext, QueryDecompositionResult qdr){
		List<SemanticStatement> statements = this.parent.getStatements(queryContext);
		for(SemanticStatement st : statements)
			this.addAllVarsAsNotCovered(st, qdr);
	}


	protected void analyseObjectsInQuery(QueryDecompositionResult qdr) {

		if(qdr.coveredSelectionStatements!=null){

			//Analyse objects in query to remove cross-ontonut interference
			for(int i=0; i <ontonutsN; i++){
				Map<String, SemanticStatement> coveredStatements = qdr.coveredSelectionStatements.get(i);
				if(coveredStatements!=null){
					Map<String,SemanticStatement> toProcess = new HashMap<String,SemanticStatement>(coveredStatements);
					Map<String,SemanticStatement> remain = null;
					boolean makesSenseToPostponeIfNotFound = (qdr.objectsInQuery!=null);
					while(!toProcess.isEmpty()){
						remain = new HashMap<String,SemanticStatement>(2);
						for(String stID : toProcess.keySet()){
							SemanticStatement st = toProcess.get(stID);
							Set<String> related = null;
							String rdfTypeVar = null;
							if(qdr.objectsInQuery!=null){
								related = qdr.objectsInQuery.get(st.subject);
								if(related==null){ // means st.subject is an object URI
									for(String var : qdr.objectsInQuery.keySet()){
										Set<String> option = qdr.objectsInQuery.get(var);
										if(option.contains(st.subject)){ // means st.subject is an object's linked (via a property) object URI 
											related = option;
											rdfTypeVar = var;
											break;
										}
									}
								}
								else rdfTypeVar = st.subject;
							}
							if(related!=null){
								if(qdr.ontonutObjects.get(i)==null || !qdr.ontonutObjects.get(i).contains(rdfTypeVar)){ // not this ontonut's object

									coveredStatements.remove(stID);
									
									//CHANGE 19.01.2017: Add as a local starter if now not covered by any ontonut
									boolean stillCovered = false;
									for(int j=0; j <ontonutsN; j++){
										if(qdr.coveredSelectionStatements.get(j).values().contains(st)){
											stillCovered = true;
											break;
										}
									}
									if(!stillCovered) qdr.addLocalStarter(stID, st);
									
									//CHANGE 19.01.2017: Added a check before removal of vars  
									// also added removal of blank node vars
									Set<String> remainingVars = new HashSet<String>();
									for(SemanticStatement remaining : coveredStatements.values()){
										this.parent.findAllVarsInStatement(remaining, remainingVars, null);
									}
									
									if(st.isObjectVar()){
										String varName = st.getObjectVariableName();
										if(!remainingVars.contains(varName))
											qdr.allOntonutVars.get(i).remove(varName);
									}
									if(st.isSubjectVar()){
										String varName = st.getSubjectVariableName();
										if(!remainingVars.contains(varName))
											qdr.allOntonutVars.get(i).remove(varName);
									}
									if(st.isObjectBlankNode()){
										if(!remainingVars.contains(st.object))
											qdr.allOntonutVars.get(i).remove(st.object);
									}
									if(st.isSubjectBlankNode()){
										if(!remainingVars.contains(st.subject))
											qdr.allOntonutVars.get(i).remove(st.subject);
									}
								}
							}
							else if(makesSenseToPostponeIfNotFound) { remain.put(stID,st); continue; }		
						}
						if(remain.size() == toProcess.size()) makesSenseToPostponeIfNotFound = false;
						toProcess = remain;
					}
				}
			}

			if(qdr.objectsInQuery!=null){
				int count = 1;
				for(String object : qdr.objectsInQuery.keySet()){
					qdr.addQueryObjectID(object, "o"+count);
					count++;
				}
			}		
		}
	}


	protected void analyseOntonutCoveredStatements(QueryDecompositionResult qdr) {

		if(qdr.coveredSelectionStatements!=null){
			for(int i=0; i <ontonutsN; i++){
				Map<String, SemanticStatement> coveredStatements = qdr.coveredSelectionStatements.get(i);
				if(coveredStatements!=null){
					
					//Run the query with all covered statements against the ontonut semantics, to find proper variable mappings
					SaplQueryResultSet r = new SaplQueryResultSet();  
					ArrayList<String> covered = new ArrayList<String>(coveredStatements.keySet());
					this.parent.hasBeliefs(covered, ontonutSemantics[i], true, r);
					
					if(r.getNumberOfSolutions()==0){
						//this means that, because of the same predicate name, coveredStatements have some extras.
						//this is not handled well in current implementation, but some cases will get resolved
						//if to remove one of statements that were optional
						if(qdr.optionalCoveredStatementIDs!=null){
							for(String optionalID : qdr.optionalCoveredStatementIDs.get(i)){
								covered.remove(optionalID);
								r = new SaplQueryResultSet();  
								this.parent.hasBeliefs(covered, ontonutSemantics[i], true, r);
								if(r.getNumberOfSolutions()>0){
									qdr.coveredSelectionStatements.get(i).remove(optionalID);
									break;
								}
								covered = new ArrayList<String>(coveredStatements.keySet());
							}
						}
					}
					
					//CHANGE 20.1: Passing and analysing all matching overall solutions, not just 1
					//Map<String,String> overallSolution = r.getSolution(0);

					for(int o=0; o<r.getNumberOfSolutions(); o++){
						Map<String,String> mapping = r.getSolution(o);
						mapping.remove("");
						qdr.addVariableMappingOption(i, mapping);
					}
					
					//For each covered statement, find data items corresponding to the variables and constants in it 
					for(String stID : coveredStatements.keySet()){
						SemanticStatement st = coveredStatements.get(stID);
						List<SemanticStatement> vec = new ArrayList<SemanticStatement>(1);
						vec.add(st);
						boolean addMandatory = qdr.optionalCoveredStatementIDs==null || qdr.optionalCoveredStatementIDs.get(i)==null || !qdr.optionalCoveredStatementIDs.get(i).contains(stID);
						this.findDataItemsInvolvedInQuery(i, ontonutSemantics[i], vec, qdr, addMandatory);
					}
				}
			}
		}

		//try to do some cleaning for the case of >1 object in the query
		//		if(qdr.objectsInQuery.size()>1){
		//			qdr.ontonutsSolutionsCombinationMode = QueryDecompositionResult.COMBINATION_JOIN;
		//			for(int o = 0; o < ontonuts.length; o++){
		//				Set<String> mandatory = qdr.mandatoryOntonutVars.get(o);
		//				Set<String> fixedMandatory = new HashSet<String>();
		//
		//				for(String var : mandatory){
		//					String varstr = SaplConstants.VARIABLE_PREFIX+var; 
		//					for (String object : qdr.objectsInQuery.keySet()){
		//						Set<Integer> ids = qdr.ontonutObjects.get(object);
		//						if(ids.contains(o)){
		//							if(qdr.objectsInQuery.get(object).contains(varstr))
		//								fixedMandatory.add(var);
		//						}
		//					}
		//				}
		//				mandatory.retainAll(fixedMandatory);
		//			}
		//		}
		
	}	


	/**Processes a list of query statements and finds the dataItems needed for answering these 
	 * query statements (extends the already given lists)*/
	protected void findDataItemsInvolvedInQuery(int ontonut, String definition, List<SemanticStatement> statements, QueryDecompositionResult qdr, boolean addToMandatory){

		Map<String,String> dataItems = this.dataItems.get(ontonut);
		List<SemanticStatement> defStatements = this.parent.getStatements(definition);

		for(SemanticStatement st : statements){

			if(st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.Optional)){
				List<SemanticStatement> vec = this.parent.getStatements(st.subject);
				this.findDataItemsInvolvedInQuery(ontonut, definition, vec, qdr, false);
			}
			else {
				Set<SemanticStatement> handledSt1s = new HashSet<SemanticStatement>();
				
				for(Map<String,String> viableSolution : qdr.variableMappingOptions.get(ontonut)){
					
					SemanticStatement st1 = new SemanticStatement(st);
					
					if(st.isSubjectVar()){
						String varName = st.getSubjectVariableName();
						if(addToMandatory) qdr.addOntonutMandatoryVar(ontonut,varName);
						String var = viableSolution.get(st1.getSubjectVariableName());
						if(var!=null) st1.subject = var; 
						else continue;
					}
					if(st.isPredicateVar()){
						String varName = st.getPredicateVariableName();
						if(addToMandatory) qdr.addOntonutMandatoryVar(ontonut,varName);
						String var = viableSolution.get(st1.getPredicateVariableName()); 
						if(var!=null) st1.predicate = var;
						else continue;
					}
					if(st.isObjectVar()){
						String varName = st.getObjectVariableName();
						if(addToMandatory) qdr.addOntonutMandatoryVar(ontonut,varName);
						String var = viableSolution.get(st1.getObjectVariableName());
						if(var!=null) st1.object = var;
						else continue;
					}
	
					if(st.isSubjectContext() || st.isSubjectBlankNode()) st1.subject=SaplConstants.VARIABLE_PREFIX+st.subject;
					if(st.isObjectContext() || st.isObjectBlankNode()) st1.object=SaplConstants.VARIABLE_PREFIX+st.object;
	
					if(handledSt1s.contains(st1)) continue;

					handledSt1s.add(st1);
					
					SemanticStatement st2 = null;
	
					for(SemanticStatement defSt : defStatements){
	
						if(st1.equalsUniversal(defSt) 
								|| defSt.equalsUniversal(st1)){
							String sVar1 = st1.getSubjectVariableName();
							String sVar2 = defSt.getSubjectVariableName();
							String oVar1 = st1.getObjectVariableName();
							String oVar2 = defSt.getObjectVariableName();
	
							if(sVar1!=null && !st.isSubjectBlankNode() && sVar2!=null && !sVar1.equals(sVar2) ||
									oVar1!=null && !st.isObjectBlankNode() && oVar2!=null && !oVar1.equals(oVar2)) 
								continue;
	
							st2 = defSt;
						}
					}
	
					if(st2==null) continue;
					
					//CHANGE 29.03.2017
					if(st.isSubjectVar() || st2.isSubjectVar() /*&& dataItems.containsKey(st2.subject) */&& !st.isSubjectBlankNode()){
						String dataItem = dataItems.get(st2.subject);
						if(dataItem==null) dataItem = "\""+st2.subject+"\"";
						qdr.addOntonutReferencedDataItem(ontonut, dataItem, st.subject);
					}
					if(st.isPredicateVar() || st2.isPredicateVar() /*&& dataItems.containsKey(st2.predicate)*/ ){
						String dataItem = dataItems.get(st2.predicate);
						if(dataItem==null) dataItem = "\""+st2.predicate+"\"";
						qdr.addOntonutReferencedDataItem(ontonut, dataItem, st.predicate);					
					}
					if(st.isObjectVar() || st2.isObjectVar() /*&& dataItems.containsKey(st2.object) */ &&!st.isObjectBlankNode()){ 
						String dataItem = dataItems.get(st2.object);
						if(dataItem==null) dataItem = "\""+st2.object+"\"";
						qdr.addOntonutReferencedDataItem(ontonut, dataItem, st.object);
						if(st.objectDatatype!=null) qdr.addOntonutConstantDatatype(ontonut,dataItems.get(st2.object),st.objectDatatype); 
					}
	
					if(st.isSubjectContext()){
						List<SemanticStatement> vec = this.parent.getStatements(st.subject);
						this.findDataItemsInvolvedInQuery(ontonut,st2.subject,vec, qdr, addToMandatory);
					}
					if(st.isObjectContext()){
						List<SemanticStatement> vec = this.parent.getStatements(st.object);
						this.findDataItemsInvolvedInQuery(ontonut,st2.object,vec, qdr, addToMandatory);
					}
				}

			}
		}
	}	

//	protected void findAllVarsInStatement(SemanticStatement st, Set<String> required, Set<String> defined){
//		if(required!=null){
//			required.addAll(this.parent.findAllRequiredVarsInStatement(st));
//		}
//		if(st.isSubjectContext()){
//			List<SemanticStatement> sts = this.parent.getStatements(st.subject);
//			for(SemanticStatement innerSt : sts)
//				findAllVarsInStatement(innerSt, required, defined);
//		}
//		if(st.isObjectContext()){
//			List<SemanticStatement> sts = this.parent.getStatements(st.object);
//			for(SemanticStatement innerSt : sts)
//				findAllVarsInStatement(innerSt, required, defined);
//		}
//
//		if(defined!=null){
//			if(st.predicate.equals(SaplConstants.expression) || st.predicate.equals(SaplConstants.count)
//					|| st.predicate.equals(SaplConstants.max) || st.predicate.equals(SaplConstants.min)
//					|| st.predicate.equals(SaplConstants.sum) || st.predicate.equals(SaplConstants.avg)){
//				defined.add(st.getSubjectVariableName());
//			}
//		}
//		if(st.predicate.equals(SaplConstants.groupedBy) || st.predicate.equals(SaplConstants.distinctBy)){
//			List<SemanticStatement> inner = this.parent.getStatements(st.subject);
//			for(SemanticStatement innerSt : inner){
//				if(SaplConstants.aggregate_predicates.contains(innerSt.predicate)){
//					if(required!=null) required.addAll(this.parent.findAllRequiredVarsInStatement(innerSt));
//					if(defined!=null) defined.add(innerSt.getSubjectVariableName());
//				}
//			}
//		}
//	}

	protected int processSpecialStatement(String stID, SemanticStatement st, QueryDecompositionResult upToNow, QueryDecompositionResult qdr, boolean isOptional){

		Set<String> vars = new HashSet<String>();
		Set<String> definedVars = new HashSet<String>();
		this.parent.findAllVarsInStatement(st, vars, definedVars);
		
		ArrayList<Integer> actual = new ArrayList<Integer>(1);
		int r = this.analyseVariablesSet(upToNow, vars, -1, actual);
		
		if(isOptional){ 
			// do not even consider using ontonuts for optional blocks with filters 
			if(r==PROCESS_HAS_SINGLE_ONTONUT_VARS || r==PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS)
				r = PROCESS_HAS_NOT_HANDLED_VARS;
		}

		if(r==PROCESS_HAS_SINGLE_ONTONUT_VARS && SaplConstants.aggregate_predicates.contains(st.predicate)){

			if(this.topLevelQDR.ontonutsSolutionsCombinationMode==QueryDecompositionResult.COMBINATION_UNION 
					|| this.topLevelQDR.joinerStatements!=null || this.topLevelQDR.localJoiners!=null || this.topLevelQDR.localFinalizers!=null){
				//a hack for cases when applying aggregators directly to ontonut response may produce wrong result 
				//may lose performance of aggregation, but should get the result right

				r = PROCESS_HAS_NOT_HANDLED_VARS; 
				for(int ontonut : actual){
					if(definedVars!=null){
						for(String definedVar : definedVars){
							qdr.addCoveredVar(ontonut, definedVar);
							String prefixedVar = SaplConstants.VARIABLE_PREFIX+definedVar;
							String newVarName="V_"+definedVar.toUpperCase();
							qdr.addOntonutReferencedDataItem(ontonut, newVarName, prefixedVar);
							qdr.addOntonutNotHandledVar(ontonut, definedVar);
						}
					}
				}
			}
		}

		if(r==PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS && upToNow.localStarterVars!=null 
				&& SaplConstants.embedded_predicates.contains(st.predicate) && !SaplConstants.aggregate_predicates.contains(st.predicate)){
			boolean allLocalStarterVars = true;
			for(String var : vars)
				if(!upToNow.localStarterVars.contains(var)){
					allLocalStarterVars = false; break;
				}
			// allow filters referring to local joiner variables only to be local starters
			if(allLocalStarterVars) r = PROCESS_HAS_LOCAL_VARS_ONLY;
		}

		if(r==PROCESS_HAS_SINGLE_ONTONUT_VARS){
			for(int ontonut : actual){
				qdr.addCoveredSpecialStatement(ontonut, stID, st);
				if(definedVars!=null){
					for(String definedVar : definedVars){
						qdr.addCoveredVar(ontonut, definedVar);
						String prefixedVar = SaplConstants.VARIABLE_PREFIX+definedVar;
						String newVarName="V_"+definedVar.toUpperCase();
						qdr.addOntonutReferencedDataItem(ontonut, newVarName, prefixedVar);
					}
				}
			}
		}
		else if(r!=PROCESS_NOT_READY){
			if(definedVars!=null){
				for(String definedVar : definedVars)
					qdr.addNotCoveredVar(definedVar);
			}			
		}

		return r;
	}

	protected int processOrStatement(boolean isTop, String stID, SemanticStatement st, QueryDecompositionResult upToNow, QueryDecompositionResult qdr, boolean isOptional){

		int statementResult = 0;
		ArrayList<Integer> actual = new ArrayList<Integer>(1);

		List<SemanticStatement> statements = this.parent.getStatements(st.subject);
		for(SemanticStatement statement : statements){
			if(statement.predicate.equals(SaplConstants.or)){
				int subResult = processOrStatement(false, stID, statement, upToNow, qdr, isOptional);
				statementResult = Math.max(statementResult, subResult);
			}
			else{
				Set<String> required = new HashSet<String>();
				this.parent.findAllVarsInStatement(st, required, null);
				
				int subResult = this.analyseVariablesSet(upToNow, required, -1, actual);
				statementResult = Math.max(statementResult, subResult);
				qdr.addFilterWithinOr(stID, statement);
			}
		}
		statements = this.parent.getStatements(st.object);
		for(SemanticStatement statement : statements){
			if(statement.predicate.equals(SaplConstants.or)){
				int subResult = processOrStatement(false, stID, statement, upToNow, qdr, isOptional);
				statementResult = Math.max(statementResult, subResult);
			}
			else{
				Set<String> required = new HashSet<String>();
				this.parent.findAllVarsInStatement(st, required, null);

				int subResult = this.analyseVariablesSet(upToNow, required, -1, actual);
				statementResult = Math.max(statementResult, subResult);
				qdr.addFilterWithinOr(stID, statement);
			}
		}

		if(isOptional){ 
			// do not even consider using ontonuts for optional blocks with filters 
			if(statementResult==PROCESS_HAS_SINGLE_ONTONUT_VARS || statementResult==PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS)
				statementResult = PROCESS_HAS_NOT_HANDLED_VARS;
		}

		if(isTop && statementResult==PROCESS_HAS_SINGLE_ONTONUT_VARS){
			for(int ontonut : actual)
				qdr.addCoveredSpecialStatement(ontonut, stID, st);	
		}

		return statementResult;
	}


	/**Returns true if s:or statement only contains filtering operations*/
	protected boolean isOrStatementFilter(SemanticStatement st){
		List<SemanticStatement> statements = this.parent.getStatements(st.subject);
		for(SemanticStatement statement : statements){
			if(statement.predicate.equals(SaplConstants.or)){
				if(!isOrStatementFilter(statement)) return false;
			}
			else if(!SaplConstants.log_predicates.contains(statement.predicate)) return false;
		}
		statements = this.parent.getStatements(st.object);
		for(SemanticStatement statement : statements){
			if(statement.predicate.equals(SaplConstants.or)){
				if(!isOrStatementFilter(statement)) return false;
			}
			else if(!SaplConstants.log_predicates.contains(statement.predicate)) return false;
		}

		return true;
	}

	/**Finds the ontonuts to the scope of which the given query variable belongs*/
	protected List<Integer> findOntonuts(QueryDecompositionResult qdr, String var){
		List<Integer> result = new ArrayList<Integer>(1);
		if(qdr.allOntonutVars==null) return result;
		
		for(int i=0; i<ontonutsN; i++){
			Set<String> vars = qdr.allOntonutVars.get(i);
			if(vars!=null && vars.contains(var)) result.add(i);
		}
		return result;
	}	

	/**Processes a S-APL expression to generate corresponding e.g. SQL expression
	 * If ontonut = -1, ontonut is not known yet and has to be inferred locally*/
	protected int analyseVariablesSet(QueryDecompositionResult qdr, Set<String> vars, int ontonut, ArrayList<Integer> actualOntonuts){
		
		int inferredOntonut = ontonut;
		boolean hadVars = false;
		boolean hadLocalVars = false;
		boolean hadLocalVarsOnly = true;
		for(String var : vars){
			hadVars = true;

			List<Integer> expressionTokenOntonuts = this.findOntonuts(qdr,var);

			if(expressionTokenOntonuts.isEmpty()){
				if(qdr.allQueryVars==null || !qdr.allQueryVars.contains(var)) return PROCESS_NOT_READY; 
				hadLocalVars = true;
				continue;
			}
			else{
				if(qdr.localStarterVars!=null && qdr.localStarterVars.contains(var)){
					return PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS;
				}
				hadLocalVarsOnly = false;
			}

			if(qdr.notHandledOntonutVars!=null){
				for(int o : expressionTokenOntonuts){
					Set<String> ontonutVars = qdr.notHandledOntonutVars.get(o);
					if(ontonutVars!=null && ontonutVars.contains(var)){
						if(actualOntonuts!=null) actualOntonuts.clear();
						return PROCESS_HAS_NOT_HANDLED_VARS;
					}
				}
			}

			if(inferredOntonut==-1){
				inferredOntonut = expressionTokenOntonuts.get(0); /////
				if(actualOntonuts!=null) actualOntonuts.addAll(expressionTokenOntonuts);
			}
			else if (!expressionTokenOntonuts.contains(inferredOntonut)){  
				if(actualOntonuts!=null){
					actualOntonuts.clear();
					actualOntonuts.addAll(expressionTokenOntonuts);					
				}

				return PROCESS_HAS_VARS_FROM_MULTIPLE_ONTONUTS;
			}

		}

		if(hadLocalVars){
			if(hadLocalVarsOnly)
				return PROCESS_HAS_LOCAL_VARS_ONLY;
			else 
				return PROCESS_HAS_BOTH_LOCAL_AND_ONTONUT_VARS;
		}

		if(!hadVars) return PROCESS_HAS_NO_VARS;
		else return PROCESS_HAS_SINGLE_ONTONUT_VARS;
	}

}
