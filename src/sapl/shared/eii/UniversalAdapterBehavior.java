package sapl.shared.eii;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.ExpressionEvaluator;
import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryEngine;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.BlankNodeGenerator;
import sapl.util.URITools;

/** 
 * The core of the DataBearings Semantic EII approach 
 * 
 * @author Artem Katasonov (VTT + Elisa since July 2018)
 * 
 * @version 4.7 (08.05.2019)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */ 

public class UniversalAdapterBehavior extends ReusableAtomicBehavior {

	public static boolean DEBUG_PRINT = false;
	public static boolean DEBUG_SOLUTIONS_PRINT = false;
	public static boolean PERFORMANCE_PRINT = true;
	public static boolean INTERACTION_PRINT = true;
	public static boolean EXCEPTION_FULL_STACK_PRINT = true;

	/**For logging only*/
	protected static int COUNTER = 0;
	protected int executionID;
	
	/**Start parameters*/
	@Parameter (name="if") protected String rule_if;
	@Parameter (name="then") protected String rule_then;
	@Parameter (name="else") protected String rule_else;
	@Parameter (name="case") protected String rule_case;
	@Parameter (name="getter") protected Collection<String> ontonutIDs;
	@Parameter (name="setter") protected Collection<String> setterOntonutIDs;

	/**Ontonut plug-ins*/
	protected Ontonut[] ontonuts;
	/**IDs of contexts giving data source structure*/ 
	protected String[] cntData; 
	/**IDs of contexts giving data source semantics*/ 
	protected String[] cntSemantics; 
	/**Data items in each data source structure*/ 
	protected List<Map<String,String>> dataItems; 

	protected QueryDecomposer decomposer;
	protected QueryDecompositionResult qdr;

	/**Solutions to sub-queries received from data source*/
	protected Map<Integer,List<Map<String,String>>> solutions = null;
	/**Solutions representing exceptions occurred in ontonuts' execution*/
	protected List<Map<String,String>> errorSolutions = null;
	
	/**Flag to indicate that the local solutions have already been added to ontonut solutions
	 * (in UNION case, needs to be done only once)*/
	protected Boolean unionSolutionsExtended = false;
	
	public final BlankNodeGenerator bnodeGenerator = new BlankNodeGenerator();


	@Override
	public void print(Object text) {
		super.print("("+executionID+") "+text);
	}
	@Override
	public void printToErr(Object text) {
		super.printToErr("("+executionID+") "+text);
	}
	@Override
	public void printError(String text) {
		super.printError("("+executionID+") "+text);
	}

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {

		this.executionID = COUNTER++;
		
		//override the default value to 'true'
		this.executeInThread = parameters.getOptionalBooleanParameter
				(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "thread"), true);

		this.rule_if = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "if"));
		this.rule_then = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "then"));
		this.rule_else = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "else"));
		this.ontonutIDs = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "getter"));

		this.rule_case = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "case"));
		
		this.setterOntonutIDs = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "setter"));

		if(DEBUG_PRINT){
			print("Getters: "+ontonutIDs);
			print("Setters: "+setterOntonutIDs);
		}
	}

	/**Read ontonut definitions, create ontonuts and let them configure themselves*/
	protected boolean initializeOntonuts(){
		int N = this.ontonutIDs.size(); 

		ontonuts = new Ontonut[N];
		cntData = new String[N]; 
		cntSemantics = new String[N]; 
		dataItems = new ArrayList<Map<String,String>>(N);

		solutions = new HashMap<Integer,List<Map<String,String>>>(N+1);
		errorSolutions = new ArrayList<Map<String,String>>(2);

		int index = -1;
		for(String ontonutID : this.ontonutIDs){
			index++;

			//Access the ontonut definition
			String query = "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
			query += "<"+ontonutID +">" +" o:type ?type; o:semantics { ?data => ?semantics }. ";

			SaplQueryResultSet resultSet = this.hasBeliefsN3(query);
			if(resultSet.getNumberOfSolutions()==0){
				printToErr("Ontonuts: ontonut "+ontonutID+" is not well defined"); this.setFailed(); return false;
			}
			Map<String, String> def = resultSet.getSolution(0);
			String type = def.get("type");

			Ontonut ontonut=null;

			//load ontonut plug-in
			try {
				@SuppressWarnings("unchecked")
				Class<Ontonut> cl = (Class<Ontonut>) Class.forName(type);
				ontonut = cl.newInstance();
			} catch (Throwable e) {
				e.printStackTrace();
				printToErr("Ontonuts: ontonut "+ontonutID+" has unknown type "+ type); 
				this.setFailed(); return false;
			}

			//Let the plug-in to process its definition
			ontonuts[index] = ontonut;
			ontonut.configure(ontonutID, index+1, this, this.rule_case);

			if(!ontonut.loadDefinition()){
				printToErr("Ontonuts: ontonut "+ontonutID+" failed to initialize"); 
				this.setFailed(); return false;
			}

			cntSemantics[index] = def.get("semantics");
			cntData[index] = def.get("data");

			//find data items in the data source structure
			Map<String,String> map = new LinkedHashMap<String,String>();
			ontonut.findSyntaxDataItems(cntData[index], map);

			if(map.isEmpty()){
				printToErr("Ontonuts: ontonut "+ontonutID+" failed to locate any data items in its syntax definition"); 
				this.setFailed(); return false;
			}	

			dataItems.add(map);
		}

		return true;
	}

	/**Check the query against ontonut definitions to decompose the query and also find mappings
	 * between query variables and data items in data sources
	 * @return false, if some statement did not match neither local base nor any ontonut, thus query is definitely false*/
	protected boolean decomposeQuery(String queryContext){
		this.decomposer = new QueryDecomposer(this, ontonuts.length, cntSemantics, dataItems);
		this.qdr = new QueryDecompositionResult();
		if(!this.decomposer.decompose(queryContext,this.qdr)) return false;

		if(this.qdr.ontonutObjects!=null){
			for (Integer ontonut: this.qdr.ontonutObjects.keySet()){
				for(String o: this.qdr.ontonutObjects.get(ontonut)){
					String object = this.qdr.objectsIDs.get(o);
					ontonuts[ontonut].addObjectID(object);
					
					if(this.qdr.negativeObjectsIDs!=null){
						if(this.qdr.negativeObjectsIDs.contains(o))
							ontonuts[ontonut].addNegativeObjectID(object);
					}
					
				}
			}
		}

		return true;
	}
	
	protected String addObjectIDToDataItem(int ontonut, String dataItem, String queryElement){
		if(dataItem==null || dataItem.startsWith("\"")) return dataItem;
		if(this.qdr.objectsInQuery==null || this.qdr.objectsInQuery.isEmpty()) return dataItem;
		Set<String> oObjs = this.qdr.ontonutObjects.get(ontonut);
		if(oObjs==null || oObjs.size()<2) return dataItem;
		for(String object: this.qdr.objectsInQuery.keySet()){
			if(!oObjs.contains(object)) continue;
			if(this.qdr.objectsInQuery.get(object).contains(queryElement)){
				return this.qdr.objectsIDs.get(object)+"."+dataItem;
			}
		}
		return dataItem;
	}
	
	protected String getExpression(String expression, int ontonut){
		StringBuilder result = new StringBuilder();
		List<String> tokens = ExpressionEvaluator.tokenize(expression);
		for(int i=0;i<tokens.size();i++){
			String token = tokens.get(i);
			if(token.startsWith(SaplConstants.VARIABLE_PREFIX) && token.length() > SaplConstants.VARIABLE_PREFIX.length()){
				String value =  this.getQueryReferencedDataItem(ontonut, token);
				String fullname = this.addObjectIDToDataItem(ontonut, value, token);
				tokens.set(i, fullname);				
			}
			else if(token.contains("\"")) tokens.set(i,token.replace("\"","'"));			
		}
		ontonuts[ontonut].processExpression(tokens);

		for (int i=0; i<tokens.size();i++) result.append(tokens.get(i));
		return result.toString();
	}

	/**Feed to ontonuts all conditions originating from basic selection query statements*/
	protected void feedOntonutsSelectionStatements(){
		
		if(this.qdr.queryReferencedDataItems!=null){
			for(int o = 0; o < this.ontonuts.length; o++){

				//CHANGE 20.1.2016: Feed variable mapping options to ontonuts
				ontonuts[o].setSelectVariableMappings(qdr.variableMappingOptions.get(o));
				
				Map<String,Set<String>> items = this.qdr.queryReferencedDataItems.get(o);
				if(items!=null){
					for(Map.Entry<String,Set<String>> entry : items.entrySet()){
						String dataItem = entry.getKey();
						if(dataItems.get(o).containsValue(dataItem) || 
								(dataItem.startsWith("\"") && dataItem.endsWith("\""))){
							Set<String> values = entry.getValue();
							this.feedOntonutSelectionStatement(o, dataItem, values);
						}
					}
				}
			}
		}
	}	

	protected void feedOntonutSelectionStatement(int o, String dataItem, Set<String> values){
		
		for(String value : values) {
			boolean isValueString=false;

			if(this.qdr.queryReferencedDataItemsConstantDatatypes!=null && !value.startsWith(SaplConstants.VARIABLE_PREFIX)){
				String str = this.qdr.queryReferencedDataItemsConstantDatatypes.get(o).get(dataItem);
				if(str!=null && (str.contains("string") || str.contains("date"))) isValueString=true;
			}

			if(!isValueString){
				String num=null;
				String intnum=null;
				try{
					Double doub = Double.valueOf(value);
					num = doub.toString();
					intnum = String.valueOf((long)doub.doubleValue());
				}catch(NumberFormatException ex){
					isValueString=true;
				} 
				if(!isValueString && !value.equals(num) && !value.equals(intnum)) isValueString=true;
			}

			String variable = null; 
			Map<String, String> items = dataItems.get(o);
			for(String var : items.keySet())
				if(items.get(var).equals(dataItem)){
					variable = var;
					break;
				}


			String fullname = this.addObjectIDToDataItem(o, dataItem, value);
			String fullvar = variable;
			if(fullname.length()>dataItem.length())
				fullvar = SaplConstants.VARIABLE_PREFIX+this.addObjectIDToDataItem(o, variable.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
			ontonuts[o].addSelectionCondition(fullname, value, isValueString, fullvar);
		}
	}

	/**Feed to ontonuts all conditions originated from special query statements,
	 * such as filters, counts, etc*/
	protected void feedOntonutsSpecialStatements(){
		
		if(this.qdr.coveredSpecialStatements!=null){
			for(Integer ontonut : this.qdr.coveredSpecialStatements.keySet()){
				Map<String,SemanticStatement> covered = this.qdr.coveredSpecialStatements.get(ontonut);
				if(covered!=null){
					covered = new LinkedHashMap<String,SemanticStatement>(covered);
					for(String stID : covered.keySet()){
						SemanticStatement st = covered.get(stID);
						boolean containsNotHandledVar = false;
						
						if(this.qdr.notHandledOntonutVars!=null){
							Set<String> notHandled = this.qdr.notHandledOntonutVars.get(ontonut);
							if(notHandled!=null){
								Set<String> requiredVars = new HashSet<String>();
								this.findAllVarsInStatement(st, requiredVars, null);
								
								for(String var : notHandled){
									if(requiredVars.contains(var)){
										containsNotHandledVar = true;
										break;
									}
								}
							}
						}

						if(containsNotHandledVar || !this.feedOntonutsSpecialStatement(ontonut, st, stID)){
							modifyQDRdueNotHandledSpecialStatement(ontonut,stID, st);
						}
					}
				}
			}
		}
	}	

	protected void modifyQDRdueNotHandledSpecialStatement(int ontonut, String stID, SemanticStatement st){
		Map<String,SemanticStatement> covered = this.qdr.coveredSpecialStatements.get(ontonut);
		covered.remove(stID);
		qdr.addLocalOntonutCannotHandle(ontonut, stID, st);

		Set<String> definedVars = new HashSet<String>();
		this.findAllVarsInStatement(st, null, definedVars);
		
		if(definedVars!=null){
			for(String var : definedVars){
				this.qdr.allOntonutVars.get(ontonut).remove(var);
				this.qdr.addOntonutNotHandledVar(ontonut, var);
			}
		}
	}

	protected boolean feedOntonutsSpecialStatement(Integer ontonut, SemanticStatement st, String stID) {
		if(SaplConstants.log_predicates.contains(st.predicate)){
			return this.feedFilterCondition(ontonut, st);
		}
		else if(SaplConstants.aggregate_predicates.contains(st.predicate)){
			if(st.predicate.equals(SaplConstants.count)){
				boolean r = this.feedCountCondition(ontonut, st);
				if(r){
					Set<String> mandatory = this.qdr.mandatoryOntonutVars.get(ontonut);
					if(mandatory!=null) mandatory.clear();
					this.qdr.addOntonutMandatoryVar(ontonut, st.getSubjectVariableName());
				}
				return r;
			}
			else if(st.predicate.equals(SaplConstants.max) || st.predicate.equals(SaplConstants.min)
					|| st.predicate.equals(SaplConstants.sum) || st.predicate.equals(SaplConstants.avg)){
				boolean r =  this.feedMinMaxSumAvgCondition(ontonut, st);
				if(r){
					Set<String> mandatory = this.qdr.mandatoryOntonutVars.get(ontonut);
					if(mandatory!=null) mandatory.clear();
					this.qdr.addOntonutMandatoryVar(ontonut, st.getSubjectVariableName());
				}
				return r;
			}
			else if(st.predicate.equals(SaplConstants.groupedBy)){
				//TODO: Process properly s:groupedBy for ontonuts that support it, e.g. SQL
				return false;
			}
			else if(st.predicate.equals(SaplConstants.distinctBy)){
				return false;
			}
		}
		else if(st.predicate.equals(SaplConstants.expression)){
			boolean r =  this.feedExpressionCondition(ontonut, st);
			if(r){
				this.qdr.addOntonutMandatoryVar(ontonut, st.getSubjectVariableName());
			}
			return r;			
		}
		else if(st.predicate.equals(SaplConstants.or)){
			Set<SemanticStatement> filters = this.qdr.filtersWithinOr.get(stID);
			return this.feedOrCondition(ontonut, filters);
		}

		return false;
	}

	/**Finds a given ontonut's dataItem corresponding to a query variable*/
	protected String getQueryReferencedDataItem(int ontonut, String prefixedVar){
		if(this.qdr.queryReferencedDataItems!=null){
			Map<String, Set<String>> items = this.qdr.queryReferencedDataItems.get(ontonut);
			if(items!=null){
				for(String dataItem : items.keySet())
					if(items.get(dataItem).contains(prefixedVar)) return dataItem;
			}
		}
		return null;
	}	
	
	protected void tailorFilterCondition(Integer ontonut, SemanticStatement st,
			List<String> dataItems, List<String> operations, List<String> values, 
			List<Boolean> isValuesString, List<String> variables){
		
		String operation = ontonuts[ontonut].getLogOperation(st.predicate);

		String dataItem;
		String value;
		boolean isDataItemExpression = false;
		boolean isValueExpression = false;
		boolean justString = false;

		String expression = this.getExpression(st.object, ontonut);
		boolean isObjectExpression = !st.object.equals(expression);
		
		if(st.isSubjectVar()){
			dataItem = this.getQueryReferencedDataItem(ontonut, st.subject);
			value = expression;
			isValueExpression = isObjectExpression;
		}
		else {
			value = st.subject;
			dataItem = expression;
			isDataItemExpression = isObjectExpression;
		}
		
		//check if a string value 
		if(st.objectDatatype!=null && (st.objectDatatype.contains("string") || st.objectDatatype.contains("date") ) )
			justString=true;
		if(!justString && !isValueExpression){
			String num=null;
			String intnum=null;
			try{
				Double doub = Double.valueOf(value);
				num = doub.toString();
				intnum = String.valueOf((long)doub.doubleValue());
			}catch(NumberFormatException ex){
				justString=true;
			}
			if(!value.equals(num) && !value.equals(intnum)) justString=true;
		}

		String variable = null; 
		Map<String, String> items = this.dataItems.get(ontonut);
		for(String var : items.keySet())
			if(items.get(var).equals(dataItem)){
				variable = var;
				break;
			}

		if(!isDataItemExpression) dataItem = addObjectIDToDataItem(ontonut, dataItem, st.subject);		
		String fullvar = variable;
		if(fullvar!=null) fullvar = SaplConstants.VARIABLE_PREFIX+this.addObjectIDToDataItem(ontonut, variable.substring(SaplConstants.VARIABLE_PREFIX.length()), value);		
		
		dataItems.add(dataItem);
		operations.add(operation);
		values.add(value);
		isValuesString.add(justString);
		variables.add(fullvar);
	}

	protected boolean feedFilterCondition(Integer ontonut, SemanticStatement st){
		
		List<String> dataItems = new ArrayList<String>(1);
		List<String> operations = new ArrayList<String>(1);
		List<String> values = new ArrayList<String>(1);
		List<Boolean> isValuesString = new ArrayList<Boolean>(1);
		List<String> variables = new ArrayList<String>(1);
		
		this.tailorFilterCondition(ontonut, st, dataItems, operations, values, isValuesString, variables);

		return ontonuts[ontonut].addFilterCondition(dataItems.get(0), operations.get(0), values.get(0),
				isValuesString.get(0), variables.get(0));	
	}
	
	protected boolean feedOrCondition(Integer ontonut, Set<SemanticStatement> filters){
		List<String> dataItems = new ArrayList<String>(filters.size());
		List<String> operations = new ArrayList<String>(filters.size());
		List<String> values = new ArrayList<String>(filters.size());
		List<Boolean> isValuesString = new ArrayList<Boolean>(filters.size());
		List<String> variables = new ArrayList<String>(filters.size());
		
		for(SemanticStatement st : filters)
			this.tailorFilterCondition(ontonut, st, dataItems, operations, values, isValuesString, variables);
		
		return ontonuts[ontonut].addOrCondition(dataItems, operations, values,
				isValuesString, variables);
	}

	protected boolean feedExpressionCondition(Integer ontonut, SemanticStatement st){
		String expression = this.getExpression(st.object, ontonut);
		String varName = st.getSubjectVariableName();
		String newVarName="V_"+varName.toUpperCase();		
		return ontonuts[ontonut].addExpressionCondition(expression, newVarName, st.subject);
	}

	protected boolean feedCountCondition(Integer ontonut, SemanticStatement st){

		Set<String> vars = new HashSet<String>();
		this.findAllVarsInStatement(st, vars, null);
		
		String countBy = "";
		for (String var : vars){
			String prefixedVar = SaplConstants.VARIABLE_PREFIX+var;
			String varvalue = var;
			if(!var.equals(SaplConstants.ANYTHING)){
				varvalue = this.getQueryReferencedDataItem(ontonut, prefixedVar);
				varvalue = 	this.addObjectIDToDataItem(ontonut, varvalue, prefixedVar);
			}
			countBy += ((countBy.isEmpty())?"":", ")+varvalue;							
		}

		String varName = st.getSubjectVariableName();
		String newVarName="V_"+varName.toUpperCase();
		return ontonuts[ontonut].addCountCondition(countBy, newVarName, st.subject);
	}

	protected boolean feedMinMaxSumAvgCondition(Integer ontonut, SemanticStatement st){
		String prefixedVar = st.object;
		String varvalue = this.getQueryReferencedDataItem(ontonut, prefixedVar);
		varvalue = 	this.addObjectIDToDataItem(ontonut, varvalue, prefixedVar);		
		String varName = st.getSubjectVariableName();
		String newVarName="V_"+varName.toUpperCase();
		if(st.predicate.equals(SaplConstants.max))
			return ontonuts[ontonut].addMaxCondition(varvalue, newVarName, st.subject);
		else if(st.predicate.equals(SaplConstants.min))
			return ontonuts[ontonut].addMinCondition(varvalue, newVarName, st.subject);
		else if(st.predicate.equals(SaplConstants.sum))
			return ontonuts[ontonut].addSumCondition(varvalue, newVarName, st.subject);
		else if(st.predicate.equals(SaplConstants.avg))
			return ontonuts[ontonut].addAvgCondition(varvalue, newVarName, st.subject);
		else return false;
	}

	/**Let ontonuts to executing queries on their data sources*/
	protected void executeOntonutQueries(final SaplQueryResultSet localSolutions){
		
		ArrayList<Thread> threads = new ArrayList<Thread>(ontonuts.length);
		this.unionSolutionsExtended = false;
		
		for(int i=0; i<ontonuts.length; i++){

			final Ontonut o = ontonuts[i];
			final int index = i;
			
			Thread th = new Thread( new Runnable() {
				public void run() {
					//Execute the ontonut
					List<Map<String, String>> sols;
					boolean ontonutFailed = false;

					Map<String,String> error = null;
					List<Map<String,String>> ontonutErrorSolutions = new ArrayList<Map<String,String>>(1);					
					try {
						sols = o.executeQuery(new SaplQueryResultSet(localSolutions), ontonutErrorSolutions);
					} catch (Throwable e) {
						printError(o.shortId+" ONTONUT ERROR!");
						printException(e, EXCEPTION_FULL_STACK_PRINT);
						ontonutFailed = true;
						sols = null;

						error = new HashMap<String,String>();
						error.put("ontonutErrorSource", o.id);
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						error.put("ontonutErrorTrace", sw.toString());
						error.put("ontonutErrorMessage", UniversalAdapterBehavior.getExceptionMeaning(e.getMessage(), sw.toString()));

						error.put("", "."+(index+1)+".1");
					}

					long s = System.currentTimeMillis();

					if(!ontonutErrorSolutions.isEmpty()){
						errorSolutions.addAll(ontonutErrorSolutions);
					}

					if(sols == null){
						sols = new ArrayList<Map<String,String>>();
						if(error!=null){
							errorSolutions.add(error);
						}
					}

					//Filter out those solutions that do not have all the mandatory variables
					List<Map<String,String>> validSols;
					if(ontonutFailed) validSols = (List<Map<String,String>>)sols;
					else {
						validSols = new ArrayList<Map<String,String>>();
						for(Map<String,String> sol: sols){
							boolean valid = true;
							if(qdr.mandatoryOntonutVars!=null){
								Set<String> vars = qdr.mandatoryOntonutVars.get(index);
								if(vars!=null){
									for(String var : vars){
										if(!sol.containsKey(var)){
											valid = false;
											if(PERFORMANCE_PRINT)
												print(o.shortId+":: Rejected as invalid "+sol+" : no mandatory "+var);
											break;
										}
									}
								}
							}
							if(valid) validSols.add(sol);
						}
					}

					synchronized (solutions) {
						solutions.put(index, validSols);
					}
					if(PERFORMANCE_PRINT)
						print(o.shortId+":: Accepted "+ validSols.size() +" valid results [" + (System.currentTimeMillis() - s) + "]");
					
					boolean toExtend = false;
					synchronized (unionSolutionsExtended) {
						if(qdr.ontonutsSolutionsCombinationMode != QueryDecompositionResult.COMBINATION_UNION
								|| unionSolutionsExtended==false) {
							toExtend = true;
							unionSolutionsExtended = true;
						}
					}
					
					if(toExtend) extendOntonutQueryResults(index);
					
					fixOntonutQueryResults(index);

				}
			});
			
			if(ontonuts.length>1){
				threads.add(th);
				th.start();
			}
			else th.run();
		}

		for (Thread th : threads){
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	protected int extendOntonutQueryResults(int ontonut){
		long s = System.currentTimeMillis();
		int ontoCounter = 0;
		List<Map<String, String>> sols = solutions.get(ontonut);
		Set<String> toCheck = this.qdr.topLevelCoveredStatementIDs.get(ontonut);
		if(toCheck!=null){
			SaplQueryResultSet rs = new SaplQueryResultSet(); 
			boolean r = this.hasBeliefs(new ArrayList<String>(toCheck), SaplConstants.G, rs);
			if(r){
				for(int i=0; i<rs.getNumberOfSolutions();i++){
					Map<String, String> sol = rs.getSolution(i);
					sol.put("", ".0."+(++ontoCounter));
					sols.add(sol);
				}
			}
		}
		if(ontoCounter>0 && PERFORMANCE_PRINT)
			print(ontonuts[ontonut].shortId+":: Solutions locally extended with "+ontoCounter+" solutions ["+(System.currentTimeMillis()-s)+"]");

		return ontoCounter;
	}

	
	/**Apply statements covered but non-handled by ontonuts*/
	protected void fixOntonutQueryResults(int ontonut){

		if(this.qdr.localOntonutCannotHandle!=null){
			Map<String, SemanticStatement> toApply = this.qdr.localOntonutCannotHandle.get(ontonut);

			if(toApply!=null && toApply.size()>0){

				List<Map<String, String>> sols = solutions.get(ontonut);

				if(!sols.isEmpty()){
					long s = System.currentTimeMillis();
					SaplQueryResultSet rs = new SaplQueryResultSet();
					rs.setSolutions(sols, this.qdr.allOntonutVars.get(ontonut));

					boolean r = this.hasBeliefs(new ArrayList<String>(toApply.keySet()), SaplConstants.G, rs);

					sols.clear();
					if(r){
						for(int i=0; i<rs.getNumberOfSolutions(); i++)
							sols.add(rs.getSolution(i));
					}

					if(PERFORMANCE_PRINT)
						print(ontonuts[ontonut].shortId+":: After ontonut-not-handled statements: "+sols.size()+" solutions ["+(System.currentTimeMillis()-s)+"]");
				}
			}
		}
	}


	protected List<Map<String,String>> combineOntonutResults(){

		List<Map<String,String>> resultSolutions = null;

		if(this.qdr.ontonutsSolutionsCombinationMode==QueryDecompositionResult.COMBINATION_JOIN){

			long maxSolutionsNum = -1;
			int maxSolutionsInd = -1;
			long maxOptSolutionsNum = -1;
			int maxOptSolutionsInd = -1;
			
			//find the solution set with most solutions to use as the ground one 
			for(int i=0; i<ontonuts.length; i++){
				if(this.qdr.mandatoryOntonutVars==null || this.qdr.mandatoryOntonutVars.get(i)==null ||
						this.qdr.mandatoryOntonutVars.get(i).isEmpty()){
					if(maxOptSolutionsNum < solutions.get(i).size()){
						maxOptSolutionsNum = solutions.get(i).size();
						maxOptSolutionsInd = i;
					}
				}
				else if(maxSolutionsNum < solutions.get(i).size()){
					maxSolutionsNum = solutions.get(i).size();
					maxSolutionsInd = i;
				}		
			}
			if(maxSolutionsInd==-1) maxSolutionsInd = maxOptSolutionsInd;

			resultSolutions = solutions.get(maxSolutionsInd);
			
			//System.out.println(resultSolutions);
			
			//Preprocess to speed up later comparisons:

			//Find variables that used for more than one ontonut, i.e. provide an implicit join condition
			HashSet<String> joiningVars = new HashSet<String>();
			for(int i=0; i<ontonuts.length; i++){
				Set<String> vars;
				if(this.qdr.notHandledOntonutVars!=null && this.qdr.notHandledOntonutVars.get(i)!=null){
					vars = new HashSet<String>(this.qdr.allOntonutVars.get(i));
					vars.addAll(this.qdr.notHandledOntonutVars.get(i));
				} else vars = this.qdr.allOntonutVars.get(i);

				for(String var : vars){
					for(int j=0; j<ontonuts.length; j++){
						if(i==j) continue;
						if(this.qdr.allOntonutVars.get(j).contains(var))
							joiningVars.add(var);
						if(this.qdr.notHandledOntonutVars!=null && this.qdr.notHandledOntonutVars.get(j)!=null 
								&& this.qdr.allOntonutVars.get(j).contains(var))
							joiningVars.add(var);
					}
				}
			}
			
			// Removing because cannot expect ontonuts to return the same URI for an object
			// such URI will likely to be just a blank node
			// so, will use only other joining variables (ids, property values, etc)
			if(this.qdr.rdfTypeVars!=null)
				joiningVars.removeAll(this.qdr.rdfTypeVars); 
			
			if(!joiningVars.isEmpty())
				if(PERFORMANCE_PRINT)
					print("Joining vars: " + joiningVars);

			//Preprocess explicit joining expressions
			Map<String,Integer> joinerToOntonut = new HashMap<String,Integer>(); //records for each joiner expression, to which ontonutIDs variables in its subject and object belong 
			Map<String,Map<String,String>> joinerStatementValues = new HashMap<String,Map<String,String>>(); //records for both subject and object of each joiner expression, the actual variable values for all solutions
			Map<String,Map<String,Double>> joinerStatementNumericValues = new HashMap<String,Map<String,Double>>();

			Map<String,Map<String,List<Map<String,String>>>> indices = new HashMap<String,Map<String,List<Map<String,String>>>>(); //records for both subject and object of each joiner expression, map of primaryKey variable values to all solutions indices with that value 
			HashMap<String,List<String>> joinerCode = new HashMap<String,List<String>>(); //to avoid compiling one expression more than once (relevant only to conmplex expressions with variables from several ontonutIDs)

			if(this.qdr.joinerStatements!=null){
				for(String joiner : this.qdr.joinerStatements.keySet()){
					String joinerS = "s"+joiner;
					SemanticStatement st = this.qdr.joinerStatements.get(joiner);
					
					//process subject
					if(!st.isSubjectVar()) joinerToOntonut.put(joinerS, -1);
					else{
						String var=st.subject.substring(1);
						boolean found = false;
						for(int i=0; i<ontonuts.length; i++){
							if(solutions.get(i).get(0).get(var)!=null){
								found = true;
								joinerToOntonut.put(joinerS,i);
								List<Map<String,String>> sols = solutions.get(i);

								Map<String,String> values = new HashMap<String,String>(sols.size()); 
								Map<String,Double> dvalues = new HashMap<String,Double>(sols.size()); 
								
								Map<String,List<Map<String,String>>> ind = new HashMap<String,List<Map<String,String>>>();
								for(int j=0; j<sols.size(); j++){
									Map<String, String> sol = sols.get(j);
									String id = sol.get("");
									String value = sol.get(var);
									values.put(id,value);
									try {
										Double dvalue = Double.valueOf(value);
										dvalues.put(id,dvalue);
									} catch (NumberFormatException e) {}
									
									List<Map<String,String>> vec = ind.get(value);
									if(vec==null){
										vec = new ArrayList<Map<String,String>>(1);
										ind.put(value,vec);
									}
									vec.add(sol);
								}
								joinerStatementValues.put(joinerS,values);
								joinerStatementNumericValues.put(joinerS,dvalues);
								indices.put(joinerS, ind);
								break;
							}
						}
						if(!found) {
							printToErr("Ontonuts: "+st.subject+" is not bound in any of result sets"); 
							return null;
						}
					}

					//process object
					boolean found = false;
					String errorMsg=null;
					for(int i=0; i<ontonuts.length; i++){
						StringBuilder errorMessage=new StringBuilder();
						List<String> code = ExpressionEvaluator.compile(st.object, st.object, errorMessage);
						if(code==null) { printToErr("Ontonuts: "+errorMessage); return null;}
						joinerCode.put(joiner,code);

						//evaluate for first solution
						errorMessage = new StringBuilder();
						String value = ExpressionEvaluator.evaluateExpression(code, this.ID, st.object, solutions.get(i).get(0), null, errorMessage);

						if(value!=null){
							found = true;
							joinerToOntonut.put(joiner,i);
							List<Map<String,String>> sols = solutions.get(i);
							Map<String,String> values = new HashMap<String,String>(sols.size());
							Map<String,Double> dvalues = new HashMap<String,Double>(sols.size()); 
							
							Map<String,List<Map<String,String>>> ind = new HashMap<String,List<Map<String,String>>>();
							values.put(solutions.get(i).get(0).get(""),value);
							List<Map<String,String>> vec = new ArrayList<Map<String,String>>(1);
							vec.add(sols.get(0));
							ind.put(value,vec);
							//evaluate for the rest of solutions
							for(int j=1; j<sols.size(); j++){
								Map<String, String> sol = sols.get(j);
								String id = sol.get("");

								errorMessage=new StringBuilder();
								value = ExpressionEvaluator.evaluateExpression(code, this.ID, st.object, solutions.get(i).get(j), null, errorMessage);
								values.put(id,value);
								try {
									Double dvalue = Double.valueOf(value);
									dvalues.put(id,dvalue);
								} catch (NumberFormatException e) {}

								vec = ind.get(value);
								if(vec==null){
									vec = new ArrayList<Map<String,String>>(1);
									ind.put(value,vec);
								}
								vec.add(sol);
							}
							joinerStatementValues.put(joiner,values);
							joinerStatementNumericValues.put(joiner,dvalues);
							indices.put(joiner, ind);
							break;
						}
						else{
							if(errorMsg==null) errorMsg = errorMessage.toString();
							else if(errorMsg!=null && !errorMsg.equals(errorMessage.toString())) errorMsg = null;
						}
					}
					if(!found) 
						if(errorMsg!=null) {printToErr("Ontonuts: "+st.object+" is not bound in any of result sets"); return null;}
						else joinerToOntonut.put(joiner,-1); // a complex expression using variables from more than one ontonut
				}
			}
			
			
//			SaplAgent.printMemory("In combine", false);

			//Do the actual JOIN operation

			//FIX 22.09.2017: If the base ontonut is Optional, add solution2 without any matches as is
			boolean isBaseOptional = this.qdr.mandatoryOntonutVars==null 
					|| this.qdr.mandatoryOntonutVars.get(maxSolutionsInd) == null || this.qdr.mandatoryOntonutVars.get(maxSolutionsInd).isEmpty();

			for(int i=0; i<ontonuts.length; i++){ //iterating all ontonutIDs except the central one - one with most solutions
				if(i==maxSolutionsInd) continue;

				boolean isOptional = this.qdr.mandatoryOntonutVars==null 
						|| this.qdr.mandatoryOntonutVars.get(i) == null || this.qdr.mandatoryOntonutVars.get(i).isEmpty();

				List<String> matchJoiners = new ArrayList<String>();
				List<String> matchJoinersS = new ArrayList<String>();
				List<String> complexJoiners = new ArrayList<String>(); //using expression with variables from different ontonutIDs
				List<String> complexJoinersS = new ArrayList<String>();
				boolean inSubject = false;
				if(this.qdr.joinerStatements!=null){
					for(String joiner : this.qdr.joinerStatements.keySet()){
						String joinerS = "s"+joiner;
						int subOntonut = joinerToOntonut.get(joinerS);
						int obOntonut = joinerToOntonut.get(joiner);
						if(obOntonut==-1){
							complexJoiners.add(joiner);
							complexJoinersS.add(joinerS);
						}
						else if((subOntonut==maxSolutionsInd && obOntonut==i && (inSubject=true)) || (subOntonut==i && obOntonut==maxSolutionsInd)){
							matchJoiners.add(joiner);
							matchJoinersS.add(joinerS);
						}
					}
				}
				
				List<Map<String,String>> newSolutions = new ArrayList<Map<String,String>>(resultSolutions.size());
				List<Map<String,String>> newSubSolutions = new ArrayList<Map<String,String>>();

//				SaplAgent.printMemory("In combine 1.5", false);

				Set<String> solutions2withMatch = null;
				if(isBaseOptional) solutions2withMatch = new HashSet<String>();
				
				for(Map<String,String> solution : resultSolutions){
					String solID = solution.get("");
					
					//boolean hasMatch = false;

					List<Map<String,String>> currOntonutSolutions;

					//for just one matching expression, use indices for speed
					if(matchJoiners.size()==1 && this.qdr.joinerStatements.get(matchJoiners.get(0)).predicate.equals(SaplConstants.eq)){
						String join = matchJoiners.get(0);
						String joinS = matchJoinersS.get(0);
						String joinI = inSubject? joinS : join;
						String val = joinerStatementValues.get(joinI).get(solID);
						currOntonutSolutions = indices.get(inSubject ? join : joinS).get(val);
					}
					else 
						currOntonutSolutions = solutions.get(i);
					
					newSubSolutions.clear();

					if(currOntonutSolutions!=null){
						int newSolCounter = 0;
						for(Map<String,String> solution2 : currOntonutSolutions){
							
							newSolCounter++;
							String sol2ID = solution2.get("");

							boolean ok=true;

							//first do variable-name based matching
							if(!joiningVars.isEmpty()){
								for(String var: joiningVars){
									String value1 = solution.get(var);
									String value2 = solution2.get(var);
									
									if(value1!=null && value2!=null && !value1.equals(value2)){  //no match
										ok = false; break;
									}
									if(this.qdr.mandatoryOntonutVars!=null){
										if(value1==null && this.qdr.mandatoryOntonutVars.get(i)!=null && this.qdr.mandatoryOntonutVars.get(i).contains(var) ||
												value2==null && this.qdr.mandatoryOntonutVars.get(maxSolutionsInd)!=null && this.qdr.mandatoryOntonutVars.get(maxSolutionsInd).contains(var)){
											ok = false; break;
										}
									}
								}
							}

							//then, check explicit joining expressions

							//if was just one joining expression, it is already handled above by indexing
							//otherwise check now
							if(ok){
								if(!matchJoiners.isEmpty() && !(matchJoiners.size()==1 && this.qdr.joinerStatements.get(matchJoiners.get(0)).predicate.equals(SaplConstants.eq))){
									//for(String joiner : matchJoiners){
									for(int j=0; j<matchJoiners.size(); j++){
										String joiner = matchJoiners.get(j);
										String joinerS = matchJoinersS.get(j);
										SemanticStatement st = this.qdr.joinerStatements.get(joiner);

										int subOnto = joinerToOntonut.get(joinerS);
										int obOnto = joinerToOntonut.get(joiner);

										String value1=null, value2=null;
										Double v1, v2;
										if(subOnto==maxSolutionsInd && obOnto==i){
											v1 = joinerStatementNumericValues.get(joinerS).get(solID);
											v2 = joinerStatementNumericValues.get(joiner).get(sol2ID);
											if(v1==null || v2==null){
												value1 = joinerStatementValues.get(joinerS).get(solID);
												value2 = joinerStatementValues.get(joiner).get(sol2ID);
											}
										}
										else if(subOnto==i && obOnto==maxSolutionsInd){
											v1 = joinerStatementNumericValues.get(joinerS).get(sol2ID);
											v2 = joinerStatementNumericValues.get(joiner).get(solID);
											if(v1==null || v2==null){
												value1 = joinerStatementValues.get(joinerS).get(sol2ID);
												value2 = joinerStatementValues.get(joiner).get(solID);
											}
										}
										else continue; 

										if(v1!=null && v2!=null){
											if(!SaplQueryEngine.checkNumericValue(v1,st.predicate,v2))
											{ok=false; break;}
										}
										else {
											if(!SaplQueryEngine.checkStringValue(value1,st.predicate,value2))
											{ok=false; break;}
										}
									}
								}
							}
							
							if(ok){
								for(int j=0; j<complexJoiners.size(); j++){
									String joiner = complexJoiners.get(j);
									String joinerS = complexJoinersS.get(j);
									SemanticStatement st = this.qdr.joinerStatements.get(joiner);
									int subOnto=joinerToOntonut.get(joinerS);
									String value1;
									if(subOnto==-1) value1=st.subject;
									else if(subOnto==maxSolutionsInd) value1 = joinerStatementValues.get(joinerS).get(solID);
									else if(subOnto==i) value1 = joinerStatementValues.get(joinerS).get(sol2ID);
									else continue;

									StringBuilder errorMessage = new StringBuilder();
									HashMap<String,String> join_solution = new HashMap<String,String>(solution);
									join_solution.putAll(solution2);

									String value2 = ExpressionEvaluator.evaluateExpression(joinerCode.get(joiner), this.ID, st.object, join_solution, null, errorMessage);
									if(value2!=null){
										if(!SaplQueryEngine.checkValue(value1,st.predicate,value2))
										{ok=false; break;}
									}
								}
							}

							if(ok){
								HashMap<String,String> new_solution = new HashMap<String,String>(solution);
								new_solution.putAll(solution2);
								new_solution.put("", solution.get("")+"."+newSolCounter);
								newSubSolutions.add(new_solution);
								
								if(isBaseOptional) solutions2withMatch.add(sol2ID);
								
								//hasMatch = true;
							}

						}
						if(!newSubSolutions.isEmpty()) newSolutions.addAll(newSubSolutions);
						else if(isOptional){
							newSolutions.add(solution);
						}
						
					}
					
					//if(!hasMatch) System.out.println("Has no match: " + solution);
				}
				
				if(isBaseOptional && solutions2withMatch.size() < solutions.get(i).size()){
					for(Map<String,String> solution2 : solutions.get(i)){
						if(!solutions2withMatch.contains(solution2.get("")))
							newSolutions.add(solution2);
					}
				}

				resultSolutions = newSolutions;
			}
			
//			SaplAgent.printMemory("In combine 2", false);

		}
		else { //UNION case
			resultSolutions = new ArrayList<Map<String,String>>();
			for(List<Map<String, String>> sols: solutions.values()){
				resultSolutions.addAll(sols);
			}
		}

		return resultSolutions;
	}

	boolean checkSolutionsViability(){
		//Check if ontonuts produced solution so that JOIN or UNION can be not empty
		if(this.qdr.ontonutsSolutionsCombinationMode==QueryDecompositionResult.COMBINATION_JOIN){
			int liveOntonutsN = ontonuts.length;
			for(int i=0; i<ontonuts.length; i++){
				List<Map<String,String>> ontonutSolutions = this.solutions.get(i);
				if(this.qdr.mandatoryOntonutVars!=null && this.qdr.mandatoryOntonutVars.get(i)!=null &&
						!this.qdr.mandatoryOntonutVars.get(i).isEmpty() && ontonutSolutions.isEmpty()){ 
					return false;
				}
				if(ontonutSolutions.isEmpty()) liveOntonutsN--;
			}
			
			//CHANGE 14.03.2017 (+ counting liveOntonutsN above)
			if(liveOntonutsN < 2) this.qdr.ontonutsSolutionsCombinationMode = QueryDecompositionResult.COMBINATION_UNION;
	
			return true;
		}
		else{
			for(List<Map<String,String>> ontonutSolutions : this.solutions.values()){
				if(!ontonutSolutions.isEmpty()){
					return true;
				}
			}
	
			return false;
		}
	}
	
	@Override
	public void doAction() throws Throwable{
		try {
			long start = System.currentTimeMillis();
			long s = start;
			boolean notEmpty = true;
			boolean notEmptyOnlyBecauseOfError = false;
	
			//LEFT SIDE OF THE RULE
	
			SaplQueryResultSet queryRS;
			if(this.ontonutIDs==null || this.ontonutIDs.size()==0){
				if(PERFORMANCE_PRINT) print("Running query locally");
				queryRS = this.hasBeliefs(this.rule_if, SaplConstants.G);
				s = System.currentTimeMillis();
				if(queryRS==null){
					notEmpty = false;
					if(PERFORMANCE_PRINT)
						print("Finally: 0 solutions ["+(s-start)+"]");
				}
				else{
					if(PERFORMANCE_PRINT)
						print("Finally: "+queryRS.getNumberOfSolutions()+" solutions ["+(s-start)+"]");
				}
			}
			else{
	
				if(!this.initializeOntonuts()) return; 
				
				//unwrap the query's from s:All etc.
				queryRS = new SaplQueryResultSet(); //will contain only wrappings now, solutions will add later
				String cntUnwrappedIf = this.unwrapQuery(this.rule_if, queryRS, true);
	
				if(!this.decomposeQuery(cntUnwrappedIf)) notEmpty = false;
				
	//			SaplAgent.printMemory("After decompose", true);
				
				if(notEmpty){
					this.feedOntonutsSelectionStatements();
		
					this.feedOntonutsSpecialStatements();
	
	//				SaplAgent.printMemory("After feed", true);
		
					if(DEBUG_PRINT){
						print("Data items:: " + this.dataItems);
						print("Query items:: " + this.qdr.queryReferencedDataItems);
						print("All query vars:: " + this.qdr.allQueryVars);
						print("Local starter vars:: " + this.qdr.localStarterVars);
						print("All ontonut handled vars:: " + this.qdr.allOntonutVars);
						print("All ontonut not-handled vars:: " + this.qdr.notHandledOntonutVars);
						print("Mandatory ontonut vars:: " + this.qdr.mandatoryOntonutVars);
						print("rdf:type vars:: "+ this.qdr.rdfTypeVars);
						print("Query objects:: " + this.qdr.objectsInQuery);
						print("Ontonut query object:: " + this.qdr.ontonutObjects);
						print("IDs of query objects:: " + this.qdr.objectsIDs);
						print("Covered selection statements::");
						if(this.qdr.coveredSelectionStatements!=null){
							for(int i=0; i<ontonuts.length; i++){
								if(this.qdr.coveredSelectionStatements.get(i)!=null && !this.qdr.coveredSelectionStatements.get(i).isEmpty()){
									print(ontonuts[i].id+"::\n"+this.produceN3(this.qdr.coveredSelectionStatements.get(i).keySet()));
								}
							}
						}
					}
	
					if(DEBUG_PRINT){
						print("Local starters:: " + (this.qdr.localStarters==null ? "" : this.produceN3(this.qdr.localStarters.keySet())));
						print("Covered special statements::");
						if(this.qdr.coveredSpecialStatements!=null){
							for(int i=0; i<ontonuts.length; i++){
								if(this.qdr.coveredSpecialStatements.get(i)!=null && !this.qdr.coveredSpecialStatements.get(i).isEmpty()){
									print(ontonuts[i].id+"::\n"+this.produceN3(this.qdr.coveredSpecialStatements.get(i).keySet()));
								}
							}
						}
						print("Joiner statements:: " + (this.qdr.joinerStatements==null ? "" : this.produceN3(this.qdr.joinerStatements.keySet())));
						print("Have to be local (ontonuts cannot handle)::"); 
						if(this.qdr.localOntonutCannotHandle!=null){
							for(int i=0; i<ontonuts.length; i++){
								if(this.qdr.localOntonutCannotHandle.get(i)!=null && !this.qdr.localOntonutCannotHandle.get(i).isEmpty()){
									print(ontonuts[i].id+"::\n"+this.produceN3(this.qdr.localOntonutCannotHandle.get(i).keySet()));
								}
							}
						}
						print("Local joiners:: " + (this.qdr.localJoiners==null ? "" : this.produceN3(this.qdr.localJoiners.keySet())));
						print("Local finalizers:: " + (this.qdr.localFinalizers==null ? "" : this.produceN3(this.qdr.localFinalizers.keySet())));
					}
	
				}
				
				s = System.currentTimeMillis();
				if(PERFORMANCE_PRINT)
					print("Ontonut definitions processed ["+(s-start)+"]");
	
				SaplQueryResultSet initialLocalRS = new SaplQueryResultSet();
	
				if(notEmpty && this.qdr.localStarters!=null && !this.qdr.localStarters.isEmpty()){
	
					notEmpty = this.hasBeliefs(new ArrayList<String>(this.qdr.localStarters.keySet()), SaplConstants.G, initialLocalRS);			
	
					if(notEmpty && this.qdr.localOntonutCannotHandle!=null){
						Map<String,SemanticStatement> toCheck = new HashMap<String,SemanticStatement>();
						for(int i=0; i<ontonuts.length; i++){
							Map<String, SemanticStatement> cannot = this.qdr.localOntonutCannotHandle.get(i);
							if(cannot!=null)
								toCheck.putAll(cannot);
						}
	
						for(String cannotID: toCheck.keySet()){
							SemanticStatement cannotSt = toCheck.get(cannotID);
	
							if(SaplConstants.embedded_predicates.contains(cannotSt.predicate) && 
									!SaplConstants.aggregate_predicates.contains(cannotSt.predicate)){
								Set<String> vars = new HashSet<String>();
								this.findAllVarsInStatement(cannotSt, vars, null);
								
								if(initialLocalRS.getDefinedVars().containsAll(vars)){
									
									if(DEBUG_PRINT) 
										print("Checking additionally as local:: " + cannotSt);
									notEmpty = this.hasBeliefs(cannotID, SaplConstants.G, initialLocalRS);
									if(!notEmpty){
										break;
									}
								}
							}
						}
					}
					if(notEmpty){
						if(PERFORMANCE_PRINT)
							print("Obtained "+initialLocalRS.getNumberOfSolutions()+" initial local solutions  ["+(System.currentTimeMillis()-s)+"]");
						if(DEBUG_SOLUTIONS_PRINT)
							print("Initial local solutions::"+initialLocalRS);
					}
					else{
						if(PERFORMANCE_PRINT){
							print("Obtained 0 initial local solutions ["+(System.currentTimeMillis()-s)+"]");
							print("Finally: 0 solutions");
						}
					}
					s = System.currentTimeMillis();
				}
				
	//			SaplAgent.printMemory("After local", true);
	
				if(notEmpty){
	
					this.executeOntonutQueries(initialLocalRS);
	
	//				SaplAgent.printMemory("After execute", true);
					
					if(PERFORMANCE_PRINT)
						print("Ontonuts executed ["+(System.currentTimeMillis()-s)+"]");
					
					s = System.currentTimeMillis();
					
	
					if(DEBUG_SOLUTIONS_PRINT)
						print("Extended and fixed solutions from all ontonuts before combination:: "+solutions);
					
	//				SaplAgent.printMemory("After fix", true);
					
					notEmpty = checkSolutionsViability();
					
					List<Map<String,String>> resultSolutions;
	
					//If several ontonutIDs were involved, join results based on specified join conditions  
					if(notEmpty){
						if(ontonuts.length>1){
							long ss = System.currentTimeMillis();
							resultSolutions = this.combineOntonutResults();
							if(PERFORMANCE_PRINT)
								print((this.qdr.ontonutsSolutionsCombinationMode==QueryDecompositionResult.COMBINATION_JOIN?"JOIN":"UNION")+" performed ["+(System.currentTimeMillis()-ss)+"]");
							if(resultSolutions==null){
								resultSolutions = new ArrayList<Map<String,String>>();
								notEmpty = false;
							}
						}
						else resultSolutions = solutions.get(0);				
					}
					else resultSolutions = new ArrayList<Map<String, String>>();
					
	//				SaplAgent.printMemory("After combine", true);
	
					if(PERFORMANCE_PRINT)
						print("After ontonuts, extending, fixing, and joining: "+resultSolutions.size()+" solutions ["+(System.currentTimeMillis()-s)+"]");
					s = System.currentTimeMillis();
	
					if(DEBUG_SOLUTIONS_PRINT)
						print("Solutions after ontonuts::"+resultSolutions);
	
					Set<String> allOntonutCoveredQueryVars = new HashSet<String>();
					if (this.qdr.allOntonutVars!=null){
						for(int o = 0; o < ontonuts.length; o++){
							Set<String> vars = this.qdr.allOntonutVars.get(o);
							if(vars!=null) allOntonutCoveredQueryVars.addAll(vars);
						}
					}
	
					if(initialLocalRS.getNumberOfSolutions()>0){
						this.qdr.joinerStatements = this.qdr.localJoiners;
						
						Set<String> ontoMandatoryVars = new HashSet<String>();
	
						if (this.qdr.mandatoryOntonutVars!=null){
							for(int o = 0; o < ontonuts.length; o++){
								Set<String> vars = this.qdr.mandatoryOntonutVars.get(o);
								if(vars!=null) ontoMandatoryVars.addAll(vars);
							}
						}
						
						if(notEmpty || ontoMandatoryVars.isEmpty()){
							notEmpty = true;
							Set<String> localVars = initialLocalRS.getDefinedVars();
							Set<String> localMandatoryVars = initialLocalRS.getSolution(0).keySet();
							localMandatoryVars.remove("");
							
							ontonuts = new Ontonut[2];
		
							this.qdr.allOntonutVars = new HashMap<Integer, Set<String>>();
							this.qdr.allOntonutVars.put(0,allOntonutCoveredQueryVars);
							this.qdr.allOntonutVars.put(1,localVars);						
							this.qdr.mandatoryOntonutVars = new HashMap<Integer, Set<String>>();
							this.qdr.mandatoryOntonutVars.put(0,ontoMandatoryVars);
							this.qdr.mandatoryOntonutVars.put(1,localMandatoryVars);
		
							List<Map<String,String>> localSols = new ArrayList<Map<String,String>>(); 
							for(int i=0;i<initialLocalRS.getNumberOfSolutions();i++)
								localSols.add(initialLocalRS.getSolution(i));
							solutions.clear();
							solutions.put(0,resultSolutions);
							solutions.put(1,localSols);
							
							this.qdr.ontonutsSolutionsCombinationMode = QueryDecompositionResult.COMBINATION_JOIN;
		
							resultSolutions = this.combineOntonutResults();
							if(resultSolutions==null){
								resultSolutions = new ArrayList<Map<String,String>>();
								notEmpty = false;
							}
							
							notEmpty = !resultSolutions.isEmpty();
		
							if(PERFORMANCE_PRINT)
								print("After join with initial local solutions: "+resultSolutions.size()+" solutions ["+(System.currentTimeMillis()-s)+"]");
		
							if(DEBUG_SOLUTIONS_PRINT)
								print("Solutions after join with initial::"+resultSolutions);
							
							s = System.currentTimeMillis();
						}
					}
	
					allOntonutCoveredQueryVars.add("ontonutErrorSource");
					allOntonutCoveredQueryVars.add("ontonutErrorMessage");
					allOntonutCoveredQueryVars.add("ontonutErrorTrace");
	
					queryRS.setSolutions(resultSolutions, allOntonutCoveredQueryVars);
					
	//				SaplAgent.printMemory("Before final", true);
	
					if(notEmpty && this.qdr.localFinalizers!=null && !this.qdr.localFinalizers.isEmpty()){
						//Processing statements to be handed by S-APL query engine
						if(PERFORMANCE_PRINT)
							print("Processing local finalizing statements"+ (DEBUG_PRINT? ": "+this.qdr.localFinalizers.values() : ""));
	
						boolean result = this.hasBeliefs(new ArrayList<String>(this.qdr.localFinalizers.keySet()), SaplConstants.G, queryRS);
						if(result==false){
							notEmpty = false;
							
						}
					}
	
					if(PERFORMANCE_PRINT)
						print("Finally: "+queryRS.getNumberOfSolutions()+" solutions ["+(System.currentTimeMillis()-s)+"]");
	
					if(!errorSolutions.isEmpty()){
						queryRS.addSolutions(errorSolutions);
						if(!notEmpty){
							notEmpty = true;
							notEmptyOnlyBecauseOfError = true;
						}
					}
	
					if(DEBUG_SOLUTIONS_PRINT)
						print("Final solutions::"+queryRS);
				}
				s = System.currentTimeMillis();
			}
	
			//RIGHT SIDE OF THE RULE
	
			if(notEmpty){
				if(notEmptyOnlyBecauseOfError || this.setterOntonutIDs==null || this.setterOntonutIDs.size()==0){
					
					this.addBeliefsAsRuleOutput(this.rule_then, queryRS);
					if(PERFORMANCE_PRINT){
						long now = System.currentTimeMillis();
						print("Rule conclusions added ["+(now-s)+"]");
						print("Total time: [" + (now - start) + "]");
					}
					
	//				SaplAgent.printMemory("End", true);
	
					this.wakeReasoner();
				}
				else{
					List<String> ids = new ArrayList<String>();
					List<SemanticStatement> contextMembers = this.getStatements(this.rule_then, ids);
					
					List<String> toUnwrap = new ArrayList<String>();
					List<String> simple = new ArrayList<String>();
	
					for(int i=0; i<contextMembers.size(); i++ ){
						SemanticStatement st = contextMembers.get(i);
						if (st.isSubjectContext()
								&& (SaplConstants.set_predicates.contains(st.predicate) || SaplConstants.set2_predicates.contains(st.predicate))) {
							
							toUnwrap.add(ids.get(i));
						} 
						else {
							simple.add(ids.get(i));
						}
					}
					
					String tempContextID = null;
					
					if(toUnwrap.isEmpty()) toUnwrap.add(this.rule_then);
					else if(!simple.isEmpty()){
						tempContextID = this.addBeliefsN3ToTempContext(this.produceN3(simple));
						toUnwrap.add(tempContextID);
					}
					
					for(String toUwrapSt : toUnwrap){
	
						SaplQueryResultSet queryRSCopy = new SaplQueryResultSet(queryRS);
						
						//unwrap right side, adding solution set modifiers to those defined for the left side
						String cntUnwrappedThen = this.unwrapQuery(toUwrapSt, queryRSCopy, true);
						
						this.ontonutIDs = new HashSet<String>();
						for(String ontonutID : this.setterOntonutIDs){
							String query = "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
							query += "<"+ontonutID +">" +" o:setPattern ?pattern ";
							SaplQueryResultSet resultSet = this.hasBeliefsN3(query);
							Map<String, String> def = resultSet.getSolution(0);
							String cntPattern = def.get("pattern");
	
							SaplQueryResultSet rs = this.hasBeliefs(cntPattern, cntUnwrappedThen);
							if(rs!=null) this.ontonutIDs.add(ontonutID);
						}
						
						if(!this.initializeOntonuts()) return;
						
						this.decomposeQuery(cntUnwrappedThen);
						
						this.feedOntonutsSelectionStatements();
		
						if(DEBUG_PRINT){
							print("Data items (update):: " + dataItems);
							print("Update items:: " + this.qdr.queryReferencedDataItems);
							print("Covered update statements::");
							if(this.qdr.coveredSelectionStatements!=null){
								for(int i=0; i<ontonuts.length; i++){
									if(!this.qdr.coveredSelectionStatements.get(i).isEmpty()){
										print(ontonuts[i].id+"::\n"+this.produceN3(this.qdr.coveredSelectionStatements.get(i).keySet()));
									}
								}
							}
							print("Local starters:: " + (this.qdr.localStarters==null ? "" : this.produceN3(this.qdr.localStarters.keySet())));
							print("Local finalizers:: " + (this.qdr.localFinalizers==null ? "" : this.produceN3(this.qdr.localFinalizers.keySet())));
						}
						
						if(PERFORMANCE_PRINT)
							print("Update ontonut definitions processed ["+(System.currentTimeMillis()-s)+"]");
						s = System.currentTimeMillis();
		
						List<Map<String, String>> updates = queryRSCopy.getSolutions();
						
						for(int i=0; i<ontonuts.length; i++){
							try{
								this.ontonuts[i].executeUpdate(updates);
							}
							catch(Throwable e){
								printError(this.ontonuts[i].shortId+" ONTONUT ERROR!");
								printException(e, EXCEPTION_FULL_STACK_PRINT);
								
								//change on 08.05.2019
								this.setFailed();
							}
						}
		
						//s = System.currentTimeMillis();
						
						if(this.qdr.localFinalizers!=null){
							if(this.qdr.localStarters==null) this.qdr.localStarters=this.qdr.localFinalizers;
							else this.qdr.localStarters.putAll(this.qdr.localFinalizers);
						}
						
						if(this.qdr.localStarters!=null && this.qdr.localStarters.size()>0){
							//Processing statements to be handed by S-APL query engine
							if(PERFORMANCE_PRINT)
								print("Adding locally: "+ this.qdr.localStarters.values());
		
							this.addBeliefsAsRuleOutput(new ArrayList<String>(this.qdr.localStarters.keySet()), queryRSCopy);
						}
					
					}
					
					//FIX 01.06.2018: Temporary contexts made persistent, need to clean up 
					if(tempContextID!=null) this.eraseBeliefsByID(tempContextID);
	
					long now = System.currentTimeMillis();
					if(PERFORMANCE_PRINT){
						print("Rule conclusions added ["+(now-s)+"]");
						print("Total time: [" + (now - start) + "]");
					}
					this.wakeReasoner();
				}
	
			}
			else{
				if(this.rule_else!=null && this.rule_else.startsWith(SaplConstants.CONTEXT_PREFIX)){
					this.addBeliefs(this.rule_else, null);
					if(PERFORMANCE_PRINT){
						long now = System.currentTimeMillis();
						print("Rule conclusions added ["+(now-s)+"]");
						print("Total time: [" + (now - start) + "]");
					}
					this.wakeReasoner();
				}
				else if(PERFORMANCE_PRINT){
					print("Total time: [" + (System.currentTimeMillis() - start) + "]");
				}
			}
		
		} catch (Throwable e){
			printToErr("ERROR in UniversalAdapterBehavior!");
			printToErr("Getters: "+ontonutIDs);
			printToErr("Setters: "+setterOntonutIDs);
			throw e;
		}
	}

	protected static String getExceptionMeaning(String exceptionMessage, String exceptionTrace) {
		if(exceptionMessage == null) return "Service error";
		String lower = exceptionMessage.toLowerCase();
		if(lower.contains("bad response")) return "Service error";
		else if(lower.contains("timed out")) return "Service unreacheable";
		else if(lower.contains("refused")) return "Service unreacheable";
		else if(exceptionTrace.contains("UnknownHostException")) return "Service address is unknown";
		else if(exceptionTrace.contains("FileNotFoundException")) return "Service did not recognize the request";
		else if(lower.contains("bad request")) return "Service reported bad request";
		else if(lower.contains("postcondition")) return "Postcondition check failed";
		else return exceptionMessage;
	}

}
