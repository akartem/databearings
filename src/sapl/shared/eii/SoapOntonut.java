package sapl.shared.eii;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Node;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.util.SoapTools;

/** 
 * Ontonut representing a SOAP Web Service
 * 
 * @author Artem Katasonov (VTT + Elisa from July 2018) 
 * 
 * @version 4.7 (14.02.2019)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class SoapOntonut extends XmlOntonut {
	protected String version;
	protected String action;

	protected boolean passwordDigest;
	
	@Override
	public boolean loadDefinition() {
		
		if(!super.loadDefinition())
			return false;
		
		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "<"+this.id+">" + " o:action ?action. {<"+this.id+">" + " o:version ?version} s:is s:Optional. {<"+this.id+"> o:passwordDigest ?digest} s:is s:Optional"; 
		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);
		if(resultSet.getNumberOfSolutions()==0){
			this.parent.printError(id+": Ontonut is not well defined"); return false;
		}		

		Map<String, String> def = resultSet.getSolution(0);

		this.version = def.get("version");
		if(this.version==null) this.version = "1.2";
		this.action = def.get("action");

		String digest = def.get("digest");
		if(digest!=null && digest.equals("true")) this.passwordDigest = true;
		else  this.passwordDigest = false;

		return true;		
	}

	protected Node sendSOAP(String request, String body, String reqUsername, String reqPassword, int subindex) throws Exception{
		long s = System.currentTimeMillis();

		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			print("Sending "+body+" to "+request, subindex);

		SOAPMessage soap = SoapTools.createSOAP(this.version, request, this.action, body, reqUsername, reqPassword, this.passwordDigest);

		if(INTERACTION_DETAILS_PRINT){
			ByteArrayOutputStream out = new ByteArrayOutputStream(); 
			try {
				soap.writeTo(out);
				String result = new String( out.toByteArray(), "UTF-8");
				System.out.println(result);		
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Request created [" + (System.currentTimeMillis() - s) + "]", subindex);
		
		s = System.currentTimeMillis();

		int readTimeout = SoapTools.DEFAULT_READ_TIMEOUT;
		if(this.timeout!=null){
			try {
				readTimeout = Integer.valueOf(this.timeout).intValue();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		SOAPMessage response = SoapTools.sendSOAP(request, soap, readTimeout);
		
		String result="";
		ByteArrayOutputStream out2 = new ByteArrayOutputStream(); 
		try {
			response.writeTo(out2);
			result = new String( out2.toByteArray(), "UTF-8");
			if(INTERACTION_DETAILS_PRINT){
				System.out.println(result);		
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		SOAPBody soapBody = response.getSOAPBody();
		Node responseBody = soapBody.getChildNodes().item(0); 
		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Response received, length: "+result.length()+" chars [" + (System.currentTimeMillis() - s) + "]", subindex);
		s = System.currentTimeMillis();

		if(responseBody.getLocalName().equals("Fault")){
			printToErr(result, subindex);
			throw new Exception("Service returned Fault");
		}		
		
		this.lastContent = soapBody;
		
		return responseBody;
		
	}
	
	@Override
	protected List<Map<String, String>> runSingleQuery(String request, String body, String reqUsername, String reqPassword, int subindex, Map<String, String> localSol) throws Exception {

		Node responseBody = this.sendSOAP(request, body, reqUsername, reqPassword, subindex);
		
		dataItemsToSelect.remove("");
		if(this.dataItemsToSelect.isEmpty()) return null;

		long s = System.currentTimeMillis();
		
		List<Map<String,String>> sols = this.processResponseNode(responseBody,"");
		if(sols==null) sols = new ArrayList<Map<String,String>>();

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Processed "+sols.size()+" results ["+(System.currentTimeMillis()-s)+"]", subindex);

		return sols;
	}
	
	@Override
	protected Node runUpdate(List<Map<String, String>> updates) throws Exception{
		String content = this.getUpdateContent(updates);
		
		String updateURL = this.uri;
		 Map<String, String> sol = updates.get(0);
		 for(String var : sol.keySet()){
			String replace = "%%"+var+"%%";
			 if(updateURL.contains(replace))
				 updateURL = updateURL.replace(replace, sol.get(var));
		 }
		
		Node responseBody = this.sendSOAP(updateURL, content, this.username, this.password, 1);
		return responseBody;
	}
	
	//Support of IDE 

	@Override
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables) throws Throwable{
		this.addVarToItemsToSelect("", "");
		executeQuery(new SaplQueryResultSet(),null);
		
		String result = "";
		result += "* d:tree {\n";
		if(this.lastContent!=null){
			Node doc = (Node) this.lastContent;
			Map<String,Integer> vars = new HashMap<String,Integer>();
			String ns = this.uri+"#";
			result += processSyntaxExampleNode(doc,1,vars,false,null,includeEntities,fillVariables,ns);
		}
		result += "} .";
		return result;
	}		

}
