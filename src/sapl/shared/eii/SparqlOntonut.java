package sapl.shared.eii;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;

/** 
 * Ontonut representing a (very simple) SPARQL endpoint
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (13.03.2017)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2017, VTT
 * 
 * */

public class SparqlOntonut extends Ontonut {

	protected String service;
	protected String username;
	protected String password;
	
	protected String query;
	
	@Override
	public boolean loadDefinition() {
		super.loadDefinition();
		
		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "<"+this.id+"> o:service ?service. "
				+ "{<"+this.id+"> o:username ?username; o:password ?password} s:is s:Optional";  
		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);
		if(resultSet==null || resultSet.getNumberOfSolutions()==0){
			this.parent.printError(id+": Ontonut is not well defined"); return false;
		}		

		Map<String, String> def = resultSet.getSolution(0);

		this.service = def.get("service");
		this.username = def.get("username");
		this.password = def.get("password");

		return true;		
	}
	
	@Override
	/**Result will map variables used in the semantics definition to path of JSON elements*/
	public void findDataItems(String contextID, Map<String, String> result) {

		List<SemanticStatement> statements = this.parent.getStatements(contextID);

		for (SemanticStatement st: statements){
			if(st.isSubjectVar()) result.put(st.subject, st.getSubjectVariableName());
			if(st.isPredicateVar()) result.put(st.predicate, st.getPredicateVariableName());
			if(st.isObjectVar()) result.put(st.object, st.getObjectVariableName());
		}
		
		SaplN3ProducerOptions options = new SaplN3ProducerOptions(false, false);
		this.query = this.parent.produceN3(contextID, options);
	}
	
	@Override
	protected boolean isSyntaxStatement(SemanticStatement st){
		//unlike for other ontonuts, any statement goes
		return true;
	}

	@Override
	protected List<Map<String,String>> runQuery(SaplQueryResultSet localSolutions,List<Map<String,String>> ontonutErrorSolutions) throws Exception{

		long s = System.currentTimeMillis();
		
		String request = getQuerySparql(); 

		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			this.parent.print(id+": Sending "+request);
		
		request = "query=" + URLEncoder.encode(request, "UTF-8");

		HttpOptions options = HttpOptions.defaultOptions();
		options.method = "POST";
		options.content = request;
		options.username = username;
		options.password = password;
		options.accept = "application/sparql-results+json";
		
		String response = HttpTools.fetch(new URL(this.service), options);

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			this.parent.print(id+": Response received [" + (System.currentTimeMillis() - s) + "]");
		s = System.currentTimeMillis();
		
		// Parse response JSON
		JSONObject result = new JSONObject(response);
		JSONArray bindings = (JSONArray) ((JSONObject) result.get("results")).get("bindings");
		List<Map<String, String>> answers = new ArrayList<Map<String, String>>();
		if ( bindings.length() > 0 ) {
			for (int i = 0; i < bindings.length(); i++) {
				Map<String, String> answer = new HashMap<String, String>();
				JSONObject binding = (JSONObject) bindings.get(i);
				Iterator<?> it = binding.keys();
				while (it.hasNext()) {
					String key = (String) it.next();
					String var = this.dataItemsToSelect.get(key.toUpperCase());
					if(var!=null){
						JSONObject value = (JSONObject) binding.get(key);
						answer.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), String.valueOf(value.get("value")));
					}
				}
				answers.add(answer);
			}
		}

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			this.parent.print(id+": Processed "+answers.size()+" results ["+(System.currentTimeMillis()-s)+"]");
		return answers;
	}

	protected String getQuerySparql(){
		return "SELECT DISTINCT * WHERE { "+this.query+" }";
	}
	
	protected String runUpdate(List<Map<String, String>> updates) throws Exception{
		long s = System.currentTimeMillis();
		
		String request = getInsertSparql(updates); 

		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			this.parent.print(id+": Sending "+request);
		
		request = "update=" + URLEncoder.encode(request, "UTF-8");
		
		HttpOptions options = HttpOptions.defaultOptions();
		options.method = "POST";
		options.content = request;
		options.username = username;
		options.password = password;
		options.accept = "application/sparql-results+json";		

		String response = HttpTools.fetch(new URL(this.service), options);

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			this.parent.print(id+": Response received: "+response+" [" + (System.currentTimeMillis() - s) + "]");
		
		return response;
	}
	
	protected String getInsertSparql(List<Map<String, String>> updates){
		
		StringBuilder insertData = new StringBuilder(); 
		for(Map<String, String> update : updates){
			String insert = this.query;
			for(String key: dataItemsToSelect.keySet()){
				String var = dataItemsToSelect.get(key);
				String value;
				if(var.startsWith(SaplConstants.VARIABLE_PREFIX)){
					value = update.get(var.substring(SaplConstants.VARIABLE_PREFIX.length()));
					if(SemanticStatement.isURI(value))
						value = "<"+value+">";
					else value = "\""+value+"\"";
				} 
				else value = var;
				
				insert = insert.replace(SaplConstants.VARIABLE_PREFIX+key.toLowerCase(), value);
			}
			if(insertData.length()>0) insertData.append(" . ");
			insertData.append(insert);
		}
		
		return "INSERT DATA { "+insertData.toString()+" }";
	}

}
