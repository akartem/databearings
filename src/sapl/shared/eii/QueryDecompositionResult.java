package sapl.shared.eii;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import sapl.core.SemanticStatement;

/** 
 * Representation of a query execution plan, produced by QueryDecomposer
 *  
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (19.01.2017)
 * 
 * @since 4.2
 * 
 * Copyright (C) 2014-2017, VTT
 * 
 * */

public class QueryDecompositionResult {

	/**The mode in which sub-query results from ontonuts are combined: JOIN or UNION
		Not sure if even possible to cover all possible cases; right now doing either JOIN or UNION
		JOIN is default, but switching to simple UNION if all non-optional query statements
		are covered by more than one ontonut */
	protected static int COMBINATION_JOIN = 0;
	protected static int COMBINATION_UNION = 1;
	protected int ontonutsSolutionsCombinationMode = COMBINATION_JOIN; 

	protected boolean hasMandatory = false;
	protected boolean allMandatoryCoveredByMoreThanOneOntonut = true;


	/**All query selection statements covered by each ontonut*/ 
	protected Map<Integer, Map<String,SemanticStatement>> coveredSelectionStatements;

	/**All query filter statements covered by each ontonut*/ 
	protected Map<Integer, Map<String,SemanticStatement>> coveredSpecialStatements;

	/**IDs of optional query selection statements covered by each ontonut*/ 
	protected Map<Integer, Set<String>> optionalCoveredStatementIDs; 

	protected Map<Integer, Set<String>> topLevelCoveredStatementIDs; 


	/**Query statements not matching any ontonuts, thus to be run on local facts base.
	 * Run before ontonuts to get initial local solutions and after to get final solutions*/
	protected Map<String, SemanticStatement> localStarters;

	/**Query filter statements that require a combination of ontonut and local data*/
	protected Map<String, SemanticStatement> localJoiners;

	/**Query statements that require data from several ontonuts or a combination of ontonut and local data**/
	protected Map<String, SemanticStatement> localFinalizers;

	/**Query statements that match some ontonuts but that ontonut reporting that cannot handle, thus to be applied locally afterwards.
	 * Also s:expression and s:or referring to variables from >1 ontonut go here.
	 * Run only after ontonuts*/
	protected Map<Integer, Map<String, SemanticStatement>> localOntonutCannotHandle;

	/**All statements referring to variables from different ontonuts*/
	protected Map<String,SemanticStatement> joinerStatements;



	/**Query variables used in <var> a <Class> query statements*/
	protected Set<String> rdfTypeVars;

	protected Map<String, Set<String>> objectsInQuery;

	protected Map<String, String> objectsIDs;

	protected Map<Integer, Set<String>> ontonutObjects;

	protected Set<String> negativeObjectsIDs;


	//Various structures used in analysis of query and ontonuts semantics, for each ontonut

	/**Map of correspondences between data source items and query variables or constants*/
	protected Map<Integer,Map<String,Set<String>>> queryReferencedDataItems;

	/**For query constants, their datatypes if given*/
	protected Map<Integer,Map<String,String>> queryReferencedDataItemsConstantDatatypes; 

	/**All query variables*/
	protected Set<String> allQueryVars;

	/**Query variables defined for each ontonut sub-query (only in handled statements)*/
	protected Map<Integer,Set<String>> allOntonutVars; 

	/**Subsets of allOntonutVars which are mandatory, i.e. not inside {} s:is s:Optional*/
	protected Map<Integer,Set<String>> mandatoryOntonutVars;

	/**Vars defined via aggregators and expressions that ontonuts themselves cannot produce.
	 * Known only after Universal Adapter's feedSpecialStatements, not to Query Decomposer*/
	protected Map<Integer,Set<String>> notHandledOntonutVars;

	/**Vars appearing  in both ontonut-covered and not covered statements*/
	protected Set<String> localStarterVars;


	//s:or structures flattened
	protected Map<String, Set<SemanticStatement>> filtersWithinOr;

	/**CHANGE 20.01.2016: All possible mappings between query variables and ontonut-internal variables (saving from QueryDecomposer.analyseOntonutCoveredStatements)*/
	protected Map<Integer, Set<Map<String,String>>> variableMappingOptions;
	
	public QueryDecompositionResult(){}

	public void addCoveredVar(int ontonut, String var) {
		if(allQueryVars==null) allQueryVars = new HashSet<String>();
		allQueryVars.add(var);		
		if(allOntonutVars==null) allOntonutVars = new HashMap<Integer,Set<String>>();
		Set<String> ontonutVars = allOntonutVars.get(ontonut);
		if(ontonutVars==null){
			ontonutVars = new HashSet<String>();
			allOntonutVars.put(ontonut, ontonutVars);
		}
		ontonutVars.add(var);
	}

	public void addNotCoveredVar(String var) {
		if(allQueryVars==null) allQueryVars = new HashSet<String>();
		allQueryVars.add(var);		
	}

	public void addOntonutMandatoryVar(int ontonut, String var) {
		if(mandatoryOntonutVars==null) mandatoryOntonutVars = new HashMap<Integer,Set<String>>();
		Set<String> ontonutVars = mandatoryOntonutVars.get(ontonut);
		if(ontonutVars==null){
			ontonutVars = new HashSet<String>();
			mandatoryOntonutVars.put(ontonut, ontonutVars);
		}
		ontonutVars.add(var);
	}

	public void addOntonutNotHandledVar(int ontonut, String var) {
		if(notHandledOntonutVars==null) notHandledOntonutVars = new HashMap<Integer,Set<String>>();
		Set<String> ontonutVars = notHandledOntonutVars.get(ontonut);
		if(ontonutVars==null){
			ontonutVars = new HashSet<String>();
			notHandledOntonutVars.put(ontonut, ontonutVars);
		}
		ontonutVars.add(var);
	}

	public void addRdfTypeVar(String var) {
		if(rdfTypeVars==null) rdfTypeVars = new HashSet<String>();
		rdfTypeVars.add(var);		
	}

	public void addLocalStarterVar(String var) {
		if(localStarterVars==null) localStarterVars = new HashSet<String>();
		localStarterVars.add(var);		
	}

	public void addQueryObject(String prefixedVar, Set<Integer> coveringOntonuts) {
		if(objectsInQuery==null) objectsInQuery = new HashMap<String, Set<String>>(2);
		if(ontonutObjects==null) ontonutObjects = new HashMap<Integer, Set<String>>(2);
		Set<String> related = objectsInQuery.get(prefixedVar);
		if(related==null){
			related = new HashSet<String>();
			objectsInQuery.put(prefixedVar, related);
			if(coveringOntonuts!=null){
				for(int o : coveringOntonuts){
					Set<String> oObjs = ontonutObjects.get(o);
					if(oObjs==null){
						oObjs = new HashSet<String>();
						ontonutObjects.put(o, oObjs);
					}
					oObjs.add(prefixedVar);
				}
			}
		}
		related.add(prefixedVar);
	}

	public void addQueryObjectID(String objectVar, String id) {
		if(objectsIDs==null) objectsIDs = new HashMap<String, String>(2);
		objectsIDs.put(objectVar, id);
	}

	public void addNegativeObjectID(String objectVar) {
		if(negativeObjectsIDs==null) negativeObjectsIDs = new HashSet<String>();
		negativeObjectsIDs.add(objectVar);		
	}

	public void addCoveredSelectionStatement(int ontonut, String statementID, SemanticStatement statement){
		if(coveredSelectionStatements==null) coveredSelectionStatements = new HashMap<Integer,Map<String,SemanticStatement>>(4);
		Map<String,SemanticStatement> ontonutStatements = coveredSelectionStatements.get(ontonut);
		if(ontonutStatements==null){
			ontonutStatements = new LinkedHashMap<String,SemanticStatement>();
			coveredSelectionStatements.put(ontonut, ontonutStatements);
		}
		ontonutStatements.put(statementID, statement);
	}

	public void addCoveredSpecialStatement(int ontonut, String statementID, SemanticStatement statement){
		if(coveredSpecialStatements==null) coveredSpecialStatements = new HashMap<Integer,Map<String,SemanticStatement>>(4);
		Map<String,SemanticStatement> ontonutStatements = coveredSpecialStatements.get(ontonut);
		if(ontonutStatements==null){
			ontonutStatements = new LinkedHashMap<String,SemanticStatement>();
			coveredSpecialStatements.put(ontonut, ontonutStatements);
		}
		ontonutStatements.put(statementID, statement);
	}

	public void addOptionalCoveredStatementID(int ontonut, String statementID){
		if(optionalCoveredStatementIDs==null) optionalCoveredStatementIDs = new HashMap<Integer,Set<String>>(4);
		Set<String> ontonutStatements = optionalCoveredStatementIDs.get(ontonut);
		if(ontonutStatements==null){
			ontonutStatements = new HashSet<String>();
			optionalCoveredStatementIDs.put(ontonut, ontonutStatements);
		}
		ontonutStatements.add(statementID);
	}

	public void addTopLevelCoveredStatementID(int ontonut, String statementID){
		if(topLevelCoveredStatementIDs==null) topLevelCoveredStatementIDs = new HashMap<Integer,Set<String>>();
		Set<String> ontonutStatements = topLevelCoveredStatementIDs.get(ontonut);
		if(ontonutStatements==null){
			ontonutStatements = new LinkedHashSet<String>();
			topLevelCoveredStatementIDs.put(ontonut, ontonutStatements);
		}
		ontonutStatements.add(statementID);
	}

	public void addOntonutReferencedDataItem(int ontonut, String dataItem, String value){
		if(queryReferencedDataItems==null) queryReferencedDataItems = new HashMap<Integer, Map<String,Set<String>>>();
		Map<String,Set<String>> ontonutItems = queryReferencedDataItems.get(ontonut);
		if(ontonutItems==null){
			ontonutItems = new HashMap<String,Set<String>>();
			queryReferencedDataItems.put(ontonut, ontonutItems);
		}
		Set<String> itemMappedValues = ontonutItems.get(dataItem);
		if(itemMappedValues==null){
			itemMappedValues =  new HashSet<String>(2);
			ontonutItems.put(dataItem, itemMappedValues);
		}
		itemMappedValues.add(value);
	}

	public void addOntonutConstantDatatype(int ontonut, String dataItem, String objectDatatype) {
		if(queryReferencedDataItemsConstantDatatypes==null) queryReferencedDataItemsConstantDatatypes = new HashMap<Integer, Map<String,String>>(4);
		Map<String,String> ontonutTypes = queryReferencedDataItemsConstantDatatypes.get(ontonut);
		if(ontonutTypes==null){
			ontonutTypes = new HashMap<String,String>();
			queryReferencedDataItemsConstantDatatypes.put(ontonut, ontonutTypes);
		}
		ontonutTypes.put(dataItem, objectDatatype);
	}


	public void addLocalStarter(String statementID, SemanticStatement statement){
		if(localStarters==null) localStarters = new LinkedHashMap<String, SemanticStatement>(4);
		localStarters.put(statementID, statement);
	}

	public void addLocalJoiner(String statementID, SemanticStatement statement){
		if(localJoiners==null) localJoiners = new LinkedHashMap<String, SemanticStatement>(4);
		localJoiners.put(statementID, statement);
	}

	public void addLocalFinalizer(String statementID, SemanticStatement statement){
		if(localFinalizers==null) localFinalizers = new LinkedHashMap<String, SemanticStatement>(4);
		localFinalizers.put(statementID, statement);
	}

	public void addJoiner(String statementID, SemanticStatement statement){
		if(joinerStatements==null) joinerStatements = new LinkedHashMap<String, SemanticStatement>(4);
		joinerStatements.put(statementID, statement);
	}

	public void addLocalOntonutCannotHandle(int ontonut, String statementID, SemanticStatement statement){
		if(localOntonutCannotHandle==null) localOntonutCannotHandle = new HashMap<Integer,Map<String,SemanticStatement>>(4);
		Map<String,SemanticStatement> ontonutStatements = localOntonutCannotHandle.get(ontonut);
		if(ontonutStatements==null){
			ontonutStatements = new LinkedHashMap<String,SemanticStatement>();
			localOntonutCannotHandle.put(ontonut, ontonutStatements);
		}
		ontonutStatements.put(statementID,statement);
	}

	public void addFilterWithinOr(String orStID, SemanticStatement st){
		if(filtersWithinOr==null) filtersWithinOr = new HashMap<String, Set<SemanticStatement>>(2);
		Set<SemanticStatement> sts = filtersWithinOr.get(orStID);
		if(sts==null){
			sts = new HashSet<SemanticStatement>(2);
			filtersWithinOr.put(orStID, sts);
		}
		sts.add(st);
	}

	public void addVariableMappingOption(int ontonut, Map<String,String> mapping){
		if(variableMappingOptions==null) variableMappingOptions = new HashMap<Integer, Set<Map<String,String>>>();
		Set<Map<String,String>> mappings = variableMappingOptions.get(ontonut);
		if(mappings==null){
			mappings = new HashSet<Map<String,String>>(); 
			variableMappingOptions.put(ontonut, mappings);
		}
		mappings.add(mapping);
	}

	public void addAllFrom(QueryDecompositionResult qdr, boolean includingCoverage){
		addBasicsFrom(qdr);

		if(qdr.ontonutObjects!=null){
			if(ontonutObjects==null) ontonutObjects = new HashMap<Integer,Set<String>>();
			for(Map.Entry<Integer, Set<String>> entry : qdr.ontonutObjects.entrySet()){
				Set<String> ontonutObjs = ontonutObjects.get(entry.getKey());
				if(ontonutObjs==null){
					ontonutObjs = new HashSet<String>();
					ontonutObjects.put(entry.getKey(), ontonutObjs);
				}				
				ontonutObjs.addAll(entry.getValue());
			}
		}

		if(qdr.allOntonutVars!=null){
			if(allOntonutVars==null) allOntonutVars = new HashMap<Integer,Set<String>>();
			for(Map.Entry<Integer, Set<String>> entry : qdr.allOntonutVars.entrySet()){
				Set<String> ontonutVars = allOntonutVars.get(entry.getKey());
				if(ontonutVars==null){
					ontonutVars = new HashSet<String>();
					allOntonutVars.put(entry.getKey(), ontonutVars);
				}				
				ontonutVars.addAll(entry.getValue());
			}
		}
		if(qdr.mandatoryOntonutVars!=null){
			if(mandatoryOntonutVars==null) mandatoryOntonutVars = new HashMap<Integer,Set<String>>();
			for(Map.Entry<Integer, Set<String>> entry : qdr.mandatoryOntonutVars.entrySet()){
				Set<String> ontonutVars = mandatoryOntonutVars.get(entry.getKey());
				if(ontonutVars==null){
					ontonutVars = new HashSet<String>();
					mandatoryOntonutVars.put(entry.getKey(), ontonutVars);
				}				
				ontonutVars.addAll(entry.getValue());
			}
		}
		if(qdr.notHandledOntonutVars!=null){
			if(notHandledOntonutVars==null) notHandledOntonutVars = new HashMap<Integer,Set<String>>();
			for(Map.Entry<Integer, Set<String>> entry : qdr.notHandledOntonutVars.entrySet()){
				Set<String> ontonutVars = notHandledOntonutVars.get(entry.getKey());
				if(ontonutVars==null){
					ontonutVars = new HashSet<String>();
					notHandledOntonutVars.put(entry.getKey(), ontonutVars);
				}				
				ontonutVars.addAll(entry.getValue());
			}
		}

		if(includingCoverage){
			if(qdr.coveredSelectionStatements!=null){
				if(coveredSelectionStatements==null) coveredSelectionStatements = new HashMap<Integer,Map<String,SemanticStatement>>();
				for(Map.Entry<Integer, Map<String,SemanticStatement>> entry : qdr.coveredSelectionStatements.entrySet()){
					Map<String,SemanticStatement> ontonutStatements = coveredSelectionStatements.get(entry.getKey());
					if(ontonutStatements==null){
						ontonutStatements = new LinkedHashMap<String,SemanticStatement>();
						coveredSelectionStatements.put(entry.getKey(), ontonutStatements);
					}				
					ontonutStatements.putAll(entry.getValue());
				}
			}
	
			if(qdr.topLevelCoveredStatementIDs!=null){
				if(topLevelCoveredStatementIDs==null) topLevelCoveredStatementIDs = new HashMap<Integer,Set<String>>();
				for(Map.Entry<Integer, Set<String>> entry : qdr.topLevelCoveredStatementIDs.entrySet()){
					Set<String> ontonutStatements = topLevelCoveredStatementIDs.get(entry.getKey());
					if(ontonutStatements==null){
						ontonutStatements = new LinkedHashSet<String>();
						topLevelCoveredStatementIDs.put(entry.getKey(), ontonutStatements);
					}				
					ontonutStatements.addAll(entry.getValue());
				}
			}
			if(qdr.optionalCoveredStatementIDs!=null){
				if(optionalCoveredStatementIDs==null) optionalCoveredStatementIDs = new HashMap<Integer,Set<String>>();
				for(Map.Entry<Integer, Set<String>> entry : qdr.optionalCoveredStatementIDs.entrySet()){
					Set<String> ontonutStatements = optionalCoveredStatementIDs.get(entry.getKey());
					if(ontonutStatements==null){
						ontonutStatements = new HashSet<String>();
						optionalCoveredStatementIDs.put(entry.getKey(), ontonutStatements);
					}				
					ontonutStatements.addAll(entry.getValue());
				}
			}
		
			if(qdr.coveredSpecialStatements!=null){
				if(coveredSpecialStatements==null) coveredSpecialStatements = new HashMap<Integer,Map<String,SemanticStatement>>();
				for(Map.Entry<Integer, Map<String,SemanticStatement>> entry : qdr.coveredSpecialStatements.entrySet()){
					Map<String,SemanticStatement> ontonutStatements = coveredSpecialStatements.get(entry.getKey());
					if(ontonutStatements==null){
						ontonutStatements = new LinkedHashMap<String,SemanticStatement>();
						coveredSpecialStatements.put(entry.getKey(), ontonutStatements);
					}				
					ontonutStatements.putAll(entry.getValue());
				}
			}
		}

		if(qdr.queryReferencedDataItems!=null){
			if(queryReferencedDataItems==null) queryReferencedDataItems = new HashMap<Integer,Map<String,Set<String>>>();
			for(Map.Entry<Integer, Map<String,Set<String>>> entry : qdr.queryReferencedDataItems.entrySet()){
				Map<String,Set<String>> ontonutQueryItems = queryReferencedDataItems.get(entry.getKey());
				if(ontonutQueryItems==null){
					ontonutQueryItems = new HashMap<String,Set<String>>();
					queryReferencedDataItems.put(entry.getKey(),ontonutQueryItems);
				}
				for(Map.Entry<String,Set<String>> entry2 : entry.getValue().entrySet()){
					Set<String> itemVars = ontonutQueryItems.get(entry2.getKey());
					if(itemVars==null){
						itemVars =  new HashSet<String>();
						ontonutQueryItems.put(entry2.getKey(),itemVars);
					}
					itemVars.addAll(entry2.getValue());
				}
			}
		}

		if(qdr.queryReferencedDataItemsConstantDatatypes!=null){
			if(queryReferencedDataItemsConstantDatatypes==null) queryReferencedDataItemsConstantDatatypes = new HashMap<Integer,Map<String,String>>();
			for(Map.Entry<Integer, Map<String,String>> entry : qdr.queryReferencedDataItemsConstantDatatypes.entrySet()){
				Map<String,String> ontonutQueryItems = queryReferencedDataItemsConstantDatatypes.get(entry.getKey());
				if(ontonutQueryItems==null){
					ontonutQueryItems = new HashMap<String,String>();
					queryReferencedDataItemsConstantDatatypes.put(entry.getKey(), ontonutQueryItems);
				}				
				ontonutQueryItems.putAll(entry.getValue());
			}
		}

		if(!qdr.allMandatoryCoveredByMoreThanOneOntonut) allMandatoryCoveredByMoreThanOneOntonut = false;
		if(qdr.hasMandatory) hasMandatory = true;

	}

	public void addBasicsFrom(QueryDecompositionResult qdr){
		if(qdr.allQueryVars!=null){
			if(allQueryVars==null) allQueryVars = new HashSet<String>(); 
			allQueryVars.addAll(qdr.allQueryVars);
		}
		if(qdr.rdfTypeVars!=null){
			if(rdfTypeVars==null) rdfTypeVars = new HashSet<String>(); 
			rdfTypeVars.addAll(qdr.rdfTypeVars);
		}
		if(qdr.objectsInQuery!=null){
			if(objectsInQuery==null) objectsInQuery = new HashMap<String,Set<String>>(); 
			for(Map.Entry<String,Set<String>> entry : qdr.objectsInQuery.entrySet()){
				Set<String> objVars = objectsInQuery.get(entry.getKey());
				if(objVars==null){
					objVars = new HashSet<String>();
					objectsInQuery.put(entry.getKey(), objVars);
				}				
				objVars.addAll(entry.getValue());
			}
		}
		if(qdr.negativeObjectsIDs!=null){
			if(negativeObjectsIDs==null) negativeObjectsIDs = new HashSet<String>(); 
			negativeObjectsIDs.addAll(qdr.negativeObjectsIDs);
		}

		if(qdr.localStarterVars!=null){
			if(localStarterVars==null) localStarterVars = new HashSet<String>(); 
			localStarterVars.addAll(qdr.localStarterVars);
		}		

		if(qdr.filtersWithinOr!=null){
			if(filtersWithinOr==null) filtersWithinOr = new HashMap<String, Set<SemanticStatement>>(2); 
			filtersWithinOr.putAll(qdr.filtersWithinOr);
		}	
	}

}
