package sapl.shared.eii;

import java.util.HashMap;
import java.util.Map;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Base class for behaviors that use the DataBearings Ontonuts-mechanism for processing data received
 * via a push (i.e. not pulled via UniversalAdapterBehavior).
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (14.08.2017)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2017, VTT
 * 
 * */

public class OntonutListenerBehavior extends ReusableAtomicBehavior{

	public static boolean DEBUG_PRINT = false;
	public static boolean INTERACTION_PRINT = true;

	@Parameter (name="ontonut") public String ontonutId;

	protected String ontonutSemantics;
	protected String ontonutSyntax;
	
	protected Ontonut ontonut;

	protected Map<String,String> ontonutDataItems;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.executeInThread = true;
		this.ontonutId = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "ontonut"));
	}

	@Override
	protected void doAction() throws Exception {
		initializeOntonut();
	}
	
	public void initializeOntonut() {
		String query = "@prefix o: <"+SaplConstants.ONTONUT_NS+">. @prefix s: <"+SaplConstants.SAPL_NS+">. ";
		//query += "<"+this.ontonutId+">" +" o:type ?type; o:semantics { ?data => ?semantics }. ";

		query += "<"+this.ontonutId+"> o:type ?type. {<"+this.ontonutId+"> o:semantics { ?data => ?semantics }} s:is s:Optional. ";

		SaplQueryResultSet resultSet = this.hasBeliefsN3(query);
		if(resultSet==null || resultSet.getNumberOfSolutions()==0){
			this.setFailed();
			throw new Error ("Ontonut "+this.ontonutId+" is not well defined");
		}
		Map<String, String> def = resultSet.getSolution(0);

		this.ontonutSemantics = def.get("semantics");
		this.ontonutSyntax = def.get("data");

		String type = def.get("type");

		//load ontonut plug-in
		try {
			@SuppressWarnings("unchecked")
			Class<Ontonut> cl = (Class<Ontonut>) Class.forName(type);
			this.ontonut = cl.newInstance();
		} catch (Throwable e) {
			this.setFailed();
			throw new Error ("Ontonut "+ontonutId+" has unknown type "+ type); 
		}

		this.ontonut.configure(this.ontonutId, 0, this, null);

		if(!this.ontonut.loadDefinition()){
			this.setFailed();
			throw new Error ("Failed to load definition of the ontonut "+ontonutId); 
		}
		
		this.ontonutDataItems = new HashMap<String,String>();
		
		if(this.ontonutSyntax!=null)
			this.ontonut.findSyntaxDataItems(this.ontonutSyntax, this.ontonutDataItems); 

		if(DEBUG_PRINT){
			print("Data items:: "+this.ontonutDataItems);
			print("Ontonut-local filters:: "+this.ontonut.filters);
		}
		
		for(String key : this.ontonutDataItems.keySet()){
			this.ontonut.addSelectionCondition(this.ontonutDataItems.get(key), key, false, key);
		}

		for(String var : this.ontonut.filterAndPostconditionVars){
			this.ontonut.addToSelectVarsMap(var, SaplConstants.VARIABLE_PREFIX+var);
		}
		
	}
	
	public Ontonut getOntonut(){
		return ontonut;
	} 

	public String getOntonutSemantics(){
		return ontonutSemantics;
	} 

	public String getOntonutSyntax(){
		return ontonutSyntax;
	} 

	
}
