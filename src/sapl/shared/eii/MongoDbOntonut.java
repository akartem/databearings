package sapl.shared.eii;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.BsonDocument;
import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;

/** 
 * Ontonut representing a MongoDB collection 
 * 
 * @author Artem Katasonov (VTT + Elisa from July 2018)
 * 
 * @version 4.7 (14.02.2019)
 * 
 * @since 4.6
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2018, VTT
 * 
 * */
 
public class MongoDbOntonut extends JsonOntonut {
	
	protected String collection;
	
	protected StringBuilder filter = new StringBuilder();
	
	@Override
	public boolean loadDefinition() {
		
		if(!super.loadDefinition())
			return false;
		
		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "<"+this.id+">" + " o:collection ?collection"; 
		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);
		if(resultSet.getNumberOfSolutions()==0){
			this.parent.printError(id+": Ontonut is not well defined"); return false;
		}		

		Map<String, String> def = resultSet.getSolution(0);
		this.collection = def.get("collection");

		return true;		
	}
	
	@Override
	public void addSelectionCondition(String dataItem, String value, boolean isValueString, String variable) {
		super.addSelectionCondition(dataItem, value, isValueString, variable);
		
		if(!value.startsWith(SaplConstants.VARIABLE_PREFIX) || value.length()<=SaplConstants.VARIABLE_PREFIX.length()){
			if(isValueString) value = "'"+value+"'";
			if(this.filter.length()>0) this.filter.append(",");
			this.filter.append("'"+dataItem.replace("/", ".")+"':"+value+"");
		}
	}
	
	@Override
	public boolean addFilterCondition(String dataItem, String operation, String value, boolean isValueString, String variable){
		super.addFilterCondition(dataItem, operation, value, isValueString, variable);

		if(!value.startsWith(SaplConstants.VARIABLE_PREFIX) && variable!=null){
			if(isValueString) value = "'"+value+"'";
			if(this.filter.length()>0) this.filter.append(",");
			if(operation.equals("=") ){
				this.filter.append("'"+dataItem.replace("/", ".")+"':"+value+"");
			}
			else{
				this.filter.append("'"+dataItem.replace("/", ".")+"':{"+operation+":"+value+"}");
			}
			return true;
		}
		else return false;
	}	
	
	@Override
	public boolean addOrCondition(List<String> dataItems, List<String> operations, List<String> values,
			List<Boolean> isValuesString, List<String> variables) {
		
		super.addOrCondition(dataItems, operations, values, isValuesString, variables);
		
		boolean canBeIn = true;
		String inDataItem = null;
		StringBuilder inValues = new StringBuilder();
		StringBuilder conditions = new StringBuilder();
		
		for(int i=0; i<dataItems.size(); i++){
			String dataItem = dataItems.get(i);
			String operation = operations.get(i);
			String value = values.get(i);
			boolean isString = isValuesString.get(i);
			if(isString) value = "'"+value+"'";
			if(canBeIn){
				if(!operation.equals("=")) canBeIn = false;
				else{ 
					if(inDataItem==null) inDataItem = dataItem;
					else if(!dataItem.equals(inDataItem)) 
						canBeIn = false;
					
					if(canBeIn){
						if(inValues.length()>0) inValues.append(",");
						inValues.append(value);
					}
				}
			}
			
			if(conditions.length()>0) conditions.append(",");
			if(operation.equals("=") ){
				conditions.append("{'"+dataItem.replace("/", ".")+"':"+value+"}");
			}
			else{
				conditions.append("{'"+dataItem.replace("/", ".")+"':{"+operation+":"+value+"}}");
			}			
		}
		
		if(canBeIn){
			if(this.filter.length()>0) this.filter.append(",");
			this.filter.append("'"+inDataItem.replace("/", ".")+"':{$in: ["+inValues.toString()+"]}");
		}
		else{
			if(this.filter.length()>0) this.filter.append(",");
			this.filter.append("$or:["+conditions.toString()+"]");
		}
		return true;
	}
	
	@Override
	public String getLogOperation(String predicate){
		String operation=null;
		if(predicate.equals(SaplConstants.eq) && (operation="=")!=null ||
				predicate.equals(SaplConstants.neq) && (operation="$ne")!=null ||
				predicate.equals(SaplConstants.lt) && (operation="$lt")!=null ||
				predicate.equals(SaplConstants.lte) && (operation="$lte")!=null ||
				predicate.equals(SaplConstants.gt) && (operation="$gt")!=null ||
				predicate.equals(SaplConstants.gte)&& (operation="$gte")!=null )
			return operation;
		else return null;
	}
	
	@Override
	protected List<Map<String, String>> runSingleQuery(String request, String body, String reqUsername, String reqPassword, int subindex, Map<String, String> localSol) throws Exception {
		long s = System.currentTimeMillis();
		
		String strFilter = "{"+this.filter.toString()+"}";
		
		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			print("Executing "+strFilter+" on "+ request, subindex);
		
		String server = null;
		String database = null;
		int ind = request.lastIndexOf("/");
		if(ind!=-1){
			server = request.substring(0, ind);
			database = request.substring(ind+1);
		}
		
		MongoClient mongoClient = new MongoClient(server);

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Connected [" + (System.currentTimeMillis() - s) + "]");
		s = System.currentTimeMillis();
		
		try{
			FindIterable<Document> result = null;
			List<Map<String,String>> sols = new ArrayList<Map<String,String>>();

			MongoDatabase mongoDatabase = mongoClient.getDatabase(database);
			MongoCollection<Document> collection = mongoDatabase.getCollection(this.collection);

			BsonDocument filter = BsonDocument.parse(strFilter);
			
			result = collection.find(filter);
			
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Query executed [" + (System.currentTimeMillis() - s) + "]", subindex);
			s = System.currentTimeMillis();			
				
			Iterator<Document> it = result.iterator();
			while(it.hasNext()){
				Document resultDoc = it.next();
				sols.addAll(this.processResponse(resultDoc.toJson()));
			}
			
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Processed "+sols.size()+" result documents ["+(System.currentTimeMillis()-s)+"]", subindex);
			
			return sols;
		}
		finally{
			mongoClient.close();
		}		
	}
	
}
