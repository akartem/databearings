package sapl.shared.eii;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.rab.Resource;
import sapl.shared.server.ServerBehaviorServerEvent;
import sapl.util.HttpException;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.StreamingUtils;

/** 
 * Generic abstract ontonut representing any REST Web Service
 * 
 * @author Artem Katasonov (VTT + Elisa from July 2018) 
 * 
 * @version 5.2 (08.04.2020)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */ 

public abstract class HttpOntonut extends Ontonut {

	public static boolean INTERACTION_DETAILS_PRINT = false;
	public static boolean UPDATE_RESPONSE_PRINT = false; // matters only when INTERACTION_DETAILS_PRINT==false

	public static boolean WRITE_FILE_APPEND = false;

	protected String uri;

	protected String username;
	protected String password;
	protected String authMethod;
	protected String authUrl;
	
	protected String timeout;

	protected String encoding;

	protected String body;
	protected String contentType;

	protected String method;
	
	//New on 06.07.2018
	protected Map<String,String> customHeaders;

	//New on 05.07.2018: if needs to access several sources, use parallel threads (default) or not
	protected boolean singleThread = false;
	
	protected String requestContent;
	protected String requestBody;


	/**Parameters appearing in the request content as %%param%% that were not yet bound*/
	protected Set<String> requestVars = new HashSet<String>(2);

	protected Object lastContent = null;

	@Override
	public boolean loadDefinition() {
		super.loadDefinition();
		
		if(this.requestOntonut==null){
			String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
			query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
			query += "{<"+this.id+"> o:service ?service. ?service o:uri ?uri. {?service o:timeout ?timeout} s:is s:Optional."
					+ " {?service o:encoding ?encoding} s:is s:Optional. {?service o:body ?body} s:is s:Optional. {?service o:contentType ?contentType} s:is s:Optional. {?service o:method ?method} s:is s:Optional. {?service o:customHeader ?custom} s:is s:Optional}"
					+ " s:or {<"+this.id+"> o:data ?data}. {<"+this.id+"> o:username ?username; o:password ?password} s:is s:Optional. "
					+ "{<"+this.id+"> o:authMethod ?authMethod} s:is s:Optional. {<"+this.id+"> o:authUrl ?authUrl} s:is s:Optional. "
							+ "{<"+this.id+"> o:singleThread ?single} s:is s:Optional."; 
	
			SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);
			if(resultSet==null || resultSet.getNumberOfSolutions()==0){
				this.parent.printError(id+": Ontonut is not well defined"); return false;
			}		
	
			Map<String, String> def = resultSet.getSolution(0);
	
			this.uri = def.get("uri");
	
			this.username = def.get("username");
			this.password = def.get("password");
			this.authMethod = def.get("authMethod");
			this.authUrl = def.get("authUrl");
			
			this.timeout = def.get("timeout");
			this.encoding = def.get("encoding");
			this.body = def.get("body");
			
			this.contentType = def.get("contentType");
			
			this.method = def.get("method");

			String custom = def.get("custom");
			if(custom!=null){
				if(resultSet.getNumberOfSolutions() > 1){
					resultSet.forAll.add("custom");
					List<Map<String, String>> sols = resultSet.getSolutions();
					for(Map<String, String> sol : sols){
						addCustomeHeader(sol.get("custom"));
					}
				}
				else addCustomeHeader(custom);
			}
			
			String single = def.get("single");
			if(single!=null && single.equals("true")) 
				this.singleThread = true; 
	
			String data = def.get("data");
			if(this.uri!=null)
				this.requestContent = this.uri;
			else this.requestContent = data;
	
			this.requestBody = this.body;
		}

		return true;		
	}
	
	void addCustomeHeader(String header){
		int index = header.indexOf(":");
		if(index != -1){
			if(this.customHeaders==null) this.customHeaders = new HashMap<String,String>(2);
			this.customHeaders.put(header.substring(0,index).trim(), header.substring(index+1).trim());
		}
	}

	@Override
	public void findDataItems(String contextID, Map<String, String> result) {
		int ind=0;
		String toAnalyse = this.requestContent + (this.requestBody!=null? this.requestBody : "")
				+ (this.username!=null? this.username : "") + (this.password!=null? this.password : "");

		while(true){
			ind = toAnalyse.indexOf("%%", ind);
			if(ind!=-1){
				int ind2 = toAnalyse.indexOf("%%", ind+3);
				if(ind2!=-1){
					String value = toAnalyse.substring(ind+2, ind2);
					String var = SaplConstants.VARIABLE_PREFIX+value;
					if(result.get(var)==null)
						result.put(var, "%%"+value+"%%");

					this.requestVars.add(value);

					ind = ind2+2;
				}
				else break;
			}
			else break;
		}
	}	

	@Override
	public void addSelectionCondition(String dataItem, String value, boolean isValueString, String variable) {
		super.addSelectionCondition(dataItem, value, isValueString, variable);

		if(variable!=null){
			String varName = variable.substring(SaplConstants.VARIABLE_PREFIX.length());
			String replace = "%%"+variable.substring(SaplConstants.VARIABLE_PREFIX.length())+"%%";

			if((!value.startsWith(SaplConstants.VARIABLE_PREFIX) || value.length() <= SaplConstants.VARIABLE_PREFIX.length()) && variable!=null){
				if(this.requestContent!=null && this.requestContent.contains(replace)){
					this.requestContent = this.requestContent.replace(replace, value);
					this.requestVars.remove(varName);
				}
				if(this.requestBody!=null && this.requestBody.contains(replace)){
					this.requestBody = this.requestBody.replace(replace, value);
					this.requestVars.remove(varName);
				}
				if(this.username!=null && this.username.contains(replace)){
					this.username = this.username.replace(replace, value);
					this.requestVars.remove(varName);
				}
				if(this.password!=null && this.password.contains(replace)){
					this.password = this.password.replace(replace, value);
					this.requestVars.remove(varName);
				}
			}
		}
	}


	@Override
	public boolean addFilterCondition(String dataItem, String operation, String value, boolean isValueString, String variable){
		super.addFilterCondition(dataItem, operation, value, isValueString, variable);

		if(variable!=null){
			String varName = variable.substring(SaplConstants.VARIABLE_PREFIX.length());
			String replace = "%%"+varName+"%%";

			if(operation.equals("=") && !(value.startsWith(SaplConstants.VARIABLE_PREFIX) && value.length() > SaplConstants.VARIABLE_PREFIX.length())){
				boolean found = false;
				if(this.requestContent!=null && this.requestContent.contains(replace)){
					found=true;
					this.requestContent = this.requestContent.replace(replace, value);
					this.requestVars.remove(varName);
				}
				if(this.requestBody!=null && this.requestBody.contains(replace)){
					found=true;
					this.requestBody = this.requestBody.replace(replace, value);
					this.requestVars.remove(varName);
				}
				if(this.username!=null && this.username.contains(replace)){
					found=true;
					this.username = this.username.replace(replace, value);
					this.requestVars.remove(varName);
				}
				if(this.password!=null && this.password.contains(replace)){
					found=true;
					this.password = this.password.replace(replace, value);
					this.requestVars.remove(varName);
				}
				return found;
			}
			else{
				return false;
				//Non-equality filters not supported
			}
		}
		else if(operation.equals("=") && dataItem!=null && dataItem.startsWith("\"") && dataItem.endsWith("\"")){
			String str = dataItem.substring(1,dataItem.length()-1);
			if(str.equals(value)) 
				return true;
			else return false; // TODO means that this ontonut will not be able to produce any solutions (constant mismatch), so could not execute at all
		}
		return false;
	}

	@Override
	protected List<Map<String,String>> runQuery(SaplQueryResultSet localSolutions, List<Map<String,String>> ontonutErrorSolutions) throws Throwable{

		if(this.requestOntonut!=null)
			return runQueryViaRequestOntonut(localSolutions, ontonutErrorSolutions);
		
		String queryData = this.requestContent;
		String queryBody = this.requestBody;

		//check remaining request parameters: they should map to S-APL query variables
		final Map<String, String> replaceMap = new HashMap<String, String>();
		final Set<String> originalRequestVars = new HashSet<String>(requestVars);

		Set<String> tempRequestVars = new HashSet<String>(requestVars);
		for(String requestVar : tempRequestVars){
			for(Map<String,String> option: selectVariableMappings){
				for(String queryVar : option.keySet()){
					String var = option.get(queryVar);
					if(var.equals(SaplConstants.VARIABLE_PREFIX+requestVar)){
						if(! (queryVar.startsWith("\"") && queryVar.startsWith("\""))){
							//substitute the ontonut-used variable with S-APL query variable, because that one is expected in local solutions
							if(!queryVar.equals(requestVar)){
								requestVars.remove(requestVar);
								requestVars.add(queryVar);
								replaceMap.put(queryVar, requestVar);
							}
						}
						else{ //constant (not sure if ever get here, addSelectionCondition should already do)
							parent.printToErr("constant (not sure if ever get here, addSelectionCondition should already do)");
							String replace = "%%"+requestVar+"%%";
							queryData = queryData.replace(replace, queryVar);
							if(queryBody!=null) queryBody = queryBody.replace(replace, queryVar);
							requestVars.remove(requestVar);
						}
					}
				}
			}
		}
		
		//remove from consideration undefined variables
		List<String> selectVars = new ArrayList<String>(requestVars);
		//FIX 04.09.2017
		selectVars.retainAll(localSolutions.getSolution(0).keySet());
		
		//getSolutions() always returns at least one empty solution, even if the result set did not have any
		//(unless s:All refers to undefined vars)
		List<String> originalForAll = localSolutions.forAll; 
		localSolutions.forAll = selectVars;
		List<Map<String, String>> localSols = localSolutions.getSolutions();
		if(localSols.isEmpty()){
			localSolutions.forAll = originalForAll;
			localSols = localSolutions.getSolutions();
		}

		List<Thread> threads = new ArrayList<Thread>(localSols.size());
		final List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		final List<Map<String,String>> errorSolutions = ontonutErrorSolutions;

		final List<Throwable> thrown = new ArrayList<Throwable>(); 

		//make separate request for each local solution and do UNION on results
		if(localSols.size()>1 && UniversalAdapterBehavior.PERFORMANCE_PRINT || UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
				print("Starting "+localSols.size()+" request branches");
		
		if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
			print("Request branching solutions: "+localSols);
		
		
		int counter = 0;
		for(Map<String, String> localSol : localSols){
			
			counter++;

			String finalRequest = queryData;
			String finalBody = queryBody;
			String finalUsername = this.username;
			String finalPassword = this.password;

			//process remaining request vars: values for which have to come from local solutions
			tempRequestVars = new HashSet<String>(requestVars);
			for(String requestVar : requestVars){
				String originalRequestVar = replaceMap.get(requestVar);
				if(originalRequestVar==null) originalRequestVar = requestVar;
				String replace = "%%"+originalRequestVar+"%%";
				String value = localSol.get(requestVar);
				if(value!=null){
					finalRequest = finalRequest.replace(replace, value);
					if(finalBody!=null) finalBody = finalBody.replace(replace, value);
					if(finalUsername!=null) finalUsername = finalUsername.replace(replace, value);
					if(finalPassword!=null) finalPassword = finalPassword.replace(replace, value);
					tempRequestVars.remove(requestVar);
				}
			}
			//for vars without values, just remove them
			for(String requestVar : tempRequestVars){
				String originalRequestVar = replaceMap.get(requestVar);
				if(originalRequestVar==null) originalRequestVar = requestVar;
				String replace = "%%"+originalRequestVar+"%%";
				finalRequest = finalRequest.replace(replace, "");
				if(finalBody!=null) finalBody = finalBody.replace(replace, "");
				if(finalUsername!=null) finalUsername = finalUsername.replace(replace, "");
				if(finalPassword!=null) finalPassword = finalPassword.replace(replace, "");
			}

			final String fRequest = finalRequest;
			final String fBody = finalBody;		
			final String fUsername = finalUsername;		
			final String fPassword = finalPassword;		
			final Map<String, String> fSol = localSol; 
			final int fCounter = counter;
			final Map<String, String> fLocalSol = localSol;
			
			Runnable r = new Runnable() {
				public void run() {			
					//run single query for one local solution
					Map<String,String> error = null;					
					List<Map<String, String>> sols;
					try {
						sols = runSingleQuery(fRequest, fBody, fUsername, fPassword, fCounter, fLocalSol);
						
						//01.09.2017
						if(postconditionQuery!=null){
							if(sols.isEmpty()){
								throw new Error("Postcondition is defined but no solutions");
							}

							Set<String> definedVars = new HashSet<String>(sols.get(0).keySet());
							SaplQueryResultSet rs = new SaplQueryResultSet(sols, definedVars);
							boolean result = parent.hasBeliefs(new ArrayList<String>(postconditions.keySet()), SaplConstants.G, rs);
		
							if(!result){
								throw new Error("Postcondition is false on "+sols);
							}
						}
						
					} catch (Throwable e) {
						thrown.add(e);
						sols = null;
						if(errorSolutions!=null){
							parent.printError(shortId+" ONTONUT ERROR!"+(uri==null?"":"\n\twhen doing "+fRequest+(fBody==null?"":"\n\twith body "+fBody)));
							parent.printException(e, UniversalAdapterBehavior.EXCEPTION_FULL_STACK_PRINT);
							error = new HashMap<String,String>();
							error.put("ontonutErrorSource", id);
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							error.put("ontonutErrorTrace", sw.toString());
							String meaning;
							if(e instanceof HttpException){
								HttpException httpe = (HttpException)e;
								meaning = "Service returned code "+httpe.getStatusCode()+" '"+httpe.getMessage()+"'";
							}
							else meaning = UniversalAdapterBehavior.getExceptionMeaning(e.getMessage(), sw.toString());
							error.put("ontonutErrorMessage", meaning);
							synchronized (errorSolutions) {
								errorSolutions.add(error);
							}
						}
					}						
					
					//add values of local vars that were used as parameters when forming the query (as they likely to be mandatory in the solution)
					if(sols!=null){
						for(Map<String, String> sol : sols){
							for(String requestVar : originalRequestVars){
								if(!sol.containsKey(requestVar)){
									boolean found = false;
									for(String saplVar : replaceMap.keySet()){
										if(replaceMap.get(saplVar).equals(requestVar)){
											sol.put(requestVar, fSol.get(saplVar));
											found = true;
											break;
										}
									}
									if(!found) sol.put(requestVar, fSol.get(requestVar));
								}
							}
							//also add vars added via o:precondition
							if(preconditionQueryVars!=null){
								for(String paramsQueryVar : preconditionQueryVars){
									if(!sol.containsKey(paramsQueryVar)){
										sol.put(paramsQueryVar, fSol.get(paramsQueryVar));
									}						
								}
							}
						}
					}
					
					if(sols!=null){
						if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
							print("Solutions: "+sols, fCounter);
						
						synchronized (result) {
							result.addAll(sols);
						}
					}
				}
			};

			if(!this.singleThread && localSols.size()>1){
				Thread th = new Thread(r);
				threads.add(th);
				th.start();
			}
			else r.run();
		}

		for (Thread th : threads){
			try {
				th.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if(ontonutErrorSolutions==null && !thrown.isEmpty())
			throw thrown.get(0);

		if(errorSolutions!=null){
			int ind = result.size()+1;
			for (Map<String, String> error : errorSolutions) {
				error.put("", "."+(executionIndex)+"."+(ind++));
			}
		}

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT && localSols.size()>1)
			print("After combining: "+(result==null?0:result.size())+" results");

		if(result.isEmpty()) return null;
		else return result;
	}


	protected List<Map<String, String>> runSingleQuery(String request, String requestBody, String reqUsername, String reqPassword, int subindex, Map<String, String> localSol) throws Exception {

		long s = System.currentTimeMillis();
		String content;
		if(this.uri!=null){
			if(UniversalAdapterBehavior.INTERACTION_PRINT){
				String toPrint = "Accessing "+request;
				if(requestBody != null){
					if(requestBody.length() <= 100) 
						toPrint += " with content " + requestBody;
					else toPrint += " with content " + requestBody.substring(0,98)+"..";
					toPrint += " ["+requestBody.length()+" chars]";
				}
				print(toPrint, subindex);
			}

			int readTimeout = HttpTools.DEFAULT_READ_TIMEOUT;
			if(this.timeout!=null){
				try {
					readTimeout = Integer.valueOf(this.timeout).intValue();
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			
			HttpOptions options = HttpOptions.defaultOptions();
			if(this.method!=null) options.method = this.method;
			options.content = requestBody;
			options.contentType = this.contentType;
			options.username = reqUsername;
			options.password = reqPassword;
			options.authMethod = this.authMethod;
			options.authUrl = this.authUrl;
			options.encoding = this.encoding;
			options.timeout = readTimeout;
			
			if(this.customHeaders!=null)
				options.customHeaders.putAll(this.customHeaders);

			content = HttpTools.getContent(request, options, true); //had o:uri
		}
		else content = request; // had o:data

		this.lastContent = content;
		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Response received, length: "+content.length()+" chars [" + (System.currentTimeMillis() - s) + "]", subindex);
		s = System.currentTimeMillis();

		if(INTERACTION_DETAILS_PRINT){
			print(content);
		}

		List<Map<String,String>> sols = null;

		//CHANGE 13.03.2017
		this.dataItemsToSelect.remove(""); //added in IDE to just get the document  
		
		if(!this.dataItemsToSelect.isEmpty())
			sols = this.processResponse(content);

		if(sols==null) sols = new ArrayList<Map<String,String>>();

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Processed "+sols.size()+" results ["+(System.currentTimeMillis()-s)+"]", subindex);
		
		return sols;
	}

	/**Processes text content to extract solutions*/
	public abstract List<Map<String,String>> processResponse(Object content) throws Exception;


	/**In support to IDE, fetch the target document if not done yet*/
	protected void getDoc() throws Throwable{
		if(this.lastContent==null){
			this.addVarToItemsToSelect("", "");
			executeQuery(new SaplQueryResultSet(),null);
		}
	}

	/**Called from subclasses: to send single update (POST or append to a file) */
	protected String sendUpdate(String updateURL, String updateData) throws Exception{
		long s = System.currentTimeMillis();

		int readTimeout = HttpTools.DEFAULT_READ_TIMEOUT;
		if(this.timeout!=null){
			try {
				readTimeout = Integer.valueOf(this.timeout).intValue();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		if(this.caseID!=null){
			Resource resource = new Resource(this.caseID);
			Object fromBlackboard = this.parent.getFromBlackboard(resource);
			if(fromBlackboard!=null && fromBlackboard instanceof ServerBehaviorServerEvent){
				ServerBehaviorServerEvent event = (ServerBehaviorServerEvent)fromBlackboard;
				HttpServletResponse response = event.getResponse();
				response.setContentType("text/plain");
				response.setCharacterEncoding("UTF-8");
				OutputStream out = response.getOutputStream();
				PrintWriter pw = new PrintWriter(new OutputStreamWriter(out, "UTF-8"), true);		
				pw.print(updateData);
				pw.flush();
				pw.close();
				out.close();
				event.getContinuation().complete();
				this.parent.removeFromBlackboard(resource);
				return null;
			}
		}
		
		if(this.requestContent.equals(SaplConstants.ONTONUT_NS+"request")){
			if(this.parent.executingWithResult()){
				this.parent.writeToResult(updateData);
				return null;
			}
		}

		String finalUsername = this.username;
		String finalPassword = this.password;
		
		if(preconditionQuery!=null){
			for(String var : preconditionQueryVars){
				String replace = "%%"+var+"%%";
				if(updateURL.contains(replace)){
					updateURL = updateURL.replace(replace, this.preconditionQueryVarsValues.get(var));
				}
				if(finalPassword!=null && finalUsername.contains(replace)){
					finalUsername = finalUsername.replace(replace, this.preconditionQueryVarsValues.get(var));
				}
				if(finalPassword!=null && finalPassword.contains(replace)){
					finalPassword = finalPassword.replace(replace, this.preconditionQueryVarsValues.get(var));
				}
			}
		}			
		
		URL url = null;
		try {
			url = new URL(updateURL.replace(" ","%20"));
		} catch (Exception e) {}

		if(url!=null){
			HttpOptions options = new HttpOptions();
			if(this.method!=null){
				options.method = this.method;
			}
			else if(!updateData.isEmpty()){
				options.method = "POST";
			}
			
			if(UniversalAdapterBehavior.INTERACTION_PRINT){
				String updateDataToPrint = INTERACTION_DETAILS_PRINT ? updateData : updateData.substring(0,Math.min(100,updateData.length())).replaceAll("\\s+", "");
				
				if(options.method.equals("POST"))
					this.print("Posting to "+updateURL+" : "+updateDataToPrint);
				else if(options.method.equals("PUT"))
					this.print("Putting to "+updateURL+" : "+updateDataToPrint);
				else if(options.method.equals("GET"))
					this.print("Sending update via GET to "+updateURL);
				else this.print("Doing "+options.method+ " to "+updateURL+" : "+updateDataToPrint);
			}
			
			options.content = updateData;
			options.contentType = this.contentType;
			options.username = finalUsername;
			options.password = finalPassword;
			options.authMethod = this.authMethod;
			options.authUrl = this.authUrl;
			options.encoding = this.encoding;
			options.timeout = readTimeout;

			if(this.customHeaders!=null)
				options.customHeaders.putAll(this.customHeaders);
			
			String result = HttpTools.getContent(updateURL, options, true);

			if(UniversalAdapterBehavior.PERFORMANCE_PRINT){
				this.print("Response received, length: "+result.length()+" chars [" + (System.currentTimeMillis() - s) + "]");
				
				if(INTERACTION_DETAILS_PRINT || UPDATE_RESPONSE_PRINT){
					print(result);
				}
			}
			
			return result;
		}
		else{
			if(UniversalAdapterBehavior.INTERACTION_PRINT)
				this.print("Writing to file "+updateURL);
			
			StreamingUtils.writeToFile(updateURL, updateData, WRITE_FILE_APPEND, "UTF-8");
			return null;
		}
	}
	
	protected List<Map<String,String>> runQueryViaRequestOntonut(SaplQueryResultSet localSolutions, List<Map<String,String>> ontonutErrorSolutions) throws Exception{
		
		long s = System.currentTimeMillis();
		
		boolean ok = this.parent.hasBeliefs(this.requestOntonutSemantics, SaplConstants.G, localSolutions);

		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			this.requestOntonut.print("After semantics query: "+localSolutions.getNumberOfSolutions()+" solutions ["+(System.currentTimeMillis()-s)+"] ");

		if(!ok) return null;
		
		if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
			this.requestOntonut.print("After-semantics local solutions: "+localSolutions);
		
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		
		List<Exception> thrown = new ArrayList<Exception>(); 

		if(localSolutions.getNumberOfSolutions()==0)
			localSolutions.addEmptySolution();
		
		//FIX 22.11.2017: Take only unique solutions with respect to vars used in request
		List<String> selectVars = new ArrayList<String>();
		for(String item : this.requestOntonut.dataItems.keySet()){
			if(item.startsWith(SaplConstants.VARIABLE_PREFIX)) 
				selectVars.add(item.substring(SaplConstants.VARIABLE_PREFIX.length()));
		}
		selectVars.retainAll(localSolutions.getSolution(0).keySet());
		
		
		List<Map<String, String>> local = localSolutions.getSolutions(selectVars);
		if(local.isEmpty()){
			local = localSolutions.getSolutions();
		}
		
		for(Map<String, String> l : local){
			List<Map<String, String>> localSols = new ArrayList<Map<String, String>>();
			localSols.add(l);

			Map<String,String> error = null;
			try {
				Object queryResult = this.requestOntonut.runUpdate(localSols);
				s = System.currentTimeMillis();
				
				List<Map<String, String>> sols =  this.processResponse(queryResult);
				if(sols!=null){
					if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
						this.requestOntonut.print("Processed "+sols.size()+" results ["+(System.currentTimeMillis()-s)+"]");
	
					result.addAll(sols);
				}
				else{
					if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
						this.requestOntonut.print("Processed 0 results ["+(System.currentTimeMillis()-s)+"]");
				}
				
			} catch (Exception e) {
				thrown.add(e);
				if(ontonutErrorSolutions!=null){
					parent.printException(e, UniversalAdapterBehavior.EXCEPTION_FULL_STACK_PRINT);
					error = new HashMap<String,String>();
					error.put("ontonutErrorSource", id);
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					error.put("ontonutErrorTrace", sw.toString());
					String meaning;
					if(e instanceof HttpException){
						HttpException httpe = (HttpException)e;
						meaning = "Service returned code "+httpe.getStatusCode()+" '"+httpe.getMessage()+"'";
					}
					else meaning = UniversalAdapterBehavior.getExceptionMeaning(e.getMessage(), sw.toString());
					error.put("ontonutErrorMessage", meaning);
					ontonutErrorSolutions.add(error);
				}
			}						
			
		}
		
		if(ontonutErrorSolutions==null && !thrown.isEmpty())
			throw thrown.get(0);

		if(ontonutErrorSolutions!=null){
			int ind = result.size()+1;
			for (Map<String, String> error : ontonutErrorSolutions) {
				error.put("", "."+(executionIndex)+"."+(ind++));
			}
		}
		
		if(UniversalAdapterBehavior.PERFORMANCE_PRINT && localSolutions.getNumberOfSolutions()>1)
			this.requestOntonut.print("After combining: "+(result==null?0:result.size())+" results");
		
		if(result.isEmpty()) return null;
		else return result;
	}
	
	//a utility for subclasses used with runQueryViaRequestOntonut 
	protected void bindRequestVars(Map<String, String> vars){
		for(String requestVar : requestVars){
			String replace = "%%"+requestVar+"%%";
			String value = vars.get(requestVar);
			if(value==null) value = "";
			this.requestContent = requestContent.replace(replace, value);
			if(this.requestBody!=null) this.requestBody = this.requestBody.replace(replace, value);
			if(this.username!=null) this.username = this.username.replace(replace, value);
			if(this.password!=null) this.password = this.password.replace(replace, value);
		}
	}
	

}
