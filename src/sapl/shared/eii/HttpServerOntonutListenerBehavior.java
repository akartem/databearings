package sapl.shared.eii;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Resource;
import sapl.shared.server.ServerBehavior;
import sapl.shared.server.ServerBehaviorServlet;
import sapl.util.URITools;

/**
 * An extension to the standard ServerBehavior that receives POSTs, processes them via an Ontonut,
 * and publishes the results (e.g. events formatted according to a defined schema) as S-APL local data. 
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (26.02.2018)
 * 
 * @since 4.5
 * 
 * Copyright (C) 2017-2018, VTT
 * 
 * */

public class HttpServerOntonutListenerBehavior extends ServerBehavior {

	protected String ontonutId;
	protected OntonutListenerBehavior ontonutBehavior = null;
	
	protected String matchRequest = null;

	protected String hostVar = null;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		super.initializeBehavior(parameters);
		this.ontonutId = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "ontonut"));
		this.matchRequest = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "request")); 
		this.hostVar = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "hostVar"));
		if(hostVar != null && hostVar.startsWith(SaplConstants.VARIABLE_PREFIX))
			hostVar = hostVar.substring(SaplConstants.VARIABLE_PREFIX.length());
	}

	@Override
	protected void doAction() {
		if(this.ontonutId != null){
			ontonutBehavior = new OntonutListenerBehavior();
			ontonutBehavior.setSaplActionEngine(this.myEngine);
			ontonutBehavior.ontonutId = this.ontonutId;
		}

		if(this.ontonutBehavior != null){ 
			ontonutBehavior.initializeOntonut();
			//re-assign the present behavior as parent of ontonut, just to enable custom logging
			ontonutBehavior.getOntonut().configure(this.ontonutId, 0, this, null);
		}			

		super.doAction();
	}

	@Override
	protected Servlet createServlet(){
		return new OntonutServlet(this);
	}
	
	protected class OntonutServlet extends ServerBehaviorServlet{
		
		private static final long serialVersionUID = 1L;

		public OntonutServlet(ReusableAtomicBehavior server) {
			super(server, REQUEST_TIMEOUT);
			if(OntonutListenerBehavior.INTERACTION_PRINT)
				print("[HttpServerOntonutListenerBehavior] Listening on port "+port);

		}

		@Override
		protected void handleRequest(HttpServletRequest request, HttpServletResponse response, String strRequest, String content) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);

			if(matchRequest!=null){
				int ind = strRequest.indexOf("?");
				String req;
				if(ind == -1) req = strRequest;
				else req = strRequest.substring(0, ind);
				if(!req.equals(matchRequest)){
					return;
				}
			}
			
			try{
				List<Map<String,String>> solutions = processMessage(content);

				if(solutions!=null && !solutions.isEmpty()){
					if(hostVar != null){
						String from = request.getRemoteHost();
						for(Map<String,String> solution : solutions)
							solution.put(hostVar, from);
					}
					
					handleSolutions(solutions, strRequest, content);
				}
			}
			catch(Exception e){
				handleException(e, content, request.getRemoteHost());
				try {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				} catch (IOException e1) {
					printException(e1, true);
				}
			}
		}

		protected void handleSolutions(List<Map<String,String>> solutions, String strRequest, String content){
			applyFilters(solutions);
			applyPrecondition(solutions);
			
			addConclusions(solutions);
		}
		
		protected List<Map<String,String>> processMessage(String message) throws Exception{
			 return ((HttpOntonut)ontonutBehavior.ontonut).processResponse(message);
		}
		
		protected void applyFilters(List<Map<String,String>> solutions){
			if(!solutions.isEmpty() && !ontonutBehavior.ontonut.filters.isEmpty()){
				Set<String> definedVars = new HashSet<String>(solutions.get(0).keySet());
				SaplQueryResultSet rs = new SaplQueryResultSet(solutions, definedVars);
				boolean r = hasBeliefs(new ArrayList<String>(ontonutBehavior.ontonut.filters.keySet()), SaplConstants.G, rs);
				if(r){
					solutions.clear();
					for(int i=0; i<rs.getNumberOfSolutions(); i++)
						solutions.add(rs.getSolution(i));
				}
				else solutions.clear();
			}
		}
		
		protected void applyPrecondition(List<Map<String,String>> solutions){
			if(!solutions.isEmpty() && ontonutBehavior.ontonut.preconditionQuery!=null){
				Set<String> definedVars = new HashSet<String>(solutions.get(0).keySet());
				SaplQueryResultSet rs = new SaplQueryResultSet(solutions, definedVars);
				boolean r = hasBeliefs(ontonutBehavior.ontonut.preconditionQuery, SaplConstants.G, rs);
				if(r){
					ArrayList<String> all = new ArrayList<String>();
					all.add(SaplConstants.ANYTHING);
					solutions.clear();
					solutions.addAll(rs.getSolutions(all));
				}
				else{
					printError("Could not finalize: "+solutions);
					solutions.clear();
				}
			}
		}
		
		protected void addConclusions(List<Map<String,String>> solutions){
			if(!solutions.isEmpty()){
				for(Map<String,String> vars : solutions){
					Resource eventID = requestIDGenerator.getNewResource();
					vars.put("this",eventID.toString());
					addBeliefs(ontonutBehavior.ontonutSemantics, vars);
				}
				wakeReasoner();
			}
		}
		
	}

	protected void handleException(Throwable e, String content, String host){
		printException(e, true);
	}
	
}
