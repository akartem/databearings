package sapl.shared.eii;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.sapln3.SaplN3Parser;

/** 
 * Ontonut representing a REST Web Service with content that is a text table of
 * delimited data items, e.g. comma-separated values (CSV)
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (02.05.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */ 

public class CsvOntonut extends HttpOntonut {
	protected String rDelimiter = null;
	protected String cDelimiter = null;
	protected boolean hasHeader = true;
	
	protected int numOfColumns = 0;

	@Override
	public boolean loadDefinition() {
		if(!super.loadDefinition())
			return false;

		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "{<"+this.id+"> o:rowDelimiter ?rowD} s:is s:Optional. {<"+this.id+"> o:columnDelimiter ?columnD} s:is s:Optional. {<"+this.id+"> o:hasHeader ?header} s:is s:Optional"; 
		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);

		if(resultSet!=null && resultSet.getNumberOfSolutions()>0){
			Map<String, String> def = resultSet.getSolution(0);
			this.rDelimiter = def.get("rowD");
			this.cDelimiter = def.get("columnD");

			String header = def.get("header");
			if(header!=null && header.equals("false")) this.hasHeader = false;
		}

		if(this.rDelimiter==null) this.rDelimiter = "\n";
		if(this.cDelimiter==null) this.cDelimiter = ",";

		return true;		
	}

	@Override
	public void findDataItems(String contextID, Map<String, String> result) {
		this.findDataItemsRecursion(contextID, result);
		super.findDataItems(contextID, result);
	}

	public void findDataItemsRecursion(String contextID, Map<String, String> result) {
		List<SemanticStatement> statements = this.parent.getStatements(contextID);
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"value") && st.isObjectVar()){
				int index = Integer.valueOf(st.subject.substring(SaplConstants.DATA_NS.length()));
				result.put(st.object, String.valueOf(index));
				if(index>numOfColumns) numOfColumns = index;
			}
			else if(st.isObjectContext()){
				this.findDataItemsRecursion(st.object, result);
			}
		}
	}


	@Override
	public List<Map<String,String>> processResponse(Object content){
		String contentStr = content.toString();
		
		List<Map<String,String>> sols = null;  

		String[] rows = contentStr.split(this.rDelimiter);

		for (int iRow = 0; iRow < rows.length; iRow++) {
			if(this.hasHeader && iRow==0) continue;
			if(rows[iRow].trim().isEmpty()) continue;

			Map<String,String> sol = new HashMap<String,String>();

			String[] cols = rows[iRow].trim().split(this.cDelimiter);

			for (int iCol = 0; iCol < cols.length; iCol++) {
				String var = this.dataItemsToSelect.get(String.valueOf(iCol+1));
				if(var!=null){
					sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), cols[iCol]);
				}
			}

			if(sol.size()>0){
				if(sols==null) sols = new ArrayList<Map<String,String>>();
				sols.add(sol);
			}
		}

		return sols;
	}
	
	@Override
	protected Object runUpdate(List<Map<String, String>> updates) throws Exception{
		
		if(updates.size()>0)
			this.bindRequestVars(updates.get(0));

		StringBuilder strUpdate = new StringBuilder();

		for(Map<String,String> update : updates){
			for(int i=1; i<=numOfColumns; i++){
				String value = null;
				String var = dataItemsToSelect.get(String.valueOf(i));
				if(var!=null){
					String varname = var.substring(SaplConstants.VARIABLE_PREFIX.length());
					value = update.get(varname);
				}
				
				if(value!=null) strUpdate.append(value);
				if(i!=numOfColumns)
					strUpdate.append(this.cDelimiter);
			}
			strUpdate.append(this.rDelimiter);
		}
		
		return this.sendUpdate(this.requestContent, strUpdate.toString()); //implemented in HttpOntonut
	}
	

	//Support of IDE 

	@Override
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables) throws Throwable{
		getDoc();

		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		ns += "#";

		String result = "";
		result += "* d:table {\n";
		if(this.lastContent!=null){
			result += "\t* d:row {\n";
	
			int numOfColumns = 0;
			String[] rows = ((String)this.lastContent).split(this.rDelimiter);
			for (int iRow = 0; iRow < rows.length; iRow++) {
				String[] cols = rows[iRow].split(this.cDelimiter);
				if(cols.length > numOfColumns) numOfColumns = cols.length;
			}
	
			String[] cols = rows[0].split(this.cDelimiter);
			for(int n=1; n<=numOfColumns; n++){
				String str = String.valueOf(n);
				String title = "0000".substring(0,4-str.length())+str;
				String varName;
				String prop;
				if(this.hasHeader && n <= numOfColumns){
					prop = cols[n-1].trim();
					varName = prop.toLowerCase();
				}
				else{
					prop = "column"+n;
					varName = title;
				}
				result += "\t\t d:"+title+" d:value "+SaplConstants.VARIABLE_PREFIX+varName+" .\n";
				
				if(fillVariables!=null) fillVariables.put(ns+prop, varName);
			}
	
			result += "\t} .\n";
		}
		result += "} .";
		return result;
	}	

	@Override
	public String generateOntologyDefinition() throws Throwable{
		getDoc();

		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		String className = ns.substring(ns.lastIndexOf("/")+1);
		ns += "#";
		
		String result = SaplN3Parser.PREFIX_KEYWORD+" rdf: <"+SaplConstants.RDF_NS+"> . "+
				SaplN3Parser.PREFIX_KEYWORD+" rdfs: <"+SaplConstants.RDFS_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" owl: <"+SaplConstants.OWL_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" ds: <"+ns+"> . \n"+
				"ds:ontology a owl:Ontology . \n";

		if(this.lastContent!=null){
			
			String result2 = "ds:"+className+" a rdfs:Class .\n";
	
			String[] rows = ((String)this.lastContent).split(this.rDelimiter);
			String[] cols = rows[0].split(this.cDelimiter);
			for(int i=0; i<cols.length; i++){
				if(this.hasHeader){
					String col = cols[i].trim();
					result2 += "ds:"+col+" a rdf:Property; rdfs:domain ds:"+className+" .\n";
				}
				else{
					result2 += "ds:column"+i+" a rdf:Property; rdfs:domain ds:"+className+" .\n";
				}
			}
			
			result +=result2;
		}
		
		return result;
	}	

}
