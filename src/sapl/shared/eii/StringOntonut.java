package sapl.shared.eii;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sapl.core.SaplConstants;
import sapl.core.SemanticStatement;

/** 
 * Ontonut representing a REST Web Service with content that is a single string of text
 * with different delimiters. Intended as setter, not a getter.
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (02.05.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */
 
public class StringOntonut extends HttpOntonut {

	protected Map<Integer,String> stringParts;
	
	@Override
	/**Result will map variables used in the semantics definition to column names*/
	public void findDataItems(String contextID, Map<String, String> result) {
		this.findDataItemsRecursion(contextID, result);
		super.findDataItems(contextID, result);
	}

	public void findDataItemsRecursion(String contextID, Map<String, String> result) {
		this.stringParts = new HashMap<Integer,String>();
		List<SemanticStatement> statements = this.parent.getStatements(contextID);
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
				int index = Integer.valueOf(st.subject.substring(SaplConstants.DATA_NS.length()));
				if(st.isObjectVar()){
					result.put(st.object, String.valueOf(index));
				}
				this.stringParts.put(index, st.object);
			}
			else if(st.isObjectContext()){
				this.findDataItemsRecursion(st.object, result);
			}
		}
	}
	

	/*Simplistic implementation: all content into one variable*/
	@Override
	public List<Map<String,String>> processResponse(Object content){
		
		String var;
		if(stringParts.size()==1){
			String key = String.valueOf(stringParts.keySet().iterator().next());
			var = dataItemsToSelect.get(key);
		}
		else{
			Integer[] keys = new Integer[stringParts.size()];
			stringParts.keySet().toArray(keys);
			Arrays.sort(keys);
			var = dataItemsToSelect.get(String.valueOf(keys[0]));
		}
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>(1);
		Map<String,String> sol = new HashMap<String,String>(1);
		result.add(sol);
		sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), String.valueOf(content));
		return result;
	}
	
	@Override
	protected String runUpdate(List<Map<String, String>> updates) throws Exception{

		if(updates.size()>0)
			this.bindRequestVars(updates.get(0));
		
		Integer[] keys = new Integer[stringParts.size()];
		stringParts.keySet().toArray(keys);
		Arrays.sort(keys);

		String result = null;
		for(Map<String, String> update : updates){

			StringBuilder updateData = new StringBuilder();

			for(int i=0; i<keys.length; i++){
				String var = dataItemsToSelect.get(String.valueOf(keys[i]));
				String value;
				if(var==null) value = stringParts.get(keys[i]);
				else{
					value = update.get(var.substring(SaplConstants.VARIABLE_PREFIX.length()));
					if(value==null) value = "";
				}
				
				updateData.append(value);
			}
			
			result = this.sendUpdate(this.requestContent, updateData.toString()); //implemented in HttpOntonut
		}
		
		return result;
	}

}
