package sapl.shared.eii;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.SaplN3Producer;
import sapl.util.WhiteSpaceString;

/** 
 * Ontonut representing a REST Web Service with JSON content
 * 
 * @author Artem Katasonov (VTT + Elisa from July 2018) 
 * 
 * @version 4.7 (14.02.2019)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class JsonOntonut extends HttpOntonut {

	Object json = null;
	
	Map<String, Boolean> canBeMultiple = new HashMap<String, Boolean>();
	
	@Override
	public boolean loadDefinition() {
		boolean initResult = super.loadDefinition();
		if(this.contentType==null) this.contentType = "application/json";
		return initResult;
	}
	
	@Override
	/**Result will map variables used in the semantics definition to path of JSON elements*/
	public void findDataItems(String contextID, Map<String, String> result) {
		this.findDataItemsRecursion(contextID, result, "");
		super.findDataItems(contextID, result);
	}

	public void findDataItemsRecursion(String contextID, Map<String, String> result, String currPath) {

		List<SemanticStatement> statements = this.parent.getStatements(contextID);

		Map<String, String> ids = new HashMap<String, String>();
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"attribute")){
				ids.put(st.subject, st.object);
				if(st.isObjectVar()){
					String namePath = currPath;
					namePath += (namePath.isEmpty()?"":"/") + "@" ; //attribute name
					result.put(st.object, namePath);
				}
			}
		}

		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
				if(st.isObjectVar()){
					String attribute = ids.get(st.subject);
					if(attribute.startsWith(SaplConstants.VARIABLE_PREFIX))
						attribute = "*"; //any
					String newPath = currPath;
					newPath += (newPath.isEmpty()?"":"/") +  attribute;
					result.put(st.object, newPath);
					
					canBeMultiple.put(newPath, st.isSubjectAny() || st.isSubjectBlankNode());
				}
			}

			if(st.isObjectContext()){
				String newPath = currPath;
				if(st.predicate.equals(SaplConstants.DATA_NS+"branch")){
					newPath += (newPath.isEmpty()?"":"/") + ids.get(st.subject) ;
				}
				
				canBeMultiple.put(newPath, st.isSubjectAny() || st.isSubjectBlankNode());
				
				this.findDataItemsRecursion(st.object, result, newPath);
			}
		}

	}

	@Override
	public List<Map<String,String>> processResponse(Object content) throws JSONException{
		String contentStr = content.toString().trim();
		
		if(contentStr.startsWith("[")){
			JSONArray array = new JSONArray(contentStr);
			List<Map<String,String>> result = null;	
			for(int i = 0; i<array.length();i++){
				Object elem = array.get(i);
				if(elem instanceof JSONObject){
					List<Map<String,String>> part = processResponseNode(elem, "", null);
					if(result==null) result = part;
					else result.addAll(part);
				}
			}
			return result;
		}
		else if(contentStr.startsWith("{")){
			JSONObject json = new JSONObject(contentStr);
			return processResponseNode(json, "", null);
		}
		else return null;
	}

	protected List<Map<String,String>> processResponseNode(Object node, String currPath, String lastItem) throws JSONException{

		if(!(node instanceof JSONObject || node instanceof JSONArray)){
			String var = this.dataItemsToSelect.get(currPath);
			String var2 = null;
			if(var==null){
				String withoutLastItem = currPath.substring(0,currPath.length()-lastItem.length());
				var = this.dataItemsToSelect.get(withoutLastItem+"*");
				var2 = this.dataItemsToSelect.get(withoutLastItem+"@");
			}
			if(var!=null || var2!=null){
				Map<String,String> sol = new HashMap<String,String>(1);
				List<Map<String,String>> sols = new ArrayList<Map<String,String>>(1);
				sols.add(sol);

				if(var!=null){
					sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), String.valueOf(node));
				}
				if(var2!=null){
					sol.put(var2.substring(SaplConstants.VARIABLE_PREFIX.length()), String.valueOf(lastItem));
				}

				return sols;
			}
			else return null;
		}
		else{

			List<Map<String,String>> solutions = null;

			if(node instanceof JSONObject){
				JSONObject json = (JSONObject)node;
				String[] names = JSONObject.getNames(json);
				
				if(names != null) {
					for(String name : names){
	
						String uName = name.toUpperCase();
	
						Object o = json.get(name);
						String newPath = currPath + (currPath.isEmpty()?"":"/") + uName;
	
						List<Map<String,String>> sols=null;
	
						if(o instanceof JSONArray){
							JSONArray array = (JSONArray)o;
							for(int i = 0; i<array.length();i++){
								Object elem = array.get(i);
								List<Map<String,String>> elem_sols = processResponseNode(elem, newPath, name);
								if(elem_sols!=null){
									if(sols==null) sols = elem_sols;
									else sols.addAll(elem_sols);
								}
							}
						}
						else{
							sols = processResponseNode(o, newPath, name);
						}
	
						List<Map<String,String>> newSolutions = new ArrayList<Map<String,String>>(); 					
						if(sols!=null){
							if(solutions==null) newSolutions.addAll(sols);
							else{
								for(Map<String,String> solution : solutions){
									for(Map<String,String> sol : sols){
										Map<String,String> toAdd = new HashMap<String,String> (solution);
										toAdd.putAll(sol);
										newSolutions.add(toAdd);
									}
								}
							}
						}
						if(!newSolutions.isEmpty())
							solutions = newSolutions;					
	
					}			
				}
			}

			return solutions;

		}	

	}
	
	
	@Override
	protected String runUpdate(List<Map<String, String>> updates) throws Exception{

		if(updates.size()>0)
			this.bindRequestVars(updates.get(0));
		
		JSONObject result = new JSONObject();
		for(Map<String,String> update : updates){
			
			Map<JSONArray, JSONObject> thisUpdateMembers = new HashMap<JSONArray, JSONObject>(1);
			Set<JSONObject> thisUpdateObjects = new HashSet<JSONObject>(1);
			
			for(String var : update.keySet()){
				String value = update.get(var);
				String item = this.dataItems.get(SaplConstants.VARIABLE_PREFIX+var);
				
				if(item!=null && !item.startsWith("%%")){
					String parts[] = item.split("/");
					
					if(parts[parts.length-1].equals("@")) continue; 
					else if(parts[parts.length-1].equals("*")){
						String attrNameItem = item.substring(0, item.length()-1)+"@";

						for(String v : this.dataItems.keySet()){
							if(this.dataItems.get(v).equals(attrNameItem)){
								String attrName = update.get(v.substring(SaplConstants.VARIABLE_PREFIX.length()));
								parts[parts.length-1] = attrName; 
								break;
							}
						}
					}
					
					JSONObject curr = result;
					String currPath = "";
					for(int i=0; i<parts.length; i++){
						String part = parts[i];
						currPath += (currPath.isEmpty()?"":"/")+part;
						if(i<parts.length-1){
							Object existing = curr.opt(part);
							if(existing!=null){
								if(existing instanceof JSONArray){
									JSONObject child = thisUpdateMembers.get((JSONArray)existing);
									if(child==null){
										child = new JSONObject();
										((JSONArray)existing).put(child);
										thisUpdateMembers.put((JSONArray)existing, child);
										thisUpdateObjects.add(child);
									}
									curr = child;								
								}
								else if(existing instanceof JSONObject){
									if(!thisUpdateObjects.contains((JSONObject)existing) 
											&& canBeMultiple.get(currPath)==true){
										JSONArray array = new JSONArray();
										array.put((JSONObject)existing);
										curr.put(part, array);
										JSONObject child = new JSONObject();
										array.put(child);
										thisUpdateMembers.put(array, child);
										thisUpdateObjects.add(child);
										curr = child;
									}
									else curr = (JSONObject)existing;
								}
								
							}
							else{
								JSONObject child = new JSONObject();
								curr.put(part, child);
								thisUpdateObjects.add(child);
								curr = child;
							}
						}
						else{
							Object existing = curr.opt(part);
							if(existing!=null){
								if(existing instanceof JSONArray){
									((JSONArray)existing).put(value);
								}
								else {
									if(canBeMultiple.get(currPath)==true){
										JSONArray array = new JSONArray();
										array.put(existing);
										curr.put(part, array);
										array.put(value);
									}
								}
							}
							else{
								curr.put(part, value);
							}
						}
					}
				}
			}
		}

		return this.sendUpdate( this.requestContent, result.toString(4) ); //implemented in HttpOntonut
	}
		

	//Support of IDE 
	
	@Override
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables)  throws Throwable{
		getDoc();
		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		ns += "#";
		
		String result = "";
		result += "* d:tree {\n";
		if(this.json != null){
			Map<String, Integer> vars = new HashMap<String, Integer>();
			result += processSyntaxExampleNode(this.json,1, vars, false,null,includeEntities,fillVariables,ns);
		}
		result += "} .";
		return result;
	}	
	
	@Override
	public String generateOntologyDefinition() throws Throwable{
		getDoc();
		String ns = this.uri;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		ns += "#";
		
		String result = SaplN3Parser.PREFIX_KEYWORD+" rdf: <"+SaplConstants.RDF_NS+"> . "+
				SaplN3Parser.PREFIX_KEYWORD+" rdfs: <"+SaplConstants.RDFS_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" owl: <"+SaplConstants.OWL_NS+"> . " +
				SaplN3Parser.PREFIX_KEYWORD+" ds: <"+ns+"> . \n"+
				"ds:ontology a owl:Ontology . \n";

		if(this.json != null){
			result += processSyntaxExampleNode(this.json,0,null,true,null,null,null,ns);
		}

		return result;
	}	
	
	@Override
	protected void getDoc() throws Throwable{
		if(this.json == null){
			this.addVarToItemsToSelect("", "");
			executeQuery(new SaplQueryResultSet(), null);
			String example = (String) this.lastContent;
			if(example.trim().startsWith("[")){
				JSONArray arr = new JSONArray(example);
				this.json = arr.getJSONObject(0);
			}
			else this.json = new JSONObject(example);
		}
	}	
	

	protected String processSyntaxExampleNode(Object node, int indentLevel, Map<String, Integer> vars, boolean doOntology, String currentClass, Set<String> includeEntities, Map<String,String> fillVariables, String namespace) throws JSONException{
		String result = "";
		String indent = WhiteSpaceString.getWhiteSpaceString(SaplN3Producer.indent_size*indentLevel);

		JSONObject json_node = (JSONObject)node;
		String[] names = JSONObject.getNames(json_node);
		for(String name : names){
			Object o = json_node.get(name);
			
			if(doOntology){
				String propName = name;
				if(o instanceof JSONObject || o instanceof JSONArray){
					if(o instanceof JSONArray)
						o = ((JSONArray)o).opt(0);
					if(o!=null){
						String className = name.substring(0,1).toUpperCase()+name.substring(1);
						String elementResult = processSyntaxExampleNode(o, indentLevel, vars, doOntology, className, includeEntities, fillVariables, namespace);	
						if(currentClass!=null){
							result += indent + "ds:"+propName+" a rdf:Property;";
							result += " rdfs:domain ds:"+currentClass+";";
							result += " rdfs:range ds:"+className+" .\n";
						}
						result += indent + "ds:"+className+" a rdfs:Class .\n";
						result += elementResult;
					}
				}
				else{
					if(currentClass!=null){
						result += indent + "ds:"+propName+" a rdf:Property;";
						result += " rdfs:domain ds:"+currentClass+";";
						result += " rdfs:range rdfs:Literal .\n";
					}
				}
			} else {
				result += indent + "[d:attribute \""+name+"\"] ";
				if(o instanceof JSONObject || o instanceof JSONArray){
					String inner = "";
					if(o instanceof JSONArray)
						o = ((JSONArray)o).opt(0);
					if(o!=null){
						if(o instanceof JSONObject)
							inner = processSyntaxExampleNode(o, indentLevel+1, vars, doOntology, currentClass, includeEntities, fillVariables, namespace);
						else{
							if(name.endsWith("s")) name=name.substring(0,name.length()-1);
							String varName = name.toLowerCase();
							Integer n = vars.get(name);
							if(n==null) n = 1;
							else{ n++; varName += n; }
							vars.put(name,n);
							result += "d:value "+SaplConstants.VARIABLE_PREFIX+varName+" .\n";
							if(fillVariables!=null) fillVariables.put(namespace+name, varName);
							continue;
						}
					}
					result += "d:branch {\n";
					result += inner;
					result += indent + "} . \n";
				}
				else{
					String varName = name.toLowerCase();
					Integer n = vars.get(name);
					if(n==null) n = 1;
					else{ n++; varName += n; }
					vars.put(name,n);
					result += "d:value "+SaplConstants.VARIABLE_PREFIX+varName+" .\n";
					if(fillVariables!=null) fillVariables.put(namespace+name, varName);
				}
			}
		}

		return result;
	}

}
