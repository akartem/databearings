package sapl.shared.eii;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;

/** 
 * Base class for ontonuts
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 5.1 (07.12.2019)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */ 

public abstract class Ontonut {

	public static boolean DEBUG_PRINT = false;

	protected String id;

	protected String shortId;

	protected int executionIndex;

	protected ReusableAtomicBehavior parent;
	
	protected String caseID;

	public static int LOG_SIZE = 1000000;
	public static int LOG_NUMBER = 10;
	
	protected Logger logger = null;
	private static List<Logger> loggers = null; //just to maintain strong references
	
	protected static Map<String,Map<String,Long>> lastLogRecords; 

	//Accessed when doing loadDefinition

	protected String[] logData = null;
	protected String[] logKey = null;
	protected long logInterval = 0;

	/**Query given via precondition*/
	public String preconditionQuery;	
	/**Query given via precondition*/
	public String postconditionQuery;
	
	public Set<String> inputVars = null;

	//Another ontonut handling the request side (if this one only handles the response side)
	public String requestOntonutID = null;
	public Ontonut requestOntonut = null;
	public String requestOntonutSemantics = null;
	
	/**New variables created via precondition query*/
	protected Set<String> preconditionQueryVars = null;
	protected Map<String,String> preconditionQueryVarsValues = null; //for updates only

	//Structures filled in when internally analyzing the syntax part 

	/**In case wrappings are used, they are recorded here*/
	protected SaplQueryResultSet wrappings = null;

	/**Map produced in findDataItems: ontonut-used variables to source data items paths*/
	protected Map<String, String> dataItems;
	/**Ontonut-internal additional statements*/
	protected Map<String, SemanticStatement> filters = new LinkedHashMap<String, SemanticStatement>();
	/**Postcondition filters*/
	protected Map<String, SemanticStatement> postconditions = new LinkedHashMap<String, SemanticStatement>();
	/**All ontonut-used variables used in ontonut-internal, and postcondition, statements*/
	protected Set<String> filterAndPostconditionVars = new HashSet<String>();

	//Structures filled after decomposition of the S-APL query by the UniversalAdapter 

	/**Map of needed source data items paths to ontonut-used variables*/
	protected Map<String,String> dataItemsToSelect = new HashMap<String,String>();

	//CHANGE 20.1.2017: Map of ontonut-used variables to a set of corresponding S-APL query variables or constants comes as a whole from query decomposition
	/**Map of ontonut-used variables to a set of corresponding S-APL query variables or constants*/
	protected Set<Map<String,String>> selectVariableMappings = null;
	
	/**Variables defined via ontonut-internal filters with mappings to S-APL query variables*/
	protected Map<String,String> probableFilterDefinedVars = new HashMap<String,String>();

	public void configure(String id, int executionIndex, ReusableAtomicBehavior parent, String caseID){
		this.id = id;
		if(id.contains("#"))
			this.shortId = id.substring(id.lastIndexOf("#")+1);
		else this.shortId = id.substring(id.lastIndexOf("/")+1);
		this.executionIndex = executionIndex;
		this.parent = parent;
		this.caseID = caseID;
		
		if(this.requestOntonut!=null) // may be, if parent calls for re-configuration
			this.requestOntonut.configure(this.requestOntonutID, this.executionIndex, this.parent, this.caseID);
	}

	/**Load definition of the ontonut from beliefs based on its known ID*/
	public boolean loadDefinition(){
		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "{<"+this.id+"> o:precondition ?pre} s:is s:Optional. "; 
		query += "{<"+this.id+"> o:input ?input} s:is s:Optional. "; 
		query += "{<"+this.id+"> o:postcondition ?post} s:is s:Optional. "; 
		query += "{<"+this.id+"> o:log ?log. {?log o:logWrite ?logWrite} s:is s:Optional. {?log o:logInterval ?logInterval} s:is s:Optional. {?log o:logKey ?logKey} s:is s:Optional } s:is s:Optional. "; 

		query += "{<"+this.id+"> o:service ?o. ?o a o:Ontonut; o:type ?type; o:semantics { ?data => ?semantics } } s:is s:Optional. "; 

		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);
		
		if(resultSet!=null && resultSet.getNumberOfSolutions()>0){
			
			Map<String, String> def = resultSet.getSolution(0);
			this.preconditionQuery = def.get("pre");
			this.postconditionQuery = def.get("post");

			String log = def.get("logWrite");
			if(log!=null){
				this.logData = log.split(",");
				for(int i=0; i<this.logData.length; i++){
					this.logData[i] = this.logData[i].trim().substring(SaplConstants.VARIABLE_PREFIX.length()); 
				}
				logger = Logger.getLogger(this.id);
				if(logger.getHandlers().length==0){
					if(loggers==null) loggers = new ArrayList<Logger>();
					loggers.add(logger);
					try {
						logger.setLevel(Level.FINE);
						Handler fh = new FileHandler("log/"+this.shortId+"_%g.log", LOG_SIZE, LOG_NUMBER);
						fh.setFormatter(new SimpleFormatter());
						logger.addHandler(fh);
						logger.setUseParentHandlers(false);		
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				String logKey = def.get("logKey");
				if(logKey!=null){
					this.logKey = logKey.split(",");
					for(int i=0; i<this.logKey.length; i++){
						this.logKey[i] = this.logKey[i].trim().substring(SaplConstants.VARIABLE_PREFIX.length());
					}
				}

				String logInt = def.get("logInterval");
				if(logInt != null){
					try{
						this.logInterval = Long.valueOf(logInt);
						if(Ontonut.lastLogRecords==null)
							Ontonut.lastLogRecords = new HashMap<String, Map<String,Long>>();
						if(Ontonut.lastLogRecords.get(this.id)==null)
							Ontonut.lastLogRecords.put(this.id, new HashMap<String,Long>());
					}
					catch(NumberFormatException e){
						printToErr("o:logInterval provided is not an integer number");
					}
				}
			}
			
			String input = def.get("input");
			if(input!=null){
				inputVars = new HashSet<String>();
				inputVars.add(input.substring(SaplConstants.VARIABLE_PREFIX.length()));
				if(resultSet.getNumberOfSolutions()>1){
					for(int i=1; i<resultSet.getNumberOfSolutions(); i++){
						Map<String, String> sol = resultSet.getSolution(i);
						input = sol.get("input");
						if(input!=null)
							inputVars.add(input.substring(SaplConstants.VARIABLE_PREFIX.length()));
					}
				}
			}
			
			//Initialize separate request-side ontonut, if specified
			this.requestOntonutID = def.get("o");
			if(this.requestOntonutID!=null){
				String type = def.get("type");
				this.requestOntonutSemantics = def.get("semantics");
				String syntax = def.get("data");
				try {
					@SuppressWarnings("unchecked")
					Class<Ontonut> cl = (Class<Ontonut>) Class.forName(type);
					this.requestOntonut = cl.newInstance();
				} catch (Throwable e) {
					throw new Error ("Ontonut "+this.requestOntonutID+" has unknown type "+ type); 
				}

				this.requestOntonut.configure(this.requestOntonutID, this.executionIndex, this.parent, this.caseID);

				if(!this.requestOntonut.loadDefinition()){
					throw new Error ("Failed to load definition of the ontonut "+this.requestOntonutID); 
				}
				
				Map<String,String> requestOntonutDataItems = new LinkedHashMap<String,String>();
				this.requestOntonut.findSyntaxDataItems(syntax, requestOntonutDataItems); 
			}
			
		}
		return true;
	}

	/**Produce map between data items and variables used in o:semantics*/
	public abstract void findDataItems(String contextID, Map<String, String> map);

	/**Process the syntax part of o:semantics*/
	public void findSyntaxDataItems(String contextID, Map<String, String> map){
		SaplQueryResultSet resultSet =  new SaplQueryResultSet();
		String newContextID = this.parent.unwrapQuery(contextID, resultSet, true);
		if(newContextID!=contextID){
			this.wrappings = resultSet;
			contextID = newContextID;
		}

		//involve specific ontonut
		this.findDataItems(contextID, map);
		this.dataItems = new LinkedHashMap<String, String>(map);

		//check additional statements given
		this.findAdditionalStatements(contextID, false);

		if(this.postconditionQuery!=null)
			this.findAdditionalStatements(this.postconditionQuery, true);
	}

	protected void findAdditionalStatements(String contextID, boolean inPostcondition){
		List<String> ids = new ArrayList<String>();
		List<SemanticStatement> statements = this.parent.getStatements(contextID, ids);
		for(int i = 0; i < statements.size(); i++){
			SemanticStatement st = statements.get(i);
			
			if(inPostcondition || !isSyntaxStatement(st)){
				String stID = ids.get(i); 
				if(inPostcondition)
					postconditions.put(stID, st);
				else filters.put(stID, st);

				Set<String> required = new HashSet<String>();
				Set<String> defined = new HashSet<String>();
				
				this.parent.findAllVarsInStatement(st, required, defined);
				
				filterAndPostconditionVars.addAll(required);
				filterAndPostconditionVars.addAll(defined);
			}

		}
	}
	
	protected boolean isSyntaxStatement(SemanticStatement st){
		return st.predicate.startsWith(SaplConstants.DATA_NS);
	}

	protected void addVarToItemsToSelect(String dataItem, String variable){

		if(!dataItem.startsWith("\"") && !dataItem.startsWith("%%")) dataItem = dataItem.toUpperCase();

		dataItemsToSelect.put(dataItem, variable);
		int ind = dataItem.indexOf(".");
		if(ind!=-1){
			String localname = dataItem.substring(ind+1);
			dataItemsToSelect.put(localname, variable);
		}
	}

	protected void addToSelectVarsMap(String ontonutVar, String saplVarOrConstant){
		//CHANGE 20.1.2017 + fix 06.02.2017 + fix 29.03.1017
		if(selectVariableMappings==null){
			selectVariableMappings = new HashSet<Map<String, String>>(1);
			selectVariableMappings.add(new HashMap<String, String>());
		}
		for(Map<String,String> option : selectVariableMappings){
			String key;
			if(saplVarOrConstant.startsWith(SaplConstants.VARIABLE_PREFIX)){
				key = saplVarOrConstant.substring(SaplConstants.VARIABLE_PREFIX.length());
				if(!option.containsKey(key)) option.put(key, SaplConstants.VARIABLE_PREFIX+ontonutVar);
			}
			else{
				key = "\""+saplVarOrConstant+"\"";
				if(!option.containsKey(key)) option.put(key, SaplConstants.VARIABLE_PREFIX+ontonutVar);
				else{
					option.put(key, option.get(key)+" "+SaplConstants.VARIABLE_PREFIX+ontonutVar);
				}
			}
		}
	}


	public void setSelectVariableMappings(Set<Map<String, String>> mappings){
		this.selectVariableMappings = mappings;
	}
		
	/** Register a basic query condition
	 * @param dataItem Address of a data item in the data
	 * @param value Either variable name or a constant used in S-APL query
	 * @param isValueString If value is a constant, specifies if this is a string or a number
	 * @param variable Original variable name used for this data item in o:semantic
	 * */
	public void addSelectionCondition(String dataItem, String value, boolean isValueString, String variable){
		if(DEBUG_PRINT)
			if(value.startsWith(SaplConstants.VARIABLE_PREFIX) && value.length()>SaplConstants.VARIABLE_PREFIX.length())			
				print("select "+dataItem+" as "+variable+" rename "+value);
			else print("select "+dataItem+" as "+variable+" must-equal "+value);

		if(variable!=null){
			addVarToItemsToSelect(dataItem, variable);
			addToSelectVarsMap(variable.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
		}

		if(dataItem.startsWith("\"") && dataItem.endsWith("\"")){
			String str = dataItem.substring(1,dataItem.length()-1);
			if(str.startsWith(SaplConstants.VARIABLE_PREFIX) ){
				probableFilterDefinedVars.put(str.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
			}
		}
	}

	/** Register a filter (variable comparison) condition*/
	public boolean addFilterCondition(String dataItem, String operation, String value, boolean isValueString, String variable){
		if(DEBUG_PRINT)
			print("filter "+dataItem+" (as "+variable+") "+operation+" "+value);

		return false;
	}

	/** Register an OR condition*/
	
	public boolean addOrCondition(List<String> dataItems, List<String> operations, List<String> values,
			List<Boolean> isValuesString, List<String> variables) {
		
		if(DEBUG_PRINT)
			print("or "+dataItems+" (as "+variables+") "+operations+" "+values);
		
		return false;
	}

	/** Register a counting condition*/
	public boolean addCountCondition(String vars, String newVarName, String saplVariable){
		if(DEBUG_PRINT)
			print("count "+vars+" as "+newVarName+" rename "+saplVariable);

		String tempVar = SaplConstants.QUERY_CONTEXT_PREFIX+saplVariable.substring(SaplConstants.VARIABLE_PREFIX.length());
		addVarToItemsToSelect(newVarName, tempVar);
		addToSelectVarsMap(tempVar.substring(SaplConstants.VARIABLE_PREFIX.length()), saplVariable);

		return false;
	}

	/** Register an expression condition (new variable definition)*/
	public boolean addExpressionCondition(String expression, String newVarName, String saplVariable) {
		if(DEBUG_PRINT)
			print("expression "+expression+" as "+newVarName+" rename "+saplVariable);

		String tempVar = SaplConstants.QUERY_CONTEXT_PREFIX+saplVariable.substring(SaplConstants.VARIABLE_PREFIX.length());
		addVarToItemsToSelect(newVarName, tempVar);
		addToSelectVarsMap(tempVar.substring(SaplConstants.VARIABLE_PREFIX.length()), saplVariable);

		return false;
	}

	public boolean addMaxCondition(String var, String newVarName, String saplVariable){
		if(DEBUG_PRINT)
			print("max "+var+" as "+newVarName+" rename "+saplVariable);

		String tempVar = SaplConstants.QUERY_CONTEXT_PREFIX+saplVariable.substring(SaplConstants.VARIABLE_PREFIX.length());
		addVarToItemsToSelect(newVarName, tempVar);
		addToSelectVarsMap(tempVar.substring(SaplConstants.VARIABLE_PREFIX.length()), saplVariable);

		return false;
	}

	public boolean addMinCondition(String var, String newVarName, String saplVariable){
		if(DEBUG_PRINT)
			print("min "+var+" as "+newVarName+" rename "+saplVariable);

		String tempVar = SaplConstants.QUERY_CONTEXT_PREFIX+saplVariable.substring(SaplConstants.VARIABLE_PREFIX.length());
		addVarToItemsToSelect(newVarName, tempVar);
		addToSelectVarsMap(tempVar.substring(SaplConstants.VARIABLE_PREFIX.length()), saplVariable);

		return false;
	}

	public boolean addSumCondition(String var, String newVarName, String saplVariable){
		if(DEBUG_PRINT)
			print("sum "+var+" as "+newVarName+" rename "+saplVariable);

		String tempVar = SaplConstants.QUERY_CONTEXT_PREFIX+saplVariable.substring(SaplConstants.VARIABLE_PREFIX.length());
		addVarToItemsToSelect(newVarName, tempVar);
		addToSelectVarsMap(tempVar.substring(SaplConstants.VARIABLE_PREFIX.length()), saplVariable);

		return false;
	}

	public boolean addAvgCondition(String var, String newVarName, String saplVariable){
		if(DEBUG_PRINT)
			print("avg "+var+" as "+newVarName+" rename "+saplVariable);

		String tempVar = SaplConstants.QUERY_CONTEXT_PREFIX+saplVariable.substring(SaplConstants.VARIABLE_PREFIX.length());
		addVarToItemsToSelect(newVarName, tempVar);
		addToSelectVarsMap(tempVar.substring(SaplConstants.VARIABLE_PREFIX.length()), saplVariable);

		return false;
	}

	public void addObjectID(String object) {
		if(DEBUG_PRINT)
			print("add object ID "+object);
	}

	public boolean addNegativeObjectID(String object) {
		if(DEBUG_PRINT)
			print("add negation object ID "+object);
		return false;
	}

	/**Translates S-APL log predicate into ontonut specific log operation*/
	public String getLogOperation(String predicate){
		String operation=null;
		if(predicate.equals(SaplConstants.eq) && (operation="=")!=null ||
				predicate.equals(SaplConstants.neq) && (operation="<>")!=null ||
				predicate.equals(SaplConstants.lt) && (operation="<")!=null ||
				predicate.equals(SaplConstants.lte) && (operation="<=")!=null ||
				predicate.equals(SaplConstants.gt) && (operation=">")!=null ||
				predicate.equals(SaplConstants.gte)&& (operation=">=")!=null )
			return operation;
		else return null;
	}

	/**Handles a tokenized S-APL expression*/
	public void processExpression (List<String> tokens){
	}


	/**Send request to the source and produce the final result set*/
	final public List<Map<String,String>> executeQuery(SaplQueryResultSet localSolutions, List<Map<String,String>> ontonutErrorSolutions) throws Throwable{

		//some data items may not be as such mapped to the output structure,
		//but needed in filter or postcondition statements -> have to select those too
		for(String filterVar: filterAndPostconditionVars){
			String dataItem = dataItems.get(SaplConstants.VARIABLE_PREFIX+filterVar);
			if(dataItem!=null){
				addVarToItemsToSelect(dataItem, SaplConstants.VARIABLE_PREFIX+filterVar);
			}
		}

		//CHANGE 07.02.2017 + moved lower 10.04.2017
		if(dataItemsToSelect.isEmpty()){
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("The list of data items to select is empty");
			return null;
		}

		//Add to localSolutions parameters defined via query in o:precondition {}.
		//Plus, detect any new variables created by this query. 
		//This will also act as a filter condition.
		if(preconditionQuery!=null){
			
			if(inputVars!=null){
				localSolutions.addDefinedVars(inputVars);
			}
			
			Set<String> inputSolutionsVars = null;
			if(localSolutions.getNumberOfSolutions()>0){
				inputSolutionsVars = localSolutions.getDefinedVars();
			}
			
			long s = System.currentTimeMillis();
			boolean r = this.parent.hasBeliefs(preconditionQuery, SaplConstants.G, localSolutions);
			if(r){
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("After precondition query: "+localSolutions.getNumberOfSolutions()+" solutions ["+(System.currentTimeMillis()-s)+"] ");

				if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
					print("Preconditon solutions: "+localSolutions);

				if(localSolutions.getNumberOfSolutions()>0){
					this.preconditionQueryVars = localSolutions.getDefinedVars();
					if(inputSolutionsVars!=null) this.preconditionQueryVars.removeAll(inputSolutionsVars);
				}
			} 
			else {
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("Precondition expression is false");
				return null;
			}
		}		

		List<Map<String,String>> sols = this.runQuery(localSolutions, ontonutErrorSolutions);

		//Log solutions
		if(sols!=null && this.logData!=null && this.logData.length>0){
			boolean doLog = true;
			
			String key = "";
			if(this.logKey!=null && this.logKey.length>0){
				 key = makeLogKey(localSolutions);
			}
			
			if(this.logInterval != 0){
				Long last = Ontonut.lastLogRecords.get(this.id).get(key);
				
				long now = System.currentTimeMillis();
				if(last==null || now >= last + this.logInterval){
					Ontonut.lastLogRecords.get(this.id).put(key, now);
				}
				else doLog = false;
			}
			
			if(doLog){
				Set<String> foundVars = new HashSet<String>();
				String toLog = makeLogRecord(sols, foundVars);
				
				String logRecord = "["+ReusableAtomicBehavior.getCurrentTime()+"] ("+((UniversalAdapterBehavior)parent).executionID+") ";
				if(key!=null) logRecord += key.toString()+" ";
				logRecord += "-> "+sols.size();
				if(foundVars.size()==this.logData.length){
					logRecord +=": "+toLog;
				}
				logger.log(Level.FINE, logRecord);
			}
		}

		//SaplAgent.printMemory("After query", true);

		if(sols==null){
			if(postconditionQuery!=null && (ontonutErrorSolutions==null || ontonutErrorSolutions.isEmpty()))
				throw new Error("Postcondition is defined but no solutions");
			return null;
		}

		if(sols.size()>0){
			//add solution ids
			for(int i=0; i<sols.size(); i++){
				Map<String,String> sol = sols.get(i);
				if(sol.get("")==null)
					sol.put("", "."+this.executionIndex+"."+String.valueOf(i+1));
			}

			//SaplAgent.printMemory("After IDs", true);

			long s = System.currentTimeMillis();

			//check postconditions if given
			if(postconditionQuery!=null){
				if(UniversalAdapterBehavior.DEBUG_PRINT)
					print("Postconditions::"+postconditions);

				Set<String> definedVars = new HashSet<String>(sols.get(0).keySet());
				SaplQueryResultSet rs = new SaplQueryResultSet(sols, definedVars);
				boolean result = this.parent.hasBeliefs(new ArrayList<String>(postconditions.keySet()), SaplConstants.G, rs);

				if(!result){
					throw new Error("Postcondition is false on "+sols);
				}

				sols.clear();
				for(int i=0; i<rs.getNumberOfSolutions(); i++)
					sols.add(rs.getSolution(i));
			}			
				
			//apply ontonut-local filters and s:expressions
			if(!filters.isEmpty()){

				if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
					print("Solutions before ontonut-local filters::"+sols);

				if(UniversalAdapterBehavior.DEBUG_PRINT)
					print("Ontonut-local filters::"+filters);

				Set<String> definedVars = new HashSet<String>(sols.get(0).keySet());

				SaplQueryResultSet rs = new SaplQueryResultSet(sols, definedVars);
				
				//SaplQueryEngine.DEBUG_PRINT = true;
				boolean result = this.parent.hasBeliefs(new ArrayList<String>(filters.keySet()), SaplConstants.G, rs);
				//SaplQueryEngine.DEBUG_PRINT = false;
				//SaplAgent.printMemory("After filters", true);

				if(!result){
					if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
						print("After ontonut-local filters: 0 solutions ["+(System.currentTimeMillis()-s)+"] ");
					return null;
				}
				
				sols.clear();
				for(int i=0; i<rs.getNumberOfSolutions(); i++)
					sols.add(rs.getSolution(i));

				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("After ontonut-local filters: "+sols.size()+" solutions ["+(System.currentTimeMillis()-s)+"] ");

				Set<String> newVars = rs.getDefinedVars();
				newVars.removeAll(definedVars);
				for(String newVar : newVars){
					String saplVar = probableFilterDefinedVars.get(newVar);
					if(saplVar!=null){
						addToSelectVarsMap(newVar, saplVar);
					}
				}

				s = System.currentTimeMillis();
			}

			
			if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
				print("Solutions before modifiers and renaming::"+sols);

			
			//FIX 19.09.2019: Apply solution set modifiers before renaming
			if(wrappings!=null){
				wrappings.setSolutions(sols, null);
				sols = wrappings.getSolutions();
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("After ontonut-local solution set modifiers: "+sols.size()+" solutions");

				if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
					print("Solutions after set modifiers::"+sols);

			}
			
			//rename variables to match original S-APL query
			List<Map<String,String>> solsRenamed = new ArrayList<Map<String,String>>(sols.size()*selectVariableMappings.size());
			
			boolean wasEmpty = sols.isEmpty(); 
			if(wasEmpty){ //create one solution for constant values if any
				Map<String, String> constantsSol = new HashMap<String, String>();
				constantsSol.put("", "."+this.executionIndex+"."+String.valueOf(1));
				sols.add(constantsSol);
			}
			for(Map<String,String> sol : sols){
				
				//CHANGE 20.1.2017 : Allow several options resulting in splitting solutions, do not use old selectVarsMap
				//+ fix 27.03.2017
				int counter = 0;
				for(Map<String,String> option: selectVariableMappings){
					counter ++;
					boolean ok = true;
					Map<String,String> solRenamed = new HashMap<String,String>(sol.size());
	
					for(String renamed : option.keySet()){
						String original = option.get(renamed);

						String value = null;
						List<String> values = null; //only for a case when the same constant value is used for several vars  
						if(original.startsWith(SaplConstants.VARIABLE_PREFIX)){
							if(!original.contains(" ")){
								value = sol.get(original.substring(SaplConstants.VARIABLE_PREFIX.length()));
							}
							else{
								String origVars[] = original.split("\\s");
								values = new ArrayList<String>(2);
								for(String ov : origVars){
									String val = sol.get(ov.substring(SaplConstants.VARIABLE_PREFIX.length()));
									if(val!=null) values.add(val);
								}
							}
						}
						else{ // a constant or blank node in the ontonut semantics part
							value = original;
							if(value.startsWith(SaplConstants.BLANK_NODE_PREFIX)){
								value = ((UniversalAdapterBehavior)parent).bnodeGenerator.getNewBlankNode();
							}
						}
						
						if(renamed.startsWith("\"") && renamed.endsWith("\"")){ //if was a constant in the S-APL query, must match
							renamed = renamed.substring(1, renamed.length()-1);
							if(values!=null){
								for(String v : values){
									if(!v.equals(renamed)){
										ok = false;
										break;
									}
								}
								if(!ok) break;
							}
							else{
								if(!value.equals(renamed)){
									ok = false;
									break;
								}
							}
						}
						else {
							if(value!=null) solRenamed.put(renamed, value);
						}
					}
					
					if(ok){
						if(!solRenamed.isEmpty()){
							if(selectVariableMappings.size()==1)
								solRenamed.put("", sol.get(""));
							else solRenamed.put("", sol.get("")+"."+counter);
							solsRenamed.add(solRenamed);
						}
					}
				}
			}
			
			if(solsRenamed.size()!=sols.size())
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("After renaming and constant-based filtering: "+solsRenamed.size()+" solutions");

			sols = solsRenamed;
			
			if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
				print("Solutions after constant-based filtering and variable renaming::"+sols);

			//SaplAgent.printMemory("After renaming", true);

		}

		//CHANGE 20.1.2017 : Removed as already handled, an in a way allowing multiple options, above
		//add constant values
//		if(constantValues.size()>0){
//			if(sols.size()>0){
//				for(int i=0; i<sols.size(); i++){
//					Map<String,String> sol = sols.get(i);
//
//					for(String cVar : constantValues.keySet()){
//						if(!sol.containsKey(cVar)){
//							String cValue =  constantValues.get(cVar);
//							if(cValue.startsWith(SaplConstants.BLANK_NODE_PREFIX)){
//								cValue = ((UniversalAdapterBehavior)parent).bnodeGenerator.getNewBlankNode();
//							}
//							sol.put(cVar, cValue);
//						}
//					}
//				}
//			}
//			else {
//				constantValues.put("", "."+this.executionIndex+"."+String.valueOf(1));
//				sols.add(constantValues);
//			}
//
//			if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
//				print("Solutions after adding constants::"+sols);
//		}

		//Log solutions
//		if(sols!=null && this.logData){
//			logger.log(Level.FINE, "["+ReusableAtomicBehavior.getCurrentTime()+"] Post-processed "+sols.size()+": "+sols.toString());
//		}

		return sols;
	}

	/**Send request to the source and produce the draft result set*/
	protected List<Map<String,String>> runQuery(SaplQueryResultSet localSolutions, List<Map<String,String>> ontonutErrorSolutions) throws Throwable{
		this.parent.printError(this.id+":: "+this.getClass().getName()+" does not implement 'runQuery()'");
		return null;
	}

	/**Send update to the the source*/
	final public Object executeUpdate(List<Map<String, String>> updates) throws Throwable {
		
		if(this.requestOntonut!=null){
			SaplQueryResultSet rs = new SaplQueryResultSet();
			rs.addSolutions(updates);
			List<Map<String, String>> sols = executeQuery(rs, null);
			return sols;
		}
		
		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			print("Processing "+updates.size()+" updates");

		if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
			print("Updates before variable renaming::"+updates);
			
		//add variables without direct mapping to data items so they are renamed properly and are not dropped,
		//as these are likely used in the pre-condition query to produce the actual data items 
		for(String var : probableFilterDefinedVars.keySet()){
			String saplVar = probableFilterDefinedVars.get(var);
			if(saplVar!=null){
				addToSelectVarsMap(var, saplVar);
			}
		}
		
		//CHANGE 28.08.2017: Do not lose, during rename, "input" variables that exploited in preconditions while not being used in the ontonut otherwise  
		Set<String> preconditionVars = null; 
		if(this.preconditionQuery!=null){
			preconditionVars = new HashSet<String>();
			List<SemanticStatement> statements = this.parent.getStatements(this.preconditionQuery);
			for(SemanticStatement st : statements){
				Set<String> required = new HashSet<String>();
				this.parent.findAllVarsInStatement(st, required, null);
				preconditionVars.addAll(required);
			}
		}
		
		//rename variables to match ontonut-internal variables
		//CHANGE 21.01.2017: Use new holistic selectVariableMappings
		//+ fix 27.03.2017 + fix 14.08.2017
		if(selectVariableMappings != null) {
			List<Map<String,String>> solsRenamed = new ArrayList<Map<String,String>>(updates.size()*selectVariableMappings.size());
			
			for(Map<String,String> sol : updates){
				for(Map<String,String> option: selectVariableMappings){
					Map<String,String> solRenamed = new HashMap<String,String>(sol.size());
					solsRenamed.add(solRenamed);
					for(String saplVarOrConstant : option.keySet()){
						String localVar = option.get(saplVarOrConstant);
						if(localVar.startsWith(SaplConstants.VARIABLE_PREFIX)){
							String value;
							if(saplVarOrConstant.startsWith("\"") && (saplVarOrConstant.endsWith("\""))){
								 value = saplVarOrConstant.substring(1,saplVarOrConstant.length()-1);
								 if(!localVar.contains(" "))
									solRenamed.put(localVar.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
								 else{
									 String localVars[] = localVar.split("\\s");
									 for(String lv : localVars)
										solRenamed.put(lv.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
								 }
							}
							else{
								value = sol.get(saplVarOrConstant);
								if(value!=null)
									solRenamed.put(localVar.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
							}
						}
					}
					//CHANGE 28.08.2017
					if(preconditionVars!=null && !preconditionVars.isEmpty()){
						for(String var : sol.keySet()){
							if(!solRenamed.containsKey(var)){
								String value = sol.get(var);
								if(value!=null)
									solRenamed.put(var,value);
							}
						}
					}
				}
			}
			
			if(solsRenamed.size()!=updates.size())
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("After renaming: "+solsRenamed.size()+" updates");
	
			updates = solsRenamed;

		}

		if(this.preconditionQuery!=null){
			long s = System.currentTimeMillis();

			Set<String> definedVars = new HashSet<String>(updates.get(0).keySet());
			SaplQueryResultSet rs = new SaplQueryResultSet(updates, definedVars);
			
			boolean r = this.parent.hasBeliefs(preconditionQuery, SaplConstants.G, rs);
			if(r){
				
				if(rs.forAll.size() > 0){
					updates = rs.getSolutions();
				}
				else {
					ArrayList<String> all = new ArrayList<String>();
					all.add(SaplConstants.ANYTHING);
					updates = rs.getSolutions(all);
				}
				
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("After precondition query: "+updates.size()+" updates ["+(System.currentTimeMillis()-s)+"] ");

				if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
					print("Preconditon updates: "+updates);

				if(rs.getNumberOfSolutions()>0){
					this.preconditionQueryVars = rs.getDefinedVars();
					this.preconditionQueryVars.removeAll(definedVars);
					this.preconditionQueryVarsValues = updates.get(0); 
				}
				
			} 
			else {
				if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
					print("Precondition expression is false");
				return null;
			}
		}			

		if(UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT)
			print("Updates after variable renaming::"+updates);		
		
		return this.runUpdate(updates);
	}

	/**Send update to the the source*/
	protected Object runUpdate(List<Map<String, String>> updates) throws Exception{
		this.parent.printError(this.shortId+":: "+this.getClass().getName()+" does not implement 'runUpdate()'");
		return null;
	}

	public void print(Object text) {
		this.parent.print(this.shortId+":: "+text);
	}
	public void print(Object text, int subindex) {
		this.parent.print(this.shortId+" ("+subindex+"):: "+text);
	}
	
	public void printToErr(Object text) {
		this.parent.printToErr(this.shortId+":: "+text);
	}
	public void printToErr(Object text, int subindex) {
		this.parent.printToErr(this.shortId+" ("+subindex+"):: "+text);
	}
	
	protected String makeLogRecord(List<Map<String,String>> sols, Set<String> foundVars){
		StringBuilder result = new StringBuilder();
		
		for(Map<String,String> sol : sols){
			StringBuilder record = new StringBuilder();
			for(int i=0; i<this.logData.length;i++){
				String var = this.logData[i];
				String value = sol.get(var);
				if(i > 0) record.append(',');
				if(value!=null){
					record.append(value);
					foundVars.add(var);
				}
			}
			if(result.length()>0) result.append(',');
			result.append('{');
			result.append(record);
			result.append('}');
		}
		return result.toString();
	}
	
	protected String makeLogKey(SaplQueryResultSet localSolutions){
		
		List<String> records = new ArrayList<String>();
		for (int i=0; i<localSolutions.getNumberOfSolutions(); i++) {
			Map<String,String> sol = localSolutions.getSolution(i);
			StringBuilder keyRecord = new StringBuilder();
			for(String key : this.logKey){
				String v = sol.get(key);
				if(v!=null){
					if(keyRecord.length()>0) keyRecord.append(',');
					keyRecord.append(key);
					keyRecord.append('=');
					keyRecord.append(v);
				}
			}
			if(keyRecord.length()>0){
				String str = keyRecord.toString();
				if(!records.contains(str))
					records.add(str);
			}
		}
		if(records.isEmpty()) return "[]";

		Collections.sort(records);
		StringBuilder result = new StringBuilder();
		result.append('[');
		for(String record : records){
			if(result.length()>1) result.append(','); 
			result.append('{');
			result.append(record);
			result.append('}');
		}
		result.append(']');
		return result.toString();
	}
	
	/**In support to IDE, produce the syntax definition from a data example
	 * @throws Exception */
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables) throws Throwable{
		throw new Error(this.shortId+":: "+this.getClass().getName()+" does not implement 'generateSyntaxDefinition()'");
	}

	/**In support to IDE, produce the ontology from a data example
	 * @throws Exception */
	public String generateOntologyDefinition() throws Throwable{
		throw new Error(this.shortId+":: "+this.getClass().getName()+" does not implement 'generateOntologyDefinition()'");
	}


}
