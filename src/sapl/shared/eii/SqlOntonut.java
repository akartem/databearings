package sapl.shared.eii;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.SemanticStatement;

/** 
 * Ontonut representing a relational database table
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (21.01.2017)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2017, VTT
 * 
 * */

public class SqlOntonut extends Ontonut {
	protected String driver;
	protected String url;
	protected String username;
	protected String password;
	protected String table;

	protected String sqlFrom;

	protected Set<String> allSelectItems = new HashSet<String>();
	protected Map<String, String> sqlSelect = new HashMap<String, String>();
	protected Map<String, String> sqlExpression = new HashMap<String, String>();
	protected Map<String, Vector<String>> sqlWhere = new HashMap<String, Vector<String>>();	
	protected Map<String, String> sqlAggregate = new HashMap<String, String>();
	protected Map<String, String> sqlGroupedBy = new HashMap<String, String>();

	protected Set<String> objects = new HashSet<String>(); 
	protected Set<String> negations = new HashSet<String>(); 

	protected Set<String> dataItemsDefinedAtIteration = new HashSet<String>();

	protected Set<String> myDataItems = new HashSet<String>();
	
	protected Set<String> primaryKey = new HashSet<String>();

	protected static Map<String, Connection> openConnections = new HashMap<String, Connection>();
	protected static Map<String,Thread> closerThreads = new HashMap<String, Thread>();

	@Override
	public boolean loadDefinition() {
		super.loadDefinition();
		
		String query = "@prefix s: <"+SaplConstants.SAPL_NS+">. ";
		query += "@prefix o: <"+SaplConstants.ONTONUT_NS+">. ";
		query += "<"+this.id+">" + " o:driver ?driver; o:service [ o:uri ?uri ]; o:username ?username; o:password ?password; o:table ?table"; 
		SaplQueryResultSet resultSet = this.parent.hasBeliefsN3(query);
		if(resultSet==null || resultSet.getNumberOfSolutions()==0){
			this.parent.printError(id+": Ontonut is not well defined"); return false;
		}		

		Map<String, String> def = resultSet.getSolution(0);

		this.driver = def.get("driver");
		this.url = def.get("uri");
		this.username = def.get("username");
		this.password = def.get("password");
		this.table = def.get("table");

		this.sqlFrom = this.table;

		return true;
	}

	@Override
	/**Result will map variables used in the semantics definition to column names*/
	public void findDataItems(String contextID, Map<String, String> result) {
		List<SemanticStatement> statements = this.parent.getStatements(contextID);
		Map<String, String> ids = new HashMap<String, String>();

		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"column")){
				ids.put(st.subject, st.object);
			}
			else if(st.predicate.equals(SaplConstants.DATA_NS+"row") && st.isSubjectVar()){
				result.put(st.subject,"ROWID");
			}
			if(st.isObjectContext()){
				this.findDataItems(st.object, result);
			}
		}
		for (SemanticStatement st: statements){
			if(st.predicate.equals(SaplConstants.DATA_NS+"value") && st.isObjectVar()){
				result.put(st.object,ids.get(st.subject));
				myDataItems.add(ids.get(st.subject));
			}
			else if(st.predicate.equals(SaplConstants.DATA_NS+"key") && st.object.equals("true")){
				primaryKey.add(ids.get(st.subject).toUpperCase());
			}
		}

	}

	@Override
	public void addSelectionCondition(String dataItem, String value, boolean isValueString, String variable) {

		super.addSelectionCondition(dataItem, value, isValueString, variable);

		int ind = dataItem.indexOf(".");
		String object = (ind==-1)?"":dataItem.substring(0,ind);
		String localname = dataItem.substring(ind+1);

		if(sqlWhere.get(object)==null){
			Vector<String> where = new Vector<String>();
			where.addElement("");
			sqlWhere.put(object, where);
		}

		if(myDataItems.contains(localname)){
			if(value.startsWith(SaplConstants.VARIABLE_PREFIX)){
				String select = sqlSelect.get(object);
				if(select==null) select = "";
				select = select + ((select.isEmpty())?"":", ") + localname + (object.isEmpty()?"":" as "+object+"_"+localname);
				sqlSelect.put(object, select);
				if(object.isEmpty()) allSelectItems.add(localname);
				else allSelectItems.add(object+"_"+localname);
			}
			else{
				if(!object.isEmpty()) value = value.replace(object+".", "");
				value = value.replace("'", "''");				
				Vector<String> whereVector = sqlWhere.get(object);
				String where = whereVector.lastElement();
				where = where + ((where.isEmpty())?"":" and ") + localname+ "=" + (isValueString?"'":"") + value + (isValueString?"'":"");
				whereVector.set(whereVector.size()-1, where);
				if(sqlSelect.get(object)==null) sqlSelect.put(object, "*");
			}
		}
	}

	@Override
	public boolean addFilterCondition(String dataItem, String operation, String value, boolean isValueString, String variable) {

		super.addFilterCondition(dataItem, operation, value, isValueString, variable);
		
		return this.addWhereCondition(dataItem, operation, value, isValueString, variable);
	}

	protected boolean addWhereCondition(String dataItem, String operation, String value, boolean isValueString, String variable) {
		int ind = dataItem.indexOf(".");
		String object = (ind==-1)?"":dataItem.substring(0,ind);
		String localname = dataItem.substring(ind+1);

		if(sqlSelect.size()>1){
			for(String otherObject :  sqlSelect.keySet()){
				if(otherObject.equals(object)) continue;
				if(value.contains(otherObject+".")){
					//Will put conditions at the top SQL level
					object = "";
				}
			}
		}

		String var = dataItem;
		
		if(myDataItems.contains(localname)){
			
			if(dataItemsDefinedAtIteration.contains(localname)){
				registerFilterIteration();
			}
			
			String extra = this.makeCondition(var, operation, value, isValueString);
			this.addToObjectWhere(object, extra);

			return true;
		}
		else return false;
	}
	
	protected void addToObjectWhere(String object, String extra){
		Vector<String> whereVector = sqlWhere.get(object);
		if(whereVector==null){
			whereVector = new Vector<String>();
			sqlWhere.put(object, whereVector);
			whereVector.add("");
		} 
		String where = whereVector.lastElement();
		
		where = where + ((where.isEmpty())?"":" and ") + extra;

		whereVector.set(whereVector.size()-1, where);
		
		if(sqlSelect.get(object)==null) sqlSelect.put(object, "*");		
	}
	
	protected String makeCondition(String var, String operation, String value, boolean isValueString){
		String where;
		if(value.equals("NULL")){
			if(operation.equals("="))
				where = var +" IS NULL";
			else where = var +" IS NOT NULL";
		}
		else{
			value = value.replace("'", "''");				
			where = var + operation + (isValueString?"'":"") + value+ (isValueString?"'":"");
		}
		
		return where;
	}

	@Override
	public boolean addOrCondition(List<String> dataItems, List<String> operations, List<String> values,
			List<Boolean> isValuesString, List<String> variables) {
		
		super.addOrCondition(dataItems, operations, values, isValuesString, variables);
		
		boolean canBeIn = true;
		String inDataItem = null;
		
		String object = null;
		
		StringBuilder inValues = new StringBuilder();
		StringBuilder conditions = new StringBuilder();
		
		for(int i=0; i<dataItems.size(); i++){
			String dataItem = dataItems.get(i);
			
			if(object == null || !object.equals("")){ 
				int ind = dataItem.indexOf(".");
				String obj = (ind==-1)?"":dataItem.substring(0,ind);
				if(object==null) object = obj;
				else if(!object.equals(obj)) object = "";
			}
			
			String operation = operations.get(i);
			String value = values.get(i);
			boolean isString = isValuesString.get(i);
			if(canBeIn){
				if(!operation.equals("=")) canBeIn = false;
				else{ 
					if(inDataItem==null) inDataItem = dataItem;
					else if(!dataItem.equals(inDataItem)) 
						canBeIn = false;
					
					if(canBeIn){
						if(inValues.length()>0) inValues.append(",");
						value = value.replace("'", "''");				
						inValues.append((isString?"'":"") + value + (isString?"'":""));
					}
				}
			}
			
			if(conditions.length()>0) conditions.append(" or ");
			conditions.append(this.makeCondition(dataItem, operation, value, isString));
		}
		
		if(canBeIn){
			this.addWhereCondition(inDataItem, " in ", "("+inValues.toString()+")", false, null);
		}
		else{
			this.addToObjectWhere(object, "("+conditions+")");
			return true;
		}
		
		return true;
	}	
	
	@Override
	public boolean addExpressionCondition(String expression, String newVarName, String saplVariable) {

		super.addExpressionCondition(expression, newVarName, saplVariable);

		int ind = newVarName.indexOf(".");
		String object = (ind==-1)?"":newVarName.substring(0,ind);
		String localname = newVarName.substring(ind+1);

		String select = sqlExpression.get(object);
		if(select==null) select = "";
		select = select + ((select.length()==0)?"":", ") + expression+" as "+(object.isEmpty()?"":object+"_")+localname;
		sqlExpression.put(object, select); 

		myDataItems.add(localname);
		dataItemsDefinedAtIteration.add(localname);
		
		if(object.isEmpty()) allSelectItems.add(localname);
		else allSelectItems.add(object+"_"+localname);
		
		return true;
	}

	@Override
	public boolean addCountCondition(String vars, String newVarName, String saplVariable){

		super.addCountCondition(vars, newVarName, saplVariable);

		int ind = newVarName.indexOf(".");
		String object = (ind==-1)?"":newVarName.substring(0,ind);
		String localname = newVarName.substring(ind+1);

		if(vars.equals(SaplConstants.ANYTHING)){
			sqlAggregate.put(object, "COUNT(1) AS "+newVarName);
		}
		else{
			if(object.isEmpty() && negations.isEmpty()) vars = vars.replace(".", "_");
			sqlAggregate.put(object, "COUNT(DISTINCT "+vars+") AS "+newVarName);
		}
		
		myDataItems.add(localname);
		dataItemsDefinedAtIteration.add(localname);
		allSelectItems.add(newVarName);
		
		return true;
	}

//	@Override
//	public boolean addCountGroupedByCondition(String vars, String newVarName, String saplVariable) {
//
//		super.addCountGroupedByCondition(vars, newVarName, saplVariable);
//
//		iterationJustRegistered = false;
//
//		int ind = newVarName.indexOf(".");
//		String object = (ind==-1)?"":newVarName.substring(0,ind);
//		String localname = newVarName.substring(ind+1);
//
//		if(object.isEmpty()) vars = vars.replace(".", "_");
//
//		String select = sqlAggregate.get(object);
//		if(select==null) select = "";
//		select = vars + ", COUNT(1) AS "+newVarName;
//		sqlAggregate.put(object, select); 
//
//		sqlGroupedBy.put(object, "GROUP BY "+vars);
//
//		myDataItems.add(localname);
//
//		return true;
//	}

	@Override
	public boolean addMaxCondition(String var, String newVarName, String saplVariable){

		super.addMaxCondition(var, newVarName, saplVariable);

		int ind = newVarName.indexOf(".");
		String object = (ind==-1)?"":newVarName.substring(0,ind);
		String localname = newVarName.substring(ind+1);

		if(object.isEmpty()) var = var.replace(".", "_");

		sqlAggregate.put(object, "MAX("+var+") AS "+newVarName); 
		myDataItems.add(localname);
		dataItemsDefinedAtIteration.add(localname);
		allSelectItems.add(newVarName);

		return true;
	}

	@Override
	public boolean addMinCondition(String var, String newVarName, String saplVariable){

		super.addMinCondition(var, newVarName, saplVariable);

		int ind = newVarName.indexOf(".");
		String object = (ind==-1)?"":newVarName.substring(0,ind);
		String localname = newVarName.substring(ind+1);

		if(object.isEmpty()) var = var.replace(".", "_");

		sqlAggregate.put(object, "MIN("+var+") AS "+newVarName); 
		myDataItems.add(localname);
		dataItemsDefinedAtIteration.add(localname);
		allSelectItems.add(newVarName);

		return true;
	}

	@Override
	public boolean addSumCondition(String var, String newVarName, String saplVariable){

		super.addSumCondition(var, newVarName, saplVariable);

		int ind = newVarName.indexOf(".");
		String object = (ind==-1)?"":newVarName.substring(0,ind);
		String localname = newVarName.substring(ind+1);

		if(object.isEmpty()) var = var.replace(".", "_");

		sqlAggregate.put(object, "SUM("+var+") AS "+newVarName); 
		myDataItems.add(localname);
		dataItemsDefinedAtIteration.add(localname);
		allSelectItems.add(newVarName);

		return true;
	}	

	@Override
	public boolean addAvgCondition(String var, String newVarName, String saplVariable){

		super.addAvgCondition(var, newVarName, saplVariable);

		int ind = newVarName.indexOf(".");
		String object = (ind==-1)?"":newVarName.substring(0,ind);
		String localname = newVarName.substring(ind+1);

		if(object.isEmpty()) var = var.replace(".", "_");

		sqlAggregate.put(object, "AVG("+var+") AS "+newVarName); 
		myDataItems.add(localname);
		dataItemsDefinedAtIteration.add(localname);
		allSelectItems.add(newVarName);

		return true;
	}	

	public void registerFilterIteration(){
		if(DEBUG_PRINT)
			print("iteration");
		dataItemsDefinedAtIteration.clear();
		
		for(String object : sqlWhere.keySet()){
			if(!sqlWhere.get(object).lastElement().isEmpty()) 
				sqlWhere.get(object).add("");	
		}
	}

	@Override
	public void addObjectID(String object) {
		super.addObjectID(object);
		objects.add(object);
	}
	
	@Override
	public boolean addNegativeObjectID(String object) {
		super.addNegativeObjectID(object);
		negations.add(object);
		return true;
	}	

	@Override
	public void processExpression (List<String> tokens){
		for(int i=0;i<tokens.size();i++){
			String token = tokens.get(i);

			// length, trim, replace have direct equivalents , so not handling those

			if(token.equals("toUpperCase")) tokens.set(i,"upper");
			else if(token.equals("toLowerCase")) tokens.set(i,"lower");
			else if(token.equals("indexOf")){
				tokens.set(i,"-1+instr");
				List<Integer> params = getParamIndices(tokens,i,null);
				if(params.size()>2){
					int ind3 = params.get(2);
					String start = tokens.get(ind3);
					tokens.set(ind3,"1+"+start);
				}
			}
			else if(token.equals("substring")){
				tokens.set(i,"substr");
				List<Integer> params = getParamIndices(tokens,i,null);
				if(params.size()>1){
					int ind1 = params.get(1);
					if(params.size()>2){
						int ind2 = params.get(2);
						String endInd = tokens.get(ind2);
						String startInd="";
						for (int j=ind1; j<ind2-1;j++) startInd=startInd+tokens.get(j);
						tokens.set(ind2,endInd+"-"+startInd);
					}
					tokens.set(ind1,"1+"+tokens.get(ind1));
				}
			}
			else if(token.equals("timeMillis")){
				List<Integer> params = getParamIndices(tokens,i,null);
				tokens.set(i,"1000*EXTRACT");
				int ind1 = params.get(0);
				String value = tokens.get(ind1);
				tokens.set(ind1,"EPOCH FROM "+value);

				if(params.size()>=2){
				}

				//				tokens.set(i,"to_date");
				//				if(params.size()>=2){
				//					int ind = params.get(1);
				//					String format=tokens.get(ind);
				//					tokens.set(ind,format.replace("HH","HH24").replace("mm","MI").replace("F","D"));
				//				}
			}
			else if(token.equals("timeString")){
				tokens.set(i,"to_char");
				List<Integer> params = getParamIndices(tokens,i,null);
				if(params.size()>=2){
					int ind = params.get(1);
					String format=tokens.get(ind);
					tokens.set(ind,format.replace("HH","HH24").replace("mm","MI").replace("F","D"));
				}
			}
			//			else if(token.equals("changeTimeZone")){
			//				ArrayList<Integer> params = getParamIndices(tokens,i,null);
			//				String date = tokens.get(params.get(0));
			//				String zone1 = tokens.get(params.get(1));
			//				String zone2 = tokens.get(params.get(2));
			//				tokens.set(i,"from_tz(cast("+date+" AS TIMESTAMP),"+zone1+") AT TIME ZONE "+zone2);
			//				for(int ind=i+1;ind<=params.get(2)+1;ind++) tokens.set(ind,"");
			//			}
		}
	}

	/**Used inside processExpression*/
	List<Integer> getParamIndices(List<String> tokens, int start, List<Integer> bracketIndices){
		if(!tokens.get(start+1).equals("(")) return null;
		else{
			if(bracketIndices!=null) bracketIndices.add(start+1);
			List<Integer> result = new ArrayList<Integer>();
			int bracketCount=1; boolean insideParam=false;
			int i = 2;
			while(start+i<tokens.size() && bracketCount>0){
				String t = tokens.get(start+i);
				if(t.equals(")")){
					bracketCount--;
					if(bracketCount==0 &&bracketIndices!=null) bracketIndices.add(start+i);
				}
				else if(t.equals(",") && bracketCount==1) insideParam=false;
				else{
					if(!insideParam && bracketCount==1) {result.add(start+i); insideParam=true;}
					if(t.equals("(")) bracketCount++;
				}
				i++;
			}
			return result;
		}
	}	

	private Connection openConnection(){
		try {
			Connection conn = openConnections.get(this.url);
			if(conn==null || conn.isClosed()){
				Class.forName(this.driver);

				if ((this.username != null) && (this.password != null)) {
					conn = DriverManager.getConnection(this.url, this.username, this.password);
				} else {
					conn = DriverManager.getConnection(this.url);
				}
				openConnections.put(this.url, conn);
			}
			else{
				closerThreads.get(this.url).interrupt();
			}
			
			final Connection connectionToClose = conn;
			Thread th = new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(5000);
						connectionToClose.close();
					} catch (InterruptedException e) {
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
			});
			closerThreads.put(this.url, th);
			th.start();	
			
			return conn;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	
	@Override
	public List<Map<String,String>> runQuery(SaplQueryResultSet localSolutions, List<Map<String,String>> ontonutErrorSolutions) throws Exception{
		
		long s = System.currentTimeMillis();

		//adjust 'dataItemsToSelect' to reflect used here way of giving aliases to query elements
		Map<String, String> fixedDataItemsToSelect = new HashMap<String, String>();
		for(String item: dataItemsToSelect.keySet()){
			String var = dataItemsToSelect.get(item);
			int ind = item.indexOf(".");
			String object = (ind==-1)?"":item.substring(0,ind);
			fixedDataItemsToSelect.put(item.replace(object+".", object+"_"), var);
		}
		dataItemsToSelect = fixedDataItemsToSelect;
		
		for(Vector<String> where: sqlWhere.values()){
			while(where.size()>1 && where.lastElement().isEmpty()){ 
				where.remove(where.size()-1);
			}
		}
		
		String sql = this.getSQL();
		if(sql==null){
			print("SQL query is empty!");
			return null;
		}

		//likely wrong:
		//		if(ontonutNum==1){
		//			if(!resultSet.orderBy.isEmpty()) SQL[i]+=" ORDER BY "+queryReferencedDataItems.get(i).get(SaplConstants.VARIABLE_PREFIX+resultSet.orderBy.get(0));
		//			if(resultSet.limit!=-1) SQL[i]+=" LIMIT "+resultSet.limit;
		//			if(resultSet.offset!=-1) SQL[i]+=" OFFSET "+resultSet.offset;
		//		}

		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			print("Executing "+sql+" on "+this.url);

		ResultSet rset = null;
		Statement stmt = null;
		
		try {
			Connection conn = openConnection();
			if(conn==null) return null;
			
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Connected [" + (System.currentTimeMillis() - s) + "]");

			stmt = conn.createStatement();
			s = System.currentTimeMillis();
			rset = stmt.executeQuery(sql);
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Query executed [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();

			ResultSetMetaData meta = rset.getMetaData(); 
			int RC = meta.getColumnCount();
			String labels[]=new String[RC];

			for(short iCol=0; iCol < RC;iCol++){
				labels[iCol] = meta.getColumnName(iCol+1).toUpperCase();
			}
			
			List<Map<String,String>> sols = new ArrayList<Map<String,String>>();
			int iRow=0;
			while(rset.next()){
				iRow++;
				Map<String,String> solution = new HashMap<String,String>();				
				for(int iCol=0;iCol<RC;iCol++){
					String value = rset.getString (iCol+1);
					if(value==null || value.equals("null") || value.equals("")) value = "NULL";

					String var = this.dataItemsToSelect.get(labels[iCol]);
					if(var!=null){
						solution.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
					}
				}
				if(!solution.isEmpty()) sols.add(solution);
			}

			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Processed "+iRow+" result rows ["+(System.currentTimeMillis()-s)+"]");

			return sols;

		} finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e) {
					// not much we can do here.
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// not much we can do here.
				}
			}
		}	

	}
	
	protected String getAllSelectItems(){
		String result = null;
		for(String item : allSelectItems){
			if(result==null) result = item;
			else result += ", "+item;
		}
		return result;
	}

	protected String getSQLForObject(String object, String objectName, String from, boolean insideJoin, boolean insideExists){
		String select = sqlSelect.get(object);
		if(select==null)
			return getSelectLine("*", from, objectName, "", insideJoin);
		
		String express = sqlExpression.get(object);
		if(express!=null){
			if(!insideExists){
				for(String o : sqlSelect.keySet()){
					if(!o.equals("") && !o.equals(object))
						express = express.replace(o+".", o+"_");
				}
			}
			select+= (select.isEmpty()?"":", ")+express;
		}
		String aggr = sqlAggregate.get(object);
		String group = sqlGroupedBy.get(object);
		Vector<String> whereVector = sqlWhere.get(object);

		String result = "";
		for(int iteration=0; iteration<whereVector.size(); iteration++){
			String where = whereVector.get(iteration);
			if(!insideExists){
				for(String o : sqlSelect.keySet()){
					if(!o.equals("") && !o.equals(object))
						where = where.replace(o+".", o+"_");
				}
			}
			
			String s; 
			if(iteration==0){
				if(whereVector.size() > 1) s = select;
				else s = ((aggr==null)?select : aggr);
				
				result = getSelectLine(s, from, objectName, where, insideJoin&&(iteration==whereVector.size()-1));
			}
			else{
				if(iteration==whereVector.size()-1) s = ((aggr==null)?/*getAllSelectItems()*/ "*": aggr);
				else s = "*";
				//TODO Handle better combinations of aggregates with following filters
				if(iteration<whereVector.size()-1 || aggr==null || where.isEmpty()){
					result = getSelectLine(s, "( "+result+" )", "iter"+iteration, where, insideJoin&&(iteration==whereVector.size()-1));
				} 
				else {
//					if(true){
//						result = getSelectLine(s, "( "+result+" )", "iter"+iteration, "", false);
//						result = getSelectLine("*", "( "+result+" )", "iter"+(iteration+1), where, isInner&&(iteration==whereVector.size()-1));
//					}
//					else {
						result = getSelectLine(s, "( "+result+" )", "iter"+iteration, "", false);
						result = getSelectLine("*", this.sqlFrom+" INNER JOIN ( "+result+" )", "foo", "", false);
						result += " ON "+where;
//					}
				}
			}
			if(iteration == whereVector.size() - 1) result += ((group==null)?"":" "+group);
		}
		return result;
	}
	
	protected String getSelectLine(String select, String from, String as, String where, boolean insideJoin){
		if(!insideJoin)
			return "SELECT "+ select + " FROM "+from+(as==null?"":" as "+as)+((where.isEmpty())?"":(" WHERE "+where));
		else{
			if(as!=null) where = where.replace(as+".", "");
			return "( SELECT "+ select + " FROM "+from+((where.isEmpty())?"":(" WHERE "+where))+" )"+(as==null?"":" as "+as); 
		}
	}
	
	protected String collectWhereParts(Vector<String> vec){
		String result = "";
		if(vec!=null){
			for(String part : vec){
				if(part!=null && !part.isEmpty())
					result += (result.isEmpty()?"":" and ")+part;
			}
		}
		return result;
	}
	
	protected String getSQL(){
		
		if(Ontonut.DEBUG_PRINT){
			print("SELECT: "+sqlSelect);
			print("expressions: "+sqlExpression);
			print("WHERE "+sqlWhere);	
			print("aggregates: "+sqlAggregate);
			print("GROUP BY "+sqlGroupedBy);
		}

		if(sqlSelect.size()==0){
			return null;
		}
		else if(sqlSelect.size()==1){
			String object = null;
			if(objects.size()>0) object = objects.iterator().next();
			return getSQLForObject("", object, this.sqlFrom, false, false);
		}
		else if(negations.isEmpty()) {
			String inner = null;
			String innerWhere = "";
			String mainObject = null;
			for(String object : sqlSelect.keySet()){
				if(object.equals("")) continue;
				if(inner==null){
					mainObject = object;
					sqlSelect.put(object, "*, "+sqlSelect.get(object));
					String objSQL = getSQLForObject(object, object, this.sqlFrom, false, false);
					int ind = objSQL.lastIndexOf("WHERE");
					if(ind!=-1){
						inner = objSQL.substring(0, ind-1);
						innerWhere = objSQL.substring(ind-1);
					}
					else inner=objSQL;
				}
				else { 
					String objSQL = getSQLForObject(object, object, this.sqlFrom, true, false);
					inner = inner + " INNER JOIN "+objSQL;
				}
			}
			
			String on = "";
			Vector<String> mainWhere = sqlWhere.get("");
			if(mainWhere!=null){
				on = mainWhere.get(0);
				mainWhere.set(0, "");
			}
			
			if(on.isEmpty()) on = "true";
			else{
				for(String o : sqlSelect.keySet()){
					if(!o.equals("") && !o.equals(mainObject))
						on = on.replace(o+".", o+"_");
				}				
			}
			inner += " ON "+on; 
			inner += innerWhere;
			
			if(sqlSelect.get("")!=null)
				return getSQLForObject("", null, "( "+inner+" ) as foo", false, false);
			else return inner;
		}
		else{
			StringBuilder result = new StringBuilder(); 
			String main = null;
			for(String object : sqlSelect.keySet()){
				if(object.equals("") || negations.contains(object)) continue;
				main = object;
				String aggr = sqlAggregate.get("");
				if(aggr!=null){
					sqlAggregate.put(object, aggr);
					sqlAggregate.remove("");
				}
				break;
			}
			
			String objSQL = getSQLForObject(main, main, this.sqlFrom, false, false);
			boolean hasWhere = objSQL.contains("WHERE");
			result.append(objSQL);
			
			for(String object : sqlSelect.keySet()){
				if(object.equals("") || object.equals(main)) continue;
				
				String expr = sqlExpression.get("");
				if(expr!=null){
					sqlExpression.put(object, expr);
					sqlExpression.remove("");
				}				
				Vector<String> where = sqlWhere.get("");
				if(where!=null){
					sqlWhere.put(object, where);
					sqlWhere.remove("");
				}				

				if(hasWhere) result.append(" AND");
				if(negations.contains(object))
					result.append(" NOT EXISTS ( ");
				else result.append(" EXISTS ( ");
				String otherSQL = getSQLForObject(object, object, this.sqlFrom, false, true);
				result.append(otherSQL);
				result.append(" )");
			}
			
			
//			String aggr = sqlAggregate.get("");
//			//String group = sqlGroupedBy.get(main);
//			String select = sqlSelect.get(main);
//			String main_express = sqlExpression.get(main);
//			result.append("\nSELECT "+ ((aggr!=null)?aggr : select) + " FROM "+sqlFrom+" as "+main+"\n");
//			
//			StringBuilder where = new StringBuilder(); 
//			where.append(collectWhereParts(sqlWhere.get(main)));
//
//			String topWhere = collectWhereParts(sqlWhere.get(""));
//			if(topWhere!=null) topWhere = topWhere.replace(main+"_",main+".").replace(main+".V_", main+"_V_");
//
//			int count = 0;
//			for(String object : sqlSelect.keySet()){
//				if(object.equals("") || object.equals(main)) continue;
//				count++;
//				if(where.length()>0) where.append("\nAND ");
//				if(negations.contains(object))
//					where.append("NOT EXISTS (\n");
//				else where.append("EXISTS (\n");
//				where.append("  SELECT * FROM ("+"\n");
//
//				String part_select = sqlSelect.get(object);
//				String part_express = sqlExpression.get(object);
//				if(part_express!=null) part_select += (part_select.isEmpty()?"":", ")+part_express;
//				if(count==1 && main_express!=null) part_select += (part_select.isEmpty()?"":", ")+main_express;
//				String part_where = sqlWhere.get(object).lastElement();
//				where.append("    SELECT "+part_select+" FROM "+sqlFrom+" as "+object+((part_where==null || part_where.isEmpty())?"":(" WHERE "+part_where))+"\n");
//				where.append("  ) as "+object+"\n");
//				if(count==1 && topWhere!=null && !topWhere.isEmpty()) where.append("  WHERE " + topWhere+"\n");
//
//				where.append(")");
//			}			
//
//			if(where.length()>0) result.append("WHERE "+where.toString());

			return result.toString();
		}
	}
	
	@Override
	protected String runUpdate(List<Map<String, String>> updates) throws Exception{
		
		long s = System.currentTimeMillis();
		
		Statement stmt = null;
		
		try{
			Connection conn = openConnection();
			if(conn==null) return null;
			
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Connected [" + (System.currentTimeMillis() - s) + "]");

			s = System.currentTimeMillis();
			
			String SQL = this.getUpdateSQL(updates);
			String shortSql;
			if(SQL.length()>200) shortSql = SQL.substring(0,200)+"...";
			else shortSql=SQL;
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Executing "+shortSql+" [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();
			
			stmt = conn.createStatement();
			
			stmt.executeUpdate(SQL);
			
			if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
				print("Update executed [" + (System.currentTimeMillis() - s) + "]");
			
			return null;
			
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// not much we can do here.
				}
			}
		}	
	}

	protected String getUpdateSQL(List<Map<String, String>> updates) throws Exception{

		StringBuilder sql = new StringBuilder();
		
		HashSet<String> keyValues = new HashSet<String>();
		
		for(Map<String, String> update : updates){
			StringBuilder columns = new StringBuilder();
			StringBuilder values = new StringBuilder();
			StringBuilder settings = new StringBuilder();
			StringBuilder where = new StringBuilder();
			
			for(String var : this.dataItems.keySet()){
				String key = this.dataItems.get(var);
				String value;
				if(var.startsWith(SaplConstants.VARIABLE_PREFIX)){
					String varname = var.substring(SaplConstants.VARIABLE_PREFIX.length());
					value = update.get(varname);
				} 
				else value = var; 
				
				if(value!=null) value = value.replace("'", "''");
				
 				if(!primaryKey.isEmpty()){
					if(primaryKey.contains(key)){
	 					if(where.length()>0) where.append(" and ");
	 					where.append(key.toLowerCase()+"='"+value+"'");
					}
	 				else{
	 					if(settings.length()>0) settings.append(",");
	 					if(value==null) settings.append(key.toLowerCase()+"=null");
	 					else settings.append(key.toLowerCase()+"='"+value+"'");
	 				}
 				}
 				
				if(value!=null){
	 				if(columns.length()>0){
						columns.append(",");
						values.append(",");
					}
					columns.append(key);
//					if(value==null) values.append("null");
//					else 
					values.append("'"+value+"'");
				}
			}

			if(sql.length()>0) sql.append("; ");
			
			if(where.length()>0){
				String whereStr = where.toString();
				if(keyValues.contains(whereStr)){
					//a primary-key duplicate within the data itself, have to skip, cannot do much
					this.parent.printError("Primary key duplicates within data: "+whereStr);
					continue;
				}
				keyValues.add(whereStr);
				
				String checkSql = "SELECT 1 FROM "+this.sqlFrom+" WHERE "+whereStr;
				
				if (ifExists(checkSql)){
					sql.append("UPDATE "+this.sqlFrom+" SET "+settings.toString()
							+" WHERE "+whereStr);
					continue;
				}
				
			}

			sql.append("INSERT INTO "+this.sqlFrom+" ("+columns.toString().toLowerCase()
					+") VALUES ("+values.toString()+")");
			
		}

		return sql.toString();
	}
	
	protected boolean ifExists(String sql) throws Exception{
		Connection conn = openConnections.get(this.url);
		Statement stmt = conn.createStatement();
		ResultSet rset = stmt.executeQuery(sql);
		boolean result = rset.next();
		rset.close();
		stmt.close();
		return result;
	}
	

	//Support of IDE 
	
	@Override
	public String generateSyntaxDefinition(Set<String> includeEntities, Map<String,String> fillVariables) throws Exception{

		String ns = this.url;
		if(ns.contains("?")) ns = ns.substring(0,ns.indexOf("?"));
		ns += "#";
		
		Connection conn = openConnection();
		Statement stmt = null;
		ResultSet rset = null;
		if(conn!=null){
			try{
				stmt = conn.createStatement();
				String sql = "SELECT * FROM "+this.table+" LIMIT 1";
				rset = stmt.executeQuery(sql);
				ResultSetMetaData meta = rset.getMetaData(); 
				int RC = meta.getColumnCount();
		
				String result = "";
				result += "* d:table {\n";
				result += "\t* d:row {\n";
				
				for(short iCol=0; iCol < RC;iCol++){
					String title = meta.getColumnName(iCol+1);
					String varName = title.toLowerCase();
					result += "\t\t[d:column \""+title+"\"] d:value "+SaplConstants.VARIABLE_PREFIX+varName+" .\n";
					
					if(fillVariables!=null) fillVariables.put(ns+title, varName);			
				}
				
				result += "\t} .\n";
				result += "} .";
				return result;
			} finally {
				if (rset != null) {
					try {
						rset.close();
					} catch (SQLException e) {}
				}
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {}
				}
			}				
		}
		return null;
	}
	

}
