package sapl.shared.eii;

import java.util.Map;

/** 
 * Ontonut that does nothing
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.2 (05.12.2014)
 *
 * @since 4.0
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */ 

public class AbstractOntonut extends Ontonut {

	@Override
	public boolean loadDefinition() {
		return super.loadDefinition();
	}

	@Override
	public void findDataItems(String contextID, Map<String, String> map) {
	}

}
