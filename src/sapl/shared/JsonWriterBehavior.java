package sapl.shared;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Produces a JSON document from a given fact of the predefined structure, 
 * which uses d:tree, d:branch, d:attribute,  d:value properties, and d:<nnnn> IDs
 * (the same as in JsonReaderBehavior), and writes it either into a file
 * or as a part of a new fact created in the facts base.
 * 
 * Parameters:
 * p:input	ID of Context that includes a fact to transform into XML document. Mandatory
 * p:result	Either the name of the file to save the content to, or a Context providing
 * the structure to be added (has to use the ?result variable that will be
 * substituted with the content). Mandatory
 * p:indents	If true, line breaks and indentation is used to separate XML elements.
 * Otherwise, a single line is produced. Default false
 * p:sort	If true, the order of sub-elements of a JSON element is enforced
 * based on their d:<nnnn> IDs. Default false
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (14.05.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 *
 * */

public class JsonWriterBehavior extends ReusableAtomicBehavior{

	@Parameter (name="input") private String input;
	@Parameter (name="result") private String resultTo;
	@Parameter (name="indents") private boolean indents;
	@Parameter (name="sort") private boolean sort;
	@Parameter (name="clean") private boolean clean;

	private final static int indent_size = 4;	
	private final static String white="                                                                                                                        ";

	public static boolean PERFORMANCE_PRINT = false;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.indents = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "indents"), false);
		this.sort = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "sort"), false);
		this.clean = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "clean"), false);
	}

	private StringBuilder recursion(String contextID, int recursionLevel){

		StringBuilder result = new StringBuilder();
		List<SemanticStatement> vec = getStatements(contextID);

		if(vec!=null){

			HashMap<String, String> names = new HashMap<String, String>(); 
			HashMap<String, String> values = new HashMap<String, String>(); 

			HashSet<String> uniqueNames = new HashSet<String>(); 
			HashSet<String> arrayNames = new HashSet<String>(); 

			Vector<ElementInfo> elements=new Vector<ElementInfo>();
			Iterator<SemanticStatement> it=vec.iterator();
			while(it.hasNext()){
				SemanticStatement st=it.next();

				if(st.predicate.equals(SaplConstants.DATA_NS+"attribute")){
					String name = st.object;

					if(!uniqueNames.add(name))
						arrayNames.add(name);

					String value = values.get(st.subject);
					if(value==null)
						names.put(st.subject, st.object);
					else
						elements.add(new ElementInfo(st.subject, name, value));
				}
				else if(st.predicate.equals(SaplConstants.DATA_NS+"branch")){
					String branch = recursion(st.object, recursionLevel+1).toString();
					String value = makeObject(branch, recursionLevel);
					
					String name = names.get(st.subject);
					if(name==null)
						values.put(st.subject, value);
					else 
						elements.add(new ElementInfo(st.subject, name, value));
				}
				else if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
					String name = names.get(st.subject);
					String value = st.object;
					if(!clean || !value.startsWith(SaplConstants.VARIABLE_PREFIX) || value.split("\\s").length>1){
						if(name==null)
							values.put(st.subject, value);
						else
							elements.add(new ElementInfo(st.subject, name, value));
					}
				}
			}

			if(!elements.isEmpty() && sort) Collections.sort(elements);

			if(!arrayNames.isEmpty()){
				Vector<ElementInfo> new_elements = new Vector<ElementInfo>();

				for(int i=0;i<elements.size();i++){
					ElementInfo elInfo = elements.elementAt(i);
					if(elInfo!=null)
						if(!arrayNames.contains(elInfo.name))
							new_elements.add(elInfo);
						else{
							StringBuilder arrayContent = new StringBuilder();
							for(int j=i; j<elements.size();j++){
								ElementInfo elInfo2 = elements.elementAt(j);
								if(elInfo2!=null && elInfo.name.equals(elInfo2.name)){
									arrayContent.append(((arrayContent.length()>0)?", ":"")+((arrayContent.length()>0 && indents)?"\n":"") + ((indents)?white.substring(0,(recursionLevel)*indent_size):"") + makeValue(elInfo2.value,recursionLevel));
									elements.setElementAt(null, j);
								}
							}
							String content = arrayContent.toString();
							if(indents) content = increaseIndent(content);
							elInfo.value = makeArray(content, recursionLevel);
							new_elements.add(elInfo);
							arrayNames.remove(elInfo.name);
						}
				}

				elements = new_elements;
			}
			
			if(!arrayNames.isEmpty()){ //had empty arrays
				for(String name : arrayNames)
					elements.add(new ElementInfo(name, name, "[]"));
			}

			for(int i=0;i<elements.size();i++)
				result.append(((i>0)?", ":"")+((i>0 && indents)?"\n":"") + makeAttribute(elements.elementAt(i).name, elements.elementAt(i).value, recursionLevel));
		}

		return result;
	}


	private String convertMultilineStringToArray(String str, int recursionLevel){
		String result = ((indents)?white.substring(0,(recursionLevel)*indent_size):"")+"["+"\n";

		String[] lines = str.split("\\n");
		boolean first = true;
		for (String line : lines){
			if(!first){
				result += ","+((indents)?"\n":"");
			}
			else first = false;

			result += ((indents)?white.substring(0,(recursionLevel+1)*indent_size):"")+"\""+RABUtils.escapeString(line.trim())+"\"";
		}

		result += ((indents)?"\n"+white.substring(0,(recursionLevel)*indent_size):"")+"]";

		return result;
	}
	
	private String increaseIndent(String value) {
		return white.substring(0,indent_size)+value.replace("\n", "\n"+white.substring(0,indent_size));
	}
	

	private String makeAttribute (String name, String value, int recursionLevel){
		return ((indents)?white.substring(0,(recursionLevel)*indent_size):"")+
				"\""+name+"\""+" : "+makeValue(value,recursionLevel);
	}

	private String makeValue (String value, int recursionLevel){
		boolean isValueObject = value.trim().startsWith("{") || value.trim().startsWith("[");
		if(!isValueObject) value = RABUtils.escapeString(value);
		if(!isValueObject && value.contains("\n")){
			value = convertMultilineStringToArray(value, recursionLevel);
			isValueObject = true;
		}
		return (isValueObject?"":"\"")+value.trim()+(isValueObject?"":"\"");
	}

	private String makeObject (String value, int recursionLevel){
		return ((indents)?white.substring(0,(recursionLevel)*indent_size):"")+"{"
				+(indents?"\n":"")
				+value
				+((indents && !value.isEmpty())?"\n":"")
				+((indents)?white.substring(0,(recursionLevel)*indent_size):"")+"}";
	}

	private String makeArray (String value, int recursionLevel){
		return ((indents)?white.substring(0,(recursionLevel)*indent_size):"")+"["
				+(indents?"\n":"")
				+value
				+((indents && !value.isEmpty())?"\n":"")
				+((indents)?white.substring(0,(recursionLevel)*indent_size):"")+"]";
	}

	@Override
	protected void doAction() throws Exception {

		long start = System.currentTimeMillis();

		String result = makeObject(recursion(input, 1).toString(), 0);
		long s = System.currentTimeMillis();
		
		if(PERFORMANCE_PRINT)
			this.print ("[JsonWriterBehavior] JSON produced ["+(s-start)+"]");
		
		boolean ok = RABUtils.putResult(this.myEngine, result, resultTo);
		if (!ok) {
			this.setFailed();
		}
	}

	private class ElementInfo implements Comparable<ElementInfo>{
		String id;
		String name;
		String value;

		public ElementInfo(String id, String name, String value){
			this.id=id; this.name = name; this.value=value;
		}

		public int compareTo(ElementInfo o){
			return id.compareTo(o.id);
		}
	}	
}
