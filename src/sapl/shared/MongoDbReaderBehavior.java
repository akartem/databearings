package sapl.shared; 

import java.util.Iterator;

import org.bson.BsonDocument;
import org.bson.Document;
import org.json.JSONObject;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.util.URITools;

/**
 * Submits a BSON query to a MongoDB database and generates from the results
 * a fact of the predefined structure that uses 
 * d:tree, d:branch, d:attribute,  d:value properties, and d:<nnnn> IDs
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (01.10.2015)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2015, VTT
 *
 * */

public class MongoDbReaderBehavior extends JsonReaderBehavior {

	@Parameter (name="database") private String url;
	@Parameter (name="collection") private String collection;
	@Parameter (name="query") private String query;
	@Parameter (name="uri") private String id;
	
	private String server;
	private String database;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException{
		Resource r = Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "database");
		this.url = parameters.getObligatoryStringParameter(r);
		int ind = this.url.lastIndexOf("/");
		if(ind!=-1){
			this.server = this.url.substring(0, ind);
			this.database = this.url.substring(ind+1);
		}
		else{
			throw new IllegalParameterConfigurationException(r, this.url, State.invalid, "Database name is missing");
		}
		
		this.collection = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "collection"));
		this.query = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "query"));

		this.id = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
	}

	@Override
	protected void doAction() throws Exception {
		long s = System.currentTimeMillis();
		
		MongoClient mongoClient = new MongoClient(this.server);
		try{
			MongoDatabase database = mongoClient.getDatabase(this.database);
			MongoCollection<Document> collection = database.getCollection(this.collection);
			
			FindIterable<Document> result = null;
			if(this.query==null) result = collection.find();
			else{
				BsonDocument filter = BsonDocument.parse(this.query);
				result = collection.find(filter);
			}
			
			StringBuilder beliefs = new StringBuilder();
			beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
	
			Iterator<Document> it = result.iterator();
			int i=0;
			while(it.hasNext()){
				Document resultDoc = it.next();
				
				JSONObject json = new JSONObject(resultDoc.toJson());
				
				beliefs.append("<"+this.id+"_"+(++i)+">" + " d:tree { ");
				beliefs.append(processJSONObject(json));
				beliefs.append(" }. ");
			}
			
			this.addBeliefsN3(beliefs.toString());
			this.wakeReasoner();		
	
			System.out.println(System.currentTimeMillis() - s);
		}
		finally{
			mongoClient.close();
		}
	}
}
