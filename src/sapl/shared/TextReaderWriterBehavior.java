package sapl.shared;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Takes the content from either the file of the passed string itself, 
 * and then outputs either into file or as a part of a new fact created
 * in the facts base. Therefore, can be used to (1) read a file, (2) write a file, 
 * (3) copy a file. Also able to read a Web document, so overlaps in 
 * functionality with HttpBehavior in that, but does not support authentication, etc.
 * 
 * Parameters:
 * p:input	Name of the file or a text string. Mandatory
 * p:result	Either the name of the file to save the content to, or a Context providing
 * the structure to be added (has to use the ?result variable that will be substituted
 * with the content). Mandatory
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (28.02.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class TextReaderWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="result") private String saveTo;
	@Parameter (name="append") private boolean append;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.saveTo = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.append = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "append"), false);
	}

	@Override
	protected void doAction() {

		String text;
		try {
			text = HttpTools.getContent(this.input, HttpOptions.defaultOptions(), false);
		} catch (Exception e) {
			this.printException(e, true);
			this.setFailed();
			return;
		}
		
		boolean ok = RABUtils.putResult(this.myEngine, text, this.saveTo, append);
		if (!ok) {
			this.setFailed();
		}
		this.wakeReasoner();
	}
}
