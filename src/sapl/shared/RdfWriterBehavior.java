package sapl.shared;

import java.io.StringReader;
import java.io.StringWriter;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFWriter;
import org.eclipse.rdf4j.rio.ntriples.NTriplesWriter;
import org.eclipse.rdf4j.rio.rdfxml.RDFXMLWriter;
import org.eclipse.rdf4j.rio.rdfxml.util.RDFXMLPrettyWriter;
import org.eclipse.rdf4j.rio.turtle.TurtleWriter;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.URITools;

/**
 * Produces an RDF document from the content of a Context, and writes it
 * either into a file or as a part of a new fact created in the facts base.
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (21.06.2018)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015-2018, VTT
 *
 * */

public class RdfWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="result") private String output;
	@Parameter (name="format") private String format;

	final static String RDFXML = "rdfxml";
	final static String PRETTYRDFXML = "prettyrdfxml";
	final static String TURTLE = "turtle";
	final static String NTRIPLES = "ntriples";

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"), SaplConstants.G);
		this.output = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.format = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "format"), PRETTYRDFXML);
	}

	@Override
	protected void doAction() throws Exception {
		SaplN3ProducerOptions options = new SaplN3ProducerOptions(false, false, true, false, false, false);
		String triples = this.produceN3(this.input, options)+".";
		
		Repository repository = new SailRepository(new MemoryStore());

		repository.initialize();
		RepositoryConnection con = repository.getConnection();
			
		con.add(new StringReader(triples), "#", RDFFormat.N3);
		
		StringWriter result = new StringWriter();
		
		RDFWriter writer; 
		
		if(this.format.equals(TURTLE))
			writer = new TurtleWriter(result);
		else if(this.format.equals(NTRIPLES))
			writer = new NTriplesWriter(result);
		else if(this.format.equals(RDFXML))
			writer = new RDFXMLWriter(result);
		else //Pretty RDFXML
			writer = new RDFXMLPrettyWriter(result);		
		
		con.export(writer);
		
		con.close();
		repository.shutDown();
		
		boolean ok = RABUtils.putResult(this.myEngine, result.toString(), this.output);
		if (!ok) {
			this.setFailed();
		}
	}

}
