package sapl.shared.xmpp;

import java.util.Collection;
import java.util.HashMap;

import fi.vtt.usefil.questionnaire.Answer;
import fi.vtt.usefil.questionnaire.Message;
import fi.vtt.usefil.questionnaire.MultiUserChatSession;
import fi.vtt.usefil.questionnaire.Question;
import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.util.URITools;

/** 
 * Sends a new XMPP message to a multiuser chat room on an XMPP server. 
 * Either creates a new connection (and closes it after sending the message) 
 * or uses an existing connection (typically created by XmppReceiverBehavior). 
 * Either p:session has to be given, or p:host, p:username, p:password, and p:room.
 * By default, sending is done is a separate thread.
 * 
 * Parameters:
 * p:message	Message to send. Mandatory
 * p:session	Blackboard ID of the session object. Optional
 * p:host	IP address of the XMPP server. Optional
 * p:username	Username at the XMPP server. Optional
 * p:password	Password at the XMPP server. Optional
 * p:room	Local ID of a multiuser chat room to join. Optional
 * p:nickname	Nickname to use in the chat room. Default "SAPL"
 * 
 * @author Artem Katasonov (VTT)
 *  
 * @version 4.1 (10.09.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class XmppSenderBehavior extends ReusableAtomicBehavior {

	public static String DEFAULT_NICKNAME = "SAPL";
	public static int WAIT_BEFORE_DISCONNECT = 1000;
	
	@Parameter (name="host") private String host;
	@Parameter (name="username") private String username;
	@Parameter (name="password") private String password;
	@Parameter (name="message") private String message;
	@Parameter (name="room") private String room;
	@Parameter (name="nickname") private String nickname;	

	@Parameter (name="attach") private Collection<String> attach;
	
	@Parameter (name="session") private Resource sessionId;
	private MultiUserChatSession session;
	
	static HashMap<String, MultiUserChatSession> openConnections = new HashMap<String, MultiUserChatSession>();
	static HashMap<String,Thread> closerThreads = new HashMap<String, Thread>();
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		//so override the default value to 'true'
		this.executeInThread = parameters.getOptionalBooleanParameter
				(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.SAPL_NS), "thread"), true);

		this.message = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "message"));

		this.sessionId = parameters.getOptionalResourceParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "session"));
		if(this.sessionId!=null){
			Object object = this.getFromBlackboard(this.sessionId);
			if (object == null || !(object instanceof MultiUserChatSession)) {
				throw new IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "session"), this.sessionId.toString(), State.notOnBlackboard);
			}
			else this.session = (MultiUserChatSession) object;
		}
		else{
			this.host = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "host"));
			this.username = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
			this.password = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));
			this.room = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "room"));
			this.nickname = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "nickname"), DEFAULT_NICKNAME);
		}
		
		this.attach = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "attach"));
	}

	@Override
	protected void doAction() throws Exception {
		if(session==null){
			session = XmppSenderBehavior.getSession(this.host,this.username,this.password,this.room, this.nickname);
		}
		
		print("Sending XMPP message: "+this.message);

		if(this.attach==null || this.attach.isEmpty()){
			Message m = new Message();
			m.setText(this.message);
			session.sendMessage(m);
		}
		else{
			Question q = new Question();
			q.setText(this.message);
			for(String item : attach){
				Answer a = new Answer();
				a.setText(item);
				a.setValue(item);
				q.add(a);
			}
			session.sendMessage(q);
		}

		if(this.sessionId==null){
			final MultiUserChatSession connectionToClose = session;
			Thread th = new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(WAIT_BEFORE_DISCONNECT);
						if(connectionToClose.isConnected()){
							connectionToClose.shutdown();
							openConnections.remove(host+username+room);
						}
					} catch (InterruptedException e) {
					}
				}
			});
			closerThreads.put(this.host+this.username+this.room, th);
			th.start();
		}
		
	}
	
	private synchronized static MultiUserChatSession getSession(String host, String username, String password, String room, String nickname){
		MultiUserChatSession session = openConnections.get(host+username+room);
		if(session!=null && session.isConnected()){
			Thread th = closerThreads.get(host+username+room);
			if(th!=null) th.interrupt();
		}			
		else{
			session = new MultiUserChatSession(username, password, room, host);
			openConnections.put(host+username+room, session);
			session.setNickname(nickname);
			session.startup();
		}
		return session;
	}

}
