package sapl.shared.xmpp;

import org.jivesoftware.smack.PacketListener;

import fi.vtt.usefil.questionnaire.MultiUserChatSession;
import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.IDGenerator;
import sapl.util.URITools;

/** 
 * Connects to a multiuser chat room on an XMPP server and waits 
 * for incoming messages.  For each message received, injects into
 * the facts base a fact. A single instance of MultiUserChatRoom is
 * placed on the Engine's blackboard and its ID (same for all incoming 
 * messages) is published as part of the injected fact. 
 * By default, the login operation is executed in a separate thread.
 * Outgoing messages can be sent with XmppSenderBehavior RAB.
 *  
 * Parameters:
 * p:host	IP address of the XMPP server.	Mandatory
 * p:username	Username at the XMPP server.	Mandatory
 * p:password	Password at the XMPP server.	Mandatory
 * p:room	Local ID of a multiuser chat room to join.	Mandatory
 * p:nickname	Nickname to use in the chat room.	Default	"SAPL"
 * p:waitOnlyFirst	If true, disconnects from the server after 
 * receiving the first message.	Default false
 * p:session	Either the name of the file to save the session blackboard ID, 
 * or a Context providing the structure to be added (has to use the ?result 
 * variable that will be substituted).	Optional	
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.2 (17.12.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class XmppReceiverBehavior extends ReusableAtomicBehavior {

	@Parameter (name="host") private String host;
	@Parameter (name="username") private String username;
	@Parameter (name="password") private String password;
	@Parameter (name="room") private String room;
	@Parameter (name="nickname") private String nickname;
	
	public static String DEFAULT_NICKNAME = "SAPL";

	@Parameter (name="session") private String sessionID;

	@Parameter (name="waitOnlyFirst") private boolean waitOnlyFirst;
	
	private MultiUserChatSession session;
	
	private boolean started = false;
	
	private Resource blackboardID;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		//login operation takes time
		//so override the default value to 'true'
		this.executeInThread = parameters.getOptionalBooleanParameter
				(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.SAPL_NS), "thread"), true);

		this.finished = false; // make this behavior cyclic, so it doesn't disappear after logged in
		
		this.host = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "host"));
		this.username = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
		this.password = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));
		this.room = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "room"));
		this.nickname = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "nickname"), DEFAULT_NICKNAME);

		this.sessionID = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "session"));
		
		this.waitOnlyFirst = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "waitOnlyFirst"), false);
		
	}

	@Override
	protected void doAction() throws Exception {
		this.block();

		if (!this.started) {
			session = new MultiUserChatSession(this.username, this.password, this.room, this.host);
			session.setNickname(this.nickname);
			if(!session.startup()) {
				this.setFailed();
				this.finished = true;
			}
			
			session.addMessageListener(new XmppMessageListener());

			blackboardID = addOnBlackboard(session);
			
			if(this.sessionID!=null && this.sessionID.startsWith(SaplConstants.CONTEXT_PREFIX)){
				RABUtils.putResult(myEngine, blackboardID.toString(), this.sessionID);
				wakeReasoner();
			}
	
			this.started = true;
		}
	}
		
	@Override
	protected void stop(){
		removeFromBlackboard(blackboardID);
		this.setFinished();
		try {
			session.shutdown();
		} catch (Exception e) {
			this.printException(e, true);
		}
	}
	

	class XmppMessageListener implements PacketListener{
		
		private final IDGenerator messageIDGenerator;
		
		public XmppMessageListener(){
			messageIDGenerator = new IDGenerator("xmppevent"); 
		}

		@Override
		public void processPacket(org.jivesoftware.smack.packet.Packet message) {
			
			if (!message.getFrom().equalsIgnoreCase(session.getParticipantName())){
				
				String content = ((org.jivesoftware.smack.packet.Message)message).getBody().trim();
				String[] parts = message.getFrom().split("/");
				if(parts.length>1){
					String from = parts[1];
	
					print("Received over XMPP from '"+from+"': "+content);
		
					if(waitOnlyFirst){
						stop();
					}
					
					Resource messageID = this.messageIDGenerator.getNewResource();
		
					//Adds beliefs about the message
					StringBuilder toAdd = new StringBuilder();
					toAdd.append('<');
					toAdd.append(messageID.toString());
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.occured);
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('{');
					toAdd.append('<');
					toAdd.append(SaplConstants.PARAM_NS);
					toAdd.append("type");
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.is);
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('"');
					toAdd.append("XMPP");
					toAdd.append('"');
					toAdd.append('.');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.PARAM_NS);
					toAdd.append("message");
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.is);
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('"');
					toAdd.append(content);
					toAdd.append('"');
					toAdd.append('.');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.PARAM_NS);
					toAdd.append("sender");
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.is);
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('"');
					toAdd.append(from);
					toAdd.append('"');
					toAdd.append('.');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.PARAM_NS);
					toAdd.append("blackboardObject");
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(SaplConstants.is);
					toAdd.append('>');
					toAdd.append(' ');
					toAdd.append('<');
					toAdd.append(blackboardID.toString());
					toAdd.append('>');
					toAdd.append('}');
					
					try{
						addBeliefsN3(toAdd.toString());
						wakeReasoner();
					}
					catch(Throwable e){
						printException(e, true);
					}
				}
			}
		}
	}	

}
