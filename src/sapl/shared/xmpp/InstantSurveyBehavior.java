package sapl.shared.xmpp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import fi.vtt.usefil.questionnaire.DatabaseCommunicationInterface;
import fi.vtt.usefil.questionnaire.DialogEngine;
import fi.vtt.usefil.questionnaire.MultiUserChatSession;
//import fi.vtt.usefil.questionnaire.TestConsoleUI;
import fi.vtt.usefil.questionnaire.UserCommunicationInterface;
import sapl.core.SaplConstants;
import sapl.core.SaplQueryResultSet;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.shared.eii.OntonutListenerBehavior;
import sapl.util.IDGenerator;
import sapl.util.URITools;

/**
 * Executes a VTT Instant Survey session
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.2 (25.10.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class InstantSurveyBehavior extends OntonutListenerBehavior {

	public static String DEFAULT_NICKNAME = "SAPL";

	@Parameter (name="input") private String input;
	
	@Parameter (name="host") private String host;
	@Parameter (name="username") private String username;
	@Parameter (name="password") private String password;
	@Parameter (name="room") private String room;
	@Parameter (name="nickname") private String nickname;	

	@Parameter (name="session") private Resource sessionId;
	private MultiUserChatSession session;
	
	private DialogEngine engine;
	private UserCommunicationInterface ui;
	
	private static Boolean dialogLock = new Boolean(true);
	private static String ongoingDialogInput = null;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		super.initializeBehavior(parameters);
		
		this.input = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));

		this.sessionId = parameters.getOptionalResourceParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "session"));
		if(this.sessionId!=null){
			Object object = this.getFromBlackboard(this.sessionId);
			if (object == null || !(object instanceof MultiUserChatSession)) {
				throw new IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "session"), this.sessionId.toString(), State.notOnBlackboard);
			}
			else this.session = (MultiUserChatSession) object;
		}
		else{
			this.host = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "host"));
			this.username = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
			this.password = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));
			this.room = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "room"));
			this.nickname = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "nickname"), DEFAULT_NICKNAME);
		}
	}

	@Override
	protected void doAction() throws Exception {
		if(ongoingDialogInput!=null && ongoingDialogInput.equals(this.input)){
			print("Warning: Attempting to start already ongoing dialog "+this.input+". Skipping.");
			return;
		}
		
		synchronized (dialogLock) {
			ongoingDialogInput = this.input;
			super.doAction();
			init();
			execute();
			ongoingDialogInput = null;
		}
	}
	
	public void init(){
		
		engine = new DialogEngine();
		engine.initializeFromFile(this.input);
		
//		ui = new TestConsoleUI();
		
		if(this.session!=null)
			ui = session;
		else{
			MultiUserChatSession muc = new MultiUserChatSession(this.username, this.password, this.room, this.host);
			muc.setNickname(this.nickname);
			ui = muc;
		}
		
		engine.setDatabaseCommunicationInterface(new SaplConnector());
	}
	
	public void execute (){
		if(this.session!=null || ui.startup()){
			engine.setUserCommunicationInterface(ui);

			engine.run();

			try {
				Thread.sleep(1000); //waiting a little, otherwise last message may not be actually sent 
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if(this.session==null) ui.shutdown();
		}		
	}	

	private class SaplConnector implements DatabaseCommunicationInterface {

		@Override
		public List<Map<String, String>> executeQuery(String query) throws JSONException, IOException {
			SaplQueryResultSet rs = hasBeliefsN3(query);
			if(rs==null) 
				return new ArrayList<Map<String, String>>();
			else
				return new ArrayList<Map<String, String>>( rs.getSolutions() );
		}

		@Override
		public void addMeasurement(String type, String value){
			Map<String, String> vars = new HashMap<String, String>();
			vars.put("property", type);
			vars.put("value", value);
			vars.put("timestamp", String.valueOf(System.currentTimeMillis()));
			
			IDGenerator eventIDGenerator = new IDGenerator("_InstantSurvey_"); 
			Resource eventID = eventIDGenerator.getNewResource();
			vars.put("this",eventID.toString());
			
			addBeliefs(ontonutSemantics, vars);
			wakeReasoner();
		}

		@Override
		public String getQueryLanguageName() {
			return "sapl";
		}

	}
}

