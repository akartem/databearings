package sapl.shared;

import java.net.URL;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;

/** 
 * Monitors whether an HTTP service is online or offline
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (13.03.2017)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015-2017, VTT
 * 
 * */

public class HttpPresenceSensorBehavior extends ReusableAtomicBehavior {

	@Parameter (name="url") private URL url;
	@Parameter (name="interval") private int interval; //seconds, default 60
	
	@Parameter (name="online") private String online;
	@Parameter (name="offline") private String offline;
	
	protected Boolean lastState = null;

	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException{
		this.executeInThread = true;
		
		this.url = parameters.getObligatoryURLParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "url"));
		this.interval = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "interval"), 60);

		this.online = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "online"));
		this.offline = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "offline"));
	}

	protected void doAction() throws Exception {
		while(!finished){
			Boolean last = lastState;
			try {
				HttpTools.getContent(this.url.toString(), HttpOptions.defaultOptions());
				if(last==null || last != true){
					
					print(this.url+" is now online");
					
					lastState = true;
					this.addBeliefs(online, null);
					this.wakeReasoner();
				}
			} catch (Exception ex) {
				if(last==null || last != false){
					
					print(this.url+" is now offline");
					
					lastState = false;
					this.addBeliefs(offline, null);
					this.wakeReasoner();
				}
			}

			if(!finished) Thread.sleep(this.interval*1000);
		}

	}
}
