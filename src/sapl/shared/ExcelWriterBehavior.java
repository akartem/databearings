package sapl.shared;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;

import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Produces an Excel file (single sheet, single table) from a given fact of the predefined structure 
 * 
 * @author Artem Katasonov (Elisa) 
 * 
 * @version 5.2 (11.02.2020)
 * 
 * @since 5.2
 * 
 * Copyright (C) 2020, Elisa
 * 
 * */

public class ExcelWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;

	@Parameter (name="result") private String resultTo;

	@Parameter (name="sort") private boolean sort;

	int rowCount = 0;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.sort = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "sort"), false);
	}

	@Override
	public void doAction() {
		String fileName = this.resultTo;
		String sheetName = null;
		int index = resultTo.lastIndexOf("#");
		if(index != -1){
			sheetName = fileName.substring(index+1);
			fileName = fileName.substring(0, index);
		}

		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
	        XSSFSheet sheet = sheetName==null ? workbook.createSheet() : workbook.createSheet(sheetName);
			
	        recursion(this.input, sheet);
			
			File datafile = new File(fileName);
			File p = datafile.getParentFile();
			if ((p != null) && !p.exists()) {
				boolean directoryMakingSuccessful = p.mkdirs();
				if (!directoryMakingSuccessful) {
					System.err.println("Failed creating necessary directories : " + p.getCanonicalPath());
					this.setFailed();
			        workbook.close();
					return;
				}
			}

			FileOutputStream outputStream = new FileOutputStream(fileName);
	        workbook.write(outputStream);
	        outputStream.close();
	        
	        workbook.close();
	        
		} catch (Exception e) {
			e.printStackTrace();
			this.setFailed();
		}
		
	}
	
	private List<String> recursion(String contextID, XSSFSheet sheet){
		
		Map<String, List<String>> rows = null;
		Map<String, String> values = null;
		
		List<String> result = null;
		
		List<SemanticStatement> vec = getStatements(contextID);
		
		if(vec!=null){
			Iterator<SemanticStatement> it = vec.iterator();
			while(it.hasNext()){
				SemanticStatement st=it.next();
				if(st.predicate.equals(SaplConstants.DATA_NS+"table"))
					recursion(st.object, sheet);
				else if(st.predicate.equals(SaplConstants.DATA_NS+"row")){
					List<String> list = recursion(st.object, sheet);
					if(!sort){
						Row r = sheet.createRow(this.rowCount++);
						for(int i=0; i<list.size(); i++) {
			                Cell cell = r.createCell(i);
			                cell.setCellValue(list.get(i));						
						}
					}					
					else{
						if(rows==null) rows = new HashMap<String, List<String>>();
						rows.put(st.subject, list);
					}
				}
				else if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
					if(values==null) values = new HashMap<String, String>();
					values.put(st.subject, st.object);
				}
			}
		}
		
		if(sort && rows!=null){
			List<String> keys = new ArrayList<String>(rows.keySet());
			Collections.sort(keys);
			for(String key: keys){
				List<String> list = rows.get(key);
				Row r = sheet.createRow(this.rowCount++);
				for(int i=0; i<list.size(); i++) {
	                Cell cell = r.createCell(i);
	                cell.setCellValue(list.get(i));						
				}
			}
		}
		if(values!=null){
			List<String> keys = new ArrayList<String>(values.keySet());
			Collections.sort(keys);
			result = new ArrayList<String>();
			for(String key: keys){
				result.add(values.get(key));
			}
		}
		
		return result;
	}
	
}
