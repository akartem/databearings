package sapl.shared;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.URITools;

/**
 * Outputs to the console (and to the log) either: given text, contents of the given Context (as N3).
 * If no arguments are given, empty string is printed out.
 * 
 * Parameters:
 * p:text The text to print. Default empty string.
 * p:context The Context, the content of which to print out (also works with a single statement). Optional.
 * p:statement The Statement to print out without using prefixes. Optional.
 * p:error	If �true�, the system error stream is printed to. Default false.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.5 (11.04.2017)
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *
 * */

public class PrintBehavior extends ReusableAtomicBehavior {

	@Parameter (name="text") private String toPrint;
	@Parameter (name="context") private String toPrintContext;
	@Parameter (name="statement") private String toPrintStatement;
	@Parameter (name="error") private boolean isError;
	@Parameter (name="turtle") private boolean turtle;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.toPrint = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "text"), "");
		this.toPrintContext = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "context"), null); 
		this.toPrintStatement = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "statement"), null); 
		this.isError = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "error"), false);
		this.turtle = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "turtle"), true);
	}

	@Override
	protected void doAction() {
		if (this.toPrintContext!=null) {
			SaplN3ProducerOptions options = new SaplN3ProducerOptions(true, true, this.turtle);
			this.printout(this.produceN3(this.toPrintContext, options));
		} else if (this.toPrintStatement!=null) {
			SaplN3ProducerOptions options = new SaplN3ProducerOptions(false, false, this.turtle);
			this.printout(this.produceN3(this.toPrintStatement, options));
		} else {
			this.printout(this.toPrint);
		}
	}

	public void printout(String text) {
		if (this.isError) {
			this.printToErr(text);
		}
		else{
			if(this.executingWithResult()){
				this.writeToResult(text);
				this.writeToResult("\n");
			}
			else super.print(text);
		}
	}

}
