package sapl.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.core.sapln3.ResourcePrefixer;
import sapl.util.URITools;

/**
 * Produces an XML document from a given fact of the predefined structure, 
 * which uses d:tree, d:branch, d:element, d:attribute,  d:value properties, 
 * and d:<nnnn> IDs (the same as in XmlReaderBehavior), and writes it either 
 * into a file or as a part of a new fact created in the facts base.
 * 
 * Parameters:
 * p:input	ID of Context that includes a fact to transform into XML document. Mandatory
 * p:result	Either the name of the file to save the content to, or a Context providing
 * the structure to be added (has to use the ?result variable that will be
 * substituted with the content). Mandatory
 * p:indents	If true, line breaks and indentation is used to separate XML elements.
 * Otherwise, a single line is produced. Default false
 * p:sort	If true, the order of sub-elements of an XML element is enforced
 * based on their d:<nnnn> IDs. Default false
 * p:encoding	If given, is added as an attribute into XML preamble.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (24.04.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class XmlWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="result") private String resultTo;
	@Parameter (name="indents") private boolean indents;
	@Parameter (name="sort") private boolean sort;
	@Parameter (name="encoding") private String encoding;

	private final static int indent_size = 4;	
	private final static String white="                                                                                                                        ";

	ResourcePrefixer prefixer;
	HashSet<String> namespaces = new HashSet<String>();

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryContainerID(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.indents = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "indents"), false);
		this.sort = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "sort"), false);
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"));
	}	

	@Override
	public void doAction() {
		prefixer = this.getPrefixer();

		String result = "<?xml version=\"1.0\"" + ((this.encoding == null) ? "" : " encoding=\"" + this.encoding + "\"") + "?>\n"
				+ this.recursion(this.input, 0, null).toString();

		boolean ok = RABUtils.putResult(this.myEngine, result, resultTo);
		if (!ok) {
			this.setFailed();
		}
	}

	private StringBuilder recursion(String forContextID, int recursionLevel, StringBuilder attrs) {
		StringBuilder result = new StringBuilder();
		List<SemanticStatement> statements = this.getStatements(forContextID);

		if (statements != null) {

			Map<String, String> names = new HashMap<String, String>(); 
			Map<String, String> values = new HashMap<String, String>(); 
			Map<String, String> valueAttrs = new HashMap<String, String>(); 

			Set<String> namesWithValues = new HashSet<String>();

			List<ElementInfo> elements = new ArrayList<ElementInfo>();
			List<ElementInfo> attributes = new ArrayList<ElementInfo>();  

			Iterator<SemanticStatement> it = statements.iterator();

			while (it.hasNext()) {
				SemanticStatement st = it.next();

				if (st.predicate.equals(SaplConstants.DATA_NS+"element")) {
					String name = st.object;

					HashMap<String, String> usedNS = new HashMap<String, String>();
					prefixer.prefixResource(name, usedNS);
					if(!usedNS.isEmpty()){
						String ns = usedNS.keySet().iterator().next();
						if(!namespaces.contains(ns)){
							namespaces.add(ns);
							String prefix = usedNS.get(ns);
							prefix = prefix.substring(0, prefix.length()-1);
							attributes.add(new ElementInfo("", makeAttribute("xmlns:"+prefix, ns)));
						}
					}

					String value = values.get(st.subject);
					if(value==null)
						names.put(st.subject, st.object);
					else{
						String elemAttrs = valueAttrs.get(st.subject);
						elements.add(new ElementInfo(st.subject, makeElement(name, elemAttrs, value, recursionLevel)));
					}
				}
				else if (st.predicate.equals(SaplConstants.DATA_NS+"attribute")) {
					String name = st.object;
					String value = values.get(st.subject);
					if(value==null)
						names.put(st.subject, st.object);
					else{
						attributes.add(new ElementInfo(st.subject, makeAttribute(name, value)));
						values.remove(st.subject);
					}
				}
				else if (st.predicate.equals(SaplConstants.DATA_NS+"branch")) {
					StringBuilder branchAttrs = new StringBuilder(); 
					String value = recursion(st.object, recursionLevel+1, branchAttrs).toString();

					String name = names.get(st.subject);
					if(name==null){
						values.put(st.subject, value);
						valueAttrs.put(st.subject, branchAttrs.toString());
					}
					else {
						elements.add(new ElementInfo(st.subject,  makeElement(name, branchAttrs.toString(), value, recursionLevel)));
					}
				} 
				else if (st.predicate.equals(SaplConstants.DATA_NS+"value")) {
					String value = st.object;
					String name = names.get(st.subject);
					if(name==null){
						values.put(st.subject, value);
					}
					else{ 
						if(!namesWithValues.contains(st.subject)){
							attributes.add(new ElementInfo(st.subject, makeAttribute(name, value)));
							namesWithValues.add(st.subject);
						}
						//else ignore: should not be 2 values attached to the same node
					}
				}
			}

			if(attrs!=null){
				for(ElementInfo a : attributes) attrs.append(" "+a.content);
			}

			for (Map.Entry<String, String> entry : values.entrySet()){
				elements.add(new ElementInfo(entry.getKey(), makeElement(null, "", entry.getValue(), recursionLevel)));
			}

			if (sort) {
				Collections.sort(elements);
			}
			for (int i = 0; i < elements.size(); i++) {
				result.append(((i > 0) ? "\n" : "") + elements.get(i).content);
			}
		}
		return result;
	}


	private String makeElement(String tag, String attrs, String value, int recursionLevel){
		if(tag==null)
			return ((indents)?white.substring(0,(recursionLevel)*indent_size):"") + value;
		else{
			tag = prefixer.prefixResource(tag);
			if(tag.startsWith("<")) tag = tag.substring(1, tag.length()-1);
			
			if(value.isEmpty()) 
				return ((indents)?white.substring(0,(recursionLevel)*indent_size):"")
					+"<"+tag+attrs+"/>";
			else {
				String result = ((indents)?white.substring(0,(recursionLevel)*indent_size):"")
						+"<"+tag+attrs+">";
				if(value.contains("<") || value.contains("\n"))
					result += (indents?"\n":"") + value +
					((indents)?"\n"+white.substring(0,(recursionLevel)*indent_size):"");
				else 
					result += value.trim();
				result += "</"+tag+">";
	
				return result;
			}
		}
	}

	private String makeAttribute(String name, String value){
		return name+"="+"\""+value+"\"";
	}

	private static class ElementInfo implements Comparable<ElementInfo> {
		String id;
		String content;

		public ElementInfo(String id, String info) {
			this.id = id;
			this.content = info;
		}

		@Override
		public int compareTo(ElementInfo o) {
			return this.id.compareTo(o.id);
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof ElementInfo)) {
				return false;
			}
			ElementInfo elementInfo = (ElementInfo) obj;
			return this.id.equals(elementInfo.id);
		}

		@Override
		public int hashCode() {
			return this.id.hashCode();
		}
	}

}
