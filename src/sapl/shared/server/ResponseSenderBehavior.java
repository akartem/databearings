package sapl.shared.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.shared.server.ServerBehaviorServerEvent;
import sapl.util.StreamingUtils;
import sapl.util.URITools;

/** 
 * Sends an HTTP response to HTTP request received earlier by ServerBehavior RAB. 
 * One and only one of parameters p:text and p:file has to be given.
 * 
 * Parameters:
 * p:text	Text to send.	Optional
 * p:file	Name of file to send.	Optional
 * p:to	Blackboard ID for sapl.shared.server.ServerBehaviorServerEvent, 
 * to which responding. Obtained from the request fact injected by ServerBehavior.	Mandatory
 * p:contentType	Content type of response. Default	�text/plain�
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * 
 * @version 4.1 (29.09.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class ResponseSenderBehavior extends ReusableAtomicBehavior {

	/**
	 * From which size should the sender start to use a thread for the actual sending of data?
	 */
	private static final long THREADSTARTLIMIT = 10000;
	/**
	 * The message which will be send back, if null, fileName isn't null
	 */
	@Parameter (name="text") private String message;
	/**
	 * The file which will be send back, if null, message isn't null
	 */
	@Parameter (name="file") private File file;

	@Parameter (name="contentType") private String contentType;

	/**
	 * The event on which to reply
	 */
	private ServerBehaviorServerEvent event;
	/**
	 * the ID of the event on the blackboard of the agent.
	 */
	@Parameter (name="to") private Resource eventID;
	/**
	 * Has this behavior been started before?
	 */
	private boolean started = false;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.executeInThread = false; //creates own thread, so does not use the general mechanism

		if (this.started) {
			//the behavior has been started before, no need to reinitialize parameters.
			return;
		}
		this.message = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "text"));
		this.file = parameters.getOptionalFileParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "file"));
		if ((this.message == null) && (this.file == null)) {
			throw new IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "text"), this.message, State.missing);
		}
		if ((this.message != null) && (this.file != null)) {
			throw new IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "file"), this.file.getName(), State.conflicting);
		}
		this.eventID = parameters.getObligatoryResourceParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "to"));
		Object object = this.getFromBlackboard(this.eventID);
		if (object == null || !(object instanceof ServerBehaviorServerEvent)) {
			throw new IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "to"), this.eventID.toString(), State.notOnBlackboard);
		}
		else this.event = (ServerBehaviorServerEvent) object;

		this.contentType = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentType"), "text/plain"); 
	}

	@Override
	protected void doAction() {
		if (this.started) {
			//the behavior has been started before, no need to do anything.
			return;
		}
		this.block(); //don't call this behavior a second time.

		this.started = true;
		this.finished = false;//become cyclic, this is because the agent then believes that this behavior is active till its inner thread has finished sending the reply.

		//reply to the request in a separate thread. if the file or the message is longer as ResponseSenderBehavior.THREADSTARTLIMIT

		Runnable replyingRunnable = new Runnable() {

			@Override
			public void run() {
				//try sending the response
				this.send();

				//after trying to send the response, we complete the continuation and remove the event from the Blackboard.
				ResponseSenderBehavior.this.event.getContinuation().complete();
				ResponseSenderBehavior.this.removeFromBlackboard(ResponseSenderBehavior.this.eventID);
				//The behavior which started this thread is now finished
				ResponseSenderBehavior.this.setFinished();
			}

			private void send() {
				if (ResponseSenderBehavior.this.message != null) {
					//there is a message
					try {
						//send message to stream
						HttpServletResponse response = ResponseSenderBehavior.this.event.getResponse();
						response.setContentType(ResponseSenderBehavior.this.contentType);
						response.setCharacterEncoding("UTF-8");
						OutputStream out = response.getOutputStream();
						PrintWriter pw = new PrintWriter(new OutputStreamWriter(out, "UTF-8"), true);		
						//PrintWriter pw = new PrintWriter(new OutputStreamWriter(out));
						pw.print(ResponseSenderBehavior.this.message);
						pw.flush();
						pw.close();
						out.close();

					} catch (IOException e) {
						ResponseSenderBehavior.this.setFailed();
						ResponseSenderBehavior.this.printException(e, true);
						return;
					}
				} else {
					//message from file
					//send file as a result to the request:
					OutputStream os;

					try {
						os = ResponseSenderBehavior.this.event.getResponse().getOutputStream();
					} catch (IOException e1) {
						ResponseSenderBehavior.this.setFailed();
						ResponseSenderBehavior.this.printException(e1, true);
						return;
					}
					try {
						StreamingUtils.sendFileToOutputStream(ResponseSenderBehavior.this.file, os);
					} catch (FileNotFoundException e) {
						ResponseSenderBehavior.this.printException(e, true);
						ResponseSenderBehavior.this.setFailed();
						return;
					} catch (IOException e) {
						ResponseSenderBehavior.this.printException(e, true);
						ResponseSenderBehavior.this.setFailed();
						return;
					}
				}
			}

		};
		if (((this.message != null) && (this.message.length() > ResponseSenderBehavior.THREADSTARTLIMIT))
				|| ((this.file != null) && (this.file.length() > ResponseSenderBehavior.THREADSTARTLIMIT))) {
			//big enough to start a thread
			new Thread(replyingRunnable).start();
		} else {
			//it shouldn't take long, do immediately
			replyingRunnable.run();
		}

		return;
	}

}
