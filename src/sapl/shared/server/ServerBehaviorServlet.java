package sapl.shared.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.continuation.Continuation;
import org.eclipse.jetty.continuation.ContinuationSupport;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.Resource;
import sapl.util.IDGenerator;
import sapl.util.StreamingUtils;
import sapl.util.StringEscape;


/**
 * The servlet implementing the actual behavior of the server When a request arrives, the request is wrapped together with a continuation
 * object in an {@link ServerBehaviorServerEvent} Then beliefs are added to the agent about the event happening. For aformat of the
 * beliefs, see {@link ServerBehavior}
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (05.01.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class ServerBehaviorServlet extends HttpServlet {

	public static boolean INTERACTION_PRINT = true;
	public static boolean CONTENT_PRINT = false;

	private static final long serialVersionUID = 1L;

	ReusableAtomicBehavior myServer;

	protected final IDGenerator requestIDGenerator;
	private final int timeout;

	/**
	 * create a server attached to a certain agent
	 * 
	 * @param server
	 *            the server behavior this Servlet is working with
	 * @param timeout
	 *            the timeout for answering to requests.
	 */
	public ServerBehaviorServlet(ReusableAtomicBehavior server, int timeout) {
		this.myServer = server;
		this.timeout = timeout;
		this.requestIDGenerator = new IDGenerator("serverevent");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if(req.getPathInfo().equals("/favicon.ico")){
			StreamingUtils.sendFileToOutputStream(new File("favicon.ico"), resp.getOutputStream());
		}
		else this.doPost(req, resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String strRequest = request.getRequestURI().substring(1);
		
		String query = request.getQueryString();
		if(query!=null) strRequest += "?"+query;

		strRequest = URLDecoder.decode(strRequest, "UTF-8");

		int len = request.getContentLength();
		
		String content = null;
		if(len>0){
			BufferedReader reader = request.getReader();
			char[] buff = new char[len]; 
			int read = reader.read(buff,0,len);
			content = String.valueOf(buff,0,read);
		}

		if(INTERACTION_PRINT){	
			String from = request.getRemoteHost();
			myServer.print("Received over HTTP from "+from+": "+request.getMethod()+" "+strRequest+(content==null?"":" . Content of "+content.length()+" chars"));
			if(CONTENT_PRINT && content!=null) 
				myServer.print("HTTP Content: "+content);
		}
	
		handleRequest(request, response, strRequest, content);
	}
		
	protected void handleRequest(HttpServletRequest request, HttpServletResponse response, String strRequest, String content){
		//we just add the request and response to the agent's blackboard and add the belief of this event happening.
		//create continuation object
		final Continuation continuation = ContinuationSupport.getContinuation(request);
		continuation.suspend(); // always suspend before registration
		continuation.setTimeout(this.timeout);

		//add the event to the blackboard
		Resource blackboardID = this.myServer.addOnBlackboard(new ServerBehaviorServerEvent(request, response, continuation));
		Resource requestID = this.requestIDGenerator.getNewResource();

		//Adds beliefs about the request
		StringBuilder toAdd = new StringBuilder();
		toAdd.append('<');
		toAdd.append(requestID.toString());
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.occured);
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('{');
		toAdd.append('<');
		toAdd.append(SaplConstants.PARAM_NS);
		toAdd.append("type");
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.is);
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('"');
		toAdd.append("HTTP");
		toAdd.append('"');
		toAdd.append('.');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.PARAM_NS);
		toAdd.append("method");
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.is);
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('"');
		toAdd.append(request.getMethod());
		toAdd.append('"');
		toAdd.append('.');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.PARAM_NS);
		toAdd.append("request");
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.is);
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('"');
		toAdd.append(StringEscape.escape(strRequest));
		toAdd.append('"');
		toAdd.append('.');
		toAdd.append(' ');

		if(content!=null){
			toAdd.append('<');
			toAdd.append(SaplConstants.PARAM_NS);
			toAdd.append("content");
			toAdd.append('>');
			toAdd.append(' ');
			toAdd.append('<');
			toAdd.append(SaplConstants.is);
			toAdd.append('>');
			toAdd.append(' ');
			toAdd.append('"');
			toAdd.append(StringEscape.escape(content));
			toAdd.append('"');
			toAdd.append('.');
			toAdd.append(' ');
		}
		
		toAdd.append('<');
		toAdd.append(SaplConstants.PARAM_NS);
		toAdd.append("blackboardObject");
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(SaplConstants.is);
		toAdd.append('>');
		toAdd.append(' ');
		toAdd.append('<');
		toAdd.append(blackboardID.toString());
		toAdd.append('>');
		toAdd.append('}');

		try{	
			this.myServer.addBeliefsN3(toAdd.toString());
		}
		catch(Throwable e){
			this.myServer.printException(e, true);
		}
		
		this.myServer.wakeReasoner();

	}
}
