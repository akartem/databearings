package sapl.shared.server;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.continuation.Continuation;

/**
 * The serverEvent from {@link ServerBehavior} This event encapsulates a {@link HttpServletRequest}, {@link HttpServletResponse} and a
 * {@link Continuation} connected to a http event. This class is immutable 18.3.2010
 * 
 * @author Michael Cochez (University of Jyäskylä) 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.0 (01.07.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class ServerBehaviorServerEvent {
	private final HttpServletRequest request;
	private final HttpServletResponse response;
	private Continuation continuation;

	public ServerBehaviorServerEvent(HttpServletRequest request, HttpServletResponse response, Continuation continuation) {
		super();
		this.response = response;
		this.request = request;
		this.continuation = continuation;
	}

	public HttpServletResponse getResponse() {
		return this.response;
	}

	public HttpServletRequest getRequest() {
		return this.request;
	}

	public Continuation getContinuation() {
		return this.continuation;
	}

}
