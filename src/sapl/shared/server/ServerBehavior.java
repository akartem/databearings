package sapl.shared.server;

import javax.servlet.Servlet;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;


/**
 * Starts an HTTP server based on Jetty. For each request received, 
 * injects into the facts base a fact. An instance of 
 * sapl.shared.server.ServerBehaviorServerEvent is placed on 
 * the Engine�s blackboard and its ID is published as part of the injected fact. 
 * A response to the request is to be sent later by ResponseSenderBehavior RAB.
 * 
 * Parameters:
 * p:port	Port of for HTTP server. Mandatory
 * p:timeout How long a request handling is allowed to be in progress. Default 300000 (5 min)
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.5 (19.06.2017)
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class ServerBehavior extends ReusableAtomicBehavior {

	/**
	 * How long is a request allowed to be in progress by default? (in milliseconds)
	 */
	public static final int REQUEST_TIMEOUT = 300000;//5 minutes
	/**
	 * Has the server been started before? If this is the case, it doesn't have to be started over again.
	 */
	private boolean started = false;
	/**
	 * The port on which the server is listening.
	 */
	@Parameter (name="port") protected int port;
	/**
	 * What is the timeout for the requests on the HTTP server?
	 */
	@Parameter (name="timeout") protected int timeout;

	/**Jetty Server*/
	Server myServer;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.port = parameters.getObligatoryNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "port"));
		this.timeout = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "timeout"), ServerBehavior.REQUEST_TIMEOUT);
	}	

	@Override
	protected void doAction(){
		if (!this.started) {
			this.finished = false; // make this behavior cyclic, so it doesn't disappear.
			//start the server
			myServer = new Server(this.port);
			ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
			context.setContextPath("/");
			myServer.setHandler(context);
			Servlet agentServerBehaviorServlet = createServlet();

			ServletHolder sh = new ServletHolder(agentServerBehaviorServlet);
			context.addServlet(sh, "/*");

			try {
				myServer.start();
			} catch (Exception e) {
				this.printException(e, true);
				//in case an error occurs, the agent has to know it.
				this.finished = true;
				this.setFailed();
			}

			this.started = true;
		}
		//Block this RAB from being executed over and over, there is nothing to be done here. The only reason this must still be alive is so the agent knows that its server is running.
		this.block();
	}

	protected Servlet createServlet(){
		return new ServerBehaviorServlet(this, this.timeout);
	}
	
	@Override
	protected void stop(){
		this.setFinished();
		try {
			myServer.stop();
		} catch (Exception e) {
			this.printException(e, true);
		}
	}

}