package sapl.shared;

import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Takes the content from either a file of the passed string itself. 
 * The content is a JSON document. 
 * Adds the content to the facts base in the form of a fact of the predefined 
 * structure that uses d:tree, d:branch, d:attribute, d:value properties, and 
 * d:<nnnn> IDs.
 * 
 * Parameters:
 * p:input	Name of the file to read (can be local or online) or a text string. Mandatory
 * p:uri	URI to use as identifier of the content. Mandatory
 * p:encoding	The name of the file encoding. Default UTF-8
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (13.03.2017)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2017, VTT
 *
 * */

public class JsonReaderBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="uri") private String id;
	@Parameter (name="encoding") private String encoding;	

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters)
			throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.id = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"), null);
	}

	@Override
	protected void doAction() throws Exception {
	
		HttpOptions options = HttpOptions.defaultOptions();
		options.encoding = this.encoding;

		InputStream is;
		try{
			is = HttpTools.getContentInputStream(this.input, options);
		}
		catch(Exception e){
			this.printException(e, true);
			this.setFailed();
			return;
		}
		
		JSONTokener tokener = new JSONTokener(is);
		JSONObject json = new JSONObject(tokener);
		
		is.close();

		StringBuilder beliefs = new StringBuilder();

		beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
		beliefs.append("<"+this.id+">" + " d:tree { ");

		beliefs.append(processJSONObject(json));

		beliefs.append(" }");

		this.addBeliefsN3(beliefs.toString());
		this.wakeReasoner();
	}

	protected String processJSONObject(JSONObject json) throws JSONException{
		StringBuilder beliefs = new StringBuilder();
		String[] names = JSONObject.getNames(json);

		if(names!=null){

			String zeros = "0000";
			int index = 1;

			for(String name : names){
				Object o = json.get(name);

				if(o instanceof JSONArray){
					JSONArray array = (JSONArray)o;
					for(int i = 0; i<array.length();i++){
						Object elem = array.get(i);
						if(elem instanceof JSONObject){
							String tagID = String.valueOf(index++);
							tagID = zeros.substring(0, zeros.length() - tagID.length()) + tagID;
							beliefs.append("d:"+tagID+" d:attribute \""+name+"\"; d:branch { ");
							beliefs.append(processJSONObject((JSONObject)elem));
							beliefs.append(" }. ");
						}
						else{
							String tagID = String.valueOf(index++);
							tagID = zeros.substring(0, zeros.length() - tagID.length()) + tagID;
							beliefs.append("d:"+tagID+" d:attribute \""+name+"\"; d:value \""+ RABUtils.escapeString(processValue(elem))+"\". ");
						}
					}
				}
				else if(o instanceof JSONObject){
					String tagID = String.valueOf(index++);
					tagID = zeros.substring(0, zeros.length() - tagID.length()) + tagID;

					beliefs.append("d:"+tagID+" d:attribute \""+name+"\"; d:branch { ");
					beliefs.append(processJSONObject((JSONObject)o));
					beliefs.append(" }. ");
				}
				else {
					String tagID = String.valueOf(index++);
					tagID = zeros.substring(0, zeros.length() - tagID.length()) + tagID;
					beliefs.append("d:"+tagID+" d:attribute \""+name+"\"; d:value \""+ RABUtils.escapeString(processValue(o))+"\". ");
				}


			}
		}

		return beliefs.toString();
	}

	private String processValue(Object value) throws JSONException{
		return value.toString();
	}


}
