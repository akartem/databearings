package sapl.shared;

import java.io.File;
import java.nio.file.Files;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/** 
 * Deletes a file.
 * 
 * @author Artem Katasonov (Elisa) 
 * 
 * @version 4.7 (27.06.2019)
 * 
 * @since 4.7
 * 
 * Copyright (C) 2019, Elisa
 *
 * */

public class FileDeleteBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private File file;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.file = parameters.getObligatoryFileParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
	}

	@Override
	protected void doAction() throws Exception{
		Files.delete(this.file.toPath());
	}
}