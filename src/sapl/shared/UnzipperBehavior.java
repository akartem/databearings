package sapl.shared;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Unzips a ZIP archive to the same folder. If parameter p:delete is true, 
 * deletes the original file. If an I/O exception occurs, ends in failure. 
 * Otherwise, ends in success.
 * 
 * Parameters:
 * p:input	Address of the ZIP file. Mandatory.
 * p:delete	Whether to delete the ZIP file after unzipping its contents. Default false.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.6 (26.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov 
 * 
 * */

public class UnzipperBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private File file;
	@Parameter (name="delete") private boolean delete;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.file = parameters.getObligatoryFileParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.delete = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "delete"), false);
	}

	@Override
	public void doAction() {

		String path = this.file.getParent();
		ZipFile zipFile = null;
		try {
			zipFile = new ZipFile(this.file);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				if (!entry.isDirectory()) {
					File f = new File(path + "/" + entry.getName());
					File p = f.getParentFile();
					
					if ((p != null) && !p.exists()) {
						boolean directoryMakingSuccessFul = p.mkdirs();
						if (!directoryMakingSuccessFul) {
							printToErr("Failed creating necessary directories : " + p.getCanonicalPath());
							this.setFailed();
							return;
						}
					}
					InputStream in = null;
					BufferedInputStream bin = null;
					OutputStream out = null;
					BufferedOutputStream bos = null;
					try {
						in = zipFile.getInputStream(entry);
						bin = new BufferedInputStream(in);
						out = new FileOutputStream(path + "/" + entry.getName());
						bos = new BufferedOutputStream(out);
						byte[] buffer = new byte[1024];
						int len;
						while ((len = bin.read(buffer)) >= 0) {
							bos.write(buffer, 0, len);
						}
					} finally {
						if (bos != null) {
							bos.close();
						} else if (out != null) {
							//close at least the underlying stream
							out.close();
						}
						if (bin != null) {
							bin.close();
						} else if (in != null) {
							in.close();
						}
					}
				}
			}

			//this.print(this.file.getPath() + " unzipped");
			zipFile.close();						

			if (this.delete) {
				try{
					Files.delete(this.file.toPath());
				}
				catch (IOException e){
					e.printStackTrace();
				}
			}

		} catch (IOException ex) {
			ex.printStackTrace();
			this.setFailed();
		} finally {
			try {
				zipFile.close();
			} catch (IOException ex) {
				ex.printStackTrace();
				this.setFailed();
			}
		}
	}
}
