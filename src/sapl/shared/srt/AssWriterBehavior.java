package sapl.shared.srt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Writes a file with movie subtitles using .ass format (that allows forcing screen placement of subtitles
 * and, via that, side-by-side subtitles in several languages)
 *  
 * @author Artem Katasonov (VTT)
 *  
 * @version 4.6 (28.02.2018)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015-2018, VTT
 * 
 * */

public class AssWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="encoding") private String encoding;
	@Parameter (name="result") private String resultTo;
	
	@Parameter (name="sort") private boolean sort;
	
	public final static int ELEMENT_COUNTER = 1;
	public final static int ELEMENT_TIME = 2;
	public final static int ELEMENT_TEXT = 3;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"), null);
		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.sort = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "sort"), false);
	}

	@Override
	public void doAction() {
		StringBuilder result = recursion(this.input);
		
		String prefix =
		"[Script Info]"+"\n"+
		"ScriptType: v4.00+"+"\n"+
		"Collisions: Normal"+"\n"+
		"PlayDepth: 0"+"\n"+
		"Timer: 100,0000"+"\n"+
		"Video Aspect Ratio: 0"+"\n"+
		"WrapStyle: 0"+"\n"+
		"ScaledBorderAndShadow: no"+"\n\n"+
		"[V4+ Styles]"+"\n"+
		"Format: Name,Fontname,Fontsize,PrimaryColour,SecondaryColour,OutlineColour,BackColour,Bold,Italic,Underline,StrikeOut,ScaleX,ScaleY,Spacing,Angle,BorderStyle,Outline,Shadow,Alignment,MarginL,MarginR,MarginV,Encoding"+"\n"+
		"Style: Default,Arial,20,&H00FFFFFF,&H00FFFFFF,&H80000000,&H80000000,-1,0,0,0,100,100,0,0,1,3,0,2,10,10,10,0"+"\n"+
		"Style: SE,Arial,20,&H00F9FFFF,&H00FFFFFF,&H80000000,&H80000000,-1,0,0,0,100,100,0,0,1,3,0,3,10,10,10,0"+"\n"+
		"Style: SW,Arial,20,&H00F9FFF9,&H00FFFFFF,&H80000000,&H80000000,-1,0,0,0,100,100,0,0,1,3,0,1,10,10,10,0"+"\n\n"+
		"[Events]"+"\n"+
		"Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text"+"\n";

		boolean ok = RABUtils.putResult(this.myEngine, prefix+result.toString(), resultTo, encoding, false);
		if (!ok) {
			this.setFailed();
		}		
	}

	private StringBuilder recursion(String contextID) {
		StringBuilder result = new StringBuilder();
		
		Map<String, StringBuilder> rows = null;
		Map<String, String> values = null;
		
		List<SemanticStatement> vec = getStatements(contextID);
		if(vec!=null){
			for(SemanticStatement st : vec){
				if(st.predicate.equals(SaplConstants.DATA_NS+"table"))
					return recursion(st.object);
				else if(st.predicate.equals(SaplConstants.DATA_NS+"row")){
					if(!sort){
						if(result.length()>0) result.append("\n");
						result.append(recursion(st.object));
					}
					else{
						if(rows==null) rows = new HashMap<String, StringBuilder>();
						rows.put(st.subject, recursion(st.object));
					}
				}
				else if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
					if(values==null) values = new HashMap<String, String>();
					values.put(st.subject, st.object);
				}
			}
			if(values!=null){
				String start = values.get(SaplConstants.DATA_NS+"0002").replace(",", ".");
				int ind = start.indexOf(".");
				if(ind!=-1 && start.length()>ind+3) start = start.substring(0,start.length()-1);
				String end = values.get(SaplConstants.DATA_NS+"0003").replace(",", ".");
				ind = end.indexOf(".");
				if(ind!=-1 && end.length()>ind+3) end = end.substring(0,end.length()-1);

				String text = values.get(SaplConstants.DATA_NS+"0004").replace("\n", "\\N");

				String style = values.get(SaplConstants.DATA_NS+"0005");

				result.append("Dialogue: 0,"+start+","+end+","+style+",,0000,0000,0000,,"+text);
			}
			else if(sort && rows!=null){
				List<String> keys = new ArrayList<String>(rows.keySet());
				Collections.sort(keys);
				for(String key: keys){
					if(result.length()>0) result.append("\n");
					result.append(rows.get(key));
				}
			}

		}
		
		return result;
	}
}
