package sapl.shared.srt;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;


/**
 * Parses a file with movie subtitles in the most typical .srt format
 *  
 * @author Artem Katasonov (VTT)
 *  
 * @version 4.6 (28.02.2018)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015-2018, VTT
 * 
 * */

public class SrtReaderBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="encoding") private String encoding;
	@Parameter (name="uri") private String id;
	
	public final static int ELEMENT_COUNTER = 1;
	public final static int ELEMENT_TIME = 2;
	public final static int ELEMENT_TEXT = 3;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.id = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"), null);
	}

	@Override
	public void doAction() {

		try{
			HttpOptions options = HttpOptions.defaultOptions();
			options.encoding = this.encoding;
			
			String content = HttpTools.getContent(this.input, options);

			String[] rows = content.split("\n");

			StringBuilder beliefs = new StringBuilder(content.length());
			beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
			beliefs.append("<"+ this.id +">"+ " d:table {");

			String zeros = "000000000000000000000000";
			int zerosToUseRow = Math.max(4, (int)Math.ceil(Math.log10(rows.length / 4)));
			
			int nowIn = ELEMENT_COUNTER;
			
			String counter = null;
			String start = null;
			String end = null;
			String text = "";
			
			int elementN = 1;

			for(int i=0; i<rows.length; i++){
				String row = rows[i];
				
				if(nowIn == ELEMENT_COUNTER){
					if(row.trim().isEmpty()) continue;
					counter = row.trim();
					nowIn = ELEMENT_TIME;
				}
				else if(nowIn == ELEMENT_TIME){
					if(row.trim().isEmpty()) continue;
					String parts[] = row.split("-->");
					start = parts[0].trim().replace(",", ".");
					end = parts[1].trim().replace(",", ".");
					nowIn = ELEMENT_TEXT;
					text = "";
				}
				else if(nowIn == ELEMENT_TEXT){
					if(row.trim().isEmpty()){
						//Element finished
						String eid = String.valueOf(elementN);
						eid = zeros.substring(0, zerosToUseRow - eid.length()) + eid;
						eid = "d:"+eid;

						beliefs.append(((elementN>1) ? ". " : "") + eid + " d:row {");
						beliefs.append("d:0001 d:value "+"'"+counter+"'. ");
						beliefs.append("d:0002 d:value "+"'"+start+"'. ");
						beliefs.append("d:0003 d:value "+"'"+end+"'. ");
						beliefs.append("d:0004 d:value "+"\""+RABUtils.escapeString(text.trim())+"\"");
						beliefs.append("}");

						nowIn = ELEMENT_COUNTER;
						elementN++;
					}
					else{
						text += row.trim()+"\n";
					}
				}
			}

			beliefs.append("}");

			this.addBeliefsN3(beliefs.toString());
			this.wakeReasoner();
			
		} catch (Exception ex) {
			this.print(this.getClass().getName() + " error: Operation failed");
			this.printException(ex, true);
			this.setFailed();
		}
	}
}
