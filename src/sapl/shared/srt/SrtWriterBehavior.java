package sapl.shared.srt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Writes a file with movie subtitles using the most typical .srt format
 *  
 * @author Artem Katasonov (VTT)
 *  
 * @version 4.6 (28.02.2018)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015-2018, VTT
 * 
 * */

public class SrtWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="encoding") private String encoding;
	@Parameter (name="result") private String resultTo;
	
	@Parameter (name="sort") private boolean sort;
	
	public final static int ELEMENT_COUNTER = 1;
	public final static int ELEMENT_TIME = 2;
	public final static int ELEMENT_TEXT = 3;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"), null);
		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.sort = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "sort"), false);
	}

	@Override
	public void doAction() {
		StringBuilder result = recursion(this.input);
		
		boolean ok = RABUtils.putResult(this.myEngine, result.toString(), resultTo, encoding, false);
		if (!ok) {
			this.setFailed();
		}		
	}

	private StringBuilder recursion(String contextID) {
		StringBuilder result = new StringBuilder();
		
		Map<String, StringBuilder> rows = null;
		Map<String, String> values = null;
		
		List<SemanticStatement> vec = getStatements(contextID);
		if(vec!=null){
			for(SemanticStatement st : vec){
				if(st.predicate.equals(SaplConstants.DATA_NS+"table"))
					return recursion(st.object);
				else if(st.predicate.equals(SaplConstants.DATA_NS+"row")){
					if(!sort){
						if(result.length()>0) result.append("\n");
						result.append(recursion(st.object));
					}
					else{
						if(rows==null) rows = new HashMap<String, StringBuilder>();
						rows.put(st.subject, recursion(st.object));
					}
				}
				else if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
					if(values==null) values = new HashMap<String, String>();
					values.put(st.subject, st.object);
				}
			}
			if(values!=null){
				String counter = values.get(SaplConstants.DATA_NS+"0001");
				String start = values.get(SaplConstants.DATA_NS+"0002").replace(".", ",");
				String end = values.get(SaplConstants.DATA_NS+"0003").replace(".", ",");
				String text = values.get(SaplConstants.DATA_NS+"0004");
				
				result.append(counter+"\n"+start+" --> "+end+"\n"+text+"\n");
			}
			else if(sort && rows!=null){
				List<String> keys = new ArrayList<String>(rows.keySet());
				Collections.sort(keys);
				for(String key: keys){
					if(result.length()>0) result.append("\n");
					result.append(rows.get(key));
				}
			}

		}
		
		return result;
	}
}
