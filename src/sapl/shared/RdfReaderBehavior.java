package sapl.shared;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.rdf4j.model.BNode;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.rio.ParseErrorListener;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.helpers.AbstractRDFParser;
import org.eclipse.rdf4j.rio.ntriples.NTriplesParser;
import org.eclipse.rdf4j.rio.rdfjson.RDFJSONParser;
import org.eclipse.rdf4j.rio.rdfxml.RDFXMLParser;
import org.eclipse.rdf4j.rio.turtle.TurtleParser;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Takes the content from either a file of the passed string itself. 
 * The content is an RDF document in one of the following notations: (1) RDF/XML, 
 * (2) Turtle, (3) RDF/JSON, (4) N-Triples. 
 * Adds the content to the facts base. 
 * Note that Turtle and N-Triples can also be loaded using SaplLoaderBehavior or 
 * simply as <filename> s:is s:true, so this RAB is mostly intended for handling RDF/XML.
 * 
 * Parameters:
 * p:input	Name of the file to read (can be local or online) or a text string. Mandatory
 * p:context	ID of Context, where to load the content. Has to be given as a typed
 * literal, e.g. "?contextID"^^xsd:string, to avoid creating a temporary copy of
 * the Context and loading into it. Default s:G
 * p:format	Format of the RDF document: one of "rdfxml", "rdfjson", "turtle",
 * "ntriples". Default "rdfxml"
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (21.06.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 *
 * */

public class RdfReaderBehavior extends ReusableAtomicBehavior {

	private StringBuilder beliefs;
	private String bnNameBase; // for blank nodes
	private HashMap<String, String> namespaces;
	
	@Parameter (name="input") private String input;
	@Parameter (name="context") private String output;
	@Parameter (name="format") private String format;
	
	final static String RDFXML = "rdfxml";
	final static String RDFJSON = "rdfjson";
	final static String TURTLE = "turtle";
	final static String NTRIPLES = "ntriples";

	private class RDFErrorHandler implements ParseErrorListener {
		@Override
		public void error(String msg, long lineNo, long colNo) {
			RdfReaderBehavior.this.print("Error in parsing RDF: " + msg);
		}

		@Override
		public void fatalError(String msg, long lineNo, long colNo) {
			RdfReaderBehavior.this.print("Fatal error in parsing RDF: " + msg);
		}

		@Override
		public void warning(String msg, long lineNo, long colNo) {
			RdfReaderBehavior.this.print("Warning in parsing RDF: " + msg);
		}
	}

	private class RDFStatementHandler implements RDFHandler {

		@Override
		public void handleNamespace(String prefix, String uri) {
			RdfReaderBehavior.this.namespaces.put(prefix + ":", uri);
		}

		@Override
		public void handleStatement(Statement statement) {
			org.eclipse.rdf4j.model.Resource subject = statement.getSubject();
			IRI predicate = statement.getPredicate();
			Value object = statement.getObject();

			String subj = null;
			if (subject instanceof IRI) {
				subj = "<" + ((IRI) subject).toString() + ">";
			} else if (subject instanceof BNode) {
				subj = RdfReaderBehavior.this.bnNameBase + ((BNode) subject).getID();
			}

			String pred = "<" + predicate.toString() + ">";

			String obj = null;
			if (object instanceof IRI) {
				obj = "<" + ((IRI) object).toString() + ">";
			} else if (object instanceof BNode) {
				obj = RdfReaderBehavior.this.bnNameBase + ((BNode) object).getID();
			} else if (object instanceof Literal) {
				obj = "\"" + RABUtils.escapeString(((Literal) object).getLabel()) + "\"";
			}
			final StringBuilder beliefsString = RdfReaderBehavior.this.beliefs;
			if (beliefsString.length() > 0) {
				beliefsString.append(". ");
			}
			beliefsString.append(subj);
			beliefsString.append(' ');
			beliefsString.append(pred);
			beliefsString.append(' ');
			beliefsString.append(obj);
		}

		@Override
		public void handleComment(String comment) {
		}

		@Override
		public void startRDF() {
		}

		@Override
		public void endRDF() {
		}
	}

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.output = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "context"), SaplConstants.G);
		this.format = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "format"), RDFXML);
	}

	@Override
	public void doAction() {
		
		InputStream is;
		try{
			is = HttpTools.getContentInputStream(this.input, HttpOptions.defaultOptions());
		}
		catch(Exception e){
			this.printException(e, true);
			this.setFailed();
			return;
		}		

		this.beliefs = new StringBuilder();

		this.bnNameBase = SaplConstants.BLANK_NODE_PREFIX + String.valueOf(System.currentTimeMillis()) + "_";

		this.namespaces = new HashMap<String, String>();

		RDFStatementHandler myStatementHandler = new RDFStatementHandler();
		RDFErrorHandler myParseErrorListener = new RDFErrorHandler();

		AbstractRDFParser parser; 
		if(this.format.equals(TURTLE))
			parser = new TurtleParser();
		else if(this.format.equals(RDFJSON))
			parser = new RDFJSONParser();
		else if(this.format.equals(NTRIPLES))
			parser = new NTriplesParser();
		else //RDFXML
			parser = new RDFXMLParser();
		
		parser.setRDFHandler(myStatementHandler);
		parser.setParseErrorListener(myParseErrorListener);
		parser.setVerifyData(true);
		parser.setStopAtFirstError(false);

		try {
			InputStreamReader iread = new InputStreamReader(is);
			BufferedReader ff = new BufferedReader(iread);
			parser.parse(ff, "");
			ff.close();
		} catch (Exception ex) {
			this.print(this.getClass().getName() + " error: Operation failed: " + ex.getClass().getName() + " " + ex.getMessage());
			this.setFailed();
			return;
		}

		StringBuilder resultSapl = new StringBuilder();
		for (Entry<String, String> entry : this.namespaces.entrySet()) {
			String prefix = entry.getKey();
			String ns = entry.getValue();
			resultSapl.append("@prefix ");
			resultSapl.append(prefix);
			resultSapl.append(' ');
			resultSapl.append('<');
			resultSapl.append(ns);
			resultSapl.append(">. ");
		}

		resultSapl.append(this.beliefs);
		
		boolean result = this.addBeliefsN3(resultSapl.toString(), this.output, true);
		if(!result) this.setFailed();
		this.wakeReasoner();
	}
}
