package sapl.shared;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.URITools;

/**
 * Produces an S-APL document from the content of a Context, and writes it
 * either into a file or as a part of a new fact created in the facts base.
 * It can be used to backup into a file the complete content of a facts base.
 * 
 * Parameters:
 * p:input	ID of Context, whose content to write into S-APL document. Default s:G
 * p:result	Either the name of the file to save the content to, or a Context
 * providing the structure to be added (has to use the ?result variable that
 * will be substituted with the content).
 * p:indents	If true, line breaks and indentation is used to separate
 * S-APL elements. Otherwise, a single line is produced.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (02.03.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *
 * */

public class SaplWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="root") private String root;
	@Parameter (name="result") private String output;
	@Parameter (name="indents") private boolean indents;
	@Parameter (name="turtle") private boolean turtle;
	@Parameter (name="append") private boolean append;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"), SaplConstants.G);
		this.root = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "root"));
		this.output = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.indents = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "indents"), false);
		this.turtle = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "turtle"), false);
		this.append = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "append"), false);
	}
	

	@Override
	protected void doAction() {
		SaplN3ProducerOptions options = new SaplN3ProducerOptions(this.indents, true, this.turtle);

		String result;
		if(root==null)
			result = this.produceN3(this.input, options);
		else result = this.produceN3(this.input, root, options);

		boolean ok = RABUtils.putResult(this.myEngine, result+" .\n", this.output, append);
		if (!ok) {
			this.setFailed();
		}
	}

}
