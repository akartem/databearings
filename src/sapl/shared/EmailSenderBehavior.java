package sapl.shared;

import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Sends an email. The sending process is done in a separate thread, so does not block
 * the Engine. If the server responds with error 451 (local error in processing) or 452
 * (insufficient system storage), and parameter p:waitOnFail is set, waits the specified
 * time and tries again. If email is sent, ends in success. Ends in failure if an exception
 * other than error 451 or 452 occurs, or on any exception if p:waitOnFail is not set.
 * 
 * Parameters:
 * p:smtp	SMTP server to use for sending email. Mandatory
 * p:port	The port used by the SMTP server. Default 25
 * p:username	Username for the SMTP server, if required. Optional 
 * p:password	Password for the SMTP server, if required. Optional 
 * p:to	Addresses to which the email to be sent, divided by commas. Mandatory
 * p:from	Address to put as the outgoing address. Some SMTP servers like Goolge's one
 * ignore this and always use the username. Default "no@addr.ess"
 * p:cc	Addresses to which a copy to be sent. Optional
 * p:bcc	Addresses to which a blind copy is to be sent. Optional
 * p:subject	Subject of the email. Default ""
 * p:message	The content of the email. Default ""
 * p:attach	Names of the files to attach to the email, divided by comma. Optional
 * p:contentType	The type of the email content, e.g. "text/plain" or "text/html". Default "text/plain"
 * p:waitOnFail	Time to wait in milliseconds before next attempt of re-sending. Optional
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (05.03.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *
 * */

public class EmailSenderBehavior extends ReusableAtomicBehavior {

	long waitTill = 0;
	Thread sendIt;

	MimeMessage msg;

	boolean tried = false;
	
	@Parameter (name="to") private String receiver;
	@Parameter (name="smtp") private String smtp;
	@Parameter (name="port") private int smtpPort;
	@Parameter (name="username") private String smtpUser;
	@Parameter (name="password") private String smtpPassword;
	@Parameter (name="cc") private String cc;
	@Parameter (name="bcc") private String bcc;
	@Parameter (name="message") private String message;
	@Parameter (name="contentType") private String content;
	@Parameter (name="subject") private String subject;
	@Parameter (name="from") private String sender;
	@Parameter (name="attach") private String attached;
	@Parameter (name="waitOnFail") private int wait;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.executeInThread = false; //creates own thread, so does not use the general mechanism
		
		this.smtp = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "smtp"));
		this.smtpPort = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "port"), 25);
		this.smtpUser = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
		this.smtpPassword = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));
		this.receiver = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "to"));
		this.sender = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "from"), "no@addr.ess");
		this.cc = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "cc"));
		this.bcc = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "bcc"));
		this.subject = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "subject"), "");
		this.message = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "message"), "");
		this.content = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentType"), "text/plain");
		this.attached = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "attach"));
		this.wait = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "waitOnFail"),-1);
		this.finished = false;
	}

	@Override
	public void doAction() {

		if (this.sendIt != null) { // control is in the sending thread
			this.block();
			return;
		}

		if (this.finished) {
			this.wakeReasoner();
			return;
		}

		if (this.waitTill != 0) {
			long now = System.currentTimeMillis();
			if (now < this.waitTill) { // waiting till next attempt of re-sending
				this.block(this.waitTill - now);
				return;
			}
		}

		try {
			Properties props = new Properties();
			props.put("mail.smtp.host", this.smtp);
			props.put("mail.smtp.port", this.smtpPort);

			Session session;
			if (this.smtpUser == null) {
				session = Session.getInstance(props, null);
			} else {
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.socketFactory.port", this.smtpPort);
				props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
				props.put("mail.smtp.socketFactory.fallback", "false");
				session = Session.getInstance(props, new EmailSenderAuthenticator(this.smtpUser, this.smtpPassword));
			}

			this.msg = new MimeMessage(session);
			this.msg.setFrom(new InternetAddress(this.sender));

			InternetAddress[] address = InternetAddress.parse(this.receiver, false);
			this.msg.setRecipients(Message.RecipientType.TO, address);
			if (this.cc != null) {
				address = InternetAddress.parse(this.cc, false);
				this.msg.setRecipients(Message.RecipientType.CC, address);
			}
			if (this.bcc != null) {
				address = InternetAddress.parse(this.bcc, false);
				this.msg.setRecipients(Message.RecipientType.BCC, address);
			}

			this.msg.setSubject(this.subject);

			if (this.attached == null) {
				this.msg.setText(this.message);
			} else {
				MimeBodyPart mbp1 = new MimeBodyPart();
				mbp1.setText(this.message);

				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);

				StringTokenizer st = new StringTokenizer(this.attached, ",");
				while (st.hasMoreTokens()) {
					String f = st.nextToken();
					MimeBodyPart mbpi = new MimeBodyPart();
					String fil = f;
					FileDataSource fds = new FileDataSource(fil);
					mbpi.setDataHandler(new DataHandler(fds));
					mbpi.setFileName(fds.getName());
					mp.addBodyPart(mbpi);
				}

				this.msg.setContent(mp);
			}

			this.msg.setHeader("X-Mailer", "S-APL platform EmailSenderBehavior");
			this.msg.setHeader("Content-Type", this.content);

			this.msg.setSentDate(new Date());

			this.print("[EmailSenderBehavior] Sending email to " + this.receiver + "..");
			Runnable runIt = new SendEmail();
			this.sendIt = new Thread(runIt);
			this.sendIt.start();

			this.block();
			
		} catch (MessagingException ex) {
			this.printError("[EmailSenderBehavior] Sending email to " + receiver + " is failed!");
			this.printException(ex, true);
			this.setFailed();
			this.finished = true;
		}
	}

	private class EmailSenderAuthenticator extends Authenticator {
		private final String smtpUser;
		private final String smtpPassword;

		public EmailSenderAuthenticator(String smtpUser, String smtpPassword) {
			super();
			this.smtpUser = smtpUser;
			this.smtpPassword = smtpPassword;
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(this.smtpUser, this.smtpPassword);
		}
	}

	private class SendEmail implements Runnable {
		@Override
		public void run() {
			try {
				Transport.send(EmailSenderBehavior.this.msg);
				EmailSenderBehavior.this.finished = true;
				EmailSenderBehavior.this.sendIt = null;
				EmailSenderBehavior.this.restart();

				EmailSenderBehavior.this.print("[EmailSenderBehavior] Email to " + EmailSenderBehavior.this.receiver + " is sent");
			} catch (MessagingException ex) {
				
				String error = ex.getMessage();

				if ((EmailSenderBehavior.this.wait < 0) || !(error.startsWith("451") || error.startsWith("452"))) {
					printError("[EmailSenderBehavior] Sending email to " + receiver + " is failed!");
					printException(ex, true);
					EmailSenderBehavior.this.setFailed();
					EmailSenderBehavior.this.finished = true;
				} else {
					long now = System.currentTimeMillis();
					EmailSenderBehavior.this.waitTill = now + EmailSenderBehavior.this.wait;

					EmailSenderBehavior.this.print("[EmailSenderBehavior] Email sending error. Waiting " + EmailSenderBehavior.this.wait / 1000 + " sec");
				}
				EmailSenderBehavior.this.sendIt = null;
				EmailSenderBehavior.this.restart();
			}
		}
	}
}
