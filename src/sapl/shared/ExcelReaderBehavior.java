package sapl.shared;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Reads the first sheet of a Microsoft Excel document (supports both .xls and .xslx). 
 * From the content of rows starting from one after the labels row, generates a fact of the predefined 
 * structure that uses d:table, d:column, d:value properties, and d:<nnnn> IDs.
 * Rows before the labels row, if any, that have exactly two cells set, interprets as additional key-value pairs  
 * and adds those to each row fact as additional virtual columns. 
 * 
 * Parameters:
 * p:input	Name of the file to read (or URL). Mandatory
 * p:uri	URI to use as identifier of the content. Mandatory
 * p:tableDelimiter If any column of a row equals this, the row is considered as a new labels column. If null, the very first row is considered as labels row 
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 5.2 (17.04.2020)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019-2020, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class ExcelReaderBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String filename;
	@Parameter (name="uri") private String id;
	@Parameter (name="tableDelimiter") private String tableDelimiter = null;
	
	public static boolean DEBUG_PRINT = false;
	public static boolean PERFORMANCE_PRINT = true;
	
	final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.US);

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.filename = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.id = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
		this.tableDelimiter = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "tableDelimiter"));
	}
	
	@Override
	public void doAction() {
		
		long start = System.currentTimeMillis();
		
		try {
			
			String sheetName = null;
			String fileName = this.filename;
			if(fileName.contains(".xlsx#") || fileName.contains(".xls#")) {
				int index = fileName.lastIndexOf("#");
				sheetName = fileName.substring(index+1);
				fileName = fileName.substring(0, index);
			}
			
			Workbook workbook;
			
			URL url = null;
			try {
				url = new URL(fileName);
			} catch (MalformedURLException e) {}

			if(url!=null){
				workbook = WorkbookFactory.create(url.openStream());
			}
			else{
				fileName = URLDecoder.decode(fileName, "UTF-8");
				workbook = WorkbookFactory.create(new File(fileName));
			}
			
			Sheet sheet;
			if(sheetName != null){
				sheet = workbook.getSheet(sheetName);
				if(sheet==null) {
					throw new Exception("Sheet "+sheetName+" does not exist in "+this.filename);
				}
			}
			else sheet = workbook.getSheetAt(0);			

			String zeros = "000000000000000000000000";

			int RN = sheet.getLastRowNum();

			int zerosToUseRow = Math.max(4, (int)Math.ceil(Math.log10(RN)));
			int zerosToUseCol = 4;
			
			if(DEBUG_PRINT) this.print("Found "+RN+" rows");
			
			StringBuilder beliefs = new StringBuilder();
			beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
			beliefs.append("<"+this.id+">" + " d:table {");
			
			String labels[] = null;
			
			boolean foundRow = false;
					
			Map<String,String> generalValues = new LinkedHashMap<String,String>();
			
			int processedRows = 0;
			
			for (int iRow = 0; iRow <= RN; iRow++) {
				Row row = sheet.getRow(iRow);
				if(row != null){
					short RC = row.getLastCellNum();
					
					if(this.tableDelimiter==null && iRow==0 || startsNewTable(row)){ // a labels row
						if(DEBUG_PRINT) this.print("Found labels row with "+RC+" columns");

						labels = new String[RC];

						for (int iCol = 0; iCol < RC; iCol++) {
							Cell cell = row.getCell(iCol);
							if(cell!=null){
								labels[iCol] = RABUtils.escapeString(this.getValue(cell));
								if(DEBUG_PRINT) this.print("Label for column "+iCol+" is "+labels[iCol]);
							}
						}

						zerosToUseCol = Math.max(4, (int)Math.ceil(Math.log10(RC)));
						
						continue;
					}
					else { //other rows
						Map<String,String> keyValue = getKeyValuePair(row);
						if(keyValue!=null){ // the row has just 2 cells set
							if(labels==null || 
									//if already have labels, only if redefining something met before labels
									generalValues.containsKey(keyValue.keySet().iterator().next())){
								generalValues.putAll(keyValue);
								continue;
							}
						}
						
						if (labels!=null) { // normal row after a labels row
							boolean foundColumn = false;
							StringBuilder rowBeliefs = new StringBuilder();
							String lastLabel = "";
							
							for (int iCol = 0; iCol < RC; iCol++) {
								Cell cell = row.getCell(iCol);
								String value = this.getValue(cell);
								
								if (value != null && !value.isEmpty()){
									String label = null;
									if (iCol < labels.length && labels[iCol] != null)
										label = labels[iCol];
									else label = lastLabel+"_next";
	
									lastLabel = label;
									
									String cid = String.valueOf(iCol + 1);
									cid = zeros.substring(0, zerosToUseCol - cid.length()) + cid;
									cid = "d:"+cid;
											
									rowBeliefs.append(((foundColumn) ? ". " : "") + cid + " d:column \""+ label +"\"; d:value \"" + RABUtils.escapeString(value) + "\"");
									foundColumn = true;
								}
							}
							if(foundColumn){
								if(generalValues!=null){
									int gCol = 0;
									for(String key : generalValues.keySet()){
										String value = generalValues.get(key);
										String cid = "g"+String.valueOf(++gCol);
										cid = zeros.substring(0, zerosToUseCol - cid.length()) + cid;
										cid = "d:"+cid;
												
										rowBeliefs.append(". "+cid + " d:column \""+ key +"\"; d:value \"" + RABUtils.escapeString(value) + "\"");
									}
								}
								
								String rid = String.valueOf(iRow);
								rid = zeros.substring(0, zerosToUseRow - rid.length()) + rid;
								rid = "d:"+rid;
								beliefs.append(((foundRow) ? ". " : "") + rid + " d:row {");
								foundRow = true;
								
								beliefs.append(rowBeliefs);
								beliefs.append("}");
								
								processedRows++;
							}
						}
						
					}
					
				}

			}			

			beliefs.append("}");
			
			workbook.close();

			this.addBeliefsN3(beliefs.toString());
			this.wakeReasoner();
			
			if(PERFORMANCE_PRINT)
				print("ExcelReaderBehavior processed "+processedRows+" rows ["+(System.currentTimeMillis()-start)+"]");
				
			
		} catch (Exception ex) {
			ex.printStackTrace();
			this.setFailed();
		}

	}
	
	protected boolean startsNewTable (Row row){
		if(this.tableDelimiter==null) return false;
		else {
			short RC = row.getLastCellNum();
			for (int iCol = 0; iCol < RC; iCol++) {
				Cell cell = row.getCell(iCol);
				if(cell!=null){
					if(cell.getStringCellValue().toLowerCase().equals(this.tableDelimiter.toLowerCase()))
						return true;
				}
			}
			return false;
		}
	}
	
	protected Map<String,String> getKeyValuePair (Row row){
		short RC = row.getLastCellNum();
		String key = null, value = null;
		for (int iCol = 0; iCol < RC; iCol++) {
			Cell cell = row.getCell(iCol);
			if(cell!=null){
				if(key==null) key = this.getValue(cell);
				else if(value==null) value = this.getValue(cell);
				else return null;
			}
		}
		if(key==null || value==null) return null;
		else{
			Map<String,String> result = new HashMap<String,String>();
			key = key.trim();
			if(key.endsWith(":")) key = key.substring(0, key.indexOf(":")).trim();
			result.put(key, value);
			return result;
		}
	}

	protected String getValue(Cell cell) {
		if (cell == null) {
			return null;
		}
		CellType type = cell.getCellTypeEnum();

		String value;
		if ((type == CellType.NUMERIC) || (type == CellType.FORMULA)) {

			short format = cell.getCellStyle().getDataFormat();
			String strFormat = BuiltinFormats.getBuiltinFormat(format);
			boolean isDate = format!=0  && strFormat.contains("m") && 
					(strFormat.contains("y") || strFormat.contains("d"));

			if(isDate){
				try{
					Date d = cell.getDateCellValue();
					value = sdf.format(d);
				}
				catch(Exception e){
					try{
						value = cell.getStringCellValue();
					}
					catch(Exception e2){
						return null;
					}
				}
			}
			else {
				try{
					Double v = cell.getNumericCellValue();
					double d = Math.floor(v);
					if (d == v) {
						value = String.valueOf((long) d);
					} else {
						value = v.toString();
					}
				}
				catch(Exception e){
					try{
						value = cell.getStringCellValue();
					}
					catch(Exception e2){
						return null;
					}
				}
			}
		} else {
			value = cell.getStringCellValue();
		}
		return value.trim();
	}	
}
