package sapl.shared;

import java.net.URL;
import java.util.Collection;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Performs an HTTP operation, such as downloads a document from a Web server or sends POST. 
 * Outputs the response either into file or as a part of a new fact created in the facts base. 
 * By default, the download process is done in a separate thread, so does not block the Engine. 
 * If everything is fine, ends in success. If there is a problem with either fetching or saving, 
 * ends in failure. Sends a wakeup signal to the Engine when completed. 
 * 
 * Parameters:
 * p:url	URL of the document. Mandatory
 * p:result	Either the name of the file to save the document to, or a Context providing the structure
 * to be added (has to use the ?result variable that will be substituted with the document content). Mandatory
 * p:method	HTTP method. Default 'GET'
 * p:username	Username for basic HTTP authentication. Optional
 * p:password	Password for basic HTTP authentication. Optional
 * p:accept	Value for HTTP "Accept" header. Optional
 * p:content Content to send for POST/PUT methods. Optional 
 * p:timeout Timeout for . Optional 
 * p:encoding Encoding for the content. Optional 
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (Univeristy of Jyväskylä + VTT 2009-2018 + Elisa since July 2018) 
 * 
 * @version 5.2 (08.04.2020)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class HttpBehavior extends ReusableAtomicBehavior {

	@Parameter (name="url") private URL url;
	@Parameter (name="result") private String resultTo;
	@Parameter (name="method") private String method;
	@Parameter (name="contentType") private String contentType;
	@Parameter (name="username") private String username;
	@Parameter (name="password") private String password;
	@Parameter (name="authMethod") private String authMethod;
	@Parameter (name="authUrl") private String authUrl;
	@Parameter (name="accept") private String accept;
	@Parameter (name="timeout") private int timeout;
	@Parameter (name="encoding") private String encoding;
	
	@Parameter (name="content") private String content;
	@Parameter (name="contentFromFile") private String contentFromFile;

	@Parameter (name="customHeader") private Collection<String> customHeaders;

	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {

		//override the default value to 'true'
		this.executeInThread = parameters.getOptionalBooleanParameter
				(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "thread"), true);
		
		this.url = parameters.getObligatoryURLParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "url"));
		
		this.resultTo = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));

		this.method = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "method"), "GET");

		this.content = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "content"), "");
		this.contentFromFile = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentFromFile"));
		
		this.contentType = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentType"));

		this.username = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
		this.password = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));

		this.authMethod = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "authMethod"), HttpTools.AUTH_TYPE_BASIC);
		this.authUrl = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "authUrl"));

		this.accept = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "accept"));
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"));

		this.timeout = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "timeout"), HttpTools.DEFAULT_READ_TIMEOUT);
		
		this.customHeaders = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "customHeader"));
	}

	@Override
	protected void doAction() {
		try {
			//long s = System.currentTimeMillis();
			
			HttpOptions options = HttpOptions.defaultOptions();
			options.method = this.method;
			options.content = this.content;
			options.contentFromFile = this.contentFromFile;
			options.contentType = this.contentType;
			options.username = this.username;
			options.password = this.password;
			options.authMethod = this.authMethod;
			options.authUrl = this.authUrl;
			options.accept = this.accept;
			options.encoding = this.encoding;
			options.timeout = this.timeout;
			
			if(this.customHeaders!=null){
				for(String header : this.customHeaders){
					int index = header.indexOf(":");
					if(index != -1){
						options.customHeaders.put(header.substring(0,index).trim(), header.substring(index+1).trim());
					}
				}
			}
			
			boolean ok = false;
			if(this.resultTo != null && this.resultTo.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				
				String result = HttpTools.fetch(this.url, options);
				
				//System.out.println(System.currentTimeMillis()-s);
				
				ok = RABUtils.putResult(this.myEngine, result, this.resultTo);
			}
			else { 
				HttpTools.fetch(this.url, options, this.resultTo);
				ok = true;
			}
			
			if (!ok) {
				this.setFailed();
			}
			
			this.wakeReasoner();

		} catch (Exception ex) {
			this.printException(ex, true);
			this.printError("Failed to retrieve " + HttpBehavior.this.url+"!");
			this.setFailed();
		}
	}

}
