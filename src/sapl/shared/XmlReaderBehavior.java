package sapl.shared;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Stack;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Takes the content from either a file of the passed string itself. 
 * The content is an XML document.
 * Adds the content to the facts base in the form of a fact
 * of the predefined structure that uses d:tree, d:branch, d:element, d:attribute,
 * d:value properties and d:<nnnn> IDs.
 * 
 * Parameters:
 * p:input	Name of the file to read (can be local or online) or a text string. Mandatory
 * p:uri	URI to use as identifier of the content. Mandatory
 * p:encoding	The name of the file encoding. Default UTF-8
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.5 (19.06.2017)
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov 
 * 
 * */

public class XmlReaderBehavior extends ReusableAtomicBehavior {

	StringBuilder beliefs;
	StringBuilder literal;
	String lastLiteral = "";

	Stack<Integer> stack;

	@Parameter (name="input") private String input;
	@Parameter (name="uri") private String id;
	@Parameter (name="encoding") private String encoding;	

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.id = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"), null);
	}

	@Override
	public void doAction() {

		HttpOptions options = HttpOptions.defaultOptions();
		options.encoding = this.encoding;

		InputStream is;
		try{
			is = HttpTools.getContentInputStream(this.input, options);
		}
		catch(Exception e){
			this.printException(e, true);
			this.setFailed();
			return;
		}
		
		this.beliefs = new StringBuilder();
		beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
		this.beliefs.append("<"+this.id +">"+ " d:tree {");
		this.literal = new StringBuilder();

		this.stack = new Stack<Integer>();
		this.stack.push(0);

		Handler handler = new Handler();

		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(true);
			SAXParser parser = factory.newSAXParser();
			Reader isr = new InputStreamReader(is);
			InputSource isource = new InputSource();
			isource.setCharacterStream(isr);
			parser.parse(is, handler);
			is.close();
		} catch (Exception ex) {
			this.printException(ex, true);
			//this.print(this.getClass().getName() + " error: Operation failed: " + ex.getClass().getName() + " " + ex.getMessage());
			this.setFailed();
			return;
		}

		this.beliefs.append("}");
		this.addBeliefsN3(this.beliefs.toString());
		this.wakeReasoner();
	}

	private class Handler extends DefaultHandler {

		private String getAndInc() {
			Integer iii = XmlReaderBehavior.this.stack.pop();
			iii++;
			XmlReaderBehavior.this.stack.push(iii);
			String zeros = "0000";
			String id = iii.toString();
			id = zeros.substring(0, zeros.length() - id.length()) + id;
			return id;
		}

		private void checkLiteral() {
			if (XmlReaderBehavior.this.literal.length() > 0) {
				String str = XmlReaderBehavior.this.literal.toString().trim();
				if (str.length() > 0) {
					if ((XmlReaderBehavior.this.beliefs.length() > 0)
							&& (XmlReaderBehavior.this.beliefs.charAt(XmlReaderBehavior.this.beliefs.length() - 1) != '{')) {
						XmlReaderBehavior.this.beliefs.append(". ");
					}

					String iii = this.getAndInc();
					//XmlReaderBehavior.this.beliefs.append("d:"+iii + " d:leaf \"" + RABUtils.escapeString(str) + "\"");
					XmlReaderBehavior.this.beliefs.append("d:"+iii + " d:value \"" + RABUtils.escapeString(str) + "\"");
					XmlReaderBehavior.this.lastLiteral = XmlReaderBehavior.this.literal.toString();
				}
				XmlReaderBehavior.this.literal = new StringBuilder();
			}
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {

			this.checkLiteral();
			if (XmlReaderBehavior.this.beliefs.charAt(XmlReaderBehavior.this.beliefs.length() - 1) != '{') {
				XmlReaderBehavior.this.beliefs.append(". ");
			}

			String iii = this.getAndInc();

			if(namespaceURI!="" && !namespaceURI.endsWith("/") && !namespaceURI.endsWith("#"))
				namespaceURI+="#";

			//XmlReaderBehavior.this.beliefs.append("{" + "d:"+iii + " d:tag " + ((namespaceURI.equals("")) ? "" : "<") + namespaceURI + localName
			//		+ ((namespaceURI.equals("")) ? "" : ">") + "} d:branch {");

			XmlReaderBehavior.this.beliefs.append("d:"+iii + " d:element " + ((namespaceURI.equals("")) ? "\"" : "<") + namespaceURI + localName
					+ ((namespaceURI.equals("")) ? "\"" : ">") + "; d:branch {");

			XmlReaderBehavior.this.stack.push(0);

			for (int i = 0; i < atts.getLength(); i++) {
				iii = this.getAndInc();
				XmlReaderBehavior.this.beliefs.append(((i > 0) ? ". " : ""));
				String ns = atts.getURI(i);
				//XmlReaderBehavior.this.beliefs.append("{" + "d:"+iii + " d:attribute " + ((ns.equals("")) ? "" : "<") + ns + atts.getLocalName(i)
				//		+ ((ns.equals("")) ? "" : ">") + "} d:leaf " + atts.getValue(i));
				XmlReaderBehavior.this.beliefs.append("d:"+iii + " d:attribute " + ((ns.equals("")) ? "" : "<") + ns + atts.getLocalName(i)
						+ ((ns.equals("")) ? "" : ">") + "; d:value \"" + RABUtils.escapeString(atts.getValue(i)) + "\"");
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			this.checkLiteral();
			if (XmlReaderBehavior.this.beliefs.charAt(XmlReaderBehavior.this.beliefs.length() - 1) == '{') {
				XmlReaderBehavior.this.beliefs.delete(XmlReaderBehavior.this.beliefs.lastIndexOf("d:branch"), XmlReaderBehavior.this.beliefs
						.length());
				XmlReaderBehavior.this.beliefs.append("d:value \"\"");
			} else {
				XmlReaderBehavior.this.beliefs.append("}");
			}
			XmlReaderBehavior.this.stack.pop();
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			XmlReaderBehavior.this.literal.append(ch, start, length);
		}
	}

}
