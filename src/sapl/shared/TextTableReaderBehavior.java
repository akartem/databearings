package sapl.shared;

import java.util.ArrayList;
import java.util.List;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Takes the content from either a file of the passed string itself. 
 * The content is a delimited table of data items, for example a CSV file
 * (comma separated values). 
 * Adds the content to the facts base in the form of a fact of the predefined
 * structure that uses d:table, d:row, d:value properties, and d:<nnnn> IDs.
 * 
 * Parameters:
 * p:input	Name of the file to read (can be local or online) or a text string.
 * p:uri	URI to use as identifier of the content.
 * p:encoding	The name of the file encoding. Default UTF-8
 * p:rowDelimiter	A regular expression to use as delimiter for data rows. Default '\n'
 * p:columnDelimiter	A regular expression to use as delimiter for data columns. Default ','
 * p:selectColumns	A whitespace-separated list of the column indexes (one-based) to read. 
 * p:hasHeader	Whether the first row is the header with column names. Optional. Default false.  
 * "all" means reading all columns. Default "all"
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.5 (13.03.2017)
 *  
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class TextTableReaderBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="rowDelimiter") private String rSeparator;
	@Parameter (name="columnDelimiter") private String cSeparator;
	@Parameter (name="encoding") private String encoding;
	@Parameter (name="uri") private String id;
	@Parameter (name="selectColumns") private String selectCols;
	@Parameter (name="hasHeader") private boolean hasHeader;
	
	private List<Integer> select;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.id = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));

		this.rSeparator = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "rowDelimiter"), "\n");
		this.cSeparator = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "columnDelimiter"), ",");
		this.encoding = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "encoding"), null);

		this.selectCols = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "selectColumns"), "all");
		
		this.select = null;
		if (!selectCols.equals("all")) {
			this.select = new ArrayList<Integer>();
			String[] sel = selectCols.split("\\s");
			for (String column : sel) {
				int columnNr = 0;
				try {
					columnNr = Integer.parseInt(column) - 1;

				} catch (NumberFormatException e) {
					throw new sapl.core.rab.IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "selectColumns"), selectCols, State.notInRange);
				}
				if (columnNr < 0) {
					throw new sapl.core.rab.IllegalParameterConfigurationException(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "selectColumns"), selectCols, State.notInRange);
				}
				this.select.add(Integer.valueOf(columnNr));
			}
		}

		this.hasHeader = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "hasHeader"), false);
	}

	@Override
	public void doAction() {

		try{
			
			HttpOptions options = HttpOptions.defaultOptions();
			options.encoding = this.encoding;
			
			String content = HttpTools.getContent(this.input, options);
			String[] rows = content.split(this.rSeparator);

			StringBuilder beliefs = new StringBuilder(content.length());
			beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
			beliefs.append("<"+ this.id +">"+ " d:table {");

			String zeros = "000000000000000000000000";
			int zerosToUseRow = Math.max(4, (int)Math.ceil(Math.log10(rows.length)));
			int zerosToUseCol = 4;
			
			boolean foundRow = false;
			for (int iRow = 0; iRow < rows.length; iRow++) {
				if(this.hasHeader && iRow==0) continue;
				
				if(rows[iRow].trim().isEmpty()) continue;
				
				String[] cols = rows[iRow].trim().split(this.cSeparator);
				
				if (iRow == 0) {
					zerosToUseCol = Math.max(4, (int)Math.ceil(Math.log10(cols.length)));
				}

				String rid = String.valueOf(iRow + (hasHeader?0:1));
				rid = zeros.substring(0, zerosToUseRow - rid.length()) + rid;
				rid = "d:"+rid;

				beliefs.append(((foundRow) ? ". " : "") + rid + " d:row {");
				foundRow = true;
				boolean foundColumn = false;
				for (Integer iCol = 0; iCol < cols.length; iCol++) {
					if ((this.select == null) || this.select.contains(iCol)) {
						String cid = String.valueOf(iCol + 1);
						cid = zeros.substring(0, zerosToUseCol - cid.length()) + cid;
						cid = "d:"+cid;
						beliefs.append(((foundColumn) ? ". " : "") + cid + " d:value \"" + RABUtils.escapeString(cols[iCol].trim()) + "\"");
						foundColumn = true;
					}
				}
				beliefs.append("}");
			}
			beliefs.append("}");

			this.addBeliefsN3(beliefs.toString());
			this.wakeReasoner();

		} catch (Exception ex) {
			this.print(this.getClass().getName() + " error: Operation failed");
			this.printException(ex, true);
			this.setFailed();
		}
	}
}
