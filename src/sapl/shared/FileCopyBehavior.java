package sapl.shared;

import java.io.File;
import java.io.FileOutputStream;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.StreamingUtils;
import sapl.util.URITools;

/** 
 * Copies a file.
 * 
 * @author Artem Katasonov (Elisa) 
 * 
 * @version 4.7 (22.11.2018)
 * 
 * @since 4.7
 * 
 * Copyright (C) 2018, Elisa
 *
 * */

public class FileCopyBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="result") private String saveTo;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.saveTo = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
	}

	@Override
	protected void doAction() throws Exception{
		File file = new File(input);
		FileOutputStream os = new FileOutputStream(saveTo);
		StreamingUtils.sendFileToOutputStream(file, os);
	}
}
