package sapl.shared;

import java.util.Set;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.AudioTools;
import sapl.util.URITools;

import java.io.IOException;
import java.lang.InterruptedException;

import marytts.MaryInterface;
import marytts.client.RemoteMaryInterface;
import marytts.util.data.audio.AudioPlayer;
import marytts.exceptions.SynthesisException;

/** 
 * Performs text-to-speech conversion using a Mary TTS server.
 * 
 * Adapted from FP7 USEFIL project code.
 * 
 * Parameters:
 * p:text	Text to say.	Mandatory
 * p:host	URL of Mary TTS server.	Default	�mary.dfki.de�
 * p:port	Port of Mary TTS server. Default 59125
 * p:voice	ID of the voice to use.	Default	�obadiah�
 * p:framed	If true, a leading and a trailing sounds are played framing
 * the voice message. Default false
 * 
 * @author Jarkko Leino (VTT)
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.1 (01.12.2014)
 * 
 * @since 4.1 
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class SpeechBehavior extends ReusableAtomicBehavior{
	
	/** Host name of MaryTTS server */
	@Parameter (name="host") private String host;
	/** Port of MaryTTS server */
	@Parameter (name="port") private int port;
	
	@Parameter (name="voice") private String voice;
	@Parameter (name="text") private String text;
	@Parameter (name="framed") private boolean framed;


	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters)
			throws IllegalParameterConfigurationException {
		
		//override the default value to 'true'
		this.executeInThread = parameters.getOptionalBooleanParameter
				(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "thread"), true);
		
		this.host = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "host"), "mary.dfki.de");
		this.port = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "port"), 59125);
		this.voice = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "voice"), "obadiah");
		this.text = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "text"));
		this.framed = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "framed"), false);
	}

	@Override
	protected void doAction() throws Exception {
		try {
			if(this.framed)
				this.framedSpeak(this.voice, "res/ding.wav", this.text, "res/chimes.wav");
			else this.speak(this.voice, this.text);
		} catch (Exception e) {
			printException(e, true);
		}
	}
	
	/** Synthesizes and plays a voice message framed with two audio sounds
	 * <p>
	 * </p>
	 * @param voice Voice to use
	 * @param startSound Sound to play before speaking. Null skips.
	 * @param text What to speak. Null returns without speaking and playing sounds.
	 * @param endSound Sound to play after speaking. Null skips.
	 * @throws IOException If the MARY server is not available. 
	 * @throws SynthesisException Speech cannot be generated.
	 */
	public void framedSpeak(String voice, String startSound, String text , String endSound)
		throws IOException , SynthesisException {

		if ( text == null || text.length() == 0 )
			return;
		      
		try {
			AudioTools.playSound(startSound);
		} catch (UnsupportedAudioFileException e){ 
			e.printStackTrace();
		}
		catch (LineUnavailableException e1) {
			e1.printStackTrace();
		}

		speak(voice, text);

		try {
			AudioTools.playSound(endSound);
		} catch (UnsupportedAudioFileException e){ 
			e.printStackTrace();
		}
		catch (LineUnavailableException e1) {
			e1.printStackTrace();
		}
	}
	
	/**Synthesizes and plays a voice message
	 * @param voice Voice to use
	 * @param text Message */
	public void speak (String voice, String text) throws IOException, SynthesisException{
		if ( text == null || text.length() == 0 )
			return;
		
		MaryInterface marytts = new RemoteMaryInterface(host, port);
		Set<String> voices = marytts.getAvailableVoices();
		
		if(voice!=null){
			for(String v : voices){
				if(v.contains(voice) && !v.contains("hsmm"))
					marytts.setVoice(v);
			}
		}
		
		//System.out.println("[MaryTTSWrapper.speak()] Voice : "+marytts.getVoice());

		AudioInputStream audio = marytts.generateAudio(text);
		AudioPlayer player = new AudioPlayer(audio);
		player.start();
		try {
			player.join();
		} catch (InterruptedException e) {}
	}


		
}