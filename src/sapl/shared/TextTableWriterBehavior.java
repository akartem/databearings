package sapl.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;

import sapl.core.SemanticStatement;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Produces a CSV document from a given fact of the predefined structure 
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (16.10.2015)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015, VTT
 * 
 * */

public class TextTableWriterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="rowDelimiter") private String rSeparator;
	@Parameter (name="columnDelimiter") private String cSeparator;

	@Parameter (name="result") private String resultTo;

	@Parameter (name="sort") private boolean sort;
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));

		this.rSeparator = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "rowDelimiter"), "\n");
		this.cSeparator = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "columnDelimiter"), ",");

		this.sort = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "sort"), false);
	}

	@Override
	public void doAction() {
		StringBuilder result = recursion(this.input);
		
		boolean ok = RABUtils.putResult(this.myEngine, result.toString(), resultTo);
		if (!ok) {
			this.setFailed();
		}		
	}
	
	private StringBuilder recursion(String contextID){
		
		StringBuilder result = new StringBuilder();
		
		Map<String, StringBuilder> rows = null;
		Map<String, String> values = null;
		
		List<SemanticStatement> vec = getStatements(contextID);
		if(vec!=null){
			Iterator<SemanticStatement> it=vec.iterator();
			while(it.hasNext()){
				SemanticStatement st=it.next();
				if(st.predicate.equals(SaplConstants.DATA_NS+"table"))
					return recursion(st.object);
				else if(st.predicate.equals(SaplConstants.DATA_NS+"row")){
					if(!sort){
						if(result.length()>0) result.append(this.rSeparator);
						result.append(recursion(st.object));
					}
					else{
						if(rows==null) rows = new HashMap<String, StringBuilder>();
						rows.put(st.subject, recursion(st.object));
					}
				}
				else if(st.predicate.equals(SaplConstants.DATA_NS+"value")){
					if(values==null) values = new HashMap<String, String>();
					values.put(st.subject, st.object);
				}
			}
		}
		if(sort && rows!=null){
			List<String> keys = new ArrayList<String>(rows.keySet());
			Collections.sort(keys);
			for(String key: keys){
				if(result.length()>0) result.append(this.rSeparator);
				result.append(rows.get(key));
			}
		}
		if(values!=null){
			List<String> keys = new ArrayList<String>(values.keySet());
			Collections.sort(keys);
			for(String key: keys){
				if(result.length()>0) result.append(this.cSeparator);
				result.append(values.get(key));
			}
			
		}
		return result;
	}
}
