package sapl.shared;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/** Starts an external software application. If the command is successfully executed
 * (it does not necessarily implies that the application is started), ends in success. 
 * If an exception occurs, ends in failure. 
 * On Windows, able to handle also Web-links or other resources for which
 * the system knows which application to start.
 * 
 * Parameters:
 * p:command	Command to execute or resource to open. Mandatory.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.1 (10.09.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *
 * */

public class ExternalAppStarterBehavior extends ReusableAtomicBehavior {

	@Parameter (name="command") private String command;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.command = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "command"));
	}

	@Override
	protected void doAction() throws Exception {
		try {
			Runtime.getRuntime().exec(this.command);
			return;
		} catch (Throwable t) {
		}

		try {
			String cmd [] = {"cmd.exe" , "/C", "start", this.command};
			Runtime.getRuntime().exec(cmd);
		} catch (Throwable t) {
			this.printException(new Exception(t), true);
			this.setFailed();
		}
	}
}
