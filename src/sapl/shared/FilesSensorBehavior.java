package sapl.shared;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Scans a given folder in the file system. Adds to the facts base information about
 * all files and sub-folders, in the form of a fact of the predefined structure that
 * uses d:tree, d:branch, d:folder, d:file properties, and d:<nnnn> IDs.
 * 
 * Parameters:
 * p:folder	Folder to sense.
 * p:uri	URI to use as identifier of the content.
 * p:maxDepth	Maximum depth to go down into subdirectories, default is -1 (infinite) 
 * p:flatOutput	If false, each folder is represented by its own N3 container. If true, all files are in a single one.  
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018 + Elisa since July 2018) 
 * 
 * @version 4.7 (27.06.2019)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov
 * 
 * */

public class FilesSensorBehavior extends ReusableAtomicBehavior {

	@Parameter (name="folder") private String foldername;
	@Parameter (name="uri") private String id;
	@Parameter (name="flatOutput") private boolean flatOutput = true;
	@Parameter (name="maxDepth") private int maxDepth = -1;
	@Parameter (name="includeAttributes") private boolean includeAttrs = false;

	@Parameter (name="pattern") private String pattern;
	
	@Parameter (name="modifiedOrCreatedAfter") private long modifiedAfter = -1;
	@Parameter (name="modifiedOrCreatedBefore") private long modifiedBefore = -1;
	
	private int globalIndex = 0;
	
	Pattern compiledPattern = null;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.foldername = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "folder"));
		this.id = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
		this.flatOutput = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "flatOutput"), flatOutput);
		this.maxDepth = parameters.getOptionalNumericParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "maxDepth"), maxDepth);

		this.includeAttrs = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "includeAttributes"), includeAttrs);
		
		this.pattern = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "pattern"));
		if(this.pattern != null)
			 this.compiledPattern = Pattern.compile(this.pattern);

		String modified = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "modifiedOrCreatedAfter"));
		if(modified != null) {
			this.modifiedAfter = Long.parseLong(modified);
		}

		String modified2 = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "modifiedOrCreatedBefore"));
		if(modified2 != null) {
			this.modifiedBefore = Long.parseLong(modified2);
		}
		
		this.foldername = this.foldername.replace("\\", "/");
		
	}

	private String senseFolder(File folder, int depth) {
		String zeros = "00000000";
		int localIndex = 0;
		
		StringBuilder result = new StringBuilder();
		
		File[] files = folder.listFiles();
		
		if(files!=null) {
			for (int i = 0; i < files.length; i++) {
				
				localIndex++;
				globalIndex++;
				
				File file = files[i];
				URI uri = file.toURI();
				String str_uri = uri.toString();
				String filename = str_uri.substring(str_uri.indexOf(this.foldername));
	
				String tagID = this.flatOutput? String.valueOf(globalIndex) : String.valueOf(localIndex);
				tagID = zeros.substring(0, zeros.length() - tagID.length()) + tagID;
				
				long lastModified = file.lastModified();
		        Path filePath = file.toPath();
	
		        try {
					BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
					long created = attributes.creationTime().toMillis();
					if(created > lastModified)
						lastModified = created;
				} catch (IOException e) {
					printException(e, true);
				}
				
				if (file.isDirectory()) {
					boolean willGoIn = this.maxDepth == -1 || depth < this.maxDepth;
					
					if(willGoIn) {
						String folderContent = this.senseFolder(file, depth+1);
						if(!folderContent.isEmpty() || this.pattern==null && this.modifiedAfter==-1 && this.modifiedBefore==-1) {
							result.append("d:"+tagID+" d:folder \""+filename+"\"");
							if(includeAttrs)
								result.append("; d:lastModified "+lastModified);
							if(!flatOutput) result.append("; d:branch { ");
							else result.append(". ");
							result.append(folderContent);
							if(!flatOutput) result.append("}. ");
						}
					}
					else if((this.modifiedAfter==-1 || file.lastModified() > this.modifiedAfter) && (this.modifiedBefore==-1 || file.lastModified() < this.modifiedBefore)) {
						result.append("d:"+tagID+" d:folder \""+filename+"\"; d:sensed false");
						if(includeAttrs)
							result.append("; d:lastModified "+lastModified+". ");
						else result.append(". ");
					}
				} 
				else {
					boolean skip = false;
					if(this.compiledPattern != null) {
						String justFileName = filename.substring(filename.lastIndexOf("/")+1);
						Matcher m = compiledPattern.matcher(justFileName);
						if(!m.find()) 
							skip = true;
					}
					
					if(!skip) {
						if(this.modifiedAfter != -1)
							if(lastModified <= this.modifiedAfter)
								skip = true;
						if(this.modifiedBefore != -1)
							if(lastModified >= this.modifiedBefore)
								skip = true;
						
						if(!skip){
							result.append("d:"+tagID+" d:file \""+filename+"\"");
							if(!includeAttrs)
								result.append(". ");
							else result.append("; d:lastModified "+lastModified+". ");
						}
					}
				}
			}
		}
		
		return result.toString();
	}


	@Override
	public void doAction() {
		StringBuilder beliefs = new StringBuilder();
		beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");

		String f = this.foldername;
		try {
			f = URLDecoder.decode(f.replace("+", "%2B"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		File file = new File(f);
		if (!file.exists()) {
			this.printError(this.getClass().getName() + " error: The folder " + f + " does not exist");
			this.setFailed();
			return;
		}
		if (!file.isDirectory()) {
			this.printError(this.getClass().getName() + " error: " + f + " is not a directory");
			this.setFailed();
			return;
		}

		URI uri = file.toURI();
		String str_uri = uri.toString();
		String filename = str_uri.substring(str_uri.indexOf(this.foldername));
		beliefs.append("<"+this.id +">"+ " d:folder \""+filename+"\"; d:tree {" + this.senseFolder(file, 0) + "}");

		this.addBeliefsN3(beliefs.toString());
	}
}
