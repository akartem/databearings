package sapl.shared;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/**
 * Submits an SQL query to a relational database and generates from the results
 * a fact of the predefined structure that uses d:table, d:column,
 * d:value properties, and d:<nnnn> IDs.
 * 
 * Parameters:
 * p:driver	Class name of the database driver, 
 * e.g. oracle.jdbc.OracleDriver or org.postgresql.Driver. Mandatory
 * p:database	URL of the database.  Mandatory
 * p:username	Username for the database connection. Optional
 * p: password	Password for the database connection. Optional
 * p:query	SQL query to submit.  Mandatory
 * p:uri URI to use as identifier of the content. Mandatory
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * 
 * @version 4.1 (10.09.2014)
 * 
 * Copyright (C) 2014, VTT
 * Copyright (C) 2007-2009, Artem Katasonov
 * 
 * */

public class SqlReaderBehavior extends ReusableAtomicBehavior {

	public static boolean PERFORMANCE_PRINT = false;

	@Parameter (name="username") private String db_user;
	@Parameter (name="database") private String db_url;
	@Parameter (name="driver") private String db_drv;
	@Parameter (name="password") private String db_password;
	@Parameter (name="query") private String db_query;
	@Parameter (name="uri") private String id;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.db_drv = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "driver"));
		this.db_url = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "database"));
		this.db_user = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
		this.db_password = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));
		this.db_query = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "query"));
		this.id = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
	}

	@Override
	public void doAction() {

		long start = System.currentTimeMillis();
		long s = start;
		ResultSet rset = null;
		Statement stmt = null;
		Connection conn = null;
		try {
			try {
				Class.forName(this.db_drv);
			} catch (ClassNotFoundException cnfe) {
				print("Cannot find the driver class!");
				this.printException(cnfe, true);
				return;
			}

			if ((this.db_user != null) && (this.db_password != null)) {
				conn = DriverManager.getConnection(this.db_url, this.db_user, this.db_password);
			} else {
				conn = DriverManager.getConnection(this.db_url);
			}
			stmt = conn.createStatement();
			if(PERFORMANCE_PRINT)
				this.print("Connected [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();
			rset = stmt.executeQuery(this.db_query);
			if(PERFORMANCE_PRINT)
				this.print("Query executed [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();
			ResultSetMetaData meta = rset.getMetaData();
			int RC = meta.getColumnCount();
			String labels[] = new String[RC];

			for (short iCol = 0; iCol < RC; iCol++) {
				labels[iCol] = meta.getColumnName(iCol + 1);
			}

			String zeros = "000000000000000000000000";
			int zerosToUseRow = 6;
			int zerosToUseCol = Math.max(4, (int)Math.ceil(Math.log10(RC)));

			s = System.currentTimeMillis();
			StringBuilder beliefs = new StringBuilder();
			beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
			beliefs.append("<"+this.id+">" + " d:table {");
			boolean foundRow = false;
			int iRow = 0;
			while (rset.next()) {
				iRow++;
				String rid = String.valueOf(iRow);
				rid = zeros.substring(0, zerosToUseRow - rid.length()) + rid;
				rid = "d:"+rid;
				beliefs.append(((foundRow) ? ". " : "") + rid + " d:row {");
				foundRow = true;
				boolean foundColumn = false;

				for (int iCol = 0; iCol < RC; iCol++) {

					String cid = String.valueOf(iCol + 1);
					cid = zeros.substring(0, zerosToUseCol - cid.length()) + cid;
					cid = "d:"+cid;

					String value = rset.getString(iCol + 1);
					if ((value == null) || value.equals("null") || value.equals("")) {
						value = "N/A";
					}
					beliefs.append(((foundColumn) ? ". " : "") + cid + " d:column \""+ labels[iCol] +"\"; d:value \"" + RABUtils.escapeString(value) + "\"");
					foundColumn = true;
				}
				beliefs.append("}");
			}
			beliefs.append("}");

			if(PERFORMANCE_PRINT)
				this.print("N3 string is ready [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();
			this.addBeliefsN3(beliefs.toString(), false);
			this.wakeReasoner();

			//this.wakeReasoner();
		} catch (Exception ex) {
			this.printException(ex, true);
			this.setFailed();
			return;
		} finally {
			if (rset != null) {
				try {
					rset.close();

				} catch (SQLException e) {
					// not much we can do here.
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					// not much we can do here.
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// not much we can do here.
				}
			}
		}
		if(PERFORMANCE_PRINT){
			this.print("Data added [" + (System.currentTimeMillis() - s) + "]");
			this.print("Total time: [" + (System.currentTimeMillis() - start) + "]");
		}
	}
}
