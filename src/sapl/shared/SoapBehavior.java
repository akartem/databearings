package sapl.shared;

import java.io.StringWriter;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Node;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.SoapTools;
import sapl.util.URITools;

/**
 * Sends a request to the given SOAP Web Service. Outputs the response (as string) either
 * into file or as a part of a new fact created in the facts base. By default,
 * the process is done in a separate thread, so does not block the Engine.
 * If everything is fine, ends in success. If there is a problem with either
 * fetching or saving, ends in failure.
 * 
 * Parameters:
 * p:service	URL of the SOAP service. Mandatory
 * p:soap	SOAP version, e.g. "1.1" or "1.2". Default "1.2"
 * p:action	URI of SOAP action. Mandatory
 * p:request	SOAP request, i.e. the content for <soap:Body> (as a string). Mandatory
 * p:username	Username for SOAP authentication. Optional
 * p:password	Password for SOAP authentication. Optional
 * p:passwordDigest	If true, "PasswordDigest" authentication method is used. 
 * If false, "PasswordText". Default false
 * p:result	Either the name of the file to save the content to, or a Context providing
 * the structure to be added (has to use the ?result variable that will be substituted
 * with the content). Mandatory
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * 
 * @version 4.3 (26.03.2015)
 * 
 * Copyright (C) 2013-2015, VTT
 * Copyright (C) 2007-2009, Artem Katasonov
 *
 * */

public class SoapBehavior extends ReusableAtomicBehavior {

	public static boolean PERFORMANCE_PRINT = false;
	
	@Parameter (name="service") private String soapService;
	@Parameter (name="soap") private String soapVersion;
	@Parameter (name="action") private String soapAction;
	@Parameter (name="request") private String soapRequest;
	@Parameter (name="username") private String soapUsername;
	@Parameter (name="password") private String soapPassword;
	@Parameter (name="passwordDigest") private boolean soapPasswordDigest;
	@Parameter (name="result") private String resultTo;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters)
			throws IllegalParameterConfigurationException {

		//override the default value to 'true'
		this.executeInThread = parameters.getOptionalBooleanParameter
				(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "thread"), true);

		this.soapService = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "service"));
		this.soapVersion = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "soap"), "1.2");
		this.soapAction = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "action"));
		this.soapRequest = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "request"));

		this.soapUsername = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "username"));
		this.soapPassword = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "password"));
		this.soapPasswordDigest = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "passwordDigest"), false);

		this.resultTo = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));

	}

	@Override
	protected void doAction() throws Exception {

		if(PERFORMANCE_PRINT)
			this.print("Sending "+this.soapRequest);
		long s = System.currentTimeMillis();		

		try{
			SOAPMessage request = SoapTools.createSOAP(this.soapVersion, this.soapService, this.soapAction, this.soapRequest, this.soapUsername, this.soapPassword, this.soapPasswordDigest);
	
			if(PERFORMANCE_PRINT)
				this.print("Request created [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();		
	
			SOAPMessage response = SoapTools.sendSOAP(this.soapService, request, SoapTools.DEFAULT_READ_TIMEOUT);
	
			if(PERFORMANCE_PRINT)
				this.print("Response received [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();
	
			Node responseBody = response.getSOAPBody().getChildNodes().item(0); 
	
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			StringWriter stringWriter = new StringWriter();
			StreamResult streamResult = new StreamResult(stringWriter);
			DOMSource domSource = new DOMSource(responseBody);
			transformer.transform(domSource, streamResult); 
	
			//System.out.println(stringWriter.toString());
	
			boolean ok = RABUtils.putResult(this.myEngine, stringWriter.toString(), this.resultTo);
			if (!ok) {
				this.setFailed();
			}
			this.wakeReasoner();
	
			if(PERFORMANCE_PRINT)
				this.print("Done [" + (System.currentTimeMillis() - s) + "]");
			s = System.currentTimeMillis();
		}
		catch(Exception ex){
			this.printException(ex, true);
			this.setFailed();
		}

	}

}
