package sapl.shared.gdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sapl.core.SaplConstants;
import sapl.core.rab.RABUtils;
import sapl.shared.eii.ExcelOntonut;
import sapl.shared.eii.UniversalAdapterBehavior;

/** Experimental:
 * Ontonut representing a Google Sheets document.
 * 
 * @author Artem Katasonov (VTT + Elisa since July 2018) 
 * 
 * @version 4.7 (16.10.2018)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2016-2018, VTT
 * 
 * */

public class GoogleSheetOntonut extends ExcelOntonut {

	@Override
	protected List<Map<String, String>> runSingleQuery(String request, String body, String reqUsername, String reqPassword, int subindex, Map<String, String> localSol) throws Exception {

		if(UniversalAdapterBehavior.INTERACTION_PRINT)
			print("Accessing "+request, subindex);

		long s = System.currentTimeMillis();

    	List<Object> params = new ArrayList<Object>();
    	params.add(request);
    	
    	Object result = GDataUtils.executeScript(GoogleSheetReaderBehavior.SCRIPT, GoogleSheetReaderBehavior.FUNCTION, params);
    	
    	if(result==null) return null;
    	
    	@SuppressWarnings("unchecked")
		List<Object> list = (List<Object>) result;
    	Object first = list.get(0);
    	@SuppressWarnings("unchecked")
		List<Object> firstRow = (List<Object>) first;
    	
    	List<String> labels = new ArrayList<String>();
    	this.lastLabels = new ArrayList<String>();
		
    	for (Object o : firstRow) {
			this.lastLabels.add(o.toString());
			labels.add(RABUtils.escapeString(o.toString().toUpperCase()));
		}
		
		if(this.dataItemsToSelect.isEmpty()) return null;
		
		List<Map<String,String>> sols  = new ArrayList<Map<String,String>>();
		
		for (int iRow=1; iRow < list.size(); iRow++) {
			Object next = list.get(iRow);
    		@SuppressWarnings("unchecked")
			List<Object> row = (List<Object>) next;		
    		
    		Map<String,String> sol = new HashMap<String,String>();
    		for (int iCol = 0; iCol < row.size(); iCol++) {
				if (!labels.get(iCol).isEmpty()) {
					String var = this.dataItemsToSelect.get(labels.get(iCol));
					if(var!=null){
						Object cell = row.get(iCol);
						String value = cell.toString().trim();
						if (!value.isEmpty()){
							sol.put(var.substring(SaplConstants.VARIABLE_PREFIX.length()), value);
						}
					}
				}
    		}
    		if(sol.size()>0) sols.add(sol);
		}
		
		if(UniversalAdapterBehavior.PERFORMANCE_PRINT)
			this.print("Processed "+sols.size()+" results ["+(System.currentTimeMillis()-s)+"]", subindex);

		if(sols.size()>0) return sols;
		else return null;

	}	

}
