package sapl.shared.gdata;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.script.Script;
import com.google.api.services.script.model.ExecutionRequest;
import com.google.api.services.script.model.Operation;

/** 
 * Experimental:
 * Utility for working with Google Sheets documents. 
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (07.02.2016)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2016, VTT
 * 
 * */

public class GDataUtils {
    /** Application name. */
    private static final String APPLICATION_NAME = "DataBearings";
    
    /** Global instance of the JSON factory*/
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    
    /** Global instance of the scopes required by this application*/
    private static final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/drive", "https://www.googleapis.com/auth/spreadsheets");
    
    /** Global instance of the FileDataStoreFactory*/
    private static FileDataStoreFactory DATA_STORE_FACTORY;
    
    /** Global instance of the HTTP transport*/
    private static HttpTransport HTTP_TRANSPORT;
    
    /** Directory to store user credentials for this application*/
    private static final java.io.File DATA_STORE_DIR = new java.io.File(
        System.getProperty("user.home"), ".credentials/databearings");
    
    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    /**
     * Creates an authorized Credential object
     */
    public static Credential authorize() throws IOException {
        // Load client secrets.
        InputStream in = new FileInputStream("client_secret.json"); //GoogleAppsScript.class.getResourceAsStream("/client_secret.json");
        
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(DATA_STORE_FACTORY)
                .setAccessType("offline")
                .build();
        
        Credential credential = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        
        System.out.println("Credentials saved to " + DATA_STORE_DIR.getAbsolutePath());
        
        return credential;
    }	
    
    /**
     * Create a HttpRequestInitializer from the given one, except set
     * the HTTP read timeout to be longer than the default (to allow called scripts time to execute).
     */
    private static HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
        return new HttpRequestInitializer() {
            @Override
            public void initialize(HttpRequest httpRequest) throws IOException {
                requestInitializer.initialize(httpRequest);
                httpRequest.setReadTimeout(380000);
            }
        };
    }    
	
	
    /**
     * Build and return an authorized Script client service.
     */
    public static Script getScriptService() throws IOException {
        Credential credential = authorize();
        
        return new Script.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, setHttpTimeout(credential))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }	
    
    
    /**Execute a function in an Apps Script*/
    public static Object executeScript(String script, String function, List<Object> params) throws IOException{
    	Script service = getScriptService();
    	
    	// Create execution request.
    	ExecutionRequest request = new ExecutionRequest()
    	        .setFunction(function)
    	        .setParameters(params)
    	        .setDevMode(false);  // Optional.
    	
		long s = System.currentTimeMillis();

		Operation op = service.scripts().run(script, request).execute();
	    
	    System.out.println("Response received ["+(System.currentTimeMillis()-s)+"]");
	    
	    if (op.getError() != null) {
	        // The API executed, but the script returned an error.
	        Map<String, Object> detail = op.getError().getDetails().get(0);
	        System.out.println("Script error! Message: " + detail.get("errorMessage"));
	        return null;
	    } else {
	        return op.getResponse().get("result");
	    }	    
    	
    }   
	
}
