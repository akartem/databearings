package sapl.shared.gdata;

import java.util.ArrayList;
import java.util.List;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/** 
 * Experimental:
 * Reads the first sheet a Google Sheets document. 
 * Considers only columns that have something in the first row and considers that as the column label. 
 * From the content of rows starting from the second, generates a fact of the predefined 
 * structure that uses d:table, d:column, d:value properties, and d:<nnnn> IDs.
 * 
 * Parameters:
 * p:input	Name of the file to read (or URL). Mandatory
 * p:uri	URI to use as identifier of the content. Mandatory
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (07.02.2016)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2016, VTT
 * 
 * */

public class GoogleSheetReaderBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="uri") private String id;
	
	public static boolean DEBUG_PRINT = false;
	
	public static String SCRIPT = "M9IyJaP6qsjXxN-dIBr_4tNDOY46kZgql";
	
	public static String FUNCTION = "getSheetData";
	
	//String sheetId = "1uQ7b8_3KlWk_kZylvxwidOXleLH62EjCTiHMAAndVD4";
	
	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws sapl.core.rab.IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		this.id = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "uri"));
	}
	
	@Override
	public void doAction() {
		
		try {
			
	    	List<Object> params = new ArrayList<Object>();
	    	params.add(this.input);
	    	
	    	Object result = GDataUtils.executeScript(SCRIPT, FUNCTION, params);
	    	
	    	if(result==null){
	    		this.setFailed();
	    		return;
	    	}
			
	    	if(result instanceof List){
	    		@SuppressWarnings("unchecked")
				List<Object> list = (List<Object>) result;
	    		
	    		Object first = list.get(0);
	    		
	    		if(first instanceof List){
	    			@SuppressWarnings("unchecked")
					List<Object> firstRow = (List<Object>) first;
	    			
		    		List<String> labels = new ArrayList<String>(firstRow.size());
		    		
					for (Object o : firstRow) {
						labels.add(RABUtils.escapeString(o.toString()));
					}	    
					if(DEBUG_PRINT) System.out.println("Labels for columns are "+labels);
					
					String zeros = "000000000000000000000000";
					int zerosToUseRow = Math.max(4, (int)Math.ceil(Math.log10(list.size())));
					int zerosToUseCol = Math.max(4, (int)Math.ceil(Math.log10(labels.size())));
					
					StringBuilder beliefs = new StringBuilder();
					beliefs.append("@prefix d: <"+SaplConstants.DATA_NS+">. ");
					beliefs.append("<"+this.id+">" + " d:table {");
					
					boolean foundRow = false;
					for (int iRow=1; iRow < list.size(); iRow++) {
						Object next = list.get(iRow);
						if(next instanceof List){
			    			@SuppressWarnings("unchecked")
							List<Object> row = (List<Object>) next;
			    			
							boolean foundColumn = false;
							StringBuilder rowBeliefs = new StringBuilder();
							for (int iCol = 0; iCol < row.size(); iCol++) {
								if (!labels.get(iCol).isEmpty()) {
									Object cell = row.get(iCol);
									String value = cell.toString().trim();
									
									if (!value.isEmpty()){
										String cid = String.valueOf(iCol + 1);
										cid = zeros.substring(0, zerosToUseCol - cid.length()) + cid;
										cid = "d:"+cid;
										
										rowBeliefs.append(((foundColumn) ? ". " : "") + cid + " d:column \""+ labels.get(iCol) +"\"; d:value \"" + RABUtils.escapeString(value) + "\"");
										foundColumn = true;
									}
								}
							}
							
							if(foundColumn){
								String rid = String.valueOf(iRow);
								rid = zeros.substring(0, zerosToUseRow - rid.length()) + rid;
								rid = "d:"+rid;
								beliefs.append(((foundRow) ? ". " : "") + rid + " d:row {");
								foundRow = true;

								beliefs.append(rowBeliefs);
								beliefs.append("}");
							}
						
						}
					}
					
					beliefs.append("}");

					this.addBeliefsN3(beliefs.toString());
					this.wakeReasoner();						
					
	    		}
			
	    	}
	    	
		} catch (Exception ex) {
			ex.printStackTrace();
			this.setFailed();
		}

	}
	
}
