package sapl.shared.jade;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.jade.ReusableAtomicBehaviorUbiwareWrapper;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;
import sapl.util.UniqueInstanceGenerator;


/**
 * Receives an ACL message (when running within JADE framework)
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.1 (05.04.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *  
 * */

public class MessageReceiverBehavior extends ReusableAtomicBehavior {

	private List<String> matchConversationID = null;
	private List<String> matchPerformative = null;
	private List<String> matchOntology = null;
	private List<String> matchSender = null;
	private List<String> matchContent = null;

	private boolean contentIsSAPL;

	private String print = null;
	private int print_length = -1;

	private String addBeliefs;
	
	private String contentToFile = null;
	private boolean waitOnlyFirst;
	
	private String maxWait = null;

	private MessageTemplate mt;

	private long waitTill = 0;

	private final static URI messageNamespace = URITools.createUncheckedURI("http://sapl.jyu.fi/messages/received/");

	private final static UniqueInstanceGenerator messageInstanceGenerator = new UniqueInstanceGenerator("",
			MessageReceiverBehavior.messageNamespace);

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		Collection<String> collection = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "matchConversationID"));
		if(collection!=null) this.matchConversationID = new ArrayList<String>(collection);
		collection = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "matchPerformative"));
		if(collection!=null) this.matchPerformative = new ArrayList<String>(collection);
		collection = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "matchOntology"));
		if(collection!=null) this.matchOntology = new ArrayList<String>(collection);
		collection = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "matchSender"));
		if(collection!=null) this.matchSender = new ArrayList<String>(collection);
		collection = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "matchContent")); 
		if(collection!=null) this.matchContent = new ArrayList<String>(collection);

		this.mt = MessageTemplate.MatchAll();
		
		if (this.matchContent!=null && this.matchContent.size() != 0) {
			MessageTemplate mt2 = MessageTemplate.MatchContent(this.matchContent.get(0));
			for (int i = 1; i < this.matchContent.size(); i++) {
				mt2 = MessageTemplate.or(mt2, MessageTemplate.MatchContent(this.matchContent.get(i)));
			}
			this.mt = MessageTemplate.and(this.mt, mt2);
		}

		if (this.matchSender!=null && this.matchSender.size() != 0) {
			MessageTemplate mt2 = MessageTemplate.MatchSender(new AID(this.matchSender.get(0), AID.ISLOCALNAME));
			for (int i = 1; i < this.matchSender.size(); i++) {
				mt2 = MessageTemplate.or(mt2, MessageTemplate.MatchSender(new AID(this.matchSender.get(i), AID.ISLOCALNAME)));
			}
			this.mt = MessageTemplate.and(this.mt, mt2);
		}
		if (this.matchConversationID!=null && this.matchConversationID.size() != 0) {
			MessageTemplate mt2 = MessageTemplate.MatchConversationId(this.matchConversationID.get(0));
			for (int i = 1; i < this.matchConversationID.size(); i++) {
				mt2 = MessageTemplate.or(mt2, MessageTemplate.MatchConversationId(this.matchConversationID.get(i)));
			}
			this.mt = MessageTemplate.and(this.mt, mt2);
		}
		if (this.matchOntology!=null && this.matchOntology.size() != 0) {
			MessageTemplate mt2 = MessageTemplate.MatchOntology(this.matchOntology.get(0));
			for (int i = 1; i < this.matchOntology.size(); i++) {
				mt2 = MessageTemplate.or(mt2, MessageTemplate.MatchOntology(this.matchOntology.get(i)));
			}
			this.mt = MessageTemplate.and(this.mt, mt2);
		}

		if (this.matchPerformative!=null && this.matchPerformative.size() != 0) {
			MessageTemplate mt2 = null;
			for (int i = 0; i < this.matchPerformative.size(); i++) {
				String mperf = this.matchPerformative.get(i);
				int p = -1;
				p = ACLMessage.getInteger(mperf.toUpperCase());
				if (mt2 == null) {
					mt2 = MessageTemplate.MatchPerformative(p);
				} else {
					mt2 = MessageTemplate.or(mt2, MessageTemplate.MatchPerformative(p));
				}
			}
			this.mt = MessageTemplate.and(this.mt, mt2);
		}

		//policy messages should not come here!
		//MessageTemplate policyMessageTemplate = MessageTemplate.MatchOntology("http://sapl.jyu.fi/2010/08/06/policies/response");
		//this.mt = MessageTemplate.and(MessageTemplate.not(policyMessageTemplate), this.mt);

		this.print = parameters.getParameterValue(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "print"));
		if (this.print == null) {
			this.print = "200";
		}
		if ((this.print != "true") && (this.print != "false")) {
			try {
				this.print_length = Integer.valueOf(this.print);
			} catch (NumberFormatException e) {
				this.print_length = 200;
			}
		}

		this.contentIsSAPL = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentIsSAPL"), false);

		this.contentToFile = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentToFile"));

		this.addBeliefs = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "addBeliefs"), "true");

		this.waitOnlyFirst = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "waitOnlyFirst"), true);

		this.maxWait = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "maxWait"));
		if (this.maxWait != null) {
			long toWait = new Long(this.maxWait).longValue();
			long now = System.currentTimeMillis();
			this.waitTill = now + toWait;
		}

		this.finished = false; //it is a cyclic behavior		
	}

	@Override
	public void doAction() {

		if(!(this.wrapper instanceof ReusableAtomicBehaviorUbiwareWrapper)){
			printError("Receiving ACL messages is only supported in JADE");
			this.setFinished();
			return;
		}
			
		ACLMessage msg = ((ReusableAtomicBehaviorUbiwareWrapper)this.wrapper).receiveAclMessage(this.mt);
		
		if (msg != null) {
			String content = msg.getContent();
			String received_ontology = msg.getOntology();

			if (!this.print.equals("false")) {
				String printContent;
				if (received_ontology.startsWith(".")) {
					int l = content.length();
					if (received_ontology.equals(".class") || received_ontology.equals(".zip")) {
						l = l / 2;
					}
					printContent = received_ontology + " (" + l + " bytes)";
				} else {
					printContent = '\"' + content + '\"';
				}
				if (!this.print.equals("true") && (printContent.length() > this.print_length)) {
					printContent = printContent.substring(0, this.print_length) + "...(" + content.length() + " chars)";
				}
				this.print(printContent + " from '" + msg.getSender().getLocalName() + "' received");
			}

			if (this.contentToFile != null) {
				try {
					File datafile = new File(this.contentToFile);
					String name = datafile.getName();
					int ind = name.indexOf('.');
					if (ind == -1) {
						datafile = new File(this.contentToFile + received_ontology);
					}

					File p = datafile.getParentFile();
					if ((p != null) && !p.exists()) {
						boolean directoryMakingSuccessFul = p.mkdirs();
						if (!directoryMakingSuccessFul) {
							printError("Failed creating needed directories : " + p.getCanonicalPath());
							return;
						}
					}

					if (!(received_ontology.equals(".class") || received_ontology.equals(".zip"))) {
						FileWriter fwrite = null;
						BufferedWriter ff = null;
						try {
							fwrite = new FileWriter(datafile, false);
							ff = new BufferedWriter(fwrite);
							ff.write(content, 0, content.length());
							ff.close();
						} finally {
							if (ff != null) {
								ff.close();
							} else {
								//try to close FileWriter
								if (fwrite != null) {
									fwrite.close();
								}
							}
						}

					} else {
						FileOutputStream fos = null;
						BufferedOutputStream ff = null;
						try {
							fos = new FileOutputStream(datafile, false);
							ff = new BufferedOutputStream(fos);
							int bufferLength = content.length() / 2;
							byte[] buffer = new byte[bufferLength];
							for (int i = 0; i < bufferLength; i++) {
								int digit = Integer.parseInt(content.substring(i * 2, i * 2 + 2), 16);
								buffer[i] = (byte) digit;
							}
							ff.write(buffer);
						} finally {
							if (ff != null) {
								ff.close();
							} else {
								//try to at least close the underlying FileOutputStream
								if (fos != null) {
									fos.close();
								}
							}

						}

					}

				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
			}

			String messageId = MessageReceiverBehavior.messageInstanceGenerator.getNewID();
			int p = msg.getPerformative();
			String perf = "other";
			if (p == ACLMessage.REQUEST) {
				perf = "request";
			} else if (p == ACLMessage.INFORM) {
				perf = "inform";
			} else if (p == ACLMessage.QUERY_IF) {
				perf = "query-if";
			}
			
			if (this.addBeliefs.equals("true")) {
				StringBuilder messageReceivedBelief = new StringBuilder();
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(messageId);
				messageReceivedBelief.append('>');
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.SAPL_NS);
				messageReceivedBelief.append("received> {");
				//begin conversationID
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.PARAM_NS);
				messageReceivedBelief.append("conversationID> ");
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.is);
				messageReceivedBelief.append('>');
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(msg.getConversationId());
				messageReceivedBelief.append('>');
				//end conversationID
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('.');
				messageReceivedBelief.append(' ');
				//begin performative
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.PARAM_NS);
				messageReceivedBelief.append("performative> ");
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.is);
				messageReceivedBelief.append('>');
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('\"');
				messageReceivedBelief.append(perf);
				messageReceivedBelief.append('\"');
				//end performative
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('.');
				messageReceivedBelief.append(' ');
				//begin sender
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.PARAM_NS);
				messageReceivedBelief.append("sender> ");
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.is);
				messageReceivedBelief.append('>');
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('\"');
				messageReceivedBelief.append(msg.getSender().getLocalName());
				messageReceivedBelief.append('\"');
				//end sender
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('.');
				messageReceivedBelief.append(' ');
				//begin ontology
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.PARAM_NS);
				messageReceivedBelief.append("ontology> ");
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.is);
				messageReceivedBelief.append('>');
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('\"');
				messageReceivedBelief.append(msg.getOntology());
				messageReceivedBelief.append('\"');
				//end ontology
				messageReceivedBelief.append(' ');
				messageReceivedBelief.append('.');
				messageReceivedBelief.append(' ');
				//begin content
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.PARAM_NS);
				messageReceivedBelief.append("content> ");
				messageReceivedBelief.append('<');
				messageReceivedBelief.append(SaplConstants.is);
				messageReceivedBelief.append('>');

				if (!this.contentIsSAPL) {
					messageReceivedBelief.append(' ');
					messageReceivedBelief.append('\"');
					messageReceivedBelief.append(RABUtils.escapeString(content));
					messageReceivedBelief.append('\"');
				} else {
					messageReceivedBelief.append(' ');
					messageReceivedBelief.append('{');
					messageReceivedBelief.append(content);
					messageReceivedBelief.append('}');
				}
				messageReceivedBelief.append('}');
				this.addBeliefsN3(messageReceivedBelief.toString());
				
			} else if (this.addBeliefs.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				HashMap<String, String> vars = new HashMap<String, String>();
				vars.put("messageID", messageId);
				vars.put("conversationID", msg.getConversationId());
				vars.put("performative", perf);
				vars.put("sender", msg.getSender().getLocalName());
				vars.put("ontology", msg.getOntology());

				
				String temp_context = null;
				if (!this.contentIsSAPL) {
					vars.put("content", content);
				} else {
					temp_context = this.addBeliefsN3ToTempContext(content);
					if (temp_context != null) {
						vars.put("content", temp_context);
					} else {
						return;
					}
				}

				this.addBeliefs(this.addBeliefs, vars);
				
			}

			if (this.waitOnlyFirst) {
				this.setFinished();
			}

		} else {
			if (this.maxWait != null) {
				long now = System.currentTimeMillis();
				if (now >= this.waitTill) {
					this.setFailed();
					this.finished = true;
				} else {
					this.block(this.waitTill - now);
				}
			} else {
				this.block();
			}
		}
	}
}
