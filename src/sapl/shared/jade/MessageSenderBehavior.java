package sapl.shared.jade;

import jade.core.AID;
import jade.lang.acl.ACLMessage;

import java.net.URI;
import java.util.HashMap;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.jade.ReusableAtomicBehaviorUbiwareWrapper;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.core.rab.IllegalParameterConfigurationException.State;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.URITools;
import sapl.util.UniqueInstanceGenerator;

/**
 * Sends an ACL message (when running within JADE framework)
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.5 (11.04.2017)
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *  
 * */

public class MessageSenderBehavior extends ReusableAtomicBehavior {

	private final static URI messageNamespace = URITools.createUncheckedURI("http://sapl.jyu.fi/messages/sent/");
	final static UniqueInstanceGenerator messageInstanceGenerator = new UniqueInstanceGenerator("", MessageSenderBehavior.messageNamespace);

	
	private String receiver;
	private String conversationID;
	private String messageId;

	private String performativeString;
	private int performative;
	
	private String ontology;

	private String messageContent;
	private boolean contentIsSapl;
	private String contentSpecifier;
	
	private boolean doPrint;

	private boolean doAddBeliefs;
	private boolean addDefaultBeliefs;
	private String nonDefaultBeliefContainerID;

	
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.receiver = parameters.getObligatoryStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "receiver"));
		boolean cleanContent = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "cleanContent"), false);
		this.doPrint = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "print"), true);

		String addBeliefs = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "addBeliefs"));
		if (addBeliefs == null) {
			this.doAddBeliefs = true;
			this.addDefaultBeliefs = true;
		} else {
			if (addBeliefs.equals("true")) {
				this.doAddBeliefs = true;
				this.addDefaultBeliefs = true;
			} else if (addBeliefs.equals("false")) {
				this.doAddBeliefs = false;
				this.addDefaultBeliefs = false;
			} else if (addBeliefs.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				this.doAddBeliefs = true;
				this.addDefaultBeliefs = false;
				this.nonDefaultBeliefContainerID = addBeliefs;
			}
		}

		Boolean issapl = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "contentIsSAPL"));
		
		Resource content = Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "content");		
		if (issapl != null) {
			if (issapl) {
				//check needed
				this.contentSpecifier = parameters.getObligatoryContainerID(content);
			} else {
				//user takes responsibility that this is a literal
				this.contentSpecifier = parameters.getObligatoryStringParameter(content);
			}
		} else {
			//automatic detection needed
			this.contentSpecifier = parameters.getObligatoryStringParameter(content);
			if (this.contentSpecifier.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				issapl = Boolean.TRUE;
			} else {
				issapl = Boolean.FALSE;
			}
		}
		//at this stage, contentSpecifier indicates the literal if issapl is false or the containerID if issapl is true
		this.contentIsSapl = issapl;
		if (!issapl) {
			//FIXME
			this.messageContent = this.contentSpecifier;
		} else {
			//generate S-APL from the Context ID
			SaplN3ProducerOptions options = new SaplN3ProducerOptions(false, true, true, true, true, cleanContent);
			this.messageContent = this.produceN3(this.contentSpecifier, options); //parameter content must be a context ID
			if (this.messageContent.equals("")) {
				throw new IllegalParameterConfigurationException(content, this.contentSpecifier, State.notInRange,
						"The generated S-APL code is the empty string.");
			}
		}
		this.messageId = MessageSenderBehavior.messageInstanceGenerator.getNewID();
		//if no conversationID is specified by the agent, we use the ID from the message instead.
		this.conversationID = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "conversationID"), this.messageId);
		
		this.performativeString = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "performative"), "request");
		this.performative = MessageSenderBehavior.getPerformative(this.performativeString);
		
		this.ontology = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "ontology"), "unspecified");
	}

	@Override
	public void doAction() {

		if(!this.sendMessage()){
			this.setFailed();
			return;
		}

		if (this.doPrint) {
			this.print("\"" + this.messageContent + "\"" + " to '" + this.receiver + "' is sent.");
		}

		if (this.doAddBeliefs) {
			if (this.addDefaultBeliefs) {
				this.addDefaultBeliefs();
			} else {
				this.addNonDefaultBeliefs();
			}
		}
	}

	/**
	 * 
	 */
	private void addNonDefaultBeliefs() {
		HashMap<String, String> vars = new HashMap<String, String>();
		vars.put("messageID", this.messageId);
		vars.put("conversationID", this.conversationID);
		vars.put("performative", this.performativeString);
		vars.put("receiver", this.receiver.toString());
		vars.put("ontology", this.ontology);

		if (!this.contentIsSapl) {
			vars.put("content", this.messageContent);
		} else {
			vars.put("content", this.contentSpecifier);
		}

		this.addBeliefs(this.nonDefaultBeliefContainerID, vars);
	}

	/**
	 * 
	 */
	private void addDefaultBeliefs() {
		StringBuilder messageReceivedBelief = new StringBuilder();
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(this.messageId);
		messageReceivedBelief.append('>');
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.SAPL_NS);
		messageReceivedBelief.append("sent> {");
		//begin conversationID
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.PARAM_NS);
		messageReceivedBelief.append("conversationID> ");
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.is);
		messageReceivedBelief.append('>');
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(this.conversationID);
		messageReceivedBelief.append('>');
		//end conversationID
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('.');
		messageReceivedBelief.append(' ');
		//begin performative
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.PARAM_NS);
		messageReceivedBelief.append("performative> ");
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.is);
		messageReceivedBelief.append('>');
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('\"');
		messageReceivedBelief.append(ACLMessage.getPerformative(this.performative).toLowerCase());
		messageReceivedBelief.append('\"');
		//end performative
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('.');
		messageReceivedBelief.append(' ');
		//begin sender
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.PARAM_NS);
		messageReceivedBelief.append("receiver> ");
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.is);
		messageReceivedBelief.append('>');
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('\"');
		messageReceivedBelief.append(this.receiver);
		messageReceivedBelief.append('\"');
		//end sender
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('.');
		messageReceivedBelief.append(' ');
		//begin ontology
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.PARAM_NS);
		messageReceivedBelief.append("ontology> ");
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.is);
		messageReceivedBelief.append('>');
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('\"');
		messageReceivedBelief.append(this.ontology);
		messageReceivedBelief.append('\"');
		//end ontology
		messageReceivedBelief.append(' ');
		messageReceivedBelief.append('.');
		messageReceivedBelief.append(' ');
		//begin content
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.PARAM_NS);
		messageReceivedBelief.append("content> ");
		messageReceivedBelief.append('<');
		messageReceivedBelief.append(SaplConstants.is);
		messageReceivedBelief.append('>');

		if (!this.contentIsSapl) {
			messageReceivedBelief.append(' ');
			messageReceivedBelief.append('\"');
			messageReceivedBelief.append(RABUtils.escapeString(this.messageContent));
			messageReceivedBelief.append('\"');
		} else {
			messageReceivedBelief.append(' ');
			messageReceivedBelief.append('{');
			messageReceivedBelief.append(this.messageContent);
			messageReceivedBelief.append('}');
		}
		messageReceivedBelief.append('}');

		this.addBeliefsN3(messageReceivedBelief.toString());

	}

	private boolean sendMessage() {
		if(this.wrapper instanceof ReusableAtomicBehaviorUbiwareWrapper){
			ACLMessage message = new ACLMessage(this.performative);
			message.addReceiver(new AID(this.receiver, AID.ISLOCALNAME));
			message.setContent(this.messageContent);
			message.setOntology(this.ontology);
			message.setConversationId(this.conversationID);
			
			//System.out.println(message);
		
			((ReusableAtomicBehaviorUbiwareWrapper)this.wrapper).sendAclMessage(message);
			
			return true;
		}
		else{
			printError("Sending ACL messages is only supported in JADE");
			return false;
		}
	}

	private static int getPerformative(String performative) {
		return ACLMessage.getInteger(performative.toUpperCase());
	}
}
