package sapl.shared;

import java.util.Collection;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplReasoner;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.RABUtils;
import sapl.core.rab.Resource;
import sapl.util.URITools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** 
 * Accesses recorded logs contents 
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.6 (25.06.2018)
 * 
 * @since 4.6
 * 
 * Copyright (C) 2018, VTT
 *
 * */

public class LogAccessBehavior extends ReusableAtomicBehavior {

	@Parameter (name="result") private String output;
	@Parameter (name="skip") private Collection<String> skip;
	@Parameter (name="separate") private String separate;

	final String fNEWLINE = System.getProperty("line.separator");

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters)
			throws IllegalParameterConfigurationException {

		this.output = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "result"));
		this.skip = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "skip"));
		this.separate = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "separate"));
		if(this.separate!=null && this.separate.equals("\n")) this.separate = fNEWLINE;
	}

	@Override
	protected void doAction() throws Throwable {
		
		String result = SaplReasoner.logOut.getAndClearSave();
		if(result.trim().isEmpty())
			result = SaplReasoner.logErr.getAndClearSave();

		if(!result.trim().isEmpty()){
			if(skip!=null){
				Pattern[] patterns = new Pattern[skip.size()];
				int i=0;
				for(String sk : skip){
					patterns[i++] = Pattern.compile(sk);
				}
				
				StringBuilder builder = new StringBuilder();
				String[] lines = result.trim().split("\n");
				for(String line : lines){
					boolean toSkip = false;
					for(Pattern pattern: patterns){
						Matcher matcher = pattern.matcher(line);
						if(matcher.find()){
							toSkip = true;
							break;
						}
					}
					if(!toSkip){
						if(separate!=null && builder.length()>0 && line.startsWith("["))
							builder.append(separate);
						builder.append(line.trim());
						builder.append(fNEWLINE);
					}
				}
				result = builder.toString();
			}
			else if(separate!=null){
				result = result.replace("\n[", "\n"+this.separate+"[");
			}
			
			if(!result.trim().isEmpty()){
				boolean ok = RABUtils.putResult(this.myEngine, result, this.output);
				if (!ok) {
					this.setFailed();
				}
			}
		}
	}

}
