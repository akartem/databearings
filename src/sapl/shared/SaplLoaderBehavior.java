package sapl.shared;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.util.HttpOptions;
import sapl.util.HttpTools;
import sapl.util.URITools;

/**
 * Takes the content from either a file of the passed string itself. 
 * The content is a S-APL document. Adds the content to the facts base. 
 * The default behavior is equivalent to just stating <filename> s:is s:true. 
 * However, provides additional options (1) loading string content, (2) running in own thread for
 * loading Web documents, (3) disabling Context join.
 * 
 * Parameters:
 * p:input	Name of the file to read (can be local or online) or a text string.
 * p:context ID of Context, where to load the content. Has to be given as a typed
 * literal, e.g. "?contextID"^^xsd:string, to avoid creating a temporary copy of 
 * the Context and loading into it. 
 * p:doChecks	If false, Contexts are not automatically joined.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.5 (13.03.2017)
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 *
 * */

public class SaplLoaderBehavior extends ReusableAtomicBehavior {

	@Parameter (name="input") private String input;
	@Parameter (name="context") private List<String> resultTo;
	@Parameter (name="checks") private boolean doChecks;

	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException {
		this.input = parameters.getObligatoryNonEmptyStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		
		Collection<String> given = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "context"));
		if(given==null){
			resultTo = new ArrayList<String>();
			resultTo.add(SaplConstants.G);
		}
		else resultTo = new ArrayList<String>(given);
		
		//this.resultTo = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "context"), SaplConstants.G);
		this.doChecks = parameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "checks"), true);
	}

	@Override
	protected void doAction() {

		String beliefs;
		try {
			beliefs = HttpTools.getContent(input, HttpOptions.defaultOptions());
		} catch (Exception e) {
			this.printException(e, true);
			this.setFailed();
			return;
		}
		
		boolean result = this.addBeliefsN3(beliefs, this.resultTo, this.doChecks);

		if(!result) this.setFailed();
		this.wakeReasoner();
	}

}
