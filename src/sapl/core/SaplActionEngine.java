package sapl.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Resource;
import sapl.core.sapln3.ResourcePrefixer;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.SaplN3Producer;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.util.IDGenerator;

/** 
 * Engine for executing external Java code based on statements
 * {sapl:I sapl:do ..} sapl:configuredAs {...} in a S-APL model
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (26.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplActionEngine {

	/**S-APL model to work with*/
	private SaplModel targetModel;

	/**SaplReasoner owning this action engine*/
	private SaplReasoner myReasoner;

	/**Loader for external action classes*/
	private static final RABLoader loader = new RABLoader(ClassLoader.getSystemClassLoader());

	/**Names of external classes that were not found*/
	private Set<String> nonAvailableBehaviors = new HashSet<String>();

	/**External action that are currently under execution*/
	final Set<ReusableAtomicBehavior> runningRABs = new HashSet<ReusableAtomicBehavior>();

	/**IDs of running RAB instances*/
	private final Map<String, String> rabInstanceIDs = new HashMap<String, String>();

	/**IDs of sapl:executionID statements for running RABs (to be able to clean)*/
	private final Map<String, String> rabExecutionStatementIDs = new HashMap<String, String>();

	private final IDGenerator executionIDGenerator = new IDGenerator("action");

	/** The blackboard of the engine, for RABs to exchange Java objects if needed*/
	private final Blackboard blackboard = new Blackboard();


	/**Constructor
	 * @param model S-APL model to work with
	 * @param owner S-APL reasoner creating this engine*/
	public SaplActionEngine (SaplModel model, SaplReasoner owner){
		this.targetModel = model;
		this.myReasoner = owner;
	} 

	/**Prints an error message*/
	private static void print(String string) {
		System.err.println("["+SaplModel.getCurrentTime()+"] "+string);
	}	

	/**Initializes for execution external Java actions as defined in current state of the S-APL model
	 * @param targetModel S-APL model to work with
	 * @return Set of actions to execute, or null if nothing to execute*/
	List<ReusableAtomicBehavior> getExternalActions(){
		ContextContainer G = this.targetModel.contexts.get(SaplConstants.GENERAL_CONTEXT);
		Set<String> index = G.index_pred.get(SaplConstants.configuredAs);
		
		if (index == null) {
			return null;
		}

		Set<String> actionStatementIDs = new LinkedHashSet<String>(index); 

		List<ReusableAtomicBehavior> result = new ArrayList<ReusableAtomicBehavior>();
		boolean anythingDone = false;

		for (String actionStatementID : actionStatementIDs) {
			SemanticStatement actionStatement = this.targetModel.beliefs.get(actionStatementID);

			if (!actionStatement.isSubjectContext()
					|| (!actionStatement.isObjectContext() && !actionStatement.object.equals(SaplConstants.NULL))) {
				print("Error in \"configuredAs\": The subject must be context container "
						+ "and the object must be either context container or sapl:null '" + actionStatement + "'");
				continue;
			}

			//there could be multiple actions with the same parameter configuration! We do an iteration over all members of the subject
			List<String> actionSubjectMemeberIDs = this.targetModel.contexts.get(actionStatement.subject).members;
			if (actionSubjectMemeberIDs.size() > 1) {
				print("Warning: More than one statements in subject container of an action, result is unpredictable");
			}

			for (String actionSubjectMemberID : actionSubjectMemeberIDs) {
				SemanticStatement actionSubjectMember = this.targetModel.beliefs.get(actionSubjectMemberID);
				String actionName = actionSubjectMember.object;
				//check whether the subject of sapl:configureAs is of the form {sapl:I sapl:do java:actioname/:embeddedAction}
				if (!actionSubjectMember.subject.equals(SaplConstants.I) || !actionSubjectMember.predicate.equals(SaplConstants.DO)
						|| (!actionName.startsWith(SaplConstants.RAB_NS) && !actionName.startsWith(SaplConstants.EMPTY_NS))) {
					//not a RAB
					continue;
				}

				Class<ReusableAtomicBehavior> rabClass;
				try {
					rabClass = SaplActionEngine.loader.loadClass(actionName);
					this.nonAvailableBehaviors.remove(actionName);
				} catch (Throwable e) {
					if (!this.nonAvailableBehaviors.contains(actionName)) {
						print("Behavior " + actionName + " is not found : " + e.getMessage());
						this.nonAvailableBehaviors.add(actionName);
						//add belief about missing RAB to the agent.
						String id = this.targetModel.statementIDGenerator.getNewStatementID();
						this.targetModel.putBelief(id,
								new SemanticStatement(SaplConstants.I, SaplConstants.missingRAB, actionName/*.replace('.', '/')*/));
						this.targetModel.putLink(SaplConstants.GENERAL_CONTEXT, id);

						anythingDone = true;
					}
					continue;
				}

				ReusableAtomicBehavior rab = this.initializeAction(rabClass, actionStatement.object, actionStatementID);
				result.add(rab);
				//after execution (or NotAllowed by policy checker. remove the links to the belief that this action should get executed.
				this.targetModel.removeLink(SaplConstants.GENERAL_CONTEXT, actionStatementID);
				anythingDone = true;
			}
		}

		if(!anythingDone) return null;
		else return result;

	}

	/**Initializes a single external Java action to execute*/
	private ReusableAtomicBehavior initializeAction(Class<ReusableAtomicBehavior> rabClass, String parametersContext, String statementID){
		Set<String> referencedContexts = new HashSet<String>();
		BehaviorStartParameters parameters = this.collectActionParameters(parametersContext, referencedContexts);

		//actual execution
		ReusableAtomicBehavior rab = null;
		try {
			rab = rabClass.newInstance();
		} catch (InstantiationException e) {
			throw new Error("A ReusableAtomicBehavior must not be abstract and must have a non throwing default constructor.", e);
		} catch (IllegalAccessException e) {
			throw new Error("A ReusableAtomicBehavior must have a public default constructor.", e);
		}

		//set parameters
		rab.setStartParameters(parameters);
		rab.setSaplActionEngine(this);

		for (String contextID : referencedContexts) {
			//increase reference counts (i.e. link to RAB that is about to start running) 
			this.targetModel.contexts.get(contextID).referenced++;
		}

		rab.linkedContexts = referencedContexts;

		rab.ID = this.executionIDGenerator.getNewID();
		
		this.runningRABs.add(rab);

		//Add beliefs about execution:
		if(this.targetModel.hasMentalContexts){
			this.targetModel.putLink(SaplConstants.DO_CONTEXT, statementID);

			String cid = this.targetModel.contextIDGenerator.getNewContextID();
			this.targetModel.putContext(cid);
			this.targetModel.putLink(cid, statementID);

			String sid = this.targetModel.statementIDGenerator.getNewStatementID();
			this.targetModel.putBelief(sid, new SemanticStatement(cid, SaplConstants.executionID, rab.ID));
			this.targetModel.putLink(SaplConstants.DO_CONTEXT, sid);
			this.rabInstanceIDs.put(statementID, rab.ID);
			this.rabExecutionStatementIDs.put(statementID, sid);			
		}

		return rab;
	}

	/** This method collects the parameters defined in the parameterContext for RAB execution. Furthermore, it adds to the
	 * referencedContexts set; all ID's of contexts referenced from inside the parameters.
	 * 
	 * @param parametersContext
	 *            The context from which the parameters must be extracted.
	 * @param referencedContexts
	 *            The set to which all context ID's of contexts referenced from the parameters must be added.
	 * @param parameterBuilder
	 *            A StringBuilder in which this method will produce the S-APL representation of the parameters.
	 * @return The parameters for RAB execution.
	 */
	private BehaviorStartParameters collectActionParameters(String parametersContext, Set<String> referencedContexts) {
		BehaviorStartParameters params = new BehaviorStartParameters();
		if (parametersContext.equals(SaplConstants.NULL)) {
			return params;
		}
		List<String> parameterIDs = new ArrayList<String>(this.targetModel.contexts.get(parametersContext).members);

		for (String parameterID : parameterIDs) {
			SemanticStatement parameterStatement = this.targetModel.beliefs.get(parameterID);
			if (parameterStatement.isSubjectContext() && parameterStatement.predicate.equals(SaplConstants.is) &&
				 parameterStatement.object.equals(SaplConstants.verbatim)){
				this.targetModel.copyLinks(parameterStatement.subject, parametersContext);
				this.targetModel.removeLink(parametersContext, parameterID);
			}
		}
		
		parameterIDs = this.targetModel.contexts.get(parametersContext).members;
		
		for (String parameterID : parameterIDs) {
			SemanticStatement parameterStatement = this.targetModel.beliefs.get(parameterID);
			if (parameterStatement.predicate.equals(SaplConstants.is)) {
				String parameter = parameterStatement.subject;
				if (!parameter.contains("://")) {
					throw new Error("Every parameter must be a resource. Found: "+parameter);
				}
				Resource parameterResource = Resource.createUncheckedResource(parameter);

				String parameterValue = parameterStatement.object;
				params.add(parameterResource, parameterValue);
				if (parameterStatement.isObjectContext()) {
					referencedContexts.add(parameterValue);
				}
			} else if (parameterStatement.predicate.equals(SaplConstants.add) && parameterStatement.isObjectContext()) {
				//add
				String contextToBeAdded = parameterStatement.object;
				String condition = parameterStatement.subject;
				if (condition.equals(SaplConstants.Start)) {
					params.addOnStart.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
				} else if (condition.equals(SaplConstants.End)) {
					params.addOnEnd.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
				} else if (condition.equals(SaplConstants.Success)) {
					params.addOnSuccess.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
				} else if (condition.equals(SaplConstants.Fail)) {
					params.addOnFail.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
					//} else if (condition.equals(SaplConstants.Denied)) {
					//ignore
				} else {
					print("Error in conditinal adding parameters : subject must be sapl:Start sapl:End sapl:Success or sapl:Fail"
							+ parameterStatement);
				}

			} else if (parameterStatement.predicate.equals(SaplConstants.remove) && parameterStatement.isObjectContext()) {
				String contextToBeAdded = parameterStatement.object;
				String condition = parameterStatement.subject;
				if (condition.equals(SaplConstants.Start)) {
					params.removeOnStart.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
				} else if (condition.equals(SaplConstants.End)) {
					params.removeOnEnd.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
				} else if (condition.equals(SaplConstants.Success)) {
					params.removeOnSuccess.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
				} else if (condition.equals(SaplConstants.Fail)) {
					params.removeOnFail.add(contextToBeAdded);
					referencedContexts.add(contextToBeAdded);
					//} else if (condition.equals(SaplConstants.Denied)) {
					//ignore
				} else {
					print("Error in conditinal remove parameters : subject must be sapl:Start sapl:End sapl:Success or sapl:Fail"
							+ parameterStatement);
				}
			} else {
				print("Error in action parameter: the predicate must be sapl:is or for conditional adding and removing, the Object must be context container: '"
						+ parameterStatement + "'");
			}
		}
		return params;
	}

	/**When RAB ends its execution, it calls this*/
	void removeRABInstance(ReusableAtomicBehavior behavior) {
		if (behavior.linkedContexts != null) {
			for (String linkedContextID : behavior.linkedContexts) {
				ContextContainer context = this.targetModel.contexts.get(linkedContextID);
				if (context != null) {
					context.referenced--;
				}
			}
		}		

		this.runningRABs.remove(behavior);
		this.rabInstanceIDs.remove(behavior.ID);
		synchronized(targetModel.lock){
			this.targetModel.removeLink(SaplConstants.DO_CONTEXT, behavior.ID);
			this.targetModel.removeLink(SaplConstants.DO_CONTEXT, this.rabExecutionStatementIDs.get(behavior.ID));
		}
	}


	/**Utility for removing beliefs from S-APL model using a context as a pattern*/
	void removeByPattern(String ID, SaplQueryResultSet resultSet) {
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in removeByPattern: Object must be context container or ID of statement");
			return;
		}
		if(resultSet==null) resultSet = new SaplQueryResultSet();
		synchronized(targetModel.lock){
			if (SaplQueryEngine.evaluateQuery(ID, SaplConstants.GENERAL_CONTEXT, this.targetModel, false, false, resultSet, true, null)) {
				Iterator<LinkInfo> it = resultSet.getForRemove().iterator();
				while (it.hasNext()) {
					LinkInfo li = it.next();
					if (!SaplConstants.protected_entities.contains(li.statementID)) {
						this.targetModel.removeLink(li.contextID, li.statementID);
					}
				}
			}
		}
	}

	/**Removes beliefs by pattern given (ID can be of a context or a statement)*/
	void removeBeliefs(String ID) {
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in removeBeliefs: the argument must be context container or ID of a statement");
			return;
		}
		this.removeByPattern(ID, null);
	}

	/**Removes beliefs by pattern given (ID can be of a context or a statement)
	 * also taking into account a set of variable bindings provided*/
	void removeBeliefs(String ID, Map<String, String> vars) {
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in removeBeliefs: the first argument must be context container or ID of a statement");
			return;
		}
		if(vars==null) this.removeByPattern(ID, null);
		else {
			SaplQueryResultSet rs = new SaplQueryResultSet();
			rs.addSolution(vars);
			this.removeByPattern(ID, rs);
		}
	}

	/**Removes beliefs (from G) based on pattern defined in an N3 string*/
	void removeBeliefsN3(String n3string){
		List<String> code = SaplN3Parser.compile(n3string, "removeBeliefsN3").getCode();
		if (code != null) {
			String id;
			synchronized(targetModel.lock){
				id = this.targetModel.contextIDGenerator.getNewContextID();
				this.targetModel.putContext(id);				
				SaplN3Parser.load(code, this.targetModel, id, null, "removeBeliefsN3", true);
				this.removeByPattern(id, null);
				targetModel.removeContext(id);
			}
		}
	}
	
	
	/**Erases beliefs by specific ID (can be of a context or a statement)*/
	void eraseBeliefsByID(String ID) {
		if(ID.startsWith(SaplConstants.CONTEXT_PREFIX)){
			synchronized(targetModel.lock){
				targetModel.removeContext(ID);
			}				
		}
		else if(ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			synchronized(targetModel.lock){
				targetModel.removeBelief(ID);
			}				
		}
		else{
			print("Error in eraseBeliefs: the argument must be context container or ID of a statement");
		}
	}
	
	/**Utility for erasing beliefs from S-APL model using a context as a pattern*/
	void eraseByPattern(String ID, SaplQueryResultSet resultSet) {
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in eraseByPattern: Object must be context container or ID of statement");
			return;
		}
		if(resultSet==null) resultSet = new SaplQueryResultSet();
		synchronized(targetModel.lock){
			if (SaplQueryEngine.evaluateQuery(ID, SaplConstants.GENERAL_CONTEXT, this.targetModel, false, true, resultSet, true, null)) {
				Iterator<LinkInfo> it = resultSet.getForRemove().iterator();
				while (it.hasNext()) {
					LinkInfo li = it.next();
					if (!SaplConstants.protected_entities.contains(li.statementID)) {
						this.targetModel.removeBelief(li.statementID);
					}
				}
			}
		}
	}	
	
	/**Erases beliefs by pattern given (ID can be of a context or a statement)*/
	void eraseBeliefs(String ID){
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in eraseBeliefsByPattern: the argument must be context container or ID of a statement");
			return;
		}
		this.eraseByPattern(ID, null);
	}
	
	/**Erases beliefs by pattern given (ID can be of a context or a statement)
	 * also taking into account a set of variable bindings provided*/
	void eraseBeliefs(String ID, Map<String, String> vars) {
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in eraseBeliefs: the first argument must be context container or ID of a statement");
			return;
		}
		if(vars==null) this.eraseByPattern(ID, null);
		else {
			SaplQueryResultSet rs = new SaplQueryResultSet();
			rs.addSolution(vars);
			this.eraseByPattern(ID, rs);
		}
	}
	
	/**Erases beliefs (from G) based on pattern defined in an N3 string*/
	void eraseBeliefsN3(String n3string){
		List<String> code = SaplN3Parser.compile(n3string, "eraseBeliefsN3").getCode();
		if (code != null) {
			String id;
			synchronized(targetModel.lock){
				id = this.targetModel.contextIDGenerator.getNewContextID();
				this.targetModel.putContext(id);				
				SaplN3Parser.load(code, this.targetModel, id, null, "eraseBeliefsN3", true);
				this.eraseByPattern(id, null);
				targetModel.removeContext(id);
			}
		}
	}
	

	/**Adds beliefs (copies a sub-graph given with ID can be of a context or a statement)
	 * with binding variables (if given)*/
	public void addBeliefs(String ID, Map<String, String> vars) {
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in addBeliefs: the argument must be context container or ID of a statement");
			return;
		}
		synchronized(targetModel.lock){
			this.targetModel.clearLocalIDs();
			List<LinkInfo> addLinks = SaplUpdateEngine.copy(ID, SaplConstants.GENERAL_CONTEXT, this.targetModel, vars);
			this.targetModel.clearLocalIDs();
			for (LinkInfo li : addLinks) {
				targetModel.putLink(li.contextID, li.statementID);
			}
		}
	}

	/**Adds beliefs in a way a rule does: using the solution set modifiers such as sapl:All defined for a result set
	 * */
	void addBeliefsAsRuleOutput(String ID, SaplQueryResultSet resultSet) {
		
		if (!ID.startsWith(SaplConstants.CONTEXT_PREFIX) && !ID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			print("Error in addBeliefs: the argument must be context container or ID of a statement");
			return;
		}

		synchronized(targetModel.lock){
			this.targetModel.clearLocalIDs();
			List<LinkInfo> addLinks = SaplUpdateEngine.addRuleOutput(resultSet, ID, this.targetModel, false);
			this.targetModel.clearLocalIDs();
			for (LinkInfo li : addLinks) {
				targetModel.putLink(li.contextID, li.statementID);
			}
		}
	}

	/**Adds beliefs in a way a rule does: using the solution set modifiers such as sapl:All defined for a result set.
	 * Adds a list of statements 
	 * */
	void addBeliefsAsRuleOutput(List<String> IDs, SaplQueryResultSet resultSet) {
		synchronized(targetModel.lock){
			this.targetModel.clearLocalIDs();
			List<LinkInfo> addLinks = SaplUpdateEngine.addRuleOutput(resultSet, IDs, this.targetModel, false);
			this.targetModel.clearLocalIDs();
			for (LinkInfo li : addLinks) {
				targetModel.putLink(li.contextID, li.statementID);
			}
		}
	}

	
	/**
	 * Adds beliefs (to G) defined by a N3 string
	 * @param n3string The String containing S-APL to be added to the beliefs.
	 * @param context ID of Context where to add 
	 * @param doChecks Whether to perform joining containers
	 * @return True if successfully parsed the string
	 */
	boolean addBeliefsN3(String n3string, String context, boolean doChecks) {
		
		if(n3string.isEmpty()) return false;
			
		if(context.equals(SaplConstants.G)) context = SaplConstants.GENERAL_CONTEXT;

		List<String> code = SaplN3Parser.compile(n3string, "addBeliefsN3").getCode();
		
		if (code != null) {
			synchronized(targetModel.lock){
				SaplN3Parser.load(code, this.targetModel, context, null ,"addBeliefsN3", doChecks);
			}
			return true;
		}
		return false;
	}

	boolean addBeliefsN3(String n3string, List<String> contexts, boolean doChecks) {
		if(contexts.size()==1) return addBeliefsN3(n3string, contexts.get(0), doChecks);
		else{
			String mainContext = SaplConstants.GENERAL_CONTEXT;
			List<String> otherContexts = new ArrayList<String>(contexts.size()-1);
			
			for(int i=0; i<contexts.size(); i++){
				if(i==0){
					if(contexts.get(i).equals(SaplConstants.G))
						mainContext = SaplConstants.GENERAL_CONTEXT;
					else mainContext = contexts.get(i);
				}
				else{
					if(contexts.get(i).equals(SaplConstants.G))
						otherContexts.add(SaplConstants.GENERAL_CONTEXT);
					else otherContexts.add(contexts.get(i));
				}
			}

			List<String> code = SaplN3Parser.compile(n3string, "addBeliefsN3").getCode();
			
			if (code != null) {
				synchronized(targetModel.lock){
					SaplN3Parser.load(code, this.targetModel, mainContext, otherContexts ,"addBeliefsN3", doChecks);
				}
				return true;
			}
			return false;
		}
	}
	
	
	/**
	 * Adds beliefs defined by a N3 string to a temporary context, returns the ID of the context.
	 * Even though it is intended as temporary, will not be garbage collected.
	 * Thus, needs to be explicitly 'eraseBeliefsByID'
	 * @param n3string
	 * @return ID of created ContextContainer
	 */
	String addBeliefsN3ToTempContext(String n3string) {
		List<String> code = SaplN3Parser.compile(n3string, "addBeliefsN3ToTempContext").getCode();

		if (code != null) {
			String id;
			synchronized(targetModel.lock){
				id = this.targetModel.contextIDGenerator.getNewContextID();
				this.targetModel.putContext(id);				
				SaplN3Parser.load(code, this.targetModel, id, null, "addBeliefsN3ToTempContext", true);
				//FIX 01.06.2018: Make persistent (not to be garbage collected)
				this.targetModel.contexts.get(id).referenced++;
			}
			return id;
		}
		return null;
	}
	

	/**Runs a query specified as ID of a context or ID of a single statement, against a specified context
	 * @param queryID ID of the query context in the S-APL model (or ID of a single statement)
	 * @param targetContextID Context
	 * @return Result set 
	 */
	SaplQueryResultSet hasBeliefs(String queryID, String targetContextID, boolean symmetricQuerying) {
		if(targetContextID.equals(SaplConstants.G)) targetContextID = SaplConstants.GENERAL_CONTEXT;
		SaplQueryResultSet resultSet =  new SaplQueryResultSet();
		boolean result;
		synchronized(targetModel.lock){
			result = SaplQueryEngine.evaluateQuery(queryID, targetContextID, this.targetModel, symmetricQuerying, false, resultSet, false, null);
		}
		if(result) return resultSet;
		else return null;
	}

	boolean hasBeliefs(String queryID, String targetContextID, boolean symmetricQuerying, SaplQueryResultSet resultSet){
		if(targetContextID.equals(SaplConstants.G)) targetContextID = SaplConstants.GENERAL_CONTEXT;
		boolean result;
		synchronized(targetModel.lock){
			result = SaplQueryEngine.evaluateQuery(queryID, targetContextID, this.targetModel, symmetricQuerying, false, resultSet, false, null);
		}
		return result;
	}

	boolean hasBeliefs(List<String> queryStatementIDs, String targetContextID, boolean symmetricQuerying, SaplQueryResultSet resultSet) {
		if(targetContextID.equals(SaplConstants.G)) targetContextID = SaplConstants.GENERAL_CONTEXT;
		boolean result;
		synchronized(targetModel.lock){
			result = SaplQueryEngine.evaluateQuery(queryStatementIDs, targetContextID, this.targetModel, symmetricQuerying, false, resultSet, false, null);
		}
		return result;
	}


	/**Runs a specified query against a specified context
	 * @param query S-APL N3 string defining the query
	 * @param targetContextID Context
	 * @return Result set
	 */
	SaplQueryResultSet hasBeliefsN3(String query, String targetContextID, boolean symmetricQuerying) {
		if(targetContextID.equals(SaplConstants.G)) targetContextID = SaplConstants.GENERAL_CONTEXT;
		SaplQueryResultSet resultSet =  new SaplQueryResultSet();
		boolean result;
		synchronized(targetModel.lock){
			result = SaplQueryEngine.evaluateQueryN3(query, targetContextID, this.targetModel, symmetricQuerying, resultSet);
		}
		if(result) return resultSet;
		else return null;
	}

	/**Runs a specified query against G
	 * @param query S-APL N3 string defining the query
	 * @return Result set
	 */
	SaplQueryResultSet hasBeliefsN3(String query, boolean symmetricQuerying) {
		return this.hasBeliefsN3(query, SaplConstants.GENERAL_CONTEXT, symmetricQuerying);
	}

	
	/**Returns the vector of copies of statements in a context container*/
	List<SemanticStatement> getStatements(String contextID) {
		return this.getStatements(contextID, null);
	}

	/**Returns the vector of copies of statements in a context container (+ fills in a list of their IDs)*/
	List<SemanticStatement> getStatements(String contextID, List<String> originalIDs) {
		if(contextID.equals(SaplConstants.G)) contextID = SaplConstants.GENERAL_CONTEXT;
		ContextContainer c = this.targetModel.contexts.get(contextID);
		if (c == null) {
			print("Error in getStatements: context container does not exist: " + contextID);
			return null;
		}
		if (originalIDs != null) {
			originalIDs.clear();
		}
		List<SemanticStatement> result = new ArrayList<SemanticStatement>();
		synchronized(targetModel.lock){
			Iterator<String> it = c.members.iterator();
			while (it.hasNext()) {
				String id = it.next();
				result.add(new SemanticStatement(this.targetModel.beliefs.get(id)));
				if (originalIDs != null) {
					originalIDs.add(id);
				}
			}
		}
		return result;
	}

	/**
	 * Adds a set of existing statements to another context
	 */
	void putStatements(List<String> ids,  String contextID)
	{
		if(contextID.equals(SaplConstants.G)) contextID = SaplConstants.GENERAL_CONTEXT;
		for(String id: ids){
			this.targetModel.putLink(contextID, id);
		}
	}


	/** Searches for actual query inside all layers of wrappings like {} sapl:All ?x , {} sapl:Some ?x
	 * @return ID of the final inner context of the query after all wrappings skipped*/ 
	String unwrapQuery(String queryID, SaplQueryResultSet resultSet, boolean recordWrappings) {
		String result;
		synchronized(targetModel.lock){
			result = SaplQueryEngine.unwrapSetPredicates(queryID, SaplConstants.GENERAL_CONTEXT, this.targetModel, resultSet, recordWrappings);
		}
		return result;
	}

	/**Produces an N3 string from a sub-graph starting with a context or a particular statement.
	 * @param ID ID of a context or a statement to produce N3 representation for*/
	String produceN3(String ID) {
		return this.produceN3(ID, SaplN3ProducerOptions.defaultOptions);
	}
	
	/**Produces an N3 string from a sub-graph
	 * @param ID ID of a context or a statement to produce N3 representation for
	 * @param options Settings*/
	String produceN3(String ID, SaplN3ProducerOptions options) {
		if(ID.equals(SaplConstants.G)) ID = SaplConstants.GENERAL_CONTEXT;
		SaplN3Producer producer = new SaplN3Producer(targetModel, targetModel.prefixer, options);

		if(ID.startsWith(SaplConstants.STATEMENT_PREFIX)){
			synchronized(targetModel.lock){
				targetModel.collectGarbage(); // otherwise, statements may still be referenced several times which will slow down the procedure
				ContextContainer c = new ContextContainer();
				c.members.add(ID);
				return producer.produceN3(c);
			}
		}
		else if(ID.startsWith(SaplConstants.CONTEXT_PREFIX)){
			synchronized(targetModel.lock){
				targetModel.collectGarbage();
				return producer.produceN3(targetModel.contexts.get(ID));
			}
		}
		else{
			throw new IllegalArgumentException("The argument for N3 production must be a context or a statement"); 
		}
	}

	/**Produces an N3 string corresponding to an arbitrary
	 * sub-set of existing in the model statements,
	 * which are treated as if they belong to the same context container.*/
	String produceN3(Collection<String> statements) {
		return this.produceN3(statements, SaplN3ProducerOptions.defaultOptions);
	}
	
	/**Produces an N3 string corresponding to an arbitrary
	 * sub-set of existing in the model statements,
	 * which are treated as if they belong to the same context container.*/
	String produceN3(Collection<String> statements, SaplN3ProducerOptions options) {
		SaplN3Producer producer = new SaplN3Producer(targetModel, targetModel.prefixer, options);

		String result;
		synchronized(targetModel.lock){
			targetModel.collectGarbage();
			ContextContainer c = new ContextContainer();
			c.members.addAll(statements);
			result = producer.produceN3(c);
		}
		return result;
	}
	
	/**Produces an N3 string from a sub-graph starting from a root subject in a context*/
	String produceN3(String contextID, String root){
		return this.produceN3(contextID, root, SaplN3ProducerOptions.defaultOptions);
	}

	/**Produces an N3 string from a sub-graph starting from a root subject in a context*/
	String produceN3(String contextID, String root, SaplN3ProducerOptions options){
		if(contextID.equals(SaplConstants.G)) contextID = SaplConstants.GENERAL_CONTEXT;
		SaplN3Producer producer = new SaplN3Producer(targetModel, targetModel.prefixer, options);
		if(contextID.startsWith(SaplConstants.CONTEXT_PREFIX)){
			synchronized(targetModel.lock){
				targetModel.collectGarbage(); // otherwise, statements may still be referenced several times which will slow down the procedure
				ContextContainer cnt = targetModel.contexts.get(contextID);
				ContextContainer temp = new ContextContainer();
				collectStatementsRecursion(cnt, root, temp.members, new HashSet<String>());

				if(!temp.members.isEmpty())
					return producer.produceN3(temp);
				else return "";
			}
		}
		else throw new IllegalArgumentException("The argument for N3 production must be a context"); 
	}
	
	private void collectStatementsRecursion(ContextContainer cnt, String root, List<String> members, Set<String> handled){
		handled.add(root);
		Set<String> sts = cnt.index_sub.get(root);
		if(sts!=null && !sts.isEmpty()){
			members.addAll(sts);
			for(String stID: sts){
				SemanticStatement st = targetModel.beliefs.get(stID);
				if(!handled.contains(st.object))
					collectStatementsRecursion(cnt, st.object, members, handled);
			}
		}
	}
	
	/**Sends a signal to SaplReasoner to wake up*/
	void wakeReasoner() {
		this.myReasoner.restart();
	}

	ResourcePrefixer getPrefixer() {
		return this.targetModel.prefixer;
	}

	/**Adds an object to blackboard, return the ID of the created object*/
	Resource addOnBlackboard(Object o) {
		return this.blackboard.addObject(o);
	}

	/**Removes an object from blackboard*/
	void removeFromBlackboard(Resource id) {
		this.blackboard.removeObject(id);
	}

	/**Returns an object from blackboard*/
	Object getFromBlackboard(Resource id) {
		return this.blackboard.getObject(id);
	}

	
	boolean executingWithResult() {
		return this.myReasoner.result != null;
	}

	void writeToResult(String text) {
		if(this.myReasoner.result != null)
			this.myReasoner.result.append(text);
	}
	
	void findAllVarsInStatement(SemanticStatement st, Set<String> required, Set<String> defined){
		Set<String> matched = new HashSet<String>();
		Set<String> calculated = new HashSet<String>();
		Set<String> used = new HashSet<String>();

		SaplQueryEngine.findAllVarsInStatement(st, targetModel, matched, calculated, used);

		matched.remove(""); //SaplQueryEngine uses that locally to indicate a presence of optional / or
		matched.remove(" "); //SaplQueryEngine uses that locally to indicate a presence of is-true

		if(required!=null){
			required.addAll(matched);
			required.addAll(used);
		}
		if(defined!=null) defined.addAll(calculated);
	}
	
}
