package sapl.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** 
 * Class for managing a set of solutions to a query over a S-APL model
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (01.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplQueryResultSet {

	/**A list of solutions to the query*/
	List<Map<String, String>> solutions;

	/**A set of variables used in the query*/
	Set<String> definedVars;

	/**When query is used as part of "remove by pattern" operation, a map of solution IDs to
	 * lists of links to remove*/
	Map<String, Set<LinkInfo>> forRemove;

	/**Maps solution IDs to their indices in the list of solutions*/
	Map<String, Integer> solutionIndices;


	/**sapl:All defined for this result set*/
	public List<String> forAll = new ArrayList<String>();
	/**sapl:Some defined for this result set*/
	List<String> forSome = new ArrayList<String>();
	/**sapl:OrderBy defined for this result set*/
	List<String> orderBy = new ArrayList<String>();
	/**sapl:Limit defined for this result set*/
	int limit = -1;
	/**sapl:Offset defined for this result set*/
	int offset = 0;

	/***/
	String goals_var = "";


	/**Default constructor*/
	public SaplQueryResultSet() {
		this(null, null);
	}
	
	/**Copy constructor*/
	public SaplQueryResultSet(SaplQueryResultSet toCopy) {
		this(toCopy.solutions, toCopy.definedVars);
		this.forAll.addAll(toCopy.forAll);
		this.forSome.addAll(toCopy.forSome);
		this.orderBy.addAll(toCopy.orderBy);
		this.limit = toCopy.limit;
		this.offset = toCopy.offset;
	}

	/**Constructor with explicit sets
	 * @param list A list of query solutions that will be used to populate the newly created instance
	 * @param defVars Set of variables to be added as 'defined' for this solution set (in addition to those explicitly used in the list of solutions given)   
	 * */
	public SaplQueryResultSet(List<Map<String, String>> list, Set<String> defVars) {
		this.definedVars = new HashSet<String>();
		this.forRemove = new HashMap<String, Set<LinkInfo>>();
		this.solutionIndices = new HashMap<String, Integer>();

		this.setSolutions(list, defVars);
	}

	public void setSolutions (List<Map<String, String>> list, Set<String> defVars){
		this.solutions = new ArrayList<Map<String, String>>();
		this.addSolutions(list);
		if(defVars!=null){
			this.definedVars.addAll(defVars);
		}
	}
	
	public void addSolutions(List<Map<String, String>> list){
		if (list != null) {
			Iterator<Map<String, String>> it = list.iterator();
			while (it.hasNext()) {
				Map<String, String> b = it.next();
				this.solutions.add(new HashMap<String, String>(b));
				this.definedVars.addAll(b.keySet());
				this.definedVars.remove("");
			}
		}	
	}

	public void addSolution(Map<String, String> sol){
		if (sol != null) {
			this.solutions.add(new HashMap<String, String>(sol));
			this.definedVars.addAll(sol.keySet());
			this.definedVars.remove("");
		}	
	}

	/**Removes solutions with the indices listed
	 * @param list List of indices for the solutions to remove*/
	void removeSolutions(List<Integer> list) {
		Collections.sort(list);
		for (int i = list.size() - 1; i >= 0; i--) {
			int ind = list.get(i).intValue();
			this.forRemove.remove(this.solutions.get(ind).get(""));
			this.solutions.remove(ind);
		}
	}

	/**Gets the number of solutions to the query
	 * @return The number of possible solutions to the query
	 * */
	public int getNumberOfSolutions(){
		return this.solutions.size();
	}

	/**Gets the solution to the query with the given index
	 * @param index Solution index
	 * @return Solution*/
	public Map<String, String> getSolution(int index) {
		if ((index > -1) && (index < this.solutions.size())) {
			return this.solutions.get(index);
		} else {
			return new HashMap<String, String>();
		}
	}

	public Set<String> getDefinedVars(){
		return new HashSet<String>(definedVars);
	}

	public void addDefinedVars(Set<String> vars){
		definedVars.addAll(vars);
	}

	public void addDefinedVar(String var){
		definedVars.add(var);
	}

	/**Fixes all solutions in the given list so each includes all the variables
	 * that are defined in query.
	 * Variable unbound in any given solution are explicitly included marked as unbound
	 * @param list List */
	void fix(List<Map<String, String>> list) {
		Iterator<Map<String, String>> it = list.iterator();
		while (it.hasNext()) {
			Map<String, String> solution = it.next();
			if (solution.size() == (this.definedVars.size() + 1)) {
				continue;
			}
			Iterator<String> var_it = this.definedVars.iterator();
			while (var_it.hasNext()) {
				String var = var_it.next();
				if (solution.get(var) == null) {
					solution.put(var, SaplConstants.UNBOUND_VARIABLE);
				}
			}
		}
	}

	/**Gets a set of solutions taking solution set modifiers into account that are recorded
	 * locally for this solution set
	 * */
	public List<Map<String, String>> getSolutions(){
		return this.getSolutions(this.forAll, this.forSome, this.orderBy, this.limit, this.offset, null, true);
	}
	
	/**Gets a set of solutions taking custom solution set modifiers into account
	 * @param forAll List of universally qualified variables 
	 * @return The list of solutions 
	 * */
	public List<Map<String, String>> getSolutions(List<String> forAll) {
		return this.getSolutions(forAll, new ArrayList<String>(), new ArrayList<String>(), -1, 0, null, true);
	}

	/**Gets a set of solutions taking custom solution set modifiers into account
	 * @param forAll List of universally qualified variables 
	 * @param groupCountVar Variable to do grouped counting by
	 * @return The list of solutions 
	 * */
	List<Map<String, String>> getSolutions(List<String> forAll, SemanticStatement grouping, boolean withinGroups) {
		return this.getSolutions(forAll, new ArrayList<String>(), new ArrayList<String>(), -1, 0, grouping, withinGroups);
	}

	/**Gets a set of solutions taking custom solution set modifiers into account
	 * @param forAll List of universally qualified variables 
	 * @param forSome List of existentially qualified variables
	 * @param orderBy List of variables to be used in sorting solutions
	 * @return The list of solutions 
	 * */
	List<Map<String, String>> getSolutions(List<String> forAll, List<String> forSome, List<String> orderBy) {
		return this.getSolutions(forAll, forSome, orderBy, -1, 0, null, true);
	}

	/**Gets a set of solutions taking custom solution set modifiers into account
	 * @param forAll List of universally qualified variables 
	 * @param forSome List of existentially qualified variables
	 * @param orderBy List of variables to be used in sorting solutions
	 * @param limit Maximum number of solutions to return (after sorting if defined)
	 * @param offset The index of the first solution to return  (after sorting if defined)
	 * @param groupVar Variable to do grouped counting by
	 * @return The list of solutions
	 * */
	List<Map<String, String>> getSolutions(List<String> forAll, List<String> forSome,
			List<String> orderBy, int limit, int offset, SemanticStatement grouping, boolean withinGroups) {
		
		boolean forAllSolutions = false;
		if(forAll.contains(SaplConstants.ANYTHING)){
			forAll = new ArrayList<String>(definedVars);
			forAllSolutions = true;	
		}
		if(forSome.contains(SaplConstants.ANYTHING)){
			forSome = new ArrayList<String>(definedVars);
		}

		if ((this.getNumberOfSolutions() == 0) || ((forAll.size() == 0) && (forSome.size() == 0))) {
			List<Map<String, String>> res = new ArrayList<Map<String, String>>();
			if (this.getNumberOfSolutions() == 0) {
				res.add(new HashMap<String, String>());
			} else {
				if ((orderBy.size() > 0) || (offset > 0)) {
					List<Map<String, String>> temp = new ArrayList<Map<String, String>>(this.solutions);
					temp = this.rearrangeSolutions(temp, orderBy, limit, offset);
					if (temp.size() > 0) {
						res.add(temp.get(0));
					}
				} else {
					res.add(this.getSolution(0));
				}
			}
			return res;
		}

		List<List<String>> vvv = new ArrayList<List<String>>();
		if (forSome.size() > 0) {
			for (int j = 0; j < forSome.size(); j++) {
				List<String> values = new ArrayList<String>();
				Set<String> hashvalues = new HashSet<String>();
				String var = forSome.get(j);
				Iterator<Map<String, String>> it = this.solutions.iterator();
				while (it.hasNext()) {
					String value = it.next().get(var);
					if (!hashvalues.contains(value)) {
						values.add(value);
						hashvalues.add(value);
					}
				}
				
				//randomizing
				List<String> randomized = new ArrayList<String>();
				int num = values.size();
				for (int k = num; k > 0; k--) {
					int ind = (int) Math.round(Math.random() * (k - 1));
					randomized.add(values.get(ind));
					values.remove(ind);
				}
				vvv.add(randomized);
			}
		}
		
		Map<Map<String, String>, Map<String, String>> result = new LinkedHashMap<Map<String, String>, Map<String, String>>();

		Map<Map<String, String>, List<Map<String, String>>> groups = null;
		if (grouping != null) {
			groups = new HashMap<Map<String, String>, List<Map<String, String>>>();
		}

		Iterator<Map<String, String>> it = this.solutions.iterator();
		while (it.hasNext()) {
			Map<String, String> solution = it.next();
			boolean add = true;

			Map<String, String> keyMap = new HashMap<String, String>();
			for (int i = 0; i < forAll.size(); i++) {
				String var = forAll.get(i);
				String value = solution.get(var);
				if( (value==null || value.equals(SaplConstants.UNBOUND_VARIABLE)) && forAllSolutions==false){
					add = false;
					break;
				}
				keyMap.put(var, value);
			}
			if(add){
				Map<String, String> res = result.get(keyMap);
				if (res != null) {
					if (grouping != null) {
						List<Map<String, String>> group = groups.get(keyMap);
						group.add(solution);
					}

					int sumdiff = 0;
					for (int k = 0; k < forSome.size(); k++) {
						String var = forSome.get(k);
						String this_value = solution.get(var);
						String exist_value = res.get(var);
						
						if (exist_value == null) {
							sumdiff = 1; // add
							break;
						}
						if (this_value == null) {
							add = false;
							break;
						}
						sumdiff += vvv.get(k).indexOf(exist_value) - vvv.get(k).indexOf(this_value);
					}
					if (!add || sumdiff <= 0) {
						add = false;
					} else {
						result.remove(keyMap);
					}
				} else if (grouping != null) {
					List<Map<String, String>> group = new ArrayList<Map<String, String>>();
					group.add(solution);
					groups.put(keyMap, group);
				}

				if (add) {
					result.put(keyMap, solution);
				}
			}
		}

		List<Map<String, String>> res_vec = new ArrayList<Map<String, String>>(result.values());

		if (res_vec.size()>0){
			if(grouping != null) {
				if(withinGroups){
					this.calculteGroupMetric(groups, grouping);
				}
				else{
					String calculatedVarName = grouping.getSubjectVariableName();
					groups = new HashMap<Map<String, String>,List<Map<String, String>>>();
					groups.put(new HashMap<String, String>(1), res_vec);
					this.calculteGroupMetric(groups, grouping);
					String calculatedVarValue = res_vec.get(0).get(calculatedVarName);
					for(Map<String, String> sol : this.solutions){
						if(!sol.containsKey(calculatedVarName))
							sol.put(calculatedVarName, calculatedVarValue);
					}
				}
			}
			else{
				res_vec = this.rearrangeSolutions(res_vec, orderBy, limit, offset);
			}
		}
		
		return res_vec;
	}


	private void calculteGroupMetric (Map<Map<String, String>,List<Map<String, String>>> groups,
			SemanticStatement grouping) {
		
		String groupVar = grouping.getSubjectVariableName();
		String operationVar = grouping.getObjectVariableName();
		
		for (Map<String, String> keyMap: groups.keySet()){
			List<Map<String, String>> group = groups.get(keyMap);
			
			boolean first = true;

			boolean haveValue = false;
			String sResult = "";
			double dResult = 0;
			Set<String> setResult = null;
			boolean numberValues = true;
			
			if(grouping.predicate.equals(SaplConstants.count) && grouping.object.equals(SaplConstants.ANYTHING)){
				sResult = String.valueOf(group.size());
				haveValue = true;
			}
			else {
				for(Map<String, String> solution : group){
					
					String value = solution.get(operationVar);
					if(value==null) continue;
					haveValue = true;
	
					if(grouping.predicate.equals(SaplConstants.count)){
						if(setResult==null) setResult = new HashSet<String>();
						setResult.add(value);
						numberValues = false;
					}
					else{
						double v = 0;
						if (numberValues) {
							try {
								v = Double.valueOf(value);
							} catch (NumberFormatException e) {
								numberValues = false;
							}
						}
						
						if (grouping.predicate.equals(SaplConstants.sum) || grouping.predicate.equals(SaplConstants.avg)) {
							if (numberValues) {
								dResult += v;
							}
							sResult += value;
						} else if (grouping.predicate.equals(SaplConstants.max)) {
							if (numberValues) {
								if (first || (v > dResult)) {
									dResult = v;
								}
							}
							if (first || (value.compareTo(sResult) > 0)) {
								sResult = value;
							}
						} else if (grouping.predicate.equals(SaplConstants.min)) {
							if (numberValues) {
								if (first || (v < dResult)) {
									dResult = v;
								}
							}
							if (first || (value.compareTo(sResult) < 0)) {
								sResult = value;
							}
						}
					}
					
					first = false;
				}
				if(haveValue){
					if(grouping.predicate.equals(SaplConstants.avg)){
						if (numberValues) {
							dResult /= group.size();
						}
					}
					if (numberValues) {
						double d = Math.floor(dResult);
						if (d == dResult) {
							sResult = String.valueOf((long) d);
						} else {
							sResult = String.valueOf(dResult);
						}
					}
					if(grouping.predicate.equals(SaplConstants.count)){
						sResult = String.valueOf(setResult.size());
					}
				}
				else{
					if(grouping.predicate.equals(SaplConstants.count)){
						sResult = "0";
						haveValue = true;
					}
				}
			}		
			
			if(haveValue){
				for(Map<String, String> solution : group){
					String s = sResult;
					solution.put(groupVar, s);				
				}
			}
		}
		
		this.definedVars.add(groupVar);		
	}

	/**Rearranges a given set of solutions and/or returns a subset of it.
	 * @param orderBy List of variables to be used in sorting solutions
	 * @param limit Maximum number of solutions to return (after sorting if defined)
	 * @param offset The index of the first solution to return  (after sorting if defined)
	 * @param res_list List of solutions to rearrange
	 * @return Resulting list of solutions
	 * */
	List<Map<String, String>> rearrangeSolutions(List<Map<String, String>> res_list, List<String> orderBy, int limit, int offset) {
		if (orderBy.size() > 0) {
			SolutionComparator c = new SolutionComparator(orderBy);
			Collections.sort(res_list, c);
		}

		if ((offset != 0) || ((limit != -1) && (limit < res_list.size()))) {
			if (limit == -1) {
				limit = res_list.size();
			}
			List<Map<String, String>> temp_vec = new ArrayList<Map<String, String>>();
			for (int i = offset; (i < (offset + limit)) && (i < res_list.size()); i++) {
				temp_vec.add(res_list.get(i));
			}
			res_list = temp_vec;
		}
		
		return res_list;
	}

	/**Adds a new empty solution, only if the result set is currently empty*/
	public Map<String,String> addEmptySolution() {
		if (this.getNumberOfSolutions() == 0) {
			Map<String, String> solution = new HashMap<String, String>();
			solution.put("", ".0");
			this.solutions.add(solution);
			return solution;
		}
		else return null;
	}

	/**Gets a list of links to remove combined from all solutions to the query
	 * @return List of links for removal*/
	Set<LinkInfo> getForRemove() {
		Set<LinkInfo> result = new HashSet<LinkInfo>();
		Iterator<Set<LinkInfo>> it = this.forRemove.values().iterator();
		while (it.hasNext()) {
			result.addAll(it.next());
		}
		return result;
	}

	/**Returns simple string representation for the solutions set*/
	@Override
	public String toString() {
		return this.solutions.toString();
	}

	/** Private utility class used only in rearrangeSolutions() for sorting solutions*/ 
	private static class SolutionComparator implements Comparator<Map<String, String>> {
		List<String> orderBy;
		int[] direction;

		public SolutionComparator(List<String> orderBy) {
			this.orderBy = orderBy;
			this.direction = new int[orderBy.size()];
			for (int i = 0; i < orderBy.size(); i++) {
				String var = orderBy.get(i);
				if (var.startsWith(" ")) {
					this.direction[i] = -1;
					orderBy.set(i, var.substring(1));
				} else {
					this.direction[i] = 1;
				}
			}
		}

		@Override
		public int compare(Map<String, String> o1, Map<String, String> o2) {
			for (int i = 0; i < this.orderBy.size(); i++) {
				String var = this.orderBy.get(i);
				String v1 = o1.get(var);
				String v2 = o2.get(var);
				if(v1==null && v2==null) return 0;
				else if(v1==null) return 1;
				else if(v2==null) return -1;
				try {
					Double d1 = new Double(v1);
					Double d2 = new Double(v2);
					int r = this.direction[i] * d1.compareTo(d2);
					if (r != 0) {
						return r;
					}
				} catch (NumberFormatException e) {
					int r = this.direction[i] * v1.compareTo(v2);
					if (r != 0) {
						return r;
					}
				}
			}
			return 0;
		}
	}

}
