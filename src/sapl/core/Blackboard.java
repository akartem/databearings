package sapl.core;

import java.io.Serializable;
import java.util.HashMap;

import sapl.core.rab.Resource;
import sapl.util.IDGenerator;

/**
 * Representation of the blackboard of an agent. The agent uses its blackboard to keep references to Java objects. 
 * The blackboard works thread safe.
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.0 (01.07.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */ 

public class Blackboard implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * The actual blackboard
	 */
	final HashMap<Resource, Object> blackboard = new HashMap<Resource, Object>();

	/**
	 * this object is kept for generating new IDs for objects put to the {@link Blackboard}
	 */
	final static IDGenerator idgenerator = new IDGenerator(SaplConstants.BLACKBOARD_OBJECT_PREFIX);

	/**
	 * Add an object to the blackboard, the returned ID can be used later to retrieve the object from the Blackboard
	 * 
	 * @param object
	 *            The object which should be put on the Blackboard.
	 * @return The ID assigned to the object put on the Blackboard
	 */
	public synchronized Resource addObject(Object object) {
		Resource id = Blackboard.idgenerator.getNewResource();
		this.blackboard.put(id, object);
		return id;
	}

	/**
	 * Remove an object from the Blackboard if it exists there. If the object existed, it will be returned.
	 * 
	 * @param ID
	 *            The ID of the desired object.
	 * @return The Object of which removal is requested.
	 */
	public synchronized Object removeObject(Resource ID) {
		return this.blackboard.remove(ID);
	}

	/**
	 * Get an object from the Blackboard if it exists there. Otherwise null will be returned.
	 * 
	 * @param ID
	 *            The ID of the desired object.
	 * @return The Object which is requested.
	 */
	public synchronized Object getObject(Resource ID) {
		return this.blackboard.get(ID);
	}
}
