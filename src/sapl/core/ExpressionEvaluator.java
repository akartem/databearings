package sapl.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TimeZone;

import sapl.util.IDGenerator;

/** 
 * Parser and evaluator of expressions within S-APL queries
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018 + Elisa since July 2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.7 (23.04.2019)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */ 

public class ExpressionEvaluator {

	/**Loader for external action classes*/
	private static final FunctionLoader loader = new FunctionLoader(ClassLoader.getSystemClassLoader());

	/**Is a static class. Constructor disabled*/
	private ExpressionEvaluator() {}
	
	private static SimpleDateFormat isoSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.US);
	private static SimpleDateFormat isoSdf_no_tz = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
	private static SimpleDateFormat isoSdf_ms = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.US);

	/**Syntax table*/
	private static final String a[][] = new String[ExpressionEvaluator.uCount][ExpressionEvaluator.tCount];
	static {
		ExpressionEvaluator.a[0][0] = "BE";
		ExpressionEvaluator.a[0][1] = "BE";
		ExpressionEvaluator.a[0][2] = "";
		ExpressionEvaluator.a[0][3] = "BE";
		ExpressionEvaluator.a[0][4] = "";
		ExpressionEvaluator.a[0][5] = "";
		ExpressionEvaluator.a[0][6] = "";
		ExpressionEvaluator.a[0][7] = "BE";
		ExpressionEvaluator.a[0][8] = "BE";
		ExpressionEvaluator.a[0][9] = "";
		ExpressionEvaluator.a[0][10] = "";
		ExpressionEvaluator.a[0][11] = "";
		
		ExpressionEvaluator.a[1][0] = "CF";
		ExpressionEvaluator.a[1][1] = "CF";
		ExpressionEvaluator.a[1][2] = "";
		ExpressionEvaluator.a[1][3] = "CF";
		ExpressionEvaluator.a[1][4] = "";
		ExpressionEvaluator.a[1][5] = "";
		ExpressionEvaluator.a[1][6] = "";
		ExpressionEvaluator.a[1][7] = "CF";
		ExpressionEvaluator.a[1][8] = "CF";
		ExpressionEvaluator.a[1][9] = "";
		ExpressionEvaluator.a[1][10] = "";
		ExpressionEvaluator.a[1][11] = "";
		
		ExpressionEvaluator.a[2][0] = "D";
		ExpressionEvaluator.a[2][1] = "D";
		ExpressionEvaluator.a[2][2] = "";
		ExpressionEvaluator.a[2][3] = "-Du";
		ExpressionEvaluator.a[2][4] = "";
		ExpressionEvaluator.a[2][5] = "";
		ExpressionEvaluator.a[2][6] = "";
		ExpressionEvaluator.a[2][7] = "D";
		ExpressionEvaluator.a[2][8] = "D";
		ExpressionEvaluator.a[2][9] = "";
		ExpressionEvaluator.a[2][10] = "";
		ExpressionEvaluator.a[2][11] = "";

		ExpressionEvaluator.a[3][0] = "G";
		ExpressionEvaluator.a[3][1] = "G";
		ExpressionEvaluator.a[3][2] = "";
		ExpressionEvaluator.a[3][3] = "";
		ExpressionEvaluator.a[3][4] = "";
		ExpressionEvaluator.a[3][5] = "";
		ExpressionEvaluator.a[3][6] = "";
		ExpressionEvaluator.a[3][7] = "@o(I)f";
		ExpressionEvaluator.a[3][8] = "G";
		ExpressionEvaluator.a[3][9] = "";
		ExpressionEvaluator.a[3][10] = "";
		ExpressionEvaluator.a[3][11] = "";

		ExpressionEvaluator.a[4][0] = "";
		ExpressionEvaluator.a[4][1] = "";
		ExpressionEvaluator.a[4][2] = "+BaE";
		ExpressionEvaluator.a[4][3] = "-BsE";
		ExpressionEvaluator.a[4][4] = "";
		ExpressionEvaluator.a[4][5] = "";
		ExpressionEvaluator.a[4][6] = "";
		ExpressionEvaluator.a[4][7] = "";
		ExpressionEvaluator.a[4][8] = "";
		ExpressionEvaluator.a[4][9] = "_";
		ExpressionEvaluator.a[4][10] = "_";
		ExpressionEvaluator.a[4][11] = "_";

		ExpressionEvaluator.a[5][0] = "";
		ExpressionEvaluator.a[5][1] = "";
		ExpressionEvaluator.a[5][2] = "_";
		ExpressionEvaluator.a[5][3] = "_";
		ExpressionEvaluator.a[5][4] = "*CmF";
		ExpressionEvaluator.a[5][5] = "/CdF";
		ExpressionEvaluator.a[5][6] = "%CrF";
		ExpressionEvaluator.a[5][7] = "";
		ExpressionEvaluator.a[5][8] = "";
		ExpressionEvaluator.a[5][9] = "_";
		ExpressionEvaluator.a[5][10] = "_";
		ExpressionEvaluator.a[5][11] = "_";
		
		ExpressionEvaluator.a[6][0] = "!";
		ExpressionEvaluator.a[6][1] = "?";
		ExpressionEvaluator.a[6][2] = "";
		ExpressionEvaluator.a[6][3] = "";
		ExpressionEvaluator.a[6][4] = "";
		ExpressionEvaluator.a[6][5] = "";
		ExpressionEvaluator.a[6][6] = "";
		ExpressionEvaluator.a[6][7] = "";
		ExpressionEvaluator.a[6][8] = "(A)";
		ExpressionEvaluator.a[6][9] = "";
		ExpressionEvaluator.a[6][10] = "";
		ExpressionEvaluator.a[6][11] = "";

		ExpressionEvaluator.a[7][0] = "A#";
		ExpressionEvaluator.a[7][1] = "A#";
		ExpressionEvaluator.a[7][2] = "";
		ExpressionEvaluator.a[7][3] = "A#";
		ExpressionEvaluator.a[7][4] = "";
		ExpressionEvaluator.a[7][5] = "";
		ExpressionEvaluator.a[7][6] = "";
		ExpressionEvaluator.a[7][7] = "A#";
		ExpressionEvaluator.a[7][8] = "A#";
		ExpressionEvaluator.a[7][9] = "";
		ExpressionEvaluator.a[7][10] = "";
		ExpressionEvaluator.a[7][11] = "";

		ExpressionEvaluator.a[8][0] = "AJ";
		ExpressionEvaluator.a[8][1] = "AJ";
		ExpressionEvaluator.a[8][2] = "";
		ExpressionEvaluator.a[8][3] = "AJ";
		ExpressionEvaluator.a[8][4] = "";
		ExpressionEvaluator.a[8][5] = "";
		ExpressionEvaluator.a[8][6] = "";
		ExpressionEvaluator.a[8][7] = "AJ";
		ExpressionEvaluator.a[8][8] = "AJ";
		ExpressionEvaluator.a[8][9] = "_";
		ExpressionEvaluator.a[8][10] = "";
		ExpressionEvaluator.a[8][11] = "";

		ExpressionEvaluator.a[9][0] = "";
		ExpressionEvaluator.a[9][1] = "";
		ExpressionEvaluator.a[9][2] = "";
		ExpressionEvaluator.a[9][3] = "";
		ExpressionEvaluator.a[9][4] = "";
		ExpressionEvaluator.a[9][5] = "";
		ExpressionEvaluator.a[9][6] = "";
		ExpressionEvaluator.a[9][7] = "";
		ExpressionEvaluator.a[9][8] = "";
		ExpressionEvaluator.a[9][9] = "_";
		ExpressionEvaluator.a[9][10] = ",AJ";
		ExpressionEvaluator.a[9][11] = "";
	}
	/**Number of terminal symbols*/
	private static final int tCount = 12;
	/**Terminal symbols*/
	private static final char t[] = new char[ExpressionEvaluator.tCount];
	static {
		ExpressionEvaluator.t[0] = '!';
		ExpressionEvaluator.t[1] = '?';
		ExpressionEvaluator.t[2] = '+';
		ExpressionEvaluator.t[3] = '-';
		ExpressionEvaluator.t[4] = '*';
		ExpressionEvaluator.t[5] = '/';
		ExpressionEvaluator.t[6] = '%';
		ExpressionEvaluator.t[7] = '@';
		ExpressionEvaluator.t[8] = '(';
		ExpressionEvaluator.t[9] = ')';
		ExpressionEvaluator.t[10] = ',';
		ExpressionEvaluator.t[11] = '#';
	}
	/**Number of non-terminal symbols: 'A','B','C'...*/
	private static final int uCount = 10;

	/**Defined functions*/
	public static final Map<String,String> functions;
	static {
		Map<String,String> functionsMap = new HashMap<String,String>();
		functionsMap.put("abs", "(<number>)");
		functionsMap.put("sqrt", "(<number>)");
		functionsMap.put("random", "()");
		functionsMap.put("pow", "(<number>,<power>)");
		functionsMap.put("ceil", "(<number>)");
		functionsMap.put("floor", "(<number>)");
		functionsMap.put("round", "(<number>[,<num_digits>])");
		functionsMap.put("length", "(<string>)");
		functionsMap.put("trim", "(<string>)");
		functionsMap.put("toLowerCase", "(<string>)");
		functionsMap.put("toUpperCase", "(<string>)");
		functionsMap.put("substring", "(<string>,<begin>[,<end>])");
		functionsMap.put("replace", "(<string>,<target>,<replacement>)");
		functionsMap.put("replaceAll", "(<string>,<regex>,<replacement>)");
		functionsMap.put("equals", "(<string>,<string>)");
		functionsMap.put("startsWith", "(<string>,<prefix>)");
		functionsMap.put("endsWith", "(<string>,<suffix>)");
		functionsMap.put("contains", "(<string>,<substring>)");
		functionsMap.put("indexOf", "(<string>,<substring>[,<from>])");
		functionsMap.put("lastIndexOf", "(<string>,<substring>[,<from>])");
		functionsMap.put("concat", "(<string>,<string>[,<string>,..])");
		functionsMap.put("getNameSpace", "(<uri>)");
		functionsMap.put("getLocalName", "(<uri>)");
		functionsMap.put("not", "(<boolean>)");
		functionsMap.put("min", "(<number_or_string>,<number_or_string>)");
		functionsMap.put("max", "(<number_or_string>,<number_or_string>)");
		functionsMap.put("timeMillis", "(<date_or_time_string>[,<format>])");
		functionsMap.put("timeString", "(<millis_or_date_or_time_string>[,<format>][,<timezone>])");
		functionsMap.put("ID", "([<variable>,..])");
		functionsMap.put("numberOfMembers", "(<contextID>)");
		functionsMap.put("unwrap", "(<contextID>)");
		functionsMap.put("exists", "(<variable>[,<variable>,..])");
		functionsMap.put("valueOf", "(<variable>)");
		functionsMap.put("firstExisting", "(<variable>[,<variable>,..])");
		functionsMap.put("isNumber", "(<number_or_string>)");
		functionsMap.put("number", "(<number_or_string>)");
		functionsMap.put("now", "()");
		functionsMap.put("put", "(<string>,<string>)");
		functionsMap.put("get", "(<string>)");
		functions = Collections.unmodifiableMap(functionsMap);
	}

	public static final Set<String> numericFunctions;
	static {
		Set<String> functionsSet = new HashSet<String>();
		functionsSet.add("abs");
		functionsSet.add("sqrt");
		functionsSet.add("pow");
		functionsSet.add("ceil");
		functionsSet.add("floor");
		functionsSet.add("round");
		functionsSet.add("min");
		functionsSet.add("max");
		functionsSet.add("isNumber");
		functionsSet.add("number");
		numericFunctions = Collections.unmodifiableSet(functionsSet);
	}	

	/**IDGenerator for the ID function, an example ID is <http://www.ubiware.jyu.fi/id#40498159194546746_1279028904845_3691>*/
	private static IDGenerator idgenerator = new IDGenerator(String.valueOf(Math.random()).substring(2));
	

	/**Records already compiled expressions to avoid doing that again*/
	private static Map<String, List<String>> savedCode = new HashMap<String, List<String>>();

	
	/**IDs generated by ID() function*/
	private static Map<String,Map<String,String>> map = new HashMap<String,Map<String,String>>();
	/**Key-value map used by put() and get() functions*/
	private static Map<String, Map<String,String>> idsGenerated = new HashMap<String,Map<String,String>>();

	
	/**Evaluates the value of an expression given as a string
	 * @param expression Expression
	 * @param textID Some identifier of the expression for error reporting
	 * @param vars A map with values for variables that may be used in the expression
	 * @param targetModel S-APL model
	 * @param errorMsg 
	 * */
	public static String calculate(String expression, String queryId, String textID, Map<String, String> vars, SaplModel targetModel, StringBuilder errorMsg) {
		
		List<String> code = ExpressionEvaluator.compile(expression, textID, errorMsg);

		if (code == null) return null;

		String result = ExpressionEvaluator.evaluateExpression(code, queryId, textID, vars, targetModel, errorMsg);
		return result;
	}

	/**Does lexical and syntax analysis of an expression*/
	public static List<String> compile(String text, String textID, StringBuilder errorMsg) {
		List<String> saved = savedCode.get(text);
		if(saved!=null) return saved; 

		List<String> tokens = new ArrayList<String>();
		StringBuilder lexStringBuilder = new StringBuilder();
		ExpressionEvaluator.lexicalAnalysis(text, tokens, lexStringBuilder);

		List<String> code = new ArrayList<String>();
		if (ExpressionEvaluator.syntaxAnalysis(tokens, lexStringBuilder.toString(), code, textID, errorMsg)) {
			savedCode.put(text, code);
			return code;
		} else {
			return null;
		}
	}

	/**Adds a lexical symbol to lexical string builder */
	private static void addLexSymbol(String word, StringBuilder lexStringBuilder){
		if (ExpressionEvaluator.functions.containsKey(word)
				|| word.startsWith("f:")) {
			lexStringBuilder.append('@');
			return;
		}

		if (word.startsWith(SaplConstants.VARIABLE_PREFIX) && word.length()>SaplConstants.VARIABLE_PREFIX.length()) {
			lexStringBuilder.append('?');
		} else {
			lexStringBuilder.append('!');
		}
	}

	/**Lexical analysis of an expression*/
	private static void lexicalAnalysis(String text, List<String> tokens, StringBuilder lexStringBuilder) {
		boolean inside_literal = false;
		char literal_start = ' ';
		int len = text.length();
		int tokS = 0;
		for (int c = 0; c < len; c++) {
			char ch = text.charAt(c);
			if (!inside_literal && ((ch == '"') || (ch == '\'') || (ch == '<'))) {
				inside_literal = true;
				literal_start = ch;
			} else if (inside_literal && (((ch == '"') && (literal_start == '"')) || ((ch == '\'') && (literal_start == '\'')) || ((ch == '>') && (literal_start == '<')))) {
				inside_literal = false;
			} else if (!inside_literal
					&& ((ch == '(') || (ch == ')') || (ch == '+') || (ch == '-') || (ch == '*') || (ch == '/') || (ch == '%') || (ch == ','))) {
				if (tokS != c) {
					String word = text.substring(tokS, c).trim();
					if(word.length() > 0){
						tokens.add(word);
						ExpressionEvaluator.addLexSymbol(word, lexStringBuilder);
					}
				}
				tokens.add("" + ch);
				lexStringBuilder.append(ch);
				tokS = c + 1;
			}
		}
		if (tokS < len) {
			String rest = text.substring(tokS, len).trim();
			tokens.add(rest);
			ExpressionEvaluator.addLexSymbol(rest, lexStringBuilder);
		}
	}

	/**Syntax analysis of an expression*/
	private static boolean syntaxAnalysis(List<String> tokens, String lexString, List<String> code, String textID, StringBuilder errorMsg) {
		lexString += '#';
		StringBuilder curString = new StringBuilder();
		curString.append('H');
		int iLex = 0;
		int iCur = 0;

		while (true) {
			while ((curString.charAt(iCur) >= 'a') && (curString.charAt(iCur) <= 'z')) {
				code.add("{@$@}" + curString.charAt(iCur));
				iCur++;
			}

			char ch = curString.charAt(iCur);
			char lex_ch = lexString.charAt(iLex);
			if (ch == lex_ch) {
				if (ch == '#') {
					return true;
				} else if ((ch == '?') || (ch == '!') || (ch == '@')) {
					String token = tokens.get(iLex);
					if(ch=='?'){
						code.add(token.substring(SaplConstants.VARIABLE_PREFIX.length()));
						code.add("{@$@}v");
					}
					else code.add(token);
				}
				iCur++;
				iLex++;
			} else {
				int iy = ch - 'A';
				if ((iy < 0) || (iy > ExpressionEvaluator.uCount)) {
					errorMsg.append("Syntax Error in " + textID);
					return false;
				}
				int ix = -1;
				for (int i = 0; i < ExpressionEvaluator.tCount; i++) {
					if (ExpressionEvaluator.t[i] == lex_ch) {
						ix = i;
						break;
					}
				}

				String fromTable = ExpressionEvaluator.a[iy][ix];

				if (fromTable.equals("") || fromTable.startsWith("$")) {
					errorMsg.append("Syntax Error in " + textID);
					return false;
				} else if (fromTable.equals("_")) {
					curString.deleteCharAt(iCur);
				} else {
					curString.replace(iCur, iCur + 1, fromTable);
				}

			}
		}
	}

	/**Converts a double value into string*/
	private static String makeStringValue(double v) {
		double d = Math.floor(v);
		if (d == v) {
			return String.valueOf((long) d);
		} else {
			return String.valueOf(v);
		}
	}
	
	private static Double getNumericValue (String s){
		if (s.equals(SaplConstants.NULL)) return new Double(0.0);
		try {
			return Double.valueOf(s);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	/**Evaluates the value of an expression, after it was compiled into code*/
	public static String evaluateExpression(List<String> code, String queryId, String textID, Map<String, String> vars, SaplModel targetModel, StringBuilder errorMsg) {
		Stack<String> stack = new Stack<String>();
		String funcName = null;
		
		for (String command : code) {
			if (command.equals("{@$@}a") || command.equals("{@$@}s") || command.equals("{@$@}m") || command.equals("{@$@}d") || command.equals("{@$@}r")) {
				String value1 = stack.pop();
				String value2 = stack.pop();
				boolean numberValues = true;
				Double v1 = getNumericValue(value1); 
				Double v2 = getNumericValue(value2); 

				if(v1==null || v2==null) numberValues = false;

				if (command.equals("{@$@}a")) {
					if (numberValues) {
						stack.push(ExpressionEvaluator.makeStringValue(v2 + v1));
					} else {
						stack.push(value2 + value1);
					}
				} else if (command.equals("{@$@}s")) {
					if (numberValues) {
						stack.push(ExpressionEvaluator.makeStringValue(v2 - v1));
					} else {
						boolean ok = false;
						Date d1 = castToDate(value1,null);
						if(d1!=null){
							Date d2 = castToDate(value2,null);
							if(d2!=null){
								stack.push(ExpressionEvaluator.makeStringValue(d2.getTime()-d1.getTime()));
								ok = true;
							}
						}
						if(!ok){
							errorMsg.append("Error in " + textID + ": Only numbers or dates can be operands of \"-\"");
							return null;
						}
					}
				} else if (command.equals("{@$@}m")) {
					if (numberValues) {
						stack.push(ExpressionEvaluator.makeStringValue(v2 * v1));
					} else {
						errorMsg.append("Error in " + textID + ": Only numbers can be operands of \"*\"");
						return null;
					}
				} else if (command.equals("{@$@}d")) {
					if (numberValues) {
						stack.push(ExpressionEvaluator.makeStringValue(v2 / v1));
					} else {
						errorMsg.append("Error in " + textID + ": Only numbers can be operands of \"/\"");
						return null;
					}
				} else if (command.equals("{@$@}r")) {
					if (numberValues) {
						stack.push(ExpressionEvaluator.makeStringValue(v2 % v1));
					} else {
						errorMsg.append("Error in " + textID + ": Only numbers can be operands of \"%\"");
						return null;
					}
				}
			}

			else if (command.equals("{@$@}u")) {
				String value1 = stack.pop();
				boolean numberValues = true;
				Double v1 = getNumericValue(value1);
				if(v1==null) numberValues = false;

				if (command.equals("{@$@}u")) {
					if (numberValues) {
						stack.push(ExpressionEvaluator.makeStringValue(-1 * v1));
					} else {
						errorMsg.append("Error in " + textID + ": Only a number can be operand of unary \"-\"");
						return null;
					}
				}
			}

			else if (command.equals("{@$@}o")) {
				funcName = stack.peek();
				stack.push(command);
			}
			
			else if (command.equals("{@$@}f")) {

				List<String> operands = new ArrayList<String>();
				List<Double> dOperands = new ArrayList<Double>();
				while (true) {
					String value = stack.pop();
					if (value.equals("{@$@}o")) {
						break;
					}
					operands.add(value);
				}

				String func = stack.pop();

				if(numericFunctions.contains(func)){
					for(String value : operands){
						Double v = getNumericValue(value);
						dOperands.add(v);
					}
				}

				//math functions
				if (func.equals("abs")) {
					if (dOperands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"abs\" must have exactly one operand");
						return null;
					}
					Double v = dOperands.get(0);
					if (v == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be operand of \"abs\"");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(Math.abs(v)));
				} else if (func.equals("sqrt")) {
					if (dOperands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"sqrt\" must have exactly one operand");
						return null;
					}
					Double v = dOperands.get(0);
					if (v == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be operand of \"sqrt\"");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(Math.sqrt(v)));
				} else if (func.equals("random")) {
					if (dOperands.size() != 0) {
						errorMsg.append("Error in " + textID + ": \"random\" must have no operands");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(Math.random()));
				} else if (func.equals("ceil")) {
					if (dOperands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"ceil\" must have exactly one operand");
						return null;
					}
					Double v = dOperands.get(0);
					if (v == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be operand of \"ceil\"");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(Math.ceil(v)));
				} else if (func.equals("floor")) {
					if (dOperands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"floor\" must have exactly one operand");
						return null;
					}
					Double v = dOperands.get(0);
					if (v == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be operand of \"floor\"");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(Math.floor(v)));
				} else if (func.equals("round")) {
					if ((dOperands.size() != 1) && (dOperands.size() != 2)) {
						errorMsg.append("Error in " + textID + ": \"round\" must have one or two operands");
						return null;
					}
					Double v2 = 0.0;
					int i = 0;
					if (operands.size() == 2) {
						v2 = dOperands.get(i++);
					}
					Double v1 = dOperands.get(i);
					if ((v1 == null) || (v2 == null)) {
						errorMsg.append("Error in " + textID + ": Only numbers can be operands of \"round\"");
						return null;
					}
					if (operands.size() == 1) {
						stack.push(ExpressionEvaluator.makeStringValue(Math.round(v1)));
					} else {
						if (v2 < 0) {
							errorMsg.append("Error in " + textID + ": The second operand of \"round\" must be >=0");
							return null;
						}
						stack.push(ExpressionEvaluator.makeStringValue((Math.round(v1 * Math.pow(10, v2))) / Math.pow(10, v2)));
					}
				} else if (func.equals("pow")) {
					if (dOperands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"round\" must have exactly two operands");
						return null;
					}
					Double v2 = dOperands.get(0);
					Double v1 = dOperands.get(1);
					if ((v1 == null) || (v2 == null)) {
						errorMsg.append("Error in " + textID + ": Only numbers can be operand of \"round\"");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(Math.pow(v1, v2)));
				}

				//math and string
				else if (func.equals("min")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"min\" must have exactly two operands");
						return null;
					}
					Double v2 = dOperands.get(0);
					Double v1 = dOperands.get(1);
					if ((v1 != null) && (v2 != null)) {
						stack.push(ExpressionEvaluator.makeStringValue(Math.min(v1, v2)));

					} else {
						String sv2 = operands.get(0);
						String sv1 = operands.get(1);
						if (sv1.compareTo(sv2) <= 0) {
							stack.push(sv1);
						} else {
							stack.push(sv2);
						}
					}
				} else if (func.equals("max")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"max\" must have exactly two operands");
						return null;
					}
					Double v2 = dOperands.get(0);
					Double v1 = dOperands.get(1);
					if ((v1 != null) && (v2 != null)) {
						stack.push(ExpressionEvaluator.makeStringValue(Math.max(v1, v2)));
					} else {
						String sv2 = operands.get(0);
						String sv1 = operands.get(1);
						if (sv1.compareTo(sv2) >= 0) {
							stack.push(sv1);
						} else {
							stack.push(sv2);
						}
					}
				}

				//string-processing functions
				else if (func.equals("length")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"length\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					stack.push(ExpressionEvaluator.makeStringValue(v.length()));
				} else if (func.equals("trim")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"trim\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					stack.push(v.trim());
				} else if (func.equals("concat")) {
					if (operands.size() < 2) {
						errorMsg.append("Error in " + textID + ": \"concat\" must have more than one operand");
						return null;
					}
					StringBuilder result = new StringBuilder(); 
					for(int i=operands.size()-1; i>=0 ; i--){
						result.append(operands.get(i));
					}
					stack.push(result.toString());
				} else if (func.equals("substring")) {

					if ((operands.size() != 2) && (operands.size() != 3)) {
						errorMsg.append("Error in " + textID + ": \"substring\" must have two or three operands");
						return null;
					}
					Double v3 = 0.0;
					int i = 0;
					if (operands.size() == 3) {
						String sv3 = operands.get(i++);
						v3 = getNumericValue(sv3);
					}
					Double v2 = null;
					String sv2 = operands.get(i++);
					v2 = getNumericValue(sv2);

					String v1 = operands.get(i);

					if (v2 == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be the second operand of \"substring\"");
						return null;
					}
					if (v3 == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be the third operand of \"substring\"");
						return null;
					}

					int ind1 = (int) v2.doubleValue();
					if (ind1 < 0) {
						ind1 = v1.length();
					}
					int ind2;
					if ((operands.size() < 3) || ((ind2 = (int) v3.doubleValue()) < 0)) {
						ind2 = v1.length();
					}
					String result = null;
					try {
						result = v1.substring(ind1, ind2);
					} catch (IndexOutOfBoundsException e) {
						result = "";
					}
					stack.push(result);
				} else if (func.equals("replace")) {
					if (operands.size() != 3) {
						errorMsg.append("Error in " + textID + ": \"replace\" must have exactly three operands");
						return null;
					}
					String v3 = operands.get(0);
					String v2 = operands.get(1);
					String v1 = operands.get(2);
					stack.push(v1.replace(v2, v3));
				} else if (func.equals("replaceAll")) {
					if (operands.size() != 3) {
						errorMsg.append("Error in " + textID + ": \"replaceAll\" must have exactly three operands");
						return null;
					}
					String v3 = operands.get(0);
					String v2 = operands.get(1);
					String v1 = operands.get(2);
					stack.push(v1.replaceAll(v2, v3));
				} else if (func.equals("equals")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"equals\" must have exactly two operands");
						return null;
					}
					String v2 = operands.get(0);
					String v1 = operands.get(1);
					if (v1.equals(v2)) {
						stack.push(SaplConstants.TRUE);
					} else {
						stack.push(SaplConstants.FALSE);
					}
				} else if (func.equals("startsWith")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"startsWith\" must have exactly two operands");
						return null;
					}
					String v2 = operands.get(0);
					String v1 = operands.get(1);
					if (v1.startsWith(v2)) {
						stack.push(SaplConstants.TRUE);
					} else {
						stack.push(SaplConstants.FALSE);
					}
				} else if (func.equals("endsWith")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"endsWith\" must have exactly two operands");
						return null;
					}
					String v2 = operands.get(0);
					String v1 = operands.get(1);
					if (v1.endsWith(v2)) {
						stack.push(SaplConstants.TRUE);
					} else {
						stack.push(SaplConstants.FALSE);
					}
				} else if (func.equals("contains")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"contains\" must have exactly two operands");
						return null;
					}
					String v2 = operands.get(0);
					String v1 = operands.get(1);
					if (v1.contains(v2)) {
						stack.push(SaplConstants.TRUE);
					} else {
						stack.push(SaplConstants.FALSE);
					}
				} else if (func.equals("indexOf")) {
					if ((operands.size() != 2) && (operands.size() != 3)) {
						errorMsg.append("Error in " + textID + ": \"indexOf\" must have two or three operands");
						return null;
					}
					Double v3 = 0.0;
					int i = 0;
					if (operands.size() == 3) {
						String sv3 = operands.get(i++); 
						v3 = getNumericValue(sv3);
					}
					String v2 = operands.get(i++);
					String v1 = operands.get(i);

					if (v3 == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be the third operand of \"indexOf\"");
						return null;
					}
					int ind;
					if (operands.size() == 3) {
						ind = (int) v3.doubleValue();
					} else {
						ind = 0;
					}
					int res = v1.indexOf(v2, ind);
					stack.push(ExpressionEvaluator.makeStringValue(res));
				} else if (func.equals("lastIndexOf")) {
					if ((operands.size() != 2) && (operands.size() != 3)) {
						errorMsg.append("Error in " + textID + ": \"lastIndexOf\" must have two or three operands");
						return null;
					}
					Double v3 = 0.0;
					int i = 0;
					if (operands.size() == 3) {
						String sv3 = operands.get(i++); 
						v3 = getNumericValue(sv3);
					}
					String v2 = operands.get(i++);
					String v1 = operands.get(i);

					if (v3 == null) {
						errorMsg.append("Error in " + textID + ": Only a number can be the third operand of \"lastIndexOf\"");
						return null;
					}
					int ind;
					if (operands.size() == 3) {
						ind = (int) v3.doubleValue();
					} else {
						ind = v1.length();
					}
					int res = v1.lastIndexOf(v2, ind);
					stack.push(ExpressionEvaluator.makeStringValue(res));
				} else if (func.equals("toLowerCase")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"toLowerCase\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					stack.push(v.toLowerCase());
				} else if (func.equals("toUpperCase")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"toUpperCase\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					stack.push(v.toUpperCase());
				} else if (func.equals("getNameSpace")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"getNameSpace\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					int ind = v.indexOf("#");
					if (ind == -1) {
						ind = v.lastIndexOf("/");
					}
					if (ind == -1) {
						stack.push("");
					} else {
						stack.push(v.substring(0, ind + 1));
					}
				} else if (func.equals("getLocalName")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"getLocalName\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					int ind = v.indexOf("#");
					if (ind == -1) {
						ind = v.lastIndexOf("/");
					}
					if (ind == -1) {
						stack.push(v);
					} else {
						stack.push(v.substring(ind + 1));
					}
				}

				//other functions
				else if (func.equals("not")) {
					if (operands.size() > 1) {
						errorMsg.append("Error in " + textID + ": \"not\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					if (v.equals(SaplConstants.FALSE) || v.equals("false")) {
						stack.push(SaplConstants.TRUE);
					} else {
						stack.push(SaplConstants.FALSE);
					}
				}
				else if (func.equals("exists")) {
					if (operands.size() == 0) {
						stack.push(SaplConstants.FALSE);
					} else {
						stack.push(SaplConstants.TRUE);
					}
				} 
				else if (func.equals("firstExisting")) {
					if (operands.size() > 0) stack.push(operands.get(operands.size()-1));
					else stack.push(SaplConstants.NULL);
				}
				else if (func.equals("numberOfMembers")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"numberOfMembers\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					ContextContainer context = targetModel.contexts.get(v);
					if (context == null) {
						errorMsg.append("Error in " + textID + ": Only a Context ID can be operand of \"numberOfMembers\"");
						return null;
					}
					stack.push(ExpressionEvaluator.makeStringValue(context.members.size()));
				} else if (func.equals("unwrap")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"unwrap\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					ContextContainer context = targetModel.contexts.get(v);
					if (context == null) {
						SemanticStatement st = targetModel.beliefs.get(v);
						if(st==null){
							errorMsg.append("Error in " + textID + ": Only a Context ID or a Statement ID can be operand of \"unwrap\"");
							return null;
						}
					}
					String innerID = SaplQueryEngine.unwrapSetPredicates(v, SaplConstants.GENERAL_CONTEXT, targetModel, null, false);
					stack.push(innerID);
				} else if (func.equals("valueOf")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"valueOf\" must have exactly one operand");
						return null;
					}
					String v = operands.get(0);
					if (v.startsWith(SaplConstants.VARIABLE_PREFIX) && v.length()>SaplConstants.VARIABLE_PREFIX.length()) {
						String value = vars.get(v.substring(1));
						if (value == null) {
							errorMsg.append(v + " not bound");
							return null;
						}
						stack.push(value);
					} else {
						errorMsg.append("Error in " + textID + ": Only a variable name can be operand of \"valueOf\", but \"" + v
								+ "\" found");
						return null;
					}
				} else if (func.equals("timeMillis")) {
					if ((operands.size() != 1) && (operands.size() != 2)) {
						errorMsg.append("Error in " + textID + ": \"timeMillis\" must have one or two operands");
						return null;
					}
					String v2 = null;
					int i = 0;
					if (operands.size() == 2) {
						v2 = operands.get(i++);
					}
					String v = operands.get(i++).trim();

					Date date = castToDate(v, v2);

					if(date==null){
						errorMsg.append("Error in " + textID
								+ ": Cannot convert the operand into Date: found "
								+ v);
						return null;
					}
					stack.push(String.valueOf(date.getTime()));
				} else if (func.equals("timeString")) {
					if ((operands.size() != 1) && (operands.size() != 2) && (operands.size() != 3)) {
						errorMsg.append("Error in " + textID + ": \"timeString\" must have one, two or three operands");
						return null;
					}
					String tzone = null;
					String format = null;
					int i = 0;
					String v2 = null;
					String v3 = null;

					if (operands.size() == 3) {
						v3 = operands.get(i++);
					}
					if (operands.size() >= 2) {
						v2 = operands.get(i++);
					}
					Double v;
					String str_v = operands.get(i); 
					v = getNumericValue(str_v);

					TimeZone timezone = null;
					if (operands.size() > 1) {
						format = v2;
						String[] tzs = TimeZone.getAvailableIDs();
						for (int itz = 0; itz < tzs.length; itz++) {
							if (tzs[itz].equals(v2)) {
								tzone = v2;
								format = v3;
								break;
							}
							if (v3 != null) {
								if (tzs[itz].equals(v3)) {
									tzone = v3;
									format = v2;
									break;
								}
							}
						}
						if ((operands.size() == 3) && (tzone == null)) {
							errorMsg.append("Error in " + textID + ": Unknown timezone given");
							return null;
						}
						if (tzone != null) {
							timezone = TimeZone.getTimeZone(tzone);
						}
					}

					GregorianCalendar gc;
					if (timezone != null) {
						gc = new GregorianCalendar(timezone);
					} else {
						gc = new GregorianCalendar();
					}
				
					if (v == null) {
						if(format==null){
							//Performance fix: avoid processing already proper ISO timestamps
							//criteria: have 'T' and have explicit timezone that is equal to required one
							TimeZone tz = (timezone!=null)? timezone : TimeZone.getDefault();
							double timezone_offset = ((double)tz.getOffset(System.currentTimeMillis())) / 3600000;
							int hours = Math.abs((int)Math.floor(timezone_offset));
							String strOffset = ( (Math.signum(timezone_offset)>=0)?"+":"-") + (hours<10?"0":"") + String.valueOf(hours) + ":";
							strOffset += ((int)((Math.abs(timezone_offset)-hours) * 6) ) + "0";
							if(str_v.contains("T") && (str_v.contains(strOffset) || timezone_offset==0 && str_v.endsWith("Z")) ){
								stack.push(str_v);
								continue;
							}
						}

						Date date = castToDate(str_v, null);
						if(date == null){
							errorMsg.append("Error in " + textID
									+ ": Cannot convert the operand into Date: found "
									+ str_v);							
							return null;
						}
						
						gc.setTimeInMillis(date.getTime());
					}
					else {
						long time = v.longValue();
						gc.setTimeInMillis(time);
					}
					
					SimpleDateFormat sdf;
					if (format == null) sdf = isoSdf;
					else sdf = new SimpleDateFormat(format, Locale.US);
					try {
						if (timezone != null) {
							sdf.setTimeZone(timezone);
						}
						String str = sdf.format(gc.getTime());
						stack.push(str);
					} catch (Exception e) {
						errorMsg.append("Error in " + textID + ": Error when formating in two-operand \"timeString\": " + v + " "
								+ v2);
						return null;
					}
				} else if (func.equals("ID")) {
					if (operands.size() == 0) {
						errorMsg.append("Error in " + textID + ": \"ID\" must have one or more operands");
						return null;
					}
					String id = null; 
					String key = "";
					Map<String,String> thisQueryIDs = idsGenerated.get(queryId);
					if(operands.size()>0){
						for (String op : operands){
							key += "{@$@}"+op;
						}
						if(thisQueryIDs!=null)
							id = idsGenerated.get(queryId).get(key);
					}
					if(id==null){
						id = ExpressionEvaluator.idgenerator.getNewID();
						if(operands.size() > 0){
							if(thisQueryIDs==null){
								thisQueryIDs = new HashMap<String,String>();
								idsGenerated.put(queryId, thisQueryIDs);
							}
							thisQueryIDs.put(key, id);
						}
					}
					stack.push(id);
				} else if (func.equals("isNumber")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"isNumber\" must have exactly one operand");
						return null;
					}
					Double d = dOperands.get(0);
					if(d!=null) stack.push(SaplConstants.TRUE);
					else stack.push(SaplConstants.FALSE);
				} else if (func.equals("number")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"number\" must have exactly one operand");
						return null;
					}
					Double d = dOperands.get(0);
					if(d!=null) stack.push(ExpressionEvaluator.makeStringValue(d));
					else stack.push("0");
				} else if (func.equals("now")) {
					if (operands.size() != 0) {
						errorMsg.append("Error in " + textID + ": \"now\" must have no operands");
						return null;
					}				
					stack.push(String.valueOf(System.currentTimeMillis()));
				} else if (func.equals("put")) {
					if (operands.size() != 2) {
						errorMsg.append("Error in " + textID + ": \"put\" must have exactly two operands");
						return null;
					}				
					String key = operands.get(1); 
					String value = operands.get(0); 
					Map<String,String> thisQueryMap = map.get(queryId);
					if(thisQueryMap==null){
						thisQueryMap = new HashMap<String,String>();
						map.put(queryId, thisQueryMap);
					}
					thisQueryMap.put(key, value);
					stack.push(value);
				} else if (func.equals("get")) {
					if (operands.size() != 1) {
						errorMsg.append("Error in " + textID + ": \"get\" must have exactly one operand");
						return null;
					}				
					String key = operands.get(0); 
					Map<String,String> thisQueryMap = map.get(queryId);
					String value = null;
					if(thisQueryMap!=null){
						value = thisQueryMap.get(key);
					}
					if(value!=null)
						stack.push(value);
					else stack.push(SaplConstants.NULL);
				}
				else if(func.startsWith("f:")){
					//externally functions loaded dynamically
					String className = func.substring("f:".length());
					Class<Function> functionClass;
					try {
						functionClass = ExpressionEvaluator.loader.loadClass(className);
						Function function = functionClass.newInstance();
						function.setQueryId(queryId);
						StringBuilder functionErrorMessage = new StringBuilder();

						Collections.reverse(operands);
						String result = function.evaluate(operands, functionErrorMessage);
						if(result==null){
							errorMsg.append("Error in " + textID + ": "+functionErrorMessage);
							return null;
						}
						else stack.push(result);

					} catch (Throwable e) {
						e.printStackTrace();
						errorMsg.append("Error in " + textID + ": Class for function '"+func+"' cannot be loaded : "+e.getClass()+ " : "+e.getMessage());
					}
				}
				else{ //should not get here, just in case
					errorMsg.append("Error in " + textID + ": Unknown function '"+func+"'");
					return null;
				}
			}
			else if(command.equals("{@$@}v")){
				String var = stack.pop();
				String value = vars.get(var);
				
				if (value != null) {
					stack.push(value);
				} else {
					if(funcName!=null && (funcName.equals("exists") || funcName.equals("firstExisting")));
					else{
						errorMsg.append(SaplConstants.VARIABLE_PREFIX+var + " is not bound");
						return null;
					}
				}
			}
			else {
				if (command.startsWith("\"") && command.endsWith("\"") || command.startsWith("'") && command.endsWith("'")) {
					command = command.substring(1, command.length() - 1);
				}
				stack.push(command);
			}
		}

		if (!stack.isEmpty()) {
			return stack.pop();
		} else {
			return null;
		}
	}

	private static Date castToDate(String v, String format){

		if (format == null) {
			//if a number
			try{
				long l = Long.valueOf(v);
				Date d = new Date(l);
				return d;
			}
			catch(NumberFormatException e){
			}

			//some simple fixes to extend the range of accepted values
			v = v.replace("/", "-");
			
			if(v.indexOf('-') == 2){
				if(v.lastIndexOf("-20")==5){
					//inverse date
					String day = v.substring(0,2);
					String month = v.substring(3,5);
					String year = v.substring(6,10);
					v = year+'-'+month+'-'+day+v.substring(10);
				}
				else{
					// just yy instead of yyyy
					v = "20"+ v;
				}
			}
			if(v.indexOf(" ") != -1) // whitespace instead of T
				v = v.replace(" ", "T");
			int ind = v.indexOf("Z");
			if(ind==-1) ind = v.indexOf("+");
			if(ind==-1) ind = v.indexOf("-", v.indexOf('T'));
			if(ind==-1){
				if(v.split(":").length == 2) v = v + ":00"; // does not have ss
				else{
					int dot = v.indexOf(".");
					if(dot != -1 && dot < v.length()-4){
						v = v.substring(0, dot+4);
					}
				}
			}
			else{
				String zone = v.substring(ind);
				String main = v.substring(0,ind);
				if(!zone.equals("Z") && !zone.contains(":")){ //timezone is not in +03:00 format
					if(zone.length()==5) zone = zone.substring(0,3)+":"+zone.substring(3);
					else if(zone.length()==3) zone = zone+":00";
					v = main+zone;
				}
				if(main.split(":").length == 2){ // does not have ss
					v = main+":00"+zone;
				}
				else{
					int dot = main.indexOf(".");
					if(dot != -1 && dot < main.length()-4){
						v = main.substring(0, dot+4) + zone;
					}
				}

			}

			try {
				Date d;
				if(ind==-1) d = isoSdf_no_tz.parse(v);
				else if(v.contains(".")){
					d = isoSdf_ms.parse(v);
				}
				else d = isoSdf.parse(v);
				return d;
			} catch (ParseException e) {
				return null;
			}

		} else {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
				Date d = sdf.parse(v);
				return d;
			} catch (Exception e) {
				return null;
			}
		}
	}

	/**A utility function that does not evaluate the expression but just does the lexical analysis of it
	 * @param expression Expression to tokenize
	 * @return List of tokens*/
	public static List<String> tokenize(String expression) {
		List<String> tokens = new ArrayList<String>();
		StringBuilder lexStringBuilder = new StringBuilder();
		ExpressionEvaluator.lexicalAnalysis(expression, tokens, lexStringBuilder);
		return tokens;
	}

	/**Clears savedCode map*/
	static void clearSavedCode(){
		ExpressionEvaluator.savedCode.clear();
	}

	/**Clears savedCode map*/
	static void cleanAfterQuery(String queryId){
		ExpressionEvaluator.map.remove(queryId);
		ExpressionEvaluator.idsGenerated.remove(queryId);
	}
	
	public static Map<String, String> getMap(String queryId){
		return map.get(queryId);
	}

}