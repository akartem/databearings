package sapl.core;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Common constants
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (09.05.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplConstants {
	
	static final boolean EVALUATION_EDITION = false;
	
	static final int EVALUATION_EDITION_SOLUTIONS_LIMIT = 20;
	static final int EVALUATION_EDITION_RULE_EVALUATIONS_LIMIT = 100; //per minute
	
	public static final String RDF_NS = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public static final String RDFS_NS = "http://www.w3.org/2000/01/rdf-schema#";
	public static final String OWL_NS = "http://www.w3.org/2002/07/owl#";
	public static final String XSD_NS = "http://www.w3.org/2001/XMLSchema#";

	public static final String SAPL_NS = "http://www.ubiware.jyu.fi/sapl#";
	public static final String RAB_NS = "http://www.ubiware.jyu.fi/rab#";
	public static final String PARAM_NS = "http://www.ubiware.jyu.fi/rab_parameters#";
	public static final String DATA_NS = "http://www.ubiware.jyu.fi/data#";
	public static final String ONTONUT_NS = "http://www.ubiware.jyu.fi/ontonut#";
	public static final String META_NS = "http://www.ubiware.jyu.fi/meta#";
	public static final String FUNC_NS = "http://www.ubiware.jyu.fi/function#";
	public static final String TECH_NS = "http://www.ubiware.jyu.fi/tech#";
	
	public static final String EMPTY_NS = "#";
	
	public static final String BLANK_NODE_PREFIX = "_:";
	public static final String VARIABLE_PREFIX = "?";
	public static final String ANYTHING = "*";

	public static final String CONTEXT_PREFIX = "_C";
	
	public static final String GENERAL_CONTEXT = CONTEXT_PREFIX+"_GENERAL";

	public static final String QUERY_CONTEXT_PREFIX = VARIABLE_PREFIX+CONTEXT_PREFIX;

	public static final String STATEMENT_PREFIX = "_S";
	public static final String STATEMENT_LOCAL_ID_PREFIX = "__S";
	
	public static final String BLACKBOARD_OBJECT_PREFIX = "_BLACKBOARD";

	public static final String RULES_CONTEXT = "_C_RULES";
	static final String GOALS_CONTEXT = "_C_GOALS";
	static final String ACHIEVE_CONTEXT = "_C_ACHIEVE";
	static final String DO_CONTEXT = "_C_DO";
	static final String FALSE_CONTEXT = "_C_FALSE";

	static final String RULES_BELIEF = "_S_RULES";
	static final String GOALS_BELIEF = "_S_GOALS";
	static final String ACHIEVE_BELIEF = "_S_ACHIEVE";
	static final String DO_BELIEF = "_S_DO";
	static final String FALSE_BELIEF = "_S_FALSE";

	static final String NAME_BELIEF = "_S_NAME";
	static final String TIME_BELIEF = "_S_TIME";
	static final String DATE_BELIEF = "_S_DATE";
	static final String NOW_BELIEF = "_S_NOW";
	static final String INPUT_BELIEF = "_S_INPUT";

	public static final String UNBOUND_VARIABLE = "{@$unbound$@}";


	public static final String rdfType = SaplConstants.RDF_NS + "type";
	public static final String rdfSubject = SaplConstants.RDF_NS + "subject";
	public static final String rdfPredicate = SaplConstants.RDF_NS + "predicate";
	public static final String rdfObject = SaplConstants.RDF_NS + "object";
	public static final String rdfFirst = SaplConstants.RDF_NS + "first";
	public static final String rdfRest = SaplConstants.RDF_NS + "rest";
	public static final String rdfNil = SaplConstants.RDF_NS + "nil";
	
	public static final String xsdString = SaplConstants.XSD_NS + "string";
	
	
	public static final String G = SaplConstants.SAPL_NS + "G";

	public static final String I = SaplConstants.SAPL_NS + "I";

	public static final String remove = SaplConstants.SAPL_NS + "remove";
	public static final String add = SaplConstants.SAPL_NS + "add";
	public static final String erase = SaplConstants.SAPL_NS + "erase";
	public static final String DO = SaplConstants.SAPL_NS + "do";
	public static final String configuredAs = SaplConstants.SAPL_NS + "configuredAs";
	public static final String NULL = SaplConstants.SAPL_NS + "null";
	public static final String Start = SaplConstants.SAPL_NS + "Start";
	public static final String End = SaplConstants.SAPL_NS + "End";
	public static final String Success = SaplConstants.SAPL_NS + "Success";
	public static final String Fail = SaplConstants.SAPL_NS + "Fail";

	public static final String Rule = SaplConstants.SAPL_NS + "Rule";
	public static final String MetaRuleLevel = SaplConstants.SAPL_NS + "MetaRuleLevel";

	public static final String implies = SaplConstants.SAPL_NS + "implies"; // =>
	public static final String impliesNow = SaplConstants.SAPL_NS + "impliesNow"; // ->
	public static final String achievedBy = SaplConstants.SAPL_NS + "achievedBy"; // >>
	public static final String infers = SaplConstants.SAPL_NS + "infers"; // ==>
	public static final String ELSE = SaplConstants.SAPL_NS + "else";
	
	public static final String hasMember = SaplConstants.SAPL_NS + "hasMember";
	public static final String memberOf = SaplConstants.SAPL_NS + "memberOf";
	public static final String doNotBelieve = SaplConstants.SAPL_NS + "doNotBelieve";
	public static final String or = SaplConstants.SAPL_NS + "or";
	public static final String and = SaplConstants.SAPL_NS + "and";
	public static final String Optional = SaplConstants.SAPL_NS + "Optional";

	public static final String FALSE = SaplConstants.SAPL_NS + "false";
	public static final String TRUE = SaplConstants.SAPL_NS + "true";

	//new is 4.6
	public static final String verbatim = SaplConstants.SAPL_NS + "verbatim";
	
	public static final String is = SaplConstants.SAPL_NS + "is";
	public static final String ID = SaplConstants.SAPL_NS + "ID";
	
	public static final String All = SaplConstants.SAPL_NS + "All";
	public static final String Some = SaplConstants.SAPL_NS + "Some";
	public static final String OrderBy = SaplConstants.SAPL_NS + "OrderBy";
	public static final String OrderByDescending = SaplConstants.SAPL_NS + "OrderByDescending";
	public static final String Limit = SaplConstants.SAPL_NS + "Limit";
	public static final String Offset = SaplConstants.SAPL_NS + "Offset";
	
	public static final String gt = SaplConstants.SAPL_NS + "gt";
	public static final String lt = SaplConstants.SAPL_NS + "lt";
	public static final String gte = SaplConstants.SAPL_NS + "gte";
	public static final String lte = SaplConstants.SAPL_NS + "lte";
	public static final String eq = SaplConstants.SAPL_NS + "eq";
	public static final String neq = SaplConstants.SAPL_NS + "neq";
	public static final String regex = SaplConstants.SAPL_NS + "regex";
	public static final String expression = SaplConstants.SAPL_NS + "expression";
	
	public static final String count = SaplConstants.SAPL_NS + "count";
	public static final String groupedBy = SaplConstants.SAPL_NS + "groupedBy";
	public static final String distinctBy = SaplConstants.SAPL_NS + "distinctBy";
	public static final String max = SaplConstants.SAPL_NS + "max";
	public static final String min = SaplConstants.SAPL_NS + "min";
	public static final String sum = SaplConstants.SAPL_NS + "sum";
	public static final String avg = SaplConstants.SAPL_NS + "avg";

	public static final String Variable = SaplConstants.SAPL_NS + "Variable";
	public static final String Context = SaplConstants.SAPL_NS + "Context";
	public static final String Now = SaplConstants.SAPL_NS + "Now";
	public static final String Time = SaplConstants.SAPL_NS + "Time";
	public static final String Date = SaplConstants.SAPL_NS + "Date";

	public static final String Input = SaplConstants.SAPL_NS + "Input";

	public static final String occured = SaplConstants.SAPL_NS + "occured";

	public static final String want = SaplConstants.SAPL_NS + "want";
	public static final String doing = SaplConstants.SAPL_NS + "doing";
	public static final String achieving = SaplConstants.SAPL_NS + "achieving";
	public static final String missingRAB = SaplConstants.SAPL_NS + "missingRAB";
	public static final String executionID = SaplConstants.SAPL_NS + "executionID";

	//public static final String missingModel = SaplConstants.SAPL_NS + "missingModel";

	
	//All concepts, the list is targeted to IDE
	public final static Set<String> concepts = SaplConstants.create_concepts();
	private static Set<String> create_concepts() {
		Set<String> words = new HashSet<String>();
		words.add(SaplConstants.G);
		words.add(SaplConstants.I);
		words.add(SaplConstants.remove);
		words.add(SaplConstants.add);
		words.add(SaplConstants.erase);
		words.add(SaplConstants.DO);
		words.add(SaplConstants.configuredAs);
		words.add(SaplConstants.NULL);
		words.add(SaplConstants.Start);
		words.add(SaplConstants.End);
		words.add(SaplConstants.Fail);
		words.add(SaplConstants.Success);
		words.add(SaplConstants.Rule);
		words.add(SaplConstants.MetaRuleLevel);
		words.add(SaplConstants.implies);
		words.add(SaplConstants.impliesNow);
		words.add(SaplConstants.achievedBy);
		words.add(SaplConstants.infers);
		words.add(SaplConstants.ELSE);
		words.add(SaplConstants.hasMember);
		words.add(SaplConstants.memberOf);
		words.add(SaplConstants.doNotBelieve);
		words.add(SaplConstants.or);
		words.add(SaplConstants.and);
		words.add(SaplConstants.Optional);
		words.add(SaplConstants.FALSE);
		words.add(SaplConstants.TRUE);
		words.add(SaplConstants.verbatim);
		words.add(SaplConstants.is);
		words.add(SaplConstants.ID);
		words.add(SaplConstants.All);
		words.add(SaplConstants.Some);
		words.add(SaplConstants.OrderBy);
		words.add(SaplConstants.OrderByDescending);
		words.add(SaplConstants.Limit);
		words.add(SaplConstants.Offset);
		words.add(SaplConstants.gt);
		words.add(SaplConstants.lt);
		words.add(SaplConstants.gte);
		words.add(SaplConstants.lte);
		words.add(SaplConstants.eq);
		words.add(SaplConstants.neq);
		words.add(SaplConstants.regex);
		words.add(SaplConstants.expression);
		words.add(SaplConstants.count);
		words.add(SaplConstants.groupedBy);
		words.add(SaplConstants.distinctBy);
		words.add(SaplConstants.max);
		words.add(SaplConstants.min);
		words.add(SaplConstants.sum);
		words.add(SaplConstants.avg);
		words.add(SaplConstants.Variable);
		words.add(SaplConstants.Context);
		words.add(SaplConstants.Now);
		words.add(SaplConstants.Time);
		words.add(SaplConstants.Date);
		words.add(SaplConstants.Input);
		words.add(SaplConstants.occured);
		words.add(SaplConstants.want);
		words.add(SaplConstants.doing);
		words.add(SaplConstants.achieving);
		words.add(SaplConstants.missingRAB);
		words.add(SaplConstants.executionID);
		return Collections.unmodifiableSet(words);
	}
	
	public final static Set<String> set_predicates = SaplConstants.create_set_predicates();
	private static Set<String> create_set_predicates() {
		//solution set modifiers that must have a variable as object
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.All);
		predicates.add(SaplConstants.Some);
		predicates.add(SaplConstants.OrderBy);
		predicates.add(SaplConstants.OrderByDescending);
		return Collections.unmodifiableSet(predicates);
	}

	public final static Set<String> set2_predicates = SaplConstants.create_set2_predicates();
	private static Set<String> create_set2_predicates() {
		//other solution set modifiers
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.Limit);
		predicates.add(SaplConstants.Offset);
		return Collections.unmodifiableSet(predicates);
	}

	public final static Set<String> context_predicates = SaplConstants.create_context_predicates();
	private static Set<String> create_context_predicates() {
		//the contexts containers of those must not be automatically joined
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.remove);
		predicates.add(SaplConstants.erase);
		predicates.add(SaplConstants.doNotBelieve);
		predicates.add(SaplConstants.hasMember);
		predicates.add(SaplConstants.memberOf);
		predicates.add(SaplConstants.configuredAs);
		predicates.add(SaplConstants.groupedBy);
		predicates.add(SaplConstants.distinctBy);
		return Collections.unmodifiableSet(predicates);
	}

	public static final Set<String> inference_predicates = SaplConstants.create_inference_predicates();
	private static Set<String> create_inference_predicates() {
		//inference predicates
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.implies); // =>
		predicates.add(SaplConstants.impliesNow); // ->
		predicates.add(SaplConstants.achievedBy); // >>
		predicates.add(SaplConstants.infers); // ==>
		return Collections.unmodifiableSet(predicates);
	}	

	public static final Set<String> embedded_predicates = SaplConstants.create_embedded_predicates();
	private static Set<String> create_embedded_predicates() {
		//statements with these are processed by the engine rather than matched against beliefs
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.gt);
		predicates.add(SaplConstants.lt);
		predicates.add(SaplConstants.gte);
		predicates.add(SaplConstants.lte);
		predicates.add(SaplConstants.eq);
		predicates.add(SaplConstants.neq);
		predicates.add(SaplConstants.regex);
		predicates.add(SaplConstants.expression);
		predicates.add(SaplConstants.count);
		predicates.add(SaplConstants.max);
		predicates.add(SaplConstants.min);
		predicates.add(SaplConstants.sum);
		predicates.add(SaplConstants.avg);
		predicates.add(SaplConstants.groupedBy);
		predicates.add(SaplConstants.distinctBy);
		return Collections.unmodifiableSet(predicates);
	}

	public static final Set<String> log_predicates = SaplConstants.create_log_predicates();
	private static Set<String> create_log_predicates() {
		//logical comparison predicates
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.gt);
		predicates.add(SaplConstants.lt);
		predicates.add(SaplConstants.gte);
		predicates.add(SaplConstants.lte);
		predicates.add(SaplConstants.eq);
		predicates.add(SaplConstants.neq);
		predicates.add(SaplConstants.regex);
		return Collections.unmodifiableSet(predicates);
	}

	public static final Set<String> aggregate_predicates = SaplConstants.create_aggregate_predicates();
	private static Set<String> create_aggregate_predicates() {
		//aggregate predicates
		Set<String> predicates = new HashSet<String>();
		predicates.add(SaplConstants.count);
		predicates.add(SaplConstants.max);
		predicates.add(SaplConstants.min);
		predicates.add(SaplConstants.sum);
		predicates.add(SaplConstants.avg);
		predicates.add(SaplConstants.groupedBy);
		predicates.add(SaplConstants.distinctBy);
		return Collections.unmodifiableSet(predicates);
	}
	
	
	public final static Set<String> protected_entities = SaplConstants.create_protected_entities();
	private static Set<String> create_protected_entities() {
		//those contexts and beliefs should not be removed from a S-APL model, neither by garbage collector nor explicitly
		Set<String> entities = new HashSet<String>();
		entities.add(SaplConstants.GENERAL_CONTEXT);
		entities.add(SaplConstants.RULES_CONTEXT);
		entities.add(SaplConstants.GOALS_CONTEXT);
		entities.add(SaplConstants.ACHIEVE_CONTEXT);
		entities.add(SaplConstants.DO_CONTEXT);
		entities.add(SaplConstants.FALSE_CONTEXT);
		entities.add(SaplConstants.RULES_BELIEF);
		entities.add(SaplConstants.GOALS_BELIEF);
		entities.add(SaplConstants.DO_BELIEF);
		entities.add(SaplConstants.DO_CONTEXT);
		entities.add(SaplConstants.FALSE_BELIEF);
		entities.add(SaplConstants.NOW_BELIEF);
		entities.add(SaplConstants.TIME_BELIEF);
		entities.add(SaplConstants.DATE_BELIEF);
		entities.add(SaplConstants.INPUT_BELIEF);
		return Collections.unmodifiableSet(entities);
	}

}
