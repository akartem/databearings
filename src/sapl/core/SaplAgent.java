package sapl.core;

import java.util.List;
import java.util.Vector;

import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Resource;
import sapl.util.URITools;

/** 
 * Middleware-free implementation of the agent using SaplReasoner.  
 * An entity acting according to an S-APL program and a set of Reusable Atomic Behaviors
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.6 (26.06.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class SaplAgent implements Runnable{

	public static boolean DEBUG_PRINT = false;
	public static boolean SLEEP_PRINT = true;
	public static boolean STOP_PRINT = true;
	public static boolean MEMORY_PRINT = false;
	public static boolean DO_GC_MEMORY_PRINT = true;
	
	//static long maxMemory = 0;

	/**Currently running behaviors*/
	protected Vector<ReusableAtomicBehaviorBasicWrapper> behaviors = new Vector<ReusableAtomicBehaviorBasicWrapper>();

	/**Currently executed behavior*/
	protected ReusableAtomicBehaviorBasicWrapper currentBehavior = null;

	/**S-APL Reasoner behavior*/
	protected ReusableAtomicBehaviorBasicWrapper reasonerWrapper;

	/**Flag for agent shutdown from an external thread calling stop()*/
	protected boolean running = true;
	
	protected Thread myThread;

	/**Constructor*/
	public SaplAgent(String name, String[] scripts, boolean withResult, String input){
		
		SaplReasoner reasoner = new SaplReasoner(name, withResult);
		BehaviorStartParameters params = new BehaviorStartParameters();

		if(scripts!=null){
			for(String script : scripts){
				params.add(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "script"), script);
			}
		}
		
		if(input!=null) 
			params.add(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"), input);
			
		reasoner.setStartParameters(params);

		this.reasonerWrapper = this.addBehavior(reasoner);
	}

	/**Constructor*/
	public SaplAgent(String name, String[] scripts){
		this(name, scripts, false, null);
	}
	
	/**Empty constructor, for subclasses overriding the normal constructor*/
	public SaplAgent(){
	}

	public void start(){
		final SaplAgent f_agent = this;
		Runtime.getRuntime().addShutdownHook(new Thread() {
	        public void run() {
	            try {
	            	f_agent.stop();
	            	myThread.join();
	            	Thread.sleep(1000);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	        }
	    });		
		
		myThread = new Thread(this);
		myThread.start();
	}

	public void stop(){
    	if(STOP_PRINT)
    		print("Shutting down..");
		this.running = false;
		synchronized (reasonerWrapper) {
			reasonerWrapper.notify();
		}
	}

	/**Main cycle*/
	@Override
	public void run(){
		
		while(this.running){
			this.currentBehavior = null;
			ReusableAtomicBehaviorBasicWrapper wrapper = this.getNextBehavior(0);
			while(this.running){
				if(wrapper!=null){
					ReusableAtomicBehavior behavior = wrapper.getBehavior();
					this.doBehavior(behavior);
					if(behavior.done() || !behavior.hasSucceededTillNow()){
						behavior.onEnd();
						ReusableAtomicBehaviorBasicWrapper next = this.getNextBehavior(0);
						this.removeBehavior(wrapper);
						if(next==wrapper) //this was the only non-blocked behavior
							break;
						wrapper = next;
					}
					else wrapper = this.getNextBehavior(0);
				}
				else{
					break;
				}
				
			}
			
			if(this.running){
				long timeout = -1;
				long until = reasonerWrapper.getBehavior().getBlockedUntil();
				if(until!=-1){
					timeout = until - System.currentTimeMillis();
					if(timeout <= 0 ) continue; //start new cycle right sway 
				}

				synchronized (reasonerWrapper) {
					int numberOfNotFinishedBehaviors = 0;
					for(ReusableAtomicBehaviorBasicWrapper b : this.behaviors){
						if(!b.getBehavior().done()) numberOfNotFinishedBehaviors++;
					}

					//check if have a threaded RAB that managed to finish
					if(numberOfNotFinishedBehaviors != this.behaviors.size())
						continue;

					//Going to sleep
					if(DEBUG_PRINT)
						((SaplReasoner)reasonerWrapper.getBehavior()).printModel(false, false);

					try {
						if(MEMORY_PRINT){
							SaplModel m = ((SaplReasoner)reasonerWrapper.getBehavior()).saplModel;
							String db = "(Contexts: "+m.contexts.size()+", Statements: "+m.beliefs.size()+")";							
							printMemory(db, DO_GC_MEMORY_PRINT, "");
						}

						if(timeout>0){
							String extra = "";
							if(numberOfNotFinishedBehaviors > 1) extra = " or until an external stimulus";
							if(SLEEP_PRINT)
								print("Agent goes to sleep for "+timeout+" ms"+extra+"..");
							reasonerWrapper.wait(timeout);
							if(System.currentTimeMillis() >= until) // otherwise, a RAB restarted itself 
								reasonerWrapper.getBehavior().restart();
						}
						else{
							if(numberOfNotFinishedBehaviors > 1) {
								if(SLEEP_PRINT)
									print("Agent goes to sleep until an external stimulus..");
								
								reasonerWrapper.wait();
							}
							else break; // Reasoner is the only behavior left and it is blocked - nothing can happen anymore - so, exit
						}

					} catch (InterruptedException e) {
						print("Interrupted");
					}
				}
			}
		}
		//stop threaded behaviors
		for(ReusableAtomicBehaviorWrapper behavior : this.behaviors){
			ReusableAtomicBehavior rab = ((ReusableAtomicBehaviorBasicWrapper)behavior).getBehavior();
			if(!rab.done()){
				if(STOP_PRINT)		
					print ("Stopping "+rab.getClass().getName());
				rab.stop();
			}
		}

		if(STOP_PRINT)		
			print("Agent exits .");
		
//		ReusableAtomicBehavior rab = new ReusableAtomicBehavior() {
//			@Override
//			protected void initializeBehavior(BehaviorStartParameters parameters)
//					throws IllegalParameterConfigurationException {
//			}
//			@Override
//			protected void doAction() throws Exception {
//			}
//		};
//		rab.setSaplActionEngine(((SaplReasoner)this.reasonerWrapper.getBehavior()).saplActionEngine);
		
	}

	/**Adds a behavior*/
	protected ReusableAtomicBehaviorBasicWrapper addBehavior(ReusableAtomicBehavior behavior){
		ReusableAtomicBehaviorBasicWrapper rabWrapper = new ReusableAtomicBehaviorBasicWrapper(behavior, this);

		if(this.behaviors.isEmpty() || this.currentBehavior==null) this.behaviors.add(rabWrapper);
		else{
			this.behaviors.insertElementAt(rabWrapper, this.behaviors.indexOf(this.currentBehavior));
		}

		return rabWrapper;
	}

	/**Removes a behavior*/	
	protected void removeBehavior(ReusableAtomicBehaviorBasicWrapper wrapper){
		this.behaviors.remove(wrapper);
	}

	/**Gets next non-blocked behavior in the execution queue.
	 * Returns null if all behaviors are blocked
	 * @param passed Number of blocked behaviors already searched through*/
	protected ReusableAtomicBehaviorBasicWrapper getNextBehavior(int passed){
		if(behaviors.isEmpty()){ //cannot really happen as reasoner is always there
			this.currentBehavior = null;
			return null;
		}
		else if(this.currentBehavior==null){
			this.currentBehavior = this.reasonerWrapper;
		}
		else{
			int ind = this.behaviors.indexOf(this.currentBehavior) + 1;
			if(ind == behaviors.size()) ind = 0;
			this.currentBehavior = this.behaviors.elementAt(ind);
		}

		if(this.currentBehavior.getBehavior().isBlocked()){
			if(passed == this.behaviors.size()) return null; 
			return this.getNextBehavior(passed+1);
		}
		else return this.currentBehavior;
	}

	/**Performs one iteration of the given behavior*/
	protected void doBehavior(ReusableAtomicBehavior behavior){
		if(behavior.isBlocked()) return;

		if(!behavior.hasBeenExecuted()) behavior.onStart();
		behavior.action();

		if(behavior instanceof SaplReasoner){
			List<ReusableAtomicBehavior> toExecute = ((SaplReasoner)behavior).toExecute;
			if(toExecute!=null){
				for(ReusableAtomicBehavior rab : toExecute){
					this.addBehavior(rab);
				}
			}
		}
	}

	protected void print(Object text) {
		System.out.println("["+SaplModel.getCurrentTime()+"] "+text);
	}

	protected void printMemory(String prefix, boolean doGC, String postfix){
		Runtime r = Runtime.getRuntime();
		if(doGC) r.gc();
		print(prefix+(prefix.isEmpty()?"":" ")+"Memory Used: "+(r.totalMemory()-r.freeMemory())+", Free: "+r.freeMemory()+", Total: "+r.totalMemory()+", Max: "+r.maxMemory()+" "+postfix);

//		long used = r.totalMemory()-r.freeMemory();
//		if(used>maxMemory){
//			maxMemory = used;
//			print("NEW MAX: "+maxMemory);
//		}
		
	}
	
	public String getResult(){
		SaplReasoner reasoner = (SaplReasoner)reasonerWrapper.getBehavior();
		if(reasoner.result==null) return null;
		else{
			String result = reasoner.result.toString();
			if(result.endsWith("\n")) result = result.substring(0, result.length()-1);
			return result;
		}
	}
	
}
