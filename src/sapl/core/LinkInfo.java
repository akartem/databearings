package sapl.core;

/** 
 * A structure describing a link from a ContextContainer to a SemanticStatement.
 * Used in S-APL model update operations. 
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.0 (02.05.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2007-2009 Artem Katasonov, Michael Cochez
 * 
 * */

public class LinkInfo {
	/**ID of context of the link*/
	public String contextID;

	/**ID of statement of the link*/
	public String statementID;

	/**Constructor
	 * @param contextID Context
	 * @param statementID Statement
	 * */
	public LinkInfo(String contextID, String statementID) {
		this.contextID = contextID;
		this.statementID = statementID;
	}

	/** Returns simple string representation for the link */ 
	@Override
	public String toString() {
		return this.contextID + "->" + this.statementID;
	}

	/** Returns hash for the link */ 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.contextID == null) ? 0 : this.contextID.hashCode());
		result = (prime * result) + ((this.statementID == null) ? 0 : this.statementID.hashCode());
		return result;
	}

	/** Compares this link to another link
	 * @return true if links are from the same context to the same statement  
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof LinkInfo)) {
			return false;
		}
		LinkInfo other = (LinkInfo) obj;
		if (this.contextID == null) {
			if (other.contextID != null) {
				return false;
			}
		} else if (!this.contextID.equals(other.contextID)) {
			return false;
		}
		if (this.statementID == null) {
			if (other.statementID != null) {
				return false;
			}
		} else if (!this.statementID.equals(other.statementID)) {
			return false;
		}
		return true;
	}

}
