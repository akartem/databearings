package sapl.core;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * The top-level context container
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.2 (05.12.2014)
 * 
 * @since 4.2
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class GeneralContext extends ContextContainer {

	@Override
	void addToIndex(String key, String statementID, Map<String, Set<String>> index) {
		Set<String> hs = index.get(key);
		if (hs == null) {
			if(key.equals(SaplConstants.configuredAs)){
				hs = new LinkedHashSet<String>(4);
			}
			else hs = new HashSet<String>(1);
			hs.add(statementID);
			index.put(key, hs);
		} else {
			hs.add(statementID);
		}
	}
}
