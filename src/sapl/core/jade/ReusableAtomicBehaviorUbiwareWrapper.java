package sapl.core.jade;

import java.util.List;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.ReusableAtomicBehaviorWrapper;
import sapl.core.SaplReasoner;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Wrapper of a Reusable Atomic Behavior as a JADE Behaviour.
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.1 (22.01.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2009, Michael Cochez
 * 
 * */

public class ReusableAtomicBehaviorUbiwareWrapper extends Behaviour implements ReusableAtomicBehaviorWrapper{

	private static final long serialVersionUID = -7788268991787214177L;

	/**ReusableAtomicBehavior being wrapped*/
	private ReusableAtomicBehavior behavior;

	/**UbiwareAgent owning this behavior*/
	private UbiwareAgent myAgent;

	/**
	 * Create a wrapper for the specified behavior.
	 * @param behavior
	 */
	public ReusableAtomicBehaviorUbiwareWrapper(ReusableAtomicBehavior behavior, UbiwareAgent agent) {
		this.behavior = behavior;
		this.myAgent = agent;
		this.behavior.setWrapper(this);
	}

	@Override
	public void action() {
		this.behavior.action();
		if(behavior instanceof SaplReasoner){
			List<ReusableAtomicBehavior> toExecute = ((SaplReasoner)behavior).toExecute;
			if(toExecute!=null){
				for(ReusableAtomicBehavior rab : toExecute){
					myAgent.addReusableAtomicBehavior(rab);
				}
			}
		}
		if(this.behavior.isBlocked()){
			long until = this.behavior.getBlockedUntil();
			if(until==-1) this.block();
			else{
				long now = System.currentTimeMillis();
				if(until-now > 0)
					this.block(until - now);
				else this.block();
			}
		}
	}

	@Override
	public boolean done() {
		return this.behavior.done();
	}

	@Override
	public void onStart() {
		this.behavior.onStart();
	}

	@Override
	public int onEnd() {
		return this.behavior.onEnd();
	}

	@Override
	public void restart(){
		this.behavior.restart();
		super.restart();
	}

	/**Used by RAB to notify about the need to restart the wrapper*/
	@Override
	public void notifyRestart() {
		super.restart();
	}
	
	/**Send an ACL message to another agent*/
	public void sendAclMessage(ACLMessage message){
		this.myAgent.send(message); 
	}

	/**Receives an ACL message to another agent*/
	public ACLMessage receiveAclMessage(MessageTemplate mt) {
		return this.myAgent.receive();
	}

}