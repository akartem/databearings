package sapl.core.jade;

import sapl.core.ReusableAtomicBehavior;
import sapl.core.SaplConstants;
import sapl.core.SaplReasoner;
import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.Resource;
import sapl.util.URITools;
import jade.core.Agent;

/**
 * S-APL agent for JADE framework
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.3 (06.10.2015)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2015, VTT
 * 
 * */

public class UbiwareAgent extends Agent{

	private static final long serialVersionUID = 3014304199749968461L;

	/**S-APL Reasoner behavior*/
	public SaplReasoner reasoner;

	@Override
	protected void setup() {
		this.reasoner = new SaplReasoner(this.getName()); 
		BehaviorStartParameters params = new BehaviorStartParameters();

		Object[] scripts = getArguments();

		for(String script : (String[])scripts){
			params.add(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "script"), script);
		}
		reasoner.setStartParameters(params);
		this.addReusableAtomicBehavior(this.reasoner);
	}

	public void addReusableAtomicBehavior(ReusableAtomicBehavior rab) {
		ReusableAtomicBehaviorUbiwareWrapper rabWrapper = new ReusableAtomicBehaviorUbiwareWrapper(rab, this);
		this.addBehaviour(rabWrapper);
	}	

}
