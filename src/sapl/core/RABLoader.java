package sapl.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Class loader to dynamically load {@link ReusableAtomicBehavior} classes.
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.1 (23.09.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2009 Michael Cochez
 * 
 * */
public class RABLoader extends ClassLoader {
	public final static Map<String, String> embeddedActions = RABLoader.generateEmbeddedActionsMap();

	private static Map<String, String> generateEmbeddedActionsMap() {
		Map<String, String> theMap = new HashMap<String, String>();
		theMap.put(SaplConstants.EMPTY_NS+"Print", "sapl.shared.PrintBehavior");
		//here other embedded actions can be added.
		return Collections.unmodifiableMap(theMap);
	}

	public RABLoader(ClassLoader parent) {
		super(parent);
	}
	
	//This cast is fine since it is checked trough reflection that the class is of that type.
	@SuppressWarnings("unchecked")
	@Override
	protected Class<? extends ReusableAtomicBehavior> findClass(String actionName) throws ClassNotFoundException {
		String className = RABLoader.embeddedActions.get(actionName);
		if (className == null) {
			if (actionName.startsWith(SaplConstants.EMPTY_NS)) {
				throw new ClassNotFoundException("The embedded action '" + actionName + "' does not exist");
			}
			//it is not an embedded action => continue searching
			int ind = actionName.indexOf("#");
			if (ind == -1) {
				ind = actionName.lastIndexOf("/");
			}
			className = actionName.substring(ind + 1);

		}

		Class<?> theClass = Class.forName(className);

		Class<?> superClass = theClass.getSuperclass();
		
		while (!ReusableAtomicBehavior.class.equals(superClass)) {
			if (superClass == null) {
				//the superclass is object or interface or ... but not an RAB
				throw new ClassNotFoundException("The classes loaded by RABLoader must be derived from sapl.core.ReusableAtomicBehavior");
			}
			superClass = superClass.getSuperclass();
		}
		return (Class<? extends ReusableAtomicBehavior>) theClass;
	}

	//This cast is fine since only RAB's can be loaded with this
	@SuppressWarnings("unchecked")
	@Override
	public Class<ReusableAtomicBehavior> loadClass(String name) throws ClassNotFoundException {
		return (Class<ReusableAtomicBehavior>) super.loadClass(name);
	}
}
