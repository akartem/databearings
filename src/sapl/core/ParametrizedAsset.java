package sapl.core;

import sapl.core.rab.BehaviorStartParameters;

/**
 * Indicates that the object can get parameters.
 * 
 * @author Artem Katasonov (University of Jyväskylä)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 * 
 * Copyright (C) 2007-2009 Artem Katasonov, Michael Cochez
 * 
 * */

public interface ParametrizedAsset {
	/**
	 * Sets the parameters on the object.
	 * @param startParams Parameters.
	 */
	public void setStartParameters(BehaviorStartParameters startParams);
}
