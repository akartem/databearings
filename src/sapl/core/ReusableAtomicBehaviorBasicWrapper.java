package sapl.core;

/** 
 * Basic wrapper of ReusableAtomicBehavior for middleware-free SaplAgent implementation
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (21.06.2017)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2017, VTT
 * 
 * */

public class ReusableAtomicBehaviorBasicWrapper implements ReusableAtomicBehaviorWrapper{

	/**ReusableAtomicBehavior being wrapped*/
	private ReusableAtomicBehavior behavior;

	/**UbiwareAgent owning this behavior*/
	private SaplAgent myAgent;

	/**
	 * Create a wrapper for the specified behavior.
	 */
	public ReusableAtomicBehaviorBasicWrapper(ReusableAtomicBehavior behavior, SaplAgent agent) {
		this.behavior = behavior;
		this.myAgent = agent;
		this.behavior.setWrapper(this);
	}

	/**Returns the behavior wrapped*/
	public ReusableAtomicBehavior getBehavior(){
		return this.behavior;
	} 

	/**Used by RAB to notify about the need to wake the agent, to let the RAB to continue work*/
	@Override
	public void notifyRestart() {
		synchronized (myAgent.reasonerWrapper) {
			myAgent.reasonerWrapper.notify();
		}
	}

}
