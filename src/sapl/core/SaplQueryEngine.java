package sapl.core;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.List;

import sapl.core.sapln3.SaplN3Parser;
import sapl.util.IDGenerator;
import sapl.util.SetOperation;

/** 
 * Engine for querying an S-APL model
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.6 (26.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov
 * 
 * */

public class SaplQueryEngine {

	public static boolean DEBUG_PRINT = false;

	private static IDGenerator idgenerator = new IDGenerator("query");

	
	/**Is a static class. Constructor disabled*/
	private SaplQueryEngine() {}

	/**Prints an error message*/
	private static void printErr(String string) {
		System.err.println("["+SaplModel.getCurrentTime()+"] "+string);
	}

	/**Evaluates a single query statement against a given context in the S-APL model
	 * @param queryId Unique ID of the query being evaluated
	 * @param st Query statement
	 * @param targetContextID Target context in the S-APL model
	 * @param targetModel S-APL model
	 * @param resultSet Current set of solutions to the overall query, which the method will start with and modify
	 * @param removeMode Whether the query is run as "remove by pattern" operation - to record the links that would be removed 
	 * @param id_vars Current list of variables used to match statement IDs
	 * @param blockingTimeMoments Current list for remembering future time moments used in the query as a filter blocking the query from gaining results
	 * @return True if any solution to the overall query is still possible
	 * */
	private static boolean evaluateStatement(String queryId, SemanticStatement st, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants, 
			SaplQueryResultSet resultSet, boolean removeMode, List<String> id_vars, List<String> time_vars, List<Long> blockingTimeMoments) {

		long start = 0;
		if(DEBUG_PRINT) start = System.currentTimeMillis();
		if(DEBUG_PRINT){
			if(st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.TRUE))
				System.out.println("  CHECKING: "+st);
			else System.out.print(st+" vs. "+targetContextID);
		}
		
		final List<Map<String, String>> newSolutions = new ArrayList<Map<String, String>>();
		boolean any = false; //any solutions remain?

		Iterator<Map<String, String>> it;

		if (resultSet.solutions.size() > 0) {
			it = resultSet.solutions.iterator();
		} else {
			List<Map<String, String>> tv = new ArrayList<Map<String, String>>();
			tv.add(new HashMap<String, String>());
			it = tv.iterator();
		}

		while (it.hasNext()) {
			Map<String, String> solution = it.next();
			String solutionID = solution.get("");
			if (solutionID == null) {
				solutionID = "";
			}

			boolean survive = false; //this solution stays?
			boolean extended = false; //this solution is extended with new variables?

			List<Map<String, String>> newSubSolutions = null;
			Map<String, String> index_newSubSolutions = null;

			SemanticStatement st_copy = new SemanticStatement(st);
			boolean changed[] = st_copy.bindVars(solution);
			boolean subject_changed = changed[0];
			boolean predicate_changed = changed[1];
			boolean object_changed = changed[2];
			
			//check if the statement refers to a variable bound as optional, and this solution does not have it
//			if (st_copy.hasVariables()) {
//				if (!subject_changed) {
//					String var = st_copy.getSubjectVariableName();
//					if (var!=null && resultSet.definedVars.contains(var)) {
//						continue;
//					}
//				}
//				if (!predicate_changed) {
//					String var = st_copy.getPredicateVariableName();
//					if (var!=null && resultSet.definedVars.contains(var)) {
//						continue;
//					}
//				}
//				if (!object_changed) {
//					String var = st_copy.getObjectVariableName();
//					if (var!=null && resultSet.definedVars.contains(var)) {
//						continue;
//					}
//				}
//			}

			//check rdf:subject, rdf:predicate and rdf:object conditions
			if (st_copy.predicate.equals(SaplConstants.rdfSubject)) {
				if (st_copy.subject.startsWith(SaplConstants.STATEMENT_PREFIX)) {
					if (!st_copy.isObjectVar()) {
						if (targetModel.beliefs.get(st_copy.subject).subject.equals(st_copy.object)) {
							newSolutions.add(solution);
							any = true;
							continue;
						}
					} else {
						solution.put(st_copy.getObjectVariableName(), targetModel.beliefs.get(st_copy.subject).subject);
						newSolutions.add(solution);
						any = true;
						continue;
					}
					if (removeMode) {
						resultSet.forRemove.remove(solutionID);
					}
					continue;
				} else if (st_copy.isSubjectVar()) {
					id_vars.add(st_copy.getSubjectVariableName());
					st_copy = new SemanticStatement(st_copy.object, SaplConstants.ANYTHING, SaplConstants.ANYTHING);
				}
			} else if (st_copy.predicate.equals(SaplConstants.rdfPredicate)) {
				if (st_copy.subject.startsWith(SaplConstants.STATEMENT_PREFIX)) {
					if (!st_copy.isObjectVar()) {
						if (targetModel.beliefs.get(st_copy.subject).predicate.equals(st_copy.object)) {
							newSolutions.add(solution);
							any = true;
							continue;
						}
					} else {
						solution.put(st_copy.getObjectVariableName(), targetModel.beliefs.get(st_copy.subject).predicate);
						newSolutions.add(solution);
						any = true;
						continue;
					}
					if (removeMode) {
						resultSet.forRemove.remove(solutionID);
					}
					continue;
				} else if (st_copy.isSubjectVar()) {
					id_vars.add(st_copy.getSubjectVariableName());
					st_copy = new SemanticStatement(SaplConstants.ANYTHING, st_copy.object, SaplConstants.ANYTHING);
				}
			} else if (st_copy.predicate.equals(SaplConstants.rdfObject)) {
				if (st_copy.subject.startsWith(SaplConstants.STATEMENT_PREFIX)) {
					
					System.out.println(st_copy);
					
					if (!st_copy.isObjectVar()) {
						if (targetModel.beliefs.get(st_copy.subject).object.equals(st_copy.object)) {
							newSolutions.add(solution);
							any = true;
							continue;
						}
					} else {
						solution.put(st_copy.getObjectVariableName(), targetModel.beliefs.get(st_copy.subject).object);
						newSolutions.add(solution);
						any = true;
						continue;
					}
					if (removeMode) {
						resultSet.forRemove.remove(solutionID);
					}
					continue;
				} else if (st_copy.isSubjectVar()) {
					id_vars.add(st_copy.getSubjectVariableName());
					st_copy = new SemanticStatement(SaplConstants.ANYTHING, SaplConstants.ANYTHING, st_copy.object);
				}
			}

			//unwrap statements from "{} is true"
			else if (st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.TRUE)) {

				if (st_copy.isSubjectContext() || st_copy.subject.startsWith(SaplConstants.STATEMENT_PREFIX)) {
					List<Map<String, String>> vec = new ArrayList<Map<String, String>>();
					vec.add(solution);
					SaplQueryResultSet temp_resultSet = new SaplQueryResultSet(vec, resultSet.definedVars);

					if (SaplQueryEngine.evaluateContext(queryId, st_copy.subject, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, temp_resultSet, removeMode, id_vars, time_vars, blockingTimeMoments)) { //super-recursion
						any = true;
						newSolutions.addAll(temp_resultSet.solutions);
						resultSet.definedVars.addAll(temp_resultSet.definedVars);
						continue;
					}
					if (removeMode) {
						temp_resultSet.forRemove.remove(solutionID);
					}
					continue; //next solution
				} else if (st_copy.isSubjectVar()) { //unbound yet variable -> link to IDs of all statements in container
					id_vars.add(st_copy.getSubjectVariableName());
					st_copy = new SemanticStatement(SaplConstants.ANYTHING, SaplConstants.ANYTHING, SaplConstants.ANYTHING);
				}
			}

			String bindContextID = null;
			if ((targetContextID.length() > 1) && targetContextID.startsWith(SaplConstants.VARIABLE_PREFIX)) {
				String value = solution.get(targetContextID.substring(SaplConstants.VARIABLE_PREFIX.length()));
				if (value != null) {
					bindContextID = value;
				}
			} else {
				bindContextID = targetContextID;
			}

			if (bindContextID == null) {
				printErr("Error: Wrong query: unbound yet context " + targetContextID);
				resultSet.forRemove.clear();
				resultSet.solutions.clear();
				return false;
			}

			ContextContainer context = targetModel.contexts.get(bindContextID);
			if (context == null) {
				if (removeMode) {
					resultSet.forRemove.remove(solutionID);
				}
				continue; //not a proper solution, not a context - should not normally appear
			}
			
			List<String> members = null;
			boolean indexFiltered = true;

			if (st_copy.isSubjectAny() && st_copy.isPredicateAny() && st_copy.isObjectAny()) {
				members = context.members;
			} else {
				members = new ArrayList<String>();
				boolean ok = true;
				Set<String> sub_set = null;
				Set<String> pred_set = null;
				Set<String> ob_set = null;
				if (!st_copy.isSubjectAny()) {
					sub_set = context.index_sub.get(st_copy.subject);
					if (sub_set == null) {
						ok = false;
					}
				}
				if (ok && !st_copy.isPredicateAny()) {
					pred_set = context.index_pred.get(st_copy.predicate);
					if (pred_set == null) {
						ok = false;
					}
				}
				if (ok && !st_copy.isObjectAny()) {
					ob_set = context.index_ob.get(st_copy.object);
					if (ob_set == null) {
						ok = false;
					}
				}

				if (ok) {
					if ((sub_set != null) && (pred_set == null) && (ob_set == null)) {
						members = new ArrayList<String>(sub_set);
					} else if ((sub_set == null) && (pred_set != null) && (ob_set == null)) {
						members = new ArrayList<String>(pred_set);
					} else if ((sub_set == null) && (pred_set == null) && (ob_set != null)) {
						members = new ArrayList<String>(ob_set);
					} else if (ob_set != null && (sub_set==null || ob_set.size()<sub_set.size())) {
						Set<String> result = SetOperation.intersect3Sets(ob_set, pred_set, sub_set);
						if (result.size() > 0) {
							members = new ArrayList<String>(result);
						}
					} else {
						Set<String> result = SetOperation.intersect3Sets(sub_set, pred_set, ob_set);
						if (result.size() > 0) {
							members = new ArrayList<String>(result);
						}
					}
				}
			}

			int basicCount = members.size();
			
			//In v4.1, symmetric querying made optional
			if(symmetricQuerying){
				if (context.statementsWithVariables != null) {
					for (int i = 0; i < context.statementsWithVariables.size(); i++) {
						String st_id = context.statementsWithVariables.get(i);
						if (!members.contains(st_id)) {
							members.add(st_id);
						}
					}
				}
			}
			
			for (int iStat = 0; iStat < members.size(); iStat++) {

				String id = members.get(iStat);
				SemanticStatement st2 = targetModel.beliefs.get(id);

				//checking equality taking universalities and variables into account
				if ((indexFiltered && (iStat < basicCount)) || st_copy.equalsUniversal(st2)) {

					boolean ok = true;
					Map<String, String> new_solution = new HashMap<String, String>(solution);

					String svar = "", pvar = "", ovar = "";

					if (st_copy.hasVariables()) {
						if (!subject_changed) {
							svar = st_copy.getSubjectVariableName();
							if (svar != null) {
								if (svar.startsWith(SaplConstants.CONTEXT_PREFIX) && !st2.subject.startsWith(SaplConstants.CONTEXT_PREFIX)) {
									ok = false;
								} else {
									String old = new_solution.put(svar, st2.subject);
									if ((old != null) && !old.equals(st2.subject)) {
										ok = false;
									}
								}
							}
						}
						if (!predicate_changed) {
							pvar = st_copy.getPredicateVariableName();
							if (pvar != null) {
								String old = new_solution.put(pvar, st2.predicate);
								if ((old != null) && !old.equals(st2.predicate)) {
									ok = false;
								}
							}
						}
						if (!object_changed) {
							ovar = st_copy.getObjectVariableName();
							if (ovar != null) {
								if (ovar.startsWith(SaplConstants.CONTEXT_PREFIX) && !st2.isObjectContext()) {
									ok = false;
								} else {
									String old = new_solution.put(ovar, st2.object);
									if ((old != null) && !old.equals(st2.object)) {
										ok = false;
									}
								}
							}
						}
					}
					if (ok) {
						//record ID of matching statement if was asked
						if ((id_vars != null) && (id_vars.size() > 0)) {
							for (int i = 0; i < id_vars.size(); i++) {
								String id_var = id_vars.get(i);
								String old = new_solution.get(id_var);
								if (old != null) {
									if (!old.equals(id)) {
										ok = false;
									}
								} else {
									new_solution.put(id_var, id);
								}
							}
						}
						if (ok) {
							String sol_id = solutionID;
							if (new_solution.size() > 0) {
								if (newSubSolutions == null) {
									newSubSolutions = new ArrayList<Map<String, String>>();
									index_newSubSolutions = new HashMap<String, String>();
								}

								new_solution.remove("");

								String tempid = index_newSubSolutions.get(new_solution.toString());

								if (tempid == null) {
									Integer ind = resultSet.solutionIndices.get(solutionID);
									if (ind == null) {
										ind = 1;
									} else {
										ind = ind + 1;
									}
									resultSet.solutionIndices.put(solutionID, ind);
									sol_id = solutionID + "." + ind.toString();

									index_newSubSolutions.put(new_solution.toString(), sol_id);

									new_solution.put("", sol_id);
									newSubSolutions.add(new_solution);
									extended = true;
								} else {
									sol_id = tempid;
								}
							}
							survive = true;
							any = true;

							//remember ID of matching statement for potential use in sapl:remove
							if (removeMode) {
								if (!(st.subject.startsWith(SaplConstants.QUERY_CONTEXT_PREFIX) ^ st.object
										.startsWith(SaplConstants.QUERY_CONTEXT_PREFIX))) {
									if (!solutionID.equals(sol_id) && (resultSet.forRemove.get(sol_id) == null)) {
										Set<LinkInfo> t = resultSet.forRemove.get(solutionID);
										if (t != null) {
											resultSet.forRemove.put(sol_id, new HashSet<LinkInfo>(t));
										}
									}
									Set<LinkInfo> temp = resultSet.forRemove.get(sol_id);
									if (temp != null) {
										temp.add(new LinkInfo(bindContextID, id));
									} else {
										temp = new HashSet<LinkInfo>();
										temp.add(new LinkInfo(bindContextID, id));
										resultSet.forRemove.put(sol_id, temp);
									}
								}
							}
						}
					}

				}//equals

			}//container members cycle

			if (newSubSolutions != null) {
				newSolutions.addAll(newSubSolutions);
			}
			if (removeMode && (!survive || extended)) {
				resultSet.forRemove.remove(solutionID);
			}

		}//solution cycle

		if (st.hasVariables()) {
			String var = st.getSubjectVariableName();
			if (var != null) {
				resultSet.definedVars.add(var);
			}
			var = st.getPredicateVariableName();
			if (var != null) {
				resultSet.definedVars.add(var);
			}
			var = st.getObjectVariableName();
			if (var != null) {
				resultSet.definedVars.add(var);
			}
		}	
		resultSet.definedVars.addAll(id_vars);

		resultSet.solutions = newSolutions;

		if(DEBUG_PRINT){
			if(st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.TRUE))
				System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] ("+resultSet.getNumberOfSolutions()+" solutions)");
			else System.out.println(" ["+(System.currentTimeMillis()-start)+"] "+"("+resultSet.getNumberOfSolutions()+" solutions)");
		}

		return any;
	}	

	/**Evaluates a part of a query given by a context in a S-APL model against another context in the same model
	 * @param queryId Unique ID of the query
	 * @param queryContextID ID of the query context in the S-APL model (or ID of a single statement)
	 * @param targetContextID ID of the target (data) context in the S-APL model
	 * @param targetModel S-APL model
	 * @param resultSet Current set of solutions to the overall query, which the method will start with and modify
	 * @param removeMode Whether the query is run as "remove by pattern" operation - to record the links that would be removed 
	 * @param idVars Current list for remembering variable names used to match statement and context IDs
	 * @param blockingTimeMoments Current list for remembering future time moments used in the query as a filter blocking the query from gaining results
	 * @return True if any solution to the overall query is still possible
	 * */
	private static boolean evaluateContext(String queryId, String queryContextID, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants, SaplQueryResultSet resultSet, boolean removeMode, List<String> idVars, List<String> time_vars, List<Long> blockingTimeMoments) {
		if (idVars == null) {
			queryContextID = SaplQueryEngine.unwrapSetPredicates(queryContextID, targetContextID, targetModel, resultSet, true);
		} else {
			queryContextID = SaplQueryEngine.unwrapSetPredicates(queryContextID, targetContextID, targetModel, resultSet, false); //it is not an upper-level call, forAll, forSome, and orderBy should not be affected
		}

		if (queryContextID.startsWith(SaplConstants.CONTEXT_PREFIX)){
			return evaluateContext(queryId, targetModel.contexts.get(queryContextID).members, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, idVars, time_vars, blockingTimeMoments);
		} else {
			List<String> list = new ArrayList<String>();
			list.add(queryContextID);
			return evaluateContext(queryId, list, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, idVars, time_vars, blockingTimeMoments);			
		}
	}

	private static void analyseStatementVars(String statementID, SemanticStatement st, SaplModel targetModel, 
			Map<String,Set<String>> statementVars, Map<String,Set<String>> statementCalculatedVars, Map<String,Set<String>> statementUsedVars){

		Set<String> matched = new HashSet<String>();
		Set<String> calculated = new HashSet<String>();
		Set<String> used = new HashSet<String>();
		
		findAllVarsInStatement(st, targetModel, matched, calculated, used);
		
		statementVars.put(statementID, matched);
		if(!calculated.isEmpty()) statementCalculatedVars.put(statementID, calculated);
		if(!used.isEmpty()) statementUsedVars.put(statementID, used);
	}
	
	
	private static boolean evaluateContext(String queryId, List<String> queryStatementIDs, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants, SaplQueryResultSet resultSet, boolean removeMode, List<String> idVars, List<String> time_vars, List<Long> blockingTimeMoments) {

		List<String> trueIf = null; //basic select conditions
		List<String> falseIf = null; //s:doNotBelieve conditions, checked after the basic ones
		List<String> filterIf = null; // statements checking conditions on variables (i.e. filters)
		List<String> calcIf = null; // statements calculating new variables
		List<String> statIf = null; // statements calculating new variables based on a set of matching solutions, rather than a single solution (i.e. aggregates)

		Map<String,Set<String>> statementMatchVars = new HashMap<String, Set<String>>(); //variables referred in statements (used or bound, but not calculated)
		Map<String,Set<String>> statementUsedVars = new HashMap<String, Set<String>>(); //variables used in statements (must be bound elsewhere)
		Map<String,Set<String>> statementCalculatedVars = new HashMap<String, Set<String>>(); //variables calculated in statements (thus cannot be bound anywhere else)
		Set<String> allCalculatedVars = null;

		//iterate all statements to divide them by type
		for(String id : queryStatementIDs){
			SemanticStatement st = targetModel.beliefs.get(id);
			if (st.subject.equals(SaplConstants.I) && st.predicate.equals(SaplConstants.doNotBelieve) && st.isObjectContext()) {
				//negative condition -> to treat later
				if (falseIf == null) {
					falseIf = new ArrayList<String>();
				}
				falseIf.add(id);
				continue;
			}
			else if(targetContextID.equals(SaplConstants.GENERAL_CONTEXT) 
					&& SaplConstants.embedded_predicates.contains(st.predicate) && !st.isObjectContext()) {
				//filtering and statistic predicates -> to treat later
				if (SaplConstants.log_predicates.contains(st.predicate)){
					if (filterIf == null) {
						filterIf = new ArrayList<String>();
					}
					filterIf.add(id);

					analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);
					
					continue;
				} else if (st.isSubjectVar()) {
					if (st.predicate.equals(SaplConstants.expression)) {
						if (calcIf == null) {
							calcIf = new ArrayList<String>();
						}
						calcIf.add(id);

						analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);
						
						continue;
					} else if (st.predicate.equals(SaplConstants.count)
							|| st.predicate.equals(SaplConstants.max) || st.predicate.equals(SaplConstants.min)
							|| st.predicate.equals(SaplConstants.sum) || st.predicate.equals(SaplConstants.avg)) {
						if (statIf == null) {
							statIf = new ArrayList<String>();
						}
						statIf.add(id);

						analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);

						continue;
					}
				} else if (st.isSubjectContext() && st.predicate.equals(SaplConstants.groupedBy) || st.predicate.equals(SaplConstants.distinctBy)){
					if (statIf == null) {
						statIf = new ArrayList<String>();
					}
					statIf.add(id);

					analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);
					
					continue;
				}
			}

			if (st.predicate.equals(SaplConstants.rdfType)
					&& (st.object.equals(SaplConstants.Context) || st.object.equals(SaplConstants.Variable))) {
				if (filterIf == null) {
					filterIf = new ArrayList<String>();
				}
				filterIf.add(id);

				analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);

				continue;
			}

			
			if((st.predicate.equals(SaplConstants.or) || st.predicate.equals(SaplConstants.and)) && st.isSubjectContext() && st.isObjectContext()){
				if(checkIfFiltersOnlyComboStatement(st, targetModel)){
					if(DEBUG_PRINT) System.out.println("    "+st+" TREATED AS FILTER");
					if (filterIf == null) {
						filterIf = new ArrayList<String>();
					}
					filterIf.add(id);
					analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);
					statementMatchVars.get(id).remove("");
					statementMatchVars.get(id).remove(" ");
					continue;
				}
			}			
			
			if (trueIf == null) {
				trueIf = new ArrayList<String>();
			}
			trueIf.add(id);
			
			analyseStatementVars(id, st, targetModel, statementMatchVars, statementCalculatedVars, statementUsedVars);
		}
		
		if(DEBUG_PRINT){
			System.out.println("    MATCH VARS for "+queryStatementIDs+": " + statementMatchVars);
			if(!statementUsedVars.isEmpty()) System.out.println("    MUST EXIST VARS : " + statementUsedVars);
			if(!statementCalculatedVars.isEmpty()) System.out.println("    CALCULATED VARS : " + statementCalculatedVars);
		}
		
		if(!statementCalculatedVars.isEmpty()){
			allCalculatedVars = new HashSet<String>();
			for(Set<String> vars : statementCalculatedVars.values())
				allCalculatedVars.addAll(vars);
		}
		
		
		//process basic matching statements
		while(true){

			//first, apply all filters and s:expression clauses that can be applied at this stage
			if(filterIf!=null){
				Iterator<String> it = filterIf.iterator();
				while(it.hasNext()){
					String lid = it.next();

					if(statementUsedVars.get(lid)==null || resultSet.definedVars.containsAll(statementUsedVars.get(lid))){
						SemanticStatement st = targetModel.beliefs.get(lid);

						if(st.predicate.equals(SaplConstants.and) && st.isSubjectContext() && st.isObjectContext()){
							it.remove();
							if(!applyAndStatement(queryId, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, time_vars, blockingTimeMoments, st, null))
								return false;
						}
						else if(st.predicate.equals(SaplConstants.or) && st.isSubjectContext() && st.isObjectContext()){
							it.remove();
							if(!applyOrStatement(queryId, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, time_vars, blockingTimeMoments, st, null))
								return false;
						}
						else if(SaplQueryEngine.applyFilterStatement(queryId, lid, targetModel, resultSet, blockingTimeMoments, time_vars, false, null))
							it.remove();
						if (resultSet.getNumberOfSolutions() == 0) {
							return false;
						}
					}
				}
			}
			if(calcIf!=null){
				Iterator<String> it = calcIf.iterator();
				while(it.hasNext()){
					String lid = it.next();
					if(statementUsedVars.get(lid)==null || resultSet.definedVars.containsAll(statementUsedVars.get(lid))){
						if(SaplQueryEngine.applyExpressionStatement(queryId, lid, targetModel, resultSet, null))
							it.remove();
						if (resultSet.getNumberOfSolutions() == 0) {
							return false;
						}
					}
				}
			}

			
			if(trueIf==null || trueIf.isEmpty()) break;

			
			//find next statement with most variables already bound
			int bestNotYetBound = Integer.MAX_VALUE;
			int bestAlreadyBound = 0;
			String bestID = trueIf.get(0);

			if(trueIf.size() > 1) {
				for(String id : trueIf){
					Set<String> used = statementUsedVars.get(id);


					if(used!=null && !resultSet.definedVars.containsAll(used)) 
						continue; //only hasMember/memberOf statements can have vars in statementUsedVars

					Set<String> vars = statementMatchVars.get(id);

					if(allCalculatedVars != null){
						Set<String> usedCalculated = new HashSet<String>(allCalculatedVars);
						usedCalculated.retainAll(vars);

						if(!resultSet.definedVars.containsAll(usedCalculated)) 
							continue; // if variable is defined via s:expression somewhere, have to wait
					}

					int notYetBound = vars.size();
					int alreadyBound = 0;

					for(String var: vars){
						if(!var.isEmpty()){
							if(resultSet.definedVars.contains(var)){
								notYetBound--;
								alreadyBound++;
							}
						}
					}

					if(vars.contains(" ")){ // includes is-true
						notYetBound = Integer.MAX_VALUE/8*7  + notYetBound; //postpone to very end
					}
					else if(vars.contains("")){ //includes optional / or
						notYetBound = Integer.MAX_VALUE/2  + notYetBound; //postpone to after any straightforward statements
						SemanticStatement st = targetModel.beliefs.get(id);
						if(st.predicate.equals(SaplConstants.or) || st.object.equals(SaplConstants.Optional))
							notYetBound += Integer.MAX_VALUE/4; // those with optional / or at top level postpone even further
					}

					//note: these are heuristics
					if(alreadyBound == 0 && bestAlreadyBound > 0) continue;
					if(bestNotYetBound != Integer.MAX_VALUE && notYetBound >= bestNotYetBound + Integer.MAX_VALUE/16) continue;
					if(bestAlreadyBound == 0 && alreadyBound > 0  || notYetBound < bestNotYetBound 
							|| notYetBound == bestNotYetBound && alreadyBound > bestAlreadyBound) {
						bestNotYetBound = notYetBound;
						bestAlreadyBound = alreadyBound;
						bestID = id;
					}

				}
			}

			trueIf.remove(bestID);

			SemanticStatement st = targetModel.beliefs.get(bestID);
			String targetID1 = targetContextID;

			List<String> id_vars;
			if (idVars == null) {
				id_vars = new ArrayList<String>();
			} else {
				id_vars = new ArrayList<String>(idVars);
			}

			//unwrap the statement inside a match for ID; ask later evaluateStatement() to find the ID
			if (st.predicate.equals(SaplConstants.ID) && st.isObjectVar() && st.isSubjectContext()) {
				id_vars.add(st.getObjectVariableName());
				String new_id;
				if (idVars == null) {
					new_id = SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, true);
				} else {
					new_id = SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, false);
				}
				boolean ok = SaplQueryEngine.evaluateContext(queryId, new_id, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);
				if (!ok) {
					return false;
				}
				continue;
			}

			//optional condition
			if (st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.Optional) && st.isSubjectContext()) {

				SaplQueryResultSet sub_resultSet = new SaplQueryResultSet(resultSet.solutions, resultSet.definedVars);
				sub_resultSet.solutionIndices = resultSet.solutionIndices;
				if (removeMode) {
					sub_resultSet.forRemove = new HashMap<String, Set<LinkInfo>>(resultSet.forRemove);
				}
				boolean ok = SaplQueryEngine.evaluateContext(queryId, st.subject, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, sub_resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);

				long start = System.currentTimeMillis();

				if (ok) {
					Set<String> b_ids = new HashSet<String>();
					for (int i = 0; i < sub_resultSet.solutions.size(); i++) {
						String bid = sub_resultSet.solutions.get(i).get("");
						int index = 0;
						b_ids.add(bid);
						while (true) {
							index = bid.indexOf(".", index + 1);
							if (index != -1) {
								b_ids.add(bid.substring(0, index));
							} else {
								break;
							}
						}
					}

					List<Map<String, String>> newSolutions = new ArrayList<Map<String, String>>();
					Map<String, Set<LinkInfo>> newForRemove = null;
					if (removeMode) {
						newForRemove = new HashMap<String, Set<LinkInfo>>();
					}
					for (int i = 0; i < resultSet.solutions.size(); i++) {
						Map<String, String> solution = resultSet.solutions.get(i);
						String solId = solution.get("");
						if (!b_ids.contains(solId)) {
							newSolutions.add(solution);
							if (removeMode) {
								newForRemove.put(solId, resultSet.forRemove.get(solId));
							}
						}
					}
					newSolutions.addAll(sub_resultSet.solutions);
					if (removeMode) {
						newForRemove.putAll(sub_resultSet.forRemove);
					}
					resultSet.solutions = newSolutions;
					if (removeMode) {
						resultSet.forRemove = newForRemove;
					}
				}
				resultSet.definedVars.addAll(sub_resultSet.definedVars);

				if(DEBUG_PRINT) System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] ("+resultSet.getNumberOfSolutions()+" solutions)");

				continue;
			}

			//AND conditions
			if (st.predicate.equals(SaplConstants.and) && st.isSubjectContext() && st.isObjectContext()) {
				if(!applyAndStatement(queryId, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, time_vars, blockingTimeMoments, st, id_vars))
					return false;
				continue;
			}

			//OR conditions
			if (st.predicate.equals(SaplConstants.or) && st.isSubjectContext() && st.isObjectContext()) {
				if(!applyOrStatement(queryId, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, time_vars, blockingTimeMoments, st, id_vars))
					return false;
				continue;
			}

			SemanticStatement st1 = new SemanticStatement(st);
			if (!treatBNodesConstants && st.isSubjectBlankNode()) {
				st1.subject = SaplConstants.VARIABLE_PREFIX + st.subject;
			}
			if (!treatBNodesConstants && st.isObjectBlankNode()) {
				st1.object = SaplConstants.VARIABLE_PREFIX + st.object;
			}

			//check hasMember or memberOf condition
			if (st.predicate.equals(SaplConstants.hasMember) || st.predicate.equals(SaplConstants.memberOf)) {

				String sts;
				String context;
				if (st.predicate.equals(SaplConstants.hasMember)) {
					sts = st1.object;
					context = st1.subject;
				} else {
					sts = st1.subject;
					context = st1.object;
				}
				if(context.equals(SaplConstants.G)) context = SaplConstants.GENERAL_CONTEXT;

				if (context.equals(SaplConstants.GENERAL_CONTEXT) || context.startsWith(SaplConstants.CONTEXT_PREFIX) || context.startsWith(SaplConstants.VARIABLE_PREFIX)) {
					if (sts.startsWith(SaplConstants.CONTEXT_PREFIX)) {
						boolean ok = SaplQueryEngine.evaluateContext(queryId, sts, context, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);

						if (!ok) {
							return false;
						}
						continue;

					} else if (sts.startsWith(SaplConstants.VARIABLE_PREFIX) || sts.startsWith(SaplConstants.STATEMENT_PREFIX)) {
						if (sts.startsWith(SaplConstants.VARIABLE_PREFIX) && !resultSet.definedVars.contains(sts.substring(SaplConstants.VARIABLE_PREFIX.length()))) {
							st1 = new SemanticStatement(sts, SaplConstants.is, SaplConstants.TRUE);
							targetID1 = context;
						} else {
							if (context.startsWith(SaplConstants.VARIABLE_PREFIX) && !resultSet.definedVars.contains(context.substring(SaplConstants.VARIABLE_PREFIX.length()))) {
								printErr("Error: Wrong query: unbound yet context: " + context);
							} else {

								int n_binds = resultSet.getNumberOfSolutions();
								if(n_binds>0){
									List<Integer> toRemove = new ArrayList<Integer>();

									for (int i = 0; i < n_binds; i++) {
										Map<String, String> solution = resultSet.getSolution(i);
										String cnt_value;
										if (context.startsWith(SaplConstants.CONTEXT_PREFIX)) {
											cnt_value = context;
										} else {
											cnt_value = solution.get(context.substring(SaplConstants.VARIABLE_PREFIX.length()));
										}
										String sts_value;
										if (sts.startsWith(SaplConstants.STATEMENT_PREFIX)) {
											sts_value = sts;
										}
										else {
											sts_value = solution.get(sts.substring(SaplConstants.VARIABLE_PREFIX.length()));
										}

										if (sts_value.startsWith(SaplConstants.CONTEXT_PREFIX)) {
											printErr("Error: Wrong query: variable value (" + sts
													+ ") must be a statement ID, but a context ID found: " + sts_value);
											toRemove.add(i);
										} else if (!targetModel.contexts.get(cnt_value).members.contains(sts_value)) {
											toRemove.add(i);
										} else {	
											String sol_id = solution.get("");
											Set<LinkInfo> temp = resultSet.forRemove.get(sol_id);
											if (temp != null) {
												temp.add(new LinkInfo(cnt_value, sts_value));
											} else {
												temp = new HashSet<LinkInfo>();
												temp.add(new LinkInfo(cnt_value, sts_value));
												resultSet.forRemove.put(sol_id, temp);
											}
										}
									}

									resultSet.removeSolutions(toRemove);
									if (resultSet.getNumberOfSolutions() == 0) {
										return false;
									}

								}
								else{
									ContextContainer c = targetModel.contexts.get(context);
									if(c==null){
										printErr("Error: Wrong query: context does not exist: " + context);
										return false;
									}
									if (c.members.contains(sts)) {
										Set<LinkInfo> temp = resultSet.forRemove.get("");
										if (temp != null) {
											temp.add(new LinkInfo(context, sts));
										} else {
											temp = new HashSet<LinkInfo>();
											temp.add(new LinkInfo(context, sts));
											resultSet.forRemove.put("", temp);
										}
									}
									else return false;
								}

								continue;
							}
						}
					} else {
						printErr("Error: Wrong query: not a context or statement ID: " + sts);
					}
				} else {
					printErr("Error: Wrong query: not a context: " + context);
				}
			}

			//Basic statement matching
			SemanticStatement st2 = new SemanticStatement(st1);
			if (st1.isSubjectContext()) {
				st2.subject = SaplConstants.VARIABLE_PREFIX + st1.subject;
			}

			if (st1.isObjectContext() && !st1.isObjectExplicitLiteral()) {
				st2.object = SaplConstants.VARIABLE_PREFIX + st1.object;
				if (targetContextID.equals(SaplConstants.GENERAL_CONTEXT) && st.subject.equals(SaplConstants.I)
						&& st.predicate.equals(SaplConstants.want)) {
					resultSet.goals_var = st2.object;
				}
			}

			if (targetContextID.equals(resultSet.goals_var)) {
				id_vars.add("__handledGoal__");
			}
			if (targetContextID.equals(SaplConstants.GENERAL_CONTEXT) && st.predicate.equals(SaplConstants.configuredAs)) {
				id_vars.add("__handledCommitment__");
			}
			if (targetContextID.equals(SaplConstants.GENERAL_CONTEXT)
					//&& (st.predicate.equals(SaplConstants.eventFromGUI) || st.predicate.equals(SaplConstants.eventFromSocket))) {
					&& (st.predicate.equals(SaplConstants.occured))) {
				id_vars.add("__handledEvent__");
			}

			if (SaplQueryEngine.evaluateStatement(queryId, st2, targetID1, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, id_vars, time_vars, blockingTimeMoments)) {
				boolean ok = true;
				if (st1.isSubjectContext()) {
					ok = SaplQueryEngine.evaluateContext(queryId, st1.subject, st2.subject, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, null, time_vars, blockingTimeMoments);
					if (!ok) {
						return false;
					}
				}
				if (ok && st1.isObjectContext() && !st1.isObjectExplicitLiteral()) {
					ok = SaplQueryEngine.evaluateContext(queryId, st1.object, st2.object, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, null, time_vars, blockingTimeMoments);
					if (!ok) {
						return false;
					}
				}
				if (ok) {
					//catching variables used in time-related conditions
					if (time_vars!=null && targetContextID.equals(SaplConstants.GENERAL_CONTEXT)
							&& (st1.subject.equals(SaplConstants.Now) || st1.subject.equals(SaplConstants.Time) || st1.subject
									.equals(SaplConstants.Date)) && st1.predicate.equals(SaplConstants.is)
							&& st1.isObjectVar()) {
						time_vars.add(st.getObjectVariableName());
					}
				}
			} else {
				return false;
			}

		}//trueIf, i.e. normal matching conditions

		if (falseIf != null) {
			Iterator<String> fit = falseIf.iterator();
			while (fit.hasNext()) {
				List<Integer> toRemove = new ArrayList<Integer>();
				SemanticStatement st = targetModel.beliefs.get(fit.next());
				String cid = st.object;
				
				if(DEBUG_PRINT) System.out.println("  CHECKING: "+st);
				
				int n_binds = resultSet.getNumberOfSolutions();
				if (n_binds == 0) {
					SaplQueryResultSet temp_resultSet = new SaplQueryResultSet();
					boolean result = SaplQueryEngine.evaluateContext(queryId, cid, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, temp_resultSet, removeMode, null, time_vars, blockingTimeMoments);
					if (DEBUG_PRINT) System.out.println(st+" [0] "+!result);
					return !result;
				} else {
//					for (int i = 0; i < n_binds; i++) {
//						List<Map<String, String>> vec = new ArrayList<Map<String, String>>();
//						Map<String, String> solution = resultSet.getSolution(i);
//						vec.add(solution);
//						SaplQueryResultSet temp_resultSet = new SaplQueryResultSet(vec, resultSet.definedVars);
//						temp_resultSet.solutionIndices = new HashMap<String, Integer>(resultSet.solutionIndices);
//
//						if (DEBUG_PRINT) System.out.println("doNotBelieve: Checking "+ solution);		
//						if (SaplQueryEngine.evaluateContext(cid, targetID, targetModel, symmetricQuerying, treatBNodesConstants, temp_resultSet, removeMode, null, time_vars, blockingTimeMoments)) {
//							toRemove.add(i);
//						}
//					}

					SaplQueryResultSet sub_resultSet = new SaplQueryResultSet(resultSet.solutions, resultSet.definedVars);
					sub_resultSet.solutionIndices = resultSet.solutionIndices;
					if (removeMode) {
						sub_resultSet.forRemove = new HashMap<String, Set<LinkInfo>>(resultSet.forRemove);
					}

					if (SaplQueryEngine.evaluateContext(queryId, cid, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, sub_resultSet, removeMode, null, time_vars, blockingTimeMoments)){
						Set<String> b_ids = new HashSet<String>();
						for (int i = 0; i < sub_resultSet.solutions.size(); i++) {
							String bid = sub_resultSet.solutions.get(i).get("");
							int index = 0;
							b_ids.add(bid);
							while (true) {
								index = bid.indexOf(".", index + 1);
								if (index != -1) {
									b_ids.add(bid.substring(0, index));
								} else {
									break;
								}
							}
						}

						for (int i = 0; i < n_binds; i++) {
							Map<String, String> solution = resultSet.getSolution(i);
							String solId = solution.get("");
							if (b_ids.contains(solId)) {
								toRemove.add(i);
							}
						}
						
					}
					
					resultSet.removeSolutions(toRemove);

					if (DEBUG_PRINT) System.out.println(st+" [0] ("+resultSet.getNumberOfSolutions()+" solutions)");
					
					if (resultSet.getNumberOfSolutions() == 0) {
						return false;
					}
				}
			}
		}//negative conditions

		
		//filters and statistical predicates

		if(calcIf!=null && calcIf.isEmpty()) calcIf = null;
		if(filterIf!=null && filterIf.isEmpty()) filterIf = null;

		while ((filterIf != null) || (statIf != null) || calcIf != null) {
			
			int calcIfReduced = 0; 
			int localIfReduced = 0; 
			int statIfReduced = 0; 
			
			if(calcIf != null){
				Set<String> remain = new HashSet<String>();
				Iterator<String> lit = calcIf.iterator();
				while (lit.hasNext()) {
					String id = lit.next();

					SaplQueryEngine.applyExpressionStatement(queryId, id, targetModel, resultSet, remain);

					if (resultSet.getNumberOfSolutions() == 0) {
						return false;
					}
				}
				
				calcIfReduced = calcIf.size()-remain.size();
				if(remain.isEmpty()) calcIf = null;
				else calcIf = new ArrayList<String>(remain);
				
			}
			
			if (filterIf != null) {
				Set<String>  remain = new HashSet<String>();				
				Iterator<String> lit = filterIf.iterator();
				while (lit.hasNext()) {
					String id = lit.next();
					
					if(!resultSet.definedVars.containsAll(statementUsedVars.get(id))){
						remain.add(id);
						continue;
					}

					SemanticStatement st = targetModel.beliefs.get(id);

					if(st.predicate.equals(SaplConstants.and) && st.isSubjectContext() && st.isObjectContext()){
						if(!applyAndStatement(queryId, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, time_vars, blockingTimeMoments, st, null))
							return false;
					}
					else if(st.predicate.equals(SaplConstants.or) && st.isSubjectContext() && st.isObjectContext()){
						if(!applyOrStatement(queryId, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, time_vars, blockingTimeMoments, st, null))
							return false;
					}
					else SaplQueryEngine.applyFilterStatement(queryId, id, targetModel, resultSet,
							blockingTimeMoments, time_vars, (statIf==null && calcIf==null), remain);

					if (resultSet.getNumberOfSolutions() == 0) {
						return false;
					}
				}

				localIfReduced = filterIf.size()-remain.size();
				if(remain.isEmpty()) filterIf = null;
				else filterIf = new ArrayList<String>(remain);
				
			}//filtering predicates
			
			if (statIf != null) {
				Set<String> remain = new HashSet<String>();
				
				int n_binds = resultSet.getNumberOfSolutions();
				Iterator<String> lit = statIf.iterator();
				while (lit.hasNext()) {
					String id = lit.next();
					SemanticStatement st = targetModel.beliefs.get(id);
					String var = st.getSubjectVariableName();
					if(var!=null){
						if (resultSet.definedVars.contains(var)) {
							printErr("Error in aggregate statement: variable is already bound: " + st);
							continue;
						}
					}

					long start = 0;
					if(DEBUG_PRINT) start = System.currentTimeMillis();

					if(st.predicate.equals(SaplConstants.groupedBy) || st.predicate.equals(SaplConstants.distinctBy)){
						List<String> groupedBy = new ArrayList<String>();
						String[] result = st.object.split(",");
						for (int x = 0; x < result.length; x++) {
							String var2 = result[x].trim().substring(SaplConstants.VARIABLE_PREFIX.length());
							if (resultSet.definedVars.contains(var2)) {
								groupedBy.add(var2);
							} else {
								printErr("Warning in \"groupedBy/distinctBy\": variable ?" + var2 + " is not bound and ignored: " + st);
							}
						}

						boolean withinGroups =  st.predicate.equals(SaplConstants.distinctBy) ? false : true;
						
						if (groupedBy.size() > 0) {
							boolean postpone = false;
							for(String innerId : targetModel.contexts.get(st.subject).members){
								SemanticStatement innerSt = targetModel.beliefs.get(innerId);
								String operationVar = innerSt.getObjectVariableName();
								if(operationVar!=null && !resultSet.definedVars.contains(operationVar)){
									remain.add(id);	
									postpone = true;
								}
							}
							if(!postpone){
								for(String innerId : targetModel.contexts.get(st.subject).members){
									SemanticStatement innerSt = targetModel.beliefs.get(innerId);
									if(innerSt.isSubjectVar() && (innerSt.isObjectAny() && innerSt.predicate.equals(SaplConstants.count) || innerSt.isObjectVar() && (innerSt.predicate.equals(SaplConstants.min) 
											|| innerSt.predicate.equals(SaplConstants.max) || innerSt.predicate.equals(SaplConstants.sum) || innerSt.predicate.equals(SaplConstants.avg)))){
										String operationVar = innerSt.getObjectVariableName();
										if(operationVar==null){
											printErr("Error in \"groupedBy/distinctBy\" aggregate statement : object must be a variable: " + innerSt);
										}
										else if(!resultSet.definedVars.contains(operationVar)){
											remain.add(id);
											postpone = true;
										}
										else{
											//resultSet will calculate the metrics and add the variable										
											resultSet.getSolutions(groupedBy, innerSt, withinGroups);
										}
									}
									else 
										printErr("Warning in \"groupedBy/distinctBy\": statement is not proper count/min/max/sum/avg and thus ignored: "+innerSt);
								}
							}
							if(postpone) continue;
						}
					}
					else if (st.predicate.equals(SaplConstants.count)){
						List<String> count = new ArrayList<String>();
						if(st.object.equals(SaplConstants.ANYTHING)){
							int num = resultSet.solutions.size();
							for (int i = 0; i < n_binds; i++) {
								Map<String, String> solution = resultSet.getSolution(i);
								solution.put(var, String.valueOf(num));
							}
							resultSet.definedVars.add(var);
						}
						else {
							String[] result = st.object.split(",");
							for (int x = 0; x < result.length; x++) {
								String var2 = result[x].trim().substring(SaplConstants.VARIABLE_PREFIX.length());
								if (resultSet.definedVars.contains(var2)) {
									count.add(var2);
								} else {
									printErr("Warning in \"count\": variable ?" + var2 + " is not bound and ignored: " + st);
								}
							}
							if (count.size() > 0) {
								int num = resultSet.getSolutions(count).size();
								for (int i = 0; i < n_binds; i++) {
									Map<String, String> solution = resultSet.getSolution(i);
									solution.put(var, String.valueOf(num));
								}
								resultSet.definedVars.add(var);
							}
						}
					} else {
						String var2 = st.getObjectVariableName();
						if (!resultSet.definedVars.contains(var2)) {
							printErr("Warning: variable ?" + var2 + " is not bound and the condition is ignored: " + st);
						}
						else {
	
							List<Map<String, String>> vec = resultSet.solutions;
							if (st.predicate.equals(SaplConstants.sum)) {
								vec = resultSet.rearrangeSolutions(new ArrayList<Map<String, String>>(vec), resultSet.orderBy, -1, 0);
							}
	
							String sResult = "";
							double dResult = 0;
							boolean numberValues = true;
	
							for (int el = 0; el < vec.size(); el++) {
								String value = vec.get(el).get(var2);
								double v = 0;
								if (numberValues) {
									try {
										v = Double.valueOf(value);
									} catch (NumberFormatException e) {
										numberValues = false;
									}
								}
								if (st.predicate.equals(SaplConstants.sum) || st.predicate.equals(SaplConstants.avg)) {
									if (numberValues) {
										dResult += v;
									}
									sResult += value;
								} else if (st.predicate.equals(SaplConstants.max)) {
									if (numberValues) {
										if ((el == 0) || (v > dResult)) {
											dResult = v;
										}
									}
									if ((el == 0) || (value.compareTo(sResult) > 0)) {
										sResult = value;
									}
								} else if (st.predicate.equals(SaplConstants.min)) {
									if (numberValues) {
										if ((el == 0) || (v < dResult)) {
											dResult = v;
										}
									}
									if ((el == 0) || (value.compareTo(sResult) < 0)) {
										sResult = value;
									}
								}
							}
							if(st.predicate.equals(SaplConstants.avg)){
								if (numberValues) {
									dResult /= vec.size();
								}
								else{
									printErr("Warning: 'avg' is undefined for string values, returning sum: " + st);
								}
							}
	
							if (numberValues) {
								double d = Math.floor(dResult);
								if (d == dResult) {
									sResult = String.valueOf((long) d);
								} else {
									sResult = String.valueOf(dResult);
								}
							}
	
							for (int i = 0; i < n_binds; i++) {
								Map<String, String> solution = resultSet.getSolution(i);
								solution.put(var, sResult);
							}
							resultSet.definedVars.add(var);
						}
					}

					if(DEBUG_PRINT) System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] "+"("+resultSet.getNumberOfSolutions()+" solutions)");

				}
				
				statIfReduced = statIf.size()-remain.size();
				if(remain.isEmpty()) statIf = null;
				else statIf = new ArrayList<String>(remain);
				
			}//statistic predicates
			
			//If does not seem to be possible to handle
			if(statIfReduced==0 && calcIfReduced==0 && localIfReduced==0){
				StringBuilder statements = new StringBuilder();
				boolean fail = true;
				
				if(filterIf!=null){
					for (String r : filterIf) {

						//Perform a check if this is a 'exists(?var)' condition that can be now resolved to false
						Set<String> vars = statementUsedVars.get(r);
						String obj = targetModel.beliefs.get(r).object;
						if(vars.size()==1){
							String var = vars.iterator().next();
							if(!resultSet.definedVars.contains(var) && obj.replaceAll("\\s+","").equals("exists("+SaplConstants.VARIABLE_PREFIX+var+")")){
								resultSet.definedVars.add(var);
								fail = false;
								break;
							}
						}
						
						statements.append(". ");
						statements.append(targetModel.beliefs.get(r));
					}
					
					if(!fail) continue; //try one more iteration
				
				}
				if(calcIf!=null){
					for (String r : calcIf) {
						statements.append(". ");
						statements.append(targetModel.beliefs.get(r));
					}
				}
				if(statIf!=null){
					for (String r : statIf) {
						statements.append(". ");
						statements.append(targetModel.beliefs.get(r));
					}
				}

				String sts = statements.toString();
				printErr("Error in query: A variable is not defined or a dead loop:" + sts);
				
				return false;				
			}
			
		}

		return true;
	}

	private static boolean applyOrStatement(String queryId, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants,
			SaplQueryResultSet resultSet, boolean removeMode, List<String> time_vars, List<Long> blockingTimeMoments,
			SemanticStatement st, List<String> id_vars) {
		
		
		if(DEBUG_PRINT) System.out.println("  CHECKING: "+st);
		
		List<Map<String, String>> newSolutions = new ArrayList<Map<String, String>>();
		Map<String, Set<LinkInfo>> newForRemove = null;
		if (removeMode) {
			newForRemove = new HashMap<String, Set<LinkInfo>>();
		}

		SaplQueryResultSet sub_resultSet = new SaplQueryResultSet(resultSet.solutions, resultSet.definedVars);
		sub_resultSet.solutionIndices = resultSet.solutionIndices;
		if (removeMode) {
			sub_resultSet.forRemove = new HashMap<String, Set<LinkInfo>>(resultSet.forRemove);
		}
		boolean ok1 = SaplQueryEngine.evaluateContext(queryId, st.subject, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, sub_resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);

		SaplQueryResultSet ob_resultSet = new SaplQueryResultSet(resultSet.solutions, resultSet.definedVars);
		ob_resultSet.solutionIndices = resultSet.solutionIndices;
		if (removeMode) {
			ob_resultSet.forRemove = new HashMap<String, Set<LinkInfo>>(resultSet.forRemove);
		}
		boolean ok2 = SaplQueryEngine.evaluateContext(queryId, st.object, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, ob_resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);

		long start = System.currentTimeMillis();

		if (!ok1 && !ok2) {
			resultSet.solutions.clear();
			if(DEBUG_PRINT) System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] ("+resultSet.getNumberOfSolutions()+" solutions)");
			return false;
		} else if (ok1 && !ok2) {
			newSolutions.addAll(sub_resultSet.solutions);
			if (removeMode) {
				newForRemove.putAll(sub_resultSet.forRemove);
			}
		} else if (!ok1 && ok2) {
			newSolutions.addAll(ob_resultSet.solutions);
			if (removeMode) {
				newForRemove.putAll(ob_resultSet.forRemove);
			}
		} else {
			newSolutions.addAll(sub_resultSet.solutions);
			if (removeMode) {
				newForRemove.putAll(sub_resultSet.forRemove);
			}
			Map<String, String> index_newSolutions = new HashMap<String, String>();
			Iterator<Map<String, String>> newIt = sub_resultSet.solutions.iterator();
			while (newIt.hasNext()) {
				Map<String, String> temp = new HashMap<String, String>(newIt.next());
				String tempid = temp.remove("");
				index_newSolutions.put(temp.toString(), tempid);
			}

			newIt = ob_resultSet.solutions.iterator();
			while (newIt.hasNext()) {
				Map<String, String> solution = newIt.next();
				String solID = solution.get("");
				Map<String, String> temp = new HashMap<String, String>(solution);
				temp.remove("");
				String tempid = index_newSolutions.get(temp.toString());
				if (tempid == null) {
					newSolutions.add(solution);
					if (removeMode) {
						newForRemove.put(solID, ob_resultSet.forRemove.get(solID));
					}
				} else if (removeMode) {
					Set<LinkInfo> t = ob_resultSet.forRemove.get(solID);
					Set<LinkInfo> t2 = newForRemove.get(tempid);
					if ((t != null) && (t2 != null)) {
						t2.addAll(t);
					}
				}
			}
		}

		resultSet.definedVars.addAll(sub_resultSet.definedVars);
		resultSet.definedVars.addAll(ob_resultSet.definedVars);
		resultSet.solutions = newSolutions;
		if (removeMode) {
			resultSet.forRemove = newForRemove;
		}
		
		if(DEBUG_PRINT) System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] ("+resultSet.getNumberOfSolutions()+" solutions)");
		
		return true;
	}

	/**Applies an s:and statement to the given Result Set*/
	private static boolean applyAndStatement(String queryId, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants,
			SaplQueryResultSet resultSet, boolean removeMode, List<String> time_vars, List<Long> blockingTimeMoments,
			SemanticStatement st, List<String> id_vars) {
		
		if(DEBUG_PRINT) System.out.println("  CHECKING: "+st);

		boolean ok = SaplQueryEngine.evaluateContext(queryId, st.subject, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);
		if(!ok){
			if(DEBUG_PRINT) System.out.println(st+" [0] (0 solutions)");
			return false;
		}
		ok = SaplQueryEngine.evaluateContext(queryId, st.object, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, id_vars, time_vars, blockingTimeMoments);
		if(DEBUG_PRINT) System.out.println(st+" [0] ("+resultSet.getNumberOfSolutions()+" solutions)");
		if(!ok) return false;
		return true;
	}

	/**Applies a single s:expression statement to the given Result Set*/
	private static boolean applyExpressionStatement(String queryId, String stId, SaplModel targetModel, SaplQueryResultSet resultSet, Set<String> remain) {
		SemanticStatement st = targetModel.beliefs.get(stId);
		String var = st.getSubjectVariableName();

		long start = 0;
		if(DEBUG_PRINT) start = System.currentTimeMillis();

		int n_binds = resultSet.getNumberOfSolutions();
		if (n_binds == 0) {
			resultSet.addEmptySolution();
			n_binds = resultSet.getNumberOfSolutions();
		}
		List<Integer> toRemove = new ArrayList<Integer>();
		for (int i = 0; i < n_binds; i++) {
			Map<String, String> solution = resultSet.getSolution(i);
			String value = solution.get(var);
			if (value != null) {
				printErr("Error in \"expression\": Variable is already bound: " + st);
				return false;
			}
			StringBuilder errorMessage = new StringBuilder();

			String expression_value = (st.isObjectExplicitLiteral() || st.isObjectURI() || st.isObjectBlankNode() || (st.object.length() < 2)) ? 
					st.object : ExpressionEvaluator.calculate(st.object, queryId, st.object, solution, targetModel, errorMessage);
			if (expression_value != null) {
				solution.put(var, expression_value);
			} else {
				String error = errorMessage.toString();
				if (error.startsWith(SaplConstants.VARIABLE_PREFIX) && error.endsWith("not bound")) {
					
					String variab = error.substring(1, error.indexOf(" "));

					if (resultSet.definedVars.contains(variab)) {
						toRemove.add(i);
					} else {
						if (remain!=null) remain.add(stId); //postpone checking
						return false;
					}
				} else {
					printErr(error);
					toRemove.add(i);
				}
			}
		}
		resultSet.definedVars.add(var);
		resultSet.removeSolutions(toRemove);

		if(DEBUG_PRINT) System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] ("+resultSet.getNumberOfSolutions()+" solutions)");
		
		return true;
	}


	/**Applies a single filter statement to the given Result Set*/
	private static boolean applyFilterStatement(String queryId, String stId, SaplModel targetModel, SaplQueryResultSet resultSet,
			List<Long> blockingTimeMoments, List<String> time_vars, boolean nothingToWait, Set<String> remain) {

		SemanticStatement st = targetModel.beliefs.get(stId);

		List<Integer> toRemove = new ArrayList<Integer>();
		long start = 0;
		if(DEBUG_PRINT) start = System.currentTimeMillis();

		String var = null;
		boolean constant = false;
		if (st.isSubjectVar()) {
			var = st.getSubjectVariableName();
		} else if(st.isSubjectBlankNode()){
			var = st.subject;
		}
		else {
			constant = true;
		}

		int n_binds = resultSet.getNumberOfSolutions();
		if (n_binds == 0) {
			resultSet.addEmptySolution();
			n_binds = resultSet.getNumberOfSolutions();
		}
		boolean knownToBeProcessable = false;
		for (int i = 0; i < n_binds; i++) {
			
			Map<String, String> solution = resultSet.getSolution(i);
			
			String value;
			if (constant) {
				value = st.subject;
			} else {
				value = solution.get(var);
			}
			if (value == null) {
				if (resultSet.definedVars.contains(var) || knownToBeProcessable || nothingToWait) {
					if (!(st.predicate.equals(SaplConstants.rdfType) && st.object
							.equals(SaplConstants.Variable))) {
						printErr("Warning: variable ?" + var
								+ " is not bound in "+solution+ " and the condition is considered false: " + st);
						toRemove.add(i);
					}
				} else {
					if(remain!=null) remain.add(stId); //postpone checking
					toRemove.clear();
					return false;
				}
				continue;
			}

			StringBuilder errorMessage = new StringBuilder();
			String expression_value;
			if (!st.predicate.equals(SaplConstants.rdfType) && !st.isObjectExplicitLiteral() && !st.isObjectURI() && !st.isObjectBlankNode() && st.object.length() >= 2) {
				// here we are trying to get the expression value (as you can see the input is the object of the statement)
				// it is needed, because the object can be some function like indexOf, sum, avg
				expression_value = ExpressionEvaluator.calculate(st.object, queryId, st.object, solution, targetModel, errorMessage);
			} else {
				expression_value = st.object;
			}
			
			if (expression_value == null) {
				String error = errorMessage.toString();
				
				if (error.startsWith(SaplConstants.VARIABLE_PREFIX) && error.endsWith("not bound")) {
					String variab = error.substring(1, error.indexOf(" "));
					if (resultSet.definedVars.contains(variab)) {
						toRemove.add(i);
					} else {
						if (!knownToBeProcessable && !nothingToWait) {
							if(remain!=null) remain.add(stId); //postpone checking
							toRemove.clear();
							return false;
						} else {
							printErr("Error in filtering predicate: " + error);
							toRemove.add(i);
						}
					}
				} else {
					printErr("Error in filtering predicate: " + error);
					toRemove.add(i);
				}
			}

			if (expression_value != null) {

				knownToBeProcessable = true; //at least one binding had all variables available, so it is not a question of a need to wait for other statements
				
				boolean cv = SaplQueryEngine.checkValue(value, st.predicate, expression_value);
				if (!cv) {
					toRemove.add(i);
				}

				//catching blocking time-related conditions
				if (time_vars!=null && blockingTimeMoments!=null && time_vars.contains(var)) {
					if (!((!cv && (st.predicate.equals(SaplConstants.lt) || st.predicate.equals(SaplConstants.lte))) || (cv && (st.predicate
							.equals(SaplConstants.gt) || st.predicate.equals(SaplConstants.gte))))) {
						try{
							long time;
							if (expression_value.charAt(4) == '-') { //was Date
								int year = Integer.valueOf(expression_value.substring(0, 4));
								int month = Integer.valueOf(expression_value.substring(5, 7)) - 1;
								int day = Integer.valueOf(expression_value.substring(8));
								if (!cv
										&& (st.predicate.equals(SaplConstants.gt) || st.predicate.equals(SaplConstants.neq))) {
									day++;
								} else if (cv
										&& (st.predicate.equals(SaplConstants.lte) || st.predicate.equals(SaplConstants.eq))) {
									day++;
								}
	
								Calendar calendar = Calendar.getInstance();
								calendar.set(year, month, day, 0, 0, 0);
								calendar.set(Calendar.MILLISECOND, 0);
								time = calendar.getTimeInMillis();
							} else if (expression_value.charAt(2) == ':') { //was Time
								int hour = Integer.valueOf(expression_value.substring(0, 2));
								int minute = Integer.valueOf(expression_value.substring(3, 5));
								int second = 0;
								if(expression_value.length() > 5){
									String rest = expression_value.substring(6);
									if(rest.contains("+")) rest = rest.substring(0,rest.indexOf("+"));
									else if(rest.contains("-")) rest = rest.substring(0,rest.indexOf("-"));
									second = Integer.valueOf(rest);
								}
								if (!cv
										&& (st.predicate.equals(SaplConstants.gt) || st.predicate.equals(SaplConstants.neq))) {
									second++;
								} else if (cv
										&& (st.predicate.equals(SaplConstants.lte) || st.predicate.equals(SaplConstants.eq))) {
									second++;
								}
	
								Calendar calendar = Calendar.getInstance();
								calendar.set(Calendar.HOUR_OF_DAY, hour);
								calendar.set(Calendar.MINUTE, minute);
								calendar.set(Calendar.SECOND, second);
								calendar.set(Calendar.MILLISECOND, 0);
								time = calendar.getTimeInMillis();
								
								if(System.currentTimeMillis()>time)
									time += 24 * 60 * 60 * 1000; //next day (fix added 16.06.2016)
								
							} else {
								time = Long.valueOf(expression_value); //was Now in mills
								if (!cv
										&& (st.predicate.equals(SaplConstants.gt) || st.predicate.equals(SaplConstants.neq))) {
									time++;
								} else if (cv
										&& (st.predicate.equals(SaplConstants.lte) || st.predicate.equals(SaplConstants.eq))) {
									time++;
								}
							}
	
							Long lTime = Long.valueOf(time);
							if (!blockingTimeMoments.contains(lTime)) {
								blockingTimeMoments.add(lTime);
							}
						}
						catch(NumberFormatException e){
							printErr("Error in a time-based condition, "+expression_value+" is not time or date");
						}
					}
				}
			}
		}
		resultSet.removeSolutions(toRemove);

		if(DEBUG_PRINT) System.out.println(st+" ["+(System.currentTimeMillis()-start)+"] "+"("+resultSet.getNumberOfSolutions()+" solutions)");

		return true;
	}
	
	/** Locates all variables referred (not defined) in a query statement*/
	public static void findAllVarsInStatement(SemanticStatement st, SaplModel targetModel, Set<String> match, Set<String> calculated, Set<String> used) {

		if(SaplConstants.log_predicates.contains(st.predicate) || st.predicate.equals(SaplConstants.expression)){
			if(SaplConstants.log_predicates.contains(st.predicate)){
				String subVar = st.getSubjectVariableName();
				if(subVar!=null){
					used.add(subVar);
				}
			}
			else{
				String subVar = st.getSubjectVariableName();
				if(subVar!=null) calculated.add(subVar);
			}
			if (!st.isObjectExplicitLiteral() && !st.isObjectURI() && !st.isObjectBlankNode() && st.object.length() >= 2){  
				List<String> tokens = ExpressionEvaluator.tokenize(st.object);
				for(String token : tokens){
					if(token.startsWith(SaplConstants.VARIABLE_PREFIX)){
						String var = token.substring(SaplConstants.VARIABLE_PREFIX.length());
						used.add(var);
					}
				}
			}
		}
		else if(SaplConstants.aggregate_predicates.contains(st.predicate)){
			String subVar = st.getSubjectVariableName();
			if(subVar!=null) calculated.add(subVar);
			
			if (st.predicate.equals(SaplConstants.count) || st.predicate.equals(SaplConstants.groupedBy) || st.predicate.equals(SaplConstants.distinctBy)){
				String[] vars = st.object.split(",");
				for(int i=0; i<vars.length; i++){
					String trimmed = vars[i].trim();
					if(trimmed.startsWith(SaplConstants.VARIABLE_PREFIX)){
						String var = trimmed.substring(SaplConstants.VARIABLE_PREFIX.length());
						used.add(var);
					}
					else if(st.predicate.equals(SaplConstants.count) && trimmed.equals(SaplConstants.ANYTHING))
						used.add(trimmed);
				}
			}
			else{
				String obVar = st.getObjectVariableName();
				if(obVar!=null){
					used.add(obVar);
				}
			}
		}
		else if(st.predicate.equals(SaplConstants.ID) && st.isSubjectContext()) {
			String obVar = st.getObjectVariableName();
			if(obVar!=null) calculated.add(obVar);
		}
		else if(st.predicate.equals(SaplConstants.rdfType)
				&& (st.object.equals(SaplConstants.Context) || st.object.equals(SaplConstants.Variable))){
			String subVar = st.getSubjectVariableName();
			if(subVar!=null){
				used.add(subVar);
			}
		}
		else if(st.predicate.equals(SaplConstants.hasMember) || st.predicate.equals(SaplConstants.memberOf)){
			String subVar = st.getSubjectVariableName();
			if(subVar!=null){
				if(st.predicate.equals(SaplConstants.hasMember))
					used.add(subVar);
				else 
					match.add(subVar);
			}
			String obVar = st.getObjectVariableName();
			if(obVar!=null){
				if(st.predicate.equals(SaplConstants.memberOf))
					used.add(obVar);
				else 
					match.add(obVar);
			}
		}
		else{
			String var = st.getPredicateVariableName();
			if(var!=null) match.add(var);
			var = st.getSubjectVariableName();
			if(var!=null) match.add(var);
			else if(st.isSubjectBlankNode()) match.add(st.subject);
				
			var = st.getObjectVariableName();
			if(var!=null) match.add(var);
			else if(st.isObjectBlankNode()) match.add(st.object);			
		}
		
		if(st.isSubjectContext() && targetModel!=null){
			ContextContainer c = targetModel.contexts.get(st.subject);
			for(String stID : c.members){
				findAllVarsInStatement(targetModel.beliefs.get(stID), targetModel, match, calculated, used);
			}
			
			//mark presence of or / Optional
			if(st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.Optional))
				match.add("");
			else if(st.predicate.equals(SaplConstants.or) && st.isObjectContext())
				match.add("");
		}
		
		if(st.isObjectContext() && targetModel!=null){
			ContextContainer c = targetModel.contexts.get(st.object);
			for(String stID : c.members){
				findAllVarsInStatement(targetModel.beliefs.get(stID), targetModel, match, calculated, used);
			}
		}
		
		//also mark presence of is-true
		if(st.isSubjectVar() && st.predicate.equals(SaplConstants.is) && st.object.equals(SaplConstants.TRUE))
			match.add(" ");

		if(!calculated.isEmpty()){
			if(!match.isEmpty()) match.removeAll(calculated);
			if(!used.isEmpty()) used.removeAll(calculated);
		}
		if(!used.isEmpty() && !match.isEmpty()) used.removeAll(match);

	}

	/**Checks a {} s:or/s:and {} statement if it contains only filters/expressions.
	 * If any normal matching statements are found, false is returned*/
	private static boolean checkIfFiltersOnlyComboStatement(SemanticStatement st, SaplModel targetModel){
		if(!((st.predicate.equals(SaplConstants.or) || st.predicate.equals(SaplConstants.and)) && st.isSubjectContext() && st.isObjectContext())) return false;

		for(String innerID : targetModel.contexts.get(st.subject).members){
			SemanticStatement innerSt = targetModel.beliefs.get(innerID);
			if(SaplConstants.log_predicates.contains(innerSt.predicate) || innerSt.predicate.equals(SaplConstants.expression) || (innerSt.predicate.equals(SaplConstants.rdfType)
					&& (innerSt.object.equals(SaplConstants.Context) || innerSt.object.equals(SaplConstants.Variable)))){
			}
			else if((innerSt.predicate.equals(SaplConstants.or) || innerSt.predicate.equals(SaplConstants.and))){
				if(!checkIfFiltersOnlyComboStatement(innerSt, targetModel)) return false;
			}
			else return false;
		}
		for(String innerID : targetModel.contexts.get(st.object).members){
			SemanticStatement innerSt = targetModel.beliefs.get(innerID);
			if(SaplConstants.log_predicates.contains(innerSt.predicate) || innerSt.predicate.equals(SaplConstants.expression) || (innerSt.predicate.equals(SaplConstants.rdfType)
					&& (innerSt.object.equals(SaplConstants.Context) || innerSt.object.equals(SaplConstants.Variable)))){
			}
			else if((innerSt.predicate.equals(SaplConstants.or) || innerSt.predicate.equals(SaplConstants.and))){
				if(!checkIfFiltersOnlyComboStatement(innerSt, targetModel)) return false;
			}
			else return false;
		}
		
		return true;
	}
	
	/** Checks a condition involving a special predicate, such as rdf:type or sapl:eq
	 * @param value1 Value of the left
	 * @param predicate Predicate
	 * @param value2 Value of the right
	 * @return True if the condition described by values and predicate holds
	 * */
	public static boolean checkValue(String value1, String predicate, String value2) {
		if (predicate.equals(SaplConstants.rdfType) && value2.equals(SaplConstants.Context)) {
			if (value1.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				return true;
			}
			return false;
		}
		if (predicate.equals(SaplConstants.rdfType) && value2.equals(SaplConstants.Variable)) {
			if (value1.startsWith(SaplConstants.VARIABLE_PREFIX)) {
				return true;
			}
			return false;
		}

		if (predicate.equals(SaplConstants.regex)) {
			return value1.matches(value2);
		}

		boolean numberValues = true;
		double v1 = 0, v2 = 0;
		try {
			v1 = Double.valueOf(value1).doubleValue();
			v2 = Double.valueOf(value2).doubleValue();
		} catch (NumberFormatException e) {
			numberValues = false;
		}
		
		if(numberValues) return checkNumericValue(v1, predicate, v2);
		else return checkStringValue(value1, predicate, value2);

	}
	
	/** Checks a condition involving a comparison predicate such as sapl:eq for numeric values
	 * @param v1
	 * @param predicate
	 * @param v2
	 * @return True if the condition described by values and predicate holds
	 */
	public static boolean checkNumericValue(double v1, String predicate, double v2) {
		if (predicate.equals(SaplConstants.eq)) {
			if(Double.isNaN(v1) && Double.isNaN(v2)) return true;
			return v1 == v2;
		} else if (predicate.equals(SaplConstants.neq)) {
			if(Double.isNaN(v1) && Double.isNaN(v2)) return false;
			return v1 != v2;
		} else if (predicate.equals(SaplConstants.gt)) {
			return v1 > v2;
		} else if (predicate.equals(SaplConstants.lt)) {
			return v1 < v2;
		} else if (predicate.equals(SaplConstants.gte)) {
			return v1 >= v2;
		} else if (predicate.equals(SaplConstants.lte)) {
			return v1 <= v2;
		} else {
			return false;
		}		
	}
	
	/** Checks a condition involving a comparison predicate such as sapl:eq for string values
	 * @param v1
	 * @param predicate
	 * @param v2
	 * @return True if the condition described by values and predicate holds
	 */
	public static boolean checkStringValue(String value1, String predicate, String value2) {
		if (predicate.equals(SaplConstants.eq)) {
			return value2.equals(SaplConstants.ANYTHING) || value1.equals(value2);
		} else if (predicate.equals(SaplConstants.neq)) {
			return !value2.equals(SaplConstants.ANYTHING) && !value1.equals(value2);
		} else if (predicate.equals(SaplConstants.gt)) {
			return value1.compareTo(value2) > 0;
		} else if (predicate.equals(SaplConstants.lt)) {
			return value1.compareTo(value2) < 0;
		} else if (predicate.equals(SaplConstants.gte)) {
			return value1.compareTo(value2) >= 0;
		} else if (predicate.equals(SaplConstants.lte)) {
			return value1.compareTo(value2) <= 0;
		} else {
			return false;
		}		
	}	
	
	/** Searches for actual query inside all layers of wrappings like {} sapl:All ?x , {} sapl:Some ?x,
	 * {} sapl:OrderBy ?x, {} sapl:Limit X and {} sapl:Offset X. Used by evaluateConext().
	 * @param queryContextID ID of the query context in the S-APL model
	 * @param targetContextID ID of the target (data) context in the S-APL model
	 * @param targetModel S-APL model
	 * @param recordWrappings If true, all wrappings are recorded to corresponding structures of the result set
	 * @return ID of the final inner context of the query after all wrappings skipped*/ 
	public static String unwrapSetPredicates(String queryContextID, String targetContextID, SaplModel targetModel, SaplQueryResultSet resultSet, boolean recordWrappings) {
		if(targetContextID.equals(SaplConstants.GENERAL_CONTEXT)){
			SemanticStatement st = null;
			if (queryContextID.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				List<String> m = targetModel.contexts.get(queryContextID).members;
				if (m.size() == 1) {
					st = targetModel.beliefs.get(m.get(0));
				}
			}
			else if(queryContextID.startsWith(SaplConstants.STATEMENT_PREFIX)){
				st = targetModel.beliefs.get(queryContextID);
			}

			if(st!=null){
				if (st.isSubjectContext()
						&& (SaplConstants.set_predicates.contains(st.predicate) || SaplConstants.set2_predicates.contains(st.predicate))) {
					if (st.predicate.equals(SaplConstants.All)) {
						if (recordWrappings){ 
							if(st.isObjectVar()) resultSet.forAll.add(st.getObjectVariableName());
							else if(st.isObjectAny()) resultSet.forAll.add(st.object);
						}
						return SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, recordWrappings);
					} else if (st.predicate.equals(SaplConstants.Some)) {
						if (recordWrappings) {
							if(st.isObjectVar()) resultSet.forSome.add(st.getObjectVariableName());
							else if(st.isObjectAny()) resultSet.forSome.add(st.object);
						}
						return SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, recordWrappings);
					} else if (st.predicate.equals(SaplConstants.OrderBy)) {
						if (recordWrappings && st.isObjectVar()) {
							resultSet.orderBy.add(st.getObjectVariableName());
						}
						return SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, recordWrappings);
					} else if (st.predicate.equals(SaplConstants.OrderByDescending)) {
						if (recordWrappings && st.isObjectVar()) {
							resultSet.orderBy.add(" " + st.getObjectVariableName());
						}
						return SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, recordWrappings);
					} else if (st.predicate.equals(SaplConstants.Limit)) {
						if (recordWrappings) {
							try {
								int value = Integer.valueOf(st.object);
								if (value < 1) {
									throw new NumberFormatException();
								}
								resultSet.limit = value;
							} catch (NumberFormatException e) {
								printErr("Error in defining Limit: the object must be integer > 0 : Ignored");
							}
						}
						return SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, recordWrappings);
					} else if (st.predicate.equals(SaplConstants.Offset)) {
						if (recordWrappings) {
							try {
								int value = Integer.valueOf(st.object);
								if (value < 0) {
									throw new NumberFormatException();
								}
								resultSet.offset = value;
							} catch (NumberFormatException e) {
								printErr("Error in defining Offset: the object must be integer >= 0 : Ignored");
							}
						}
						return SaplQueryEngine.unwrapSetPredicates(st.subject, targetContextID, targetModel, resultSet, recordWrappings);
					}
				}
			}
		}
		return queryContextID;
	}


	/**Evaluates a complete query given by a context in a S-APL model against another context in the same model
	 * @param queryID ID of the query context in the S-APL model (or ID of a single statement)
	 * @param targetID ID of the target (data) context in the S-APL model
	 * @param targetModel S-APL model
	 * @param resultSet Empty set of solutions to query, which the method will fill. If the query does not have any variables, resultSet will remain empty, even if the method will return true (direct match).
	 * @param removeMode Whether the query is run as "remove by pattern" operation - to record the links that would be removed 
	 * @param idVars Empty list of variables used to match statement IDs
	 * @param blockingTimeMoments Current list for remembering future time moments used in the query as a filter blocking the query from gaining results
	 * @return True if any solution if found. If the query did not have any variables, resultSet will be empty however. 
	 * */
	public static boolean evaluateQuery(String queryContextID, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants, SaplQueryResultSet resultSet, boolean removeMode,  List<Long> blockingTimeMoments){
		if(targetContextID==null) targetContextID = SaplConstants.GENERAL_CONTEXT;
		String queryId = idgenerator.getNewID();
		if(DEBUG_PRINT) System.out.println("\nQUERY "+queryId+" START");
		boolean result = evaluateContext(queryId, queryContextID, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, null, new ArrayList<String>(1), blockingTimeMoments);
		if(DEBUG_PRINT) System.out.println("QUERY "+queryId+" END: " + result+'\n');
		ExpressionEvaluator.cleanAfterQuery(queryId);
		if(!result) resultSet.solutions.clear();
		return result;
	}

	public static boolean evaluateQuery(List<String> queryStatementIDs, String targetContextID, SaplModel targetModel, boolean symmetricQuerying, boolean treatBNodesConstants, SaplQueryResultSet resultSet, boolean removeMode,  List<Long> blockingTimeMoments){
		if(targetContextID==null) targetContextID = SaplConstants.GENERAL_CONTEXT;
		String queryId = idgenerator.getNewID();
		if(DEBUG_PRINT) System.out.println("\nQUERY "+queryId+" START");
		boolean result =  evaluateContext(queryId, queryStatementIDs, targetContextID, targetModel, symmetricQuerying, treatBNodesConstants, resultSet, removeMode, null, new ArrayList<String>(1), blockingTimeMoments);
		if(DEBUG_PRINT) System.out.println("QUERY "+queryId+" END: " + result+'\n');
		ExpressionEvaluator.cleanAfterQuery(queryId);
		if(!result) resultSet.solutions.clear();
		return result; 
	}


	/**Evaluates a complete query given by N3 S-APL string against a context in a model
	 * @param query Query
	 * @param targetID ID of the target (data) context in the S-APL model
	 * @param targetModel S-APL model
	 * @param resultSet Empty set of solutions to query, which the method will fill. If the query does not have any variables, resultSet will remain empty, even if the method will return true (direct match).
	 * @return True if any solution if found. If the query did not have any variables, resultSet will be empty however.
	 */
	public static boolean evaluateQueryN3(String query, String targetID, SaplModel targetModel, boolean symmetricQuerying, SaplQueryResultSet resultSet){
		List<String> code = SaplN3Parser.compile(query, "evaluateQueryN3").getCode();

		if (code != null) {
			String cid = targetModel.contextIDGenerator.getNewContextID();
			boolean result;
			synchronized (targetModel.lock) {
				targetModel.putContext(cid);
				SaplN3Parser.load(code, targetModel, cid, null, "evaluateQueryN3", true);
				result = evaluateQuery(cid, targetID, targetModel, symmetricQuerying, false, resultSet, false, null);
				targetModel.removeContext(cid);
			}
			if(!result) resultSet.solutions.clear();
			return result;
		}
		return false;
	}

}
