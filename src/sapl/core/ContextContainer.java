package sapl.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.List;

/**
 * Representation of a Context, i.e. a container for statements (aka. 'model' in N3)
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä)
 * 
 * @version 4.2 (25.10.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */  

public class ContextContainer{

	/**The list of IDs of SemanticStatements that belong to this context*/
	public List<String> members; //IDs

	/**ID of the SemanticStatement about this context (statement that defines the meaning of the context)*/
	public String referencedBy = null;

	/**Times the context appears in statements. Normally it is equal to 1 for all except for General Context for which it is 0. 
	 * If it is more than 1, it means that the context is linked also to a running RAB */
	public int referenced = 0;

	/**An index map for speeding up searching for a statement in the context
	 * Maps a string value to the list of all statements having that value as the subject*/
	public Map<String, Set<String>> index_sub = null;
	/**An index map for speeding up searching for a statement in the context
	 * Maps a string value to the list of all statements having that value as the predicate*/
	public Map<String, Set<String>> index_pred = null;
	/**An index map for speeding up searching for a statement in the context
	 * Maps a string value to the list of all statements having that value as the object*/
	public Map<String, Set<String>> index_ob = null;

	/**Vector containing all IDs of member statements which have variables in them.
	 * Used for speeding up searches*/
	public List<String> statementsWithVariables = null;

	/**Default constructor*/
	public ContextContainer() {
		this(4); //4 is a good number, the membersize of Sets has to be a power of 2 and 2 would be too small.
	}

	/**Constructor specifying the initial capacity for the members list*/
	public ContextContainer(int membersSize) {
		this.members = new ArrayList<String>(membersSize);
		this.index_sub = new HashMap<String, Set<String>>(membersSize);
		this.index_pred = new HashMap<String, Set<String>>(membersSize);
		this.index_ob = new HashMap<String, Set<String>>(membersSize);
	}

	/** Returns simple string representation for the context including the IDs of all member statements */ 
	@Override
	public String toString() {
		return this.members.toString() ; //+ " (" + String.valueOf(this.referenced) + ")";
	}


	/**Adds a statement ID to a context container's index
	 * @param key The key, i.e. the value of the subject, predicate, or object of the statement, depending on the index in question
	 * @param statementID ID of the statement
	 * @param index Reference to the index in question  
	 * */
	void addToIndex(String key, String statementID, Map<String, Set<String>> index) {
		Set<String> hs = index.get(key);
		if (hs == null) {
			hs = new HashSet<String>(1);
			hs.add(statementID);
			index.put(key, hs);
		} else {
			hs.add(statementID);
		}
	}

	/**Adds a statement to a context container's indices
	 * @param ss Statement to add to indices
	 * @param statementID ID of that statement
	 * @param cont Context container to which the statement is being added
	 * */
	void addToIndices(SemanticStatement ss, String statementID) {
		addToIndex(ss.subject, statementID, index_sub);
		addToIndex(ss.predicate, statementID, index_pred);
		addToIndex(ss.object, statementID, index_ob);
		if (ss.isSubjectAny() || ss.isPredicateAny() || ss.isObjectAny()) {
			if (statementsWithVariables == null) {
				statementsWithVariables = new ArrayList<String>();
			}
			statementsWithVariables.add(statementID);
		}
	}

	/**Removes a statement ID from a context container's index
	 * @param key The key, i.e. the value of the subject, predicate, or object of the statement, depending on the index in question
	 * @param statementID ID of the statement
	 * @param index Reference to the index in question  
	 * */
	void removeFromIndex(String key, String statementID, Map<String, Set<String>> index) {
		Set<String> hs = index.get(key);
		if (hs.size() > 1) {
			hs.remove(statementID);
		} else {
			index.remove(key);
		}
	}

	/**Removes a statement from a context container's indices
	 * @param ss Statement to remove from indices
	 * @param statementID ID of that statement
	 * @param cont Context container from where the statement is being removed
	 * */
	void removeFromIndices(SemanticStatement ss, String statementID) {
		removeFromIndex(ss.subject, statementID, index_sub);
		removeFromIndex(ss.predicate, statementID, index_pred);
		removeFromIndex(ss.object, statementID, index_ob);
		if ((statementsWithVariables != null) && statementsWithVariables.contains(statementID)) {
			statementsWithVariables.remove(statementID);
		}
	}
	
}
