package sapl.core;

import java.util.List;

/**
 * This abstract class has to be extended in order to implement a custom function 
 * to be used within s:expression and query filters (>, =, etc.).
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.6 (21.02.2018)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public abstract class Function {
	
	protected String queryId;

	void setQueryId(String queryId){
		this.queryId = queryId;
	}
	
	/**Evaluate the function for a list of passed operands. 
	 * Return the evaluation result. Can also return null, 
	 * in that case the errorMessage has to be set.
	 * @param operands The list of function operands.
	 * @param errorMessage Error message, if function evaluation cannot be done, e.g. wrong number of operands.
	 * @return Function evaluation result, or null + error message
	 * */
	public abstract String evaluate(List<String> operands, StringBuilder errorMessage) throws Exception;
	
}
