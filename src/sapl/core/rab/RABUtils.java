package sapl.core.rab;

import java.util.HashMap;

import sapl.core.SaplActionEngine;
import sapl.core.SaplConstants;
import sapl.util.StreamingUtils;
import sapl.util.StringEscape;

/**
 * A collection of static methods for RABs to use 
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT since April 2009) 
 * 
 * @version 4.6 (28.02.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009 Artem Katasonov
 * 
 * */

public class RABUtils {

	//escapes a String
	public static String escapeString(String str) {
		return StringEscape.escape(str);
	}

	//un-escapes a String
	public static String unescapeString(String str) {
		return StringEscape.unEscape(str);
	}

	//saves result of a RAB operation.
	//saveTo can be either filename or a context container specifying a pattern with ?result variable
	public static boolean putResult(SaplActionEngine myEngine, String result, String saveTo, String encoding, boolean appendIfFile) {
		if (saveTo.startsWith(SaplConstants.CONTEXT_PREFIX)) {
			//saving to beliefs
			HashMap<String, String> vars = new HashMap<String, String>();
			vars.put("result", result);
			myEngine.addBeliefs(saveTo, vars);
			return true;
		}
		else{
			if(encoding==null) encoding = "UTF-8";
			return StreamingUtils.writeToFile(saveTo, result, appendIfFile, encoding);
		}
	}

	public static boolean putResult(SaplActionEngine myEngine, String result, String saveTo) {
		return putResult(myEngine, result, saveTo, null, false);
	}

	public static boolean putResult(SaplActionEngine myEngine, String result, String saveTo, boolean appendIfFile) {
		return putResult(myEngine, result, saveTo, null, appendIfFile);
	}

}
