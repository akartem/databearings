package sapl.core.rab;

/**
 * An exception class thrown during RAB initialization
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 * 
 * */

public class InvalidResourceException extends Exception {
	private static final long serialVersionUID = 1L;
	private final String value;

	public InvalidResourceException(String value) {
		this(value, null);
	}

	public InvalidResourceException(String value, Exception e) {
		super("The given resource '" + value + "'is not a valid URI.", e);
		this.value = value;
	}

	public String getViolatingResource() {
		return this.value;
	}
}
