package sapl.core.rab;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.SaplConstants;
import sapl.core.rab.IllegalParameterConfigurationException.State;

/**
 * Collection maintaining a list of BehaviorStartParameters. 
 * The collection can be queried for values of certain parameters. 
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.3 (25.08.2015)
 * 
 * Copyright (C) 2013-2015, VTT
 * Copyright (C) 2009, Michael Cochez
 * 
 * */ 

public class BehaviorStartParameters {
	/**
	 * The map of Resource (parameterName) to String (parameter Value) It is a multimap, because parameters can be multi valued.
	 * We expect an average RAB to have 5 parameters and one value for each parameter.
	 */
	private final HashMap<Resource, Set<String>> parameters = new HashMap<Resource, Set<String>>();

	/**
	 * A behaviour can have parameters like sapl:Success sapl:add ... and so on. This class keeps references to the ID's of the contexts.
	 * Those values will be empty if they are not set. Otherwise ID's of contexts. These fields are only visible in this package, rab's do
	 * not need direct access to these variables.
	 */
	public final List<String> addOnStart = new LinkedList<String>();
	public final List<String> addOnEnd = new LinkedList<String>();
	public final List<String> addOnFail = new LinkedList<String>();
	public final List<String> addOnSuccess = new LinkedList<String>();

	public final List<String> removeOnStart = new LinkedList<String>();
	public final List<String> removeOnEnd = new LinkedList<String>();
	public final List<String> removeOnFail = new LinkedList<String>();
	public final List<String> removeOnSuccess = new LinkedList<String>();

	/**
	 * Add a parameter to the collection.
	 * 
	 * @param parameter
	 *            The resource of the parameter.
	 * @param value
	 *            The value of the parameter.
	 */
	public void add(Resource parameter, String value) {
		Set<String> currentSet = this.parameters.get(parameter);
		if (currentSet == null){
			currentSet = new HashSet<String>();
			this.parameters.put(parameter, currentSet);
		}
		currentSet.add(value);
	}

	/**
	 * get an unmodifiable collection containing all parameters added.
	 * 
	 * @return
	 */
	Map<Resource, Set<String>> getParameters() {
		return Collections.unmodifiableMap(this.parameters);
	}	

	/**
	 * Retrieves the value of the parameter from the collection. If the no such parameter exists, null is returned.
	 * 
	 * @param name
	 *            the parameter name
	 * @return the value of the parameter
	 * @throws IllegalParameterConfigurationException
	 *             If multiple values are specified for this parameter.
	 */
	public String getParameterValue(Resource name) throws IllegalParameterConfigurationException {
		Set<String> possibilities = this.parameters.get(name);
		int count = possibilities==null? 0 : possibilities.size();

		switch (count) {
		case 0:
			return null;
		case 1:
			return possibilities.iterator().next();
		default:
			throw new IllegalParameterConfigurationException(name, possibilities.toString(), State.multivalued);
		}
	}

	/**
	 * retrieves all the values of the parameter with the given parameter name from the collection in an unmodifiable collection
	 * 
	 * @param name
	 *            the parameter name
	 * @return the values bound to this parameter
	 */
	public Collection<String> getParameterValues(Resource name) {
		Set<String> values = this.parameters.get(name);
		if(values==null) return null;
		else return Collections.unmodifiableCollection(values);
	}

	//////////convenience methods for getting parameter values.

	//String methods//

	/**
	 * Returns a String representation of the value for that parameter
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter.
	 * @throws IllegalParameterConfigurationException
	 *             If there is no argument with name name
	 */
	public String getObligatoryStringParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getParameterValue(name);
		if (parameterValue == null) {
			throw new IllegalParameterConfigurationException(name, null, State.missing);
		}
		return parameterValue;
	}

	/**
	 * Returns a non empty String representation of the value for that parameter
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter.
	 * @throws IllegalParameterConfigurationException
	 *             If there is no argument with name name or the argument value is empty.
	 */
	public String getObligatoryNonEmptyStringParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getObligatoryStringParameter(name);
		if (parameterValue.equals("")) {
			throw new IllegalParameterConfigurationException(name, "", State.notInRange);
		}
		return parameterValue;
	}

	/**
	 * Returns a String representation of the value for that parameter if it exists, otherwise null is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter or null if it doesn't exist.
	 * @throws IllegalParameterConfigurationException
	 *             thrown if more as one binding for this parameter is found.
	 */
	public String getOptionalStringParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getParameterValue(name);
		return parameterValue;
	}

	/**
	 * Returns a String representation of the value for that parameter if it exists, otherwise null is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param defaultValue
	 *            The default value if the parameter does not exist.
	 * @return The value of the parameter or null if it doesn't exist.
	 * @throws IllegalParameterConfigurationException
	 *             thrown if more as one binding for this parameter is found.
	 */
	public String getOptionalStringParameter(Resource name, String defaultValue) throws IllegalParameterConfigurationException {
		String parameterValue = this.getOptionalStringParameter(name);
		if (parameterValue == null) {
			return defaultValue;
		}
		return parameterValue;
	}

	//container ID methods//

	/**
	 * Get the value for the given resource. If the value is not specified, or is not a container ID, an
	 * {@link IllegalParameterConfigurationException} is thrown.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The String representation of the container ID
	 * @throws IllegalParameterConfigurationException
	 *             If no such parameter is specified or the value is not a container ID.
	 * 
	 */
	public String getObligatoryContainerID(Resource name) throws IllegalParameterConfigurationException {
		String value = this.getObligatoryNonEmptyStringParameter(name);
		if (!value.startsWith(SaplConstants.CONTEXT_PREFIX)) {
			throw new IllegalParameterConfigurationException(name, value, State.notAContainer);
		}
		return value;
	}

	/**
	 * Get the value for the given resource. If the value is not a container ID, an {@link IllegalParameterConfigurationException} is
	 * thrown. If the value is not specified, null is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The String representation of the container ID or null if not specified.
	 * @throws IllegalParameterConfigurationException
	 *             If the parameter is specified and the value is not a container ID.
	 * 
	 */
	public String getOptionalContainerID(Resource name) throws IllegalParameterConfigurationException {
		return this.getOptionalContainerID(name, null);
	}

	/**
	 * Get the value for the given resource. If the value is not a container ID, an {@link IllegalParameterConfigurationException} is
	 * thrown. If the value is not specified, the default value is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param defaultValue
	 *            The default value which will be returned in case this parameter is not specified.
	 * @return The String representation of the container ID
	 * @throws IllegalParameterConfigurationException
	 *             If the parameter is specified and the value is not a container ID.
	 * 
	 */
	public String getOptionalContainerID(Resource name, String defaultValue) throws IllegalParameterConfigurationException {
		String value = this.getOptionalStringParameter(name);
		if (value == null) {
			return defaultValue;
		}
		if (!value.startsWith(SaplConstants.CONTEXT_PREFIX)) {
			throw new IllegalParameterConfigurationException(name, value, State.notAContainer);
		}
		return value;
	}

	//boolean methods//

	/**
	 * Returns a boolean representation of the value for that parameter
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter.
	 * @throws IllegalParameterConfigurationException
	 *             If there is no argument with name name or the value is not "true" or "false"
	 */
	public boolean getObligatoryBooleanParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getObligatoryStringParameter(name);
		if (parameterValue.equals("true")) {
			return true;
		} else if (parameterValue.equals("false")) {
			return false;
		} else {
			throw new IllegalParameterConfigurationException(name, parameterValue, State.notBooleanValue);
		}
	}

	/**
	 * Returns a Boolean representation of the value for that parameter if it exists, otherwise null is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter or null if it doesn't exist.
	 * @throws IllegalParameterConfigurationException
	 *             If there is a value but it is none of "true" or "false". Or, if there are multiple bindings for this variable.
	 */
	public Boolean getOptionalBooleanParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getOptionalStringParameter(name);
		if (parameterValue == null) {
			return null;
		} else if (parameterValue.equals("true")) {
			return true;
		} else if (parameterValue.equals("false")) {
			return false;
		} else {
			throw new IllegalParameterConfigurationException(name, parameterValue, State.notBooleanValue);
		}
	}

	/**
	 * Returns a Boolean representation of the value for that parameter if it exists, otherwise the default is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param defaultValue
	 *            The default value, which will be returned in case this parameter is not specified.
	 * @return The value of the parameter or null if it doesn't exist.
	 * @throws IllegalParameterConfigurationException
	 *             If there is a value but it is none of "true" or "false". Or, if there are multiple bindings for this variable.
	 */
	public boolean getOptionalBooleanParameter(Resource name, boolean defaultValue) throws IllegalParameterConfigurationException {
		Boolean parameterValue = this.getOptionalBooleanParameter(name);
		if (parameterValue == null) {
			return defaultValue;
		}
		return parameterValue;
	}

	//integer methods//

	/**
	 * Returns a int representation of the value for that parameter.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter.
	 * @throws IllegalParameterConfigurationException
	 *             If there is no argument with name name or the value triggers a {@link NumberFormatException} in
	 *             {@link Integer#parseInt(String)}
	 */
	public int getObligatoryNumericParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getObligatoryStringParameter(name);
		try {
			return Integer.parseInt(parameterValue);
		} catch (NumberFormatException e) {
			// parsing of the numeric value went wrong => IllegalParameterConfigurationException
			throw new IllegalParameterConfigurationException(name, parameterValue, State.notNumericValue);
		}
	}

	/**
	 * Returns a Integer representation of the value for that parameter if it exists, otherwise null is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The value of the parameter or null if it doesn't exist.
	 * @throws IllegalParameterConfigurationException
	 *             If the value is specified but not convertible to integer by {@link Integer#parseInt(String)}
	 */
	public Integer getOptionalNumericParameter(Resource name) throws IllegalParameterConfigurationException {
		String parameterValue = this.getOptionalStringParameter(name);
		if (parameterValue == null) {
			return null;
		}
		try {
			return Integer.parseInt(parameterValue);
		} catch (NumberFormatException e) {
			// parsing of the numeric value went wrong => IllegalParameterConfigurationException
			throw new IllegalParameterConfigurationException(name, parameterValue, State.notNumericValue);
		}
	}

	/**
	 * Returns a Integer representation of the value for that parameter if it exists, otherwise the default is returned.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param defaultValue
	 *            The value which will be returned in case the parameter is not specified.
	 * @return The value of the parameter or null if it doesn't exist.
	 * @throws IllegalParameterConfigurationException
	 *             If the value is specified but is not convertible to integer by {@link Integer#parseInt(String)}
	 */
	public int getOptionalNumericParameter(Resource name, int defaultValue) throws IllegalParameterConfigurationException {
		Integer parameterValue = this.getOptionalNumericParameter(name);
		if (parameterValue == null) {
			return defaultValue;
		}
		return parameterValue;
	}

	//file methods//

	/**
	 * Returns the {@link File} for the value of the parameter
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The File.
	 * @throws IllegalParameterConfigurationException
	 *             If there is no argument with name name
	 */
	public File getObligatoryFileParameter(Resource name) throws IllegalParameterConfigurationException {
		String fileName = this.getObligatoryNonEmptyStringParameter(name);
		return new File(fileName);
	}

	/**
	 * Returns the {@link File} for the value of the parameter or null if there is no such parameter.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The File.
	 * @throws IllegalParameterConfigurationException
	 *             Never thrown at the moment, might be if more checks get implemented.
	 */
	public File getOptionalFileParameter(Resource name) throws IllegalParameterConfigurationException {
		String fileName = this.getOptionalStringParameter(name);
		if (fileName == null) {
			return null;
		}
		return new File(fileName);
	}

	/**
	 * Returns the {@link File} for the value of the parameter or the default if there is no such parameter.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param defaultValue
	 *            The default which will be returned in case this parameter is not specified.
	 * @return The File.
	 * @throws IllegalParameterConfigurationException
	 *             Never thrown at the moment, might be if more checks get implemented.
	 */
	public File getOptionalFileParameter(Resource name, File defaultValue) throws IllegalParameterConfigurationException {
		File parameterValue = this.getOptionalFileParameter(name);
		if (parameterValue == null) {
			return defaultValue;
		}
		return parameterValue;
	}

	//URL methods//

	/**
	 * Returns the {@link URL} for the value of the parameter
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The URL.
	 * @throws IllegalParameterConfigurationException
	 *             If there is no argument with name name or does not have a value which is a valid URL
	 *             this object?
	 */
	public URL getObligatoryURLParameter(Resource name) throws IllegalParameterConfigurationException {
		String urlString = this.getObligatoryStringParameter(name);
		try {
			return new URL(urlString.replace(" ","%20"));
		} catch (Exception e) {
			throw new IllegalParameterConfigurationException(name, urlString, State.notInRange);
		}
	}

	/**
	 * Returns the {@link URL} for the value of the parameter or null if there is no such parameter.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @return The URL.
	 * @throws IllegalParameterConfigurationException
	 *             If the argument is given, but does not have a value which is a valid URL. 
	 *             object?
	 */
	public URL getOptionalURLParameter(Resource name) throws IllegalParameterConfigurationException{
		String urlString = this.getOptionalStringParameter(name);
		if (urlString == null) {
			return null;
		}
		try {
			return new URL(urlString.replace(" ","%20"));
		} catch (Exception e) {
			throw new IllegalParameterConfigurationException(name, urlString, State.notInRange);
		}
	}

	/**
	 * Returns the {@link URL} for the value of the parameter or the default if there is no such parameter.
	 * 
	 * @param name
	 *            The name of the parameter.
	 * @param defaultValue
	 *            The default which will be returned in case this parameter is not specified.
	 * @return The URL.
	 * @throws IllegalParameterConfigurationException
	 *             If the argument is given, but does not have a value which is a valid URL
	 *             object?
	 */
	public URL getOptionalURLParameter(Resource name, URL defaultValue) throws IllegalParameterConfigurationException, UnsupportedEncodingException {
		URL parameterValue = this.getOptionalURLParameter(name);
		if (parameterValue == null) {
			return defaultValue;
		}
		return parameterValue;
	}

	//resource methods//

	/**
	 * Returns the {@link Resource} given as a value for the given parameter.
	 * 
	 * @param name
	 *            The parameter name.
	 * @return The {@link Resource} specified as a parameter value.
	 * @throws IllegalParameterConfigurationException
	 *             If the parameter is not specified or is not convertible to a resource.
	 */
	public Resource getObligatoryResourceParameter(Resource name) throws IllegalParameterConfigurationException {
		String resourceString = this.getObligatoryNonEmptyStringParameter(name);
		Resource resource;
		try {
			resource = new Resource(resourceString);
		} catch (InvalidResourceException e) {
			throw new IllegalParameterConfigurationException(name, resourceString, State.notAResource, e);
		}
		return resource;
	}

	/**
	 * Returns the {@link Resource} given as a value for the given parameter or null if the parameter is not specified.
	 * 
	 * @param name
	 *            The parameter name.
	 * @return The {@link Resource} specified as a parameter value or the null if the parameter is not specified.
	 * @throws IllegalParameterConfigurationException
	 *             If the parameter is specified but is not convertible to a resource.
	 */
	public Resource getOptionalResourceParameter(Resource name) throws IllegalParameterConfigurationException {
		return this.getOptionalResourceParameter(name, null);
	}

	/**
	 * Returns the {@link Resource} given as a value for the given parameter or the default if the parameter is not specified.
	 * 
	 * @param name
	 *            The parameter name.
	 * @param defaultValue
	 *            The default value which will be returned in case the parameter is not specified.
	 * @return The {@link Resource} specified as a parameter value or the default if the parameter is not specified.
	 * @throws IllegalParameterConfigurationException
	 *             If the parameter is specified but is not convertible to a resource.
	 */
	public Resource getOptionalResourceParameter(Resource name, Resource defaultValue) throws IllegalParameterConfigurationException {
		String parameterValue = this.getOptionalStringParameter(name);
		if (parameterValue == null) {
			return defaultValue;
		}
		try {
			Resource resource = new Resource(parameterValue);
			return resource;
		} catch (InvalidResourceException e) {
			throw new IllegalParameterConfigurationException(name, parameterValue, State.notAResource, e);
		}

	}
}