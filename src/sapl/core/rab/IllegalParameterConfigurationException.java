package sapl.core.rab;

/**
 * This exception indicates that the parameters are not set correctly, its state indicate 
 * what is wrong with the parameter.
 * 
 * @author Michael Cochez (University of Jyväskylä)
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.3 (27.10.2015)
 * 
 * Copyright (C) 2013-2015, VTT
 * Copyright (C) 2009, Michael Cochez
 * 
 * */ 

public class IllegalParameterConfigurationException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * The state of the {@link IllegalParameterConfigurationException}
	 * 
	 * @author Michael Cochez for UBIWARE project
	 */
	public enum State {

		/**
		 * The parameter is missing.
		 */
		missing("missing"),
		/**
		 * The value was expected to be boolean, but was not.
		 */
		notBooleanValue("not a boolean value"),
		/**
		 * The value was expected to be numerical, but was not.
		 */
		notNumericValue("not a numerical value"),
		/**
		 * The value was of the correct type, but was not in range.
		 */
		notInRange("not in range"),
		/**
		 * The referred object could not be found on the blackboard.
		 */
		notOnBlackboard("not on the blackboard of the agent"),
		/**
		 * The value is not a proper URI for a resource (the structure is wrong)
		 */
		notAResource("not a proper resource URI"),
		/**
		 * Over configuration : this parameter should not co-exist with another parameter.
		 */
		conflicting("conflicting with another parameter"),
		/**
		 * The parameter value is not a container even tough this was expected.
		 */
		notAContainer("not a containerID"),
		/**
		 * The parameter is multi-valued even tough this was expected.
		 */
		multivalued("specified multiple times, while only one specification was expected."),
		/**
		 * 
		 */
		resourceNotFound("The resource referred to by the parameter could not be found."), 

		multiStatement("expected to contain exactly one statement."), 

		empty("expected to contain at least one statement."),

		invalid("invalid");
		
		private String representation;

		private State(String representation) {
			this.representation = representation;
		}

		@Override
		public String toString() {
			return this.representation;
		}
	}

	//private State state;
	//private String parameterName;

	/**
	 * @param name
	 *            The name of the parameter which is wrong
	 * @param parameterValue
	 *            The value which the parameter has, if the parameter was missing, supply null
	 * @param state
	 *            The state in which the parameter is.
	 * @param extraInfo
	 *            More value about why this parameter has an illegal value
	 */
	public IllegalParameterConfigurationException(Resource name, String parameterValue, State state, String extraInfo) {
		super("the parameter with name '" + name + "' with value '" + parameterValue + "' is " + state.toString() + "\n" + extraInfo);
	}

	/**
	 * @param name
	 *            The name of the parameter which is wrong
	 * @param parameterValue
	 *            The value which the parameter has, if the parameter was missing, supply null
	 * @param state
	 *            The state in which the parameter is.
	 * @param cause
	 *            The exception which is causing this parameter to have an illegal value
	 */
	public IllegalParameterConfigurationException(Resource name, String parameterValue, State state, Throwable cause) {
		super("the parameter with name '" + name + "' with value '" + parameterValue + "' is " + state.toString(), cause);
	}

	/**
	 * @param name
	 *            The name of the parameter which is wrong
	 * @param parameterValue
	 *            The value which the parameter has, if the parameter was missing, supply null
	 * @param state
	 *            The state in which the parameter is.
	 */
	public IllegalParameterConfigurationException(Resource name, String parameterValue, State state) {
		super("the parameter with name '" + name + "' with value '" + parameterValue + "' is " + state.toString(), null);
	}
}
