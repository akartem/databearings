package sapl.core.rab;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation of a RAB parameter for IDE
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.3 (29.05.2015)
 * 
 * @since 4.3
 * 
 * Copyright (C) 2015 VTT
 * 
 * */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Parameter {
	String name();
}
