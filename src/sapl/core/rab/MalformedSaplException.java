package sapl.core.rab;

/**
 * An exception class thrown during RAB initialization
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 * 
 * Copyright (C) 2009, Michael Cochez
 * 
 * */

public class MalformedSaplException extends Exception {
	private static final long serialVersionUID = 1L;

	public MalformedSaplException(String message, Throwable cause) {
		super(message, cause);
	}

	public MalformedSaplException(String message) {
		super(message);
	}
}
