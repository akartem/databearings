package sapl.core.rab;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import sapl.util.IDGenerator;

/**
 * Representation of an RDF resource (identified by an URI), used by RABs for their parameters
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.0 (24.10.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2009, Michael Cochez
 * 
 * */

public class Resource{

	private final String value;

	/**
	 * Creates a Resource with the given prefix.
	 * 
	 * @param prefix
	 *            The prefix onto which the qname will be appended
	 * @param qname
	 *            The qname of the resource.
	 * @throws MalformedSaplException
	 *             In case the concatenation of prefix and qname does not result in a valid URI
	 */
	public Resource(URI prefix, String qname) throws MalformedSaplException {
		this(prefix.toString(), qname);
	}

	/**
	 * Creates a Resource with the given prefix.
	 * 
	 * @param prefix
	 *            The prefix onto which the qname will be appended
	 * @param qname
	 *            The qname of the resource.
	 * @throws MalformedSaplException
	 *             In case the concatenation of prefix and qname does not result in a valid URI
	 */
	public Resource(String prefix, String qname) throws MalformedSaplException {
		if (!prefix.endsWith("#") && !prefix.endsWith("/")) {
			System.err.println("Creation of resource with a prefix which does not end in # or / detected. This is valid but strongly discouraged. (prefix="+prefix+", res="+qname+")");
			try {
				throw new Exception();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String resource = prefix + qname;
		try {
			new URL(resource);
		} catch (MalformedURLException e) {
			throw new MalformedSaplException("The concatenation of the given prefix '" + prefix + "' and qname '" + qname
					+ "' did not result in a valid URI.", e);
		}
		int length = resource.length();
		StringBuilder resourceBuilder = new StringBuilder(length);
		resourceBuilder.append(resource);
		this.value = resourceBuilder.toString().intern();
	}

	/**
	 * private constructor to create anonymous nodes
	 * 
	 * @param VOID
	 *            for syntactic difference with the version with string parameter only
	 */
	private Resource(String value, int VOID) {
		this.value = value.intern();
	}

	private static final IDGenerator idgenerator = new IDGenerator("anonymous");

	public static Resource creatAnonymousNode() {
		String value = Resource.idgenerator.getNewID();
		Resource r = new Resource(value, 0);
		return r;
	}

	/**
	 * Creates a Resource from the given String representation. The representation must have opening and closing rectangular braces.
	 * 
	 * @param resource
	 *            The resource in String form.
	 * @throws InvalidResourceException
	 *             In case resource is not a valid URL.
	 */
	public Resource(String resource) throws InvalidResourceException {
		try {
			new URL(resource);
		} catch (MalformedURLException e) {
			throw new InvalidResourceException(resource, e);
		}
		this.value = resource.intern();
	}

	/**
	 * Creates a Resource without the need to catch checked exceptions. An Error is thrown if the resourceString is not a valid Resource
	 * representation.
	 * 
	 * @param resourceString
	 *            A String representing a resource.
	 * @return A Resource.
	 * @throws Error
	 *             If the resourceString does not represent a valid Resource.
	 */
	public static Resource createUncheckedResource(String resourceString) {
		try {
			return new Resource(resourceString);
		} catch (InvalidResourceException e) {
			throw new Error("an unchecked resource creation failed for " + resourceString, e);
		}
	}

	/**
	 * Creates a Resource without the need to catch checked exceptions. An Error is thrown if the preix and qname don't form a valid
	 * Resource together.
	 * 
	 * @param prefix
	 *            The prefix of the URI
	 * @param qname
	 *            the qname which will be appended to the prefix to form the resource.
	 * @return A Resource.
	 * @throws Error
	 *             If the prefix and qname don't form a valid Resource together.
	 */
	public static Resource createUncheckedResource(URI prefix, String qname) {
		try {
			return new Resource(prefix, qname);
		} catch (MalformedSaplException e) {
			throw new Error("an unchecked resource creation failed", e);
		}
	}

	/**
	 * Gives a String representation of this resource. This means including angular brackets.
	 */
	@Override
	public String toString() {
		return this.value;
	}

	@Override
	public int hashCode() {
		return this.value.hashCode();
	}

	@Override
	public boolean equals(java.lang.Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		Resource other = (Resource) obj;
		if (this.value == other.value) {//this speeds up equals since all constructors intern the value.
			return true;
		}
		return this.value.equals(other.value);
	}

}
