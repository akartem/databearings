package sapl.core;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** 
 * Engine for updating an S-APL model based on constructs defined within the model
 * itself (self-update), such as rules and direct update commands.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.6 (26.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov
 * 
 * */

public class SaplUpdateEngine {

	/**Whether symmetric query evaluation is to be used when evaluation rules*/
	public static final boolean SYMMETRIC_QUERYING = false;
	
	/**Whether the engine handles time-related beliefs*/
	public boolean updateTimeBeliefs;

	/**Links scheduled for addition*/
	private List<LinkInfo> addLinks = new ArrayList<LinkInfo>(); //order is important
	/**Links scheduled for removal*/
	private Set<LinkInfo> removeLinks = new HashSet<LinkInfo>();
	/**Statements scheduled for removal*/
	private Set<String> removeBeliefs = new HashSet<String>();
	/**Contexts scheduled for removal*/
	private Set<String> removeContexts = new HashSet<String>();

	//Support for the evaluation edition control
	private List<Long> ruleTimes = null; 
	
	
	/**Constructor
	 * @param updateTimeBeliefs If true, engine will update time-related beliefs on every update
	 */
	public SaplUpdateEngine(boolean updateTimeBeliefs){
		this.updateTimeBeliefs = updateTimeBeliefs;
	}

	/**Prints an error message*/
	private static void printErr(String string) {
		System.err.println("["+SaplModel.getCurrentTime()+"] "+string);
	}

	/**Performs a single round of S-APL updates
	 * @param targetModel S-APL model to work with
	 * @param blockingTimeMoments Empty list for future time moments blocking some rules, method will fill it
	 * @return True if any updates were made to the model, not counting time-related beliefs*/
	public boolean update(SaplModel targetModel, List<Long> blockingTimeMoments){

		if(this.updateTimeBeliefs) this.updateTimeBeliefs(targetModel);

		boolean changed = false; 

		//start by cleaning goals that already been achieved
		if(targetModel.hasMentalContexts){
			this.cleanAchievedGoals(targetModel);
			if(this.commitUpdates(targetModel)) changed = true;
		}
		
		//apply s:add, s:remove, s:erase, in case RABs added those
		this.doDirectUpdates(targetModel);
		if(this.commitUpdates(targetModel)) changed = true;

		//do metarules (to allow catching conditional commitments): all levels except 0
		if(targetModel.hasRuleContexts){
			for(int i = targetModel.metaruleLevels.size()-1; i>=0; i--){
				Integer level = targetModel.metaruleLevels.get(i);
				if(level==0) continue;
				String context = targetModel.metaruleContexts.get(level);
				if(targetModel.contexts.containsKey(context)){
					this.doInferences(targetModel, context, blockingTimeMoments);
					if(this.commitUpdates(targetModel)) changed = true;
					//apply s:add, s:remove, s:erase
					this.doDirectUpdates(targetModel);
					if(this.commitUpdates(targetModel)) changed = true;
				}
			}
		}

		//do rules
		if(targetModel.hasRuleContexts) this.doInferences(targetModel, SaplConstants.RULES_CONTEXT, blockingTimeMoments);
		//do conditional commitments
		this.doInferences(targetModel, SaplConstants.GENERAL_CONTEXT, blockingTimeMoments);
		//commit only after both rules and do conditional commitments are processed 
		if(this.commitUpdates(targetModel)) changed = true;
		//apply s:add, s:remove, s:erase
		this.doDirectUpdates(targetModel);
		if(this.commitUpdates(targetModel)) changed = true;

		//do metarules level 0 (to allow catching RAB executions)
		if(targetModel.hasRuleContexts){
			if(targetModel.metaruleLevels.contains(0)){
				String context = targetModel.metaruleContexts.get(0);
				this.doInferences(targetModel, context, blockingTimeMoments);
				if(this.commitUpdates(targetModel)) changed = true;
				//apply s:add, s:remove, s:erase
				this.doDirectUpdates(targetModel);
				if(this.commitUpdates(targetModel)) changed = true;
			}
		}

		return changed;

	}

	/**Updating time-related beliefs that the update engine itself maintains
	 * */
	public void updateTimeBeliefs(SaplModel targetModel) {

		//current system time in milliseconds
		SemanticStatement st = targetModel.beliefs.get(SaplConstants.NOW_BELIEF);
		if (st == null) {
			st = new SemanticStatement(SaplConstants.Now, SaplConstants.is, "");
			targetModel.putBelief(SaplConstants.NOW_BELIEF, st);
			targetModel.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.NOW_BELIEF);
		}
		ContextContainer c = targetModel.contexts.get(SaplConstants.GENERAL_CONTEXT);
		c.removeFromIndex(st.object, SaplConstants.NOW_BELIEF, c.index_ob);
		st.object = String.valueOf(System.currentTimeMillis());
		c.addToIndex(st.object, SaplConstants.NOW_BELIEF, c.index_ob);

		//current time of the day
		st = targetModel.beliefs.get(SaplConstants.TIME_BELIEF);
		if (st == null) {
			st = new SemanticStatement(SaplConstants.Time, SaplConstants.is, "");
			targetModel.putBelief(SaplConstants.TIME_BELIEF, st);
			targetModel.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.TIME_BELIEF);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ssZZZ");
		GregorianCalendar calendar = new GregorianCalendar();
		String time = sdf.format(calendar.getTime());
		if(time.endsWith("Z"))
			time = time.substring(0, time.length()-1)+"+00:00";
		else time = time.substring(0, time.length()-2)+":"+ time.substring(time.length()-2);

		c.removeFromIndex(st.object, SaplConstants.TIME_BELIEF,c.index_ob);
		st.object = time;
		c.addToIndex(st.object, SaplConstants.TIME_BELIEF,c.index_ob);

		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf2.format(calendar.getTime());

		//current date
		st = targetModel.beliefs.get(SaplConstants.DATE_BELIEF);
		if (st == null) {
			st = new SemanticStatement(SaplConstants.Date, SaplConstants.is, "");
			targetModel.putBelief(SaplConstants.DATE_BELIEF, st);
			targetModel.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.DATE_BELIEF);
		}
		c.removeFromIndex(st.object, SaplConstants.DATE_BELIEF,c.index_ob);
		st.object = date;
		c.addToIndex(st.object, SaplConstants.DATE_BELIEF,c.index_ob);
	}



	/**Does inferences such as sapl:implies
	 * @param targetModel S-APL model to work with
	 * @param blockingTimeMoments List for future time moments blocking some rules, method will add to it
	 * @param context Context where to look for inference statements
	 * */
	private void doInferences(SaplModel targetModel, String context, List<Long> blockingTimeMoments){
		ContextContainer cont = targetModel.contexts.get(context);
		Set<String> statementIDs = new HashSet<String>();
		Set<String> set1 = cont.index_pred.get(SaplConstants.implies);
		if(set1!=null) statementIDs.addAll(set1);
		Set<String> set2 = cont.index_pred.get(SaplConstants.impliesNow);
		if(set2!=null) statementIDs.addAll(set2);
		Set<String> set3 = cont.index_pred.get(SaplConstants.infers);
		if(set3!=null) statementIDs.addAll(set3);
		Set<String> set4 = cont.index_pred.get(SaplConstants.achievedBy);
		if(set4!=null) statementIDs.addAll(set4);

		for(String id : statementIDs){
			SemanticStatement st = targetModel.beliefs.get(id);

			//For the evaluation edition, limit the number of rules evaluated per minute
			if(SaplConstants.EVALUATION_EDITION){
				long now = System.currentTimeMillis();
				if(ruleTimes==null) ruleTimes = new ArrayList<Long>(SaplConstants.EVALUATION_EDITION_RULE_EVALUATIONS_LIMIT);
				if(ruleTimes.size()==SaplConstants.EVALUATION_EDITION_RULE_EVALUATIONS_LIMIT){
					Long first = ruleTimes.remove(0);
					long diff = 60000 - (now - first);
					while(diff>0){
						String a="Lim", b="g ru", c="aluati", d="r min", e="itin", f="o ";
						System.out.println(a+"itin"+b+"le ev"+c+"ons t"+f+SaplConstants.EVALUATION_EDITION_RULE_EVALUATIONS_LIMIT+" pe"+d+"ute. Wa"+e+"g "+diff);
						try {
							Thread.sleep(diff);
						} catch (InterruptedException ex) {}
						now = System.currentTimeMillis();
						diff = 60000 - (now - first);
					}
				}
				ruleTimes.add(now);
			}
			
			// now we will check the rule and get the binding for the variables used
			SaplQueryResultSet resultSet = this.checkRule(st, targetModel, blockingTimeMoments);

			if (resultSet != null) {
				//use of achievedBy causes automatic removal of goals/commitments/events as they are considered 'handled'
				if (st.predicate.equals(SaplConstants.achievedBy)) {
					this.removeHandledMentalAttitudes(targetModel, resultSet);
				}

				//add to G the contents of the right side of the rule, for each solution
				targetModel.clearLocalIDs();
				if (st.predicate.equals(SaplConstants.infers)) {
					this.addLinks.addAll(SaplUpdateEngine.addRuleOutput(resultSet, st.object, targetModel, true));
				} else {
					this.addLinks.addAll(SaplUpdateEngine.addRuleOutput(resultSet, st.object, targetModel, false));
				}
				targetModel.clearLocalIDs();

				//inferences located in G are one-time, so dropped
				if (context.equals(SaplConstants.GENERAL_CONTEXT)) {
					this.removeLinks.add(new LinkInfo(context, id));
					if (st.predicate.equals(SaplConstants.impliesNow)) {
						//find possible sapl:else part just to remove it
						this.checkElsePart(st.subject, targetModel, false);
					}
				}

			} else if (st.predicate.equals(SaplConstants.impliesNow)) {
				//sapl:impliesNow statements dropped when there are no solutions
				this.removeLinks.add(new LinkInfo(context, id));
				
				//find possible sapl:else part and add it to G
				this.checkElsePart(st.subject, targetModel, true);
			}

		}

	}

	/**Evaluates the query given in the left side (IF) of an inference statement
	 * @param st Inference statement
	 * @param targetModel S-APL model to work with
	 * @param blockingTimeMoments List for future time moments blocking some rules, method will add to it
	 * @return Result set for the query 
	 * */
	private SaplQueryResultSet checkRule(SemanticStatement st, SaplModel targetModel, List<Long> blockingTimeMoments) {
		if (!st.isSubjectContext() || !st.isObjectContext()) {
			printErr("Error in rule: Both subject and object must be context containers: " + st);
			return null;
		}

		SaplQueryResultSet resultSet = new SaplQueryResultSet();
		if (SaplQueryEngine.evaluateQuery(st.subject, SaplConstants.GENERAL_CONTEXT, targetModel, SYMMETRIC_QUERYING, false, resultSet, false, blockingTimeMoments)) {

			if (!st.predicate.equals(SaplConstants.infers)) {
				if ((resultSet.offset == 0)
						|| ((resultSet.forAll.size() == 0) && (resultSet.getNumberOfSolutions() > resultSet.offset))
						|| ((resultSet.forAll.size() > 0) && (resultSet.getSolutions(resultSet.forAll).size() > resultSet.offset))) {
					return resultSet;
				}
				else return null;
			} else {

				//For sapl:infers, exclude any solution for which its right side is already true in G
				List<Integer> toRemove = new ArrayList<Integer>();
				for (int i = 0; i < resultSet.getNumberOfSolutions(); i++) {
					List<Map<String, String>> vec = new ArrayList<Map<String, String>>();
					Map<String, String> solution = resultSet.getSolution(i);
					vec.add(solution);
					SaplQueryResultSet temp_resultSet = new SaplQueryResultSet(vec, resultSet.definedVars);

					temp_resultSet.solutionIndices = new HashMap<String, Integer>(resultSet.solutionIndices);
					if (SaplQueryEngine.evaluateQuery(st.object, SaplConstants.GENERAL_CONTEXT, targetModel, SYMMETRIC_QUERYING, false, temp_resultSet, false, null)) {
						toRemove.add(i);
					}
				}
				resultSet.removeSolutions(toRemove);
				if (resultSet.getNumberOfSolutions() == 0) {
					return null;
				}
				return resultSet;

			}
		}
		else return null;
	}

	/**Adds to S-APL model new content given by the right side of an inference statement
	 * @param resultSet Query result set obtained after evaluating the left side of inference
	 * @param contextID Context giving the new content, i.e. the right side of inference
	 * @param targetModel S-APL model to work with
	 * @param inference True if sapl:infers is being processed
	 * @return A list of links to add to commit the copy operation (before that, new content is created but disconnected from the model)
	 * */
	static List<LinkInfo> addRuleOutput(SaplQueryResultSet resultSet, String contextID, SaplModel targetModel, boolean inference) {
		return addRuleOutput(resultSet, targetModel.contexts.get(contextID).members, targetModel, inference);
	}

	static List<LinkInfo> addRuleOutput(SaplQueryResultSet resultSet, List<String> contextMembers, SaplModel targetModel, boolean inference) {

		List<LinkInfo> toAdd = new ArrayList<LinkInfo>();

		List<String> simple = new ArrayList<String>();
		List<SemanticStatement> toUnwrap = new ArrayList<SemanticStatement>();

		//S-APL allows set wrappings like sapl:All to appear also in the right side, so have to process
		for(String id : contextMembers){
			SemanticStatement st = targetModel.beliefs.get(id);
			if (st.isSubjectContext()
					&& (SaplConstants.set_predicates.contains(st.predicate) || SaplConstants.set2_predicates.contains(st.predicate))) {
				toUnwrap.add(st);
			} else {
				simple.add(id);
			}
		}

		//process statements without set wrappings
		if (simple.size() > 0) {
			List<Map<String, String>> vec;
			if (!inference) {
				vec = resultSet.getSolutions(resultSet.forAll, resultSet.forSome,
						resultSet.orderBy, resultSet.limit, resultSet.offset, null, true);
				resultSet.fix(vec);
			} else {
				resultSet.fix(resultSet.solutions);
				vec = resultSet.solutions;
			}
			
			
			
			//For the evaluation edition, limit the number of processed solutions
			if(SaplConstants.EVALUATION_EDITION){
				if(vec.size() > SaplConstants.EVALUATION_EDITION_SOLUTIONS_LIMIT){
					String a="ting ", b=" solu", c ="ns set t", d="om ";
					System.out.println("Limi"+a+"the"+b+"tio"+c+"o "+SaplConstants.EVALUATION_EDITION_SOLUTIONS_LIMIT+" (fr"+d+vec.size()+")");
					vec = vec.subList(0, SaplConstants.EVALUATION_EDITION_SOLUTIONS_LIMIT);
				}
			}
			
			Iterator<Map<String, String>> it = vec.iterator();
			while (it.hasNext()) {
				Map<String, String> vars = it.next();
				toAdd.addAll(SaplUpdateEngine.copy(simple, SaplConstants.GENERAL_CONTEXT, targetModel, vars));

				if (it.hasNext()) {
					targetModel.clearLocalIDs();
				}
			}
		}

		Iterator<SemanticStatement> sit = toUnwrap.iterator();
		List<String> originalAll = new ArrayList<String>(resultSet.forAll);
		List<String> originalSome = new ArrayList<String>(resultSet.forSome);
		List<String> originalOrderBy = new ArrayList<String>(resultSet.orderBy);
		int originalLimit = resultSet.limit;
		int originalOffset = resultSet.offset;

		while (sit.hasNext()) {
			SemanticStatement st = sit.next();
			
			if (st.predicate.equals(SaplConstants.All)) {
				if(st.isObjectVar()) resultSet.forAll.add(st.getObjectVariableName());
				else if(st.isObjectAny()) resultSet.forAll.add(st.object);
			} else if (st.predicate.equals(SaplConstants.Some)) {
				if(st.isObjectVar()) resultSet.forSome.add(st.getObjectVariableName());
				else if(st.isObjectAny()) resultSet.forSome.add(st.object);
			} else if (st.predicate.equals(SaplConstants.OrderBy)) {
				if(st.isObjectVar()) resultSet.orderBy.add(st.getObjectVariableName());
			} else if (st.predicate.equals(SaplConstants.OrderByDescending)) {
				if(st.isObjectVar()) resultSet.orderBy.add(" " + st.getObjectVariableName());
			} else if (st.predicate.equals(SaplConstants.Limit)) {
				try {
					int value = Integer.valueOf(st.object);
					if (value < 1) {
						throw new NumberFormatException();
					}
					resultSet.limit = value;
				} catch (NumberFormatException e) {
					printErr("Error in defining Limit: the object must be integer > 0 : Ignored");
				}
			} else if (st.predicate.equals(SaplConstants.Offset)) {
				try {
					int value = Integer.valueOf(st.object);
					if (value < 0) {
						throw new NumberFormatException();
					}
					resultSet.offset = value;
				} catch (NumberFormatException e) {
					printErr("Error in defining Offset: the object must be integer >= 0 : Ignored");
				}
			}

			toAdd.addAll(SaplUpdateEngine.addRuleOutput(resultSet, st.subject, targetModel, inference));

			resultSet.forAll = new ArrayList<String>(originalAll);
			resultSet.forSome = new ArrayList<String>(originalSome);
			resultSet.orderBy = new ArrayList<String>(originalOrderBy);
			resultSet.limit = originalLimit;
			resultSet.offset = originalOffset;
		}

		return toAdd;

	}


	/** Processes the sapl:else part of sapl:impliesNow statements
	 * @param ifContext ID of the context that has the left side of the inference
	 * @param targetModel targetModel S-APL model to work with
	 * @param add If true, the found else part is added to G. If false, it is only searched to be removed.
	 * */
	private void checkElsePart(String ifContext, SaplModel targetModel, boolean add) {
		Set<String> otherContexts = targetModel.getParallelContexts(ifContext);

		if (otherContexts != null) {
			Iterator<String> it = otherContexts.iterator();
			while (it.hasNext()) {
				String id = it.next();
				String statementID = targetModel.contexts.get(id).referencedBy;
				if (statementID != null) {
					SemanticStatement st = targetModel.beliefs.get(statementID);
					if (this.belongsToRuleContexts(targetModel, st)
							&& st.predicate.equals(SaplConstants.ELSE)) {
						if (add) {
							targetModel.clearLocalIDs();
							this.addLinks.addAll(SaplUpdateEngine.copy(st.object, SaplConstants.GENERAL_CONTEXT, targetModel, null));
							targetModel.clearLocalIDs();
						}
						if (st.memberOf.contains(SaplConstants.GENERAL_CONTEXT)) {
							this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, statementID));
						}
						if (st.memberOf.contains(SaplConstants.RULES_CONTEXT)) {
							this.removeLinks.add(new LinkInfo(SaplConstants.RULES_CONTEXT, statementID));
						}
					}
				}
			}
		}
	}
	
	private boolean belongsToRuleContexts(SaplModel targetModel, SemanticStatement st){
		if (st.memberOf.contains(SaplConstants.GENERAL_CONTEXT) || st.memberOf.contains(SaplConstants.RULES_CONTEXT))
			return true;
		for(String metaruleContextID: targetModel.metaruleContexts.values())
			if(st.memberOf.contains(metaruleContextID)) return true;
		return false;
	}


	/**Specifically for sapl:achievedBy, removes goals, commitments and events that are considered
	 *   handled after the inference is done
	 * @param targetModel S-APL model to work with
	 * */
	private void removeHandledMentalAttitudes(SaplModel targetModel, SaplQueryResultSet resultSet) {

		List<Map<String, String>> list = resultSet.getSolutions(resultSet.forAll, resultSet.forSome,
				resultSet.orderBy, resultSet.limit, resultSet.offset, null, true);

		Set<String> handledGoals = new HashSet<String>();
		Set<String> handledCommitments = new HashSet<String>();
		Set<String> handledEvents = new HashSet<String>();

		for(Map<String, String> solution: list){
			String id_var = solution.get("__handledGoal__");
			if (id_var != null) {
				handledGoals.add(id_var);
			}
			id_var = resultSet.getSolution(0).get("__handledCommitment__");
			if (id_var != null) {
				handledCommitments.add(id_var);
			}
			id_var = resultSet.getSolution(0).get("__handledEvent__");
			if (id_var != null) {
				handledEvents.add(id_var);
			}				
		}

		if(targetModel.hasMentalContexts){
			Iterator<String> ait = handledGoals.iterator();
			while (ait.hasNext()) {
				String aid = ait.next();
				this.removeLinks.add(new LinkInfo(SaplConstants.GOALS_CONTEXT, aid));
				this.addLinks.add(new LinkInfo(SaplConstants.ACHIEVE_CONTEXT, aid));
			}
		}
		Iterator<String> ait = handledCommitments.iterator();
		while (ait.hasNext()) {
			String aid = ait.next();
			this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, aid));
		}
		ait = handledEvents.iterator();
		while (ait.hasNext()) {
			String aid = ait.next();
			this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, aid));
			//find and remove also corresponding "hasBlackboardObject" statement
//			String id = targetModel.beliefs.get(aid).subject;
//			ContextContainer G = targetModel.contexts.get(SaplConstants.GENERAL_CONTEXT);
//			Set<String> set1 = G.index_pred.get(SaplConstants.hasBlackboardObject);
//			if(set1!=null){
//				Set<String> set2 = G.index_sub.get(id);
//				set1.retainAll(set2);
//				if(!set1.isEmpty()) this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, set1.iterator().next()));
//			}
		}
	}

	/**Does direct update operations such as sapl:add, sapl:remove, and sapl:erase
	 * @param targetModel S-APL model to work with
	 * */
	private void doDirectUpdates(SaplModel targetModel){

		ContextContainer G = targetModel.contexts.get(SaplConstants.GENERAL_CONTEXT);

		//sapl:remove statements
		Set<String> removeStatementIDs = G.index_pred.get(SaplConstants.remove);
		if(removeStatementIDs!=null){
			for(String id : removeStatementIDs){
				SemanticStatement st = targetModel.beliefs.get(id);
				if (st.subject.equals(SaplConstants.I)) {
					if ((!st.isObjectContext() || (targetModel.contexts.get(st.object) == null))
							&& (!st.object.startsWith(SaplConstants.STATEMENT_PREFIX) || (targetModel.beliefs
									.get(st.object) == null))) {
						printErr("Error in \"remove\": Object must be context container or ID of statement: "
								+ st);
						continue;
					}
					SaplQueryResultSet resultSet = new SaplQueryResultSet();

					if (SaplQueryEngine.evaluateQuery(st.object, SaplConstants.GENERAL_CONTEXT, targetModel, SYMMETRIC_QUERYING, false, resultSet, true, null)) {
						this.removeLinks.addAll(resultSet.getForRemove());
					}

					this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, id));
				}
			}
		}

		//sapl:add statements
		Set<String> addStatementIDs = G.index_pred.get(SaplConstants.add);
		if(addStatementIDs!=null){
			for(String id : addStatementIDs){
				SemanticStatement st = targetModel.beliefs.get(id);
				if (st.subject.equals(SaplConstants.I)) {
					if ((!st.isObjectContext() || (targetModel.contexts.get(st.object) == null))
							&& (!st.object.startsWith(SaplConstants.STATEMENT_PREFIX) || (targetModel.beliefs.get(st.object) == null))) {
						printErr("Error in \"add\": Object must be context container or ID of statement: " + st);
						continue;
					}
					targetModel.clearLocalIDs();
					this.addLinks.addAll(SaplUpdateEngine.copy(st.object, SaplConstants.GENERAL_CONTEXT, targetModel, null));
					targetModel.clearLocalIDs();
					this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, id));
				}
			}
		}


		//sapl:erase statements
		Set<String> eraseStatementIDs = G.index_pred.get(SaplConstants.erase);
		if(eraseStatementIDs!=null){
			for(String id : eraseStatementIDs){
				SemanticStatement st = targetModel.beliefs.get(id);

				if (st.subject.equals(SaplConstants.I)) {
					if (st.object.startsWith(SaplConstants.STATEMENT_PREFIX)
							&& (targetModel.beliefs.get(st.object) != null)) {
						this.removeBeliefs.add(st.object);
					} else if (st.isObjectContext() && (targetModel.contexts.get(st.object) != null)) {
						//if sapl:erase {<query>} if used, not sapl:erase ?contextID 
						if(targetModel.contexts.get(st.object).referencedBy.equals(id)){
							SaplQueryResultSet resultSet = new SaplQueryResultSet();
							if (SaplQueryEngine.evaluateQuery(st.object, SaplConstants.GENERAL_CONTEXT, targetModel, SYMMETRIC_QUERYING, true, resultSet, true, null)) {
								for(LinkInfo li : resultSet.getForRemove()){
									this.removeBeliefs.add(li.statementID);
								}
							}
						}
						this.removeContexts.add(st.object);
					} else {
						//print("Error in \"erase\": Object must be ID of statement or context container: "
						//		+ st);
						//continue;
					}
					this.removeLinks.add(new LinkInfo(SaplConstants.GENERAL_CONTEXT, id));
				}

			}
		}
	}

	/**Copies contents of one context, or a single statement, into another context performing substitution of variables with their values
	 * @param sourceID ID of source context or ID of a statement
	 * @param targetContextID Destination context
	 * @param targetModel S-APL model to work with
	 * @param vars A map with values for variables to be used when copying, can be null
	 * @return A list of links to add to commit the copy operation (before that, new content is created but disconnected from the model)
	 * */
	static List<LinkInfo> copy(String sourceID, String targetContextID, SaplModel targetModel, Map<String, String> vars) {
		if (!sourceID.startsWith(SaplConstants.CONTEXT_PREFIX) && !sourceID.startsWith(SaplConstants.STATEMENT_PREFIX)) {
			printErr("Error in \"copy\": Object must be context container or ID of statement");
			return new ArrayList<LinkInfo>();
		}
		List<String> sortedVarNames = null;
		if (vars != null) {
			sortedVarNames = new ArrayList<String>(vars.keySet());
			Collections.sort(sortedVarNames);
		}

		List<LinkInfo> result = SaplUpdateEngine.copyRecursion(sourceID, targetContextID, targetModel, vars, sortedVarNames, false, false, new HashMap<String, String>());
		return result;
	}

	/**Copies a list of statements into another context performing substitution of variables with their values
	 * @param vecID List of statements to copy
	 * @param targetContextID Destination context
	 * @param targetModel S-APL model to work with
	 * @param vars A map with values for variables to be used when copying, can be null
	 * @return A list of links to add to commit the copy operation (before that, new content is created but disconnected from the model)
	 * */
	static List<LinkInfo> copy(List<String> vecID, String targetContextID, SaplModel targetModel, Map<String, String> vars) {
		List<String> sortedVarNames = null;
		if (vars != null) {
			sortedVarNames = new ArrayList<String>(vars.keySet());
			Collections.sort(sortedVarNames);
		}

		List<LinkInfo> result = new ArrayList<LinkInfo>();

		Map<String, String> bNodeMap = new HashMap<String, String>();
		for (String toBeAdded : vecID) {
			result.addAll(SaplUpdateEngine.copyRecursion(toBeAdded, targetContextID, targetModel, vars, sortedVarNames, false, false, bNodeMap));
		}
		return result;
	}

	/**Recursive step of copy operation*/
	private static List<LinkInfo> copyRecursion(String ID, String targetContextID, SaplModel targetModel, Map<String, String> vars, List<String> sortedVarNames,
			boolean insideVerbatim, boolean insideImplies, Map<String, String> bNodeMap) {

		List<LinkInfo> toAdd = new ArrayList<LinkInfo>();
		Iterator<String> it;
		if (ID.startsWith(SaplConstants.CONTEXT_PREFIX)) {
			ContextContainer c = targetModel.contexts.get(ID);
			if (c == null) {
				printErr("Error: context container does not exist: " + ID);
				return toAdd;
			}
			it = c.members.iterator();
		} else {
			List<String> tv = new ArrayList<String>();
			tv.add(ID);
			it = tv.iterator();
		}

		while (it.hasNext()) {
			String old_id = it.next();
			String id = targetModel.ids.get(old_id);

			boolean copy = true;

			if (id == null) { //have not been copied yet
				SemanticStatement st = targetModel.beliefs.get(old_id);
				SemanticStatement newSt = new SemanticStatement(st);
				//New on 09.05.2018: Added 's:is s:verbatim' to disable variable binding  
				if (vars != null && !insideVerbatim) {
					for (int i = sortedVarNames.size() - 1; i >= 0; i--) {
						String variable = sortedVarNames.get(i);
						if (!variable.isEmpty() && !variable.startsWith(SaplConstants.CONTEXT_PREFIX)) {
							String value = vars.get(variable);
							boolean unbound = value.equals(SaplConstants.UNBOUND_VARIABLE);
							if (unbound) {
								value = "";
							}
							String replace = SaplConstants.VARIABLE_PREFIX + variable;
							if (newSt.subject.contains(replace)) {
								if (unbound && newSt.subject.equals(replace)) {
									copy = false;
								} else {
									newSt.subject = newSt.subject.replace(replace, value);
								}
							}
							if (copy && newSt.predicate.contains(replace)) {
								if (unbound && newSt.predicate.equals(replace)) {
									copy = false;
								} else {
									newSt.predicate = newSt.predicate.replace(replace, value);
								}
							}
							if (copy && newSt.object.contains(replace)) {
								if (unbound && newSt.object.equals(replace) && !SaplConstants.set_predicates.contains(newSt.predicate)) {
									copy = false;
								} else {
									newSt.object = newSt.object.replace(replace, value);
								}
							}
						}
					}
				}
				
				// do not copy comparison statements after variables have been bound (was there for inter-agent communication) 
//				if (copy) {
//					if (!insideImplies && SaplConstants.embedded_predicates.contains(newSt.predicate) && !newSt.isSubjectVar()
//							&& !newSt.isObjectVar()) {
//						copy = false;
//					}
//				}

				if (copy) {

					if (!st.isSubjectVar() && newSt.isSubjectBlankNode()) {
						String newName = bNodeMap.get(newSt.subject);
						if (newName == null) {
							newName = targetModel.bnodeGenerator.getNewBlankNode();
							bNodeMap.put(newSt.subject, newName);
						}
						newSt.subject = newName;
					}
					if (!st.isObjectVar() && newSt.isObjectBlankNode()) {
						String newName = bNodeMap.get(newSt.object);
						if (newName == null) {
							newName = targetModel.bnodeGenerator.getNewBlankNode();
							bNodeMap.put(newSt.object, newName);
						}
						newSt.object = newName;
					}
				}

				if (copy) {
					if (!newSt.predicate.equals(SaplConstants.hasMember)) {
						if (st.isSubjectContext()) {
							String newCid = targetModel.contextIDGenerator.getNewContextID();
							targetModel.putContext(newCid);
							boolean isVerbatim = insideVerbatim || newSt.predicate.equals(SaplConstants.is) && newSt.object.equals(SaplConstants.verbatim); 
							boolean isImplies = insideImplies || (SaplConstants.inference_predicates.contains(newSt.predicate));
							
							toAdd.addAll(SaplUpdateEngine.copyRecursion(st.subject, newCid, targetModel, vars, sortedVarNames,
									isVerbatim, isImplies, bNodeMap));
							newSt.subject = newCid;
						} else if (newSt.isSubjectContext()) {
							ContextContainer c = targetModel.contexts.get(newSt.subject);
							if (c.referenced > 0) { //disallow making two statements about the same context
								String newCid = targetModel.contextIDGenerator.getNewContextID();
								targetModel.putContext(newCid, newSt.subject);
								newSt.subject = newCid;
							}
						}
					}

					if(newSt.predicate.equals(SaplConstants.erase) && !st.isObjectContext()){
						newSt.objectDatatype = SaplConstants.xsdString; 
					} 
					else if (!newSt.predicate.equals(SaplConstants.memberOf)) {
						if (!newSt.isObjectExplicitLiteral() && st.isObjectContext()) {
							String newCid = targetModel.contextIDGenerator.getNewContextID();
							targetModel.putContext(newCid);
							toAdd.addAll(SaplUpdateEngine.copyRecursion(st.object, newCid, targetModel, vars, sortedVarNames, insideVerbatim, insideImplies, bNodeMap));
							newSt.object = newCid;
						} else if (!newSt.isObjectExplicitLiteral() && newSt.isObjectContext()) {
							ContextContainer c = targetModel.contexts.get(newSt.object);
							if (c.referenced > 0) {//disallow making two statements about the same context
								String newCid = targetModel.contextIDGenerator.getNewContextID();
								targetModel.putContext(newCid, newSt.object);
								newSt.object = newCid;
							}
						}
					}

					id = targetModel.statementIDGenerator.getNewStatementID();

					targetModel.putBelief(id, newSt);
					targetModel.ids.put(old_id, id);
				}
			}
			if (copy) {
				toAdd.add(new LinkInfo(targetContextID, id));
			}
		}
		return toAdd;
	}


	/** Removes goals that are already achieved i.e. found in G
	 * @param targetModel S-APL model to work with
	 * */
	private void cleanAchievedGoals(SaplModel targetModel) {
		Iterator<String> it;
		it = targetModel.contexts.get(SaplConstants.GOALS_CONTEXT).members.iterator();
		while (it.hasNext()) {
			String id = it.next();
			SaplQueryResultSet resultSet = new SaplQueryResultSet();
			if (SaplQueryEngine.evaluateQuery(id, SaplConstants.GENERAL_CONTEXT, targetModel, SYMMETRIC_QUERYING, false, resultSet, false, null)) {
				this.removeLinks.add(new LinkInfo(SaplConstants.GOALS_CONTEXT, id));
			}
		}
		it = targetModel.contexts.get(SaplConstants.ACHIEVE_CONTEXT).members.iterator();
		while (it.hasNext()) {
			String id = it.next();
			SaplQueryResultSet resultSet = new SaplQueryResultSet();
			if (SaplQueryEngine.evaluateQuery(id, SaplConstants.GENERAL_CONTEXT, targetModel, SYMMETRIC_QUERYING, false, resultSet, false, null)) {
				this.removeLinks.add(new LinkInfo(SaplConstants.ACHIEVE_CONTEXT, id));
			}
		}
	}


	/**Actually performs all the scheduled updates to the model
	 * @param targetModel S-APL model to work with
	 * @return True if any changes to the model were done
	 * */
	private boolean commitUpdates(SaplModel targetModel) {
		Iterator<String> it = this.removeContexts.iterator();
		while (it.hasNext()) {
			String id = it.next();
			if (!SaplConstants.protected_entities.contains(id)) {
				targetModel.removeContext(id);
			}
		}
		it = this.removeBeliefs.iterator();
		while (it.hasNext()) {
			String statementID = it.next();
			if (!SaplConstants.protected_entities.contains(statementID)) {
				targetModel.removeBelief(statementID);
			}
		}
		Iterator<LinkInfo> lit = this.removeLinks.iterator();
		while (lit.hasNext()) {
			LinkInfo li = lit.next();
			if (!SaplConstants.protected_entities.contains(li.statementID)) {
				targetModel.removeLink(li.contextID, li.statementID);
			}
		}
		lit = this.addLinks.iterator();
		while (lit.hasNext()) {
			LinkInfo li = lit.next();
			targetModel.putLink(li.contextID, li.statementID);
		}

		boolean changed = false;
		if ((this.addLinks.size() > 0) || (this.removeLinks.size() > 0) || (this.removeBeliefs.size() > 0)
				|| (this.removeContexts.size() > 0)) {
			changed = true;
		}

		this.addLinks.clear();
		this.removeLinks.clear();
		this.removeBeliefs.clear();
		this.removeContexts.clear();

		return changed;
	}


}

