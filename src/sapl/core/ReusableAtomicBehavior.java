package sapl.core;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Parameter;
import sapl.core.rab.Resource;
import sapl.core.sapln3.ResourcePrefixer;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.core.sapln3.SaplSyntaxError;
import sapl.util.URITools;

/**
 * The base class for all external actions that can be executed by S-APL Action Engine.
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.6 (26.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009 Artem Katasonov, Michael Cochez
 * 
 * */

public abstract class ReusableAtomicBehavior implements ParametrizedAsset {

	public boolean PRINT_EXCEPTION_FULL_INFO = true; 
	
	/**S-APL action engine that executed this action*/
	protected SaplActionEngine myEngine;

	/**Setting: whether doAction() method is to be run in a separate thread*/
	@Parameter (name="thread")	protected boolean executeInThread = false;

	/**If behavior is blocked, it is inactive and skipped from execution but remains in the queue and checked on every pass*/
	protected boolean blocked = false;

	/**When time-blocked, the timestamp in ms when blocking will end*/
	protected long blockedUntil = -1;

	/**
	 * Is this RAB finished? Should it still run? Every RAB is by default a one-shot behavior, initially set to true. Deriving RAB's
	 * can change this value to false to indicate that the behavior has not finished yet.
	 */
	protected boolean finished = true;

	/**
	 * Did the RAB execution end in a success? the default is that it ends in success, once the flag is set by {@link #setFailed()}, the RAB
	 * will be failed forever. The status of the flag can be requested by {@link #hasSucceededTillNow()}
	 */
	protected boolean success = true;

	/**
	 * Indicates if this RAB still waits its first (maybe the only) execution
	 */
	protected boolean executed = false;

	/**
	 * The parameters for execution of the RAB
	 */
	protected BehaviorStartParameters startParameters = null;

	/**
	 * The ID of this RAB.
	 */
	protected String ID = null;

	/**
	 * IDs of contexts which are referenced from inside the RAB. These contexts will avoid removal 
	 * by the garbage collector of S-APL model, until explicitly released by RAB in onEnd() 
	 */
	protected Set<String> linkedContexts;


	/**Potential wrapper of this behavior, e.g. for a middleware such as JADE*/
	protected ReusableAtomicBehaviorWrapper wrapper = null;


	protected Thread myThread = null;

	/**
	 * Every RAB must have at least a default constructor. This because the agent runtime will instantiate the behavior using java
	 * reflection searching for a default constructor.
	 */
	public ReusableAtomicBehavior() {
		super();
	}


	public void setWrapper (ReusableAtomicBehaviorWrapper wrapper){
		this.wrapper = wrapper;
	}

	/**
	 * Derived RABs should implement this method and initialize all the needed parameters to run the RAB. The supplied parameters object
	 * might become invalid after this call.
	 * 
	 * @param parameters
	 *            The parameters specified by the user of this RAB.
	 * 
	 * @throws IllegalParameterConfigurationException
	 *             If one of the parameters isn't correctly given.
	 */
	protected abstract void initializeBehavior(BehaviorStartParameters parameters) throws IllegalParameterConfigurationException;

	/**
	 * Derived RAB's should implement this method and do their action within it. The derived RAB can assume that doAction is never called
	 * without a successful call to {@link ReusableAtomicBehavior#initializeRAB(BehaviorStartParameters)}
	 */
	protected abstract void doAction() throws Throwable;


	/**Sets the S-APL action engine that executed this action
	 * @param engine S-APL Action Engine*/
	final public void setSaplActionEngine(SaplActionEngine engine) {
		this.myEngine = engine;
	}


	/**
	 * This method is called by the agent when this RAB must perform its action. The action will only be called if this RAB was successful
	 * until now. This means that parameter initialization and any previous doAction call returned without throwing exceptions.
	 */
	public final void action() {
		if (this.hasSucceededTillNow()) {
			if(!executeInThread){
				this.executed = true;
				try {
					this.doAction();
				} catch (Throwable e) {
					this.setFailed();
					this.finished = true;
					this.printException(e, true);
				}
			}
			else{
				if(!this.hasBeenExecuted()){
					this.executed = true;
					
					final boolean finishedValue = this.finished;
					this.finished = false;
					
					this.block();
					myThread = new Thread(new Runnable (){
						@Override
						public void run() {
							try {
								ReusableAtomicBehavior.this.doAction();
								if(finishedValue) 
									ReusableAtomicBehavior.this.setFinished();
							} catch (Throwable e) {
								ReusableAtomicBehavior.this.setFailed();
								ReusableAtomicBehavior.this.setFinished();
								ReusableAtomicBehavior.this.printException(e, true);
							}
							myThread = null;
						}
					});
					myThread.start();
				}
			}
		}
	}

	/**
	 * Marks this RAB as being failed, this used to be done by setting the success flag to false (<CODE>this.success = false</CODE>) This
	 * new approach must ensure that once the RAB has been marked as failed, it cannot be marked as a success again. (The flag can only be
	 * set, not unset)
	 */
	protected final void setFailed() {
		this.success = false;
	}

	protected final void setFinished() {
		this.finished = true;
		this.restart();
	}

	@SuppressWarnings("deprecation")
	/**Inherently unsafe way of stopping, but is called only when agent is shutdown.
	 * Subclasses implementing server-like behaviors should override this*/
	protected void stop(){
		finished = true;
		if(myThread != null){
			myThread.stop();
		}
	}

	/**
	 * Returns true iff the RAB has succeeded in its operation up to this point. (The setFailed() method has not been called)
	 * 
	 * @return only true as long as setFailed has not been called.
	 */
	public final boolean hasSucceededTillNow() {
		return this.success;
	}

	/**
	 * Blocks this behavior. If this method is called from within action() method, behaviour suspension occurs as soon as action() returns.
	 * And as long as no new message arrives or no explicit restart is called on the behavior.
	 */
	protected final void block() {
		this.blocked = true;
	}

	/**
	 * Blocks this behavior the specified amount of time. If this method is called from within action() method, behaviour suspension occurs
	 * as soon as action() returns. The behavior will be restarted when among the three following events happens. A time of millis
	 * milliseconds has passed since the call to block(). An ACL message is received by the agent this behavior belongs to. Method
	 * restart() is called explicitly on this behavior object.
	 * 
	 * @param millis
	 *            How many milliseconds should this behavior be blocked.
	 */
	protected final void block(long millis) {
		this.blocked = true;
		this.blockedUntil = System.currentTimeMillis() + millis;
	}

	/**
	 * Restarts a blocked behavior.
	 */
	public final void restart() {
		this.blocked = false;
		this.blockedUntil = -1;
		if(this.wrapper!=null)
			this.wrapper.notifyRestart();
	}

	public final boolean isBlocked() {
		return this.blocked;
	}

	public final long getBlockedUntil() {
		return this.blockedUntil;
	}

	/**
	 * Set the start parameters for this behavior.
	 */
	@Override
	public final void setStartParameters(BehaviorStartParameters startParams) {
		this.startParameters = startParams;
	}

	/**
	 * Called when this RAB starts, calls in turn this{@link #onBehaviorStart()} and this
	 * {@link #initializeBehavior(BehaviorStartParameters)}
	 */
	public final void onStart() {
		//print(getClass().getName());		

		if(this.startParameters != null){
			List<String> addBeliefs = this.startParameters.addOnStart;
			List<String> removeBeliefs = this.startParameters.removeOnStart;

			for (String contextID : removeBeliefs) {
				this.myEngine.removeBeliefs(contextID);
			}

			for (String contextID : addBeliefs) {
				this.myEngine.addBeliefs(contextID, null);
			}
		}
		
		try {
			this.executeInThread = this.startParameters.getOptionalBooleanParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "thread"), false);
			this.initializeBehavior(this.startParameters);
			this.onBehaviorStart();
		} catch (Throwable e) {
			this.printException(e, true);
			this.setFailed();
		}

	}


	/**
	 * Method called once when this RAB is started. Hook to be overridden if desired.
	 */
	protected void onBehaviorStart() {
		//hook
	}

	/**
	 * Called when this execution of the RAB ends, calls, in turn, onBehaviorEnd()
	 */
	public final int onEnd() {
		
		try{
			this.onBehaviorEnd();
		} catch (Throwable e) {
			this.printException(e, true);
		}
		
		if(this.startParameters != null){
			List<String> addBeliefs;
			List<String> removeBeliefs;
			if (this.success) {
				addBeliefs = this.startParameters.addOnSuccess;
				removeBeliefs = this.startParameters.removeOnSuccess;
			} else {
				addBeliefs = this.startParameters.addOnFail;
				removeBeliefs = this.startParameters.removeOnFail;
			}

			addBeliefs.addAll(this.startParameters.addOnEnd);
			removeBeliefs.addAll(this.startParameters.removeOnEnd);

			for (String contextID : removeBeliefs) {
				this.myEngine.removeBeliefs(contextID);
			}

			for (String contextID : addBeliefs) {
				this.myEngine.addBeliefs(contextID, null);
			}

			if(addBeliefs.size()!=0 || removeBeliefs.size()!=0) 
				this.wakeReasoner();
		}

		if (this.ID != null) {
			this.myEngine.removeRABInstance(this);
		}

		return 0;
	}

	/**
	 * Method called once when this RAB is ended. Hook to be overridden if desired for example if resources have to be cleaned up. This
	 * method will also be called if the initialization of the RAB failed.
	 */
	protected void onBehaviorEnd() {
		//hook
	}

	/**
	 * @return Has this behavior finished its job and can it be removed from the agent scheduler.
	 */
	public final boolean done() {
		return this.finished;
	}

	
	/**
	 * Has this RAB been executed at least once?
	 * 
	 * @return only true if this RAB has been executed before.
	 */
	public final boolean hasBeenExecuted() {
		return this.executed;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * Two RABs are only equal if they are the same RAB!
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

	/**Used by print functions*/
	public static String getCurrentTime(){
		return SaplModel.getCurrentTime();
	}
	
	// Interface to subclasses, i.e. RABs
	// All are public because may need to be used also by constituent objects of RABs,
	// specifically Ontonuts 
	
	/**
	 * Prints to the screen (and to the log) specified text"
	 * 
	 * @param text
	 *            The Object from which the text must be made (using {@link Object#toString()})
	 */
	public void print(Object text) {
		System.out.println("["+getCurrentTime()+"] "+text);
		//logger.log(Level.INFO, String.valueOf(text));
	}
	
	public void printToErr(Object text) {
		System.err.println("["+getCurrentTime()+"] "+text);
		//logger.log(Level.SEVERE, String.valueOf(text));
	}

	public void printError(String message) {
		String m = "Error at "+this.getClass().getName() + " : " + message;
		System.err.println("["+getCurrentTime()+"] "+m);
		//logger.log(Level.SEVERE, m);
	}

	public void printException(Throwable e, boolean fullTrace) {
		this.printException(e, null, fullTrace);
	}
	
	public void printException(Throwable e, String message, boolean fullTrace) {
		if(message==null)
			System.err.println("["+getCurrentTime()+"] Exception in "+getClass().getName());
		else 
			System.err.println("["+getCurrentTime()+"] "+message);
		if(e instanceof SaplSyntaxError){
			SaplSyntaxError err = (SaplSyntaxError)e;
			System.err.println("Syntax error in "+err.getTextID()+", line "+err.getToken().getLine()+", column "+err.getToken().getLineCharStart()+":");
			System.err.println("\t"+err.getMessage());
			if(PRINT_EXCEPTION_FULL_INFO){
				System.err.println("\tIn: "+err.getText());
				if(fullTrace) err.printStackTrace();
			}
		}
		else{
			if(fullTrace){
				e.printStackTrace();
			}
			else{
				StackTraceElement[] trace = e.getStackTrace();
				String str = e.toString();
				if(trace.length>0) str += "\n\tat "+trace[0].toString();
				if(trace.length>1) str += "\n\tat "+trace[1].toString();
				Throwable cause = e.getCause();
				while(cause!=null){
					str+="\ncaused by: "+cause.toString();
					cause = cause.getCause();
				}
				System.err.println(str);
			}
		}
	}


	/**Sends a signal to SaplReasoner to wake up*/
	public final void wakeReasoner() {
		this.myEngine.wakeReasoner();
	}

	/**Adds beliefs (copies a sub-graph given with ID can be of a context or a statement) with binding variables (if given)
	 * 
	 * @param contextOrStatementID
	 *            the statement or context which should be copied
	 * @param vars
	 *            The variable association which should be used in the copy action.
	 */
	public final void addBeliefs(String contextOrStatementID, Map<String, String> vars) {
		this.myEngine.addBeliefs(contextOrStatementID, vars);
	}

	/**Adds beliefs in a way a rule does: using the solution set modifiers such as sapl:All defined for a result set
	 * 
	 * @param contextOrStatementID the statement or context which should be copied
	 * @param resultSet SaplQueryResultSet giving variable mappings as well as solution set 
	 */
	public final void addBeliefsAsRuleOutput(String contextOrStatementID, SaplQueryResultSet resultSet) {
		this.myEngine.addBeliefsAsRuleOutput(contextOrStatementID, resultSet);
	}

	/**Adds beliefs in a way a rule does: using the solution set modifiers such as sapl:All defined for a result set
	 * 
	 * @param statementIDs List of statements which should be copied
	 * @param resultSet SaplQueryResultSet giving variable mappings as well as solution set 
	 */
	public final void addBeliefsAsRuleOutput(List<String> statementIDs, SaplQueryResultSet resultSet) {
		this.myEngine.addBeliefsAsRuleOutput(statementIDs, resultSet);
	}


	/**
	 * Adds beliefs to G defined by a N3 string
	 * 
	 * @param n3string
	 *            the S-APL string which must be added to the agent.
	 * @param doChecks
	 *            should checks be performed for joining containers
	 * @return true is beliefs are added. False if either adding beliefs failed or no beliefs where added.
	 */
	public final boolean addBeliefsN3(String n3string, boolean doChecks) {
		try{
			return this.myEngine.addBeliefsN3(n3string, SaplConstants.GENERAL_CONTEXT, doChecks);
		}
		catch(Throwable e){
			printException(e, true);
			return false;
		}
	}

	/**
	 * Same as {@link #addBeliefsN3(String, boolean)} with doChecks set to true
	 * 
	 * @param n3string
	 *            The string of S-APL which should get added to the agent's beliefs structure.
	 * @return true if beliefs are added. False if either adding beliefs failed or no beliefs where added.
	 */
	public final boolean addBeliefsN3(String n3string) {
		return this.myEngine.addBeliefsN3(n3string, SaplConstants.GENERAL_CONTEXT, true);
	}

	public final boolean addBeliefsN3(String n3string, String context, boolean doChecks) {
		return this.myEngine.addBeliefsN3(n3string, context, doChecks);
	}

	public final boolean addBeliefsN3(String n3string, List<String> contexts, boolean doChecks) {
		return this.myEngine.addBeliefsN3(n3string, contexts, doChecks);
	}

	public final String addBeliefsN3ToTempContext(String n3string) {
		return this.myEngine.addBeliefsN3ToTempContext(n3string);
	}
	

	/**
	 * Removes beliefs by pattern given (ID can be of a context or a statement)
	 * 
	 * @param beliefID
	 *            the ID of the context or statement specifying the beliefs to be removed.
	 */
	public final void removeBeliefs(String beliefID) {
		this.myEngine.removeBeliefs(beliefID);
	}
	
	public final void removeBeliefs(String ID, Map<String, String> vars){
		this.myEngine.removeBeliefs(ID, vars);
	}


	/**Removes beliefs (from G) based on pattern defined in an N3 string*/
	public final void removeBeliefsN3(String n3string){
		this.myEngine.removeBeliefsN3(n3string);
	}
	
	/**
	 * Erases beliefs by pattern given (ID can be of a context or a statement).
	 * Erasing, in contrast to removing (1) treats blank nodes as constants, (2) eliminates statements from all contexts they are linked to 
	 * @param ID the ID of the context or statement specifying the beliefs to be erased
	 */
	public final void eraseBeliefs(String ID){
		this.myEngine.eraseBeliefs(ID);
	}

	public final void eraseBeliefs(String ID, Map<String, String> vars){
		this.myEngine.eraseBeliefs(ID, vars);
	}

	/**Erases beliefs (from G) based on pattern defined in an N3 string*/
	public final void eraseBeliefsN3(String n3string){
		this.myEngine.eraseBeliefsN3(n3string);
	}

	/**Erases specific beliefs identified by ID of a context or a statement, no pattern matching is performed*/
	public final void eraseBeliefsByID(String ID){
		this.myEngine.eraseBeliefsByID(ID);
	}
		
	/**
	 * Returns possible bindings of variables to beliefs given (with ID can be of a context or a statement)
	 * @param contextOrStatementID ID of the query context in the S-APL model (or ID of a single statement)
	 * @param targetContextID Context against which the query is run
	 * @return The set with bindings for the query.
	 */
	public final SaplQueryResultSet hasBeliefs(String contextOrStatementID, String targetContextID, boolean symmetricQuerying) {
		return this.myEngine.hasBeliefs(contextOrStatementID, targetContextID, symmetricQuerying);
	}
	public final SaplQueryResultSet hasBeliefs(String contextOrStatementID, String targetContextID) {
		return this.myEngine.hasBeliefs(contextOrStatementID, targetContextID, SaplUpdateEngine.SYMMETRIC_QUERYING);
	}

	/**Evaluates a query, constrained by a given starting solution set*/
	public final boolean hasBeliefs(String contextOrStatementID, String targetContextID, boolean symmetricQuerying, SaplQueryResultSet resultSet){
		return this.myEngine.hasBeliefs(contextOrStatementID, targetContextID, symmetricQuerying, resultSet);
	}
	public final boolean hasBeliefs(String contextOrStatementID, String targetContextID, SaplQueryResultSet resultSet){
		return this.myEngine.hasBeliefs(contextOrStatementID, targetContextID, SaplUpdateEngine.SYMMETRIC_QUERYING, resultSet);
	}

	/**Evaluates a query given as a set of statement IDs, constrained by a given starting solution set*/
	public final boolean hasBeliefs(List<String> statementIDs, String targetContextID, boolean symmetricQuerying, SaplQueryResultSet resultSet) {
		return this.myEngine.hasBeliefs(statementIDs, targetContextID, symmetricQuerying, resultSet);
	}	
	public final boolean hasBeliefs(List<String> statementIDs, String targetContextID, SaplQueryResultSet resultSet) {
		return this.myEngine.hasBeliefs(statementIDs, targetContextID, SaplUpdateEngine.SYMMETRIC_QUERYING, resultSet);
	}	

	/**
	 * Returns all possible bindings of variables (from the specified queryContext) to the query specified in the S-APL string Returns
	 * possible solutions (combinations of bindings of variables) to match beliefs specified in an S-APL (Notation3) string. Returns null if
	 * no match is found.
	 * 
	 * @param n3string
	 *            The String containing the query
	 * @param targetContextID
	 *            The context in which matches to the query must be searched.
	 * @return A {@link BindingsSet} containing all possible bindings to the variables specified.
	 */
	public final SaplQueryResultSet hasBeliefsN3(String n3string, String targetContextID, boolean symmetricQuerying) {
		return this.myEngine.hasBeliefsN3(n3string, targetContextID, symmetricQuerying);
	}
	public final SaplQueryResultSet hasBeliefsN3(String n3string, String targetContextID) {
		return this.myEngine.hasBeliefsN3(n3string, targetContextID, SaplUpdateEngine.SYMMETRIC_QUERYING);
	}

	/**
	 * Returns all possible bindings of variables (from the global context G) to the query specified in the S-APL document.
	 * Returns possible solutions (combinations of bindings of variables) to match beliefs specified in an S-APL (Notation3) string. 
	 * Returns null if no match is found.
	 * @param n3string The String containing the query
	 * @return A {@link BindingsSet} containing all possible bindings to the variables specified.
	 */
	public final SaplQueryResultSet hasBeliefsN3(String n3string, boolean symmetricQuerying) {
		return this.myEngine.hasBeliefsN3(n3string, symmetricQuerying);
	}
	public final SaplQueryResultSet hasBeliefsN3(String n3string) {
		return this.myEngine.hasBeliefsN3(n3string, SaplUpdateEngine.SYMMETRIC_QUERYING);
	}

	/**
	 * Same as {@link #produceN3(String, ProduceN3Options)} with the default options.
	 * 
	 * @param contextID
	 *            The context for which S-APL N3 must be generated.
	 * @return The produced S-APL code
	 */
	public final String produceN3(String contextOrStatementID) {
		return this.myEngine.produceN3(contextOrStatementID);
	}

	/**
	 * Returns an S-APL (Notation3) string corresponding to a sub-graph starting with context container given. The class ProduceN3Options
	 * has following boolean fields: indents (default true), cleanContent (default false), and useNameSpaces (default true). If indents =
	 * true, the line breaks and indentation are used. If cleanContent = true, the content is cleaned in the sense that potential query
	 * constructs such as Optional, or, filtering are not included. If useNameSpaces = true, the known namespaces are substituted with their
	 * prefixes.
	 * 
	 * @param contextID
	 *            The context ID for which N3 must be produced.
	 * @param options
	 *            The options for production.
	 * @return The produced N3
	 */
	public final String produceN3(String contextOrStatementID, SaplN3ProducerOptions options) {
		return this.myEngine.produceN3(contextOrStatementID, options);
	}
	
	/**
	 * Returns an S-APL (Notation3) string corresponding to an arbitrary
	 * sub-set of existing in the model statements,
	 * which are treated as if they belong to the same context container.
	 * 
	 * @param statements
	 *            The list of statement for which S-APL N3 must be generated.
	 * @return The produced S-APL code
	 */
	public final String produceN3(Collection<String> statementIDs) {
		return this.myEngine.produceN3(statementIDs);
	}

	/**
	 * Returns an S-APL (Notation3) string corresponding to an arbitrary
	 * sub-set of existing in the model statements,
	 * which are treated as if they belong to the same context container.
	 */
	public final String produceN3(Collection<String> statementIDs, SaplN3ProducerOptions options) {
		return this.myEngine.produceN3(statementIDs, options);
	}
	
	/** 
	 * Returns an S-APL (Notation3) string corresponding to a sub-graph starting 
	 * from a root subject in a context
	 * */
	public final String produceN3(String contextID, String root) {
		return this.myEngine.produceN3(contextID, root);
	}

	/** 
	 * Returns an S-APL (Notation3) string corresponding to a sub-graph starting 
	 * from a root subject in a context
	 * */
	public final String produceN3(String contextID, String root, SaplN3ProducerOptions options) {
		return this.myEngine.produceN3(contextID, root, options);
	}
	

	/**
	 * Returns the vector of copies of statements in a context container (+ a fills a vector of their original IDs)
	 * @param contextID
	 *            the context which should be copied.
	 * @param ids
	 *            This is an in-out parameter! To this list, the IDs of the original statements will be added.
	 * @return The copies.
	 */
	public final List<SemanticStatement> getStatements(String contextID, List<String> ids) {
		return this.myEngine.getStatements(contextID, ids);
	}

	/**
	 * Adds a set of existing statements to another context
	 */
	public final void putStatements(List<String> ids,  String contextID)
	{
		this.myEngine.putStatements(ids, contextID);
	}

	/**
	 * Returns the vector of copies of statements in a context container. SemanticStaments objects have three String data members: subject,
	 * predicate and object.
	 * 
	 * @param contextID
	 *            The context ID which should eb copied.
	 * @return The list of semantic statements. Keep in mind that this list is in unspecified order.
	 */
	public final List<SemanticStatement> getStatements(String contextID) {
		return this.myEngine.getStatements(contextID);
	}


	/** Searches for actual query inside all layers of wrappings like {} sapl:All ?x , {} sapl:Some ?x
	 * @return ID of the final inner context of the query after all wrappings skipped*/ 
	public String unwrapQuery(String queryID, SaplQueryResultSet resultSet, boolean recordWrappings) {
		return this.myEngine.unwrapQuery(queryID, resultSet, recordWrappings);
	}


	/**
	 * Add an object to blackboard, return the ID of the created object
	 * 
	 * @param o
	 *            the Object which should be put on the {@link Blackboard} of the {@link UbiwareAgent}
	 * @return The ID of the created object which can be used for later retrieval.
	 */
	public final Resource addOnBlackboard(Object o) {
		return this.myEngine.addOnBlackboard(o);
	}

	/**
	 * Removes an object from blackboard.
	 * 
	 * @param id
	 *            The ID of the {@link Blackboard} object
	 */
	public final void removeFromBlackboard(Resource id) {
		this.myEngine.removeFromBlackboard(id);
	}

	/**
	 * returns an object from blackboard of the {@link UbiwareAgent}
	 * 
	 * @param id
	 *            the id of the {@link Blackboard} object
	 * @return The Object associated with this ID.
	 */
	public final Object getFromBlackboard(Resource id) {
		return this.myEngine.getFromBlackboard(id);
	}


	public final ResourcePrefixer getPrefixer(){
		return this.myEngine.getPrefixer();
	}
	
	
	public boolean executingWithResult() {
		return this.myEngine.executingWithResult();
	}
	
	public void writeToResult(String text) {
		this.myEngine.writeToResult(text);
	}
	
	public void findAllVarsInStatement(SemanticStatement st, Set<String> matchedOrUsed, Set<String> defined){
		this.myEngine.findAllVarsInStatement(st, matchedOrUsed, defined);
	}
	

}
