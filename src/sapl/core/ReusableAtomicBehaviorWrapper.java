package sapl.core;

/**
 * Interface to be implemented by wrappers of ReusableAtomicBehavior for different agent implementations
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.0 (02.05.2013)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013, VTT
 * 
 * */

public interface ReusableAtomicBehaviorWrapper {
	/**Used by RAB to notify about the need to restart the wrapper*/
	public void notifyRestart();
}
