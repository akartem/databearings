package sapl.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Representation of an RDF triple (subject, predicate, object).
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä)
 * 
 * @version 4.5 (20.01.2017)
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */ 

public class SemanticStatement{

	/**Subject of the triple*/
	public String subject = null;

	/**Predicate of the triple*/
	public String predicate = null;

	/**Object of the triple*/
	public String object = null;

	/** For explicitly typed literal object, the data-type of the object */
	public String objectDatatype = null;

	/** For literal objects with the language defined, the language of the object */
	public String objectLanguage = null;

	/**
	 * List of IDs of referencing ContextContainers
	 */
	public List<String> memberOf;

	/**
	 * The number of referencing ContextContainers.
	 */
	public int referenced = 0;

	/**
	 * Short constructor
	 *  
	 * @param s Subject of the statement.
	 * @param p Predicate of the statement.
	 * @param o Object of the statement.
	 */
	public SemanticStatement(String s, String p, String o) {
		this(s, p, o, null, null);
	}

	/**
	 * Long constructor
	 * 
	 * @param s Subject of the statement.
	 * @param p Predicate of the statement.
	 * @param o Object of the statement.
	 * @param oD Datatype of the object.
	 * @param oL Language of the object
	 */
	public SemanticStatement(String s, String p, String o, String oD, String oL) {
		this.subject = s;
		this.predicate = p;
		this.object = o;
		this.objectDatatype = oD;
		this.objectLanguage = oL;

		this.fix();

		this.memberOf = new ArrayList<String>(1);
	}

	/**
	 * Copy constructor: Creates an identical semantic statement (excluding references from ContextContainers)
	 * 
	 * @param toCopy the semantic statement to copy from.
	 */
	public SemanticStatement(SemanticStatement toCopy) {
		this.subject = toCopy.subject;
		this.predicate = toCopy.predicate;
		this.object = toCopy.object;
		this.objectDatatype = toCopy.objectDatatype;
		this.objectLanguage = toCopy.objectLanguage;
		this.memberOf = new ArrayList<String>(1);
	}

	/**
	 * Fixes this statement : sets '?' instead of null in subject, predicate and object
	 */
	private void fix() {
		if (this.subject == null) {
			this.subject = "?";
		}
		if (this.predicate == null) {
			this.predicate = "?";
		}
		if (this.object == null) {
			this.object = "?";
		}
	}	

	/**
	 * Is the subject SaplConstants.ANYTHING or a variable?
	 * 
	 * @return true if the subject is SaplConstants.ANYTHING or a variable, false otherwise
	 */
	public boolean isSubjectAny() {
		return this.subject.equals(SaplConstants.ANYTHING) || this.isSubjectVar();
	}

	/**
	 * Is the predicate SaplConstants.ANYTHING or a variable?
	 * 
	 * @return true if the predicate is SaplConstants.ANYTHING or a variable, false otherwise
	 */
	public boolean isPredicateAny() {
		return this.predicate.equals(SaplConstants.ANYTHING) || this.isPredicateVar();
	}

	/**
	 * Is the object SaplConstants.ANYTHING or a variable?
	 * 
	 * @return true if the object is SaplConstants.ANYTHING or a variable, false otherwise
	 */
	public boolean isObjectAny() {
		return this.object.equals(SaplConstants.ANYTHING) || this.isObjectVar();
	}

	/**
	 * Is the subject a variable?
	 * 
	 * @return true if the subject is a variable, false otherwise
	 */
	public boolean isSubjectVar() {
		return (this.subject.length() > SaplConstants.VARIABLE_PREFIX.length()) && this.subject.startsWith(SaplConstants.VARIABLE_PREFIX);
	}

	/**
	 * Is the predicate a variable?
	 * 
	 * @return true if the predicate is a variable, false otherwise
	 */
	public boolean isPredicateVar() {
		return (this.predicate.length() > SaplConstants.VARIABLE_PREFIX.length()) && this.predicate.startsWith(SaplConstants.VARIABLE_PREFIX);
	}

	/**
	 * Is the object a variable?
	 * 
	 * @return true if the object is a variable, false otherwise
	 */
	public boolean isObjectVar() {
		return (this.object.length() > SaplConstants.VARIABLE_PREFIX.length()) && this.object.startsWith(SaplConstants.VARIABLE_PREFIX);
	}

	/**
	 * Does this statement have variables?
	 * 
	 * @return true if this statement has a variable, false otherwise
	 */
	public boolean hasVariables() {
		return this.isSubjectVar() || this.isPredicateVar() || this.isObjectVar();
	}

	/**
	 * Returns the name of the variable of the subject, if it is a variable.
	 * 
	 * @return the name of the variable if the subject is a variable, null otherwise.
	 */
	public String getSubjectVariableName() {
		return this.isSubjectVar() ? this.subject.substring(SaplConstants.VARIABLE_PREFIX.length()) : null;
	}

	/**
	 * Returns the name of the variable of the predicate, if it is a variable.
	 * 
	 * @return the name of the variable if the predicate is a variable, null otherwise.
	 */
	public String getPredicateVariableName() {
		return this.isPredicateVar() ? this.predicate.substring(SaplConstants.VARIABLE_PREFIX.length()) : null;
	}

	/**
	 * Returns the name of the variable of the object if it is a variable.
	 * 
	 * @return the name of the variable if the object is a variable, null otherwise.
	 */
	public String getObjectVariableName() {
		return this.isObjectVar() ? this.object.substring(SaplConstants.VARIABLE_PREFIX.length()) : null;
	}

	/**
	 * Is subject is the ID of a ContextContainer?
	 * 
	 * @return true if the object is ID a ContextContainer
	 */
	public boolean isSubjectContext() {
		return this.subject.startsWith(SaplConstants.CONTEXT_PREFIX);
	}

	/**
	 * Is object is the ID of a ContextContainer?
	 * 
	 * @return true if the object is ID of a ContextContainer
	 */
	public boolean isObjectContext() {
		return this.object.startsWith(SaplConstants.CONTEXT_PREFIX);
	}	

	/**
	 * Is subject an RDF blank node?
	 * 
	 * @return true if the subject is a blank node
	 */
	public boolean isSubjectBlankNode() {
		return this.subject.startsWith(SaplConstants.BLANK_NODE_PREFIX);
	}	

	/**
	 * Is object an RDF blank node?
	 * 
	 * @return true if the object is a blank node
	 */
	public boolean isObjectBlankNode() {
		return this.object.startsWith(SaplConstants.BLANK_NODE_PREFIX);
	}	
	

	/**
	 * Does string appear to be an URI? 
	 */
	public static boolean isURI(String str){
		return str.startsWith("http://") || str.startsWith("https://") || str.startsWith("file://") || str.startsWith("#");
	}

	/**
	 * Does subject appear to be an URI? 
	 */
	public boolean isSubjectURI() {
		return isURI(this.subject);
	}
	
	/**
	 * Does predicate appear to be an URI? 
	 */
	public boolean isPredicateURI() {
		return isURI(this.predicate);
	}	
	
	/**
	 * Does object appear to be an URI? 
	 */
	public boolean isObjectURI() {
		return !isObjectExplicitLiteral() && isURI(this.object);
	}	
	
	/**
	 * Is object explictly literal (typed or with a language attached) 
	 */
	public boolean isObjectExplicitLiteral() {
		return this.objectDatatype!=null || this.objectLanguage!=null; 
	}

	/**
	 * Returns true if this statement is an exact match to the other statement.
	 */
	
	@Override
	public boolean equals(Object o) {
		if (! (o instanceof SemanticStatement)) return false;
		SemanticStatement other = (SemanticStatement) o;
		return this.subject.equals(other.subject)
				&& this.predicate.equals(other.predicate)
				&& this.object.equals(other.object);
	}
	
	/**
	 * Returns true if this statement is a match (taking universality into account) to the other statement.
	 */
	public boolean equalsUniversal(SemanticStatement other) {
		if (other == null) {
			return false;
		}
		if (!this.isSubjectAny() && !other.isSubjectAny()) {
			if (!this.subject.equals(other.subject)) {
				return false;
			}
		}
		if (!this.isPredicateAny() && !other.isPredicateAny()) {
			if (!this.predicate.equals(other.predicate)) {
				return false;
			}
		}
		if (!this.isObjectAny() && !other.isObjectAny()) {
			if (!this.object.equals(other.object)) {
				return false;
			}
		}

		return true;
	}

	/** Replaces variables with their values from the binding map
	 * 
	 * @param vars A map with bindings of variables
	 * @return array of 3 boolean values indicating if subject, predicate, and object have been changed within this operation 
	 */
	public boolean[] bindVars(Map<String, String> vars) {
		boolean[] changed = new boolean[3];
		changed[0] = false;
		changed[1] = false;
		changed[2] = false;
		if (vars.size() > 0) {
			String var = this.getSubjectVariableName();
			if (var != null) {
				String value = vars.get(var);
				if ((value != null) && !value.equals("")) {
					this.subject = value;
					changed[0] = true;
				}
			}
			var = this.getPredicateVariableName();
			if (var != null) {
				String value = vars.get(var);
				if ((value != null) && !value.equals("")) {
					this.predicate = value;
					changed[1] = true;
				}
			}
			var = this.getObjectVariableName();
			if (var != null) {
				String value = vars.get(var);
				if ((value != null) && !value.equals("")) {
					this.object = value;
					changed[2] = true;
				}
			}
		}
		return changed;
	}

	/** Returns hash for the statement */ 
	@Override
	public int hashCode() {
		String str = this.subject + " " + this.predicate + " " + this.object;
		return str.hashCode();
	}

	/** Returns simple string representation for the statement */ 
	@Override
	public String toString() {
		return this.subject + " " + this.predicate + " " + this.object ; //+ " (" + String.valueOf(this.referenced) + ")";
	}

}
