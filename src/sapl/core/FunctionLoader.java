package sapl.core;

/**
 * Class loader to dynamically load {@link Function} classes.
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.2 (14.01.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class FunctionLoader extends ClassLoader {

	public FunctionLoader(ClassLoader parent) {
		super(parent);
	}

	//This cast is fine since it is checked trough reflection that the class is of that type.
	@SuppressWarnings("unchecked")
	@Override
	protected Class<? extends Function> findClass(String actionName) throws ClassNotFoundException {
		
		Class<?> theClass = Class.forName(actionName);
		
		Class<?> superClass = theClass.getSuperclass();
		
		while (!Function.class.equals(superClass)) {
			if (superClass == null) {
				//the superclass is object or interface or ... but not an RAB
				throw new ClassNotFoundException("The classes loaded by FunctionLoader must be derived from sapl.core.Function");
			}
			superClass = superClass.getSuperclass();
		}
		return (Class<? extends Function>) theClass;
	}

	//This cast is fine since only Functions can be loaded with this
	@SuppressWarnings("unchecked")
	@Override
	public Class<Function> loadClass(String name) throws ClassNotFoundException {
		return (Class<Function>) super.loadClass(name);
	}
}
