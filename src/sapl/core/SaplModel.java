package sapl.core;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import sapl.core.sapln3.ResourcePrefixer;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.SaplSyntaxError;
import sapl.util.BlankNodeGenerator;
import sapl.util.ContextIDGenerator;
import sapl.util.HttpTools;
import sapl.util.SetOperation;
import sapl.util.StatementIDGenerator;

/** 
 * S-APL data model that contains interlinked SemanticStatements and ContextContainers
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 *
 * @version 4.6 (26.06.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009 Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplModel {
	
	protected static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");

	/**Map of SemanticStatements and their IDs*/
	public final Map<String, SemanticStatement> beliefs = new HashMap<String, SemanticStatement>();

	/**Map of ContextContainers and their IDs*/
	public final Map<String, ContextContainer> contexts = new HashMap<String, ContextContainer>();

	/**Whether rule and metarule contexts were created*/
	public boolean hasRuleContexts;
	/**Whether special agent-oriented contexts were created*/
	public boolean hasMentalContexts;

	/**List of identifiers needed when doing such operations like
	 * SaplN3Parser.load() and SaplUpdateEngine.copy()*/
	Map<String, String> ids;

	/** The resource prefixer to use with the model*/
	public final ResourcePrefixer prefixer = new ResourcePrefixer();

	/**Generator for statement IDs*/
	public final ContextIDGenerator contextIDGenerator = new ContextIDGenerator();

	/**Generator for context IDs*/
	public final StatementIDGenerator statementIDGenerator = new StatementIDGenerator();

	/**Generator for blank node IDs*/
	public final BlankNodeGenerator bnodeGenerator = new BlankNodeGenerator();

	/**Synchronization lock*/
	public Boolean lock = new Boolean(true);

	/**Contexts created as {} s:MetaRuleLevel N*/
	public Map<Integer, String> metaruleContexts = new HashMap<Integer, String>();
	public List<Integer> metaruleLevels = new ArrayList<Integer>();


	/**Constructor
	 * @param addRuleContexts If true, rule and meta-rule contexts are added
	 * @param addMentalContexts If true, special agent-oriented contexts are added
	 * */
	public SaplModel (boolean addRuleContexts, boolean addMentalContexts){
		ContextContainer G = new GeneralContext();
		this.contexts.put(SaplConstants.GENERAL_CONTEXT, G);

		this.hasRuleContexts = addRuleContexts; 
		this.hasMentalContexts = addMentalContexts; 

		if(addRuleContexts){
			this.putContext(SaplConstants.RULES_CONTEXT); //container for rules
			this.putBelief(SaplConstants.RULES_BELIEF, new SemanticStatement(SaplConstants.RULES_CONTEXT, SaplConstants.is,
					SaplConstants.Rule));
			this.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.RULES_BELIEF);
		}

		if(addMentalContexts){
			this.putContext(SaplConstants.FALSE_CONTEXT); //container for statements known to be false
			this.putBelief(SaplConstants.FALSE_BELIEF, new SemanticStatement(SaplConstants.FALSE_CONTEXT, SaplConstants.is,
					SaplConstants.FALSE));
			this.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.FALSE_BELIEF);

			this.putContext(SaplConstants.GOALS_CONTEXT); //container for goals
			this.putBelief(SaplConstants.GOALS_BELIEF,
					new SemanticStatement(SaplConstants.I, SaplConstants.want, SaplConstants.GOALS_CONTEXT));
			this.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.GOALS_BELIEF);

			this.putContext(SaplConstants.ACHIEVE_CONTEXT); //container for goals that are attempted to be achieved
			this.putBelief(SaplConstants.ACHIEVE_BELIEF, new SemanticStatement(SaplConstants.I, SaplConstants.achieving,
					SaplConstants.ACHIEVE_CONTEXT));
			this.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.ACHIEVE_BELIEF);

			this.putContext(SaplConstants.DO_CONTEXT); //container for running RABs: actually commitments that led to execution
			this.putBelief(SaplConstants.DO_BELIEF, new SemanticStatement(SaplConstants.I, SaplConstants.doing, SaplConstants.DO_CONTEXT));
			this.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.DO_BELIEF);
		}
	}

	/**Clears the list of local (within-document) identifiers*/
	public void clearLocalIDs() {
		if (this.ids == null) {
			this.ids = new HashMap<String, String>();
		} else {
			this.ids.clear();
		}
	}

	/**Prints an error message*/
	private static void print(String string) {
		System.err.println("["+getCurrentTime()+"] "+string);
	}


	/**Creates a new context container and adds it to contexts
	 * @param id ID for the new context
	 */
	public synchronized void putContext(String id) {
		this.putContext(id, null);
	}

	/**Creates a new context container and adds it to contexts
	 * @param id ID for the new context
	 * @param membersSize Initial capacity for the members list 
	 */
	public synchronized void putContext(String id, int membersSize) {
		this.putContext(id, null, membersSize);
	}

	/**Creates a new context container and adds it to contexts
	 * @param id ID for the new context
	 * @param toCopy ID of another context from which the links to the statements have to be copied, or null  
	 * */
	public synchronized void putContext(String id, String toCopy) {
		ContextContainer context = new ContextContainer();
		this.contexts.put(id, context);
		if (toCopy != null) {
			this.copyLinks(toCopy, id);
		}
	}

	/**Creates a new context container and adds it to contexts
	 * @param id ID for the new context
	 * @param toCopy ID of another context from which the links to the statements have to be copied, or null
	 * @param membersSize Initial capacity for the members list 
	 * */
	public synchronized void putContext(String id, String toCopy, int membersSize) {
		ContextContainer context = new ContextContainer(membersSize);
		this.contexts.put(id, context);
		if (toCopy != null) {
			this.copyLinks(toCopy, id);
		}
	}

	/**Removes a context from contexts and also all the beliefs about this context
	 * @param id ID of the context 
	 * */
	public synchronized void removeContext(String id) {
		ContextContainer context = this.contexts.get(id);
		if (context != null) {
			if (context.referencedBy != null) {
				this.removeBelief(context.referencedBy);
			}

			Iterator<String> it = context.members.iterator();
			while (it.hasNext()) {
				SemanticStatement st = this.beliefs.get(it.next());
				if (st != null) {
					st.referenced--;
					st.memberOf.remove(id);
				}
			}
			this.contexts.remove(id);
		}
	}


	/**Add a semantic statement to beliefs
	 * @param id ID for the new statement
	 * @param statement Statement to add
	 * */
	public synchronized void putBelief(String id, SemanticStatement statement) {
		if (statement.isSubjectContext() && !(statement.predicate.equals(SaplConstants.hasMember))) {
			ContextContainer c = this.contexts.get(statement.subject);
			if (c != null) {
				if (c.referencedBy != null) {
					print("Warning: Making second statement about a context: " + statement);
				}
				c.referenced++;
				c.referencedBy = id;
			} else {
				print("Error: context container does not exist: " + statement.toString());
			}
		}
		if (!statement.isObjectExplicitLiteral() && statement.isObjectContext() && !statement.predicate.equals(SaplConstants.memberOf)) {
			ContextContainer c = this.contexts.get(statement.object);
			if (c != null) {
				if (c.referencedBy != null) {
					//should not ever happen
					print("Warning: Making second statement about a context: " + statement);
				}
				c.referenced++;
				c.referencedBy = id;
			} else {
				print("Error: context container does not exist: " + statement.toString());
			}
		}

		this.beliefs.put(id, statement);

		if (statement.predicate.equals(SaplConstants.ID) && statement.isSubjectContext() && !statement.isObjectVar()
				&& !statement.isObjectContext()) {
			//remember the local (within-document) ID that was defined here for a statement 
			List<String> temp = this.contexts.get(statement.subject).members;
			if (temp.size() > 0) {
				this.ids.put(statement.object, temp.get(0));
			}
		}
	}

	/**Removes a statement from beliefs
	 * @param id ID of statement to remove
	 * */
	synchronized void removeBelief(String id) {
		SemanticStatement statement = this.beliefs.get(id);
		if (statement != null) {
			if (statement.isSubjectContext() && !statement.predicate.equals(SaplConstants.hasMember)) {
				ContextContainer c = this.contexts.get(statement.subject);
				if (c != null) {
					c.referenced--;
					c.referencedBy = null;
				}
			}
			if (!statement.isObjectExplicitLiteral() && statement.isObjectContext() && !statement.predicate.equals(SaplConstants.memberOf)) {
				ContextContainer c = this.contexts.get(statement.object);
				if (c != null) {
					c.referenced--;
					c.referencedBy = null;
				}
			}

			Iterator<String> it = statement.memberOf.iterator();
			while (it.hasNext()) {
				String contextID = it.next();
				ContextContainer c = this.contexts.get(contextID);
				if (c != null) {
					if (c.members.remove(id)) {
						c.removeFromIndices(statement, id);
					}
					if(contextID.equals(SaplConstants.GENERAL_CONTEXT) && statement.predicate.equals(SaplConstants.MetaRuleLevel)){
						Integer level = Integer.valueOf(statement.object);
						metaruleContexts.remove(level);
						metaruleLevels.remove(level);
					}			
				}
			}

			this.beliefs.remove(id);
		}
	}


	/**Links a context to a belief. Joins contexts if needed.
	 * In many situations, does not create the specified link as such but creates something else
	 * leaving the statement hanging -> garbage collector will clean
	 * @param contextID ID of the context
	 * @param statementID ID of the statement
	 * */
	public synchronized void putLink(String contextID, String statementID) {
		this.putLink(contextID, statementID, true);
	}

	/**Links a context to a statement. Joins contexts if needed.
	 * In many situations, does not create the specified link as such but creates something else
	 * leaving the statement hanging -> garbage collector will clean
	 * @param contextID ID of the context
	 * @param statementID ID of the statement
	 * @param doJoinCheck Whether to do contexts join according to S-APL rules
	 * */
	public synchronized void putLink(String contextID, String statementID, boolean doJoinCheck) {
		SemanticStatement ss = this.beliefs.get(statementID);
		if (ss == null) {
			return;
		}
		ContextContainer container = this.contexts.get(contextID);

		if (ss.predicate.startsWith(SaplConstants.STATEMENT_PREFIX) || ss.predicate.startsWith(SaplConstants.CONTEXT_PREFIX)) {
			print("Error: Cannot use context or statement ID as a predicate: " + ss);
			return;
		}

		//creating links through hasMember and memberOf
		if (contextID.equals(SaplConstants.GENERAL_CONTEXT)
				&& ((ss.predicate.equals(SaplConstants.hasMember) && (ss.isSubjectContext() || ss.subject.equals(SaplConstants.G))) ||
						(ss.predicate.equals(SaplConstants.memberOf) && (ss.isObjectContext()  || ss.object.equals(SaplConstants.G)) ))) {
			String sts;
			String context;
			if (ss.predicate.equals(SaplConstants.hasMember)) {
				sts = ss.object;
				context = ss.subject;
			} else {
				sts = ss.subject;
				context = ss.object;
			}
			if(context.equals(SaplConstants.G)) context = SaplConstants.GENERAL_CONTEXT;

			if (sts.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				this.copyLinks(sts, context);
				return;
			}
			if (this.beliefs.get(sts) != null) {
				this.putLink(context, sts);
				return;
			}
		}
		//changing resources through rdf:subject, rdf:object and rdf:predicate
		if (contextID.equals(SaplConstants.GENERAL_CONTEXT) && ss.subject.startsWith(SaplConstants.STATEMENT_PREFIX) &&
				(ss.predicate.equals(SaplConstants.rdfSubject)
						|| ss.predicate.equals(SaplConstants.rdfPredicate)
						|| ss.predicate.equals(SaplConstants.rdfObject))
				) {
			SemanticStatement st = this.beliefs.get(ss.subject);
			if (st != null) {
				for(String cont : st.memberOf)
					this.contexts.get(cont).removeFromIndices(st, ss.subject);

				if(ss.predicate.equals(SaplConstants.rdfSubject))
					st.subject = ss.object;
				else if(ss.predicate.equals(SaplConstants.rdfPredicate))
					st.predicate = ss.object;
				if(ss.predicate.equals(SaplConstants.rdfObject))
					st.object = ss.object;
				
				for(String cont : st.memberOf)
					this.contexts.get(cont).addToIndices(st, ss.subject);
			}
			return;
		}
		//remove sapl:or wrappings if either of the sides is empty
		else if (ss.predicate.equals(SaplConstants.or)) {
			ContextContainer sc = this.contexts.get(ss.subject);
			ContextContainer oc = this.contexts.get(ss.object);
			if ((sc != null) && (oc != null)) {
				if ((sc.members.size() == 0) && (oc.members.size() == 0)) {
					return;
				}
				if ((sc.members.size() == 0) && (oc.members.size() != 0)) {
					this.copyLinks(ss.object, contextID);
					return;
				}
				if ((oc.members.size() == 0) && (sc.members.size() != 0)) {
					this.copyLinks(ss.subject, contextID);
					return;
				}
			}
		}
		//remove residual set wrappings like sapl:All (after variables were bound) - this is for inter-agent querying
		else if (SaplConstants.set_predicates.contains(ss.predicate) && !ss.isObjectAny()) {
			this.copyLinks(ss.subject, contextID);
			return;
		}

		//statement without contexts
		else if (!ss.isSubjectContext() && (!ss.isObjectContext() || ss.isObjectExplicitLiteral())) {
			//statementID sapl:is sapl:true case
			//reference to a local ID -> substitute with real ID
			if (ss.predicate.equals(SaplConstants.is) && ss.object.equals(SaplConstants.TRUE) && !ss.isSubjectVar()) {
				String id = this.ids.get(ss.subject);
				if (id == null) {
					//reference to a real ID, also allowed
					if (this.beliefs.get(ss.subject) != null) {
						id = ss.subject;
					} else {
						//New in v4.1: may also be a script to include
						try {
							String content = HttpTools.getContent(ss.subject);
							
							if(!content.equals(ss.subject)){
								SaplCode code = SaplN3Parser.compile(content, ss.subject);
								if (code != null) {
									SaplN3Parser.load(code.getCode(), this, contextID, null, ss.subject, true);
								}
								return;
							}
						} catch (SaplSyntaxError e) {
							print("Syntax error in "+e.getTextID()+", line "+e.getToken().getLine()+", column "+e.getToken().getLineCharStart()+":");
							print("\t"+e.getMessage());
							return;
						} catch (Throwable e) {
							print("Error: when loading included script in " + ss);
							e.printStackTrace();
							return;
						}

						print("Error: undefined local ID or unknown script name '" + ss.subject + "' in " + ss);
						return;
					}
				}
				this.putLink(contextID, id);
				return;
			}

			Set<String> sub_set = container.index_sub.get(ss.subject);
			if (sub_set != null) {
				Set<String> ob_set = container.index_ob.get(ss.object);
				if (ob_set != null) {
					Set<String> pred_set = container.index_pred.get(ss.predicate);
					if (pred_set != null) {
						Set<String> result = SetOperation.intersect3Sets(ob_set, sub_set, pred_set);
						if (result.size() > 0) {
							return; // do not add if already exists
						}
					}
				}
			}
		} else if (ss.isSubjectContext() ^ ss.isObjectContext()) {
			//remove "{} is true" wrappings
			if (ss.predicate.equals(SaplConstants.is) && ss.object.equals(SaplConstants.TRUE)) {
				this.copyLinks(ss.subject, contextID);
				return;
			}

			//New on 09.05.2018
			//remove "{} is verbatim" wrappings (only if on G level)
			if (ss.predicate.equals(SaplConstants.is) && ss.object.equals(SaplConstants.verbatim)
					&& contextID.equals(SaplConstants.GENERAL_CONTEXT)) {
				this.copyLinks(ss.subject, contextID);
				return;
			}

			//unwrap the statement inside the definition of a local ID, ID itself has already been added in putBelief()
			if (ss.predicate.equals(SaplConstants.ID) && ss.isSubjectContext() && !ss.isObjectVar()) {
				this.copyLinks(ss.subject, contextID);
				return;
			}

			if (doJoinCheck && !contextID.equals(SaplConstants.GOALS_CONTEXT) && !contextID.equals(SaplConstants.ACHIEVE_CONTEXT)
					&& !SaplConstants.context_predicates.contains(ss.predicate) && !ss.object.equals(SaplConstants.Optional)) {
				String requestedID = null;
				boolean toCheck = true;
				Set<String> hs = null;
				if (ss.isSubjectContext()) {
					Set<String> ob_set = container.index_ob.get(ss.object);
					if (ob_set != null) {
						Set<String> pred_set = container.index_pred.get(ss.predicate);
						if (pred_set != null) {
							hs = SetOperation.intersect3Sets(ob_set, pred_set, null);
							if (hs.size() == 0) {
								toCheck = false;
							} else {
								requestedID = ss.subject;
							}
						} else {
							toCheck = false;
						}
					} else {
						toCheck = false;
					}
				} else {
					Set<String> sub_set = container.index_sub.get(ss.subject);
					if (sub_set != null) {
						Set<String> pred_set = container.index_pred.get(ss.predicate);
						if (pred_set != null) {
							hs = SetOperation.intersect3Sets(sub_set, pred_set, null);
							if (hs.size() == 0) {
								toCheck = false;
							} else {
								requestedID = ss.object;
							}
						} else {
							toCheck = false;
						}
					} else {
						toCheck = false;
					}
				}

				if (toCheck) {
					Iterator<String> it = hs.iterator();
					while (it.hasNext()) {
						String matchID = it.next();
						SemanticStatement match = this.beliefs.get(matchID);
						String existID;
						if (ss.isSubjectContext()) {
							existID = match.subject;
						} else {
							existID = match.object;
						}
						if (existID.startsWith(SaplConstants.CONTEXT_PREFIX)) {
							ContextContainer cont = this.contexts.get(existID);
							if (cont != null) {
								if (!requestedID.equals(existID)) {
									this.copyLinks(requestedID, existID);
								}
								return;
							}
						}
					}
				}
			}
		}

		if ((ss.subject.startsWith(SaplConstants.STATEMENT_PREFIX) && !ss.predicate.equals(SaplConstants.memberOf))
				|| (ss.object.startsWith(SaplConstants.STATEMENT_PREFIX) && !ss.isObjectExplicitLiteral()
						&& !ss.predicate.equals(SaplConstants.add) && !ss.predicate.equals(SaplConstants.remove) && !ss.predicate.equals(SaplConstants.erase))
						&& !ss.predicate.equals(SaplConstants.hasMember)) {
			print("Error: Statements about statements are not allowed: " + ss);
			return;
		}


		if(contextID.equals(SaplConstants.GENERAL_CONTEXT) && ss.predicate.equals(SaplConstants.MetaRuleLevel)){
			try{
				Integer level = Integer.valueOf(ss.object);
				metaruleContexts.put(level, ss.subject);
				if(!metaruleLevels.contains(level)){
					metaruleLevels.add(level);
					Collections.sort(metaruleLevels);
				}
			}
			catch(NumberFormatException e){
				print("Error: MetaRuleLevels must be integers" + ss);
				return;
			}
		}

		//if nothing else has been triggered, just add the link
		ContextContainer cont = this.contexts.get(contextID);
		cont.members.add(statementID);
		ss.referenced++;
		ss.memberOf.add(contextID);
		cont.addToIndices(ss, statementID);
	}

	/**Removes a link from a context to a statement
	 * @param contextID ID of the context
	 * @param statementID ID of the statement 
	 * */
	synchronized void removeLink(String contextID, String statementID) {
		ContextContainer cont = this.contexts.get(contextID);
		if (cont!=null && cont.members.remove(statementID)) {
			SemanticStatement st = this.beliefs.get(statementID);
			if (st != null) {
				cont.removeFromIndices(st, statementID);
				st.referenced--;
				st.memberOf.remove(contextID);

				if(contextID.equals(SaplConstants.GENERAL_CONTEXT) && st.predicate.equals(SaplConstants.MetaRuleLevel)){
					Integer level = Integer.valueOf(st.object);
					metaruleContexts.remove(level);
					metaruleLevels.remove(level);
				}
			}

		}
	}


	/**Links beliefs from one context to another context
	 * @param contextID Source context
	 * @param targetID Target context
	 * */
	synchronized void copyLinks(String contextID, String targetID) {
		Iterator<String> it = this.contexts.get(contextID).members.iterator();
		while (it.hasNext()) {
			this.putLink(targetID, it.next());
		}
	}

	/**Searches for contexts that have all the same statements as the given context.
	 * Such a situation is created by such constructs as {s1, s2, ...} -> {} ; sapl:else {} 
	 * @param contextID Context
	 * @return A set of found contexts, or null if none found 
	 * */
	synchronized Set<String> getParallelContexts(String contextID) {
		ContextContainer cnt = this.contexts.get(contextID);
		if (cnt == null) {
			return null;
		}

		Set<String> otherContexts = new HashSet<String>();

		Iterator<String> it = cnt.members.iterator();
		boolean first = true;
		while (it.hasNext()) {
			SemanticStatement st = this.beliefs.get(it.next());
			if (first) {
				if (st.referenced > 1) {
					Iterator<String> cit = st.memberOf.iterator();
					while (cit.hasNext()) {
						String context = cit.next();
						if (!context.equals(contextID)) {
							otherContexts.add(context);
						}
					}
				}
				first = false;
			} else {
				Iterator<String> cit = otherContexts.iterator();
				while (cit.hasNext()) {
					String context = cit.next();
					if (!st.memberOf.contains(context)) {
						cit.remove();
					}
				}
			}
			if (otherContexts.size() == 0) {
				return null;
			}
		}
		return otherContexts;
	}


	/**Collects garbage, i.e. non-referenced contexts and statements as well as empty contexts*/
	void collectGarbage() {
		synchronized(lock){
			boolean done = false;
			while (!done) {
				done = true;
				//removing contexts that are not referenced or are empty
				List<String> removeContexts = new ArrayList<String>();
				for (Entry<String, ContextContainer> entry : this.contexts.entrySet()) {
					String contextID = entry.getKey();
					if (SaplConstants.protected_entities.contains(contextID)) {
						continue;
					}
					ContextContainer contextContainer = entry.getValue();
					if ((contextContainer.referenced <= 0) || contextContainer.members.isEmpty()) {
						removeContexts.add(contextID);
						done = false;
					}
				}

				for (String context : removeContexts) {
					this.removeContext(context);
				}

				//removing hanging beliefs
				List<String> removeBeliefs = new ArrayList<String>();
				for (Entry<String, SemanticStatement> entry : this.beliefs.entrySet()) {
					SemanticStatement statement = entry.getValue();
					if (statement.referenced <= 0) {
						removeBeliefs.add(entry.getKey());
						done = false;
					}
				}

				for (String belief : removeBeliefs) {
					this.removeBelief(belief);
				}
			}
		}
	}
	
	/**Used by print functions*/
	public static String getCurrentTime(){
		return sdf.format(new Date(System.currentTimeMillis()));
	}

}
