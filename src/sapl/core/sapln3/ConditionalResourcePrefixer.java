package sapl.core.sapln3;

import java.util.HashMap;
import java.util.Map;

/**
 * ResourcePrefixer with memory about the namespaces it has been using. 
 * Furthermore, it only prefixes in case it is initialized to do that.
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.0 (02.05.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2007-2009, Michael Cochez
 * 
 * */

public class ConditionalResourcePrefixer {
	/**
	 * The prefixer which is doing the actual work
	 */
	private final ResourcePrefixer actualPrefixer;
	/**
	 * Do I have to prefix at all?
	 */
	private final boolean usenamespaces;
	/**
	 * The namespaces which have been used by this prefixer up to now.
	 */
	private final Map<String, String> usedNS = new HashMap<String, String>();

	/**
	 * Construct a conditional resourcePrefixer.
	 * 
	 * @param prefixer
	 *            the actual prefixer able to do prefixing.
	 * @param prefix
	 *            only if true, prefixing will be done
	 */
	public ConditionalResourcePrefixer(ResourcePrefixer prefixer, boolean prefix) {
		this.actualPrefixer = prefixer;
		this.usenamespaces = prefix;
	}

	/**
	 * Prefixes the given resource and if a previously not used namespace is used, it will be remembered by this prefixer.
	 * 
	 * @param resource
	 *            the resource which should be prefixed
	 * @return the prefixed representation of the resource
	 */
	public String prefixResource(String resource) {
		if (!this.usenamespaces) {
			return resource;
		}
		//call the actualPrefixer and catch which namesaces he's using
		return this.actualPrefixer.prefixResource(resource, this.usedNS);
	}

	/**
	 * returns a HashMap containing all namespaces used by this prefixer in prefix-url form
	 * 
	 * @return The map of namespaces which have been used.
	 */
	public Map<String, String> getUsedNameSpaces() {
		return new HashMap<String, String>(this.usedNS);
	}
}
