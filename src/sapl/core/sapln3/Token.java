package sapl.core.sapln3;

/**
 * Representation of a text token, used by the S-APL Parser 
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.1 (10.10.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class Token {
	String token;
	int line;

	int textCharStart = -1;
	int lineCharStart = -1;
	
	public Token(String token, int line, int textCharStart, int lineCharStart){
		this.token = token;
		this.line = line;
		this.textCharStart = textCharStart;
		this.lineCharStart = lineCharStart;
	}
	
	public String getToken(){
		return token;
	}
	
	public int getLine(){
		return line;
	}

	public int getTextCharStart(){
		return textCharStart;
	}
	
	public int getLineCharStart(){
		return lineCharStart;
	}
	
	public String toString(){
		return token;
	}
}
