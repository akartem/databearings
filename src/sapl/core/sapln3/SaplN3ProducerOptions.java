package sapl.core.sapln3;

/**
 * Options for producing N3-based notation of S-APL. 
 * These options are passed to {@link SaplN3Producer}
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.5 (11.04.2017) 
 * 
 * Copyright (C) 2013-2017, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplN3ProducerOptions {

	/** Default constructor.
	 * Identical to SaplN3ProducerOptions(true, true, true, false)
	 */
	private SaplN3ProducerOptions() {
		this(true, true, true, true, false, false);
	}

	public static SaplN3ProducerOptions defaultOptions = new SaplN3ProducerOptions();

	/**
	 * Identical to SaplN3ProducerOptions(indents, true, useNameSpaces, false)
	 * 
	 * @param indents
	 *            Should indentation be used?
	 * @param usePrefixes
	 *            Should prefixes be used?
	 */
	public SaplN3ProducerOptions(boolean indents, boolean usePrefixes) {
		this(indents, true, usePrefixes, true, false, false);
	}
	
	public SaplN3ProducerOptions(boolean indents, boolean usePrefixes, boolean produceTurtle) {
		this(indents, true, usePrefixes, true, produceTurtle, false);
	}
	

	/**
	 * Create a options object for SaplN3 generation
	 * 
	 * @param indents
	 *            Should indentation be used?
	 * @param includeRules
	 *            Should rules be included in the result?
	 * @param usePrefixes
	 *            Should prefixes be used?
	 * @param cleanContent
	 *            Should certain content be cleaned?
	 */
	public SaplN3ProducerOptions(boolean indents, boolean includeRules, boolean usePrefixes, boolean includeNested, boolean produceTurtle, boolean cleanContent) {
		super();
		this.indents = indents;
		this.includeRules = includeRules;
		this.includeNested = includeNested;
		this.usePrefixes = usePrefixes;
		this.cleanContent = cleanContent;
		this.produceTurtle = produceTurtle;
	}

	/**
	 * Should the produced S-APL N3 text use indentation (more readable, more verbose)
	 */
	public final boolean indents;

	/**
	 * Should prefixes be used or should the output always use full URIs
	 */
	public final boolean usePrefixes;

	/**
	 * Should rules be included in the output (The ones in sapl:Rule or sapl:MetaRule contexts?
	 * If false and the S-APL contains rules, "Contents are skipped" is added to the output instead of the rules.
	 */
	public final boolean includeRules;

	/**
	 * Should the whole N3 structure be produced (normal use), or only plain RDF statements be included?  
	 */
	public final boolean includeNested;

	
	/**Should Turtle notation be attempted? If not, N-Triples are output inside each context*/
	public final boolean produceTurtle;
	
	
	/**
	 * Should certain content be cleaned from the output? Check {@link SaplN3Producer} implementation for what gets removed exactly.
	 * Was introduced for use in inter-agent communication, otherwise not normally needed.
	 */
	public final boolean cleanContent;

}
