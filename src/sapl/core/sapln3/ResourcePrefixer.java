package sapl.core.sapln3;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import sapl.core.SaplConstants;

/**
 * Provides methods for replacing the whole name of a resource by their prefix example:
 * <http://www.ubiware.jyu.fi/sapl#MyResource> to sapl:MyResource
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * 
 * @version 4.1 (16.01.2014)
 * 
 * Copyright (C) 2013-2014, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class ResourcePrefixer {
	/**
	 * The namespaces known by this prefixer
	 * maps namespaces to prefixes 
	 */
	private Map<String, String> knownNamespaces = new HashMap<String, String>();

	/**
	 * Prefixes a resource using known namespaces
	 * @param resource The resource whose namespace should be replaced by its prefix
	 * @return The prefixed string
	 */
	public String prefixResource(String resource) {
		return prefixResource(resource, null);
	}

	/**
	 * Prefixes a resource using known namespaces + add to the map of namespaces actually used
	 * @param resource The resource whose url should be replaced by its prefix
	 * @param usedNS a map to which the namespace - prefix pair which was used will be added
	 * @return The prefixed string
	 */
	public String prefixResource(String resource, Map<String, String> usedNS) {
		if (resource.startsWith(SaplConstants.BLANK_NODE_PREFIX)) {
			return resource;
		}

		for (String namespace : this.knownNamespaces.keySet()) {
			if (resource.startsWith(namespace)) {
				String prefix = this.knownNamespaces.get(namespace);
				if(usedNS!=null) usedNS.put(namespace, prefix);
				//remove the namespace from the resource:
				String resourceWithoutNamespace = resource.substring(namespace.length(), resource.length());
				return prefix + resourceWithoutNamespace;
			}
		}
		//no prefix for the resource found, return integrally.
		return resource;
	}

	/**
	 * Adds namespaces to this ResourcePrefixer. 
	 * If a previous namespace was defined with the another prefix, the old value will be overwritten.
	 * @param namespaces The pairs of prefix -> namespace
	 */
	void addNameSpaces(HashMap<String, String> namespaces) {
		for (Entry<String, String> entry : namespaces.entrySet()) {
			String prefix = entry.getKey();
			String namespace = entry.getValue();
			this.knownNamespaces.put(namespace, prefix);
		}
	}

}
