package sapl.core.sapln3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SemanticStatement;
import sapl.util.HttpTools;
import sapl.util.StringEscape;

/**
 * Parser for the textual N3-based notation of S-APL language.
 * Adds the content to a S-APL data model
 * 
 * @author Artem Katasonov (Univeristy of Jyväskylä + VTT 2009-2018 + Elisa since July 2018) 
 * @author Michael Cochez (University of Jyväskylä)
 * 
 * @version 5.1 (31.07.2019)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplN3Parser {

	public final static String PREFIX_KEYWORD = "@prefix";
	
	/**Is a static class. Constructor disabled*/
	private SaplN3Parser() {}

	/**Syntax table*/
	private static final String a[][] = new String[SaplN3Parser.uCount][SaplN3Parser.tCount];
	/**Number of terminal symbols*/
	private static final int tCount = 11;
	/**Terminal symbols*/
	private static final char t[] = new char[SaplN3Parser.tCount];
	/**Number of non-terminal symbols: 'A','B','C'...*/
	//private static final int uCount = 11;
	private static final int uCount = 12;
	/**Error messages*/
	//private static final String error[] = new String[4];
	private static final String error[] = new String[5];

	static {

		SaplN3Parser.t[0] = '?';
		SaplN3Parser.t[1] = '{';
		SaplN3Parser.t[2] = '}';
		SaplN3Parser.t[3] = '.';
		SaplN3Parser.t[4] = ';';
		SaplN3Parser.t[5] = ',';
		SaplN3Parser.t[6] = '[';
		SaplN3Parser.t[7] = ']';
		SaplN3Parser.t[8] = '(';
		SaplN3Parser.t[9] = ')';
		SaplN3Parser.t[10] = '#';

		SaplN3Parser.a[0][0] = "B#"; //A: Program
		SaplN3Parser.a[0][1] = "B#";
		SaplN3Parser.a[0][2] = "$3";
		SaplN3Parser.a[0][3] = "$3";
		SaplN3Parser.a[0][4] = "$3";
		SaplN3Parser.a[0][5] = "$3";
		SaplN3Parser.a[0][6] = "B#";
		SaplN3Parser.a[0][7] = "$3";
		SaplN3Parser.a[0][8] = "B#";
		SaplN3Parser.a[0][9] = "$3";
		SaplN3Parser.a[0][10] = "_"; 
		SaplN3Parser.a[1][0] = "DaC"; //B: StatementList
		SaplN3Parser.a[1][1] = "DaC";
		SaplN3Parser.a[1][2] = "_";
		SaplN3Parser.a[1][3] = "$3";
		SaplN3Parser.a[1][4] = "$3";
		SaplN3Parser.a[1][5] = "$3";
		SaplN3Parser.a[1][6] = "DaC";
		SaplN3Parser.a[1][7] = "$3";
		SaplN3Parser.a[1][8] = "DaC";
		SaplN3Parser.a[1][9] = "$3";
		SaplN3Parser.a[1][10] = "_"; 
		SaplN3Parser.a[2][0] = ""; //C: StatementListTail
		SaplN3Parser.a[2][1] = "";
		SaplN3Parser.a[2][2] = "_";
		SaplN3Parser.a[2][3] = ".B";
		SaplN3Parser.a[2][4] = "";
		SaplN3Parser.a[2][5] = "";
		SaplN3Parser.a[2][6] = "";
		SaplN3Parser.a[2][7] = "$3";
		SaplN3Parser.a[2][8] = "";
		SaplN3Parser.a[2][9] = "$3";
		SaplN3Parser.a[2][10] = "_"; 
		SaplN3Parser.a[3][0] = "EF"; //D: SameSubjectStatements
		SaplN3Parser.a[3][1] = "EF";
		SaplN3Parser.a[3][2] = "$3";
		SaplN3Parser.a[3][3] = "$3";
		SaplN3Parser.a[3][4] = "$3";
		SaplN3Parser.a[3][5] = "$3";
		SaplN3Parser.a[3][6] = "EF";
		SaplN3Parser.a[3][7] = "$3";
		SaplN3Parser.a[3][8] = "EF";
		SaplN3Parser.a[3][9] = "$3";
		SaplN3Parser.a[3][10] = "$3"; 
		SaplN3Parser.a[4][0] = "?"; //E: Subject
		SaplN3Parser.a[4][1] = "{dB}e";
		SaplN3Parser.a[4][2] = "$3";
		SaplN3Parser.a[4][3] = "$3";
		SaplN3Parser.a[4][4] = "$3";
		SaplN3Parser.a[4][5] = "$3";
		//SaplN3Parser.a[4][6] = "[fF]b";
		SaplN3Parser.a[4][6] = "[fL";
		
		SaplN3Parser.a[4][7] = "$3";
		SaplN3Parser.a[4][8] = "(flhEbK)jka";
		SaplN3Parser.a[4][9] = "$3";
		SaplN3Parser.a[4][10] = "$3"; 
		SaplN3Parser.a[5][0] = "JIHG"; //F: PropertyList
		SaplN3Parser.a[5][1] = "$0";
		SaplN3Parser.a[5][2] = "$0";
		SaplN3Parser.a[5][3] = "$0";
		SaplN3Parser.a[5][4] = "$0";
		SaplN3Parser.a[5][5] = "$0";
		SaplN3Parser.a[5][6] = "$0";
		SaplN3Parser.a[5][7] = "$0";
		SaplN3Parser.a[5][8] = "$0";
		SaplN3Parser.a[5][9] = "$0";
		SaplN3Parser.a[5][10] = "$0"; 
		SaplN3Parser.a[6][0] = "$2"; //G: PropertyListTail
		SaplN3Parser.a[6][1] = "$2";
		SaplN3Parser.a[6][2] = "_";
		SaplN3Parser.a[6][3] = "_";
		SaplN3Parser.a[6][4] = ";bF";
		SaplN3Parser.a[6][5] = "";
		SaplN3Parser.a[6][6] = "$2";
		SaplN3Parser.a[6][7] = "_";
		SaplN3Parser.a[6][8] = "$2";
		SaplN3Parser.a[6][9] = "";
		SaplN3Parser.a[6][10] = "_"; 
		SaplN3Parser.a[7][0] = "$2"; //H: ObjectTail
		SaplN3Parser.a[7][1] = "$2";
		SaplN3Parser.a[7][2] = "_";
		SaplN3Parser.a[7][3] = "_";
		SaplN3Parser.a[7][4] = "_";
		SaplN3Parser.a[7][5] = ",cIH";
		SaplN3Parser.a[7][6] = "$2";
		SaplN3Parser.a[7][7] = "_";
		SaplN3Parser.a[7][8] = "$2";
		SaplN3Parser.a[7][9] = "";
		SaplN3Parser.a[7][10] = "_"; 
		SaplN3Parser.a[8][0] = "?"; //I: Object
		SaplN3Parser.a[8][1] = "{dB}e";
		SaplN3Parser.a[8][2] = "$1";
		SaplN3Parser.a[8][3] = "$1";
		SaplN3Parser.a[8][4] = "$1";
		SaplN3Parser.a[8][5] = "$1";
		//SaplN3Parser.a[8][6] = "[fF]b";
		SaplN3Parser.a[8][6] = "[fL";
		
		SaplN3Parser.a[8][7] = "$1";
		SaplN3Parser.a[8][8] = "(flhEbK)jka";
		SaplN3Parser.a[8][9] = "$1";
		SaplN3Parser.a[8][10] = "$1"; 
		SaplN3Parser.a[9][0] = "?"; //J: Predicate
		SaplN3Parser.a[9][1] = "$0";
		SaplN3Parser.a[9][2] = "$0";
		SaplN3Parser.a[9][3] = "$0";
		SaplN3Parser.a[9][4] = "$0";
		SaplN3Parser.a[9][5] = "$0";
		SaplN3Parser.a[9][6] = "$0";
		SaplN3Parser.a[9][7] = "$0";
		SaplN3Parser.a[9][8] = "$0";
		SaplN3Parser.a[9][9] = "$0";
		SaplN3Parser.a[9][10] = "$0"; 
		SaplN3Parser.a[10][0] = "jfihEbK"; //K: ResourceListTail
		SaplN3Parser.a[10][1] = "jfihEbK";
		SaplN3Parser.a[10][2] = "$1";
		SaplN3Parser.a[10][3] = "$1";
		SaplN3Parser.a[10][4] = "$1";
		SaplN3Parser.a[10][5] = "$1";
		SaplN3Parser.a[10][6] = "jfihEbK";
		SaplN3Parser.a[10][7] = "$1";
		SaplN3Parser.a[10][8] = "jfihEbK";
		SaplN3Parser.a[10][9] = "_";
		SaplN3Parser.a[10][10] = "$1"; 
		
		SaplN3Parser.a[11][0]="F]b"; //L: PropertyList+']' or just ']' 
		SaplN3Parser.a[11][1]="$4";
		SaplN3Parser.a[11][2]="$4";
		SaplN3Parser.a[11][3]="$4";
		SaplN3Parser.a[11][4]="$4";
		SaplN3Parser.a[11][5]="$4";
		SaplN3Parser.a[11][6]="$4";
		SaplN3Parser.a[11][7]="]";
		SaplN3Parser.a[11][8]="$4";
		SaplN3Parser.a[11][9]="$4";
		SaplN3Parser.a[11][10]="$4";	 

		SaplN3Parser.error[0] = "A value (predicate) is expected but ";
		SaplN3Parser.error[1] = "A value (object), [ , ( or { is expected but ";
		SaplN3Parser.error[2] = "The statement is finalized but additional ";
		SaplN3Parser.error[3] = "A value (subject), [ or { is expected but ";
		
		SaplN3Parser.error[4] = "A value (predicate) or ] is expected but ";
	}

	/**Values to be substituted when producing a S-APL model (language shorthands)*/
	public static final Map<String, String> replace = SaplN3Parser.createReplaceMap();
	static Map<String, String> createReplaceMap() {
		Map<String, String> replaceMap = new HashMap<String, String>();
		replaceMap.put("a", SaplConstants.rdfType);

		replaceMap.put("=>", SaplConstants.implies);
		replaceMap.put(">>", SaplConstants.achievedBy);
		replaceMap.put("->", SaplConstants.impliesNow);
		replaceMap.put("==>", SaplConstants.infers);

		replaceMap.put("<-", SaplConstants.expression);

		replaceMap.put(">", SaplConstants.gt);
		replaceMap.put("<", SaplConstants.lt);
		replaceMap.put(">=", SaplConstants.gte);
		replaceMap.put("<=", SaplConstants.lte);
		replaceMap.put("!=", SaplConstants.neq);
		replaceMap.put("=", SaplConstants.eq);
		return Collections.unmodifiableMap(replaceMap);
	}


	/**Loads an S-APL document from a file and complies it into code
	 * that can be used for producing an S-APL model*/
	public static SaplCode compileFile(String filename) throws Exception {
		String text = HttpTools.getContent(filename);
		//File datafile = new File(filename);
		//String text = StreamingUtils.getInputFromFile(datafile, "UTF-8");
		return SaplN3Parser.compile(text, filename);
	}

	/**Compiles a S-APL document into code that can be used for producing an S-APL model
	 * @param text S-APL document
	 * @param textID Some identifier of the document: for error reporting
	 * */
	public static SaplCode compile(String text, String textID) {
		int sz = Math.min(100, text.length() / 10);
		List<Token> tokens = new ArrayList<Token>(sz);
		StringBuilder lexStringBuilder = new StringBuilder(sz);
		
		text = text+" ";
		SaplN3Parser.lexicalAnalysis(text, tokens, lexStringBuilder);
		
		List<String> code = new ArrayList<String>(sz);

		if ((lexStringBuilder.length() > 0) && SaplN3Parser.syntaxAnalysis(tokens, lexStringBuilder.toString(), code, textID, text)) {
			return new SaplCode(textID, tokens, code);
		} else {
			return null;
		}
	}

	/**Performs the lexical analysis of S-APL N3 document
	 * @param text S-APL N3 document
	 * @param tokens An empty list which the method will fill with tokens from the document 
	 * @param lexStringBuilder An empty StringBuilder which the method will fill with a lexical string representing the document
	 * */
	private static void lexicalAnalysis(String text, List<Token> tokens, StringBuilder lexStringBuilder) {
		boolean inside_literal = false;
		char literal_start = ' ';
		boolean inside_comment = false;
		boolean comment_is_line = false;
		boolean inside_uri = false;

		int currLine = 1;
		int currLineStart = 0;
		
		int len = text.length();
		int tokS = 0;
		for (int c = 0; c < len; c++) {
			char ch = text.charAt(c);
			if (ch == '\n') {
				inside_uri = false;
				if (!inside_comment && !inside_literal) {
					if (tokS != c) {
						tokens.add(new Token(text.substring(tokS, c), currLine, tokS, tokS-currLineStart));
						lexStringBuilder.append('?');
					}
					tokS = c + 1;
				} else if (inside_comment && comment_is_line) {
					inside_comment = false;
					tokS = c + 1;
				}

				//tokens.add(" "); //to count lines
				//lexStringBuilder.append(' '); //to count lines
				currLine++;
				currLineStart = c;
				
			} else if (!inside_comment && ((ch == '"') || (ch == '\'')) && ((c == 0) || (text.charAt(c - 1) != '\\'))) {
				if (!inside_literal) {
					inside_literal = true;
					literal_start = ch;
				} else if (ch==literal_start){
					inside_literal = false;
					inside_uri = false;
				}
			} else if (ch == '<') {
				inside_uri = true;
			} else if (ch == '>') {
				inside_uri = false;
			} else if (!inside_uri && !inside_literal && !inside_comment && ch == '#') {
				inside_comment = true;
				comment_is_line = true;
			} else if (!inside_literal && !inside_comment && ((c + 1) < len) && (ch == '/')) {
				if ((text.charAt(c + 1) == '*') || (!inside_uri && (text.charAt(c + 1) == '/'))) {
					if (tokS != c) {
						tokens.add(new Token(text.substring(tokS, c),currLine, tokS, tokS-currLineStart));
						lexStringBuilder.append('?');
					}
					inside_comment = true;
					if (text.charAt(c + 1) == '/') {
						comment_is_line = true;
					} else {
						comment_is_line = false;
					}
					c++;
				}
			} else if (inside_comment && !comment_is_line && ((c + 1) < len) && (ch == '*') && (text.charAt(c + 1) == '/')) {
				inside_comment = false;
				tokS = c + 2;
				c++;
			} else if (!inside_comment && !inside_literal && Character.isWhitespace(ch)) {
				inside_uri = false;
				if (tokS != c) {
					tokens.add(new Token(text.substring(tokS, c),currLine, tokS, tokS-currLineStart));
					lexStringBuilder.append('?');
				}
				tokS = c + 1;
			} else if (!inside_comment
					&& !inside_literal
					&& !inside_uri
					&& ((ch == '{') || (ch == '}') || (ch == '[') || (ch == ']') || (ch == '(') || (ch == ')') || (((ch == '.')
							|| (ch == ';') || (ch == ','))
							&& ((c + 1) < len) && Character.isWhitespace(text.charAt(c + 1))))) {
				if (tokS != c) {
					tokens.add(new Token(text.substring(tokS, c),currLine, tokS, tokS-currLineStart));
					lexStringBuilder.append('?');
				}
				tokens.add(new Token(""+ch,currLine, tokS, tokS-currLineStart));
				lexStringBuilder.append(ch);
				tokS = c + 1;
			}
		}
		if (!inside_comment && (tokS < len)) {
			String rest = text.substring(tokS, len);
			tokens.add(new Token(rest,currLine, tokS, tokS-currLineStart));
			lexStringBuilder.append('?');
		}
	}
	
	/**Performs the syntax analysis of S-APL N3 document.
	 * @param tokens A list with tokens from the S-APL document produced by lexical analysis 
	 * @param lexStringBuilder A lexical string representing the document produced by lexical analysis
	 * @param code An empty list which the method will fill in with the code representing the document
	 * @param textID Some identifier of the S-APL document: for error reporting only 
	 * @param originalText S-APL document itself: for error reporting only
	 */
	private static boolean syntaxAnalysis(List<Token> tokens, String lexString, List<String> code, String textID, String originalText) {

		lexString += '#';
		StringBuilder curString = new StringBuilder(100);
		curString.append('A');
		int iLex = 0;
		int iCur = 0;
		//int currLineNumber = 1;
		
		HashSet<String> definedPrefixes = new HashSet<String>();

		while (true) {
			while ((curString.charAt(iCur) >= 'a') && (curString.charAt(iCur) <= 'z')) {
				code.add("{@$@}" + curString.charAt(iCur));
				iCur++;
			}
//			while (lexString.charAt(iLex) == ' ') {
//				currLineNumber++;
//				iLex++;
//			}

			char ch = curString.charAt(iCur);
			char lex_ch = lexString.charAt(iLex);
			if (ch == lex_ch) {
				if (ch == '#') {
					return true;
				} else if (ch == '?') {
					String token = tokens.get(iLex).getToken();
					if (token.startsWith("{@$@}") || token.startsWith(SaplConstants.CONTEXT_PREFIX) || token.startsWith("\""+SaplConstants.CONTEXT_PREFIX) || token.startsWith("'"+SaplConstants.CONTEXT_PREFIX)
							|| token.startsWith(SaplConstants.QUERY_CONTEXT_PREFIX)   
							|| token.startsWith(SaplConstants.STATEMENT_PREFIX) || token.startsWith("\""+SaplConstants.STATEMENT_PREFIX) || token.startsWith("'"+SaplConstants.STATEMENT_PREFIX)) {
						throw new SaplSyntaxError("Illegal value: " + token, textID, tokens.get(iLex), originalText);
					}
					code.add(token);
					
					if(token.equals(PREFIX_KEYWORD)){
						definedPrefixes.add(tokens.get(iLex+1).getToken());
					}
					else{
						if (! (token.startsWith("\"") && token.endsWith("\"") || token.startsWith("'") && token.endsWith("'") || token.startsWith("<") && token.endsWith(">"))) {

							String uri = token;
							if (token.startsWith("\"") || token.startsWith("'")){
								int typingInd = token.indexOf("^^");
								if(typingInd!=-1){
									uri = token.substring(typingInd+2);
									if(uri.startsWith("\"") && uri.endsWith("\"") || uri.startsWith("'") && uri.endsWith("'") || uri.startsWith("<") && uri.endsWith(">"))
										uri=null;
								}
								else uri = null;
							}
							
							if(uri!=null) {
								int ind = uri.indexOf(":");
								if (ind >= 1 && !uri.startsWith(SaplConstants.BLANK_NODE_PREFIX)) {
									String prefix = uri.substring(0, ind + 1);
									if(!definedPrefixes.contains(prefix))
										throw new SaplSyntaxError("Undefined namespace prefix '" + prefix + "' in '"+token+"'", textID, tokens.get(iLex), originalText);
								}								
							}
						}
					}
				}
				iCur++;
				iLex++;
			} else {
				int iy = ch - 'A';
				if ((iy < 0) || (iy > SaplN3Parser.uCount)) {
					String found;
					if (lex_ch == '?') {
						found = "Value (" + tokens.get(iLex) + ")";
					} else if (lex_ch == '#') {
						found = "End of text";
					} else {
						found = "" + lex_ch;
					}
					String expected;
					if (ch == '#') {
						expected = "End of text";
					} else {
						expected = "" + ch;
					}
					
					if(iLex==tokens.size())
						throw new SaplSyntaxError(expected + " is expected but " + found + " is found", textID, tokens.get(iLex-1), originalText);
					throw new SaplSyntaxError(expected + " is expected but " + found + " is found", textID, tokens.get(iLex), originalText);
				}
				int ix = -1;
				for (int i = 0; i < SaplN3Parser.tCount; i++) {
					if (SaplN3Parser.t[i] == lex_ch) {
						ix = i;
						break;
					}
				}

				String fromTable = SaplN3Parser.a[iy][ix];

				if (fromTable.equals("") || fromTable.startsWith("$")) {
					String err = "";
					if (fromTable.startsWith("$")) {
						int ind = Integer.valueOf(fromTable.substring(1));
						err = SaplN3Parser.error[ind];
					}
					String found;
					if (lex_ch == '?') {
						found = "Value (" + tokens.get(iLex) + ")";
					} else if (lex_ch == '#') {
						found = "End of text";
					} else {
						found = "" + lex_ch;
					}

					if(iLex==tokens.size())
						throw new SaplSyntaxError(err + found + " is found", textID, tokens.get(iLex-1), originalText);
					else throw new SaplSyntaxError(err + found + " is found", textID, tokens.get(iLex), originalText);

				} else if (fromTable.equals("_")) {
					curString.deleteCharAt(iCur);
				} else {
					curString.replace(iCur, iCur + 1, fromTable);
				}

			}
		}
	}
	
	/**Adds new content to an S-APL model
	 * @param code Code produced from the analysis of a S-APL document
	 * @param targetModel S-APL model to extend
	 * @param topContext Context to which new content is added
	 * @param textID Some identifier of the S-APL document: for error reporting only
	 * @param doJoinCheck Whether to do contexts join according to S-APL rules
	 * */
	public static void load(List<String> code, SaplModel targetModel, String topContext, List<String> otherContexts, String textID, boolean doJoinCheck) {

		if (targetModel.contexts.get(topContext) == null) {
			throw new Error("Error in beliefs load: context " + topContext + " does not exist\n");
		}
		if(otherContexts!=null){
			for(String otherContext : otherContexts){
				if (targetModel.contexts.get(otherContext) == null) {
					throw new Error("Error in beliefs load: context " + topContext + " does not exist\n");
				}
			}
		}

		targetModel.clearLocalIDs();

		Stack<String> stack = new Stack<String>();
		Stack<String> statementsStack = new Stack<String>();

		HashMap<String, String> namespaces = new HashMap<String, String>();
		namespaces.put(":", "#");

		for (String command : code) {

			//Triple is stated
			//{@$@}a means statement is ended, i.e "." was used
			//{@$@}n means another triple about the same subject follows, i.e ";" was used
			//{@$@}c means another triple about the same subject with the same predicate follows, i.e "," was used
			//{@$@}i means another triple about the same object follows - possible only when parsing RDF lists like (1 2 3)
			if (command.equals("{@$@}a") || command.equals("{@$@}b") || command.equals("{@$@}c") || command.equals("{@$@}i")) {
				String object = stack.pop();
				String predicate = stack.pop();
				String subject = stack.pop();

				String objectDatatype = null;
				String objectLanguage = null;

				if (subject.equals(SaplN3Parser.PREFIX_KEYWORD)) { //namespace is defined
					if (object.startsWith("<") && object.endsWith(">") || object.startsWith("\"") && object.endsWith("\"")) {
						object = object.substring(1, object.length() - 1);
					}
					namespaces.put(predicate, object);
				} else {
					
					String subjectFixed = SaplN3Parser.fixResource(subject, namespaces, textID);
					String predicateFixed = SaplN3Parser.fixResource(predicate, namespaces, textID);
					String match = SaplN3Parser.replace.get(predicateFixed);
					if (match != null) {
						predicateFixed = match;
					}

					String objectFixed = object;
					if (objectFixed.startsWith("\"") && !objectFixed.endsWith("\"") || objectFixed.startsWith("'") && !objectFixed.endsWith("'")) {
						int ind = objectFixed.indexOf("\"^^");
						if(ind==-1) ind = objectFixed.indexOf("'^^");
						if (ind > 0) {
							objectDatatype = objectFixed.substring(ind + 3);
							objectFixed = SaplN3Parser.fixResource(objectFixed.substring(0, ind + 1), namespaces, textID);
							objectDatatype = SaplN3Parser.fixResource(objectDatatype, namespaces, textID);
						} else {
							ind = objectFixed.indexOf("\"@");
							if(ind==-1) ind = objectFixed.indexOf("'@");
							if (ind > 0) {
								objectLanguage = objectFixed.substring(ind + 2);
								objectFixed = SaplN3Parser.fixResource(objectFixed.substring(0, ind+1), namespaces, textID);
							}
						}
					} else {
						objectFixed = SaplN3Parser.fixResource(objectFixed, namespaces, textID);
					}

					String id = targetModel.statementIDGenerator.getNewStatementID();//UbiwareAgent.STATEMENT_PREFIX + String.valueOf(++targetAgent.statementCount);
					SemanticStatement st = new SemanticStatement(subjectFixed, predicateFixed, objectFixed, objectDatatype, objectLanguage);
					targetModel.putBelief(id, st);

					statementsStack.push(id);
				}

				if (command.equals("{@$@}b") || command.equals("{@$@}c")) {
					String cid = subject;
					if (subject.startsWith(SaplConstants.CONTEXT_PREFIX)) {
						subject = targetModel.contextIDGenerator.getNewContextID();
						targetModel.putContext(subject, cid);
					}
					stack.push(subject);
					if (command.equals("{@$@}c")) {
						stack.push(predicate);
					}
				} else if (command.equals("{@$@}i")) {
					stack.push(object);
				}
			} else if (command.equals("{@$@}d")) {//beginning of context
				statementsStack.push("{@$@}");
			} else if (command.equals("{@$@}e")) {//end of context

				List<String> statements = new ArrayList<String>();
				while (true) {
					String statementID = statementsStack.pop();
					if (statementID.equals("{@$@}")) {
						break;
					} else {
						statements.add(statementID);
					}
				}

				String id = targetModel.contextIDGenerator.getNewContextID();
				if (statements.size() > 10) {
					targetModel.putContext(id, statements.size());
				} else {
					targetModel.putContext(id);
				}

				for (int i = statements.size() - 1; i >= 0; i--) {
					targetModel.putLink(id, statements.get(i), doJoinCheck);
				}
				stack.push(id);
			} else if (command.equals("{@$@}f")) {//creation of anonymous node
				String bnID = targetModel.bnodeGenerator.getNewBlankNode();
				stack.push(bnID);
			} else if (command.equals("{@$@}l")) { //copy previous resource
				String object = stack.peek();
				stack.push(object);
			} else if (command.equals("{@$@}h")) {//push rdf:first
				stack.push("<"+SaplConstants.rdfFirst+">");
			} else if (command.equals("{@$@}j")) {//push rdf:rest
				stack.push("<"+SaplConstants.rdfRest+">");
			} else if (command.equals("{@$@}k")) {//push rdf:nil
				stack.push("<"+SaplConstants.rdfNil+">");
			} else {
				stack.push(command);
			}
		}

		if(!stack.isEmpty()){
			throw new Error("Stack is not empty after loading " + textID + " : "+ stack.toString()+"\n");
		}

		List<String> statements = new ArrayList<String>();
		while (!statementsStack.empty()) {
			statements.add(statementsStack.pop());
		}

		for (int i = statements.size() - 1; i >= 0; i--) {
			targetModel.putLink(topContext, statements.get(i), doJoinCheck);
			if(otherContexts!=null){
				for(String otherContext : otherContexts){
					targetModel.putLink(otherContext, statements.get(i), doJoinCheck);
				}
			}
		}

		targetModel.clearLocalIDs();
		targetModel.prefixer.addNameSpaces(namespaces);

	}


	/**Fixes a resource: to its full URI form, or as literal*/
	private static String fixResource(String resource, HashMap<String, String> namespaces, String textID) {
		resource = StringEscape.unEscape(resource);
		if (resource.startsWith("\"") && resource.endsWith("\"") || resource.startsWith("'") && resource.endsWith("'")) {
			//a literal without specifiers, strip off quotes
			return resource.substring(1, resource.length() - 1);
		}
		else if (resource.startsWith("<") && resource.endsWith(">")){
			//URI, strip off angular brackets
			return resource.substring(1, resource.length() - 1);
		} 
		else {
			int ind = resource.indexOf(":");
			if (ind >= 0) {
				String prefix = resource.substring(0, ind + 1);
				if (!prefix.equals(SaplConstants.BLANK_NODE_PREFIX)){
					String uri = namespaces.get(prefix);
					if (uri == null) {
						if (ind == 0) {
							return SaplConstants.EMPTY_NS + resource.substring(1);
						} else {
							//should not ever happen as from v4.1 caught already in syntaxAnalysis
							//throw new SaplSyntaxError("Undefined namespace prefix '" + prefix + "' in '"+resource+"'", textID, 1);
						}
					} else {
						return uri + resource.substring(ind + 1);
					}
				}
			}
		}
		return resource;
	}	

}