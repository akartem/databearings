package sapl.core.sapln3;

/**
 * Syntax errors thrown by S-APL N3 parser
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.5 (19.06.2017)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2017, VTT
 * 
 * */

public class SaplSyntaxError extends Error {

	private static final long serialVersionUID = 1L;
	
	String textID;
	Token token;
	String text;
	
	public SaplSyntaxError(String message, String textID, Token token, String text){
		super(message);
		this.token = token;
		this.textID = textID;
		this.text = text;
	}
	
	public String getTextID(){
		return textID;
	}

	public Token getToken(){
		return token;
	}
	
	public String getText() {
		return text;
	}

}
