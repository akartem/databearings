package sapl.core.sapln3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import sapl.core.ContextContainer;
import sapl.core.SaplConstants;
import sapl.core.SaplModel;
import sapl.core.SemanticStatement;
import sapl.util.StringEscape;
import sapl.util.WhiteSpaceString;

/**
 * Producer of a N3-based notation of S-APL.
 * This is a faster implementation of the original version by Artem Katasonov.
 * Speed improvement is mainly achieved by not generating unused ID's
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * 
 * @version 4.6 (05.07.2018) 
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */

public class SaplN3Producer {
	
	public static final Map<String, String> replace = SaplN3Producer.produceReplaceMap();
	static Map<String, String> produceReplaceMap() {
		Map<String, String> replaceMapInv = new HashMap<String, String>(SaplN3Parser.replace.size());
	    for (Entry<String, String> entry : SaplN3Parser.replace.entrySet())
	    	replaceMapInv.put(entry.getValue(), entry.getKey());
		return Collections.unmodifiableMap(replaceMapInv);
	}
	
	/**
	 * The size of an indentation
	 */
	public final static int indent_size = 4;

	/**
	 * The S-APL model this producer is producing for.
	 */
	private final SaplModel model;

	private final ResourcePrefixer actualPrefixer;

	/**
	 * @see SaplN3ProducerOptions
	 */
	private final boolean usePrefixes;
	private final boolean indents;
	private final boolean cleanContent;
	private final boolean includeRules;
	private final boolean includeNested;

	private boolean produceTurtle; //otherwise N-Triples

	public SaplN3Producer(SaplModel model, ResourcePrefixer prefixer, SaplN3ProducerOptions options) {
		this.model = model;
		this.actualPrefixer = prefixer;
		this.usePrefixes = options.usePrefixes;
		this.indents = options.indents;
		this.cleanContent = options.cleanContent;
		this.includeRules = options.includeRules;
		this.includeNested = options.includeNested;
		this.produceTurtle = options.produceTurtle;
	}

	/**
	 * Produces S-APL N3 for the given {@link ContextContainer}.
	 * 
	 * @param c The {@link ContextContainer}
	 * @return A string containing the S-APL N3 code.
	 */
	public String produceN3(ContextContainer c) {
		if (c == null) {
			throw new NullPointerException("The context container to produce N3 from mustn't be null");
		}
		//check which beliefs are referenced more as once in the structure:
		Set<String> duplicateStatements = this.searchDuplicates(c);

		//the stringbuilder in which the whole code will be put.
		StringBuilder result = new StringBuilder();
		//use of a conditional prefixer, depending on the usePrefixes option and used later to get the used namespaeces.
		ConditionalResourcePrefixer prefixer = new ConditionalResourcePrefixer(this.actualPrefixer, this.usePrefixes);
		this.produceN3Recursion(c, duplicateStatements, new HashMap<String, String>(), result, prefixer, 0);
		//prepend namespaces.

		if (this.usePrefixes) {
			StringBuilder ns_str = new StringBuilder();
			for (Entry<String, String> entry : prefixer.getUsedNameSpaces().entrySet()) {
				String ns = entry.getKey();
				String prefix = entry.getValue();
				if (!ns.equals("#") || !prefix.equals(":")) {
					ns_str.append("@prefix ");
					ns_str.append(prefix);
					ns_str.append(" <");
					ns_str.append(ns);
					ns_str.append("> .");
					if (this.indents) {
						ns_str.append('\n');
					} else {
						ns_str.append(' ');
					}
				}
			}
			ns_str.append(result);//is similar to a prepending the namespaces in the beginning, but this might be slightly faster because less data has to be copied
			return ns_str.toString();//return the string to which we appended everything.
		} else {
			return result.toString();
		}
	}

	/**
	 * Recursive step of the generation. Generates the N3 for the given {@link ContextContainer}
	 * 
	 * @param context
	 *            the {@link ContextContainer} for which N3 should be generated.
	 * @param duplicateStatements
	 *            statements which appear more as once in the document.
	 * @param generatedReferableStatements
	 *            statements which are already generated previously and can be referred to.
	 * @param result
	 *            the StringBuilder to which the result must be appended.
	 * @param prefixer
	 *            the prefixer to use when prefixing a resource.
	 * @param recursiondepth
	 *            the current recursion depth.
	 */
	private void produceN3Recursion(final ContextContainer context, final Set<String> duplicateStatements,
			final Map<String, String> generatedReferableStatements,//map, mapping statementIDs on local IDs, only referable statements are in the map. 
			final StringBuilder result, ConditionalResourcePrefixer prefixer, final int recursiondepth) {

		Map<String, Map<String, List<String>>> subject_map = null;
		Map<String, Integer> bnodeObjectUses = null;
		if(this.produceTurtle){
			subject_map = new LinkedHashMap<String, Map<String, List<String>>>();
			bnodeObjectUses = new HashMap<String, Integer>(); 
			for (String statementID : context.members) {
				final SemanticStatement statement = this.model.beliefs.get(statementID);
				Map<String, List<String>> subject_predicate_map = subject_map.get(statement.subject);
				if(subject_predicate_map==null){
					subject_predicate_map = new LinkedHashMap<String, List<String>>(4);
					subject_map.put(statement.subject, subject_predicate_map);
				}
				List<String> subject_predicate_statements = subject_predicate_map.get(statement.predicate);
				if(subject_predicate_statements==null){
					subject_predicate_statements = new ArrayList<String>(2);
					subject_predicate_map.put(statement.predicate, subject_predicate_statements);
				}
				subject_predicate_statements.add(statementID);
				
				if(!duplicateStatements.contains(statementID) && statement.isObjectBlankNode()){
					Integer count = bnodeObjectUses.get(statement.object);
					if(count==null) bnodeObjectUses.put(statement.object, 1);
					else bnodeObjectUses.put(statement.object, count+1);
				}
			}
		}
		else{
			subject_map = new HashMap<String, Map<String, List<String>>>(1);
			Map<String, List<String>> subject_predicate_map = new HashMap<String, List<String>>(1);
			subject_map.put("", subject_predicate_map);
			subject_predicate_map.put("", context.members);
		}
		
		boolean notFirstContextStatement = false;
		for(String s : subject_map.keySet()){
			if(this.produceTurtle && s.startsWith(SaplConstants.BLANK_NODE_PREFIX)){
				Integer count = bnodeObjectUses.get(s);
				if(count!=null && count==1) continue; // will be expanded as 's p []', so no need to generate anything here
			}
			Map<String, List<String>> subject_predicate_map = subject_map.get(s);
			notFirstContextStatement = produceCommonSubjectStatements(subject_predicate_map, subject_map, bnodeObjectUses,
					duplicateStatements, generatedReferableStatements, result, prefixer, recursiondepth, notFirstContextStatement, false);
		}

	}

	private boolean produceCommonSubjectStatements(Map<String, List<String>> subject_predicate_map,
			Map<String, Map<String, List<String>>> subject_map, Map<String, Integer> bnodeObjectUses, 
			final Set<String> duplicateStatements, final Map<String, String> generatedReferableStatements, 
			final StringBuilder result, ConditionalResourcePrefixer prefixer, final int recursiondepth,
			boolean notFirstContextStatement, boolean skipSubject) {
		
		boolean notFirstSubjectStatement = false;
		
		boolean bnodeClassesProduced = false; //whether [a Class] generation took place, only applies to produceTurtle=true
		
		for(String p : subject_predicate_map.keySet()){	

			if(bnodeClassesProduced && p.equals(SaplConstants.rdfType)) continue;

			List<String> subject_predicate_statements = subject_predicate_map.get(p);
			
			boolean notFirstSubjectPredicateStatement = false; 
			
			for(String statementID : subject_predicate_statements){

				final SemanticStatement statement = this.model.beliefs.get(statementID);
				
				//skip if plain RDF structure is wished for
				if(!this.includeNested && (statement.isSubjectContext() || statement.isObjectContext()))
					continue;

				//if Turtle is asked for, it cannot be done for statements linked to several Contexts
				boolean canDoTurtle = this.produceTurtle;
				boolean duplicateStatementsContainsStatementID = duplicateStatements.contains(statementID);
				if (duplicateStatementsContainsStatementID) {
					canDoTurtle = false;
					notFirstSubjectStatement = false;
					notFirstSubjectPredicateStatement = false;
				}
				
				//generate dot if this is not the first statement in the container
				boolean newLine = true;
				if (this.produceTurtle && notFirstSubjectPredicateStatement){
					result.append(", ");
					newLine = false;
				}
				else if (this.produceTurtle && notFirstSubjectStatement){
					result.append("; ");
					result.append('\n');
					this.putWhiteSpace(result, (recursiondepth+1) * SaplN3Producer.indent_size);
					newLine = false;
				}
				else if (notFirstContextStatement && !bnodeClassesProduced) {
					result.append(" .");
					if (this.indents) {
						result.append('\n');
					} else {
						result.append(' ');
					}
				}
				else if (bnodeClassesProduced || skipSubject) newLine = false;

				if (this.indents && newLine) {
					this.putWhiteSpace(result, recursiondepth * SaplN3Producer.indent_size);
				}
				
				//if the statement has been referenced earlier, it does not have to be created again:
				String localID = generatedReferableStatements.get(statementID);
				if (localID != null) {
					this.produceN3localIDReference(localID, result, prefixer);
					notFirstContextStatement = true;
					continue; //<<<<<<< if a localIDReference is created we are done for this statement
				}

				final String subject = statement.subject;
				final String predicate = statement.predicate;
				final String object = statement.object;
				//if the flag cleanContent is set, certain content should not be displayed on the 0-th recursion level:
				if (this.cleanContent && (recursiondepth == 0)) {
					//if the statement matches '* embeddedPredicate *' with the first * not a variable
					if (SaplConstants.embedded_predicates.contains(predicate) && !subject.startsWith("?")) {
						notFirstContextStatement = false; // this results in not generating a dot twice since nothing is generated.
						continue;
					}
					//matches '* sapl:hasMember *' and subject is not a variable
					if (predicate.equals(SaplConstants.hasMember) && !subject.startsWith("?")) {
						notFirstContextStatement = false; // this results in not generating a dot twice since nothing is generated.
						continue;
					}
					//matches '* sapl:memberOf *' and object is not a variable
					if (predicate.equals(SaplConstants.memberOf) && !object.startsWith("?")) {
						notFirstContextStatement = false; // this results in not generating a dot twice since nothing is generated.
						continue;
					}
					//matches '* sapl:is Optional' , '* sapl:Limit *', '* sapl:Offset *'
					if ((predicate.equals(SaplConstants.is) && object.equals(SaplConstants.Optional))
							|| predicate.equals(SaplConstants.Limit) || predicate.equals(SaplConstants.Offset)) {

						if (this.indents) {
							this.putWhiteSpace(result, recursiondepth * SaplN3Producer.indent_size);
						}
						//recursiondepth does not have to be increased because we're unwrapping a container.
						this.produceN3Recursion(this.model.contexts.get(subject), duplicateStatements, generatedReferableStatements, result,
								prefixer, recursiondepth);

						notFirstContextStatement = true;
						continue;
					}
					//matches '* sapl>or *' , generates the two containers the sapl:or is connecting as if they where on the sapl:or level.
					if (predicate.equals(SaplConstants.or)) {
						if (this.indents) {
							this.putWhiteSpace(result, recursiondepth * SaplN3Producer.indent_size);
						}
						//generate the code for the subject
						this.produceN3Recursion(this.model.contexts.get(subject), duplicateStatements, generatedReferableStatements, result,
								prefixer, recursiondepth);
						//because we are joining the two containers, an extra dot is needed.
						result.append(" .");
						if (this.indents) {
							result.append('\n');
						} else {
							result.append(' ');
						}
						//repeat for object container
						if (this.indents) {
							this.putWhiteSpace(result, recursiondepth * SaplN3Producer.indent_size);
						}
						//generate the code for the object
						this.produceN3Recursion(this.model.contexts.get(object), duplicateStatements, generatedReferableStatements, result,
								prefixer, recursiondepth);

						notFirstContextStatement = true;
						continue;
					}

				}

				//from here on we have to generate the statement.
				
				//if the statement will be referenced later, we've to generate an ID for it:
				if (duplicateStatementsContainsStatementID) {
					localID = SaplConstants.STATEMENT_LOCAL_ID_PREFIX + generatedReferableStatements.size();
					generatedReferableStatements.put(statementID, localID);
					result.append('{'); //start bracket to enclose the statement in order to be able to add the id later.
					
					canDoTurtle = false;
					notFirstSubjectStatement = false;
					notFirstSubjectPredicateStatement = false;
				}

				if(!canDoTurtle || (!notFirstSubjectStatement && !skipSubject && !bnodeClassesProduced)){
					//generate subject
					if(canDoTurtle && statement.isSubjectBlankNode() 
							&& !bnodeObjectUses.containsKey(subject)){
						
						boolean canDoTurtleBnode = true;
						
						for(List<String> l : subject_predicate_map.values()){	
							for(String stID : l){
								if(duplicateStatements.contains(stID)){
									canDoTurtleBnode = false;
									break;
								}
							}
							if(!canDoTurtleBnode) break;
						}
						
						if(canDoTurtleBnode){
							List<String> sts = subject_predicate_map.get(SaplConstants.rdfType);
							if(sts!=null && subject_predicate_map.size() > 1){
								result.append("[");
								this.produceN3Predicate(SaplConstants.rdfType, result, prefixer);
								result.append(' ');
								boolean first = true;
								for(String stID : sts){
									if(!first){
										result.append(',');
										result.append(' ');
									}
									SemanticStatement clSt = this.model.beliefs.get(stID);
									this.produceN3Object(clSt.object, clSt.objectDatatype, clSt.objectLanguage, duplicateStatements,
											generatedReferableStatements, result, prefixer, recursiondepth);
									first = false;
								}
								result.append("]");
								bnodeClassesProduced = true;
							}
							else result.append("[]");
						}
						else this.produceN3Subject(subject, duplicateStatements, generatedReferableStatements, result, prefixer, recursiondepth);
					}
					else this.produceN3Subject(subject, duplicateStatements, generatedReferableStatements, result, prefixer, recursiondepth);
					result.append(' ');
				}
				
				if(bnodeClassesProduced && p.equals(SaplConstants.rdfType)) break;
				
				if(!canDoTurtle || !notFirstSubjectPredicateStatement){
					//generate predicate
					this.produceN3Predicate(predicate, result, prefixer);
					result.append(' ');
				}
				//generate object
				if(canDoTurtle && statement.isObjectBlankNode() && bnodeObjectUses.get(object)==1){
					if(subject_map.containsKey(object)){
						//expand the blank node description here
						result.append("[ ");
						//result.append('\n');
						Map<String, List<String>> blank_node_map = subject_map.get(object);
						produceCommonSubjectStatements(blank_node_map, subject_map, bnodeObjectUses, duplicateStatements, generatedReferableStatements, result, prefixer, recursiondepth + 1, false, true);
						//result.append('\n');
						//this.putWhiteSpace(result, (recursiondepth+1) * SaplN3Producer.indent_size);
						result.append(" ]");
					}
					else result.append("[]");
				}
				else this.produceN3Object(object, statement.objectDatatype, statement.objectLanguage, duplicateStatements,
						generatedReferableStatements, result, prefixer, recursiondepth + (canDoTurtle&&notFirstSubjectStatement?1:0) );
				//if the statement will be referenced later, we've to generate an ID for it:
				if (duplicateStatementsContainsStatementID) {
					result.append('}');
					result.append(' ');
					if(!usePrefixes) result.append("<");
					result.append(prefixer.prefixResource(SaplConstants.ID));
					if(!usePrefixes) result.append(">");
					result.append(' ');
					//append the previously generated ID
					result.append(localID);
				}
				else{
					notFirstSubjectStatement = true;
					notFirstSubjectPredicateStatement = true;
				}

				notFirstContextStatement = true;

			}
			if(bnodeClassesProduced && p.equals(SaplConstants.rdfType)) continue;
			
		}
		return notFirstContextStatement;
	}

	/**
	 * Appends the requested amount of whitespace to the StringBuilder
	 * 
	 * @param result
	 * @param length
	 */
	private void putWhiteSpace(StringBuilder result, int length) {
		result.append(WhiteSpaceString.getWhiteSpaceString(length));
	}

	/**
	 * Produces a reference to another statement (which is previousely generated in the document)
	 * 
	 * @param localID
	 *            The ID local to the document
	 * @param result
	 *            The StringBuilder to which the code must be appended.
	 * @param prefixer
	 *            The Prefixer to use to prefix possible resources.
	 */
	private void produceN3localIDReference(final String localID, final StringBuilder result, ConditionalResourcePrefixer prefixer) {
		result.append(localID);
		result.append(' ');
		if(!usePrefixes) result.append("<");
		result.append(prefixer.prefixResource(SaplConstants.is));
		if(!usePrefixes) result.append(">");
		result.append(' ');
		if(!usePrefixes) result.append("<");
		result.append(prefixer.prefixResource(SaplConstants.TRUE));
		if(!usePrefixes) result.append(">");

	}

	/**
	 * Produces the code of a subject
	 * 
	 * @param subject
	 *            The subject to produce
	 * @param duplicateStatements
	 *            statements which appear more as once in the document.
	 * @param generatedReferableStatements
	 *            statements which are already generated previously and can be referred to.
	 * @param result
	 *            the StringBuilder to which the result must be appended.
	 * @param prefixer
	 *            the prefixer to use when prefixing a resource.
	 * @param recursiondepth
	 *            the current recursion depth.
	 */
	private void produceN3Subject(String subject, final Set<String> duplicateStatements,
			final Map<String, String> generatedReferableStatements,//map, mapping statementIDs on local IDs, the local ID will be null if no ID is generated. 
			final StringBuilder result, ConditionalResourcePrefixer prefixer, final int recursiondepth) {
		if(subject.startsWith(SaplConstants.BLANK_NODE_PREFIX)){
			result.append(subject);
		}
		else if (SemanticStatement.isURI(subject)) {
			//it is a URI
			String prefixed = prefixer.prefixResource(subject);
			boolean bracketsNeeded = subject.equals(prefixed);
			if(bracketsNeeded) result.append("<"); 
			result.append(prefixed);
			if(bracketsNeeded) result.append(">"); 
		} else if (subject.startsWith(SaplConstants.CONTEXT_PREFIX)) {
			//it is a container
			result.append('{');
			//here is a slight difference with the original implementation : when indentation is on and rules are skipped and there are no rules,
			//that empty container will show as '{\n neededwhitespace }\n' while the original implementation just showed {} 
			if (this.indents) {
				result.append('\n');
			}
			//end of the code which is different. original : ((options.indents && (str.length() > 0)) ? "\n" : "") where str was the produced content.
			if (!this.includeRules && (subject.equals(SaplConstants.RULES_CONTEXT) || model.metaruleContexts.containsValue(subject))) {
				//skip the rules.
				if (this.model.contexts.get(subject).members.size() > 0) {
					this.putWhiteSpace(result, (recursiondepth+1) * SaplN3Producer.indent_size);
					result.append("<Contents> <are> <skipped>");
				} else {
					//nothing to do
				}
			} else {
				//do recursion
				ContextContainer container = this.model.contexts.get(subject);
				this.produceN3Recursion(container, duplicateStatements, generatedReferableStatements, result, prefixer, recursiondepth + 1);
			}
			//here is a slight difference with the original implementation : when indentation is on and rules are skipped and there are no rules,
			//that empty container will show as '{\n needed whitespace }\n' while the original implementation just showed {}
			if (this.indents) {
				result.append('\n');
				this.putWhiteSpace(result, recursiondepth * SaplN3Producer.indent_size);
			}
			//end of the code which is different. original :((options.indents && (str.length() > 0)) ? "\n"	+ WhiteSpaceString.getWhiteSpaceString((recursionLevel) * SaplN3.indent_size) : "") where str was the produced content. 
			result.append("}");
		} else {
			//it is a literal or variable
			boolean isVar = subject.equals(SaplConstants.ANYTHING) || (subject.startsWith(SaplConstants.VARIABLE_PREFIX)  && !subject.contains(" "));
			if(!isVar) result.append("\"");
			result.append(StringEscape.escape(subject));
			if(!isVar) result.append("\"");
		}

	}

	/**
	 * Produces the code of a predicate
	 * 
	 * @param predicate
	 *            the predicate to produce code for
	 * @param result
	 *            The StringBuilder to which the code shoud be appended
	 * @param prefixer
	 *            the prefixer which must be used to prefix resources.
	 */
	private void produceN3Predicate(final String predicate, final StringBuilder result, ConditionalResourcePrefixer prefixer) {
		String match = SaplN3Producer.replace.get(predicate);
		if (match != null) {
			result.append(match);
		}
		else if(SemanticStatement.isURI(predicate)) {
			//it is a URI
			String prefixed = prefixer.prefixResource(predicate);
			boolean bracketsNeeded = predicate.equals(prefixed);
			if(bracketsNeeded) result.append("<"); 
			result.append(prefixed);
			if(bracketsNeeded) result.append(">"); 

		} else {
			boolean isVar = predicate.equals(SaplConstants.ANYTHING) || (predicate.startsWith(SaplConstants.VARIABLE_PREFIX)  && !predicate.contains(" "));
			if(!isVar) result.append("\"");
			result.append(StringEscape.escape(predicate));
			if(!isVar) result.append("\"");
		}
	}

	/**
	 * Produces the code of a object
	 * 
	 * @param object
	 *            The subject to produce
	 * @param duplicateStatements
	 *            statements which appear more as once in the document.
	 * @param generatedReferableStatements
	 *            statements which are already generated previously and can be referred to.
	 * @param result
	 *            the StringBuilder to which the result must be appended.
	 * @param prefixer
	 *            the prefixer to use when prefixing a resource.
	 * @param recursiondepth
	 *            the current recursion depth.
	 * 
	 * @param objectDatatype
	 * @see {@link SemanticStatement#objectDatatype}
	 * @param objectLanguage
	 * @see {@link SemanticStatement#objectLanguage}
	 */
	private void produceN3Object(final String object, final String objectDatatype, final String objectLanguage,
			final Set<String> duplicateStatements, final Map<String, String> generatedReferableStatements,//map, mapping statementIDs on local IDs, the local ID will be null if no ID is generated. 
			final StringBuilder result, ConditionalResourcePrefixer prefixer, final int recursiondepth) {

		if(object.startsWith(SaplConstants.BLANK_NODE_PREFIX)){
			result.append(object); 
		}
		else if (object.startsWith(SaplConstants.CONTEXT_PREFIX) && (objectDatatype == null)) {
			//it is a container
			result.append('{');
			//original : + ((options.indents && (str.length() > 0)) ? "\n" : "")
			if (this.indents) {
				result.append('\n');
			}
			//recursive step
			ContextContainer container = this.model.contexts.get(object);
			this.produceN3Recursion(container, duplicateStatements, generatedReferableStatements, result, prefixer, recursiondepth + 1);
			//original		+ ((options.indents && (str.length() > 0)) ? "\n"+ WhiteSpaceString.getWhiteSpaceString((recursionLevel) * SaplN3.indent_size) : "") + 
			if (this.indents) {
				result.append('\n');
				this.putWhiteSpace(result, recursiondepth * SaplN3Producer.indent_size);
			}
			result.append("}");
		} else if (objectDatatype != null || objectLanguage != null || !SemanticStatement.isURI(object)) {
			//it is a literal, depending on the type, do a different thing:
			if (objectDatatype != null) {
				String prefixedDatatype = prefixer.prefixResource(objectDatatype);
				boolean bracketsNeeded = !objectDatatype.startsWith(SaplConstants.BLANK_NODE_PREFIX) && prefixedDatatype.equals(objectDatatype);
				result.append("\"");
				result.append(StringEscape.escape(object));
				result.append("\"^^");
				if(bracketsNeeded) result.append("<"); 
				result.append(prefixedDatatype);
				if(bracketsNeeded) result.append(">"); 
			} else if (objectLanguage != null) {
				result.append("\"");
				result.append(StringEscape.escape(object));
				result.append("\"@");
				result.append(objectLanguage);
			} else {
				boolean isVar = object.equals(SaplConstants.ANYTHING) || ( object.startsWith(SaplConstants.VARIABLE_PREFIX) && !object.contains(" ") && object.lastIndexOf(SaplConstants.VARIABLE_PREFIX)==0);
				if(!isVar) result.append("\"");
				result.append(StringEscape.escape(object));
				if(!isVar) result.append("\"");
			}
		} else {
			//it is a URI
			String prefixed = prefixer.prefixResource(object);
			boolean bracketsNeeded = object.equals(prefixed);
			if(bracketsNeeded) result.append("<"); 
			result.append(prefixed);
			if(bracketsNeeded) result.append(">"); 
		}

	}

	/**
	 * Returns a set of all statementIDs which are used more as once in the ContextContainer
	 * 
	 * @param c
	 *            the {@link ContextContainer}
	 * @return the set of statement ID's
	 */
	private Set<String> searchDuplicates(ContextContainer c) {
		Set<String> duplicates = new HashSet<String>();
		this.searchDuplicates(c, new HashSet<String>(), duplicates);
		return duplicates;
	}

	/**
	 * Performs a pre-order traversal of the tree of ContextContainers to find duplicates.
	 * 
	 * @param c
	 *            the {@link ContextContainer}
	 * @param referencedEarlier
	 *            Set of earlier referenced {@link SemanticStatement}
	 * @param duplicates
	 *            Set of duplicate {@link SemanticStatement}
	 */
	private void searchDuplicates(ContextContainer c, Set<String> referencedEarlier, Set<String> duplicates) {

		for (String statementID : c.members) {
			SemanticStatement statement = this.model.beliefs.get(statementID);
			//we only consider this statement as a possible candidate if it has more as one reference
			//In a later implementation phase when referenced is not an attribute of statement anymore, this check can be safely removed.
			if (statement.referenced > 1) {
				if (referencedEarlier.contains(statementID)) {
					duplicates.add(statementID);
				} else {
					referencedEarlier.add(statementID);
				}
			}
			//if subject or object of the statement are containers, continue recursively.
			String subject = statement.subject;
			String object = statement.object;
			if (subject.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				ContextContainer subjectContainer = this.model.contexts.get(subject);
				this.searchDuplicates(subjectContainer, referencedEarlier, duplicates);
			}
			if (object.startsWith(SaplConstants.CONTEXT_PREFIX)) {
				ContextContainer objectContainer = this.model.contexts.get(object);
				this.searchDuplicates(objectContainer, referencedEarlier, duplicates);
			}
		}
	}
}
