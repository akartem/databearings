package sapl.core.sapln3;

import java.util.List;

/**
 * Intermediary representation of a compiled S-APL document, returned by S-APL Parser
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.1 (10.10.2014)
 * 
 * @since 4.1
 * 
 * Copyright (C) 2013-2014, VTT
 * 
 * */

public class SaplCode {
	String textID;
	
	List<Token> tokens;
	
	List<String> code;
	
	SaplCode(String textID, List<Token> tokens, List<String> code){
		this.textID = textID;
		this.code = code;
		this.tokens = tokens;
	}
	
	public String getTextID(){
		return textID;
	}

	public List<String> getCode(){
		return code;
	}

	public List<Token> getTokens(){
		return tokens;
	}
	
}
