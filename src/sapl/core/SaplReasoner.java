package sapl.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import sapl.core.rab.BehaviorStartParameters;
import sapl.core.rab.IllegalParameterConfigurationException;
import sapl.core.rab.Resource;
import sapl.core.sapln3.SaplCode;
import sapl.core.sapln3.SaplN3Parser;
import sapl.core.sapln3.SaplN3ProducerOptions;
import sapl.shared.eii.HttpOntonut;
import sapl.shared.eii.Ontonut;
import sapl.shared.eii.UniversalAdapterBehavior;
import sapl.util.LoggerHelper;
import sapl.util.URITools;

/** 
 * Main class of the S-APL framework.
 * Implements the deliberation cycle of an agent, making it to act
 * according to a S-APL program
 * 
 * @author Artem Katasonov (VTT + Elisa since July 2018)
 * 
 * @version 5.0 (04.07.2019)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class SaplReasoner extends ReusableAtomicBehavior{

	public static boolean CREATE_MENTAL_CONTEXTS = false;
	
	public static boolean SLEEP_PRINT = true;
	public static boolean PERFORMANCE_CYCLE_PRINT = true;
	public static boolean PERFORMANCE_CYCLE_END_PRINT = false;

	public static boolean LOG = true;
	public static int LOG_SIZE = 1000000;
	public static int LOG_NUMBER = 10;

	/**Name of the reasoning entity*/
	protected String name;
	
	/**S-APL model*/
	protected SaplModel saplModel;

	/**Engine for self-updating the S-APL model*/
	protected SaplUpdateEngine saplUpdateEngine;

	/**Engine for executing external Java action from current state of the S-APL model*/
	protected SaplActionEngine saplActionEngine;	

	/**Number of current reasoning iteration*/
	int lifeCycle = 0;

	long start; 
	long time; 

	/**List of RABs to execute as result of last iteration*/
	public List<ReusableAtomicBehavior> toExecute;
	
	/**When using Sapl.executeWithResult(), PrintBehavior redirects all normal output here*/
	StringBuilder result = null;
	
	public static LoggerHelper logOut = null;
	public static LoggerHelper logErr = null;
	
	
	/**Constructor */
	public SaplReasoner(String name, boolean withResult){
		
		start = System.currentTimeMillis(); 
		
		this.name = name;
		
		this.finished = false; //cyclic

		if(LOG){
			LoggerHelper.init(name, LOG_SIZE, LOG_NUMBER);
			
			if(logOut==null){
				logOut = new LoggerHelper(System.out, Level.INFO, false);
				System.setOut(logOut);
			}
			if(logErr==null){
				logErr = new LoggerHelper(System.err, Level.SEVERE, true);
				System.setErr(logErr);
			}
		}
		
		if(withResult) this.result = new StringBuilder();
	}

	public SaplReasoner(String name){
		this(name, false);
	}

	
	/**Initializes the reasoning behavior*/
	@Override
	protected void initializeBehavior(BehaviorStartParameters parameters)
			throws IllegalParameterConfigurationException {

		time = start; 

		this.saplModel = new SaplModel(true, CREATE_MENTAL_CONTEXTS);
		
		PRINT_EXCEPTION_FULL_INFO = false;
		Collection<String> scripts = parameters.getParameterValues(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "script"));
		if(scripts!=null){
			for(String script : scripts){
				SaplCode code;
				try {
					code = SaplN3Parser.compileFile(script);
					
					if (code != null) {
						SaplN3Parser.load(code.getCode(), this.saplModel, SaplConstants.GENERAL_CONTEXT, null, script, true);
					}		
				} catch (Throwable e) {
					printException(e, true);
				}
			}
		}
		PRINT_EXCEPTION_FULL_INFO = true;
		
		String input = parameters.getOptionalStringParameter(Resource.createUncheckedResource(URITools.createUncheckedURI(SaplConstants.PARAM_NS), "input"));
		if(input!=null){
			SemanticStatement st = new SemanticStatement(SaplConstants.Input, SaplConstants.is, input);
			this.saplModel.putBelief(SaplConstants.INPUT_BELIEF, st);
			this.saplModel.putLink(SaplConstants.GENERAL_CONTEXT, SaplConstants.INPUT_BELIEF);
			ContextContainer c = this.saplModel.contexts.get(SaplConstants.GENERAL_CONTEXT);
			c.addToIndex(st.object, SaplConstants.INPUT_BELIEF,c.index_ob);
		}
		
		this.saplUpdateEngine = new SaplUpdateEngine(true);
		this.saplActionEngine = new SaplActionEngine(this.saplModel, this);

		this.myEngine = this.saplActionEngine;

		processSettings();
	}
	
	//process settings
	void processSettings(){
		SaplQueryResultSet rs = this.hasBeliefsN3("?x <"+SaplConstants.TECH_NS+"setup> { ?s ?p ?o }");
		if(rs!=null){
			for(int i=0; i<rs.getNumberOfSolutions(); i++){
				Map<String,String> sol = rs.getSolution(i);
				String s = sol.get("s");
				String p = sol.get("p");
				String o = sol.get("o");
				if(s.equals(SaplConstants.TECH_NS+"queryEngine")){
					if(p.equals(SaplConstants.TECH_NS+"logDebugInfo")){ 
						if(o.equals("true")) SaplQueryEngine.DEBUG_PRINT = true;
						else if(o.equals("false")) SaplQueryEngine.DEBUG_PRINT = false;
					}
				}
				else if(s.equals(SaplConstants.TECH_NS+"reasoner")){
					if(p.equals(SaplConstants.TECH_NS+"logCycleInfo")){ 
						if(o.equals("true")) SaplReasoner.PERFORMANCE_CYCLE_PRINT = true;
						else if(o.equals("false")) SaplReasoner.PERFORMANCE_CYCLE_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logSleepInfo")){ 
						if(o.equals("true")) SaplReasoner.SLEEP_PRINT = true;
						else if(o.equals("false")) SaplReasoner.SLEEP_PRINT = false;
					}
				}
				else if(s.equals(SaplConstants.TECH_NS+"agent")){
					if(p.equals(SaplConstants.TECH_NS+"logDatabaseContent")){ 
						if(o.equals("true")) SaplAgent.DEBUG_PRINT = true;
						else if(o.equals("false")) SaplAgent.DEBUG_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logSleepInfo")){ 
						if(o.equals("true")) SaplAgent.SLEEP_PRINT = true;
						else if(o.equals("false")) SaplAgent.SLEEP_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logStopInfo")){ 
						if(o.equals("true")) SaplAgent.STOP_PRINT = true;
						else if(o.equals("false")) SaplAgent.STOP_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logMemoryInfo")){ 
						if(o.equals("true")) SaplAgent.MEMORY_PRINT = true;
						else if(o.equals("false")) SaplAgent.MEMORY_PRINT = false;
					}
				}
				else if(s.equals(SaplConstants.TECH_NS+"eii")){
					if(p.equals(SaplConstants.TECH_NS+"logDebugInfo")){ 
						if(o.equals("true")){
							UniversalAdapterBehavior.DEBUG_PRINT = true;
							Ontonut.DEBUG_PRINT = true;
						}
						else if(o.equals("false")){
							UniversalAdapterBehavior.DEBUG_PRINT = false;
							Ontonut.DEBUG_PRINT = false;
						}
					}
					else if(p.equals(SaplConstants.TECH_NS+"logPerformanceInfo")){ 
						if(o.equals("true")) UniversalAdapterBehavior.PERFORMANCE_PRINT = true;
						else if(o.equals("false")) UniversalAdapterBehavior.PERFORMANCE_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logDebugSolutions")){ 
						if(o.equals("true")) UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT = true;
						else if(o.equals("false")) UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logInteractionInfo")){ 
						if(o.equals("true")) UniversalAdapterBehavior.INTERACTION_PRINT = true;
						else if(o.equals("false")) UniversalAdapterBehavior.INTERACTION_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logInteractionDetails")){ 
						if(o.equals("true")) HttpOntonut.INTERACTION_DETAILS_PRINT = true;
						else if(o.equals("false")) HttpOntonut.INTERACTION_DETAILS_PRINT = false;
					}
					else if(p.equals(SaplConstants.TECH_NS+"logUpdateResponseDetails")){ 
						if(o.equals("true")) HttpOntonut.UPDATE_RESPONSE_PRINT = true;
						else if(o.equals("false")) HttpOntonut.UPDATE_RESPONSE_PRINT = false;
					}
				}
				else if(s.equals(SaplConstants.TECH_NS+"logger")){
					if(p.equals(SaplConstants.TECH_NS+"save")){ 
						if(o.equals("err")){
							logErr.setDoSave(true); logOut.setDoSave(false);
						}
						else if(o.equals("all")){
							logOut.setDoSave(true); logErr.setDoSave(false);
						}
					}
					
				}
			}
			
			
		}
	}


	/**Performs one iteration of the reasoning behavior*/
	@Override
	public void doAction(){
		
		//to overrule incorrect scheduling mechanisms such as JADE's one
		//if there are RABs in the queue that have not been yet executed, do not start next cycle yet			
		for (ReusableAtomicBehavior behavior : saplActionEngine.runningRABs) {
			if (!behavior.hasBeenExecuted()) {
				return;
			}
		}

		final long now = System.currentTimeMillis();
		lifeCycle++;
		if(PERFORMANCE_CYCLE_PRINT)
			print("--Cycle: "+lifeCycle+ " ["+ (now - start) + " ("+ (now - time) +")"+"]");
		
		time = now;
		
		boolean changed;
		List<Long> blockingTimeMoments;

		synchronized(saplModel.lock){			
			blockingTimeMoments = new ArrayList<Long>();

			changed = saplUpdateEngine.update(saplModel, blockingTimeMoments);
			//System.out.println(blockingTimeMoments);///////////////

			this.toExecute = saplActionEngine.getExternalActions();

			final long now2 = System.currentTimeMillis();
			if(PERFORMANCE_CYCLE_PRINT && PERFORMANCE_CYCLE_END_PRINT)
				print("--Cycle end ["+ (now2 - now)+"]");
			

			if(!changed && toExecute == null){
				saplModel.collectGarbage();
				ExpressionEvaluator.clearSavedCode();

				if(SLEEP_PRINT){
					long timeofsleep = System.currentTimeMillis();
					print("Reasoner goes to sleep.. ["+ (timeofsleep - start) + " ("+ (timeofsleep - time) +")"+"]");
				}

				if (blockingTimeMoments.size() == 0) {
					this.block();
				} else {
					Collections.sort(blockingTimeMoments);
					for(Long moment : blockingTimeMoments){
						long time = moment.longValue();
						if (time > now) {
							this.block(time - now);
							return;
						}
					}
					this.block();
				}			
			}
		}
	}
	
	void printModel(boolean includeRules, boolean printRaw){
		if(printRaw){
			System.out.println(this.saplModel.beliefs);
			System.out.println(this.saplModel.contexts+"\n");
		}
		SaplN3ProducerOptions options = new SaplN3ProducerOptions(true, includeRules, true, true, true, false);
		System.out.println(this.produceN3(SaplConstants.G, options));		
	}

}
