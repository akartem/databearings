package sapl;

import java.util.ArrayList;
import java.util.List;

import sapl.core.SaplAgent;
import sapl.core.SaplReasoner;
import sapl.shared.eii.ClassifierBehavior;
import sapl.shared.eii.Ontonut;
import sapl.shared.eii.UniversalAdapterBehavior;

/**
 * Command-line executable for an S-APL Agent
 * 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.6 (31.05.2018)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class Sapl {
	
	public static void main(String[] args) {

		if(args.length<1){
			System.out.println("USAGE: \n Sapl [-name <name>] [-clean] [-printdb] [-eiidebug] [-eiidebugfull] <script1.sapl> [<script2.sapl> ...]");
		}
		else {
			//System.setProperty ("jsse.enableSNIExtension", "false");

			List<String> scripts = new ArrayList<String>();
			String name = null;
			boolean inName = false;
			for(String arg : args){
				if(inName){
					name = arg;
					inName = false;
				}
				else {
					if(arg.startsWith("-")){
						if(arg.equals("-name")){
							inName = true;
						}
						if(arg.equals("-noprint")){
							SaplReasoner.SLEEP_PRINT = false;
							SaplReasoner.PERFORMANCE_CYCLE_PRINT = false;
							SaplAgent.MEMORY_PRINT = false;
							SaplAgent.SLEEP_PRINT = false;
							SaplAgent.STOP_PRINT = false;
						}
						if(arg.equals("-printdb")){
							SaplAgent.DEBUG_PRINT = true;
						}
						if(arg.equals("-eiidebug")){
							UniversalAdapterBehavior.DEBUG_PRINT = true;
							ClassifierBehavior.DEBUG_PRINT = true;
							Ontonut.DEBUG_PRINT = true;
						}
						if(arg.equals("-eiidebugfull")){
							UniversalAdapterBehavior.DEBUG_PRINT = true;
							Ontonut.DEBUG_PRINT = true;
							UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT = true;
						}
					}
					else scripts.add(arg);
				}
			}
			
			if(name==null) name = String.valueOf(System.currentTimeMillis());
			SaplAgent agent = new SaplAgent(name, scripts.toArray(new String[scripts.size()]));
			agent.run();
		}
	}
	
	public static void execute(String name, String[] scripts){
		if(name==null) name = String.valueOf(System.currentTimeMillis());
		SaplAgent agent = new SaplAgent(name, scripts);
		agent.run();
	}

	public static String executeWithResult(String name, String[] scripts, String input){
		if(name==null) name = String.valueOf(System.currentTimeMillis());
		SaplAgent agent = new SaplAgent(name, scripts, true, input);
		agent.run();
		return agent.getResult();
	}
	
}
