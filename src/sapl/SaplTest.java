package sapl;

import java.io.IOException;

import fi.vtt.usefil.questionnaire.MultiUserChatSession;
import sapl.core.SaplAgent;
import sapl.shared.eii.ClassifierBehavior;
import sapl.shared.eii.Ontonut;
import sapl.shared.eii.QueryDecomposer;
import sapl.shared.eii.UniversalAdapterBehavior;

/**
 * Executor for S-APL script /apps/test/test.sapl
 *   
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.3 (05.10.2015)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2015, VTT
 * 
 * */ 

public class SaplTest {

	static String[] scripts = {
		"apps/test/test.sapl"
	};

	public static void main(String[] args) throws IOException, InterruptedException {
		
		//System.setProperty("jsse.enableSNIExtension", "false");
		
		SaplAgent.DEBUG_PRINT = false; // print DB 
		
		SaplAgent.MEMORY_PRINT = false;
		
		//SaplQueryEngine.DEBUG_PRINT = true;
		
		UniversalAdapterBehavior.PERFORMANCE_PRINT = true;
		UniversalAdapterBehavior.INTERACTION_PRINT = true;
		
		UniversalAdapterBehavior.DEBUG_PRINT = false;
		
		UniversalAdapterBehavior.DEBUG_SOLUTIONS_PRINT = false;

		Ontonut.DEBUG_PRINT = false;
		
		QueryDecomposer.DEBUG_PRINT = false;
		
		ClassifierBehavior.DEBUG_PRINT = true;
		
		MultiUserChatSession.DEBUG_PRINT = true;
		
		//run
		Sapl.execute("test", scripts);
		
//		String result = Sapl.executeWithResult("test", scripts, "personid=45");
//		System.out.println(result);
		
		
		/*SaplModel saplModel = new SaplModel(false, false);

		List<String> code;
		try {
			code = SaplN3Parser.compileFile("test.sapl");
			if (code != null) {
				SaplN3Parser.load(code, saplModel, SaplConstants.GENERAL_CONTEXT, "Test", true);
			}		
		} catch (IOException e) {
			e.printStackTrace();
		}

		SaplQueryResultSet resultSet =  new SaplQueryResultSet();
		SaplQueryEngine.evaluateQueryN3("@prefix platx: <http://www.finnpark.fi/platfomx#>. ?x a platx:Event; platx:plate ?plate", SaplConstants.GENERAL_CONTEXT, saplModel, resultSet);
		System.out.println(resultSet);

		System.out.println(saplModel.beliefs);
		System.out.println(saplModel.contexts+"\n");		
		SaplN3Producer producer = new SaplN3Producer(saplModel, saplModel.prefixer, SaplN3ProducerOptions.defaultOptions);
		String result = producer.produceN3(saplModel.contexts.get(SaplConstants.GENERAL_CONTEXT));
		System.out.println(result);		
		 */
	}

}
