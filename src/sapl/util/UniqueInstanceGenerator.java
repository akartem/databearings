package sapl.util;

import java.net.URI;

import sapl.core.rab.Resource;

/**
 * This class implements an ID generator, every time getNewID is called, a new uninque ID is generated. 
 * The id's are in the "http://www.ubiware.jyu.fi/id#" namespace
 * 
 * @author Michael Cochez (University of Jyväskylä)
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.0 (25.10.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2009 Michael Cochez
 * 
 * */

public class UniqueInstanceGenerator extends Counter {

	/**
	 * The prefix for the IDs.
	 */
	private final String prefix;
	/**
	 * The longest size an ID can have. Used to estimate the length of generated ID's.
	 */
	private final int maxIDsize;

	/**
	 * The maximum number of digits a string representation from a long in base 10 can have 9223372036854775807 is the longest long.
	 */
	private static final int MAX_LONG_STRING_LENGTH = 19;

	/**
	 * Create an IDGenerator which uses the given prefix.
	 * 
	 * @param prefixForID
	 *            the prefix for the IDs
	 */
	public UniqueInstanceGenerator(final String prefixForID, final URI namespace) {
		super();
		final long time = System.currentTimeMillis();
		this.prefix = namespace.toString() + prefixForID + '_' + Long.toString(time) + '_';
		this.maxIDsize = this.prefix.length() + UniqueInstanceGenerator.MAX_LONG_STRING_LENGTH + 1; //prefix + counter + '>'
	}

	public Resource getNewResource() {
		return Resource.createUncheckedResource(this.getNewID());
	}

	/**
	 * Returns a new primaryKey ID.
	 * 
	 * @return the ID
	 */
	public String getNewID() {
		final StringBuilder stringbuilder = new StringBuilder(this.maxIDsize);
		stringbuilder.append(this.prefix);
		//increase of the counter in synchronized block to prevent different threads to modify the counter simultaneously.
		final long requestNr = super.getNextNumber();
		stringbuilder.append(Long.toString(requestNr));
		return stringbuilder.toString(); //this.prefix +  this.requestNr + '>';
	}
}
