package sapl.util;

/**
 * A synchronized counter, generating a new long on every request.
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 * 
 * Copyright (C) 2009, Michael Cochez
 *  
 * */

public class Counter {
	/**
	 * How many ID's have been generated before?
	 */
	private long requestNr = 0;
	/**
	 * To make sure only one increases the counter.
	 */
	private final Object requestNrLock = new Object();

	/**
	 * Generates a new primaryKey number. The reason this method is protected is to ensure that no one,
	 * using the derived generator uses this method straight,
	 * since this would endanger the guarantees of the generators.
	 * 
	 * @return the next number in the sequence
	 */
	protected final long getNextNumber() {
		synchronized (this.requestNrLock) {
			final long result = this.requestNr;
			this.requestNr++;
			return result;
		}
	}

	/**Zeros the counter*/
	protected void zero(){
		synchronized (this.requestNrLock) {
			this.requestNr = 0;
		}		
	}
}
