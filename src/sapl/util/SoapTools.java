package sapl.util;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * Utility for forming SOAP messages
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018) 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.3 (02.09.2015)
 * 
 * Copyright (C) 2013-2015, VTT
 * Copyright (C) 2007-2009 Artem Katasonov, Michael Cochez
 * 
 * */

public class SoapTools {
	
	public static int DEFAULT_READ_TIMEOUT = 30; //seconds
	public static int CONNECTION_TIMEOUT = 10; //seconds

	final static String ADDR_NS = "http://www.w3.org/2005/08/addressing";

	final static String WSSE_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	final static String WSSE_UTILITY_NS ="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";
	final static String WSSE_USER_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#";
	final static String WSSE_SOAP_NS = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#";


	public static SOAPMessage createSOAP(String soapVersion, String soapService, String soapAction, String soapRequest, String username, String password, boolean digest) 
			throws Exception{
		MessageFactory messageFactory;
		if(soapVersion.contains("1.1")) messageFactory = MessageFactory.newInstance();
		else messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);

		SOAPMessage message = messageFactory.createMessage();

		MimeHeaders mimeHeaders = message.getMimeHeaders();
		mimeHeaders.addHeader("SOAPAction", soapAction);

		SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();

		SOAPHeader header = envelope.getHeader();

		if(soapService.startsWith("https")){
			envelope.addNamespaceDeclaration("a", ADDR_NS);			
			header.addChildElement("To", "a").setTextContent(soapService);
			header.addChildElement("Action", "a").setTextContent(soapAction);
		}		

		if(username!=null){
			envelope.addNamespaceDeclaration("wsse", WSSE_NS);
			envelope.addNamespaceDeclaration("wsu", WSSE_UTILITY_NS);

			SOAPElement security = header.addChildElement("Security", "wsse");
			//security.setAttribute("SOAP-ENV:mustUnderstand", "1");

			SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
			//usernameToken.setAttribute("wsu:Id", "UsernameToken-38");

			usernameToken.addChildElement("Username", "wsse").setTextContent(username);
			SOAPElement passwordElement = usernameToken.addChildElement("Password", "wsse");

			if(!digest){
				passwordElement.setAttribute("Type", WSSE_SOAP_NS+"PasswordText");
				passwordElement.setTextContent(password);			
			}
			else{
				passwordElement.setAttribute("Type", WSSE_USER_NS+"PasswordDigest");

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.UK);
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
				GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
				String timestamp = sdf.format(calendar.getTime());

				byte[] nonce = new byte[16];
				Random rand;
				rand = SecureRandom.getInstance("SHA1PRNG");
				rand.nextBytes(nonce);
				byte[] encodedNonce = Base64.encodeBase64(nonce);
				String encodedNonceString = new String(encodedNonce, "UTF-8");

				SOAPElement nonceElement = usernameToken.addChildElement("Nonce", "wsse");
				nonceElement.setAttribute("EncodingType", WSSE_SOAP_NS+"Base64Binary");
				nonceElement.setTextContent(encodedNonceString);

				usernameToken.addChildElement("Created", "wsu").setTextContent(timestamp);

				MessageDigest messageDigester = MessageDigest.getInstance("SHA-1");
				// SHA-1 ( nonce + created + password )
				messageDigester.reset();
				messageDigester.update(nonce);
				messageDigester.update(timestamp.getBytes("UTF-8"));
				messageDigester.update(password.getBytes("UTF-8"));
				String passwordDigest = new String (Base64.encodeBase64(messageDigester.digest()), "UTF-8");            	

				passwordElement.setTextContent(passwordDigest);

			}

		}

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new InputSource(new StringReader(soapRequest)));

		SOAPBody body = envelope.getBody();

		SoapTools.copyXMLtoSOAP(doc, body, new Vector<String>());

		return message;
	}

	private static void copyXMLtoSOAP(Node xml, SOAPElement soap, Vector<String> definedPrefixes) throws Exception{

		NodeList children = xml.getChildNodes();

		if(children.getLength()==1 && children.item(0).getNodeType() == Node.TEXT_NODE) soap.setTextContent(children.item(0).getTextContent().trim());

		for (int i = 0; i < children.getLength(); i++){
			Node item = children.item(i);
			if(item.getNodeType() == Node.ELEMENT_NODE){

				String prefix = item.getPrefix();
				String ns = item.getNamespaceURI();
				if(ns!=null && !definedPrefixes.contains(prefix)){
					soap.addNamespaceDeclaration(prefix, ns);
					definedPrefixes.add(prefix);
				}

				SOAPElement newSoap = null;
				if(item.getPrefix()==null)
					newSoap = soap.addChildElement(item.getLocalName());
				else newSoap = soap.addChildElement(item.getLocalName(), item.getPrefix());
				copyXMLtoSOAP(item, newSoap, definedPrefixes);
			}
		}
	}

	public static SOAPMessage sendSOAP(String soapService, SOAPMessage request, int timeout) throws MalformedURLException, UnsupportedOperationException, SOAPException{

		//		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
		//		try {
		//			request.writeTo(out);
		//			String result = new String( out.toByteArray(), "UTF-8");
		//			System.out.println(result);		
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}

		final int readTimeout = timeout*1000;
		
		URL client = new URL( new URL(soapService), "", 
				new URLStreamHandler() {
			@Override
			protected URLConnection openConnection(URL url) throws IOException {
				URL target = new URL(url.toString());
				URLConnection connection = target.openConnection();
				// Connection settings
				connection.setConnectTimeout(CONNECTION_TIMEOUT*1000); 
				connection.setReadTimeout(readTimeout);
				return(connection);
			}
		}); 
		
		SOAPConnection connection = null;
		try{
			SOAPConnectionFactory connectionFactory = SOAPConnectionFactory.newInstance();
			connection = connectionFactory.createConnection();
			SOAPMessage response = connection.call(request, client);
	
			//		ByteArrayOutputStream out2 = new ByteArrayOutputStream(); 
			//		try {
			//			response.writeTo(out2);
			//			String result = new String( out2.toByteArray(), "UTF-8");
			//			System.out.println(result);		
			//		} catch (IOException e) {
			//			e.printStackTrace();
			//		}
			return response;
		}
		finally{
			if(connection!=null) connection.close();
		}

	}
}
