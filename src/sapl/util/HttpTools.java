package sapl.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.URLStreamHandler;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;

/**
 * Utility for doing HTTP requests
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018 + Elisa since July 2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 5.2 (08.04.2020)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */ 

public class HttpTools {
	
	public static int DEFAULT_READ_TIMEOUT = 30; //seconds
	public static int DEFAULT_CONNECTION_TIMEOUT = 10; //seconds
	
	public final static String AUTH_TYPE_BASIC = "BASIC"; 
	public final static String AUTH_TYPE_BEARER = "BEARER"; 
	public final static String AUTH_TYPE_OAUTH = "OAUTH"; 
	public final static String AUTH_TYPE_FORM = "FORM"; 

	static CookieManager cookieManager = new CookieManager();

	public static String fetch(URL aURL, HttpOptions options) throws Exception {
		return fetch(aURL, options, null);
	}
	
	public static String fetch(URL aURL, HttpOptions options, String writeToFile) throws Exception {
		
		if(options.authMethod!=null){
			if(options.authMethod.equals(AUTH_TYPE_FORM)){
				return fetchWithFormAuth(aURL, options);
			}
			else if(options.authMethod.equals(AUTH_TYPE_OAUTH)){
				return fetchWithOAuth(aURL, options);
			}
		}
		
		final String fNEWLINE = System.getProperty("line.separator");
		
		if(options.encoding==null) options.encoding="UTF-8";

		final int readTimeout = options.timeout*1000;
		final int connectionTimeout = options.connection_timeout*1000;
		
		URL client = new URL( aURL, "", 
				new URLStreamHandler() {
			@Override
			protected URLConnection openConnection(URL url) throws IOException {
				URL target = new URL(url.toString());
				URLConnection connection = target.openConnection();
				connection.setConnectTimeout(connectionTimeout); 
				connection.setReadTimeout(readTimeout);
				return(connection);
			}
		});
		
//		System.out.println(InetAddress.getByName(client.getHost()).getHostAddress());
		
		HttpURLConnection connection = null;
		BufferedReader reader = null;
		
		try{
					
			connection = (HttpURLConnection)client.openConnection();
			
//			if(connection instanceof HttpsURLConnection){
//				((HttpsURLConnection)connection).setHostnameVerifier(new HostnameVerifier() {
//					@Override
//					public boolean verify(String hostName, SSLSession paramSSLSession) {
//						try {
//							Certificate[] certs = paramSSLSession.getPeerCertificates();
//							System.out.println(certs[0]);
//						} catch (SSLPeerUnverifiedException e) {
//							e.printStackTrace();
//						}
//						return true;
//					}
//				});
//			}
			
			if(options.username!=null){
				if(options.authMethod==null || options.authMethod.equals(AUTH_TYPE_BASIC)){
					byte[] encoded = Base64.encodeBase64((options.username+":"+options.password).getBytes());		
					connection.setRequestProperty("Authorization", "Basic " + new String(encoded));
				}
				else if(options.authMethod.equals(AUTH_TYPE_BEARER)){
					connection.setRequestProperty("Authorization", "Bearer " + options.username); //assuming access token is passed
				}
			}

			if (options.accept!=null) 
				connection.setRequestProperty("Accept", options.accept);
			//else connection.setRequestProperty("Accept", "*/*");

			
			//Add custom headers if defined
			if(!options.customHeaders.isEmpty()){
				for(String customKey : options.customHeaders.keySet()){
					connection.setRequestProperty(customKey, options.customHeaders.get(customKey));
				}
			}
			
			
			//Add cookies if any
			List<HttpCookie> requestCookies = cookieManager.getCookieStore().get(new URI(aURL.getProtocol()+"://"+aURL.getHost()+":"+aURL.getPort()));
			if(!requestCookies.isEmpty()){
				String cookiesStr = ""; 
				for(HttpCookie cookie : requestCookies){
					cookiesStr = cookiesStr + (!cookiesStr.isEmpty()?",":"")+cookie.toString(); 
				}
				connection.setRequestProperty("Cookie",cookiesStr);
			}
			
			if(!options.method.equals("GET")){
				if(!options.method.equals("POST") && !options.method.equals("PUT") && !options.method.equals("POST")
					&& !options.method.equals("HEAD") && !options.method.equals("OPTIONS") && !options.method.equals("DELETE")
					&& !options.method.equals("TRACE")){
							
					connection.setRequestProperty("X-HTTP-Method-Override", options.method);
					connection.setRequestMethod("PUT");
				
				}
				else {	
					connection.setRequestMethod(options.method);
				}
				
				connection.setDoInput(true);
				connection.setDoOutput(true);
				//connection.setChunkedStreamingMode(0);

				if(options.content!=null || options.contentFromFile!=null){
					if(options.contentType!=null)
						connection.setRequestProperty("Content-Type", options.contentType);

					OutputStream output = new BufferedOutputStream(connection.getOutputStream());
					if(options.contentFromFile!=null) {
						File file = new File(options.contentFromFile);
						StreamingUtils.sendFileToOutputStream(file, output);
					}
					else {
						output.write(options.content.getBytes(Charset.forName("UTF-8")));
						output.flush();
						output.close();
					}
				}
			}
			
			int code = connection.getResponseCode();
			
			//System.out.println(code);
			
			String message = connection.getResponseMessage();
			
			//Record cookies
			List<String> cookiesHeader = connection.getHeaderFields().get("Set-Cookie");
			if(cookiesHeader!=null){
				for (String cookieStr : cookiesHeader){ 
					List<HttpCookie> cookies = HttpCookie.parse(cookieStr);
					for(HttpCookie cookie : cookies){
						cookieManager.getCookieStore().add(new URI(aURL.getProtocol()+"://"+aURL.getHost()+":"+aURL.getPort()), cookie);
					}
				}
			}
			
			InputStream input; 

			try{
				input = new BufferedInputStream(connection.getInputStream());
			}
			catch (IOException e){
				input = new BufferedInputStream(connection.getErrorStream());
			}
			
			StringBuffer result = new StringBuffer();
			
			if(writeToFile==null || code >= 400) {
				reader = new BufferedReader(new InputStreamReader(input, Charset.forName(options.encoding)));
				String line = null;
				try{
					while ((line = reader.readLine()) != null) {
						result.append(line);
						result.append(fNEWLINE);
					}
				}
				catch (IOException e){
					throw new HttpException(code, message, result.toString(), e);
				}			
				
				reader.close();

				if(code >= 400){
					System.err.println("HTTP Error Code: " + code);
					if(result.length()>0){
						if(result.length() > 200)
							System.err.println(result.toString().substring(0,200));
						else System.err.println(result.toString().trim());
					}
					throw new HttpException(code, message, result.toString()); 
				}
				else{
					return result.toString();
				}				
				
			}
			else {
				FileOutputStream output = new FileOutputStream(writeToFile);
		        byte[] buffer = new byte[64*1024];
		        int length;
		        while ((length = input.read(buffer)) != -1) {
		            output.write(buffer,0,length);
		        }	
		        output.close();
		        
		        return "";
			}
			
		}
		catch(Throwable e){
			if(connection != null){
				connection.disconnect();
			}
			throw e;
		}
		finally{
			if(reader != null){ 
				reader.close();
			}
//			if(connection != null){
//				connection.disconnect();
//			}
		}

		//		try {
		//			InputStream is;
		//			if(client.getProtocol().equals("https")){
		//
		//				//				X509TrustManager tm = new X509TrustManager() {
		//				//		        	@Override
		//				//		        	public X509Certificate[] getAcceptedIssuers() {
		//				//		        		return null;
		//				//		        	}
		//				//		        	@Override	        	 
		//				//		        	public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
		//				//		        	}
		//				//		        	@Override
		//				//		        	public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {
		//				//		        	}
		//				//		        };
		//				//		        	
		//				//		        SSLContext ctx = SSLContext.getInstance("TLS");
		//				//		        ctx.init(null, new TrustManager[] { tm }, null);
		//				//		        SSLContext.setDefault(ctx);  
		//
		//				HttpsURLConnection httpsCon = (HttpsURLConnection) client.openConnection();  
		//
		//				//		        httpsCon.setHostnameVerifier(new HostnameVerifier() {
		//				//			        @Override
		//				//			        public boolean verify(String paramString, SSLSession paramSSLSession) {
		//				//			        	return true;
		//				//			        }
		//				//		        });
		//
		//				httpsCon.connect();
		//
		//				is = httpsCon.getInputStream();
		//			}
		//			else is = client.openStream(); 
		//
		//			reader = new BufferedReader( new InputStreamReader(is) );
		//			String line = null;
		//			while ( (line = reader.readLine()) != null) {
		//				result.append(line);
		//				result.append(fNEWLINE);
		//			}
		//		}

	}


	public static String getContent(String input) throws Exception{
		return getContent(input, HttpOptions.defaultOptions(), false);
	}

	public static String getContent(String input, HttpOptions options) throws Exception{
		return getContent(input, options, false);
	}
	
	public static String getContent(String input, HttpOptions options, boolean warnIfData) throws Exception{
		
		if(input.startsWith("file:///")) input = input.substring("file:///".length()).replace("|", ":");
		if(input.startsWith("file:/")) input = input.substring("file:/".length()).replace("|", ":");

		//FIX 24.11.2017: URL-encode parameters
		Pattern pattern = Pattern.compile("=([^&]*?)$|=([^&]*?)&");
		Matcher matcher = pattern.matcher(input);
		while(matcher.find()){
			String param = matcher.group();
			boolean atEnd = !param.endsWith("&");
			param = param.substring(1, (atEnd ? param.length() : param.length()-1));
			String encoded = URLEncoder.encode(param, "UTF-8");
			if(!encoded.equals(param)){
				encoded = encoded.replace("%3A", ":");
				input = input.substring(0, matcher.start()+1)+encoded+(atEnd? "":input.substring(matcher.end()-1));
			}
		}
		
		URL url = null;
		try {
			url = new URL(input);
		} catch (Exception e) {
		}

		if(url != null){
			String content = HttpTools.fetch(url, options);
			return content;
		}
		else{
			input = URLDecoder.decode(input.replace("+", "%2B"), "UTF-8");
			File file = new File(input);

			if(file.exists()){
				if(options.encoding==null) options.encoding = "UTF-8";
				String content = StreamingUtils.getInputFromFile(file, options.encoding);
				return content;
			}
			else{
				if(warnIfData) System.err.println("HttpTools warning: '"+input + "' is not an URL or an existing file");
				return input;
			}			
		}		
	}

	public static InputStream getContentInputStream (String input, HttpOptions options) throws Exception{
		if(input.startsWith("file:///")) input = input.substring("file:///".length()).replace("|", ":");
		if(input.startsWith("file:/")) input = input.substring("file:/".length()).replace("|", ":");

		if(options.encoding==null) options.encoding = "UTF-8";
		InputStream is;

		URL url = null;
		try {
			url = new URL(input.replace(" ","%20"));
		} catch (Exception e1) {
		}

		if(url!=null){
			String content = HttpTools.fetch(url, options);
			is = new ByteArrayInputStream(content.getBytes(options.encoding));
			return is;
		}
		else{
			File file = new File(input);

			if(file.exists()){
				String content = StreamingUtils.getInputFromFile(file, options.encoding);
				is = new ByteArrayInputStream(content.getBytes("UTF-8"));
				return is;
			}
			else{
				is = new ByteArrayInputStream(input.getBytes(options.encoding));
				return is;
			}
		}
	}
	
	public static List<HttpCookie> getCookies(URI uri){
		List<HttpCookie> requestCookies = cookieManager.getCookieStore().get(uri);
		return requestCookies;
	}
	

	static String fetchWithFormAuth(URL aURL, HttpOptions options) throws Exception{
		HttpOptions modified_options = new HttpOptions(options);
		modified_options.authMethod = AUTH_TYPE_BASIC;
		String result = HttpTools.fetch(aURL, modified_options);
		
		if(result.contains("action=\"j_security_check\"")){
			String url = aURL.toString();
			if(url.contains("?")) url = url.substring(0, url.indexOf("?"));
			if(url.contains("/")) url = url.substring(0, url.lastIndexOf("/"));
			String login_url = url+"/j_security_check?j_username=" + options.username + "&j_password=" + options.password;
			
			modified_options.method = "GET";
			modified_options.username = null;
			modified_options.password = null;
			result = HttpTools.fetch(new URL(login_url), modified_options);
		}
		
		return result;
	}  
	
	static String fetchWithOAuth(URL aURL, HttpOptions options) throws Exception{
		HttpOptions authOptions = HttpOptions.defaultOptions();
		authOptions.method = "POST";
		authOptions.content = "{\"grant_type\": \"client_credentials\", \"client_id\": \""+options.username+"\", \"client_secret\": \""+options.password+"\"}";
		authOptions.contentType = "application/json";
		String result = HttpTools.fetch(new URL(options.authUrl), authOptions);
		JSONObject json = new JSONObject(result);
		String token = json.getString("access_token");
		
		HttpOptions modified_options = new HttpOptions(options);
		modified_options.authMethod = HttpTools.AUTH_TYPE_BEARER;
		modified_options.username = token;
		modified_options.password = null;
		
		result = HttpTools.fetch(aURL, modified_options);
		
		return result;
	}
	
//  For Android tests	
//	public static String getAsset(String name){
//		try {
//			AssetManager am = MainActivity.context.getAssets();
//			InputStream input = am.open(name);
//			BufferedReader ff = new BufferedReader(new InputStreamReader(input));
//			StringBuilder str = new StringBuilder();
//			int read = 0;
//			char[] chars = new char[1024];
//			while ((read = ff.read(chars)) != -1) {
//				str.append(new String(chars, 0, read));
//			}
//			input.close();
//			return str.toString();
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}	

}
