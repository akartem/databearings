package sapl.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Options structure for HttpTools utility
 * 
 * @author Artem Katasonov (VTT + Elisa since July 2018) 
 * 
 * @version 5.1 (31.07.2019)
 * 
 * since 4.5
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2017-2018, VTT
 * 
 * */

public class HttpOptions{
	public String method = "GET";
	public String username = null;
	public String password = null;
	public String authMethod = HttpTools.AUTH_TYPE_BASIC;
	public String authUrl = null;
	public String accept = null;
	public String contentType = null;
	public String encoding = "UTF-8";
	public int timeout = HttpTools.DEFAULT_READ_TIMEOUT; 
	public int connection_timeout = HttpTools.DEFAULT_CONNECTION_TIMEOUT;

	public String content = null;
	public String contentFromFile = null;

	public Map<String,String> customHeaders = new HashMap<String, String>();
	
	public HttpOptions(){}
	
	public HttpOptions(HttpOptions other){
		this.method = other.method;
		this.content = other.content;
		this.contentFromFile = other.contentFromFile;
		this.username = other.username;
		this.password = other.password;
		this.authMethod = other.authMethod;
		this.authUrl = other.authUrl;
		this.accept = other.accept;
		this.contentType = other.contentType;
		this.encoding = other.encoding;
		this.timeout = other.timeout;
		this.connection_timeout = other.connection_timeout;
		this.customHeaders.putAll(other.customHeaders);
	}
	
	public static HttpOptions defaultOptions(){
		return new HttpOptions();
	}
}  
