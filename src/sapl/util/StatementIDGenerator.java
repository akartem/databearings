package sapl.util;

import sapl.core.SaplConstants;

/** 
 * Utility for producing sequential IDs for new statements
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 * 
 * Copyright (C) 2009 Michael Cochez
 * 
 * */

public class StatementIDGenerator extends Counter {
	/**
	 * The maximum number of digits a string representation from a long in base 10 can have 9223372036854775807 is the longest long.
	 */
	private final static int MAX_LONG_STRING_LENGTH = 19;

	/**
	 * The maximum length a statement ID can ever have, used as bounds for the StringBuilder.
	 */
	private final static int MAX_ID_LENGTH = StatementIDGenerator.MAX_LONG_STRING_LENGTH + SaplConstants.STATEMENT_PREFIX.length();

	/**
	 * Produces the next primaryKey statement ID
	 * @return String representation of the ID
	 */
	public String getNewStatementID() {
		StringBuilder builder = new StringBuilder(StatementIDGenerator.MAX_ID_LENGTH);
		builder.append(SaplConstants.STATEMENT_PREFIX);
		builder.append(super.getNextNumber());
		return builder.toString();
	}
}
