package sapl.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Provides some common operations on sets
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 *  * 
 * Copyright (C) 2009, Michael Cochez
 *  
 * */

public class SetOperation {
	/**
	 * Generates a set containing the elements which are present in all provided sets. If a set is null, it is ignored.
	 * 
	 * @param <T>
	 *            The type of the elements in the sets.
	 * @param set1
	 *            the first set in the intersection
	 * @param set2
	 *            the second set in the intersection
	 * @param set3
	 *            the third set in the intersection
	 * @return the set containing the elements which were present in all three sets.
	 */
	public static <T> Set<T> intersect3Sets(Set<T> set1, Set<T> set2, Set<T> set3) {
		HashSet<T> result = new HashSet<T>(set1);
		Iterator<T> it = result.iterator();
		while (it.hasNext()) {
			T value = it.next();
			if ((set2 != null) && !set2.contains(value)) {
				it.remove();
			} else if ((set3 != null) && !set3.contains(value)) {
				it.remove();
			}
		}
		return result;

	}

	//the following method uses the java collections framework, but throws exceptions on null values. => we cannot use this implementation.	
	//	public static <T> Set<T> intersect3Sets(Set<T> set1, Set<T> set2, Set<T> set3){
	//		HashSet<T> result = new HashSet<T>(set1);
	//		result.retainAll(set2);
	//		result.retainAll(set3);
	//		
	//		return result;
	//	}
}
