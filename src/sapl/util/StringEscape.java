package sapl.util;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * String escape utility
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 4.0 (23.09.2013)
 * 
 * Copyright (C) 2013, VTT
 * Copyright (C) 2007-2009, Artem Katasonov, Michael Cochez
 * 
 * */ 

public class StringEscape {

	public static String escape(String pString) {
		final int lStringLength = pString.length();
		final int bufferLength = lStringLength + 2; //the original length + 2 extra positions for escaping
		final StringBuilder lResult = new StringBuilder(bufferLength);
		for (int i = 0; i < lStringLength; i++) {
			char lCharacter = pString.charAt(i);
			switch (lCharacter) {
			case '\\': // back-slash
				lResult.append("\\\\");
				break;
			case '\b': // back-space
				lResult.append("\\b");
				break;
			case '\r': // carriage return
				lResult.append("\\r");
				break;
			case '\f': // form feed
				lResult.append("\\f");
				break;
			case '\t': // horizontal tab
				lResult.append("\\t");
				break;
			case '\n': // new line
				lResult.append("\\n");
				break;
				//			case '\'': // single quote
				//				lResult.append("\\'");
				//				break;
			case '"': // double quote
				lResult.append("\\\"");
				break;
			default:
				// escape ISO control codes with octal escapes to make them visible
				if (Character.isISOControl(lCharacter)) {
					lResult.append('\\' + StringEscape.leftPad(Integer.toOctalString(lCharacter), '0', 3));
				} else { // copy other characters
					lResult.append(lCharacter);
				}
			}
		}
		return (lResult.toString());
	}

	public static String escapeUnsupported(String pString, Charset pOutputEncoding) {
		if (pOutputEncoding.name().startsWith("UTF")) { // if Unicode Transformation Format ...
			return (pString); // ... just return the input string - every character is supported
		} else {
			int lStringLength = pString.length();
			StringBuilder lResult = new StringBuilder(lStringLength);
			CharsetEncoder lEncoder = pOutputEncoding.newEncoder();
			for (int i = 0; i < lStringLength; i++) {
				char lCharacter = pString.charAt(i);
				if (lEncoder.canEncode(lCharacter)) { // if character is supported by encoding ...
					lResult.append(lCharacter); // ... write it as is ...
				} else { // ... otherwise write a Unicode escape for it
					lResult.append("\\u").append(StringEscape.leftPad(Integer.toHexString(lCharacter), '0', 4));
				}
			}
			return (lResult.toString());
		}
	}

	private static boolean isOctalDigit(char pDigit) {
		return ((pDigit >= '0') && (pDigit <= '7'));
	}

	public static String leftPad(String pString, char pWithCharacter, int pFinalLength) {
		int lStringLength = pString.length();
		if (lStringLength >= pFinalLength) {
			return (pString);
		} else {
			StringBuilder lResult = new StringBuilder(pFinalLength);
			for (int i = pFinalLength - lStringLength; i > 0; i--) {
				lResult.append(pWithCharacter);
			}
			lResult.append(pString);
			return (lResult.toString());
		}
	}

	public static String unEscape(String pString) {
		if (!pString.contains("\\")) {
			return pString;
		}

		String lString = StringEscape.unEscapeUnicode(pString, false, false); // first, un-escape Unicode escapes
		int lStringLength = lString.length();
		StringBuilder lResult = new StringBuilder(lStringLength);
		boolean lBackSlash = false;
		char lCharacter;
		for (int i = 0; i < lStringLength; i++) {
			lCharacter = lString.charAt(i);
			if (lBackSlash) {
				switch (lCharacter) {
				case '\\': // back-slash
					lResult.append("\\");
					break;
				case 'b': // back-space
					lResult.append("\b");
					break;
				case 'r': // carriage return
					lResult.append("\r");
					break;
				case 'f': // form feed
					lResult.append("\f");
					break;
				case 't': // horizontal tab
					lResult.append("\t");
					break;
				case 'n': // new line
					lResult.append("\n");
					break;
				case '"': // double quote
					lResult.append("\"");
					break;
				case '\'': // single quote
					lResult.append("'");
					break;
				case '0': // octal 8-bit character escape
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
					String lOctalDigits = Character.toString(lCharacter);
					// if the next character is also an octal digit
					if ((i + 1 < lStringLength) && StringEscape.isOctalDigit(lString.charAt(i + 1))) {
						i++;
						lOctalDigits += lString.charAt(i);
						// if the next character is also an octal digit
						if ((i + 1 < lStringLength) && StringEscape.isOctalDigit(lString.charAt(i + 1)) && (lOctalDigits.charAt(0) <= '3')) { // only 8-bit allowed
							i++;
							lOctalDigits += lString.charAt(i);
						}
					}
					lResult.append((char) Short.parseShort(lOctalDigits, 8));
					break;
				default: // invalid escape
					lResult.append(lCharacter);
				}
				lBackSlash = false;
			} else {
				if (lCharacter == '\\') {
					lBackSlash = true;
				} else {
					lResult.append(lCharacter);
				}
			}
		}
		return (lResult.toString());
	}

	public static String unEscapeUnicode(String pString, boolean pUnEscape7bitOnly, boolean pJustOneLevel) {
		int lStringLength = pString.length();
		StringBuilder lResult = new StringBuilder(lStringLength);
		boolean lBackSlash = false;
		char lCharacter;
		for (int i = 0; i < lStringLength; i++) {
			lCharacter = pString.charAt(i);
			if (lCharacter == '\\') {
				lBackSlash = !lBackSlash;
				// odd backslash followed by 'u', must be an Unicode escape
				if (lBackSlash && (i + 1 < lStringLength) && (pString.charAt(i + 1) == 'u')) {
					int lIndex = i;
					if (pJustOneLevel && (i + 2 < lStringLength) // if un-escape just one level ...
							&& (pString.charAt(i + 2) == 'u')) { // ... and more than one level ...
						i++; // ... just decrease the level
					} else { // if un-escape everything or current level ist the last one, unescape
						//try {
						while (pString.charAt(lIndex + 2) == 'u') { // skip the extra 'u's
							lIndex++;
						} // while
						int lUnicode = Integer.parseInt(pString.substring(lIndex + 2, lIndex + 6), 16);
						if (!pUnEscape7bitOnly || (lUnicode < 0x80)) {
							lCharacter = (char) lUnicode;
							i = lIndex + 5;
							lBackSlash = false;
						} else {
							i = lIndex;
						}
					}
				}
			} else {
				lBackSlash = false;
			}
			lResult.append(lCharacter);
		}
		return (lResult.toString());
	}

}
