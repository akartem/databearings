package sapl.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Utilities which make working with byte and character streams more convenient.
 * 
 * @author Michael Cochez (University of Jyväskylä)
 * @author Artem Katasonov (VTT + Elisa since July 2018) 
 * 
 * @version 5.1 (31.07.2018)
 * 
 * Copyright (C) 2019, Elisa
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2009 Michael Cochez
 * 
 * */

public class StreamingUtils {
	
	public static final String UTF8_BOM = "\uFEFF";
	
	public static long sendFileToOutputStream(File file, OutputStream os) throws FileNotFoundException, IOException {

		FileInputStream fis = new FileInputStream(file);
		try {
			long total = 0;
			int read = 0;
			byte[] bytes = new byte[1024];
			//While there are still bytes in the file, read them and write them to our OutputStream
			while ((read = fis.read(bytes)) != -1) {
				os.write(bytes, 0, read);
				total += read;
			}
			return total;
		} finally {
			//Clean resources
			fis.close();
			os.flush();
			os.close();
		}
	}

	public static String getInputFromFile(File file, String encoding) throws IOException {
		int len = (int) file.length();
		char mess[] = new char[len];
		InputStreamReader iread = null;
		BufferedReader ff = null;
		try {
			
			if (encoding==null) {
				iread = new FileReader(file);
			} else {
				iread = new InputStreamReader(new FileInputStream(file), encoding);
			}
			ff = new BufferedReader(iread);
			int read = ff.read(mess, 0, len);
			String data = new String(mess, 0, read);
			
			//Fix in 4.5: remove potential UTF-8 byte order mark
			if(encoding.equals("UTF-8") && data.startsWith(UTF8_BOM))
	            data = data.substring(1); 

			return data;
		} finally {
			if (ff != null) {
				ff.close();
			} else if (iread != null) {
				iread.close();
			}
		}
	}
	
	public static boolean writeToFile(String filename, String text, boolean append, String encoding){
		BufferedWriter ff = null;
		OutputStreamWriter fwrite = null;
		try {
			File datafile = new File(filename);
			File p = datafile.getParentFile();
			if ((p != null) && !p.exists()) {
				boolean directoryMakingSuccessFul = p.mkdirs();
				if (!directoryMakingSuccessFul) {
					System.err.println("Failed creating necessary directories : " + p.getCanonicalPath());
					return false;
				}
			}
			if(encoding==null){
				fwrite = new FileWriter(filename, append);
			}
			else {
				fwrite = new OutputStreamWriter(new FileOutputStream(filename, append), encoding);
			}
			ff = new BufferedWriter(fwrite);

			ff.write(text, 0, text.length());
			ff.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (ff != null) {
				try {
					ff.close();
				} catch (IOException e) {
					// not much we can do here.
				}
			} else {
				//try closing FileWriter at least
				if (fwrite != null) {
					try {
						fwrite.close();
					} catch (IOException e) {
						// not much we can do here.
					}
				}
			}
		}
	}
}
