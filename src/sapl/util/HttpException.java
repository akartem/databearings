package sapl.util;

/**
 * Exception thrown by HttpTools utility
 * 
 * @author Artem Katasonov (VTT) 
 * 
 * @version 4.5 (14.12.2017)
 * 
 * @since 4.5
 * 
 * Copyright (C) 2017, VTT
 * 
 * */

public class HttpException extends Exception{
	private static final long serialVersionUID = 1L;

	int statusCode;
	
	String result;
	
	public HttpException(int s, String m, String r){
		super(m);
		statusCode = s; 
		result = r;
	}

	public HttpException(int s, String m, String r, Throwable cause){
		super(m, cause);
		statusCode = s; 
		result = r;
	}
	
	public int getStatusCode() {
		return statusCode;
	}

	public String getResult() {
		return result;
	}
	
	@Override
	public void printStackTrace() {
		super.printStackTrace();
		System.err.println("HTTP Code: " + statusCode);
		System.err.println("HTTP Message: " + this.getMessage());
		System.err.println("HTTP Body: " + result);
	}
}
