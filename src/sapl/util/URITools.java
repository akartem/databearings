package sapl.util;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Some URI utilities
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2019)
 * 
 * Copyright (C) 2009 Michael Cochez
 * 
 * */

public class URITools {
	/**
	 * Generates a URI if the a URI can be made with the given String representation. Throws an error with message
	 * "The String representation for this URI is hard coded and must not be wrong." otherwise. Use this method only to create URI's from
	 * Strings which are hard coded and about which the developer is sure that it is a valid URI.
	 * 
	 * @param uriString
	 * @return The URI
	 */
	public static URI createUncheckedURI(String uriString) {
		try {
			return new URI(uriString);
		} catch (URISyntaxException e) {
			throw new Error("The String representation for this URI is hard coded and must not be wrong.");
		}
	}
}
