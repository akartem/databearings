package sapl.util;

import java.lang.ref.SoftReference;

/**
 * This class is used for optimizing the usage of long strings of whitespaces. It has one long string from which substrings are taken. This
 * allows the JVM to reuse that memory. When a whitespace string longer than the cached string is requested, a new (longer) string will be
 * generated and kept for later use.
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2019) 
 * 
 * Copyright (C) 2009 Michael Cochez
 * 
 * */

public class WhiteSpaceString {

	private static final String initialString = "                                                                                                                        ";
	/**
	 * The cached white space string.
	 */
	private static SoftReference<String> whiteSpace = new SoftReference<String>(null);

	/**
	 * generates a {@link String} of whitespaces with the given length
	 * 
	 * @param length
	 * @return The string of whitespaces with the requested length.
	 */
	public static String getWhiteSpaceString(int length) {
		String cachedString = WhiteSpaceString.whiteSpace.get();//get hard reference to cached string
		String theString;//the string from which we'll return a part
		if (cachedString == null) {//there is no chached string
			theString = WhiteSpaceString.initialString;//start from the initialString.
			//WhiteSpaceString.whiteSpace = new SoftReference<String>("                                                   ");
		} else {
			theString = cachedString;//start from the cachedString
		}
		int theStringLength = theString.length();//length of the string we'll return a part from
		if (length > theStringLength) { //we need to grow the string.
			//build a string which is long enough by copying the current string as often as needed.
			StringBuilder builder = new StringBuilder(length * 2); // it can never need more as length * 2 since length < theStringLength
			builder.append(theString);
			for (int i = 0; i < (length / theStringLength); i++) {//append theString as many times an needed.
				builder.append(theString);
				System.out.println("increased size by " + theStringLength + " to " + builder.length());
			}
			theString = builder.toString();//take this as the new string
		}
		if ((cachedString == null) || (theString.length() > cachedString.length())) {//cache theString for later reuse. shortcut prevents NullPointerException.
			WhiteSpaceString.whiteSpace = new SoftReference<String>(theString);
		}
		return theString.substring(0, length);
	}

	/**
	 * runs a small test for the optimizer
	 * 
	 * @param args
	 */
	/*public static void main(String[] args) {
		//start from a positive test
		boolean success = true;
		for (int i = 0; i < 50; i++) {
			if (!WhiteSpaceString.test(i)) {
				//a test failed => whole test failed
				success = false;
				break;
			}
		}
		for (int i = 250; i < 23592960; i++) {
			if (!WhiteSpaceString.test(i)) {
				//a test failed => whole test failed
				success = false;
				break;
			}
		}
		if (success) {
			System.out.println("All tests passed");
		} else {
			System.out.println("Test failed");
		}
	}

	private static boolean test(int length) {
		String a = WhiteSpaceString.getWhiteSpaceString(length);
		if (a.length() != length) {
			return false;
		}
		return true;
	}
	 */
}