package sapl.util;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * A utility to play sound (AIFF, AU, WAV, MIDI types 1 and 2, and RMF) files.
 *  <p>
 *   Modified from the code at
 *   <a href="http://stackoverflow.com/questions/2416935/how-to-play-wav-files-with-java">How to play .wav files with java</a>
 *   by user <i>painkiller</i></p>
 *  <p>
 *   The supported audio file types are probably AIFF, AU, WAV, MIDI (types 1 and 2), and RMF sound formats which are
 *   rendered at 22 KHz in 16-bit stereo with relatively low CPU usage.
 *  </p>
 * 
 * Adapted from FP7 USEFIL project code
 * 
 * @author Jarkko Leino (VTT)
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.1 (17.01.2014) 
 * 
 * @since 4.1
 * 
 * Copyright (C) 2014, VTT
 * 
 * */

public class AudioTools {

    private static final int BUFFER_SIZE = 128000;

    /**
     * Plays a sound file
     * @param filename Name of the file to play
     * @throws IOException 
     * @throws UnsupportedAudioFileException 
     * @throws LineUnavailableException 
     */
    public static void playSound(String filename) throws UnsupportedAudioFileException, IOException, LineUnavailableException{

        String strFilename = filename;

        if ( filename==null || filename.length() == 0 )
          return;
        
        File soundFile = new File(strFilename);

        AudioInputStream audioStream = AudioSystem.getAudioInputStream(soundFile);

        AudioFormat audioFormat = audioStream.getFormat();

        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);

        SourceDataLine sourceLine = (SourceDataLine) AudioSystem.getLine(info);
        sourceLine.open(audioFormat);

        sourceLine.start();

        int nBytesRead = 0;
        byte[] abData = new byte[BUFFER_SIZE];
        while (nBytesRead != -1) {
            nBytesRead = audioStream.read(abData, 0, abData.length);

            if (nBytesRead >= 0) {
                sourceLine.write(abData, 0, nBytesRead);
            }
        }

        sourceLine.drain();
        sourceLine.close();
    }
}