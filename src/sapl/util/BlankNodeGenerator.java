package sapl.util;

import sapl.core.SaplConstants;

/** 
 * Utility for producing IDs for new RDF blank nodes
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.0 (25.10.2013)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013, VTT
 * 
 * */

public class BlankNodeGenerator extends Counter {
	/**
	 * The maximum number of digits a string representation from a long in base 10 can have 9223372036854775807 is the longest long.
	 */
	private final static int MAX_LONG_STRING_LENGTH = 19;

	/**
	 * The maximum length a statement ID can ever have, used as bounds for the StringBuilder.
	 */
	private final static int MAX_ID_LENGTH = BlankNodeGenerator.MAX_LONG_STRING_LENGTH + SaplConstants.BLANK_NODE_PREFIX.length();

	private String bnNameBase = null; 


	/**
	 * Produces the next primaryKey statement ID
	 * @return String representation of the ID
	 */
	public String getNewBlankNode() {

		String time = String.valueOf(System.currentTimeMillis()); 

		if ((this.bnNameBase == null) || !this.bnNameBase.contains(time)) {
			super.zero();
			this.bnNameBase = SaplConstants.BLANK_NODE_PREFIX + time + "_";
		}

		StringBuilder builder = new StringBuilder(BlankNodeGenerator.MAX_ID_LENGTH);
		builder.append(this.bnNameBase);
		builder.append(super.getNextNumber());
		return builder.toString();
	}
}
