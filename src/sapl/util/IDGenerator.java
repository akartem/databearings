package sapl.util;

import java.net.URI;

/**
 * This class implements an ID generator, every time getNewID is called, a new uninque ID is generated. The id's are in the
 * "http://www.ubiware.jyu.fi/id#" namespace
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.0 (01.07.2013)
 * 
 *  Copyright (C) 2013, VTT
 *  Copyright (C) 2009, Michael Cochez
 *  
 * */

public class IDGenerator extends UniqueInstanceGenerator {

	/**
	 * The URI used to prefix all ID's.
	 */
	public static final URI ID_NS_URI = URITools.createUncheckedURI("http://www.ubiware.jyu.fi/id/");

	/**
	 * Create an IDGenerator which uses the given prefix.
	 * 
	 * @param prefixForID
	 *            the prefix for the IDs
	 */
	public IDGenerator(final String prefixForID) {
		super(prefixForID, IDGenerator.ID_NS_URI);
	}

}
