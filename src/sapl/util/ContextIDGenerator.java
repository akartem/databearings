package sapl.util;

import sapl.core.SaplConstants;

/** 
 * Utility for producing sequential IDs for new context containers
 * 
 * @author Michael Cochez (University of Jyväskylä) 
 * 
 * @version 3.1 (2009)
 * 
 * Copyright (C) 2009, Michael Cochez
 *  
 * */

public class ContextIDGenerator extends Counter {
	/**
	 * The maximum number of digits a string representation from a long in base 10 can have 9223372036854775807 is the longest long.
	 * 
	 */
	private final static int MAX_LONG_STRING_LENGTH = 19;

	/**
	 * The maximum length a context ID can ever have, used as bounds for the StringBuilder.
	 */
	private final static int MAX_ID_LENGTH = ContextIDGenerator.MAX_LONG_STRING_LENGTH + SaplConstants.CONTEXT_PREFIX.length();

	/**
	 * get the next primaryKey contextID
	 * 
	 * @return String representation of the ID
	 */
	public String getNewContextID() {
		StringBuilder builder = new StringBuilder(ContextIDGenerator.MAX_ID_LENGTH);
		builder.append(SaplConstants.CONTEXT_PREFIX);
		builder.append(super.getNextNumber());
		return builder.toString();
	}
}
