package sapl.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Used to redirect output System.out and System.err into the common log
 * 
 * @author Artem Katasonov (University of Jyväskylä + VTT 2009-2018)
 * 
 * @version 4.6 (22.02.2018)
 * 
 * Copyright (C) 2013-2018, VTT
 * Copyright (C) 2007-2009, Artem Katasonov
 *  
 * */

public class LoggerHelper extends PrintStream{

	private static Logger logger = null;
	private static Logger logger2 = null;

	public PrintStream oldOut;
	private Level level;
	private boolean bothLoggers;
	
	private StringBuilder save;
	private boolean doSave = false;

	public static void init(String reasonerName, int log_limit, int log_number){
		if(logger!=null) return;
		logger = Logger.getLogger("saplOut");
		logger2 = Logger.getLogger("saplErr");
		
		logger.setLevel(Level.FINE);
		logger2.setLevel(Level.FINE);
		
		System.setProperty ("java.util.logging.SimpleFormatter.format", "%4$s: %5$s%n");

		try {
			File folder = new File("log");
			if(!folder.exists()) folder.mkdir();

			Handler fh = new FileHandler("log/"+reasonerName+"_%g.log", log_limit, log_number);
			fh.setFormatter(new SimpleFormatter());
			logger.addHandler(fh);
			logger.setUseParentHandlers(false);
			
			Handler fh2 = new FileHandler("log/"+reasonerName+"_err_%g.log", log_limit, log_number);
			fh2.setFormatter(new SimpleFormatter());
			logger2.addHandler(fh2);
			logger2.setUseParentHandlers(false);
				
		} catch (IOException le) {
			le.printStackTrace();
		}		
	}

	public LoggerHelper(PrintStream old, Level level, boolean both){
		super(old);
		this.oldOut = old;
		this.level = level;
		this.bothLoggers = both;
		
		save = new StringBuilder();
	}
	
	public void setDoSave(boolean value){
		doSave = value;
	}

	@Override
	public void write(byte  b[], int  off, int  len){ 
		//send to log
		String text = new String(b, off, len);
		if(!text.trim().isEmpty()){
			logger.log(level, text);
			if(bothLoggers) logger2.log(level, text);
		}
		
		//print to original stream
		oldOut.write(b,off,len);
		
		if(doSave) save.append(text);
	}
	
	public String getAndClearSave(){
		String result = save.toString();
		save = new StringBuilder();
		return result;
	}
	
}