package sapl;

import java.util.ArrayList;

import sapl.core.SaplAgent;
import sapl.core.SaplReasoner;
import sapl.shared.eii.UniversalAdapterBehavior;

/**
 * Wrapping an S-APL Agent for execution as a Windows service or Linux daemon 
 *   
 * @author Artem Katasonov (VTT)
 * 
 * @version 4.5 (08.09.2017)
 * 
 * @since 4.0
 * 
 * Copyright (C) 2013-2018, VTT
 * 
 * */

public class SaplDaemon {

	//for jsvc on Linux 
	protected SaplAgent agent;

	//for procrun on Windows
	protected static SaplAgent staticAgent;
	
	protected static String name = null;
	protected static String[] scripts;

	static {
		//System.setProperty ("jsse.enableSNIExtension", "false");
		
		SaplReasoner.PERFORMANCE_CYCLE_PRINT = false;
		SaplReasoner.SLEEP_PRINT = false;
		SaplAgent.SLEEP_PRINT = false;
		UniversalAdapterBehavior.EXCEPTION_FULL_STACK_PRINT = false;
	}

	//for jsvc on Linux
	public void init(String[] args){
		System.out.println("in init");

		processArgs(args);

		for(String arg : scripts)
			System.out.println(arg);
	}

	//for jsvc on Linux
	public void start(){
		System.out.println("in start");
		agent = new SaplAgent(name, scripts);
		agent.start();
	}

	//for jsvc on Linux
	public void stop(){
		System.out.println("in stop");
		agent.stop();
	}

	//for jsvc on Linux
	public void destroy(){
		System.out.println("in destroy");
	}


	//for procrun on Windows
	public static void start(String[] args){
		System.out.println("in start");
		processArgs(args);
		staticAgent = new SaplAgent(name, scripts);
		staticAgent.start();
	}

	//for procrun on Windows
	public static void stop(String[] args){
		System.out.println("in stop");
		staticAgent.stop();
	}
	
	
	protected static void processArgs(String[] args){
		ArrayList<String> scriptArgs = new ArrayList<String>();
		boolean inName = false;
		for(String arg : args){
			
			arg = arg.trim();
			
			if(inName){
				name = arg;
				inName = false;
			}
			else {
				if(arg.startsWith("-")){
					if(arg.equals("-name")){
						inName = true;
					}
				}
				else scriptArgs.add(arg);
			}
		}
		
		if(name==null) name = String.valueOf(System.currentTimeMillis());
		scripts = scriptArgs.toArray(new String[scriptArgs.size()]);
	}

}
